# ConcepTIS project
# (c) Alex V Eustrop 2009
# see LICENSE at the project's root directory
#

AWK?="awk"
usage:
	@echo "Usage: make (all|clean|status|mkcert|wc)"
all: 
	@echo "-- Making everything"
#	cd src/java/tests; ${MAKE} build
	cd contrib; ${MAKE} all
	cd src/sql/PGSQL; ${MAKE} all
	cd src/java/zDAO; ${MAKE} build
	cd src/java/zWebapps; ${MAKE} build
	cd src/java/qDBPool; ${MAKE} build
	cd src/java/qSessionCookie; ${MAKE} build
	cd src/java/qTISWebApps; ${MAKE} build
	cd src/java/zDBPool; ${MAKE} build
	cd src/java/webapps/psql; ${MAKE} all
	cd src/java/webapps/tisc; ${MAKE} install
clean: 
	@echo "-- Cleaning everything"
	cd contrib; ${MAKE} clean
	cd src/java/tests; ${MAKE} clean
	cd src/java/; ${MAKE} clean
	cd src/sql/PGSQL; ${MAKE} clean
wc: clean
	@echo "counting code lines"
	find ./ -type f -name "*.sql" -or -name "*.awk" -or \
	-name "Makefile" -or -name "*.tab" -or -name "*.ot" \
	-or -name "*.java" -or -name "*.jsp" -or -name "*.js" \
	-or -name "*.css" -or -name "*.sh" | xargs wc -l
mkcert:
	@echo "Creating new self-signed SSL certificate for HTTPS:"
	mkdir -p ssl/cert/
	@echo "WARNING: existing self-signed certificate will be LOST!"
	@echo "press Enter to continue or Ctl+C for stopping"
	@read a
	echo Ok! use: openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ssl/cert/server-selfsigned.key -out ssl/cert/server-selfsigned.crt
install_fbsd_pkgs:
	@echo This target requires root access
	sh etc/freebsd_pkg_install.sh
push_pub: push_bitbucket
push_bitbucket:
	git push git@bitbucket.org:eustrop/conceptis.git
status:
	hg status | more
re: clean all
