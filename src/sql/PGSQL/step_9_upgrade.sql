-- (re)load all functions.
-- NOTE: this action should be safe, since no table or sequence would be dropped
\i SAM/load_tis.sql
\i TIS/load_tis.sql
\i TISC/load_tis.sql
\i FS/load_tis.sql
\i MSG/load_tis.sql
\i QR/load_tis.sql
