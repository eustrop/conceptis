DROP FUNCTION IF EXISTS SAM.update_Group(
	v_id	bigint,
	v_sid	bigint,
	v_name	varchar(32),
	v_descr	varchar(99)
) CASCADE;
