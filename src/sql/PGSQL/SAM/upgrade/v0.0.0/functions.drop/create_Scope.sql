DROP FUNCTION IF EXISTS SAM.create_Scope(
	v_id	bigint,
	v_name	varchar(32),
	v_is_local	char(1),
	v_auditlvl	char(1),
	v_descr	varchar(99)
) CASCADE;
