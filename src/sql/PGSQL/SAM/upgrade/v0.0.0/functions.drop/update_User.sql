DROP FUNCTION IF EXISTS SAM.update_User(
	v_id	bigint,
	v_sid	bigint,
	v_slevel	smallint,
	v_locked	char(1),
	v_lang	char(2),
	v_login	varchar(32),
	v_db_user	varchar(64),
	v_full_name	varchar(99)
) CASCADE;
