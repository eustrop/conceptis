-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

-- REMEMBER! All functions execulable by PUBLIC by default!
-- this set of functions are safe but another one are not.
CREATE OR REPLACE FUNCTION sam.h2i(text) RETURNS bigint IMMUTABLE AS
$$select ('x'||lpad(translate($1,':- ',''),16,'0'))::bit(64)::int8$$
	LANGUAGE SQL SECURITY INVOKER;
GRANT EXECUTE on function sam.h2i(text) to PUBLIC;
CREATE OR REPLACE FUNCTION sam.h2s(varchar(4)) RETURNS smallint IMMUTABLE AS
$$select ('x'||lpad($1,8,'0'))::bit(32)::int4::int2$$
	LANGUAGE SQL SECURITY INVOKER;
GRANT EXECUTE on function sam.h2s(varchar(4)) to PUBLIC;

-- 0. SLEVEL MAC configuration
--    get_slevel_max() - maximum slevel this system installation permitted to processing
--    get_slevel_min() - minimum slevel this system permit to create or write to its users via standard mechanisms
--    get_slevel_strongBLM() - use strong BLM to all users with slevel greater or equal so objects starting from this slevel more protected from leakage 
--    get_slevel_system() - writing to objects with system level or less require special capability WRITE_SYSEM_OBJECTS at least (other conditions may be required)
--    get_slevel_SAM() - treate all SAM objects as having this SLEVEL at least
--    get_scope_slevel_max(bigint) - helper function to  find maximum slevel for scope by id
--    get_scope_slevel_min(bigint) - helper function to  find minimum slevel for scope by id
--    default get_slevel_max() - 64 (40) - PRE-CONFIDENTIAL
--    default get_slevel_min() - 16 (10) - lowest from "Public" slevel (0x10..0x1F)
--    default get_slevel_strongBLM() - 64 (40) - PRE-CONFIDENTIAL
--    default get_slevel_system() - 15 (0F) System
--    default get_slevel_SAM() - 47 (2F) Public+
--num	Code - Value 	Description
-- 15 	0F - System 	objects with SLEVEL System (00..0F) or less are writable only by subjects with SLEVEL not greater than system
-- 31 	1F - Public 	objects with SLEVEL Public (10..1F) are readable by anyone including guests
-- 47 	2F - Public+ 	objects with SLEVEL Public+ (20..2F) are readable by identified users only with SLEVEL greater or equal its SLEVEL
-- 63 	3F - Restricted/FOUO (DSP) 	objects with SLEVEL Restrited/FOUO (30..3F) are readable only by authorized users with SLEVEL greater or equal its SLEVEL
-- 64 	40 - PRE-CONFIDENTIAL (DSP+) 	objects and subjects with SLEVEL PRE-CONFIDENTIAL (0x40) must be treated under CONFIDENTIAL policy
-- 79 	4F - CONFIDENTIAL (S) 	objects with SLEVEL CONFIDENTIAL (40..4F) are under goverment regulation and are managed in accordance to strict MAC model (Bell–LaPadula)
-- 95 	5F - SECRET (SS) 	objects with SLEVEL SECRET (50..5F) are under goverment regulation and are managed in accordance to strict MAC model (Bell–LaPadula)
-- 111 	6F - TOP SECRET (SSOV) 	objects with SLEVEL SECRET (60..6F) are under goverment regulation and are managed in accordance to strict MAC model (Bell–LaPadula)
-- 127 	7F - GUO God-use-only 	SLEVEL God-use-only (70..7F) can be used for objects and subjects that are more guarded than any legally defined levels

CREATE OR REPLACE FUNCTION sam.get_slevel_max() RETURNS smallint STABLE AS 'select 64::smallint' 
    LANGUAGE SQL SECURITY INVOKER;
GRANT EXECUTE on function sam.get_slevel_max() to PUBLIC;

CREATE OR REPLACE FUNCTION sam.get_slevel_min() RETURNS smallint STABLE AS 'select 16::smallint' 
    LANGUAGE SQL SECURITY INVOKER;
GRANT EXECUTE on function sam.get_slevel_min() to PUBLIC;

CREATE OR REPLACE FUNCTION sam.get_slevel_strongBLM() RETURNS smallint STABLE AS 'select 64::smallint' 
    LANGUAGE SQL SECURITY INVOKER;
GRANT EXECUTE on function sam.get_slevel_strongBLM() to PUBLIC;

CREATE OR REPLACE FUNCTION sam.get_slevel_system() RETURNS smallint STABLE AS 'select 15::smallint' 
    LANGUAGE SQL SECURITY INVOKER;
GRANT EXECUTE on function sam.get_slevel_system() to PUBLIC;

CREATE OR REPLACE FUNCTION sam.get_slevel_SAM() RETURNS smallint STABLE AS 'select 47::smallint' 
    LANGUAGE SQL SECURITY INVOKER;
GRANT EXECUTE on function sam.get_slevel_SAM() to PUBLIC;

CREATE OR REPLACE FUNCTION sam.get_scope_slevel_max(bigint) RETURNS smallint STABLE AS 'select slevel_max from SAM.Scope where id = $1' 
    LANGUAGE SQL SECURITY INVOKER;
-- SIC! must think about allowance of this to PUBLIC 2023-04-44 Eustrop
GRANT EXECUTE on function sam.get_scope_slevel_max(bigint) to PUBLIC;

CREATE OR REPLACE FUNCTION sam.get_scope_slevel_min(bigint) RETURNS smallint STABLE AS 'select slevel_min from SAM.Scope where id = $1'
    LANGUAGE SQL SECURITY INVOKER;
-- SIC! must think about allowance of this to PUBLIC 2023-04-44 Eustrop
GRANT EXECUTE on function sam.get_scope_slevel_min(bigint) to PUBLIC;

-- 1. get_ver* - functions to get version of TIS system
CREATE OR REPLACE FUNCTION sam.get_ver_project() RETURNS text STABLE AS 'select ''ConcepTIS'''
    LANGUAGE SQL SECURITY INVOKER;
GRANT EXECUTE on function sam.get_ver_project() to PUBLIC;

CREATE OR REPLACE FUNCTION sam.get_ver_major() RETURNS int STABLE AS 'select 0 '
    LANGUAGE SQL SECURITY INVOKER;
GRANT EXECUTE on function sam.get_ver_major() to PUBLIC;

CREATE OR REPLACE FUNCTION sam.get_ver_minor() RETURNS int STABLE AS 'select 9 '
    LANGUAGE SQL SECURITY INVOKER;
GRANT EXECUTE on function sam.get_ver_minor() to PUBLIC;

CREATE OR REPLACE FUNCTION sam.get_ver_revision() RETURNS int STABLE AS 'select 3 '
    LANGUAGE SQL SECURITY INVOKER;
GRANT EXECUTE on function sam.get_ver_revision() to PUBLIC;

CREATE OR REPLACE FUNCTION sam.get_ver_build() RETURNS bigint STABLE AS 'select 202312202316'
    LANGUAGE SQL SECURITY INVOKER;
GRANT EXECUTE on function sam.get_ver_build() to PUBLIC;

CREATE OR REPLACE FUNCTION sam.get_ver_status() RETURNS text STABLE AS 'select ''ALPHA2'''
    LANGUAGE SQL SECURITY INVOKER;
GRANT EXECUTE on function sam.get_ver_status() to PUBLIC;

CREATE OR REPLACE FUNCTION sam.get_version() RETURNS text STABLE AS
    'select sam.get_ver_project()||''.v''||sam.get_ver_major()||''.''||sam.get_ver_minor()||''.''||sam.get_ver_revision()||''-''||sam.get_ver_status()||''-b''||sam.get_ver_build()'
    LANGUAGE SQL SECURITY INVOKER;
GRANT EXECUTE on function sam.get_version() to PUBLIC;

-- check QOID for validity at this installation

CREATE OR REPLACE FUNCTION sam.check_QOID(QOID bigint) RETURNS bigint STABLE AS
    'select CASE WHEN QOID > 1048575 THEN QOID ELSE null END'
    LANGUAGE SQL SECURITY INVOKER;

--
-- inital configiration for scope 0 (main)
--
CREATE OR REPLACE FUNCTION sam.inital_configuration_once() RETURNS text VOLATILE
  LANGUAGE plpgSQL SECURITY INVOKER as $$
DECLARE
 v_r SAM.Scope%ROWTYPE;
BEGIN
 -- 1) lock required tables
 LOCK TABLE SAM.Scope IN EXCLUSIVE MODE;
 SELECT * from SAM.Scope XS into v_r WHERE XS.id = 0;
 -- 2) return null if no sequence
 IF FOUND THEN
  RETURN 'Fail! main scope (id=0) already configured';
 END IF;
 -- Scopes
 insert into SAM.Scope values(0,0,sam.get_slevel_min(),sam.get_slevel_max(),'MAIN','Y','0',null,'this installation scope');
 insert into SAM.Scope values(2^20,2^20,sam.get_slevel_min(),sam.get_slevel_max(),'LOCAL','Y','0',null,'LOCAL gray scope for local gray objects');
 -- Ranges
 insert into SAM.Range values(0,32::int2,(2^32-1)::bigint,0,40::int2,0,'0/40 - local installation ''gray'' range 0/32 part of global ''gray''');
 insert into SAM.Range values(0,20::int2,(2^20-1)::bigint,0,32::int2,0,'0/30 - system static objects shipped with ConcepTIS/QTIS distribution');
 insert into SAM.Range values((2^20)::bigint,20::int2,(2^21-1)::bigint,0,32::int2,0,'10:0000/20 - this installation ''gray'' objects range');
 insert into SAM.Range values(sam.h2i('0000:0100:0000:0000'),40::int2,sam.h2i('0000:01FF:FFFF:FFFF'),0,64::int2,0,'0100:0000:0000/40 - qxyz.ru range');
 -- qxyz.ru ranges
 insert into SAM.Range values(sam.h2i('0000:0100:0000:0000'),32::int2,sam.h2i('0000:0100:FFFF:FFFF'),sam.h2i('0000:0100:0000:0000'),40::int2,0,'0100:0000:0000/32 - qxyz.ru range for system');
 insert into SAM.Range values(sam.h2i('0000:0101:0000:0000'),32::int2,sam.h2i('0000:0101:FFFF:FFFF'),sam.h2i('0000:0100:0000:0000'),40::int2,0,'0100:0000:0000/32 - qxyz.ru range for clients');
 -- static users definition. Next regular dfatabase users was defined in QTIS project
 --CREATE USER qtisadmin LOGIN INHERIT IN ROLE qtis_users;
 --CREATE USER qtisoperator LOGIN INHERIT IN ROLE qtis_users;
 --CREATE USER qtisreplicator LOGIN INHERIT IN ROLE qtis_users;
 --CREATE USER qtiswww LOGIN INHERIT IN ROLE qtis_users;
 --CREATE USER qtisguest LOGIN INHERIT IN ROLE qtis_users;
 --CREATE USER qtisnobody LOGIN INHERIT IN ROLE qtis_users;

 insert into SAM.User values(0,0,15::int2,0::int2,127::int2,'N','EN','dbo',SESSION_USER,'Database Owner');
 insert into SAM.UserCapability values(0,0,'SAM_MANAGE');
 insert into SAM.UserCapability values(0,0,'SAM_MANAGE_SLEVEL_MAX');
 insert into SAM.User values(1,0,63::int2,0::int2,127::int2,'N','EN','qtisadmin','qtisadmin','Database Administrator');
 insert into SAM.UserCapability values(1,0,'SAM_MANAGE');
 insert into SAM.UserCapability values(1,0,'SAM_MANAGE_SLEVEL_MAX');
 insert into SAM.User values(2,0,63::int2,16::int2,63::int2,'N','EN','qtisoperator','qtisoperator','Database Operator');
 insert into SAM.User values(3,0,sam.get_slevel_max(),0::int2,sam.get_slevel_max(),'N','EN','qtisreplicator','qtisreplicator','Database Replicator');
 insert into SAM.User values(80,0,47::int2,16::int2,sam.get_slevel_max(),'N','EN','qtiswww','qtiswww','Public-avilable WWW server/corporative portal');
 insert into SAM.User values(65634,0,31::int2,31::int2,32::int2,'Y','EN','qtisguest','qtisguest','Guest user, only ''Public'' information allowed');
 insert into SAM.User values(65635,0,0::int2,1::int2,0::int2,'Y','EN','qtisnobody','qtisnobody','Nobody/No access');
 -- Sequnces inital configuration for local scope
 insert into SAM.RangeSeq values(x'100000'::bigint,x'10FFFF'::bigint,null,x'100000'::bigint,20::int2,x'100000'::bigint,'SAM.XU',0,NOW());
 insert into SAM.RangeSeq values(x'110000'::int8,x'11FFFF'::int8,null,x'100000'::int8,20::int2,x'100000'::int8,'SAM.XG',0,NOW());
 insert into SAM.RangeSeq values(x'120000'::int8,x'12FFFF'::int8,null,x'100000'::int8,20::int2,x'100000'::int8,'SAM.XS',0,NOW());
 -- TISC data objects
 insert into SAM.RangeSeq values(x'130000'::int8,x'13FFFF'::int8,null,x'100000'::int8,20::int2,x'100000'::int8,'TISC.A',0,NOW());
 insert into SAM.RangeSeq values(x'140000'::int8,x'14FFFF'::int8,null,x'100000'::int8,20::int2,x'100000'::int8,'TISC.C',0,NOW());
 insert into SAM.RangeSeq values(x'150000'::int8,x'15FFFF'::int8,null,x'100000'::int8,20::int2,x'100000'::int8,'TISC.D',0,NOW());
 insert into SAM.RangeSeq values(x'160000'::int8,x'16FFFF'::int8,null,x'100000'::int8,20::int2,x'100000'::int8,'FS.F',0,NOW());
 insert into SAM.RangeSeq values(x'170000'::int8,x'170FFF'::int8,null,x'100000'::int8,20::int2,x'100000'::int8,'TIS.Q',0,NOW());
 insert into SAM.RangeSeq values(x'171000'::int8,x'171FFF'::int8,null,x'100000'::int8,20::int2,x'100000'::int8,'QR.M',0,NOW());
 insert into SAM.RangeSeq values(x'172000'::int8,x'172FFF'::int8,null,x'100000'::int8,20::int2,x'100000'::int8,'QR.P',0,NOW());
 insert into SAM.RangeSeq values(x'173000'::int8,x'176FFF'::int8,null,x'100000'::int8,20::int2,x'100000'::int8,'QR.C',0,NOW());
 insert into SAM.RangeSeq values(x'177000'::int8,x'17AFFF'::int8,null,x'100000'::int8,20::int2,x'100000'::int8,'QR.Q',0,NOW());
 insert into SAM.RangeSeq values(x'17B000'::int8,x'17BFFF'::int8,null,x'100000'::int8,20::int2,x'100000'::int8,'MSG.C',0,NOW());

 --insert into SAM.RangeSeq values(sam.h2i('12:0000'),sam.h2i('12:FFFF'),null,sam.h2i('10:0000'),20::int2,sam.h2i('10:0000'),'SAM.XG',0,NOW());
 --insert into SAM.RangeSeq values(sam.h2int8('12:0000'),sam.h2int8('12:FFFF'),null,sam.h2int8('10:0000'),20::int2,sam.h2int8('10:0000'),'SAM.XG',0,NOW());
 -- finally
 RETURN  'Ok, local installation configured';
END $$;
--
-- inital configuration of this installation
--
CREATE OR REPLACE FUNCTION sam.inital_local_configuration_once() RETURNS text VOLATILE
  LANGUAGE plpgSQL SECURITY INVOKER as $$
DECLARE
 v_r SAM.Scope%ROWTYPE;
BEGIN
 RETURN  'NOT IMPLEMENMTED, write your configuration procedure SAM.inital_local_configuration_once()';
END $$;
