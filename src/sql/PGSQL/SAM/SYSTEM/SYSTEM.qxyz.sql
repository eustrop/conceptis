-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

--CREATE OR REPLACE FUNCTION sam.check_QOID(QOID bigint) RETURNS bigint STABLE AS
--    'select CASE WHEN QOID > 1048575 THEN QOID ELSE null END'
--    LANGUAGE SQL SECURITY INVOKER;

--
-- inital configuration of qxyz.ru installation
--
CREATE OR REPLACE FUNCTION sam.inital_local_configuration_once() RETURNS text VOLATILE
  LANGUAGE plpgSQL SECURITY INVOKER as $$
DECLARE
 v_r SAM.Scope%ROWTYPE;
BEGIN
 -- qxyz.ru ranges (configured globally)
 --insert into SAM.Range values(sam.h2i('0100:0000:0000'),40::int2,sam.h2i('01FF:FFFF:FFFF'),0,64::int2,0,'0100:0000:0000/40 - qxyz.ru range');
 --insert into SAM.Range values(sam.h2i('0100:0000:0000'),32::int2,sam.h2i('0100:FFFF:FFFF'),sam.h2i('0100:0000:0000'),40::int2,0,'0100:0000:0000/32 - qxyz.ru range for system');
 --insert into SAM.Range values(sam.h2i('0101:0000:0000'),32::int2,sam.h2i('0101:FFFF:FFFF'),sam.h2i('0100:0000:0000'),40::int2,0,'0100:0000:0000/32 - qxyz.ru range for clients');
 SELECT * from SAM.Scope XS into v_r WHERE XS.id = sam.h2i('0000:0100:0000:0000');
 IF FOUND THEN
  RETURN 'Fail! qxyz.ru scope (id=0000:0100:0000:0000) already configured';
 END IF;
 insert into SAM.Scope values(sam.h2i('0100:0000:0000'),sam.h2i('0100:0000:0000'),sam.get_slevel_min(),sam.get_slevel_max(),'qxyz.ru','Y','0',null,'qxyz.ru service scope');
 -- sid   | obj_type | maxcount | ocount | doaudit | dodebug
 insert into SAM.ScopePolicy values(sam.h2i('0100:0000:0000'),'TISC.A',null,null,'N','N');
 insert into SAM.ScopePolicy values(sam.h2i('0100:0000:0000'),'TISC.C',null,null,'N','N');
 insert into SAM.ScopePolicy values(sam.h2i('0100:0000:0000'),'TISC.D',null,null,'N','N');
 insert into SAM.ScopePolicy values(sam.h2i('0100:0000:0000'),'FS.F',null,null,'N','N');
 insert into SAM.ScopePolicy values(sam.h2i('0100:0000:0000'),'TIS.Q',null,null,'N','N');
 insert into SAM.ScopePolicy values(sam.h2i('0100:0000:0000'),'QR.M',null,null,'N','N');
 insert into SAM.ScopePolicy values(sam.h2i('0100:0000:0000'),'QR.P',null,null,'N','N');
 insert into SAM.ScopePolicy values(sam.h2i('0100:0000:0000'),'QR.C',null,null,'N','N');
 insert into SAM.ScopePolicy values(sam.h2i('0100:0000:0000'),'QR.Q',null,null,'N','N');
 insert into SAM.ScopePolicy values(sam.h2i('0100:0000:0000'),'MSG.C',null,null,'N','N');

 -- Sequnces inital configuration for qxyz.ru  scope
 -- start  |  rend   | lastid  | prange  | pbitl |   sid   | qtype  | sorder | ts
 insert into SAM.RangeSeq values(sam.h2i('0100:0000:0000'),sam.h2i('100:0000:FFFF'),null,sam.h2i('0100:0000:0000'),32::int2,sam.h2i('0100:0000:0000'),'SAM.XU',0,NOW());
 insert into SAM.RangeSeq values(sam.h2i('0100:0001:0004'),sam.h2i('100:0001:FFFF'),null,sam.h2i('0100:0000:0000'),32::int2,sam.h2i('0100:0000:0000'),'SAM.XG',0,NOW());
 insert into SAM.RangeSeq values(sam.h2i('0100:0002:0000'),sam.h2i('100:0002:FFFF'),null,sam.h2i('0100:0000:0000'),32::int2,sam.h2i('0100:0000:0000'),'SAM.XS',0,NOW());
 -- TISC data objects
 insert into SAM.RangeSeq values(sam.h2i('0100:0003:0000'),sam.h2i('100:0003:FFFF'),null,sam.h2i('0100:0000:0000'),32::int2,sam.h2i('0100:0000:0000'),'TISC.A',0,NOW());
 insert into SAM.RangeSeq values(sam.h2i('0100:0004:0000'),sam.h2i('100:0004:FFFF'),null,sam.h2i('0100:0000:0000'),32::int2,sam.h2i('0100:0000:0000'),'TISC.C',0,NOW());
 insert into SAM.RangeSeq values(sam.h2i('0100:0005:0000'),sam.h2i('100:0005:FFFF'),null,sam.h2i('0100:0000:0000'),32::int2,sam.h2i('0100:0000:0000'),'TISC.D',0,NOW());
 -- FS
 insert into SAM.RangeSeq values(sam.h2i('0100:0006:0000'),sam.h2i('100:0006:FFFF'),null,sam.h2i('0100:0000:0000'),32::int2,sam.h2i('0100:0000:0000'),'FS.F',0,NOW());
 -- SEQ
 insert into SAM.RangeSeq values(sam.h2i('0100:0007:0000'),sam.h2i('100:0007:FFFF'),null,sam.h2i('0100:0000:0000'),32::int2,sam.h2i('0100:0000:0000'),'TIS.Q',0,NOW());
 -- QR
 insert into SAM.RangeSeq values(sam.h2i('0100:0008:0000'),sam.h2i('100:0008:FFFF'),null,sam.h2i('0100:0000:0000'),32::int2,sam.h2i('0100:0000:0000'),'QR.M',0,NOW());
 insert into SAM.RangeSeq values(sam.h2i('0100:0009:0000'),sam.h2i('100:0009:FFFF'),null,sam.h2i('0100:0000:0000'),32::int2,sam.h2i('0100:0000:0000'),'QR.P',0,NOW());
 insert into SAM.RangeSeq values(sam.h2i('0100:000A:0000'),sam.h2i('100:000A:FFFF'),null,sam.h2i('0100:0000:0000'),32::int2,sam.h2i('0100:0000:0000'),'QR.C',0,NOW());
 insert into SAM.RangeSeq values(sam.h2i('0100:000B:0000'),sam.h2i('100:000B:FFFF'),null,sam.h2i('0100:0000:0000'),32::int2,sam.h2i('0100:0000:0000'),'QR.Q',0,NOW());
 -- MSG
 insert into SAM.RangeSeq values(sam.h2i('0100:000C:0000'),sam.h2i('100:000C:FFFF'),null,sam.h2i('0100:0000:0000'),32::int2,sam.h2i('0100:0000:0000'),'MSG.C',0,NOW());


 -- qxyz.ru Groups
 -- id    |   sid   |      name      |               descr
 insert into SAM.Group values(sam.h2i('0100:0001:0000')+1,sam.h2i('0100:0000:0000'),'users@qxyz.ru','пользователи участника qxyz.ru (чтение/запись)');
 insert into SAM.Group values(sam.h2i('0100:0001:0000')+3,sam.h2i('0100:0000:0000'),'qr@qxyz.ru','qr-пользователи участника qxyz.ru (чтение)');
 -- qxyz.ru ACLS
 --  gid   |   sid   | obj_type | writea | createa | deletea | reada | locka | historya
 insert into SAM.ACLScope values(sam.h2i('0100:0001:0000')+1,sam.h2i('0100:0000:0000'),'TIS.Q','Y','Y','Y','Y','Y','Y');
 insert into SAM.ACLScope values(sam.h2i('0100:0001:0000')+1,sam.h2i('0100:0000:0000'),'MSG.C','Y','Y','Y','Y','Y','Y');
 insert into SAM.ACLScope values(sam.h2i('0100:0001:0000')+1,sam.h2i('0100:0000:0000'),'QR.M','Y','Y','Y','Y','Y','Y');
 insert into SAM.ACLScope values(sam.h2i('0100:0001:0000')+1,sam.h2i('0100:0000:0000'),'QR.P','Y','Y','Y','Y','Y','Y');
 insert into SAM.ACLScope values(sam.h2i('0100:0001:0000')+1,sam.h2i('0100:0000:0000'),'QR.C','Y','Y','Y','Y','Y','Y');
 insert into SAM.ACLScope values(sam.h2i('0100:0001:0000')+1,sam.h2i('0100:0000:0000'),'QR.Q','Y','Y','Y','Y','Y','Y');
 insert into SAM.ACLScope values(sam.h2i('0100:0001:0000')+1,sam.h2i('0100:0000:0000'),'FS.F','Y','Y','Y','Y','Y','Y');
 --
 --insert into SAM.ACLScope values(sam.h2i('0100:0001:0000')+3,sam.h2i('0100:0000:0000'),'TIS.Q','N','N','N','Y','N','N');
 --insert into SAM.ACLScope values(sam.h2i('0100:0001:0000')+3,sam.h2i('0100:0000:0000'),'MSG.C','N','N','N','Y','N','N');
 insert into SAM.ACLScope values(sam.h2i('0100:0001:0000')+3,sam.h2i('0100:0000:0000'),'QR.M','N','N','N','Y','N','N');
 insert into SAM.ACLScope values(sam.h2i('0100:0001:0000')+3,sam.h2i('0100:0000:0000'),'QR.P','N','N','N','Y','N','N');
 --insert into SAM.ACLScope values(sam.h2i('0100:0001:0000')+3,sam.h2i('0100:0000:0000'),'QR.C','N','N','N','Y','N','N');
 insert into SAM.ACLScope values(sam.h2i('0100:0001:0000')+3,sam.h2i('0100:0000:0000'),'QR.Q','N','N','N','Y','N','N');
 insert into SAM.ACLScope values(sam.h2i('0100:0001:0000')+3,sam.h2i('0100:0000:0000'),'FS.F','N','N','N','Y','N','N');
 -- qxyz.ru UserGroup
 insert into SAM.UserGroup values(80,sam.h2i('0100:0001:0000')+3);
 -- qxyz.ru TIS.Q

 -- EXAMPLED scope (example of typical qxyz.ru member)
 insert into SAM.Range values(sam.h2i('0100:000D:0000'),16::int2,sam.h2i('0100:000D:FFFF'),sam.h2i('0100:0000:0000'),32::int2,0,'0100:000D:0000/32 - qxyz.ru EXAMPLED member');
 
 insert into SAM.Scope values(sam.h2i('0100:000D:0000'),sam.h2i('0100:000D:0000'),sam.get_slevel_min(),sam.get_slevel_max(),'EXAMPLED','Y','0',null,'EXAMPLED member scope');
 insert into SAM.ScopePolicy values(sam.h2i('0100:000D:0000'),'TIS.Q',null,null,'N','N');
 insert into SAM.ScopePolicy values(sam.h2i('0100:000D:0000'),'FS.F',null,null,'N','N');
 insert into SAM.ScopePolicy values(sam.h2i('0100:000D:0000'),'QR.M',null,null,'N','N');
 insert into SAM.ScopePolicy values(sam.h2i('0100:000D:0000'),'QR.P',null,null,'N','N');
 insert into SAM.ScopePolicy values(sam.h2i('0100:000D:0000'),'QR.C',null,null,'N','N');
 insert into SAM.ScopePolicy values(sam.h2i('0100:000D:0000'),'QR.Q',null,null,'N','N');
 insert into SAM.ScopePolicy values(sam.h2i('0100:000D:0000'),'MSG.C',null,null,'N','N');
 -- EXAMPLED scope RangeSeq
 insert into SAM.RangeSeq values(sam.h2i('0100:000D:0004'),sam.h2i('100:000D:000F'),null,sam.h2i('0100:000D:0000'),16::int2,sam.h2i('0100:000D:0000'),'SAM.XU',0,NOW());
 insert into SAM.RangeSeq values(sam.h2i('0100:000D:0014'),sam.h2i('100:000D:001F'),null,sam.h2i('0100:000D:0000'),16::int2,sam.h2i('0100:000D:0000'),'SAM.XG',0,NOW());
 insert into SAM.RangeSeq values(sam.h2i('0100:000D:0020'),sam.h2i('100:000D:002F'),null,sam.h2i('0100:000D:0000'),16::int2,sam.h2i('0100:000D:0000'),'SAM.XS',0,NOW());
 insert into SAM.RangeSeq values(sam.h2i('0100:000D:0030'),sam.h2i('100:000D:003F'),null,sam.h2i('0100:000D:0000'),16::int2,sam.h2i('0100:000D:0000'),'TIS.Q',0,NOW());
 insert into SAM.RangeSeq values(sam.h2i('0100:000D:0100'),sam.h2i('100:000D:01FF'),null,sam.h2i('0100:000D:0000'),16::int2,sam.h2i('0100:000D:0000'),'MSG.C',0,NOW());
 insert into SAM.RangeSeq values(sam.h2i('0100:000D:1000'),sam.h2i('100:000D:1FFF'),null,sam.h2i('0100:000D:0000'),16::int2,sam.h2i('0100:000D:0000'),'QR.M',0,NOW());
 insert into SAM.RangeSeq values(sam.h2i('0100:000D:2000'),sam.h2i('100:000D:2FFF'),null,sam.h2i('0100:000D:0000'),16::int2,sam.h2i('0100:000D:0000'),'QR.P',0,NOW());
 insert into SAM.RangeSeq values(sam.h2i('0100:000D:3000'),sam.h2i('100:000D:3FFF'),null,sam.h2i('0100:000D:0000'),16::int2,sam.h2i('0100:000D:0000'),'QR.C',0,NOW());
 insert into SAM.RangeSeq values(sam.h2i('0100:000D:9000'),sam.h2i('100:000D:9FFF'),null,sam.h2i('0100:000D:0000'),16::int2,sam.h2i('0100:000D:0000'),'QR.Q',0,NOW());
 insert into SAM.RangeSeq values(sam.h2i('0100:000D:D000'),sam.h2i('100:000D:DFFF'),null,sam.h2i('0100:000D:0000'),16::int2,sam.h2i('0100:000D:0000'),'FS.F',0,NOW());
 -- EXAMPLED Users
 insert into SAM.User values(sam.h2i('0100:000D:0000')+1,sam.h2i('0100:000D:0000'),63::int2,15::int2,63::int2,'N','RU','admin@EXAMPLED',null,'Администратор (EXAMPLED)');
 --insert into SAM.UserCapability values(sam.h2i('0100:000D:0000')+1,sam.h2i('0100:000D:0000'),'SAM_MANAGE');
 --insert into SAM.UserCapability values(sam.h2i('0100:000D:0000')+1,sam.h2i('0100:000D:0000'),'SAM_MANAGE_SLEVEL_MAX');
 insert into SAM.UserCapability values(sam.h2i('0100:000D:0000')+1,sam.h2i('0100:000D:0000'),'SLVL_DECREASE');
 insert into SAM.UserCapability values(sam.h2i('0100:000D:0000')+1,sam.h2i('0100:000D:0000'),'SLVL_MANAGE');
 insert into SAM.User values(sam.h2i('0100:000D:0000')+2,sam.h2i('0100:000D:0000'),63::int2,15::int2,63::int2,'N','RU','user@EXAMPLED',null,'Пользователь (EXAMPLED)');
 insert into SAM.User values(sam.h2i('0100:000D:0000')+3,sam.h2i('0100:000D:0000'),47::int2,15::int2,47::int2,'N','RU','qr@EXAMPLED',null,'QR-Пользователь (EXAMPLED)');
 -- EXAMPLED Groups
 -- id    |   sid   |      name      |               descr
 insert into SAM.Group values(sam.h2i('0100:000D:0010')+1,sam.h2i('0100:000D:0000'),'users@EXAMPLED','пользователи участника EXAMPLED (чтение/запись)');
 insert into SAM.Group values(sam.h2i('0100:000D:0010')+3,sam.h2i('0100:000D:0000'),'qr@EXAMPLED','qr-пользователи участника EXAMPLED (чтение)');
 -- EXAMPLED ACLS
 --  gid   |   sid   | obj_type | writea | createa | deletea | reada | locka | historya
 insert into SAM.ACLScope values(sam.h2i('0100:000D:0010')+1,sam.h2i('0100:000D:0000'),'TIS.Q','Y','Y','Y','Y','Y','Y');
 insert into SAM.ACLScope values(sam.h2i('0100:000D:0010')+1,sam.h2i('0100:000D:0000'),'MSG.C','Y','Y','Y','Y','Y','Y');
 insert into SAM.ACLScope values(sam.h2i('0100:000D:0010')+1,sam.h2i('0100:000D:0000'),'QR.M','Y','Y','Y','Y','Y','Y');
 insert into SAM.ACLScope values(sam.h2i('0100:000D:0010')+1,sam.h2i('0100:000D:0000'),'QR.P','Y','Y','Y','Y','Y','Y');
 insert into SAM.ACLScope values(sam.h2i('0100:000D:0010')+1,sam.h2i('0100:000D:0000'),'QR.C','Y','Y','Y','Y','Y','Y');
 insert into SAM.ACLScope values(sam.h2i('0100:000D:0010')+1,sam.h2i('0100:000D:0000'),'QR.Q','Y','Y','Y','Y','Y','Y');
 insert into SAM.ACLScope values(sam.h2i('0100:000D:0010')+1,sam.h2i('0100:000D:0000'),'FS.F','Y','Y','Y','Y','Y','Y');
 --
 --insert into SAM.ACLScope values(sam.h2i('0100:000D:0010')+3,sam.h2i('0100:000D:0000'),'TIS.Q','N','N','N','Y','N','N');
 --insert into SAM.ACLScope values(sam.h2i('0100:000D:0010')+3,sam.h2i('0100:000D:0000'),'MSG.C','N','N','N','Y','N','N');
 insert into SAM.ACLScope values(sam.h2i('0100:000D:0010')+3,sam.h2i('0100:000D:0000'),'QR.M','N','N','N','Y','N','N');
 insert into SAM.ACLScope values(sam.h2i('0100:000D:0010')+3,sam.h2i('0100:000D:0000'),'QR.P','N','N','N','Y','N','N');
 --insert into SAM.ACLScope values(sam.h2i('0100:000D:0010')+3,sam.h2i('0100:000D:0000'),'QR.C','N','N','N','Y','N','N');
 insert into SAM.ACLScope values(sam.h2i('0100:000D:0010')+3,sam.h2i('0100:000D:0000'),'QR.Q','N','N','N','Y','N','N');
 insert into SAM.ACLScope values(sam.h2i('0100:000D:0010')+3,sam.h2i('0100:000D:0000'),'FS.F','N','N','N','Y','N','N');
 -- EXAMPLED UserGroup
 insert into SAM.UserGroup values(sam.h2i('0100:000D:0000')+1,sam.h2i('0100:000D:0010')+1);
 insert into SAM.UserGroup values(sam.h2i('0100:000D:0000')+2,sam.h2i('0100:000D:0010')+1);
 insert into SAM.UserGroup values(sam.h2i('0100:000D:0000')+3,sam.h2i('0100:000D:0010')+3);
 insert into SAM.UserGroup values(80,sam.h2i('0100:000D:0010')+3);
 -- EXAMPLED TIS.Q
 
 RETURN  'Ok, qxyz.ru installation configured';
END $$;
