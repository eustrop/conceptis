#!/usr/bin/awk -f
# ConcepTIS project
# (c) Alex V Eustrop 2009
# (c) Alex V Eustrop & EustroSoft.org 2023
# see LICENSE.ConcepTIS at the project's root directory
#
# SAM subsystem codegeneration

BEGIN{
} #/BEGIN
END{
if(is_aborted)exit 1;
WDIR="work/"
#CODEGEN_STAMP=ENVIRON["CODEGEN_STAMP"];
#print CODEGEN_STAMP
make_create_table(WDIR "/tables/" SHORT_NAME ".sql");
make_table_drop(WDIR "/tables.drop/" SHORT_NAME ".sql");
make_create_view(WDIR "/views/V_" SHORT_NAME ".sql");
make_view_grant(WDIR "/views.grant/V_" SHORT_NAME ".sql");
make_proc_grant(WDIR "/functions.grant/" SHORT_NAME ".sql");
make_create_sequence(WDIR "/sequences/" SHORT_NAME "_seq.sql");
make_create_proc(WDIR "/functions/create_" SHORT_NAME ".sql");
make_create_proc_decl(WDIR "/declarations/create_" SHORT_NAME ".sql");
make_update_proc(WDIR "/functions/update_" SHORT_NAME ".sql");
make_update_proc_decl(WDIR "/declarations/update_" SHORT_NAME ".sql");
make_delete_proc(WDIR "/functions/delete_" SHORT_NAME ".sql");
make_delete_proc_decl(WDIR "/declarations/delete_" SHORT_NAME ".sql");
make_QC_proc(WDIR "/functions/QC_" SHORT_NAME ".sql");
make_QC_proc_decl(WDIR "/declarations/QC_" SHORT_NAME ".sql");
make_java_record_class(WDIR "/java/zDAO/" SHORT_NAME ".java");
make_java_semiproducts(WDIR "/java/semiproducts/" SHORT_NAME ".java");
} #/END
/^[ \t]*#/{next;} # comments
($1=="NAME"){NAME=$2;CODE=$3;
	n2=split(NAME,tmp_a,".");
	SUBSYS=tmp_a[1];SHORT_NAME=tmp_a[2];
	if(SHORT_NAME==""){SHORT_NAME = NAME; SUBSYS="SAM";}
	next;
	}
($1=="HEADER"){HEADER=$2;next;}
($1=="PKEY"){PKEY=$2;next;}
($1=="OBJECT"){OBJECT=$2;next;}
($1=="INDEX"){INDEX_COUNT++;INDEX[INDEX_COUNT]=$2;next;}
($1~"[0-9][0-9]"){ # FIELD
	FIELDS_COUNT++;
	f_no[FIELDS_COUNT]=$1;
	f_name[FIELDS_COUNT]=$2;
	f_type[FIELDS_COUNT]=$3;
	n=split($3,tmp_attrib,"[()]");
	if(n>1) f_size[FIELDS_COUNT]=tmp_attrib[2];
	 else f_size[FIELDS_COUNT]=0;
	f_attrib[FIELDS_COUNT]=$4;
	n=split($4,tmp_attrib,",");
	for(i=1;i<=n;i++){
	 n2=split(tmp_attrib[i],tmp_a,"=");
	 if(tmp_a[1]=="NN"){f_NN[FIELDS_COUNT]=1;}
	 else if(tmp_a[1]=="NUL"){f_NN[FIELDS_COUNT]=0;}
	 else if(tmp_a[1]=="SEQID"){f_SEQID[FIELDS_COUNT]=1;
		if(SEQID_FIELD=="")SEQID_FIELD=f_name[FIELDS_COUNT];
		if(ZID_FIELD=="")ZID_FIELD=f_name[FIELDS_COUNT];
		f_ZID[FIELDS_COUNT]=1; }
	 else if(tmp_a[1]=="ZID"){f_ZID[FIELDS_COUNT]=1;
		if(ZID_FIELD=="")ZID_FIELD=f_name[FIELDS_COUNT]; }
	 else if(tmp_a[1]=="UNIQ"){f_UNIQ[FIELDS_COUNT]=1;}
	 else if(tmp_a[1]=="NOEDIT"){f_NOEDIT[FIELDS_COUNT]=1;
		f_NOEDIT_DEFAULT[FIELDS_COUNT] = tmp_a[2];
		if(tmp_a[2] == "")
		f_NOEDIT_DEFAULT[FIELDS_COUNT] = "null";}
	 else if(tmp_a[1]=="SID"){f_SID[FIELDS_COUNT]=1;
		if(SID_FIELD=="")SID_FIELD=f_name[FIELDS_COUNT];}
	 else if(tmp_a[1]=="SLEVEL"){f_SLEVEL[FIELDS_COUNT]=1;
		if(SLEVEL_FIELD=="")SLEVEL_FIELD=f_name[FIELDS_COUNT];}
	 else if(tmp_a[1]=="SLEVEL_MIN"){}
	 else if(tmp_a[1]=="SLEVEL_MAX"){}
	 else if(tmp_a[1]=="UID"){f_UID[FIELDS_COUNT]=1;}
	 else if(tmp_a[1]=="GID"){f_GID[FIELDS_COUNT]=1;}
	 else if(tmp_a[1]=="DIC"){
	  f_DIC[FIELDS_COUNT]=tmp_a[2];
	  if(f_DIC[FIELDS_COUNT]=="")
	   f_DIC[FIELDS_COUNT]= "" CODE f_no[FIELDS_COUNT];
	 }
	 else{do_abort("Invalid attributes token: " tmp_a[1]);} 
	}
	next;
 } # FIELD
#//{print ;}
//{do_abort("Invalid start token: " $1); }
function do_abort(msg)
{
print "ERROR:" msg " at line: " NR " : " $0;
is_aborted=1;
exit 1;
}

# assistance

function make_no_header(f)
{
 printf("") >f;
}
function make_header(f)
{
 printf("") >f;
 printf("-- ConcepTIS project/QTIS project\n") >>f;
 printf("-- (c) Alex V Eustrop 2009\n") >>f;
 printf("-- (c) Alex V Eustrop & EustroSoft.org 2023\n") >>f;
 printf("-- see LICENSE.ConcepTIS at the project's root directory\n") >>f;
 printf("--\n") >>f;
 printf("-- $Id$\n") >>f;
 printf("--\n") >>f;
 printf("\n") >>f;
}

function make_table_drop(f)
{
 make_no_header(f);
 printf("DROP TABLE IF EXISTS %s CASCADE;\n",NAME) >>f;
}
# create table
function make_create_table(f)
{
 make_header(f);
 printf("--DROP TABLE IF EXISTS %s CASCADE;\n",NAME) >>f;
 printf("CREATE TABLE %s (\n",NAME) >>f;
 tmp_UNIQ="";
 for(i=1;i<=FIELDS_COUNT;i++)
 {
  NUL="NULL"; if(f_NN[i]) NUL="NOT " NUL;
  tmp_s=","; if(i==FIELDS_COUNT && PKEY=="") tmp_s="";
  printf("\t%s\t%s %s%s\n",f_name[i],f_type[i],NUL,tmp_s) >>f;
  if(f_UNIQ[i] && SUBSYS == "SAM"){
    if(tmp_UNIQ!="")
    tmp_UNIQ=tmp_UNIQ ",\n";tmp_UNIQ=tmp_UNIQ "\tUNIQUE(" f_name[i] ")";
   }
 }

 if(PKEY != ""){
  tmp_s=","; if(tmp_UNIQ=="") tmp_s="";
  printf("\tPRIMARY KEY (%s)%s\n",PKEY,tmp_s) >>f;
 }
if(tmp_UNIQ!="") print tmp_UNIQ >>f;
# printf("\tUNIQUE(name)\n") >>f;
 printf("\t);\n") >>f;
 for(i=1;i<=INDEX_COUNT;i++)
 {
  printf("CREATE INDEX %s_idx%s on %s(%s);\n",SHORT_NAME,i,NAME,INDEX[i]) >>f;
 }
} # /make_create_table(f)

# create view
function make_view_grant(f)
{
 make_no_header(f);
 printf("GRANT SELECT ON %s.V_%s to tis_users;\n",SUBSYS,SHORT_NAME) >>f;
}
function make_create_view(f)
{
 make_header(f);
 printf("DROP VIEW IF EXISTS %s.V_%s CASCADE;\n",SUBSYS,SHORT_NAME) >>f;
 printf("CREATE VIEW %s.V_%s AS SELECT * FROM %s\n" \
 	"\tWHERE NOT SAM.get_user() IS NULL;\n",SUBSYS,SHORT_NAME,NAME) >>f;
 printf("--GRANT SELECT ON %s.V_%s to tis_users;\n",SUBSYS,SHORT_NAME) >>f;
} # /make_create_view(f)

# create sequence <<NAME>>_seq

function make_create_sequence(f)
{
 make_header(f);
 printf("DROP SEQUENCE IF EXISTS %s_seq;\n",NAME) >>f;
 printf("CREATE SEQUENCE %s_seq;\n",NAME) >>f;
}

# access to functions
function make_proc_grant(f)
{
 make_no_header(f);
 printf("GRANT SELECT ON %s.V_%s to tis_users;\n",SUBSYS,SHORT_NAME) >>f;
 # create
 printf("REVOKE ALL ON ") >>f;
  create_proc_definition(f);
 printf(" FROM PUBLIC;\n") >>f;
 printf("GRANT EXECUTE ON ") >>f;
  create_proc_definition(f);
 printf(" TO tis_users;\n") >>f;
 # update
 printf("GRANT EXECUTE ON ") >>f;
  update_proc_definition(f);
 printf(" TO tis_users;\n") >>f;
 # delete
 printf("REVOKE EXECUTE ON ") >>f;
  delete_proc_definition(f);
 printf(" FROM PUBLIC;\n") >>f;
 printf("GRANT EXECUTE ON ") >>f;
  delete_proc_definition(f);
 printf(" TO tis_users;\n") >>f;
}

# create_<<NAME>>

# update_<<NAME>>


# QC_<<NAME>>


# assistance

function make_copy_v2r(shift,target_action,f){
 if(target_action != "C" && target_action != "U")
  do_abort("Invalid target_action for make_copy_v2r():" target_action);
 for(i=1;i<=FIELDS_COUNT;i++)
 {
  if(f_SEQID[i] && target_action == "C") # create
   printf(" v_r.%s := null; -- new record;\n",f_name[i]) >>f;
  else {
   if(f_NOEDIT[i]){
    printf(" -- v_r.%s := v_%s; -- std editing unallowed\n",f_name[i],f_name[i]) >>f;
    if( target_action == "C" ) printf(" v_r.%s := %s; -- std editing unallowed\n",
       f_name[i],f_NOEDIT_DEFAULT[i]) >>f; }
   else
    printf(" v_r.%s := v_%s;\n",f_name[i],f_name[i]) >>f;
  } #/else if(f_SEQID[i]
 }
} #/make_copy_v2r

function make_enum_fields(prefix,target_action,f){
 if(target_action != "C" && target_action != "U")
  do_abort("Invalid target_action for make_copy_v2r():" target_action);
 tmp_s="";
 for(i=1;i<=FIELDS_COUNT;i++)
 {
  if(!((f_ZID[i] || f_NOEDIT[i]) && target_action == "U")) # not update
  {
  if(tmp_s != "") tmp_s=tmp_s ",";
  if(!(i-int(i/6)*6)) tmp_s=tmp_s "\n\t";
  tmp_s = tmp_s prefix f_name[i];
  }
 }
   printf("%s",tmp_s) >>f;
} #/make_enum_fields

function create_proc_definition(f){
 printf("FUNCTION %s.create_%s(\n",SUBSYS,SHORT_NAME) >>f;
 for(i=1;i<=FIELDS_COUNT;i++)
 {
  tmp_s=","; if(i==FIELDS_COUNT) tmp_s="";
  if(!f_NOEDIT[i])
  printf("\tv_%s\t%s%s\n",f_name[i],f_type[i],tmp_s) >>f;
 }
 printf(")") >>f;
} #/create_proc_definition

function update_proc_definition(f){
 printf("FUNCTION %s.update_%s(\n",SUBSYS,SHORT_NAME) >>f;
 for(i=1;i<=FIELDS_COUNT;i++)
 {
  tmp_s=","; if(i==FIELDS_COUNT) tmp_s="";
  if(!f_NOEDIT[i])
  printf("\tv_%s\t%s%s\n",f_name[i],f_type[i],tmp_s) >>f;
 }
 printf(")") >>f;
} #/update_proc_definition

function delete_proc_definition(f){
 printf("FUNCTION %s.delete_%s(",SUBSYS,SHORT_NAME) >>f;
 tmp_s="";
 for(i=1;i<=FIELDS_COUNT;i++)
 {
  if(f_ZID[i]){
   if(tmp_s!="") tmp_s = tmp_s ",\n\t";
   tmp_s = tmp_s "v_" f_name[i] " " f_type[i];
  }
 }
  printf("%s)\n",tmp_s) >>f;
} #/delete_proc_definition

function make_return_notimplemented(f){
 printf("DECLARE\n") >>f;
 printf(" v_es SAM.execstatus;\n") >>f;
 printf("BEGIN\n") >>f;
 printf(" v_es.errnum :=1; v_es.errcode :='E_NOTIMPLEMENTED';\n") >>f;
 printf(" v_es.errdesc :=v_es.errcode; RETURN v_es;\n") >>f;
 printf("END $$;\n") >>f;
}

# create_<<NAME>>

function make_create_proc_decl(f){
 make_header(f);
 printf("CREATE OR REPLACE ") >>f;
  create_proc_definition(f);
 printf(" RETURNS SAM.execstatus VOLATILE\n") >>f;
 printf("  LANGUAGE plpgSQL SECURITY DEFINER as $$\n") >>f;
 make_return_notimplemented(f);
} #/make_create_proc_decl

function make_create_proc(f){
 make_header(f);
 printf("BEGIN TRANSACTION;\n") >>f;
 printf("CREATE OR REPLACE ") >>f;
  create_proc_definition(f);
 printf(" RETURNS SAM.execstatus VOLATILE\n") >>f;
 printf("  LANGUAGE plpgSQL SECURITY DEFINER as $$\n") >>f;
 printf("DECLARE\n") >>f;
 printf(" v_ps SAM.procstate;\n") >>f;
 printf(" v_as SAM.auditstate;\n") >>f;
 printf(" v_es SAM.execstatus;\n") >>f;
 printf(" v_r %s%%ROWTYPE;\n",NAME) >>f;
 printf(" v_user bigint;\n") >>f;
 printf("BEGIN\n") >>f;
 printf(" -- 0) enter into procedure\n") >>f;
 printf(" v_as.subsys := '%s';\n",SUBSYS) >>f;
 printf(" v_as.eaction :='C';\n") >>f;
 printf(" v_as.obj_type :='%s';\n",CODE) >>f;
 #
 if(SID_FIELD == ""){
  printf(" v_as.sid := null;\n") >>f; }
  else { printf(" v_as.sid := v_%s;\n",SID_FIELD) >>f };
 printf(" -- v_as.ZID := v_id;\n") >>f;
 printf(" v_as.ZTYPE := 'X';\n") >>f; # common object code for SAM
 if(SLEVEL_FIELD == ""){
  printf(" v_as.ZLVL := null;\n") >>f; }
  else { printf(" v_as.ZLVL := v_%s;\n",SLEVEL_FIELD) >>f };
 printf(" v_as.proc := 'create_%s';\n",SHORT_NAME) >>f;
 printf(" v_as := SAM.do_auditlog_enter(v_as);\n") >>f;
 printf(" -- 0.1) copy parameters to v_r\n") >>f;
 make_copy_v2r("","C",f);
 printf("<<try>>\n") >>f;
 printf("BEGIN\n") >>f;
 printf(" -- 1) check isolation level\n") >>f;
 printf(" IF NOT SAM.check_isolation() THEN\n") >>f;
 printf("    v_es := SAM.make_es_transisolation(null,null); EXIT try;\n") >>f;
 printf(" END IF;\n") >>f;
 printf(" -- 1.1) lock required tables\n") >>f;
 printf(" LOCK TABLE %s IN EXCLUSIVE MODE;\n",NAME) >>f;
 printf(" -- 2) identify user\n") >>f;
 printf(" v_user := sam.get_user();\n") >>f;
 printf(" IF v_user IS NULL THEN\n") >>f;
 printf("    v_es := sam.make_execstatus(null,null,'E_NOUSER',session_user); EXIT try;\n") >>f;
 printf(" END IF;\n") >>f;
 printf(" -- 3) check capability\n") >>f;
 printf(" IF NOT SAM.check_capability('SAM_MANAGE') THEN\n") >>f;
 printf("   v_es := SAM.make_execstatus(null,null,'E_NOCAPABILITY','SAM_MANAGE','MAIN');\n") >>f;
 printf("   EXIT try; END IF;\n") >>f;
 printf("   v_as := SAM.do_auditlog_capuse_sole(v_as,'1','SAM_MANAGE',0);\n") >>f;
 printf(" -- 4) check data\n") >>f;
 printf(" v_ps := %s.QC_%s(v_as,v_r); v_as := v_ps.a;\n",SUBSYS,SHORT_NAME) >>f;
 printf("   IF NOT v_ps.success THEN v_es := v_ps.e; EXIT try; END IF;\n") >>f;
 printf(" -- 5) add record\n") >>f;
 if(SEQID_FIELD != "")
  printf(" v_r.%s := nextval('%s_seq');\n",SEQID_FIELD,NAME) >>f;
 printf(" INSERT INTO %s values(v_r.*);\n",NAME) >>f;
 if(SEQID_FIELD != "")
  printf(" v_as.ZID := v_r.%s;\n", SEQID_FIELD) >>f;
 if(SID_FIELD=="") tmp_sid = "null"; else tmp_sid="v_r." SID_FIELD;
 if(ZID_FIELD=="") tmp_zid = "null"; else tmp_zid="v_r." ZID_FIELD;
 printf(" v_as := SAM.do_auditlog_da_sole(v_as,'1',null,null,%s,%s);\n", tmp_sid,tmp_zid) >>f;
 printf(" -- 6) exit\n") >>f;
 printf(" v_es := sam.make_execstatus(%s,null,'I_SUCCESS');\n",tmp_zid) >>f;
 printf("EXCEPTION\n") >>f;
 printf(" WHEN OTHERS THEN\n") >>f;
 printf("\tv_es:=sam.make_execstatus(%s,null,'E_SQL',SQLSTATE,SQLERRM);\n",tmp_zid) >>f;
 printf("END;\n") >>f;
 printf("-- FINALLY:\n") >>f;
 printf(" PERFORM SAM.do_auditlog_exit(v_as,v_es);\n") >>f;
 printf(" RETURN v_es;\n") >>f;
 printf("END $$;\n") >>f;
 printf("-- GRANT/REVOKE moved to functions.grant/ directory ") >>f;
# printf("REVOKE ALL ON ") >>f;
#  create_proc_definition(f);
# printf(" FROM PUBLIC;\n") >>f;
# printf("GRANT EXECUTE ON ") >>f;
#  create_proc_definition(f);
# printf(" TO tis_users;\n") >>f;
 printf("COMMIT TRANSACTION;\n") >>f;
} # /create_proc

# update_<<NAME>>

function make_update_proc_decl(f){
 make_header(f);
 printf("CREATE OR REPLACE ") >>f;
  update_proc_definition(f);
 printf(" RETURNS SAM.execstatus VOLATILE\n") >>f;
 printf("  LANGUAGE plpgSQL SECURITY DEFINER as $$\n") >>f;
 make_return_notimplemented(f);
} #/make_update_proc_decl

function make_update_proc(f){
 make_header(f);
 printf("CREATE OR REPLACE ") >>f;
  update_proc_definition(f);
 printf(" RETURNS SAM.execstatus VOLATILE\n") >>f;
 printf("  LANGUAGE plpgSQL SECURITY DEFINER as $$\n") >>f;
 printf("DECLARE\n") >>f;
 printf(" v_ps SAM.procstate;\n") >>f;
 printf(" v_as SAM.auditstate;\n") >>f;
 printf(" v_es SAM.execstatus;\n") >>f;
 printf(" v_r %s%%ROWTYPE;\n",NAME) >>f;
 printf(" v_user bigint;\n") >>f;
 printf(" i bigint; -- counter\n") >>f;
 printf("BEGIN\n") >>f;
 #printf(" -- v_es:=sam.make_execstatus('E_NOTIMPLEMENTED');\n") >>f;
 printf(" -- 0) enter into procedure\n") >>f;
 printf(" v_as.subsys = '%s';\n",SUBSYS) >>f;
 printf(" v_as.eaction :='U';\n") >>f;
 printf(" v_as.obj_type :='%s';\n",CODE) >>f;
 if(SID_FIELD == ""){
  printf(" v_as.sid := null;\n") >>f; }
  else { printf(" v_as.sid := v_%s;\n",SID_FIELD) >>f };
 if(ZID_FIELD == "")
  printf(" v_as.ZID := null;\n") >>f;
  else printf(" v_as.ZID := v_%s;\n",ZID_FIELD) >>f;
 printf(" v_as.ZTYPE := 'X';\n") >>f; # common object code for SAM
 if(SLEVEL_FIELD != "" && ZID_FIELD != "")
  { printf(" SELECT %s INTO v_as.ZLVL FROM %s WHERE %s = v_%s;\n",
    SLEVEL_FIELD, NAME, ZID_FIELD, ZID_FIELD) >>f; }
  else { printf(" v_as.ZLVL := null; -- unknown at this moment\n") >>f; }
 printf(" v_as.proc := 'update_%s';\n",SHORT_NAME) >>f;
 printf(" v_as := SAM.do_auditlog_enter(v_as);\n") >>f;
 printf(" -- 0.1) copy parameters to v_r\n") >>f;
 make_copy_v2r("","U",f);
 printf("<<try>>\n") >>f;
 printf("BEGIN\n") >>f;
 printf(" -- 1) check isolation level\n") >>f;
 printf(" IF NOT SAM.check_isolation() THEN\n") >>f;
 printf("    v_es := SAM.make_es_transisolation(null,null); EXIT try;\n") >>f;
 printf(" END IF;\n") >>f;
 printf(" -- 1.1) lock required tables\n") >>f;
 printf(" LOCK TABLE %s IN EXCLUSIVE MODE;\n",NAME) >>f;
 printf(" -- 2) identify user\n") >>f;
 printf(" v_user := sam.get_user();\n") >>f;
 printf(" IF v_user IS NULL THEN\n") >>f;
 if( ZID_FIELD == "") 
  printf("    v_es := sam.make_execstatus(null,null,'E_NOUSER',session_user); EXIT try;\n") >>f;
 else
  printf("    v_es := sam.make_execstatus(v_r.%s,null,'E_NOUSER',session_user); EXIT try;\n",ZID_FIELD) >>f;
 printf(" END IF;\n") >>f;
 printf(" -- 3) check capability\n") >>f;
 printf(" IF NOT SAM.check_capability('SAM_MANAGE') THEN\n") >>f;
 if( ZID_FIELD == "") 
  printf("   v_es :=SAM.make_execstatus(null,null,'E_NOCAPABILITY','SAM_MANAGE','MAIN');\n") >>f;
 else
  printf("   v_es :=SAM.make_execstatus(v_r.%s,null,'E_NOCAPABILITY','SAM_MANAGE','MAIN');\n",ZID_FIELD) >>f;
 printf("   EXIT try; END IF;\n") >>f;
 printf("   v_as := SAM.do_auditlog_capuse_sole(v_as,'1','SAM_MANAGE',0);\n") >>f;
 if(SID_FIELD=="") tmp_sid = "null"; else tmp_sid="v_r." SID_FIELD;
 if(ZID_FIELD=="") tmp_zid = "null"; else tmp_zid="v_r." ZID_FIELD;
 printf(" -- 4) check data\n") >>f;
 printf(" -- 4.1) check for record's existence\n") >>f;
 printf(" SELECT count(*) INTO i FROM %s ",NAME) >>f;
 if(ZID_FIELD!="") printf(" WHERE %s = v_r.%s;\n",ZID_FIELD,ZID_FIELD) >>f;
  else printf(" WHERE FALSE;\n") >>f;
 printf(" IF i = 0 THEN\n") >>f;
 printf("   v_es := sam.make_execstatus(%s,null,'E_NOBJECT','%s',\n",tmp_zid,NAME) >>f;
 printf("   '%s',CAST(%s as text)); EXIT try;\n",ZID_FIELD,tmp_zid) >>f;
 printf(" END IF;\n") >>f;
 printf(" IF i <> 1 THEN\n") >>f;
 printf("   v_es := sam.make_execstatus(%s,null,'E_NONSOLEOBJECT','%s',\n",tmp_zid,NAME) >>f;
 printf("   '%s',CAST(%s as text)); EXIT try;\n",ZID_FIELD,tmp_zid) >>f;
 printf(" END IF;\n") >>f;
 printf(" -- 4.2) STD Quality Control\n") >>f;
 printf(" v_ps := SAM.QC_%s(v_as,v_r); v_as := v_ps.a;\n",SHORT_NAME) >>f;
 printf("   IF NOT v_ps.success THEN v_es := v_ps.e; EXIT try; END IF;\n") >>f;
 printf(" -- 5) update record\n") >>f;
 printf(" UPDATE %s SET (",NAME) >>f;
  make_enum_fields("","U",f);
  printf(")\n") >>f;
 printf(" = (") >>f;
  make_enum_fields("v_r.","U",f);
  printf(")\n") >>f;
 if(ZID_FIELD!="") printf(" WHERE %s = v_r.%s;\n",ZID_FIELD,ZID_FIELD) >>f;
  else printf(" WHERE FALSE;\n") >>f;
 printf(" IF NOT FOUND THEN\n") >>f;
 printf("   v_es := sam.make_execstatus(%s,null,'E_NOBJECT','%s',\n",tmp_zid,NAME) >>f;
 printf("   '%s',CAST(%s as text)); EXIT try;\n",ZID_FIELD,tmp_zid) >>f;
 printf(" END IF;\n") >>f;
 printf(" v_as := SAM.do_auditlog_da_sole(v_as,'1',null,null,null,null);\n") >>f;
 printf(" -- 6) exit\n") >>f;
 printf(" v_es := sam.make_execstatus(%s,null,'I_SUCCESS');\n",tmp_zid) >>f;
 printf("EXCEPTION\n") >>f;
 printf(" WHEN OTHERS THEN\n") >>f;
 printf("\tv_es:=sam.make_execstatus(%s,null,'E_SQL',SQLSTATE,SQLERRM);\n",tmp_zid) >>f;
 printf("END;\n") >>f;
 printf("-- FINALLY:\n") >>f;
 printf(" PERFORM SAM.do_auditlog_exit(v_as,v_es);\n") >>f;
 printf(" RETURN v_es;\n") >>f;
 printf("END $$;\n") >>f;
 printf("-- GRANT/REVOKE moved to functions.grant/ directory ") >>f;
# printf("GRANT EXECUTE ON ") >>f;
#  update_proc_definition(f);
# printf(" TO tis_users;\n") >>f;
}

#
# delete_<<NAME>>

function make_delete_proc_decl(f){
 make_header(f);
 printf("CREATE OR REPLACE ") >>f;
  delete_proc_definition(f);
 printf(" RETURNS SAM.execstatus VOLATILE\n") >>f;
 printf("  LANGUAGE plpgSQL SECURITY DEFINER as $$\n") >>f;
 make_return_notimplemented(f);
} #/make_delete_proc_decl

function make_delete_proc(f){
 make_header(f);
 printf("BEGIN TRANSACTION;\n") >>f;
 printf("CREATE OR REPLACE ") >>f;
  delete_proc_definition(f);
 printf(" RETURNS SAM.execstatus VOLATILE\n") >>f;
 printf("  LANGUAGE plpgSQL SECURITY DEFINER as $$\n") >>f;
 printf("DECLARE\n") >>f;
 printf(" v_as SAM.auditstate;\n") >>f;
 printf(" v_es SAM.execstatus;\n") >>f;
 printf(" v_user bigint;\n") >>f;
 printf(" i bigint; --couner\n") >>f;
 printf("BEGIN\n") >>f;
 printf(" -- v_es:=sam.make_execstatus('E_NOTIMPLEMENTED');\n") >>f;
 printf(" -- 0) enter into procedure\n") >>f;
 printf(" v_as.subsys = '%s';\n",SUBSYS) >>f;
 printf(" v_as.eaction :='D';\n") >>f;
 printf(" v_as.obj_type :='%s';\n",CODE) >>f;
 tmp_s=""; tmp_s2="";
 if(SID_FIELD == ""){
  printf(" v_as.sid := null; -- not applicable for %s\n",NAME) >>f; }
  else { tmp_s = SID_FIELD; tmp_s2 = "v_as.sid";}
 if(SLEVEL_FIELD == ""){
  printf(" v_as.ZLVL := null; -- not applicable for %s\n",NAME) >>f; }
  else {
    if(tmp_s != "") tmp_s = tmp_s ",";
    if(tmp_s2 != "") tmp_s2 = tmp_s2 ",";
    tmp_s = tmp_s SLEVEL_FIELD; tmp_s2 = tmp_s2 "v_as.ZLVL";
    }
 if(ZID_FIELD == "")
  printf(" v_as.ZID := null; -- not applicable for %s\n",NAME) >>f;
  else printf(" v_as.ZID := v_%s;\n",ZID_FIELD) >>f;
 printf(" v_as.ZTYPE := 'X';\n") >>f; # common object code for SAM
 if(tmp_s != "" && ZID_FIELD != "")
  { printf(" SELECT %s INTO %s  FROM %s WHERE %s = v_%s;\n",
    tmp_s, tmp_s2, NAME, ZID_FIELD, ZID_FIELD) >>f; }
  else { printf("  -- v_as.sid/ZLVL not applicable or unknown \n") >>f; }
 printf(" v_as.proc := 'delete_%s';\n",SHORT_NAME) >>f;
 printf(" v_as := SAM.do_auditlog_enter(v_as);\n") >>f;
 printf("<<try>>\n") >>f;
 printf("BEGIN\n") >>f;
 printf(" -- 1) check isolation level\n") >>f;
 printf(" IF NOT SAM.check_isolation() THEN\n") >>f;
 printf("    v_es := SAM.make_es_transisolation(null,null); EXIT try;\n") >>f;
 printf(" END IF;\n") >>f;
 printf(" -- 1.1) lock requared tables\n") >>f;
 printf(" LOCK TABLE %s IN EXCLUSIVE MODE;\n",NAME) >>f;
 printf(" -- 2) identify user\n") >>f;
 printf(" v_user := sam.get_user();\n") >>f;
 printf(" IF v_user IS NULL THEN\n") >>f;
 printf("    v_es := sam.make_execstatus(null,null,'E_NOUSER',session_user); EXIT try;\n") >>f;
 printf(" END IF;\n") >>f;
 printf(" -- 3) check capability\n") >>f;
 printf(" IF NOT SAM.check_capability('SAM_MANAGE') THEN\n") >>f;
 printf("   v_es := SAM.make_execstatus(null,null,'E_NOCAPABILITY','SAM_MANAGE','MAIN');\n") >>f;
 printf("   EXIT try; END IF;\n") >>f;
 printf("   v_as := SAM.do_auditlog_capuse_sole(v_as,'1','SAM_MANAGE',0);\n") >>f;
 printf(" -- 4) check data\n") >>f;
 if(SID_FIELD=="") tmp_sid = "null"; else tmp_sid="v_as.sid";
 if(ZID_FIELD=="") tmp_zid = "null"; else tmp_zid="v_" ZID_FIELD;
 printf(" -- 4.1) check for record's existence\n") >>f;
 printf(" SELECT count(*) INTO i FROM %s ",NAME) >>f;
 if(ZID_FIELD!="") printf(" WHERE %s = v_%s;\n",ZID_FIELD,ZID_FIELD) >>f;
  else printf(" WHERE FALSE;\n") >>f;
 printf(" IF i = 0 THEN\n") >>f;
 printf("   v_es := sam.make_execstatus(%s,null,'E_NOBJECT','%s',\n",tmp_zid,NAME) >>f;
 printf("   '%s',CAST(%s as text)); EXIT try;\n",ZID_FIELD,tmp_zid) >>f;
 printf(" END IF;\n") >>f;
 printf(" IF i <> 1 THEN\n") >>f;
 printf("   v_es := sam.make_execstatus(%s,null,'E_NONSOLEOBJECT','%s',\n",tmp_zid,NAME) >>f;
 printf("   '%s',CAST(%s as text)); EXIT try;\n",ZID_FIELD,tmp_zid) >>f;
 printf(" END IF;\n") >>f;
 printf(" -- 5) delete record\n") >>f;
 printf(" DELETE FROM %s WHERE\n",NAME) >>f;
  if(ZID_FIELD!="") printf("   %s = v_%s;\n",ZID_FIELD,ZID_FIELD) >>f;
  else printf("   FALSE;\n") >>f;
 printf(" v_as := SAM.do_auditlog_da_sole(v_as,'1',null,null,%s,%s);\n",tmp_sid,tmp_zid) >>f;
 printf(" -- 6) exit\n") >>f;
 printf(" v_es := sam.make_execstatus(%s,null,'I_SUCCESS');\n",tmp_zid) >>f;
 printf("EXCEPTION\n") >>f;
 printf(" WHEN OTHERS THEN\n") >>f;
 printf("\tv_es:=sam.make_execstatus(%s,null,'E_SQL',SQLSTATE,SQLERRM);\n",tmp_zid) >>f;
 printf("END;\n") >>f;
 printf("-- FINALLY:\n") >>f;
 printf(" PERFORM SAM.do_auditlog_exit(v_as,v_es);\n") >>f;
 printf(" RETURN v_es;\n") >>f;
 printf("END $$;\n") >>f;
 printf("-- GRANT/REVOKE moved to functions.grant/ directory ") >>f;
# printf("REVOKE EXECUTE ON ") >>f;
#  delete_proc_definition(f);
# printf(" FROM PUBLIC;\n") >>f;
# printf("GRANT EXECUTE ON ") >>f;
#  delete_proc_definition(f);
# printf(" TO tis_users;\n") >>f;
 printf("COMMIT TRANSACTION;\n") >>f;
}

#
# make_QC

function make_QC_proc_decl(f){
 make_header(f);
 printf("CREATE OR REPLACE FUNCTION %s.QC_%s(\n",SUBSYS,SHORT_NAME) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_r\t%s\n",NAME) >>f;
 printf(") RETURNS SAM.procstate VOLATILE\n") >>f;
 printf("  LANGUAGE plpgSQL SECURITY DEFINER as $$\n") >>f;
 printf("DECLARE\n") >>f;
 printf(" v_ps SAM.procstate;\n") >>f;
 printf(" v_es SAM.execstatus;\n") >>f;
 printf("BEGIN\n") >>f;
 printf(" v_es.errnum :=1; v_es.errcode :='E_NOTIMPLEMENTED';\n") >>f;
 printf(" v_es.errdesc := v_es.errcode || ',QC_%s()'; v_ps.e := v_es;\n",SHORT_NAME) >>f;
 printf(" v_ps.a := v_as; v_ps.success := FALSE; RETURN v_ps;\n") >>f;
 printf("END $$;\n") >>f;
} #/make_QC_proc_decl


function make_QC_proc(f){
 make_header(f);
 printf("BEGIN TRANSACTION;\n") >>f;
 printf("CREATE OR REPLACE FUNCTION %s.QC_%s(\n",SUBSYS,SHORT_NAME) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_r\t%s\n",NAME) >>f;
 printf(") RETURNS SAM.procstate VOLATILE\n") >>f;
 printf("  LANGUAGE plpgSQL SECURITY DEFINER as $$\n") >>f;
 printf("DECLARE\n") >>f;
 printf(" v_ps SAM.procstate;\n") >>f;
 printf(" v_zid\tbigint;\n") >>f;
 printf(" i\tinteger;\n") >>f;
 printf("BEGIN\n") >>f;
 printf(" v_ps.success := FALSE;\n") >>f;
 printf("<<try>>\n") >>f;
 printf("BEGIN\n") >>f;
 if(ZID_FIELD != "") printf(" v_zid := v_r.%s;\n",ZID_FIELD) >>f;
 else printf(" v_zid := null; -- not applicable for %s\n",NAME) >>f;
 printf(" -- 1) check NOT NULLs\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 if(!f_NN[i] || f_ZID[i]) continue;
 printf(" IF v_r.%s IS NULL THEN v_ps.e := SAM.make_es_notnull(v_zid,null,\n",f_name[i]) >>f;
 printf("\t'%s','%s'); EXIT try; END IF;\n",NAME,f_name[i]) >>f;
 }
 printf(" -- 2) check dictionary fields\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 if(f_DIC[i] == "") continue;
 tmp_s = ""; if(!f_NN[i]) tmp_s = "_nula";
 printf(" IF NOT dic.check_code%s('%s',v_r.%s) THEN\n",tmp_s,f_DIC[i],f_name[i]) >>f;
 printf("   v_ps.e := SAM.make_es_invalidcode(v_zid,null,v_r.%s,\n",f_name[i]) >>f;
 printf("   '%s.%s','%s'); EXIT try; END IF;\n",NAME,f_name[i],f_DIC[i]) >>f;
 }
 for(i=1;i<=FIELDS_COUNT;i++) {
 if(!f_SLEVEL[i]) continue;
 printf(" IF NOT dic.check_code('SLEVEL',CAST(v_r.%s as text)) THEN\n",f_name[i]) >>f;
 printf("   v_ps.e := SAM.make_es_invalidcode(v_zid,null,CAST(v_r.%s as text),\n",f_name[i]) >>f;
 printf("   '%s.%s','SLEVEL'); EXIT try; END IF;\n",NAME,f_name[i]) >>f;
 }
 printf(" -- 3) check uniqueness\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 if(!f_UNIQ[i]) continue;
 printf(" SELECT COUNT(*) INTO i FROM %s WHERE %s = v_r.%s AND\n",NAME,f_name[i],f_name[i]) >>f;
 printf("\t(%s <> v_r.%s OR v_r.%s IS NULL);\n",ZID_FIELD,ZID_FIELD,ZID_FIELD) >>f;
 printf(" IF i <> 0 THEN\n") >>f;
 printf("  v_ps.e :=SAM.make_execstatus(v_zid,null,'E_DUPOBJECT','%s',\n",NAME) >>f;
 printf("  '%s',v_r.%s);\n",f_name[i],f_name[i]) >>f;
 printf("  EXIT try; END IF;\n") >>f;
 }
 printf(" -- 4) check references\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 if(!f_SID[i]) continue;
 printf("  IF v_r.%s <> 0 THEN\n",f_name[i]) >>f;
 printf("   SELECT COUNT(*) INTO i FROM SAM.Scope WHERE id = v_r.%s;\n",f_name[i]) >>f;
 printf("   IF i = 0 THEN v_ps.e := SAM.make_execstatus(v_zid,null,'E_NOSCOPE',\n") >>f;
 printf("\tCAST(v_r.%s AS text)); EXIT try; END IF;\n",f_name[i]) >>f;
 printf("  END IF;\n") >>f;
 }
 printf(" -- 99) all tests passed\n") >>f;
 printf(" v_ps.success := TRUE;\n") >>f;
 printf("END;\n") >>f;
 printf("-- FINALLY:\n") >>f;
 printf(" v_ps.a := v_as;\n") >>f;
 printf(" RETURN v_ps;\n") >>f;
 printf("END $$;\n") >>f;
 printf("REVOKE ALL ON FUNCTION %s.QC_%s(\n",SUBSYS,SHORT_NAME) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_r\t%s\n",NAME) >>f;
 printf("\t) FROM PUBLIC;\n") >>f;
 #printf("GRANT EXECUTE ON FUNCTION %s.QC_%s(\n",SUBSYS,SHORT_NAME) >>f;
 #printf("\tv_as\tSAM.auditstate,\n") >>f;
 #printf("\tv_r\t%s\n",NAME) >>f;
 #printf("\t) TO tis_users;\n") >>f;
 printf("COMMIT TRANSACTION;\n") >>f;
} #/make_QC_proc

#
# Java classes generation
#

# true if datatype is CHAR (not VARCHAR)
# and rtrim required in some cases
function is_CHAR_datatype(field_no)
{
 if(f_DIC[field_no] == "YESNO") return(0);
 if( toupper(f_type[field_no]) ~ "^CHAR") return(1);
 return(0);
}

function javatype4fieldno(field_no)
{
 if(f_DIC[field_no] == "YESNO") return("YesNo");
 return(java4sqltype(f_type[field_no]));
} #// javatype4fieldno(field_no)

function java4sqltype(sql_type,		st_name, st_length, tmp)
{
 st_name = toupper(sql_type);
 if (st_name == "INT") { return("Integer"); }
 if (st_name == "INT4") { return("Integer"); }
 if (st_name == "BIGINT") { return("Long"); }
 if (st_name == "SMALLINT") { return("Short"); }
 if (st_name ~ "CHAR" ) { return("String"); }
 if (st_name == "TEXT" ) { return("String"); }
 if (st_name == "INET" ) { return("String"); }
 if (st_name == "TIMESTAMP") { return("DateTime"); }
 if (st_name == "TIMESTAMPTZ") { return("DateTime"); }
 return("Object");
}

function get_proc_questionmarks(	sz, i)
{
 sz=""; for(i=1;i<=FIELDS_COUNT;i++) {
  if(sz == "") sz="?"; else sz=sz ",?";
 }
 return(sz);
}

function make_java_header(f){
 printf("") >f;
 printf("// ConcepTIS project/QTIS project\n") >>f;
 printf("// (c) Alex V Eustrop 2009-2010\n") >>f;
 printf("// (c) Alex V Eustrop & EustroSoft.org 2023\n") >>f;
 printf("// see LICENSE.ConcepTIS at the project's root directory\n") >>f;
 printf("//\n") >>f;
 printf("// $Id$\n") >>f;
 printf("//\n") >>f;
 printf("\n") >>f;
}

function make_java_record_class(f){
 make_java_header(f);
 printf("package ru.mave.ConcepTIS.dao.SAM;\n") >>f;
 printf("\n") >>f;
 printf("import ru.mave.ConcepTIS.dao.*;\n") >>f;
 printf("\n") >>f;
 printf("/** .\n") >>f;
 printf(" *\n") >>f;
 printf(" */\n") >>f;
 printf("public class %s extends Record\n",SHORT_NAME) >>f;
 printf("{\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 printf("public %s\t%s;\n", javatype4fieldno(i), f_name[i] ) >>f;
 }
 printf("\n") >>f;
 if(ZID_FIELD == "")
  printf("public Long getID(){return(null);}\n") >>f;
 else
  printf("public Long getID(){return(%s);}\n",ZID_FIELD) >>f;

 for(i=1;i<=FIELDS_COUNT;i++) {
  if(f_UNIQ[i] != ""){
   printf("public String getKey(){return(%s);}\n",f_name[i]) >>f;
   break;
  }
 }
 if(i>FIELDS_COUNT)
  printf("public String getKey(){return(null);}\n") >>f;
 printf("public String getCaption(){return(getKey());}\n") >>f;
 printf("\n") >>f;
 printf("// Strings\n") >>f;
 printf("\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 printf("public final static String F_%s = \"%s\";\n",
	toupper(f_name[i]),f_name[i]) >>f;
 }
 printf("\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 printf("public final static String FC_%s = \"%s%s\";\n",
	toupper(f_name[i]),CODE,f_no[i]) >>f;
 }
 printf("\n") >>f;
 printf("// SQL requests\n") >>f;
 printf("public final static String TISQL_LIST =\n") >>f;
 printf("\t\"select * from %s.V_%s\";\n",SUBSYS,SHORT_NAME) >>f;
 printf("public final static String TISQL_LOAD =\n") >>f;
 if(ZID_FIELD == "")
  printf("\t\"select * from %s.V_%s where ? = null\";\n",
  SUBSYS,SHORT_NAME) >>f;
 else
  printf("\t\"select * from %s.V_%s where %s = ?\";\n",
  SUBSYS,SHORT_NAME,ZID_FIELD) >>f;
 printf("public final static String TISQL_CREATE =\n") >>f;
 printf("\t\"select * from %s.create_%s(%s)\";\n",
	SUBSYS,SHORT_NAME,get_proc_questionmarks()) >>f;
 printf("public final static String TISQL_UPDATE =\n") >>f;
 printf("\t\"select * from %s.update_%s(%s)\";\n",
	SUBSYS,SHORT_NAME,get_proc_questionmarks()) >>f;
 printf("public final static String TISQL_DELETE =\n") >>f;
 printf("\t\"select * from %s.delete_%s(?)\";\n",SUBSYS,SHORT_NAME) >>f;
 printf("\n") >>f;
 printf("// record processing methods\n") >>f;
 printf("\n") >>f;
 printf("public void getFromRS(java.sql.ResultSet rs)\n") >>f;
 printf(" throws java.sql.SQLException\n") >>f;
 printf("{\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
  if(is_CHAR_datatype(i))
    printf(" %s = rtrimString(getRS%s(rs,%u));\n",
	f_name[i],javatype4fieldno(i),i) >>f;
  else
    printf(" %s = getRS%s(rs,%u);\n",
	f_name[i],javatype4fieldno(i),i) >>f;
 }
 printf("} //\n") >>f;
 printf("\n") >>f;
 printf("public void getFromFS(ZFieldSet fs)\n") >>f;
 printf(" throws ZException\n") >>f;
 printf("{\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 printf(" %s = getFS%s(fs,F_%s);\n",
	f_name[i],javatype4fieldno(i),toupper(f_name[i])) >>f;
 }
 printf("} //\n") >>f;
 printf("\n") >>f;
 printf("public void putToPS(java.sql.PreparedStatement ps)\n") >>f;
 printf(" throws java.sql.SQLException\n") >>f;
 printf("{\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 printf(" setPS%s(ps,%u,%s);\n",
	javatype4fieldno(i), i, f_name[i]) >>f;
 }
 printf("} //\n") >>f;
 printf("\n") >>f;
 printf("public ZFieldSet toFieldSet()\n") >>f;
 printf("{\n") >>f;
 printf(" ZFieldSet fs = new ZFieldSet();\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 printf(" fs.add(new ZField(F_%s,%s,FC_%s,ZField.T_UNKNOWN,%u));\n",
	toupper(f_name[i]),f_name[i],toupper(f_name[i]),f_size[i]) >>f;
 }
 printf(" return(fs);\n") >>f;
 printf("} //\n") >>f;
 printf("\n") >>f;
 printf("// constructors\n") >>f;
 printf("public %s() { }\n",SHORT_NAME) >>f;
 printf("public %s(ZFieldSet fs) throws ZException { this.getFromFS(fs); }\n", \
	 SHORT_NAME) >>f;
 printf("public %s(java.sql.ResultSet rs)\n",SHORT_NAME) >>f;
 printf(" throws java.sql.SQLException\n") >>f;
 printf("{\n") >>f;
 printf(" this.getFromRS(rs);\n") >>f;
 printf("} // %s(java.sql.ResultSet rs)\n",SHORT_NAME) >>f;
 printf("\n") >>f;
 printf("} //%s\n",SHORT_NAME) >>f;
} #/make_java_record_class()

function make_java_semiproducts(f){
 printf("// %s table semiproducts\n",NAME) >f;
 printf("\n// table caption\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 printf("was.printHCellCaption(fs.get(%s.F_%s));\n",
	SHORT_NAME,toupper(f_name[i])) >>f;
 }
 printf("\n// table row from ZFieldSet\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 printf("was.printCellField(fs.get(%s.F_%s));\n",
	SHORT_NAME,toupper(f_name[i])) >>f;
 }
 printf("\n// table row from zDAO Record object\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 printf("was.printCellValue(wam.obj2string(r2.%s));\n",
	f_name[i]) >>f;
 }
 printf("\n// edit form from ZFieldSet\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 printf("was.printTabFieldInput( fs, ACLScope.F_%s);\n",
	toupper(f_name[i])) >>f;
 }
} #/make_java_semiproducts()
