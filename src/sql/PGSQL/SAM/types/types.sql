-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- purpose: special SQL data types for SAM subsystem

-- result of execution
DROP TYPE IF EXISTS SAM.execstatus CASCADE;
CREATE TYPE SAM.execstatus AS (
	ZID bigint, -- id of processed object (id,ZOID or ZRID)
	ZVER bigint,-- version of processed object (ZVER) if applicable
	errnum int, -- error number. 0 or less for success (see dic.ErrorMsg)
	errcode char(32), -- symbolic code for errnum
	errdesc varchar(1024), -- description of error
	-- parameters included to errdesc (for debug purpose):
	p1 varchar(255), 
	p2 varchar(255),
	p3 varchar(255)
	);


-- accumulative state for auditing procedure execution.
-- single auditstate variable must come over all stages of single user call.
DROP TYPE IF EXISTS SAM.auditstate CASCADE;
CREATE TYPE SAM.auditstate AS (
	-- eventrecord fields :
	id	bigint,
	sn	smallint,
	eaction	char(2),
	subsys	char(10),
	obj_type char(20),
	-- user info
	uid	bigint,
	slevel	smallint,
	capcode	varchar(32),
	capsid	bigint,
	-- object info
	sid	bigint,
	ZOID	bigint,
	ZVER	bigint,
	ZTYPE	varchar(14),
	ZID	bigint,
	ZLVL	smallint,
	-- debug info
	proc	name,
	-- assistance
	da_sole	boolean, -- only single target object accesed (defined above).
			-- write log at do_auditlog_exit(), even success or no
	da_eclass char(1), -- topmost data access eclass. used at unsuccessful or
			-- sole exit by do_auditlog_exit() if more important
			-- then default one.
	da_msg text -- textual explanation of data access action taken
	);

-- accumulative state of single user call.
DROP TYPE IF EXISTS SAM.procstate CASCADE;
CREATE TYPE SAM.procstate AS (
	success	boolean,
	a	SAM.auditstate,
	e	SAM.execstatus
	);
