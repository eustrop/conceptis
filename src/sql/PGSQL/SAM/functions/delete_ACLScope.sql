-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION SAM.delete_ACLScope(
	v_gid	bigint,
	v_sid	bigint,
	v_obj_type	char(20)
	)
 RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_as SAM.auditstate;
 v_es SAM.execstatus;
 v_user bigint;
 i bigint; --couner
 v_ro SAM.ACLScope%ROWTYPE;
 v_eventmsg text;
BEGIN
 -- v_es:=sam.make_execstatus('E_NOTIMPLEMENTED');
 -- 0) enter into procedure
 v_as.subsys = 'SAM';
 v_as.eaction :='D';
 v_as.obj_type :='XAS';
 v_as.ZLVL := null; -- not applicable for SAM.ACLScope
 v_as.ZID := v_gid;
 v_as.ZTYPE := 'X';
 -- v_as.ZLVL not applicable or unknown 
 v_as.sid := v_sid;
 v_as.proc := 'delete_ACLScope';
 v_as := SAM.do_auditlog_enter(v_as);
<<try>>
BEGIN
 -- 1) check isolation level
 IF NOT SAM.check_isolation() THEN
    v_es := SAM.make_es_transisolation(null,null); EXIT try;
 END IF;
 -- 1.1) lock requared tables
 LOCK TABLE SAM.ACLScope IN EXCLUSIVE MODE;
 -- 2) identify user
 v_user := sam.get_user();
 IF v_user IS NULL THEN
    v_es := sam.make_execstatus(null,null,'E_NOUSER',session_user); EXIT try;
 END IF;
 -- 3) check capability
 IF NOT SAM.check_capability('SAM_MANAGE') THEN
   v_es := SAM.make_execstatus(null,null,'E_NOCAPABILITY','SAM_MANAGE','MAIN');
   EXIT try; END IF;
   v_as := SAM.do_auditlog_capuse_sole(v_as,'1','SAM_MANAGE',0);
 -- 4) check data
 -- 4.1) check for record's existence
 -- 5.1) get current record
 SELECT * INTO v_ro from SAM.ACLScope
   WHERE gid = v_gid AND sid = v_sid AND obj_type = v_obj_type;
 IF NOT FOUND THEN
   v_es := sam.make_execstatus(v_gid,null,'E_NORECORD','SAM.ACLScope',
   '(gid,sid,obj_type)',
   '('||COALESCE(''||v_gid,'<NULL>')||','||COALESCE(''||v_sid,'<NULL>')||','||
   COALESCE(v_obj_type,'<NULL>')||')'); EXIT try;
 ELSE
  v_eventmsg := COALESCE( v_ro.writea,'-')||COALESCE(v_ro.createa,'-')||
   COALESCE( v_ro.deletea,'-')||COALESCE(v_ro.reada,'-')||
   COALESCE(v_ro.locka,'-')||COALESCE(v_ro.historya,'-');
 END IF;
 -- 5) delete record
 DELETE FROM SAM.ACLScope 
   WHERE gid = v_gid AND sid = v_sid AND obj_type = v_obj_type;
 v_as := SAM.do_auditlog_da_sole(v_as,'1',null,null,v_as.sid,null,
    'ACLScope:'||
   COALESCE(''||v_gid,'<NULL>')||','||COALESCE(''||v_sid,'<NULL>')||','||
   COALESCE(v_obj_type,'<NULL>')||':'||v_eventmsg);
 -- 6) exit
 v_es := sam.make_execstatus(v_gid,null,'I_SUCCESS');
EXCEPTION
 WHEN OTHERS THEN
	v_es:=sam.make_execstatus(v_gid,null,'E_SQL',SQLSTATE,SQLERRM);
END;
-- FINALLY:
 PERFORM SAM.do_auditlog_exit(v_as,v_es);
 RETURN v_es;
END $$;
REVOKE ALL ON FUNCTION SAM.delete_ACLScope(
	v_gid	bigint,
	v_sid	bigint,
	v_obj_type	char(5)
	) FROM PUBLIC;
--GRANT EXECUTE ON FUNCTION SAM.delete_ACLScope(
--	v_gid	bigint,
--	v_sid	bigint,
--	v_obj_type	char(5)
--	) TO tis_users;
COMMIT TRANSACTION;
