-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION SAM.grant_UserCapability(
	v_uid	bigint,
	v_sid	bigint,
	v_capcode	char(16)
) RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 -- v_ps SAM.procstate;
 v_as SAM.auditstate;
 v_es SAM.execstatus;
 v_r SAM.UserCapability%ROWTYPE;
 v_user bigint;
 v_zid bigint;
 i bigint; -- counter
BEGIN
 -- 0) enter into procedure
 v_as.subsys := 'SAM';
 v_as.eaction :='C';
 v_as.obj_type :='XUC';
 v_as.sid := v_sid;
 v_as.ZID := v_uid;
 v_as.ZTYPE := 'X';
 v_as.ZLVL := null;
 v_as.proc := 'grant_UserCapability';
 v_as := SAM.do_auditlog_enter(v_as);
 -- 0.1) copy parameters to v_r
 v_r.uid := v_uid;
 v_r.sid := v_sid;
 v_r.capcode := v_capcode;
<<try>>
BEGIN
 -- 1) check isolation level
 IF NOT SAM.check_isolation() THEN
    v_es := SAM.make_es_transisolation(null,null); EXIT try;
 END IF;
 -- 1.1) lock required tables
 LOCK TABLE SAM.UserCapability IN EXCLUSIVE MODE;
 -- 2) identify user
 v_user := sam.get_user();
 IF v_user IS NULL THEN
    v_es := sam.make_execstatus(null,null,'E_NOUSER',session_user); EXIT try;
 END IF;
 -- 3) check capability
 IF NOT SAM.check_capability('SAM_MANAGE') THEN
   v_es := SAM.make_execstatus(null,null,'E_NOCAPABILITY','SAM_MANAGE','MAIN');
   EXIT try; END IF;
   v_as := SAM.do_auditlog_capuse_sole(v_as,'1','SAM_MANAGE',0);
 -- 4) check data
 v_zid := v_r.uid;
 -- 4.1) check NOT NULLs
 IF v_r.uid IS NULL THEN v_es := SAM.make_es_notnull(v_zid,null,
	'SAM.UserCapability','uid'); EXIT try; END IF;
 IF v_r.sid IS NULL THEN v_es := SAM.make_es_notnull(v_zid,null,
	'SAM.UserCapability','sid'); EXIT try; END IF;
 IF v_r.capcode IS NULL THEN v_es := SAM.make_es_notnull(v_zid,null,
	'SAM.UserCapability','capcode'); EXIT try; END IF;
 -- 4.2) check dictionary fields
 IF NOT dic.check_code('CAPABILITY',v_r.capcode) THEN
   v_es := SAM.make_es_invalidcode(v_zid,null,v_r.capcode,
   'SAM.UserCapability.capcode','CAPABILITY'); EXIT try; END IF;
 -- 4.3) check uniqueness
 PERFORM * FROM SAM.UserCapability
  WHERE uid = v_r.uid AND sid = v_r.sid AND capcode = v_r.capcode;
 IF FOUND THEN
  v_es :=SAM.make_execstatus(v_zid,null,'E_DUPRECORD','SAM.UserCapability',
  '(uid,sid,capcode)','('||v_r.uid||','||v_r.sid||','||v_r.capcode||')');
  EXIT try;
 END IF;
 -- 4.4) check references
  IF v_r.sid <> 0 THEN
   SELECT COUNT(*) INTO i FROM SAM.Scope WHERE id = v_r.sid;
   IF i = 0 THEN v_es := SAM.make_execstatus(v_zid,null,'E_NOSCOPE',
	CAST(v_r.sid AS text)); EXIT try; END IF;
  END IF;
  SELECT COUNT(*) INTO i FROM SAM.User WHERE id = v_r.uid;
   IF i = 0 THEN v_es := SAM.make_execstatus(v_zid,null,'E_NOBJECT',
	'SAM.User','id',CAST(v_r.uid AS text)); EXIT try; END IF;
 -- 5) add record
 INSERT INTO SAM.UserCapability values(v_r.*);
 v_as := SAM.do_auditlog_da_sole(v_as,'1',null,null,v_r.sid,null,
 	'grant capcode='||v_r.capcode||' on sid='||v_r.sid);
 -- 6) exit
 v_es := sam.make_execstatus(v_r.uid,null,'I_SUCCESS');
EXCEPTION
 WHEN OTHERS THEN
	v_es:=sam.make_execstatus(v_r.uid,null,'E_SQL',SQLSTATE,SQLERRM);
END;
-- FINALLY:
 PERFORM SAM.do_auditlog_exit(v_as,v_es);
 RETURN v_es;
END $$;
--GRANT EXECUTE ON FUNCTION SAM.grant_UserCapability(
--	v_uid	bigint,
--	v_sid	bigint,
--	v_capcode	char(16)
--) TO tis_users;
REVOKE ALL ON FUNCTION SAM.grant_UserCapability(
	v_uid	bigint,
	v_sid	bigint,
	v_capcode	char(16)
) FROM PUBLIC;
COMMIT TRANSACTION;
