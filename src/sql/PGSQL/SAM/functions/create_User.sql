-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION SAM.create_User(
	v_id	bigint,
	v_sid	bigint,
	v_slevel	smallint,
	v_slevel_min	smallint,
	v_slevel_max	smallint,
	v_locked	char(1),
	v_lang	char(3),
	v_login	varchar(64),
	v_db_user	varchar(64),
	v_full_name	varchar(255)
) RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 v_as SAM.auditstate;
 v_es SAM.execstatus;
 v_r SAM.User%ROWTYPE;
 v_user bigint;
BEGIN
 -- 0) enter into procedure
 v_as.subsys := 'SAM';
 v_as.eaction :='C';
 v_as.obj_type :='XU';
 v_as.sid := v_sid;
 -- v_as.ZID := v_id;
 v_as.ZTYPE := 'X';
 v_as.ZLVL := v_slevel;
 v_as.proc := 'create_User';
 v_as := SAM.do_auditlog_enter(v_as);
 -- 0.1) copy parameters to v_r
 v_r.id := null; -- new record;
 v_r.sid := v_sid;
 v_r.slevel := v_slevel;
 v_r.slevel_min := COALESCE(v_slevel_min,v_slevel);
 v_r.slevel_max := COALESCE(v_slevel_max,v_slevel);
 v_r.locked := v_locked;
 v_r.lang := v_lang;
 v_r.login := v_login;
 v_r.db_user := v_db_user;
 v_r.full_name := v_full_name;
<<try>>
BEGIN
 -- 1) check isolation level
 IF NOT SAM.check_isolation() THEN
    v_es := SAM.make_es_transisolation(null,null); EXIT try;
 END IF;
 -- 1.1) lock required tables
 LOCK TABLE SAM.User IN EXCLUSIVE MODE;
 -- 2) identify user
 v_user := sam.get_user();
 IF v_user IS NULL THEN
    v_es := sam.make_execstatus(null,null,'E_NOUSER',session_user); EXIT try;
 END IF;
 -- 3) check capability
 IF NOT SAM.check_capability('SAM_MANAGE') THEN
   v_es := SAM.make_execstatus(null,null,'E_NOCAPABILITY','SAM_MANAGE','MAIN');
   EXIT try; END IF;
   v_as := SAM.do_auditlog_capuse_sole(v_as,'1','SAM_MANAGE',0);
 -- 4) check data
 v_ps := SAM.QC_User(v_as,v_r); v_as := v_ps.a;
   IF NOT v_ps.success THEN v_es := v_ps.e; EXIT try; END IF;
 -- 5) add record
 --v_r.id := nextval('SAM.User_seq');
 v_r.id := SAM.next_QOID(v_r.sid,'SAM.XU');
 INSERT INTO SAM.User values(v_r.*);
 v_as.ZID := v_r.id;
 v_as := SAM.do_auditlog_da_sole(v_as,'1',null,null,v_r.sid,v_r.id);
 -- 6) exit
 v_es := sam.make_execstatus(v_r.id,null,'I_SUCCESS');
EXCEPTION
 WHEN OTHERS THEN
	v_es:=sam.make_execstatus(v_r.id,null,'E_SQL',SQLSTATE,SQLERRM);
END;
-- FINALLY:
 PERFORM SAM.do_auditlog_exit(v_as,v_es);
 RETURN v_es;
END $$;
REVOKE ALL ON FUNCTION SAM.create_User(
	v_id	bigint,
	v_sid	bigint,
	v_slevel	smallint,
	v_slevel_min	smallint,
	v_slevel_max	smallint,
	v_locked	char(1),
	v_lang	char(3),
	v_login	varchar(64),
	v_db_user	varchar(64),
	v_full_name	varchar(255)
) FROM PUBLIC;
--GRANT EXECUTE ON FUNCTION SAM.create_User(
--	v_id	bigint,
--	v_sid	bigint,
--	v_slevel	smallint,
--	v_slevel_min	smallint,
--	v_slevel_max	smallint,
--	v_locked	char(1),
--	v_lang	char(3),
--	v_login	varchar(64),
--	v_db_user	varchar(64),
--	v_full_name	varchar(255)
--) TO tis_users;
COMMIT TRANSACTION;
