-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- purpose: check current user access rights using DAC/MAC
--- functions:
--   SAM.check_MAC_write( v_sid, v_obj_type, v_ZLVL) RETURNS SAM.execstatus
--     core Mandatory Access Control (MAC) function
--     returns SAM.execstatus with errnum=0 if write allowed
--     errnum>0 if denied
--     returned object can be used to inform user about error
--
--   SAM.check_access_create(...) RETURNS SAM.procstate
--   SAM.check_access_write(...) RETURNS SAM.procstate
--   SAM.check_access_delete(...) RETURNS SAM.procstate
--   SAM.check_access_lock(...) RETURNS SAM.procstate
--   Parameters:
--     v_as SAM.auditstate,
--     v_sid bigint,		-- target (or source) Scope id ZSID/QSID
--     v_obj_type varchar(20),	-- data object, table of field code
--				-- SUBSYSTEM.OBJ
--				-- SUBSYSTEM.OTAB
--				-- SUBSYSTEM.OBJ[.TAB.FIELD]
--				-- SUBSYSTEM.OTAB[.OFIELD]
--				-- SUBSYSTEM max length 10 char
--				-- OBJ max length 3 char
--				-- TAB max length 2 char
--				-- OTAB max length 3 char
--				-- FIELD length 2 char
--				-- OFIELD length from  2 to 5 char
--				-- only SUBSYSTEM.OBJ/SUBSYSTEM.OTAB implemented
--     v_ZLVL smallint		-- SLEVEL of target object
--   RETURNS SAM.procstate with next fields:
--      success boolean,		-- true if requested access allowed
--      a       SAM.auditstate,	-- used to audit log
--      e       SAM.execstatus	-- used to inform about error or access denied cause
--

BEGIN TRANSACTION;

CREATE OR REPLACE FUNCTION SAM.check_MAC_write( v_sid bigint, v_obj_type varchar(20), v_ZLVL smallint)
  RETURNS SAM.execstatus LANGUAGE plpgSQL SECURITY DEFINER STABLE AS $$
-- v_obj_type - type of object for creation
-- v_sid - scope for new object
-- v_ZLVL - ZLVL requested for new object
DECLARE
 v_ps SAM.procstate;
 v_zid	bigint;
 i	integer;
 v_es SAM.execstatus;
BEGIN
<<try>>
BEGIN
 -- 1) MAC
 -- 1.0) is requested slevel more than maximum slevel configured for this system
 -- 1.1) is user slevel at strong Bell–LaPadula Model
 IF sam.get_user_slevel() >= sam.get_slevel_strongBLM() THEN
 IF v_ZLVL < sam.get_user_slevel() THEN
  v_es := SAM.make_execstatus(null,null,'E_SLVL_LOW',
  v_ZLVL::text,CAST(sam.get_user_slevel() AS text)); EXIT try; END IF;
 END IF;
 -- 1.2) check slevel downgrading
 -- 1.2.1) is requested slevel less or equal system slevel 
 IF v_ZLVL <= sam.get_slevel_system() THEN
  -- SIC! check_capability('WRITE_SYSTEM_OBJECT') not implemented yet, capcode char(16) -> varchar(32) conversion needed
  v_es := SAM.make_execstatus(null,null,'E_SLVL_SYSLOW',
  v_ZLVL::text,CAST(sam.get_slevel_system() AS text)); EXIT try; END IF;
 -- 1.2.2) minimum user allowed slevel
 IF v_ZLVL < sam.get_user_slevel_min() THEN
  v_es := SAM.make_execstatus(null,null,'E_SLVL_LOW_USER',
  v_ZLVL::text,CAST(sam.get_user_slevel_min() AS text)); EXIT try; END IF;
 -- 1.2.3) minimum scope allowed slevel
 IF v_ZLVL < sam.get_scope_slevel_min(v_sid) THEN
  v_es := SAM.make_execstatus(null,null,'E_SLVL_LOW_SID',
  v_ZLVL::text,CAST(sam.get_scope_slevel_min(v_sid) AS text)); EXIT try; END IF;
 -- 1.2.4) minimum scope/objecttype allowed slevel
 -- SIC! not implemented yet
 -- 1.2.4) minimum system allowed slevel
 IF v_ZLVL < sam.get_slevel_min() THEN
  v_es := SAM.make_execstatus(null,null,'E_SLVL_LOW_SYS',
  v_ZLVL::text,CAST(sam.get_slevel_min() AS text)); EXIT try; END IF;
 -- 1.3) check slevel upgrading
 -- 1.3.1) check slevel upgrading per user
 IF v_ZLVL > sam.get_user_slevel_max() THEN
  v_es := SAM.make_execstatus(null,null,'E_SLVL_HIGH_USER',
  v_ZLVL::text,CAST(sam.get_user_slevel_max() AS text)); EXIT try; END IF;
 -- 1.3.2) check slevel upgrading per scope
 IF v_ZLVL > sam.get_scope_slevel_max(v_sid) THEN
  v_es := SAM.make_execstatus(null,null,'E_SLVL_HIGH_SID',
  v_ZLVL::text,CAST(sam.get_scope_slevel_max(v_sid) AS text)); EXIT try; END IF;
 -- 1.3.3) check slevel upgrading per system
 IF v_ZLVL > sam.get_slevel_max() THEN
  v_es := SAM.make_execstatus(null,null,'E_SLVL_HIGH_SYS',
  v_ZLVL::text,CAST(sam.get_slevel_max() AS text)); EXIT try; END IF;
 -- OK, all tests passed - write allowed by MAC
 v_es.errnum=0;
END;
-- FINALLY:
 RETURN v_es;
END $$;

CREATE OR REPLACE FUNCTION SAM.check_access_create( v_as SAM.auditstate,
 v_sid bigint, v_obj_type varchar(20), v_ZLVL smallint)
  RETURNS SAM.procstate LANGUAGE plpgSQL SECURITY DEFINER STABLE AS $$
-- v_obj_type - type of object for creation
-- v_sid - scope for new object
-- v_ZLVL - ZLVL requested for new object
DECLARE
 v_ps SAM.procstate;
 v_zid	bigint;
 i	integer;
 v_es_fail SAM.execstatus;
BEGIN
 v_ps.success := FALSE;
<<try>>
BEGIN
 -- 1) MAC
 v_es_fail = SAM.check_MAC_write(v_sid,v_obj_type,v_ZLVL);
 IF v_es_fail.errnum > 0 THEN
  v_ps.e := v_es_fail;
  EXIT try;
 END IF;
 -- 2) DAC - look for permissions
 PERFORM XAS.gid FROM SAM.ACLScope XAS, SAM.UserGroup XUG WHERE
	XUG.uid = SAM.get_user() AND XUG.gid = XAS.gid AND
	XAS.sid = v_sid AND XAS.obj_type = v_obj_type AND
	XAS.createa = 'Y';
 IF NOT FOUND THEN
  v_ps.e := SAM.make_execstatus(null,null,'E_NOACCESS'); EXIT try; END IF;
 -- 3) DAC - look for rejections
 PERFORM XAS.gid FROM SAM.ACLScope XAS, SAM.UserGroup XUG WHERE
	XUG.uid = SAM.get_user() AND XUG.gid = XAS.gid AND
	XAS.sid = v_sid AND XAS.obj_type = v_obj_type AND
	XAS.createa = 'R';
 IF FOUND THEN
  v_ps.e := SAM.make_execstatus(null,null,'E_ACCESSDENIED','ACL'); EXIT try; END IF;
 -- 99) all tests passed
 v_ps.success := TRUE;
END;
-- FINALLY:
 v_as := SAM.do_auditlog_ac(v_as,null,'AC',v_obj_type,v_sid,null,v_ZLVL,v_ps);
 v_ps.a:=v_as;
 RETURN v_ps;
END $$;
-- this function for internal use only
REVOKE ALL ON FUNCTION SAM.check_access_create( v_as SAM.auditstate,
 v_sid bigint, v_obj_type varchar(20), v_ZLVL smallint) FROM PUBLIC;

CREATE OR REPLACE FUNCTION SAM.check_access_write( v_as SAM.auditstate,
 v_sid bigint, v_obj_type varchar(20), v_ZOID bigint, v_ZLVL smallint)
  RETURNS SAM.procstate LANGUAGE plpgSQL SECURITY DEFINER STABLE AS $$
-- v_obj_type - type of object for creation
-- v_sid - scope for new object
-- v_ZOID - ZOID of object, can be used with ACLObject (future reservation)
-- v_ZLVL - ZLVL requested for new object
DECLARE
 v_ps SAM.procstate;
 v_zid	bigint;
 i	integer;
 v_es_fail SAM.execstatus;
BEGIN
 v_ps.success := FALSE;
<<try>>
BEGIN
 -- 1) MAC
 v_es_fail = SAM.check_MAC_write(v_sid,v_obj_type,v_ZLVL);
 IF v_es_fail.errnum > 0 THEN
  v_ps.e := v_es_fail;
  EXIT try;
 END IF;
 -- 2) DAC - look for permissions
 PERFORM XAS.gid FROM SAM.ACLScope XAS, SAM.UserGroup XUG WHERE
	XUG.uid = SAM.get_user() AND XUG.gid = XAS.gid AND
	XAS.sid = v_sid AND XAS.obj_type = v_obj_type AND
	XAS.writea = 'Y';
 IF NOT FOUND THEN
  v_ps.e := SAM.make_execstatus(null,null,'E_NOACCESS'); EXIT try; END IF;
 -- 3) DAC - look for rejections
 PERFORM XAS.gid FROM SAM.ACLScope XAS, SAM.UserGroup XUG WHERE
	XUG.uid = SAM.get_user() AND XUG.gid = XAS.gid AND
	XAS.sid = v_sid AND XAS.obj_type = v_obj_type AND
	XAS.writea = 'R';
 IF FOUND THEN
  v_ps.e := SAM.make_execstatus(null,null,'E_ACCESSDENIED','ACL'); EXIT try; END IF;
 -- 99) all tests passed
 v_ps.success := TRUE;
END;
-- FINALLY:
 v_as := SAM.do_auditlog_ac(v_as,null,'AW',v_obj_type,v_sid,null,v_ZLVL,v_ps);
 v_ps.a:=v_as;
 RETURN v_ps;
END $$;
-- this function for internal use only
REVOKE ALL ON FUNCTION SAM.check_access_write(v_as SAM.auditstate,v_sid bigint,
	v_obj_type varchar(20), v_ZOID bigint, v_ZLVL smallint) FROM PUBLIC;

CREATE OR REPLACE FUNCTION SAM.check_access_delete( v_as SAM.auditstate,
 v_sid bigint, v_obj_type varchar(20), v_ZOID bigint, v_ZLVL smallint)
  RETURNS SAM.procstate LANGUAGE plpgSQL SECURITY DEFINER STABLE AS $$
-- v_obj_type - type of object for creation
-- v_sid - scope for new object
-- v_ZOID - ZOID of object, can be used with ACLObject (future reservation)
-- v_ZLVL - ZLVL requested for new object
DECLARE
 v_ps SAM.procstate;
 v_zid	bigint;
 i	integer;
 v_es_fail SAM.execstatus;
BEGIN
 v_ps.success := FALSE;
<<try>>
BEGIN
 -- 1) MAC
 v_es_fail = SAM.check_MAC_write(v_sid,v_obj_type,v_ZLVL);
 IF v_es_fail.errnum > 0 THEN
  v_ps.e := v_es_fail;
  EXIT try;
 END IF;
 -- 2) DAC - look for permissions
 PERFORM XAS.gid FROM SAM.ACLScope XAS, SAM.UserGroup XUG WHERE
	XUG.uid = SAM.get_user() AND XUG.gid = XAS.gid AND
	XAS.sid = v_sid AND XAS.obj_type = v_obj_type AND
	XAS.deletea = 'Y';
 IF NOT FOUND THEN
  v_ps.e := SAM.make_execstatus(null,null,'E_NOACCESS'); EXIT try; END IF;
 -- 3) DAC - look for rejections
 PERFORM XAS.gid FROM SAM.ACLScope XAS, SAM.UserGroup XUG WHERE
	XUG.uid = SAM.get_user() AND XUG.gid = XAS.gid AND
	XAS.sid = v_sid AND XAS.obj_type = v_obj_type AND
	XAS.deletea = 'R';
 IF FOUND THEN
  v_ps.e := SAM.make_execstatus(null,null,'E_ACCESSDENIED','ACL'); EXIT try; END IF;
 -- 99) all tests passed
 v_ps.success := TRUE;
END;
-- FINALLY:
 v_as := SAM.do_auditlog_ac(v_as,null,'AD',v_obj_type,v_sid,null,v_ZLVL,v_ps);
 v_ps.a:=v_as;
 RETURN v_ps;
END $$;
-- this function for internal use only
REVOKE ALL ON FUNCTION SAM.check_access_delete(v_as SAM.auditstate,v_sid bigint,
	v_obj_type varchar(20), v_ZOID bigint, v_ZLVL smallint) FROM PUBLIC;

CREATE OR REPLACE FUNCTION SAM.check_access_lock( v_as SAM.auditstate,
 v_sid bigint, v_obj_type varchar(20), v_ZOID bigint, v_ZLVL smallint)
  RETURNS SAM.procstate LANGUAGE plpgSQL SECURITY DEFINER STABLE AS $$
-- v_obj_type - type of object for creation
-- v_sid - scope for new object
-- v_ZOID - ZOID of object, can be used with ACLObject (future reservation)
-- v_ZLVL - ZLVL requested for new object
DECLARE
 v_ps SAM.procstate;
 v_zid	bigint;
 i	integer;
 v_es_fail SAM.execstatus;
BEGIN
 v_ps.success := FALSE;
<<try>>
BEGIN
 -- 1) MAC
 v_es_fail = SAM.check_MAC_write(v_sid,v_obj_type,v_ZLVL);
 IF v_es_fail.errnum > 0 THEN
  v_ps.e := v_es_fail;
  EXIT try;
 END IF;
 -- 2) DAC - look for permissions
 PERFORM XAS.gid FROM SAM.ACLScope XAS, SAM.UserGroup XUG WHERE
	XUG.uid = SAM.get_user() AND XUG.gid = XAS.gid AND
	XAS.sid = v_sid AND XAS.obj_type = v_obj_type AND
	XAS.locka = 'Y';
 IF NOT FOUND THEN
  v_ps.e := SAM.make_execstatus(null,null,'E_NOACCESS'); EXIT try; END IF;
 -- 3) DAC - look for rejections
 PERFORM XAS.gid FROM SAM.ACLScope XAS, SAM.UserGroup XUG WHERE
	XUG.uid = SAM.get_user() AND XUG.gid = XAS.gid AND
	XAS.sid = v_sid AND XAS.obj_type = v_obj_type AND
	XAS.locka = 'R';
 IF FOUND THEN
  v_ps.e := SAM.make_execstatus(null,null,'E_ACCESSDENIED','ACL'); EXIT try; END IF;
 -- 99) all tests passed
 v_ps.success := TRUE;
END;
-- FINALLY:
 v_as := SAM.do_auditlog_ac(v_as,null,'AL',v_obj_type,v_sid,null,v_ZLVL,v_ps);
 v_ps.a:=v_as;
 RETURN v_ps;
END $$;
-- this function for internal use only
REVOKE ALL ON FUNCTION SAM.check_access_lock(v_as SAM.auditstate,v_sid bigint,
	v_obj_type varchar(20), v_ZOID bigint, v_ZLVL smallint) FROM PUBLIC;
COMMIT TRANSACTION;
