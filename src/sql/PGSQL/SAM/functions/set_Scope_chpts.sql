-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION SAM.set_Scope_chpts(
	v_id	bigint,
	v_chpts	timestamp
) RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 v_as SAM.auditstate;
 v_es SAM.execstatus;
 v_r SAM.Scope%ROWTYPE;
 v_user bigint;
 i bigint; -- counter
 v_current_chpts timestamp;
BEGIN
 -- 0) enter into procedure
 v_as.subsys = 'SAM';
 v_as.eaction :='M';
 v_as.obj_type :='XS';
 v_as.sid := v_id;
 v_as.ZID := v_id;
 v_as.ZTYPE := 'X';
 v_as.ZLVL := null; -- unknown at this moment
 v_as.proc := 'set_Scope_chpts';
 v_as := SAM.do_auditlog_enter(v_as);
 -- 0.1) copy parameters to v_r
 v_r.id := v_id;
 v_r.chpts := v_chpts; -- std editing unallowed
<<try>>
BEGIN
 -- 1) check isolation level
 IF NOT SAM.check_isolation() THEN
    v_es := SAM.make_es_transisolation(null,null); EXIT try;
 END IF;
 -- 1.1) lock required tables
 LOCK TABLE SAM.Scope IN EXCLUSIVE MODE;
 -- 2) identify user
 v_user := sam.get_user();
 IF v_user IS NULL THEN
    v_es := sam.make_execstatus(v_r.id,null,'E_NOUSER',session_user); EXIT try;
 END IF;
 -- 3) check capability
 IF NOT SAM.check_capability('SAM_MANAGE') THEN
   v_es :=SAM.make_execstatus(v_r.id,null,'E_NOCAPABILITY','SAM_MANAGE','MAIN');
   EXIT try; END IF;
   v_as := SAM.do_auditlog_capuse_sole(v_as,'1','SAM_MANAGE',0);
 -- 4) check data
 -- 4.1) check for record's existence
 SELECT chpts INTO v_current_chpts FROM SAM.Scope WHERE id = v_r.id;
 IF NOT FOUND THEN
   v_es := sam.make_execstatus(v_r.id,null,'E_NOBJECT','SAM.Scope',
   'id',CAST(v_r.id as text)); EXIT try;
 END IF;
 -- 4.2) STD Quality Control
   -- dosn't needed
 -- 4.3) check current CHPTS
 IF (v_current_chpts > v_r.chpts) or (v_r.chpts is null) THEN
   v_es := sam.make_execstatus(v_r.id,null,'E_INVALIDCHPTS',CAST(v_id as text),
   CAST(v_r.chpts as text),CAST(v_current_chpts as text)); EXIT try;
 END IF;
 -- 5) update record
 UPDATE SAM.Scope SET (chpts) = (v_r.chpts)
 WHERE id = v_r.id and (chpts <= v_r.chpts or chpts IS NULL);
 IF NOT FOUND THEN
   v_es := sam.make_execstatus(v_r.id,null,'E_NOBJECT','SAM.Scope',
   'id',CAST(v_r.id as text)); EXIT try;
 END IF;
 v_as := SAM.do_auditlog_fchanges(v_as,'1',cast(-1 as smallint),'chpts',
	CAST(v_current_chpts as text), CAST(v_r.chpts as text));
 v_as := SAM.do_auditlog_da_sole(v_as,'1',null,null,null,null);
 -- 6) exit
 v_es := sam.make_execstatus(v_r.id,null,'I_SUCCESS');
EXCEPTION
 WHEN OTHERS THEN
	v_es:=sam.make_execstatus(v_r.id,null,'E_SQL',SQLSTATE,SQLERRM);
END;
-- FINALLY:
 PERFORM SAM.do_auditlog_exit(v_as,v_es);
 RETURN v_es;
END $$;
REVOKE ALL ON FUNCTION SAM.set_Scope_chpts(
	v_id	bigint,
	v_chpts	timestamp
) FROM PUBLIC;
--GRANT EXECUTE ON FUNCTION SAM.set_Scope_chpts(
--	v_id	bigint,
--	v_chpts	timestamp
--) TO tis_users;
COMMIT TRANSACTION;
