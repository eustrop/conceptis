-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

-- set_UserPasswordMD5
BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION SAM.set_UserPasswordMD5(
	v_uid	bigint,
--	v_algo	varchar(8),
--	v_iterations	INT,
--	v_datefrom	timestamp,
--	v_dateto	timestamp,
	v_password	varchar(127)
--	v_salt	varchar(127)
) RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 v_as SAM.auditstate;
 v_es SAM.execstatus;
 v_r SAM.UserPassword%ROWTYPE;
 v_user bigint;
BEGIN
 -- 0) enter into procedure
 v_as.subsys := 'SAM';
 v_as.eaction := 'U'; -- 'C';
 v_as.obj_type :='XUP';
 v_as.sid := null;
 -- v_as.ZID := v_id;
 v_as.ZTYPE := 'X';
 v_as.ZLVL := null;
 v_as.proc := 'set_UserPasswordMD5';
 v_as := SAM.do_auditlog_enter(v_as);
 -- 0.1) copy parameters to v_r
 v_r.uid := v_uid;
 v_r.algo := 'MD5'; -- v_algo;
 v_r.iterations := 0; -- v_iterations;
 v_r.datefrom := NOW(); -- v_datefrom;
 v_r.dateto := null; -- v_dateto;
 v_r.password := v_password;
 v_r.salt := null; -- v_salt;
<<try>>
BEGIN
 -- 1) check isolation level
 IF NOT SAM.check_isolation() THEN
    v_es := SAM.make_es_transisolation(null,null); EXIT try;
 END IF;
 -- 1.1) lock required tables
 LOCK TABLE SAM.UserPassword IN EXCLUSIVE MODE;
 -- 2) identify user
 v_user := sam.get_user();
 IF v_user IS NULL THEN
    v_es := sam.make_execstatus(null,null,'E_NOUSER',session_user); EXIT try;
 END IF;
 -- 3) check capability
 IF NOT SAM.check_capability('SAM_MANAGE') THEN
   v_es := SAM.make_execstatus(null,null,'E_NOCAPABILITY','SAM_MANAGE','MAIN');
   EXIT try; END IF;
   v_as := SAM.do_auditlog_capuse_sole(v_as,'1','SAM_MANAGE',0);
 -- 4) check data
 v_ps := SAM.QC_UserPassword(v_as,v_r); v_as := v_ps.a;
   IF NOT v_ps.success THEN v_es := v_ps.e; EXIT try; END IF;
 -- 5) add record
 -- 5.1) update
 UPDATE SAM.UserPassword SET (algo,iterations,datefrom,dateto,password,salt)
 = (v_r.algo,v_r.iterations,v_r.datefrom,v_r.dateto, v_r.password,v_r.salt)
 WHERE uid = v_r.uid;
 -- 5.1) create
 IF NOT FOUND THEN
  INSERT INTO SAM.UserPassword values(v_r.*); v_as.eaction := 'C';
 END IF;
 v_as := SAM.do_auditlog_da_sole(v_as,'1',null,null,null,null);
 -- 6) exit
 v_es := sam.make_execstatus(null,null,'I_SUCCESS');
EXCEPTION
 WHEN OTHERS THEN
	v_es:=sam.make_execstatus(null,null,'E_SQL',SQLSTATE,SQLERRM);
END;
-- FINALLY:
 PERFORM SAM.do_auditlog_exit(v_as,v_es);
 RETURN v_es;
END $$;
-- GRANT/REVOKE moved to functions.grant/ directory COMMIT TRANSACTION;
COMMIT;
