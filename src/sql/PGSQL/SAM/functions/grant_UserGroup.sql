-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION SAM.grant_UserGroup(
	v_uid	bigint,
	v_gid	bigint
) RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 -- v_ps SAM.procstate;
 v_as SAM.auditstate;
 v_es SAM.execstatus;
 v_r SAM.UserGroup%ROWTYPE;
 v_user bigint;
 v_zid bigint;
 i bigint; -- counter
BEGIN
 -- 0) enter into procedure
 v_as.subsys := 'SAM';
 v_as.eaction :='C';
 v_as.obj_type :='XUC';
 -- v_as.sid := v_sid;
 SELECT sid INTO v_as.sid from SAM.V_User where id = v_uid;
 v_as.ZID := v_uid;
 v_as.ZTYPE := 'X';
 v_as.ZLVL := null;
 v_as.proc := 'grant_UserGroup';
 v_as := SAM.do_auditlog_enter(v_as);
 -- 0.1) copy parameters to v_r
 v_r.uid := v_uid;
 v_r.gid := v_gid;
<<try>>
BEGIN
 -- 1) check isolation level
 IF NOT SAM.check_isolation() THEN
    v_es := SAM.make_es_transisolation(null,null); EXIT try;
 END IF;
 -- 1.1) lock required tables
 LOCK TABLE SAM.UserGroup IN EXCLUSIVE MODE;
 -- 2) identify user
 v_user := sam.get_user();
 IF v_user IS NULL THEN
    v_es := sam.make_execstatus(null,null,'E_NOUSER',session_user); EXIT try;
 END IF;
 -- 3) check capability
 IF NOT SAM.check_capability('SAM_MANAGE') THEN
   v_es := SAM.make_execstatus(null,null,'E_NOCAPABILITY','SAM_MANAGE','MAIN');
   EXIT try; END IF;
   v_as := SAM.do_auditlog_capuse_sole(v_as,'1','SAM_MANAGE',0);
 -- 4) check data
 v_zid := v_r.uid;
 -- 4.1) check NOT NULLs
 IF v_r.uid IS NULL THEN v_es := SAM.make_es_notnull(v_zid,null,
	'SAM.UserGroup','uid'); EXIT try; END IF;
 IF v_r.gid IS NULL THEN v_es := SAM.make_es_notnull(v_zid,null,
	'SAM.UserGroup','gid'); EXIT try; END IF;
 -- 4.2) check dictionary fields
  -- not necessary
 -- 4.3) check uniqueness
 PERFORM * FROM SAM.UserGroup
  WHERE uid = v_r.uid AND gid = v_r.gid;
 IF FOUND THEN
  v_es :=SAM.make_execstatus(v_zid,null,'E_DUPRECORD','SAM.UserGroup',
  '(uid,gid)','('||v_r.uid||','||v_r.gid||')');
  EXIT try;
 END IF;
 -- 4.4) check references
  IF v_r.gid <> 0 THEN
   SELECT COUNT(*) INTO i FROM SAM.Group WHERE id = v_r.gid;
   IF i = 0 THEN v_es := SAM.make_execstatus(v_zid,null,'E_NOBJECT',
	'SAM.Group','gid',CAST(v_r.gid AS text)); EXIT try; END IF;
  END IF;
  SELECT COUNT(*) INTO i FROM SAM.User WHERE id = v_r.uid;
   IF i = 0 THEN v_es := SAM.make_execstatus(v_zid,null,'E_NOBJECT',
	'SAM.User','id',CAST(v_r.uid AS text)); EXIT try; END IF;
 -- 5) add record
 INSERT INTO SAM.UserGroup values(v_r.*);
 v_as := SAM.do_auditlog_da_sole(v_as,'1',null,null,v_as.sid,null,
  'grant group='||v_r.gid||' to uid='||v_r.uid);
 -- 6) exit
 v_es := sam.make_execstatus(v_r.uid,null,'I_SUCCESS');
EXCEPTION
 WHEN OTHERS THEN
	v_es:=sam.make_execstatus(v_r.uid,null,'E_SQL',SQLSTATE,SQLERRM);
END;
-- FINALLY:
 PERFORM SAM.do_auditlog_exit(v_as,v_es);
 RETURN v_es;
END $$;
--GRANT EXECUTE ON FUNCTION SAM.grant_UserGroup(
--	v_uid	bigint,
--	v_gid	bigint
--) TO tis_users;
REVOKE ALL ON FUNCTION SAM.grant_UserGroup(
	v_uid	bigint,
	v_gid	bigint
) FROM PUBLIC;
COMMIT TRANSACTION;
