-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION SAM.create_ScopePolicy(
	v_sid	bigint,
	v_obj_type	varchar(14),
	v_maxcount	bigint,
	v_doaudit	char(1),
	v_dodebug	char(1)
) RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 v_as SAM.auditstate;
 v_es SAM.execstatus;
 v_r SAM.ScopePolicy%ROWTYPE;
 v_user bigint;
BEGIN
 -- 0) enter into procedure
 v_as.subsys := 'SAM';
 v_as.eaction :='C';
 v_as.obj_type :='XSP';
 v_as.sid := v_sid;
 -- v_as.ZID := v_id;
 v_as.ZTYPE := 'X';
 v_as.ZLVL := null;
 v_as.proc := 'create_ScopePolicy';
 v_as := SAM.do_auditlog_enter(v_as);
 -- 0.1) copy parameters to v_r
 v_r.sid := v_sid;
 v_r.obj_type := v_obj_type;
 v_r.maxcount := v_maxcount;
 -- v_r.ocount := v_ocount; -- std editing unallowed
 v_r.ocount := null; -- std editing unallowed
 v_r.doaudit := v_doaudit;
 v_r.dodebug := v_dodebug;
<<try>>
BEGIN
 -- 1) check isolation level
 IF NOT SAM.check_isolation() THEN
    v_es := SAM.make_es_transisolation(null,null); EXIT try;
 END IF;
 -- 1.1) lock required tables
 LOCK TABLE SAM.ScopePolicy IN EXCLUSIVE MODE;
 -- 2) identify user
 v_user := sam.get_user();
 IF v_user IS NULL THEN
    v_es := sam.make_execstatus(null,null,'E_NOUSER',session_user); EXIT try;
 END IF;
 -- 3) check capability
 IF NOT SAM.check_capability('SAM_MANAGE') THEN
   v_es := SAM.make_execstatus(null,null,'E_NOCAPABILITY','SAM_MANAGE','MAIN');
   EXIT try; END IF;
   v_as := SAM.do_auditlog_capuse_sole(v_as,'1','SAM_MANAGE',0);
 -- 4) check data
 v_ps := SAM.QC_ScopePolicy(v_as,v_r); v_as := v_ps.a;
   IF NOT v_ps.success THEN v_es := v_ps.e; EXIT try; END IF;
 -- 5) add record
 INSERT INTO SAM.ScopePolicy values(v_r.*);
 v_as := SAM.do_auditlog_da_sole(v_as,'1',null,null,v_r.sid,null);
 -- 6) exit
 v_es := sam.make_execstatus(null,null,'I_SUCCESS');
EXCEPTION
 WHEN OTHERS THEN
	v_es:=sam.make_execstatus(null,null,'E_SQL',SQLSTATE,SQLERRM);
END;
-- FINALLY:
 PERFORM SAM.do_auditlog_exit(v_as,v_es);
 RETURN v_es;
END $$;
-- GRANT/REVOKE moved to functions.grant/ directory
COMMIT TRANSACTION;
