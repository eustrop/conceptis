-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

-- logonMD5
BEGIN TRANSACTION;
--drop function SAM.logonMD5(varchar(64),varchar(127));
CREATE OR REPLACE FUNCTION SAM.logonMD5(
	v_login	varchar(64),
--	v_uid	bigint,
--	v_algo	varchar(8),
--	v_iterations	INT,
--	v_datefrom	timestamp,
--	v_dateto	timestamp,
	v_password	varchar(127),
	v_secretcookie	varchar(127),
	v_devicefrom	varchar(127)
--	v_salt	varchar(127)
) RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 v_as SAM.auditstate;
 v_es SAM.execstatus;
 v_r SAM.UserPassword%ROWTYPE;
 v_r_XUS SAM.UserSession%ROWTYPE;
 v_r_XB SAM.SessionBind%ROWTYPE;
 v_user bigint;
 v_slevel smallint;
BEGIN
 -- 0) enter into procedure
 v_as.subsys := 'SAM';
 v_as.eaction := 'U'; -- 'C';
 v_as.obj_type :='XUP';
 v_as.sid := null;
 -- v_as.ZID := v_id;
 v_as.ZTYPE := 'X';
 v_as.ZLVL := null;
 v_as.proc := 'logonMD5';
 v_as := SAM.do_auditlog_enter(v_as);
 -- 0.1) copy parameters to v_r
 v_r.uid := null; -- v_uid;
 v_r.algo := 'MD5'; -- v_algo;
 v_r.iterations := 0; -- v_iterations;
 v_r.datefrom := NOW(); -- v_datefrom;
 v_r.dateto := null; -- v_dateto;
 v_r.password := v_password;
 v_r.salt := null; -- v_salt;
<<try>>
BEGIN
 -- 1) check isolation level
 IF NOT SAM.check_isolation() THEN
    v_es := SAM.make_es_transisolation(null,null); EXIT try;
 END IF;
-- -- 1.1) lock required tables
-- LOCK TABLE SAM.UserPassword IN EXCLUSIVE MODE;
-- -- 2) identify user
 v_user := sam.get_user();
 IF NOT v_user IS NULL THEN
    v_es := sam.make_execstatus(null,null,'E_USERSESSIONBOUND',v_user::varchar); EXIT try;
 END IF;
 -- 3) check is this db user allowed to logon as another user?
    -- will be implemented later via SAM.DBPoolUser table lookup
    -- or as checking some capability for user above
-- IF NOT SAM.check_capability('SAM_MANAGE') THEN
--   v_es := SAM.make_execstatus(null,null,'E_NOCAPABILITY','SAM_MANAGE','MAIN');
--   EXIT try; END IF;
--   v_as := SAM.do_auditlog_capuse_sole(v_as,'1','SAM_MANAGE',0);
 -- 4) check data
  -- resolve passed login to uid
  SELECT id,slevel into v_user,v_slevel from SAM.User where login = v_login and locked = 'N';
  IF NOT FOUND THEN
    v_es := sam.make_execstatus(null,null,'E_USERNOTGRANTED',v_login); EXIT try;
  END IF;
  IF v_password IS NULL THEN
    v_es := sam.make_execstatus(null,null,'E_NOTNULL','password','loginMD5'); EXIT try;
  END IF;
  IF v_secretcookie IS NULL THEN
    v_es := sam.make_execstatus(null,null,'E_NOTNULL','secretcookie','loginMD5'); EXIT try;
  END IF;
  IF length(v_secretcookie) < 16 THEN
    v_es := sam.make_execstatus(null,null,'E_SECRETTOOSHORT','16','loginMD5'); EXIT try;
  END IF;
  PERFORM FROM SAM.User XU, SAM.UserPassword XUP WHERE XU.id = v_user AND XU.id = XUP.uid
          AND XU.locked = 'N'
          AND XUP.password = v_password and XUP.algo = 'MD5' and iterations = 0 
          AND (dateto IS NULL or dateto < NOW());
  IF NOT FOUND THEN
    v_es := sam.make_execstatus(null,null,'E_USERINVALIDPASSWORD',v_login); EXIT try;
  END IF;
  -- 5. create session record and bind it to current postgresql session
   -- 5.1 create session
    PERFORM from SAM.UserSessionSeq where uid = v_user and ( usi>=usi_max);
    IF FOUND THEN
      v_es := sam.make_execstatus(null,null,'E_USERLOCKED',v_login || '(no more SAM.UserSessionSeq)'); EXIT try;
    END IF;
    -- session_id (usi) generation will be implemented later
    update SAM.UserSessionSeq set usi = usi + 1 where uid = v_user and ( usi<usi_max OR usi_max is null);
    IF NOT FOUND THEN
     INSERT INTO SAM.UserSessionSeq values(v_user,1,null,NOW());
     v_r_XUS.usi := 1;
    ELSE
     SELECT usi into v_r_XUS.usi from SAM.UserSessionSeq where uid = v_user and ( usi<=usi_max OR usi_max is null);
    END IF;
    IF v_r_XUS.usi IS NULL THEN
      v_es := sam.make_execstatus(null,null,'E_UNKNOWN','null usi while loginMD5'); EXIT try;
    END IF;
    --PERFORM pg_sleep(10); -- tested, locks after update works fine!
    --
   v_r_XUS.uid := v_user;
   --v_r_XUS.usi := 1; -- SIC! use SAM.UserSessionSeq
   v_r_XUS.datefrom := NOW();
   v_r_XUS.dateto := NOW() + INTERVAL '8 hour';
   v_r_XUS.secretcookie := v_secretcookie;
   v_r_XUS.servicefrom := session_user;
   v_r_XUS.devicefrom := v_devicefrom;
   INSERT INTO SAM.UserSession values(v_r_XUS.*); v_as.eaction := 'C';

   -- 5.2 create bind record
   v_r_XB.pg_pid := pg_backend_pid();
   v_r_XB.db_user := session_user;
   v_r_XB.uid := v_r_XUS.uid;
   v_r_XB.usi := v_r_XUS.usi;
   v_r_XB.slevel := v_slevel;
   v_r_XB.locked := 'N';
   v_r_XB.datefrom := v_r_XUS.datefrom;
   v_r_XB.dateto := v_r_XUS.dateto;
   v_r_XB.pg_s_time := pg_postmaster_start_time();
   v_r_XB.cl_addr := inet_client_addr();
   v_r_XB.cl_port := inet_client_port();
   -- remove stale binding (for instance: pg_s_time != pg_postmaster_start_time() or dateto < NOW() etc)
   DELETE from SAM.SessionBind where pg_pid = pg_backend_pid();
   -- add binding record from above configuration
   INSERT INTO SAM.SessionBind values(v_r_XB.*); v_as.eaction := 'C';
  -- 6) exit
 v_es := sam.make_execstatus(v_r_XB.uid,v_r_XB.usi,'I_SUCCESS');
EXCEPTION
 WHEN OTHERS THEN
	v_es:=sam.make_execstatus(null,null,'E_SQL',SQLSTATE,SQLERRM);
END;
-- FINALLY:
-- PERFORM SAM.do_auditlog_exit(v_as,v_es);
 RETURN v_es;
END $$;
-- GRANT/REVOKE moved to functions.grant/ directory COMMIT TRANSACTION;
COMMIT;
