-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION SAM.QC_Group(
	v_as	SAM.auditstate,
	v_r	SAM.Group
) RETURNS SAM.procstate VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 v_zid	bigint;
 i	integer;
BEGIN
 v_ps.success := FALSE;
<<try>>
BEGIN
 v_zid := v_r.id;
 -- 1) check NOT NULLs
 IF v_r.sid IS NULL THEN v_ps.e := SAM.make_es_notnull(v_zid,null,
	'SAM.Group','sid'); EXIT try; END IF;
 IF v_r.name IS NULL THEN v_ps.e := SAM.make_es_notnull(v_zid,null,
	'SAM.Group','name'); EXIT try; END IF;
 -- 2) check dictionary fields
 -- 3) check uniqueness
 SELECT COUNT(*) INTO i FROM SAM.Group WHERE name = v_r.name AND
	(id <> v_r.id OR v_r.id IS NULL);
 IF i <> 0 THEN
  v_ps.e :=SAM.make_execstatus(v_zid,null,'E_DUPOBJECT','SAM.Group',
  'name',v_r.name);
  EXIT try; END IF;
 -- 4) check references
  IF v_r.sid <> 0 THEN
   SELECT COUNT(*) INTO i FROM SAM.Scope WHERE id = v_r.sid;
   IF i = 0 THEN v_ps.e := SAM.make_execstatus(v_zid,null,'E_NOSCOPE',
	CAST(v_r.sid AS text)); EXIT try; END IF;
  END IF;
 -- 99) all tests passed
 v_ps.success := TRUE;
END;
-- FINALLY:
 v_ps.a := v_as;
 RETURN v_ps;
END $$;
REVOKE ALL ON FUNCTION SAM.QC_Group(
	v_as	SAM.auditstate,
	v_r	SAM.Group
	) FROM PUBLIC;
COMMIT TRANSACTION;
