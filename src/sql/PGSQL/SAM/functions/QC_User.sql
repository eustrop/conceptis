-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION SAM.QC_User(
	v_as	SAM.auditstate,
	v_r	SAM.User
) RETURNS SAM.procstate VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 v_zid	bigint;
 i	integer;
BEGIN
 v_ps.success := FALSE;
<<try>>
BEGIN
 v_zid := v_r.id;
 -- 1) check NOT NULLs
 IF v_r.sid IS NULL THEN v_ps.e := SAM.make_es_notnull(v_zid,null,
	'SAM.User','sid'); EXIT try; END IF;
 IF v_r.slevel IS NULL THEN v_ps.e := SAM.make_es_notnull(v_zid,null,
	'SAM.User','slevel'); EXIT try; END IF;
 IF v_r.locked IS NULL THEN v_ps.e := SAM.make_es_notnull(v_zid,null,
	'SAM.User','locked'); EXIT try; END IF;
 IF v_r.login IS NULL THEN v_ps.e := SAM.make_es_notnull(v_zid,null,
	'SAM.User','login'); EXIT try; END IF;
 -- 2) check dictionary fields
 IF NOT dic.check_code('YESNO',v_r.locked) THEN
   v_ps.e := SAM.make_es_invalidcode(v_zid,null,v_r.locked,
   'SAM.User.locked','YESNO'); EXIT try; END IF;
 IF NOT dic.check_code_nula('LANG',v_r.lang) THEN
   v_ps.e := SAM.make_es_invalidcode(v_zid,null,v_r.lang,
   'SAM.User.lang','LANG'); EXIT try; END IF;
 IF NOT dic.check_code('SLEVEL',CAST(v_r.slevel as text)) THEN
   v_ps.e := SAM.make_es_invalidcode(v_zid,null,CAST(v_r.slevel as text),
   'SAM.User.slevel','SLEVEL'); EXIT try; END IF;
 -- 3) check uniqueness
 SELECT COUNT(*) INTO i FROM SAM.User WHERE login = v_r.login AND
	(id <> v_r.id OR v_r.id IS NULL);
 IF i <> 0 THEN
  v_ps.e :=SAM.make_execstatus(v_zid,null,'E_DUPOBJECT','SAM.User',
  'login',v_r.login);
  EXIT try; END IF;
 SELECT COUNT(*) INTO i FROM SAM.User WHERE db_user = v_r.db_user AND
	(id <> v_r.id OR v_r.id IS NULL);
 IF i <> 0 THEN
  v_ps.e :=SAM.make_execstatus(v_zid,null,'E_DUPOBJECT','SAM.User',
  'db_user',v_r.db_user);
  EXIT try; END IF;
 -- 4) check references
  IF v_r.sid <> 0 THEN
   SELECT COUNT(*) INTO i FROM SAM.Scope WHERE id = v_r.sid;
   IF i = 0 THEN v_ps.e := SAM.make_execstatus(v_zid,null,'E_NOSCOPE',
	CAST(v_r.sid AS text)); EXIT try; END IF;
  END IF;
 -- 99) all tests passed
 v_ps.success := TRUE;
END;
-- FINALLY:
 v_ps.a := v_as;
 RETURN v_ps;
END $$;
REVOKE ALL ON FUNCTION SAM.QC_User(
	v_as	SAM.auditstate,
	v_r	SAM.User
	) FROM PUBLIC;
COMMIT TRANSACTION;
