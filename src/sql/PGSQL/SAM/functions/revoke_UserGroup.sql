-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION SAM.revoke_UserGroup(
	v_uid	bigint,
	v_gid	bigint
	)
 RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_as SAM.auditstate;
 v_es SAM.execstatus;
 v_user bigint;
 i bigint; --couner
 v_zid bigint;
BEGIN
 -- v_es:=sam.make_execstatus('E_NOTIMPLEMENTED');
 -- 0) enter into procedure
 v_as.subsys = 'SAM';
 v_as.eaction :='D';
 v_as.obj_type :='XUC';
 v_as.ZLVL := null; -- not applicable for SAM.UserGroup
 v_as.ZID := v_uid; 
 -- v_as.sid := v_sid; 
 SELECT sid INTO v_as.sid from SAM.V_User where id = v_uid;
 v_as.ZTYPE := 'X';
  -- v_as.sid/ZLVL not applicable or unknown 
 v_as.proc := 'revoke_UserGroup';
 v_as := SAM.do_auditlog_enter(v_as);
<<try>>
BEGIN
 -- 1) check isolation level
 IF NOT SAM.check_isolation() THEN
    v_es := SAM.make_es_transisolation(null,null); EXIT try;
 END IF;
 -- 1.1) lock requared tables
 LOCK TABLE SAM.UserGroup IN EXCLUSIVE MODE;
 -- 2) identify user
 v_user := sam.get_user();
 IF v_user IS NULL THEN
    v_es := sam.make_execstatus(null,null,'E_NOUSER',session_user); EXIT try;
 END IF;
 -- 3) check capability
 IF NOT SAM.check_capability('SAM_MANAGE') THEN
   v_es := SAM.make_execstatus(null,null,'E_NOCAPABILITY','SAM_MANAGE','MAIN');
   EXIT try; END IF;
   v_as := SAM.do_auditlog_capuse_sole(v_as,'1','SAM_MANAGE',0);
 -- 4) check data
 -- 4.1) check for record's existence
 v_zid := v_uid;
 -- 4.1) check NOT NULLs
 IF v_uid IS NULL THEN v_es := SAM.make_es_notnull(v_zid,null,
	'SAM.UserGroup','uid'); EXIT try; END IF;
 IF v_gid IS NULL THEN v_es := SAM.make_es_notnull(v_zid,null,
	'SAM.UserGroup','gid'); EXIT try; END IF;
 -- 4.2) check dictionary fields
  -- not necessary
 -- 4.3) check existence
 PERFORM * FROM SAM.UserGroup WHERE uid = v_uid AND gid = v_gid;
 IF NOT FOUND THEN
  v_es :=SAM.make_execstatus(v_zid,null,'E_NORECORD','SAM.UserGroup',
  '(uid,gid)','('||v_uid||','||v_gid||')');
  EXIT try;
 END IF;
 -- 5) delete record
 DELETE FROM SAM.UserGroup WHERE uid = v_uid AND gid = v_gid;
 v_as := SAM.do_auditlog_da_sole(v_as,'1',null,null,v_as.sid,null,
  'revoke gid='||v_gid||' from uid='||v_uid);
 -- 6) exit
 v_es := sam.make_execstatus(v_uid,null,'I_SUCCESS');
EXCEPTION
 WHEN OTHERS THEN
	v_es:=sam.make_execstatus(v_uid,null,'E_SQL',SQLSTATE,SQLERRM);
END;
-- FINALLY:
 PERFORM SAM.do_auditlog_exit(v_as,v_es);
 RETURN v_es;
END $$;
--GRANT EXECUTE ON FUNCTION SAM.revoke_UserGroup(
--	v_uid	bigint,
--	v_gid	bigint
--	) TO tis_users;
REVOKE ALL ON FUNCTION SAM.revoke_UserGroup(
	v_uid	bigint,
	v_gid	bigint
	) FROM PUBLIC;
COMMIT TRANSACTION;
