-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION SAM.QC_UserPassword(
	v_as	SAM.auditstate,
	v_r	SAM.UserPassword
) RETURNS SAM.procstate VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 v_zid	bigint;
 i	integer;
BEGIN
 v_ps.success := FALSE;
<<try>>
BEGIN
 v_zid := null; -- not applicable for SAM.UserPassword
 -- 1) check NOT NULLs
 IF v_r.uid IS NULL THEN v_ps.e := SAM.make_es_notnull(v_zid,null,
	'SAM.UserPassword','uid'); EXIT try; END IF;
 IF v_r.algo IS NULL THEN v_ps.e := SAM.make_es_notnull(v_zid,null,
	'SAM.UserPassword','algo'); EXIT try; END IF;
 IF v_r.iterations IS NULL THEN v_ps.e := SAM.make_es_notnull(v_zid,null,
	'SAM.UserPassword','iterations'); EXIT try; END IF;
 IF v_r.datefrom IS NULL THEN v_ps.e := SAM.make_es_notnull(v_zid,null,
	'SAM.UserPassword','datefrom'); EXIT try; END IF;
 -- 2) check dictionary fields
 IF NOT dic.check_code('DIGEST_ALGO',v_r.algo) THEN
   v_ps.e := SAM.make_es_invalidcode(v_zid,null,v_r.algo,
   'SAM.UserPassword.algo','DIGEST_ALGO'); EXIT try; END IF;
 -- 3) check uniqueness
 -- 4) check references
 -- 99) all tests passed
 v_ps.success := TRUE;
END;
-- FINALLY:
 v_ps.a := v_as;
 RETURN v_ps;
END $$;
REVOKE ALL ON FUNCTION SAM.QC_UserPassword(
	v_as	SAM.auditstate,
	v_r	SAM.UserPassword
	) FROM PUBLIC;
COMMIT TRANSACTION;
