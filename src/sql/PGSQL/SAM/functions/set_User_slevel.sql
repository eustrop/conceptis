-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- purpose: set user's slevel

BEGIN;
CREATE OR REPLACE FUNCTION SAM.set_User_slevel(v_uid bigint, v_slevel smallint)
  RETURNS SAM.execstatus LANGUAGE SQL SECURITY DEFINER AS $$
  SELECT SAM.update_user(id,sid,$2,locked,lang,login,db_user,full_name)
  FROM SAM.User WHERE id = $1;
$$;
REVOKE ALL ON FUNCTION SAM.set_User_slevel(bigint,smallint) FROM PUBLIC;
--GRANT EXECUTE ON FUNCTION SAM.set_User_slevel(bigint,smallint) to tis_users; 
COMMIT;
