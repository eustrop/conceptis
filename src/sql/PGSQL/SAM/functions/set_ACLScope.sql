-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION SAM.set_ACLScope(
	v_gid	bigint,
	v_sid	bigint,
	v_obj_type	varchar(20),
	v_writea	char(1),
	v_createa	char(1),
	v_deletea	char(1),
	v_reada	char(1),
	v_locka	char(1),
	v_historya	char(1)
) RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 -- v_ps SAM.procstate;
 v_as SAM.auditstate;
 v_es SAM.execstatus;
 v_r SAM.ACLScope%ROWTYPE;
 v_ro SAM.ACLScope%ROWTYPE;
 v_eventmsg text;
 v_user bigint;
 v_zid bigint;
 i bigint;
BEGIN
 -- 0) enter into procedure
 v_as.subsys := 'SAM';
 v_as.eaction :='U';
 v_as.obj_type :='XAS';
 v_as.sid := v_sid;
 v_as.ZID := v_gid;
 v_as.ZTYPE := 'X';
 v_as.ZLVL := null;
 v_as.proc := 'set_ACLScope';
 v_as := SAM.do_auditlog_enter(v_as);
 -- 0.1) copy parameters to v_r
 v_r.gid := v_gid;
 v_r.sid := v_sid;
 v_r.obj_type := v_obj_type;
 v_r.writea := v_writea;
 v_r.createa := v_createa;
 v_r.deletea := v_deletea;
 v_r.reada := v_reada;
 v_r.locka := v_locka;
 v_r.historya := v_historya;
<<try>>
BEGIN
 -- 1) check isolation level
 IF NOT SAM.check_isolation() THEN
    v_es := SAM.make_es_transisolation(null,null); EXIT try;
 END IF;
 -- 1.1) lock required tables
 LOCK TABLE SAM.ACLScope IN EXCLUSIVE MODE;
 -- 2) identify user
 v_user := sam.get_user();
 IF v_user IS NULL THEN
    v_es := sam.make_execstatus(null,null,'E_NOUSER',session_user); EXIT try;
 END IF;
 -- 3) check capability
 IF NOT SAM.check_capability('SAM_MANAGE') THEN
   v_es := SAM.make_execstatus(null,null,'E_NOCAPABILITY','SAM_MANAGE','MAIN');
   EXIT try; END IF;
   v_as := SAM.do_auditlog_capuse_sole(v_as,'1','SAM_MANAGE',0);
 -- 4) check data
 v_zid := v_gid; -- this ACL could be considered as part of SAM.group
 -- 4.1) check NOT NULLs
 IF v_r.gid IS NULL THEN v_es := SAM.make_es_notnull(v_zid,null,
	'SAM.ACLScope','gid'); EXIT try; END IF;
 IF v_r.sid IS NULL THEN v_es := SAM.make_es_notnull(v_zid,null,
	'SAM.ACLScope','sid'); EXIT try; END IF;
 IF v_r.obj_type IS NULL THEN v_es := SAM.make_es_notnull(v_zid,null,
	'SAM.ACLScope','obj_type'); EXIT try; END IF;
 IF v_r.writea IS NULL THEN v_es := SAM.make_es_notnull(v_zid,null,
	'SAM.ACLScope','writea'); EXIT try; END IF;
 IF v_r.createa IS NULL THEN v_es := SAM.make_es_notnull(v_zid,null,
	'SAM.ACLScope','createa'); EXIT try; END IF;
 IF v_r.deletea IS NULL THEN v_es := SAM.make_es_notnull(v_zid,null,
	'SAM.ACLScope','deletea'); EXIT try; END IF;
 IF v_r.reada IS NULL THEN v_es := SAM.make_es_notnull(v_zid,null,
	'SAM.ACLScope','reada'); EXIT try; END IF;
 IF v_r.locka IS NULL THEN v_es := SAM.make_es_notnull(v_zid,null,
	'SAM.ACLScope','locka'); EXIT try; END IF;
 IF v_r.historya IS NULL THEN v_es := SAM.make_es_notnull(v_zid,null,
	'SAM.ACLScope','historya'); EXIT try; END IF;
 -- 4.2) check dictionary fields
 IF NOT dic.check_code('ACL_OBJECT_TYPE',v_r.obj_type) THEN
   v_es := SAM.make_es_invalidcode(v_zid,null,v_r.obj_type,
   'SAM.ACLScope.obj_type','ACL_OBJECT_TYPE'); EXIT try; END IF;
 IF NOT dic.check_code('ACLA',v_r.writea) THEN
   v_es := SAM.make_es_invalidcode(v_zid,null,v_r.writea,
   'SAM.ACLScope.writea','ACLA'); EXIT try; END IF;
 IF NOT dic.check_code('ACLA',v_r.createa) THEN
   v_es := SAM.make_es_invalidcode(v_zid,null,v_r.createa,
   'SAM.ACLScope.createa','ACLA'); EXIT try; END IF;
 IF NOT dic.check_code('ACLA',v_r.deletea) THEN
   v_es := SAM.make_es_invalidcode(v_zid,null,v_r.deletea,
   'SAM.ACLScope.deletea','ACLA'); EXIT try; END IF;
 IF NOT dic.check_code('ACLA',v_r.reada) THEN
   v_es := SAM.make_es_invalidcode(v_zid,null,v_r.reada,
   'SAM.ACLScope.reada','ACLA'); EXIT try; END IF;
 IF NOT dic.check_code('ACLA',v_r.locka) THEN
   v_es := SAM.make_es_invalidcode(v_zid,null,v_r.locka,
   'SAM.ACLScope.locka','ACLA'); EXIT try; END IF;
 IF NOT dic.check_code('ACLA',v_r.historya) THEN
   v_es := SAM.make_es_invalidcode(v_zid,null,v_r.historya,
   'SAM.ACLScope.historya','ACLA'); EXIT try; END IF;
 -- 4.3) check uniqueness
  -- not applicable
 -- 4.4) check references
  SELECT COUNT(*) INTO i FROM SAM.Scope WHERE id = v_r.sid;
   IF i = 0 THEN v_es := SAM.make_execstatus(v_zid,null,'E_NOSCOPE',
	CAST(v_r.sid AS text)); EXIT try; END IF;
  SELECT COUNT(*) INTO i FROM SAM.Group WHERE id = v_r.gid;
   IF i = 0 THEN v_es := SAM.make_execstatus(v_zid,null,'E_NOBJECT',
	'SAM.Group','id',CAST(v_r.gid AS text)); EXIT try; END IF;
 -- 5) add or update record
 -- 5.1) get current record
 SELECT * INTO v_ro from SAM.ACLScope
   WHERE gid = v_r.gid AND sid = v_r.sid AND obj_type = v_r.obj_type;
 IF FOUND THEN
  v_eventmsg := COALESCE( v_ro.writea,'-')||COALESCE(v_ro.createa,'-')||
   COALESCE( v_ro.deletea,'-')||COALESCE(v_ro.reada,'-')||
   COALESCE(v_ro.locka,'-')||COALESCE(v_ro.historya,'-');
 ELSE
  v_eventmsg := NULL; -- this is defaults but be paranoid
 END IF;
 -- 5.2) update current record
 UPDATE SAM.ACLScope SET (writea,createa, deletea,reada,locka,historya) =
   (v_r.writea,v_r.createa, v_r.deletea,v_r.reada,v_r.locka,v_r.historya)
   WHERE gid = v_r.gid AND sid = v_r.sid AND obj_type = v_r.obj_type;
 -- 5.3) create new record if no one
 IF NOT FOUND THEN
  INSERT INTO SAM.ACLScope values(v_r.*); v_as.eaction := 'C';
 END IF;
 v_as := SAM.do_auditlog_da_sole(v_as,'1',null,null,v_r.sid,null,
   'ACLScope:'||v_r.gid||','||
   v_r.sid||','||v_r.obj_type||':'||COALESCE(v_eventmsg,'<NONE>')||'->'||
   v_r.writea||v_r.createa|| v_r.deletea||v_r.reada||v_r.locka||v_r.historya);
 -- 6) exit
 v_es := sam.make_execstatus(v_r.gid,null,'I_SUCCESS');
EXCEPTION
 WHEN OTHERS THEN
	v_es:=sam.make_execstatus(v_r.gid,null,'E_SQL',SQLSTATE,SQLERRM);
END;
-- FINALLY:
 PERFORM SAM.do_auditlog_exit(v_as,v_es);
 RETURN v_es;
END $$;
COMMIT TRANSACTION;
