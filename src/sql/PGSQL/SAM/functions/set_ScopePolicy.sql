-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION SAM.set_ScopePolicy(
	v_sid	bigint,
	v_obj_type	varchar(14),
	v_maxcount	bigint,
	v_doaudit	char(1),
	v_dodebug	char(1)
) RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 v_as SAM.auditstate;
 v_es SAM.execstatus;
 v_r SAM.ScopePolicy%ROWTYPE;
 v_ro SAM.ScopePolicy%ROWTYPE;
 v_user bigint;
 i bigint; -- counter
 v_zid bigint;
 v_eventmsg text;
BEGIN
 -- 0) enter into procedure
 v_as.subsys = 'SAM';
 v_as.eaction :='U';
 v_as.obj_type :='XSP';
 v_as.sid := v_sid;
 v_as.ZID := null;
 v_as.ZTYPE := 'X';
 v_as.ZLVL := null; -- unknown at this moment
 v_as.proc := 'set_ScopePolicy';
 v_as := SAM.do_auditlog_enter(v_as);
 -- 0.1) copy parameters to v_r
 v_r.sid := v_sid;
 v_r.obj_type := v_obj_type;
 v_r.maxcount := v_maxcount;
 -- v_r.ocount := v_ocount; -- std editing unallowed
 v_r.doaudit := v_doaudit;
 v_r.dodebug := v_dodebug;
<<try>>
BEGIN
 -- 1) check isolation level
 IF NOT SAM.check_isolation() THEN
    v_es := SAM.make_es_transisolation(null,null); EXIT try;
 END IF;
 -- 1.1) lock required tables
 LOCK TABLE SAM.ScopePolicy IN EXCLUSIVE MODE;
 -- 2) identify user
 v_user := sam.get_user();
 IF v_user IS NULL THEN
    v_es := sam.make_execstatus(null,null,'E_NOUSER',session_user); EXIT try;
 END IF;
 -- 3) check capability
 IF NOT SAM.check_capability('SAM_MANAGE') THEN
   v_es :=SAM.make_execstatus(null,null,'E_NOCAPABILITY','SAM_MANAGE','MAIN');
   EXIT try; END IF;
   v_as := SAM.do_auditlog_capuse_sole(v_as,'1','SAM_MANAGE',0);
 -- 4) check data
 v_zid := v_r.sid;
 -- 4.1) check NOT NULLs
 IF v_r.sid IS NULL THEN v_es := SAM.make_es_notnull(v_zid,null,
	'SAM.ScopePolicy','sid'); EXIT try; END IF;
 IF v_r.obj_type IS NULL THEN v_es := SAM.make_es_notnull(v_zid,null,
	'SAM.ScopePolicy','obj_type'); EXIT try; END IF;
 IF v_r.doaudit IS NULL THEN v_es := SAM.make_es_notnull(v_zid,null,
	'SAM.ScopePolicy','doaudit'); EXIT try; END IF;
 IF v_r.dodebug IS NULL THEN v_es := SAM.make_es_notnull(v_zid,null,
	'SAM.ScopePolicy','dodebug'); EXIT try; END IF;
 -- 4.2) check dictionary fields
 IF NOT dic.check_code('TIS_OBJECT_TYPE',v_r.obj_type) THEN
   v_es := SAM.make_es_invalidcode(v_zid,null,v_r.obj_type,
   'SAM.ScopePolicy.obj_type','TIS_OBJECT_TYPE'); EXIT try; END IF;
 IF NOT dic.check_code('YESNO',v_r.doaudit) THEN
   v_es := SAM.make_es_invalidcode(v_zid,null,v_r.doaudit,
   'SAM.ScopePolicy.doaudit','YESNO'); EXIT try; END IF;
 IF NOT dic.check_code('YESNO',v_r.dodebug) THEN
   v_es := SAM.make_es_invalidcode(v_zid,null,v_r.dodebug,
   'SAM.ScopePolicy.dodebug','YESNO'); EXIT try; END IF;
 -- 4.3) check uniqueness
 -- 4.4) check references
 SELECT COUNT(*) INTO i FROM SAM.Scope WHERE id = v_r.sid;
   IF i = 0 THEN v_es := SAM.make_execstatus(v_zid,null,'E_NOSCOPE',
	CAST(v_r.sid AS text)); EXIT try; END IF;
 -- 5) add or update record
 SELECT * INTO v_ro  FROM SAM.ScopePolicy
   WHERE sid = v_r.sid and obj_type = v_r.obj_type;
 IF FOUND THEN
  v_eventmsg := 'max='||COALESCE(''||v_ro.maxcount,'<NULL>')||','||
  COALESCE(v_ro.doaudit,'-')||','||COALESCE(v_ro.dodebug,'-');
 END iF;
 -- 5.1) update
 UPDATE SAM.ScopePolicy SET (maxcount,doaudit,dodebug)
   = (v_r.maxcount,v_r.doaudit,v_r.dodebug)
   WHERE sid = v_r.sid and obj_type = v_r.obj_type;
 -- 5.1) create
 IF NOT FOUND THEN
  INSERT INTO SAM.ScopePolicy values(v_r.*); v_as.eaction := 'C';
 END IF;
 v_as := SAM.do_auditlog_da_sole(v_as,'1',null,null,v_r.sid,null,
   'ScopePolicy:'|| v_r.sid||','||v_r.obj_type||':'||
   COALESCE(v_eventmsg,'<NONE>')||'->'||
   'max='||COALESCE(''||v_r.maxcount,'<NULL>')||','||
    COALESCE(v_r.doaudit,'-')||','||COALESCE(v_r.dodebug,'-'));
 -- 6) exit
 v_es := sam.make_execstatus(v_r.sid,null,'I_SUCCESS');
EXCEPTION
 WHEN OTHERS THEN
	v_es:=sam.make_execstatus(v_r.sid,null,'E_SQL',SQLSTATE,SQLERRM);
END;
-- FINALLY:
 PERFORM SAM.do_auditlog_exit(v_as,v_es);
 RETURN v_es;
END $$;
COMMIT TRANSACTION;
