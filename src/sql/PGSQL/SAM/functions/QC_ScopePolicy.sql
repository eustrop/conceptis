-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION SAM.QC_ScopePolicy(
	v_as	SAM.auditstate,
	v_r	SAM.ScopePolicy
) RETURNS SAM.procstate VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 v_zid	bigint;
 i	integer;
BEGIN
 v_ps.success := FALSE;
<<try>>
BEGIN
 v_zid := null; -- not applicable for SAM.ScopePolicy
 -- 1) check NOT NULLs
 IF v_r.sid IS NULL THEN v_ps.e := SAM.make_es_notnull(v_zid,null,
	'SAM.ScopePolicy','sid'); EXIT try; END IF;
 IF v_r.obj_type IS NULL THEN v_ps.e := SAM.make_es_notnull(v_zid,null,
	'SAM.ScopePolicy','obj_type'); EXIT try; END IF;
 IF v_r.doaudit IS NULL THEN v_ps.e := SAM.make_es_notnull(v_zid,null,
	'SAM.ScopePolicy','doaudit'); EXIT try; END IF;
 IF v_r.dodebug IS NULL THEN v_ps.e := SAM.make_es_notnull(v_zid,null,
	'SAM.ScopePolicy','dodebug'); EXIT try; END IF;
 -- 2) check dictionary fields
 IF NOT dic.check_code('OBJECTS',v_r.obj_type) THEN
   v_ps.e := SAM.make_es_invalidcode(v_zid,null,v_r.obj_type,
   'SAM.ScopePolicy.obj_type','OBJECTS'); EXIT try; END IF;
 IF NOT dic.check_code('YESNO',v_r.doaudit) THEN
   v_ps.e := SAM.make_es_invalidcode(v_zid,null,v_r.doaudit,
   'SAM.ScopePolicy.doaudit','YESNO'); EXIT try; END IF;
 IF NOT dic.check_code('YESNO',v_r.dodebug) THEN
   v_ps.e := SAM.make_es_invalidcode(v_zid,null,v_r.dodebug,
   'SAM.ScopePolicy.dodebug','YESNO'); EXIT try; END IF;
 -- 3) check uniqueness
 -- 4) check references
  IF v_r.sid <> 0 THEN
   SELECT COUNT(*) INTO i FROM SAM.Scope WHERE id = v_r.sid;
   IF i = 0 THEN v_ps.e := SAM.make_execstatus(v_zid,null,'E_NOSCOPE',
	CAST(v_r.sid AS text)); EXIT try; END IF;
  END IF;
 -- 99) all tests passed
 v_ps.success := TRUE;
END;
-- FINALLY:
 v_ps.a := v_as;
 RETURN v_ps;
END $$;
COMMIT TRANSACTION;
