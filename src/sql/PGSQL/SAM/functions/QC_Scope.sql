-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION SAM.QC_Scope(
	v_as	SAM.auditstate,
	v_r	SAM.Scope
) RETURNS SAM.procstate VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 v_zid	bigint;
 i	integer;
BEGIN
 v_ps.success := FALSE;
<<try>>
BEGIN
 v_zid := v_r.id;
 -- 1) check NOT NULLs
 IF v_r.name IS NULL THEN v_ps.e := SAM.make_es_notnull(v_zid,null,
	'SAM.Scope','name'); EXIT try; END IF;
 IF v_r.is_local IS NULL THEN v_ps.e := SAM.make_es_notnull(v_zid,null,
	'SAM.Scope','is_local'); EXIT try; END IF;
 IF v_r.auditlvl IS NULL THEN v_ps.e := SAM.make_es_notnull(v_zid,null,
	'SAM.Scope','auditlvl'); EXIT try; END IF;
 -- 2) check dictionary fields
 IF NOT dic.check_code('YESNO',v_r.is_local) THEN
   v_ps.e := SAM.make_es_invalidcode(v_zid,null,v_r.is_local,
   'SAM.Scope.is_local','YESNO'); EXIT try; END IF;
 IF NOT dic.check_code('EVENT_CLASS',v_r.auditlvl) THEN
   v_ps.e := SAM.make_es_invalidcode(v_zid,null,v_r.auditlvl,
   'SAM.Scope.auditlvl','EVENT_CLASS'); EXIT try; END IF;
 -- 3) check uniqueness
 SELECT COUNT(*) INTO i FROM SAM.Scope WHERE name = v_r.name AND
	(id <> v_r.id OR v_r.id IS NULL);
 IF i <> 0 THEN
  v_ps.e :=SAM.make_execstatus(v_zid,null,'E_DUPOBJECT','SAM.Scope',
  'name',v_r.name);
  EXIT try; END IF;
 -- 4) check references
 -- 99) all tests passed
 v_ps.success := TRUE;
END;
-- FINALLY:
 v_ps.a := v_as;
 RETURN v_ps;
END $$;
REVOKE ALL ON FUNCTION SAM.QC_Scope(
	v_as	SAM.auditstate,
	v_r	SAM.Scope
	) FROM PUBLIC;
COMMIT TRANSACTION;
