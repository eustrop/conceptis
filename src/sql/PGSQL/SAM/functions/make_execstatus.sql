-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION SAM.make_execstatus(
  v_ZID bigint,v_ZVER bigint,v_errcode varchar(32),v_p1 text,v_p2 text,v_p3 text
  ) RETURNS SAM.execstatus LANGUAGE plpgSQL SECURITY DEFINER STABLE AS $$
DECLARE
 v_ZEM dic.ZErrorMsg%ROWTYPE;
 v_es SAM.execstatus;
BEGIN
 v_es.ZID:=v_ZID; v_es.ZVER := v_ZVER; v_es.errcode := v_errcode;
 v_ZEM.lang := COALESCE(sam.get_user_lang(),'EN');
 -- optimization for performance on success
 IF v_errcode = 'I_SUCCESS' THEN
  v_es.errnum:=0; v_es.errdesc ='Ok';
  RETURN v_es;
 END IF;
 -- find error message
 SELECT * from dic.ZErrorMsg AS ZEM INTO v_ZEM WHERE
  ZEM.errcode = v_errcode and ZEM.lang = v_ZEM.lang;
 -- v_es.p1 := v_p1; v_es.p2 := v_p2; v_es.p3 := v_p3;
 IF NOT FOUND THEN
 SELECT * from dic.ZErrorMsg AS ZEM INTO v_ZEM WHERE
  ZEM.errcode = v_errcode and ZEM.lang = '*';
 END IF;
 IF NOT v_ZEM.errcode IS NULL THEN
  v_es.errnum:=v_ZEM.errnum; v_es.errdesc := v_ZEM.errmsg;
  v_es.errdesc := replace(v_es.errdesc,'%1',COALESCE(v_p1,'%1'));
  v_es.errdesc := replace(v_es.errdesc,'%2',COALESCE(v_p2,'%2'));
  v_es.errdesc := replace(v_es.errdesc,'%3',COALESCE(v_p3,'%3'));
 ELSE
  v_es.errnum:=2; v_es.errdesc:=v_es.errcode;
 END IF;
 RETURN v_es;
END $$;
REVOKE ALL ON  FUNCTION SAM.make_execstatus(
  v_ZID bigint,v_ZVER bigint,v_errcode varchar(32),v_p1 text,v_p2 text,v_p3 text
  ) FROM PUBLIC;
COMMIT TRANSACTION;
