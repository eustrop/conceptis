-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION SAM.do_MNTLog( v_msg	varchar(1024)) RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_es SAM.execstatus;
 v_r SAM.MNTLog%ROWTYPE;
 v_user bigint;
BEGIN
 -- 0) enter into procedure
 -- do not do auditlog
 -- 0.1) copy parameters to v_r
 v_r.ts := NOW();
 --v_r.uid := v_uid; -- identified later
 v_r.dbuser := SESSION_USER;
 v_r.msg := v_msg;
<<try>>
BEGIN
 -- 1) check isolation level
 IF NOT SAM.check_isolation() THEN
    v_es := SAM.make_es_transisolation(null,null); EXIT try;
 END IF;
 -- 1.1) lock required tables
 -- LOCK TABLE SAM.MNTLog IN EXCLUSIVE MODE;
 -- 2) identify user
 v_user := sam.get_user();
 IF v_user IS NULL THEN
    v_es := sam.make_execstatus(null,null,'E_NOUSER',session_user); EXIT try;
 END IF;
 v_r.uid = v_user;
 v_r.dbuser = SESSION_USER;
 -- 3) check capability
 IF NOT SAM.check_capability('SAM_MANAGE') THEN
   v_es := SAM.make_execstatus(null,null,'E_NOCAPABILITY','SAM_MANAGE','MAIN');
   EXIT try; END IF;
 -- 4) check data
  -- accept any data
 -- 5) add record
 INSERT INTO SAM.MNTLog values(v_r.*);
 -- 6) exit
 v_es := sam.make_execstatus(null,null,'I_SUCCESS');
EXCEPTION
 WHEN OTHERS THEN
	v_es:=sam.make_execstatus(null,null,'E_SQL',SQLSTATE,SQLERRM);
END;
-- FINALLY:
 RETURN v_es;
END $$;
-- GRANT/REVOKE moved to functions.grant/ directory COMMIT TRANSACTION;
COMMIT;
