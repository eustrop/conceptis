-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION SAM.delete_Scope(v_id bigint)
 RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_as SAM.auditstate;
 v_es SAM.execstatus;
 v_user bigint;
 i bigint; --couner
BEGIN
 -- v_es:=sam.make_execstatus('E_NOTIMPLEMENTED');
 -- 0) enter into procedure
 v_as.subsys = 'SAM';
 v_as.eaction :='D';
 v_as.obj_type :='XS';
 v_as.sid := v_id;
 v_as.ZLVL := null; -- not applicable for SAM.Scope
 v_as.ZID := v_id;
 v_as.ZTYPE := 'X';
  -- v_as.sid/ZLVL not applicable or unknown 
 v_as.proc := 'delete_Scope';
 v_as := SAM.do_auditlog_enter(v_as);
<<try>>
BEGIN
 -- 1) check isolation level
 IF NOT SAM.check_isolation() THEN
    v_es := SAM.make_es_transisolation(null,null); EXIT try;
 END IF;
 -- 1.1) lock requared tables
 LOCK TABLE SAM.Scope IN EXCLUSIVE MODE;
 -- 2) identify user
 v_user := sam.get_user();
 IF v_user IS NULL THEN
    v_es := sam.make_execstatus(null,null,'E_NOUSER',session_user); EXIT try;
 END IF;
 -- 3) check capability
 IF NOT SAM.check_capability('SAM_MANAGE') THEN
   v_es := SAM.make_execstatus(null,null,'E_NOCAPABILITY','SAM_MANAGE','MAIN');
   EXIT try; END IF;
   v_as := SAM.do_auditlog_capuse_sole(v_as,'1','SAM_MANAGE',0);
 -- 4) check data
 -- 4.1) check for record's existence
 SELECT count(*) INTO i FROM SAM.Scope  WHERE id = v_id;
 IF i = 0 THEN
   v_es := sam.make_execstatus(v_id,null,'E_NOBJECT','SAM.Scope',
   'id',CAST(v_id as text)); EXIT try;
 END IF;
 IF i <> 1 THEN
   v_es := sam.make_execstatus(v_id,null,'E_NONSOLEOBJECT','SAM.Scope',
   'id',CAST(v_id as text)); EXIT try;
 END IF;
 -- 5) delete record
 DELETE FROM SAM.Scope WHERE
   id = v_id;
 v_as := SAM.do_auditlog_da_sole(v_as,'1',null,null,null,v_id);
 -- 6) exit
 v_es := sam.make_execstatus(v_id,null,'I_SUCCESS');
EXCEPTION
 WHEN OTHERS THEN
	v_es:=sam.make_execstatus(v_id,null,'E_SQL',SQLSTATE,SQLERRM);
END;
-- FINALLY:
 PERFORM SAM.do_auditlog_exit(v_as,v_es);
 RETURN v_es;
END $$;
REVOKE ALL ON FUNCTION SAM.delete_Scope(v_id bigint) FROM PUBLIC;
--GRANT EXECUTE ON FUNCTION SAM.delete_Scope(v_id bigint) TO tis_users;
COMMIT TRANSACTION;
