-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION SAM.create_Scope(
	v_id	bigint,
	v_sid	bigint,
	v_slevel_min	smallint,
	v_slevel_max	smallint,
	v_name	varchar(64),
	v_is_local	char(1),
	v_auditlvl	char(1),
	v_descr	varchar(255)
) RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 v_as SAM.auditstate;
 v_es SAM.execstatus;
 v_r SAM.Scope%ROWTYPE;
 v_user bigint;
 i bigint; --couner
BEGIN
 -- 0) enter into procedure
 v_as.subsys := 'SAM';
 v_as.eaction :='C';
 v_as.obj_type :='XS';
 v_as.sid := v_id;
 -- v_as.ZID := v_id;
 v_as.ZTYPE := 'X';
 v_as.ZLVL := null;
 v_as.proc := 'create_Scope';
 v_as := SAM.do_auditlog_enter(v_as);
 -- 0.1) copy parameters to v_r
 v_r.id := v_id;
 v_r.sid := v_sid;
 v_r.slevel_min := v_slevel_min;
 v_r.slevel_max := v_slevel_max;
 v_r.name := v_name;
 v_r.is_local := v_is_local;
 v_r.auditlvl := v_auditlvl;
 -- v_r.chpts := v_chpts; -- std editing unallowed
 v_r.chpts := null; -- std editing unallowed
 v_r.descr := v_descr;
<<try>>
BEGIN
 -- 1) check isolation level
 IF NOT SAM.check_isolation() THEN
    v_es := SAM.make_es_transisolation(null,null); EXIT try;
 END IF;
 -- 1.1) lock required tables
 LOCK TABLE SAM.Scope IN EXCLUSIVE MODE;
 -- 2) identify user
 v_user := sam.get_user();
 IF v_user IS NULL THEN
    v_es := sam.make_execstatus(null,null,'E_NOUSER',session_user); EXIT try;
 END IF;
 -- 3) check capability
 IF NOT SAM.check_capability('SAM_MANAGE') THEN
   v_es := SAM.make_execstatus(null,null,'E_NOCAPABILITY','SAM_MANAGE','MAIN');
   EXIT try; END IF;
   v_as := SAM.do_auditlog_capuse_sole(v_as,'1','SAM_MANAGE',0);
 -- 4) check data
 -- 4.1) check for record's existence
 SELECT count(*) INTO i FROM SAM.Scope WHERE id = v_r.id;
 IF i <> 0 THEN
   v_es := sam.make_execstatus(v_r.id,null,'E_DUPOBJECT','SAM.Scope',
   'id',CAST(v_r.id as text)); EXIT try;
 END IF;
 -- 4.2) std QC
 v_ps := SAM.QC_Scope(v_as,v_r); v_as := v_ps.a;
   IF NOT v_ps.success THEN v_es := v_ps.e; EXIT try; END IF;
 -- 5) add record
 INSERT INTO SAM.Scope values(v_r.*);
 v_as := SAM.do_auditlog_da_sole(v_as,'1',null,null,null,v_r.id);
 -- 6) exit
 v_es := sam.make_execstatus(v_r.id,null,'I_SUCCESS');
EXCEPTION
 WHEN OTHERS THEN
	v_es:=sam.make_execstatus(v_r.id,null,'E_SQL',SQLSTATE,SQLERRM);
END;
-- FINALLY:
 PERFORM SAM.do_auditlog_exit(v_as,v_es);
 RETURN v_es;
END $$;
COMMIT TRANSACTION;
