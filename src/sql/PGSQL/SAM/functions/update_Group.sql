-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

CREATE OR REPLACE FUNCTION SAM.update_Group(
	v_id	bigint,
	v_sid	bigint,
	v_name	varchar(64),
	v_descr	varchar(255)
) RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 v_as SAM.auditstate;
 v_es SAM.execstatus;
 v_r SAM.Group%ROWTYPE;
 v_user bigint;
 i bigint; -- counter
BEGIN
 -- 0) enter into procedure
 v_as.subsys = 'SAM';
 v_as.eaction :='U';
 v_as.obj_type :='XG';
 v_as.sid := v_sid;
 v_as.ZID := v_id;
 v_as.ZTYPE := 'X';
 v_as.ZLVL := null; -- unknown at this moment
 v_as.proc := 'update_Group';
 v_as := SAM.do_auditlog_enter(v_as);
 -- 0.1) copy parameters to v_r
 v_r.id := v_id;
 v_r.sid := v_sid;
 v_r.name := v_name;
 v_r.descr := v_descr;
<<try>>
BEGIN
 -- 1) check isolation level
 IF NOT SAM.check_isolation() THEN
    v_es := SAM.make_es_transisolation(null,null); EXIT try;
 END IF;
 -- 1.1) lock required tables
 LOCK TABLE SAM.Group IN EXCLUSIVE MODE;
 -- 2) identify user
 v_user := sam.get_user();
 IF v_user IS NULL THEN
    v_es := sam.make_execstatus(v_r.id,null,'E_NOUSER',session_user); EXIT try;
 END IF;
 -- 3) check capability
 IF NOT SAM.check_capability('SAM_MANAGE') THEN
   v_es :=SAM.make_execstatus(v_r.id,null,'E_NOCAPABILITY','SAM_MANAGE','MAIN');
   EXIT try; END IF;
   v_as := SAM.do_auditlog_capuse_sole(v_as,'1','SAM_MANAGE',0);
 -- 4) check data
 -- 4.1) check for record's existence
 SELECT count(*) INTO i FROM SAM.Group  WHERE id = v_r.id;
 IF i = 0 THEN
   v_es := sam.make_execstatus(v_r.id,null,'E_NOBJECT','SAM.Group',
   'id',CAST(v_r.id as text)); EXIT try;
 END IF;
 IF i <> 1 THEN
   v_es := sam.make_execstatus(v_r.id,null,'E_NONSOLEOBJECT','SAM.Group',
   'id',CAST(v_r.id as text)); EXIT try;
 END IF;
 -- 4.2) STD Quality Control
 v_ps := SAM.QC_Group(v_as,v_r); v_as := v_ps.a;
   IF NOT v_ps.success THEN v_es := v_ps.e; EXIT try; END IF;
 -- 5) update record
 UPDATE SAM.Group SET (sid,name,descr)
 = (v_r.sid,v_r.name,v_r.descr)
 WHERE id = v_r.id;
 IF NOT FOUND THEN
   v_es := sam.make_execstatus(v_r.id,null,'E_NOBJECT','SAM.Group',
   'id',CAST(v_r.id as text)); EXIT try;
 END IF;
 v_as := SAM.do_auditlog_da_sole(v_as,'1',null,null,null,null);
 -- 6) exit
 v_es := sam.make_execstatus(v_r.id,null,'I_SUCCESS');
EXCEPTION
 WHEN OTHERS THEN
	v_es:=sam.make_execstatus(v_r.id,null,'E_SQL',SQLSTATE,SQLERRM);
END;
-- FINALLY:
 PERFORM SAM.do_auditlog_exit(v_as,v_es);
 RETURN v_es;
END $$;
-- GRANT/REVOKE moved to functions.grant/ directory 
