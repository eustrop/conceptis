-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

-- SAM.clearSessionBinding(), SAM.bindUser2Session(uid bigint, usi bigint, secretecookie varchar(127))
BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION SAM.clearSessionBinding() RETURNS VOID
AS ' delete from SAM.SessionBind XB where XB.pg_pid = pg_backend_pid(); '
LANGUAGE SQL SECURITY DEFINER;

CREATE OR REPLACE FUNCTION SAM.bindUser2Session(
   uid bigint, usi bigint, secretecookie varchar(127)
        ) RETURNS bigint
  AS ' delete from SAM.SessionBind XB where XB.pg_pid = pg_backend_pid();
       INSERT INTO SAM.SessionBind
            select pg_backend_pid(),session_user,XUS.uid,XUS.usi,XU.slevel,''N'',XUS.datefrom,XUS.dateto,pg_postmaster_start_time(),inet_client_addr(),inet_client_port()
            from SAM.UserSession XUS, SAM.User XU where
              XUS.uid= $1 and XUS.usi = $2 and XUS.secretcookie = $3
              and XU.id = XUS.uid and XU.locked=''N'' and XUS.dateto > NOW();
	select SAM.get_user(); '
    LANGUAGE SQL SECURITY DEFINER;
--grant execute on function SAM.bindUser2Session(uid bigint, usi bigint, secretecookie varchar(127)) to tis_users;
--grant execute on function SAM.clearSessionBinding() to tis_users;
COMMIT;
