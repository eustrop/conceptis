-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- purpose: returns true if current user has requested capability at target scope
--	or at MAIN scope with sid=0 if omitted

-- check for scope capability
BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION SAM.check_capability(v_capcode char(16), v_sid bigint)
  RETURNS boolean LANGUAGE plpgSQL SECURITY DEFINER STABLE AS $$
BEGIN
 PERFORM uid FROM SAM.UserCapability XUC WHERE XUC.uid = SAM.get_user()
  AND XUC.sid = COALESCE(v_sid,0) and XUC.capcode = v_capcode;
 RETURN FOUND;
END $$;
REVOKE ALL ON FUNCTION
 SAM.check_capability(v_capcode char(16), v_sid bigint)
 FROM PUBLIC;
-- required for use in views selectable by tis_users:
--GRANT EXECUTE ON FUNCTION SAM.check_capability(v_capcode char(16), v_sid bigint) TO tis_users;

-- check for global capability
CREATE OR REPLACE FUNCTION SAM.check_capability(v_capcode char(16))
  RETURNS boolean LANGUAGE plpgSQL SECURITY DEFINER STABLE AS $$
DECLARE
 i integer;
BEGIN
 SELECT COUNT(*) INTO i FROM SAM.UserCapability XUC WHERE
 	XUC.uid = SAM.get_user() AND XUC.sid = 0 and XUC.capcode = v_capcode;	
 RETURN (i > 0);
END $$;
REVOKE ALL ON FUNCTION
 SAM.check_capability(v_capcode char(16))
 FROM PUBLIC;
-- required for use in views selectable by tis_users:
--GRANT EXECUTE ON FUNCTION SAM.check_capability(v_capcode char(16)) TO tis_users;

-- check for scope capability and for then for global capability if no one
-- doing auditlog for any call at eclass '3' on success or fail
CREATE OR REPLACE FUNCTION SAM.check_capability(v_as SAM.auditstate,
  v_capcode char(16), v_sid bigint)
  RETURNS SAM.procstate LANGUAGE plpgSQL SECURITY DEFINER AS $$
DECLARE
 v_ps SAM.procstate;
 C_ECLASS CONSTANT CHAR(1) := '3';
BEGIN
 v_ps.success := FALSE;
 IF NOT SAM.check_capability(v_capcode,v_sid) THEN
  IF NOT SAM.check_capability(v_capcode) THEN
   v_ps.a := SAM.do_auditlog_capuse_fail(v_as,C_ECLASS,v_capcode,v_sid);
  ELSE
   v_ps.success := TRUE;
   v_ps.a := SAM.do_auditlog_capuse(v_as,C_ECLASS,v_capcode,0);
  END IF;
 ELSE
  v_ps.success := TRUE;
  v_ps.a := SAM.do_auditlog_capuse(v_as,C_ECLASS,v_capcode,v_sid);
 END IF;
 IF NOT v_ps.success THEN
  v_ps.e := SAM.make_execstatus(null,null,'E_NOCAPABILITY',
  	v_capcode,CAST(v_sid as text));
 END IF;
 RETURN v_ps;
END $$;
REVOKE ALL ON FUNCTION
 SAM.check_capability(v_as SAM.auditstate, v_capcode char(16), v_sid bigint)
 FROM PUBLIC;

-- check for scope capability and for then for global capability if no one
-- doing auditlog for any call at eclass '4' on success or fail using
-- sam.do_auditlog_capuse_sole() on success.
CREATE OR REPLACE FUNCTION SAM.check_capability_sole(v_as SAM.auditstate,
  v_capcode char(16), v_sid bigint)
  RETURNS SAM.procstate LANGUAGE plpgSQL SECURITY DEFINER AS $$
DECLARE
 v_ps SAM.procstate;
 C_ECLASS CONSTANT CHAR(1) := '4';
BEGIN
 v_ps.success := FALSE;
 IF NOT SAM.check_capability(v_capcode,v_sid) THEN
  IF NOT SAM.check_capability(v_capcode) THEN
   v_ps.a := SAM.do_auditlog_capuse_fail(v_as,C_ECLASS,v_capcode,v_sid);
  ELSE
   v_ps.success := TRUE;
   v_ps.a := SAM.do_auditlog_capuse_sole(v_as,C_ECLASS,v_capcode,0);
  END IF;
 ELSE
  v_ps.success := TRUE;
  v_ps.a := SAM.do_auditlog_capuse_sole(v_as,C_ECLASS,v_capcode,v_sid);
 END IF;
 IF NOT v_ps.success THEN
  v_ps.e := SAM.make_execstatus(null,null,'E_NOCAPABILITY',
  	v_capcode,CAST(v_sid as text));
 END IF;
 RETURN v_ps;
END $$;
REVOKE ALL ON FUNCTION
 SAM.check_capability_sole(v_as SAM.auditstate, v_capcode char(16), v_sid bigint)
 FROM PUBLIC;

COMMIT TRANSACTION;
