-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

DROP VIEW IF EXISTS SAM.V_ScopePolicy CASCADE;
CREATE VIEW SAM.V_ScopePolicy AS SELECT * FROM SAM.ScopePolicy
	WHERE SAM.get_user_slevel() >= SAM.get_slevel_SAM();
--GRANT SELECT ON SAM.V_ScopePolicy to tis_users;
