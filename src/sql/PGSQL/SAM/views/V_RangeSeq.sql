-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

DROP VIEW IF EXISTS SAM.V_RangeSeq CASCADE;
CREATE VIEW SAM.V_RangeSeq AS SELECT * FROM SAM.RangeSeq
	WHERE SAM.get_user_slevel() >= SAM.get_slevel_SAM()
        AND ( sid = SAM.get_user_sid_root() or sid =  SAM.get_user_sid() or sid = 0
              or SAM.get_user_sid() = 0 or sid = SAM.get_user());

DROP VIEW IF EXISTS SAM.V_RangeSeqInfo CASCADE;
CREATE VIEW SAM.V_RangeSeqInfo AS SELECT
	start,
	to_hex(start) as start_hex,
	rend,
	to_hex(rend) as rend_hex,
	lastid,
	to_hex(lastid) as lastid_hex,
	prange,
	to_hex(prange) as prange_hex,
	pbitl,
	sid,
	qtype,
	sorder,
	ts
  FROM SAM.RangeSeq
	WHERE SAM.get_user_slevel() >= SAM.get_slevel_SAM()
        AND ( sid = SAM.get_user_sid_root() or sid =  SAM.get_user_sid() or sid = 0
              or SAM.get_user_sid() = 0 or sid = SAM.get_user());
--GRANT SELECT ON SAM.V_RangeSeq to tis_users;

