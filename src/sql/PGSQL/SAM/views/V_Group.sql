-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

DROP VIEW IF EXISTS SAM.V_Group CASCADE;
CREATE VIEW SAM.V_Group AS SELECT * FROM SAM.Group
	WHERE SAM.get_user_slevel() >= SAM.get_slevel_SAM()
	AND 
	(
	 (sid = SAM.get_user_sid() OR SAM.get_user_sid() = 0) 
	OR
	 sid = SAM.get_user_sid_root()
	OR sid = SAM.get_user()
        );
-- last condition for scopes constucted around the SAM.user (sid = SAM.get_user())
--GRANT SELECT ON SAM.V_Group to tis_users;
