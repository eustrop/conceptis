-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

DROP VIEW IF EXISTS SAM.V_UserCapability CASCADE;
CREATE VIEW SAM.V_UserCapability AS SELECT * FROM SAM.UserCapability
	WHERE SAM.get_user_slevel() >= SAM.get_slevel_SAM();
--GRANT SELECT ON SAM.V_UserCapability to tis_users;
