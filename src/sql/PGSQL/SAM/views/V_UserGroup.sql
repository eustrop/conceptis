-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

DROP VIEW IF EXISTS SAM.V_UserGroup CASCADE;
CREATE VIEW SAM.V_UserGroup AS SELECT * FROM SAM.UserGroup
	WHERE SAM.get_user_slevel() >= SAM.get_slevel_SAM();
--GRANT SELECT ON SAM.V_UserGroup to tis_users;
