-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- 2023-10-13 20:00 eustrop. should be used at user interface to obtain list of scopes where user able to create
--

DROP VIEW IF EXISTS SAM.V_ScopeCurrentUserCreatea CASCADE;
CREATE VIEW SAM.V_ScopeCurrentUserCreatea
   AS
   SELECT XS.*, XALC.obj_type FROM
   SAM.Scope XS, SAM.UserGroup XUG, SAM.ACLScope XALC
   WHERE XUG.uid= SAM.get_user() AND XUG.gid = XALC.gid AND XALC.sid= XS.id and XALC.createa = 'Y';
--GRANT SELECT ON SAM.V_ScopeCurrentUserCreatea to tis_users;
