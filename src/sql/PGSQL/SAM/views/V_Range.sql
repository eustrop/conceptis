-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

DROP VIEW IF EXISTS SAM.V_Range CASCADE;
CREATE VIEW SAM.V_Range AS SELECT * FROM SAM.Range
	WHERE NOT SAM.get_user() IS NULL;

DROP VIEW IF EXISTS SAM.V_RangesInfo CASCADE;
CREATE VIEW SAM.V_RangesInfo AS SELECT
	startid,
	to_hex(startid) as startid_hex,
	bitl,
	endid,
	to_hex(endid) as endid_hex,
	prange,
	to_hex(prange) as prange_hex,
	pbitl,
	ownerid,
	descr
  FROM SAM.Range
	WHERE NOT SAM.get_user() IS NULL;
--GRANT SELECT ON SAM.V_Range to tis_users;
