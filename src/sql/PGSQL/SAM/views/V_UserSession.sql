-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

DROP VIEW IF EXISTS SAM.V_UserSession CASCADE;
CREATE VIEW SAM.V_UserSession AS SELECT
	uid,
	usi,
	datefrom,
	dateto,
	'****************'::varchar(127) as secretcookie,
	servicefrom,
	devicefrom
 FROM SAM.UserSession
	WHERE uid= SAM.get_user() or (
        exists (select uid from SAM.UserCapability where uid = SAM.get_user() and capcode = 'SAM_MANAGE' and sid = 0)
	AND SAM.get_user_slevel() >= SAM.get_slevel_SAM() );
--GRANT SELECT ON SAM.V_UserSession to tis_users;
