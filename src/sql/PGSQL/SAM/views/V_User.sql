-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

DROP VIEW IF EXISTS SAM.V_User CASCADE;
CREATE VIEW SAM.V_User AS SELECT * FROM SAM.User
	WHERE ( SAM.get_user_slevel() >= SAM.get_slevel_SAM()
	AND 
	(
	 (sid = SAM.get_user_sid() OR SAM.get_user_sid() = 0) 
	OR
	 (sid in (select id from SAM.Scope XS where XS.sid = SAM.get_user_sid())
	)
	)) OR id = SAM.get_user() ;
-- last condition for scopes constucted around the SAM.user (sid = SAM.get_user())
--GRANT SELECT ON SAM.V_User to tis_users;
