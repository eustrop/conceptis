-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

DROP VIEW IF EXISTS SAM.V_UserPassword CASCADE;
CREATE OR REPLACE VIEW SAM.V_UserPassword AS SELECT
	uid,
	algo,
	iterations,
	datefrom,
	dateto,
--	password,
--	'**********'::varchar(127) as password,
	(CASE WHEN password is null then null ELSE '**********' END)::varchar(127) as password,
	salt
 FROM SAM.UserPassword
	WHERE uid= SAM.get_user() or
	( exists (select uid from SAM.UserCapability where uid = SAM.get_user() and capcode = 'SAM_MANAGE' and sid = 0)
	AND SAM.get_user_slevel() >= SAM.get_slevel_SAM() );
--GRANT SELECT ON SAM.V_UserPassword to tis_users;
