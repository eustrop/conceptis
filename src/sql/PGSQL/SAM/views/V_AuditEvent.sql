-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- purpose: regular users with SAM_VIEWAUDITLOG cpability able to view it

DROP VIEW IF EXISTS SAM.V_AuditEvent CASCADE;
CREATE VIEW SAM.V_AuditEvent AS SELECT * FROM SAM.AuditEvent
	WHERE SAM.check_capability('SAM_VIEWAUDITLOG')
	OR SAM.check_capability('SAM_VIEWAUDITLOG',sid);
--GRANT SELECT ON SAM.V_AuditEvent to tis_users;
