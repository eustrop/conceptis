-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- purpose: store information from user activity audit
--	serions changes since Manifesto:
--	id - common number for all records which describes of the same user call
--	sn - is sequental event number of record inside id (PP from LNNPPRR)
--	eclass - event class (L from LNNPPRR), '0','1',... see EVENT_CLASS dic
--	eaction - requested action (NN from LNNPPRR), 'C','U','D'... see EVENT_ACTION
--	errnum - error number from dic.ZErrorMsg.errnum (and RR from LNNPPRR)
--	uid,sid,obj_type,ZID,capcode - from Manifesto
--	capsid - original sid of UserCapability
--	slevel - slevel of user 
--	ZLVL - slevel of requested ZID object
--	ZOID,ZVER - logical transaction identification (only if applicable)
--	ZID - row id (ZRID for object's record, id,sid,uid,gid for SAM objects)
--	einfo - eventinfo from Manifesto. 
--	subsys - subsystem code (SAM,TIS,TISC, etc). source of the event
--	proc - procedure (function) name. source of the event. debug only. 

--DROP TABLE IF EXISTS SAM.AuditEvent CASCADE;
CREATE TABLE SAM.AuditEvent (
	-- mandatory fields
	id	bigint	NOT NULL,
	sn	smallint NOT NULL,
	ts	timestamp NOT NULL,
	ec	char(1) NOT NULL, -- eclass
	ea	char(2) NOT NULL, -- eaction
	subsys	varchar(10) NOT NULL,
	obj_type varchar(20) NOT NULL,
	errnum smallint NOT NULL,
	-- optional fields
	-- user info:
	uid	bigint	NULL,
	slevel	smallint NULL,
	capcode	varchar(32) NULL,
	capsid	bigint NULL,
	-- object info:
	sid	bigint NULL,
	ZOID	bigint	NULL,
	ZVER	bigint	NULL,
	ZTYPE	varchar(14)	NULL,
	ZID	bigint	NULL,
	ZLVL	smallint NULL,
	-- debug info:
	proc	name	NULL,
	einfo	text	NULL,
	PRIMARY KEY(id,sn)
	);
