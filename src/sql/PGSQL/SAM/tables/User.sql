-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

--DROP TABLE IF EXISTS SAM.User CASCADE;
CREATE TABLE SAM.User (
	id	bigint NOT NULL,
	sid	bigint NOT NULL,
	slevel	smallint NOT NULL,
	slevel_min	smallint NOT NULL,
	slevel_max	smallint NOT NULL,
	locked	char(1) NOT NULL,
	lang	varchar(3) NULL,
	login	varchar(64) NOT NULL,
	db_user	varchar(64) NULL,
	full_name	varchar(255) NULL,
	PRIMARY KEY (id),
	UNIQUE(login),
	UNIQUE(db_user)
	);
--CREATE INDEX User_idx1 on SAM.User(login);
--CREATE INDEX User_idx2 on SAM.User(db_user);
