-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

--DROP TABLE IF EXISTS SAM.UserSession CASCADE;
CREATE TABLE SAM.UserSession (
	uid	bigint NOT NULL,
	usi	bigint NULL,
	datefrom	timestamptz NOT NULL,
	dateto	timestamptz NULL,
	secretcookie	varchar(127) NULL,
	servicefrom	varchar(63) NULL,
	devicefrom	varchar(127) NULL,
	PRIMARY KEY (uid,usi)
	);
