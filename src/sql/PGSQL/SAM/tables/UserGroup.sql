-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

--DROP TABLE IF EXISTS SAM.UserGroup CASCADE;
CREATE TABLE SAM.UserGroup (
	uid	bigint NOT NULL,
	gid	bigint NOT NULL,
	PRIMARY KEY (uid,gid)
	);
