-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

--DROP TABLE IF EXISTS SAM.Group CASCADE;
CREATE TABLE SAM.Group (
	id	bigint NOT NULL,
	sid	bigint NOT NULL,
	name	varchar(64) NOT NULL,
	descr	varchar(255) NULL,
	PRIMARY KEY (id),
	UNIQUE(name)
	);
CREATE INDEX Group_idx1 on SAM.Group(name);
