-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

--DROP TABLE IF EXISTS SAM.Scope CASCADE;
CREATE TABLE SAM.Scope (
	id	bigint NOT NULL,
	sid	bigint NOT NULL,
	slevel_min	smallint NOT NULL,
	slevel_max	smallint NOT NULL,
	name	varchar(64) NOT NULL,
	is_local	char(1) NOT NULL,
	auditlvl	char(1) NOT NULL,
	chpts	timestamptz NULL,
	descr	varchar(255) NULL,
	PRIMARY KEY (id),
	UNIQUE(name,sid)
	);
