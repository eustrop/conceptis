-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

--DROP TABLE IF EXISTS SAM.UserCapability CASCADE;
CREATE TABLE SAM.UserCapability (
	uid	bigint NOT NULL,
	sid	bigint NOT NULL,
	capcode	char(32) NOT NULL,
	PRIMARY KEY (uid,sid,capcode)
	);
CREATE INDEX UserCapability_idx1 on SAM.UserCapability(capcode);
