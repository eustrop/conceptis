-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

--DROP TABLE IF EXISTS SAM.UserSessionSeq CASCADE;
CREATE TABLE SAM.UserSessionSeq (
	uid	bigint NOT NULL,
	usi	bigint NULL,
	usi_max	bigint NULL,
	ts	timestamptz NOT NULL,
	PRIMARY KEY (uid)
	);
