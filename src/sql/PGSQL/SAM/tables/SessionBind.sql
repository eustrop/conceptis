-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

--DROP TABLE IF EXISTS SAM.SessionBind CASCADE;
CREATE TABLE SAM.SessionBind (
	pg_pid	int4 NOT NULL,
	db_user	varchar(63) NOT NULL,
	uid	bigint NOT NULL,
	usi	bigint NULL,
	slevel	smallint NOT NULL,
	locked	char(1) NOT NULL,
	datefrom	timestamptz NOT NULL,
	dateto	timestamptz NULL,
	pg_s_time	timestamptz NULL,
	cl_addr	inet NULL,
	cl_port	int4 NULL,
	PRIMARY KEY (pg_pid)
	);
