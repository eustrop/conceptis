-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

--DROP TABLE IF EXISTS SAM.ACLScope CASCADE;
CREATE TABLE SAM.ACLScope (
	gid	bigint NOT NULL,
	sid	bigint NOT NULL,
	obj_type	varchar(20) NOT NULL,
	writea	char(1) NOT NULL,
	createa	char(1) NOT NULL,
	deletea	char(1) NOT NULL,
	reada	char(1) NOT NULL,
	locka	char(1) NOT NULL,
	historya	char(1) NOT NULL,
	PRIMARY KEY (gid,sid,obj_type)
	);
