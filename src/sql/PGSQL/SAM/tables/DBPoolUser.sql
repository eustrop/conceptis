-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

--DROP TABLE IF EXISTS SAM.DBPoolUser CASCADE;
CREATE TABLE SAM.DBPoolUser (
	db_user	varchar(64) NULL,
	sid	bigint NOT NULL,
	slevel_min	smallint NOT NULL,
	slevel_max	smallint NOT NULL,
	descr	varchar(99) NULL,
	PRIMARY KEY (db_user),
	UNIQUE(db_user)
	);
