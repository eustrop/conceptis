-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

--DROP TABLE IF EXISTS SAM.UserPassword CASCADE;
CREATE TABLE SAM.UserPassword (
	uid	bigint NOT NULL,
	algo	varchar(8) NOT NULL,
	iterations	INT NOT NULL,
	datefrom	timestamp NOT NULL,
	dateto	timestamp NULL,
	password	varchar(127) NULL,
	salt	varchar(127) NULL,
	PRIMARY KEY (uid)
	);
