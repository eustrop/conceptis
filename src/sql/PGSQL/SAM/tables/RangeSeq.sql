-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

--DROP TABLE IF EXISTS SAM.RangeSeq CASCADE;
CREATE TABLE SAM.RangeSeq (
	start	bigint NOT NULL,
	rend	bigint NOT NULL,
	lastid	bigint NULL,
	prange	bigint NOT NULL,
	pbitl	smallint NOT NULL,
	sid	bigint NULL,
	qtype	varchar(14) NULL,
	sorder	smallint NULL,
	ts	timestamptz NOT NULL,
	PRIMARY KEY (start)
	);
CREATE INDEX RangeSeq_idx1 on SAM.RangeSeq(sid,qtype);
CREATE INDEX RangeSeq_idx2 on SAM.RangeSeq(prange,pbitl);
