-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

--DROP TABLE IF EXISTS SAM.MNTLog CASCADE;
CREATE TABLE SAM.MNTLog (
	ts	timestamptz NOT NULL,
	uid	bigint NOT NULL,
	dbuser	varchar(64) NOT NULL,
	msg	varchar(1024) NULL
	);
