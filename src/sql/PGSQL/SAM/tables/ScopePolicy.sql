-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

--DROP TABLE IF EXISTS SAM.ScopePolicy CASCADE;
CREATE TABLE SAM.ScopePolicy (
	sid	bigint NOT NULL,
	obj_type	varchar(14) NOT NULL,
	maxcount	bigint NULL,
	ocount	bigint NULL,
	doaudit	char(1) NOT NULL,
	dodebug	char(1) NOT NULL,
	PRIMARY KEY (sid,obj_type)
	);
