-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

--DROP TABLE IF EXISTS SAM.Range CASCADE;
CREATE TABLE SAM.Range (
	startid	bigint NOT NULL,
	bitl	smallint NOT NULL,
	endid	bigint NOT NULL,
	prange	bigint NOT NULL,
	pbitl	smallint NOT NULL,
	ownerid	bigint NULL,
	descr	varchar(99) NULL,
	PRIMARY KEY (startid,bitl)
	);
CREATE INDEX Range_idx1 on SAM.Range(prange);
