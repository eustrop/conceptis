DROP FUNCTION IF EXISTS sam.bind_dbo(bigint,smallint) CASCADE;
DROP FUNCTION IF EXISTS sam.get_xu_user() CASCADE;
DROP FUNCTION IF EXISTS sam.get_xu_slevel() CASCADE;
DROP FUNCTION IF EXISTS sam.get_xu_login() CASCADE;
DROP FUNCTION IF EXISTS sam.get_xu_lang() CASCADE;
DROP FUNCTION IF EXISTS sam.get_xb_user() CASCADE;
DROP FUNCTION IF EXISTS sam.get_xb_slevel() CASCADE;
DROP FUNCTION IF EXISTS sam.get_xb_login() CASCADE;
DROP FUNCTION IF EXISTS sam.get_xb_lang() CASCADE;
DROP FUNCTION IF EXISTS sam.get_user() CASCADE;
DROP FUNCTION IF EXISTS sam.get_user_slevel() CASCADE;
DROP FUNCTION IF EXISTS sam.get_user_login() CASCADE;
DROP FUNCTION IF EXISTS sam.get_user_lang() CASCADE;
DROP FUNCTION IF EXISTS sam.get_user_sid() CASCADE;
DROP FUNCTION IF EXISTS sam.get_user_slevel_min() CASCADE;
DROP FUNCTION IF EXISTS sam.get_user_slevel_max() CASCADE;

