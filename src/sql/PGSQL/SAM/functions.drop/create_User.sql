DROP FUNCTION IF EXISTS SAM.create_User(
	v_id	bigint,
	v_sid	bigint,
	v_slevel	smallint,
	v_slevel_min	smallint,
	v_slevel_max	smallint,
	v_locked	char(1),
	v_lang	char(3),
	v_login	varchar(64),
	v_db_user	varchar(64),
	v_full_name	varchar(255)
) CASCADE;
