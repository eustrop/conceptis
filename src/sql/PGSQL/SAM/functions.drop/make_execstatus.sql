DROP FUNCTION IF EXISTS SAM.make_execstatus(
  v_ZID bigint, v_ZVER bigint, v_errcode varchar(32), v_p1 text, v_p2 text, v_p3 text
  ) CASCADE;
DROP FUNCTION IF EXISTS SAM.make_execstatus(
	v_ZID bigint, v_ZVER bigint, v_e SAM.execstatus
	) CASCADE;
DROP FUNCTION IF EXISTS SAM.make_execstatus(
	v_ZID bigint, v_ZVER bigint, v_errcode varchar(32), v_p1 text, v_p2 text
	) CASCADE;

DROP FUNCTION IF EXISTS SAM.make_execstatus(
	v_ZID bigint, v_ZVER bigint, v_errcode varchar(32), v_p1 text
	) CASCADE;

DROP FUNCTION IF EXISTS SAM.make_execstatus(
	v_ZID bigint, v_ZVER bigint, v_errcode varchar(32)
	) CASCADE;

DROP FUNCTION IF EXISTS SAM.make_execstatus(
	v_errcode varchar(32)
	) CASCADE;

DROP FUNCTION IF EXISTS SAM.make_es_success( v_ZID bigint,
	v_ZVER bigint) CASCADE;

DROP FUNCTION IF EXISTS SAM.make_es_notimplemented( v_ZID bigint,
	v_ZVER bigint ) CASCADE;

DROP FUNCTION IF EXISTS SAM.make_es_transisolation( v_ZID bigint,
	v_ZVER bigint ) CASCADE;
DROP FUNCTION IF EXISTS SAM.make_es_invalidcode(v_ZID bigint, v_ZVER bigint,v_code text,
	v_field text,v_dic text) CASCADE;

DROP FUNCTION IF EXISTS SAM.make_es_notnull(v_ZID bigint, v_ZVER bigint,v_table text,
	v_field text) CASCADE;

