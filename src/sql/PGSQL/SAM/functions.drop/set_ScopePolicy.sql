DROP FUNCTION SAM.set_ScopePolicy(
	v_sid	bigint,
	v_obj_type	varchar(14),
	v_maxcount	bigint,
	v_doaudit	char(1),
	v_dodebug	char(1)
);
