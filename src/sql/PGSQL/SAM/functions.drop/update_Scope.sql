DROP FUNCTION IF EXISTS SAM.update_Scope(
	v_id	bigint,
	v_sid	bigint,
	v_slevel_min	smallint,
	v_slevel_max	smallint,
	v_name	varchar(64),
	v_is_local	char(1),
	v_auditlvl	char(1),
	v_descr	varchar(255)
) CASCADE;
