DROP FUNCTION IF EXISTS SAM.create_Group(
	v_id	bigint,
	v_sid	bigint,
	v_name	varchar(64),
	v_descr	varchar(255)
) CASCADE;
