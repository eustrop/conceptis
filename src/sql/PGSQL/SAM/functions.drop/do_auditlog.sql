DROP FUNCTION IF EXISTS SAM.check_loglvl(v_eclass char(1),v_sid bigint) CASCADE;
DROP FUNCTION IF EXISTS SAM.get_topmost_eclass(v_e1 char(1),v_e2 char(1)) CASCADE;
DROP FUNCTION IF EXISTS SAM.get_topmost_eclass(v_e1 char(1),v_e2 char(1), v_e3 char(1)) CASCADE;

DROP FUNCTION IF EXISTS SAM.write_auditlog( v_a SAM.AuditEvent ) CASCADE;
DROP FUNCTION IF EXISTS SAM.cast_auditstate2auditevent (v_as SAM.auditstate) CASCADE;

DROP FUNCTION IF EXISTS SAM.do_auditlog_msg(v_as INOUT SAM.auditstate,
	v_eclass char(1), v_errnum smallint, v_einfo text) CASCADE;
DROP FUNCTION IF EXISTS SAM.do_auditlog_msg(v_as INOUT SAM.auditstate,
	v_eclass char(1), v_errnum integer, v_einfo text) CASCADE;
DROP FUNCTION IF EXISTS SAM.do_auditlog_enter(v_as INOUT SAM.auditstate) CASCADE;

DROP FUNCTION IF EXISTS SAM.do_auditlog_capuse(v_as INOUT SAM.auditstate,
	v_eclass char(1), v_capcode char(16),v_capsid bigint) CASCADE;
DROP FUNCTION IF EXISTS SAM.do_auditlog_capuse_fail(v_as INOUT SAM.auditstate,
	v_eclass char(1), v_capcode char(16),v_capsid bigint) CASCADE;

DROP FUNCTION IF EXISTS SAM.do_auditlog_capuse_sole(v_as INOUT
   SAM.auditstate, v_eclass char(1), v_capcode char(16),v_capsid bigint) CASCADE;

DROP FUNCTION IF EXISTS SAM.do_auditlog_da(v_as INOUT SAM.auditstate,
	v_eclass char(1), v_eaction char(2), v_obj_type char(5),
	v_sid bigint, v_ZID bigint, v_msg text) CASCADE;

DROP FUNCTION IF EXISTS SAM.do_auditlog_da(v_as INOUT SAM.auditstate,
	v_eclass char(1), v_eaction char(2), v_obj_type char(5),
	v_sid bigint, v_ZID bigint) CASCADE;
DROP FUNCTION IF EXISTS SAM.do_auditlog_da(v_as INOUT SAM.auditstate,
	v_eclass char(1)) CASCADE;

DROP FUNCTION IF EXISTS SAM.do_auditlog_da_sole(v_as INOUT SAM.auditstate,
	v_eclass char(1), v_eaction char(2), v_obj_type char(5),
	v_sid bigint, v_ZID bigint, v_msg text) CASCADE;
DROP FUNCTION IF EXISTS SAM.do_auditlog_da_sole(v_as INOUT SAM.auditstate,
	v_eclass char(1), v_eaction char(2), v_obj_type char(5),
	v_sid bigint, v_ZID bigint) CASCADE;
DROP FUNCTION IF EXISTS SAM.do_auditlog_exit(v_as INOUT SAM.auditstate,
	v_es SAM.execstatus) CASCADE;
DROP FUNCTION IF EXISTS SAM.do_auditlog_fchanges(v_as INOUT SAM.auditstate,
	v_eclass char(1), v_errnum smallint,
	v_field text, v_oldvalue text, v_newvalue text) CASCADE;
DROP FUNCTION IF EXISTS SAM.do_auditlog_ac(v_as INOUT SAM.auditstate,
	v_eclass char(1), v_eaction char(2), v_obj_type char(5),
	v_errnum smallint, v_sid bigint, v_ZID bigint, v_ZLVL smallint,
	v_msg text) CASCADE;
DROP FUNCTION IF EXISTS SAM.do_auditlog_ac(v_as INOUT SAM.auditstate,
	v_eclass char(1), v_eaction char(2), v_obj_type char(5),
	v_errnum smallint, v_sid bigint, v_ZID bigint, v_ZLVL smallint) CASCADE;
DROP FUNCTION IF EXISTS SAM.do_auditlog_ac(v_as INOUT SAM.auditstate,
	v_eclass char(1), v_eaction char(2), v_obj_type char(5),
	v_sid bigint, v_ZID bigint, v_ZLVL smallint, v_ps SAM.procstate) CASCADE;
