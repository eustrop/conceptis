GRANT EXECUTE ON FUNCTION SAM.grant_UserCapability(
	v_uid	bigint,
	v_sid	bigint,
	v_capcode	char(16)
) TO tis_users;
REVOKE ALL ON FUNCTION SAM.grant_UserCapability(
	v_uid	bigint,
	v_sid	bigint,
	v_capcode	char(16)
) FROM PUBLIC;
