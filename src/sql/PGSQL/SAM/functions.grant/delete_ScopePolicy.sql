REVOKE ALL ON FUNCTION SAM.delete_ScopePolicy(
	v_sid	bigint,
	v_obj_type	varchar(14)
	) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION SAM.delete_ScopePolicy(
	v_sid	bigint,
	v_obj_type	varchar(14)
	) TO tis_users;
