GRANT EXECUTE ON FUNCTION SAM.revoke_UserGroup(
	v_uid	bigint,
	v_gid	bigint
	) TO tis_users;
REVOKE ALL ON FUNCTION SAM.revoke_UserGroup(
	v_uid	bigint,
	v_gid	bigint
	) FROM PUBLIC;
