GRANT EXECUTE on function sam.get_xu_user() to tis_users;
GRANT EXECUTE on function sam.get_xu_slevel() to tis_users;
GRANT EXECUTE on function sam.get_xu_login() to tis_users;
GRANT EXECUTE on function sam.get_xu_lang() to tis_users;
GRANT EXECUTE on function sam.get_xb_user() to tis_users;
GRANT EXECUTE on function sam.get_xb_slevel() to tis_users;
GRANT EXECUTE on function sam.get_xb_login() to tis_users;
GRANT EXECUTE on function sam.get_xb_lang() to tis_users;
GRANT EXECUTE on function sam.get_user() to tis_users;
GRANT EXECUTE on function sam.get_user_slevel() to tis_users;
GRANT EXECUTE on function sam.get_user_login() to tis_users;
GRANT EXECUTE on function sam.get_user_lang() to tis_users;
GRANT EXECUTE on function sam.get_user_sid() to tis_users;
GRANT EXECUTE on function sam.get_user_sid_root() to tis_users;
GRANT EXECUTE on function sam.get_user_slevel_min() to tis_users;
GRANT EXECUTE on function sam.get_user_slevel_max() to tis_users;

