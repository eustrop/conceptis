REVOKE ALL ON FUNCTION SAM.update_Group(
	v_id	bigint,
	v_sid	bigint,
	v_name	varchar(64),
	v_descr	varchar(255)
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION SAM.update_Group(
	v_id	bigint,
	v_sid	bigint,
	v_name	varchar(64),
	v_descr	varchar(255)
) TO tis_users;
