REVOKE ALL ON FUNCTION
 SAM.check_capability(v_capcode char(16), v_sid bigint)
 FROM PUBLIC;
-- required for use in views selectable by tis_users:
GRANT EXECUTE ON FUNCTION SAM.check_capability(v_capcode char(16), v_sid bigint) TO tis_users;
REVOKE ALL ON FUNCTION
 SAM.check_capability(v_capcode char(16))
 FROM PUBLIC;
-- required for use in views selectable by tis_users:
GRANT EXECUTE ON FUNCTION SAM.check_capability(v_capcode char(16))
 TO tis_users;

-- check for scope capability and for then for global capability if no one
-- doing auditlog for any call at eclass '3' on success or fail
REVOKE ALL ON FUNCTION
 SAM.check_capability(v_as SAM.auditstate, v_capcode char(16), v_sid bigint)
 FROM PUBLIC;

-- check for scope capability and for then for global capability if no one
-- doing auditlog for any call at eclass '4' on success or fail using
-- sam.do_auditlog_capuse_sole() on success.
REVOKE ALL ON FUNCTION
 SAM.check_capability_sole(v_as SAM.auditstate, v_capcode char(16), v_sid bigint)
 FROM PUBLIC;
