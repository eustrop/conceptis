REVOKE ALL ON FUNCTION SAM.set_Scope_chpts(
	v_id	bigint,
	v_chpts	timestamp
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION SAM.set_Scope_chpts(
	v_id	bigint,
	v_chpts	timestamp
) TO tis_users;
