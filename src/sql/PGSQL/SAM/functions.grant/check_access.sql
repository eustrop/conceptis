-- this function for internal use only
REVOKE ALL ON FUNCTION SAM.check_access_write(v_as SAM.auditstate,v_sid bigint,
	v_obj_type varchar(20), v_ZOID bigint, v_ZLVL smallint) FROM PUBLIC;
-- this function for internal use only
REVOKE ALL ON FUNCTION SAM.check_access_delete(v_as SAM.auditstate,v_sid bigint,
	v_obj_type varchar(20), v_ZOID bigint, v_ZLVL smallint) FROM PUBLIC;
-- this function for internal use only
REVOKE ALL ON FUNCTION SAM.check_access_lock(v_as SAM.auditstate,v_sid bigint,
	v_obj_type varchar(20), v_ZOID bigint, v_ZLVL smallint) FROM PUBLIC;
