REVOKE ALL ON FUNCTION SAM.set_ACLScope(
	v_gid	bigint,
	v_sid	bigint,
	v_obj_type	varchar(20),
	v_writea	char(1),
	v_createa	char(1),
	v_deletea	char(1),
	v_reada	char(1),
	v_locka	char(1),
	v_historya	char(1)
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION SAM.set_ACLScope(
	v_gid	bigint,
	v_sid	bigint,
	v_obj_type	varchar(20),
	v_writea	char(1),
	v_createa	char(1),
	v_deletea	char(1),
	v_reada	char(1),
	v_locka	char(1),
	v_historya	char(1)
) TO tis_users;
