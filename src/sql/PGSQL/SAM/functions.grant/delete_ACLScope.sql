REVOKE ALL ON FUNCTION SAM.delete_ACLScope(
	v_gid	bigint,
	v_sid	bigint,
	v_obj_type	char(5)
	) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION SAM.delete_ACLScope(
	v_gid	bigint,
	v_sid	bigint,
	v_obj_type	char(5)
	) TO tis_users;
