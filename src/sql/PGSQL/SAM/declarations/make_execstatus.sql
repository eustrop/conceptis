-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
--
--

CREATE OR REPLACE FUNCTION SAM.make_execstatus(
  v_ZID bigint, v_ZVER bigint, v_errcode varchar(32), v_p1 text, v_p2 text, v_p3 text
  ) RETURNS SAM.execstatus LANGUAGE plpgSQL SECURITY DEFINER STABLE AS $$
DECLARE
 v_es SAM.execstatus;
BEGIN
 -- this dummy "no dependence" definition will be overloaded by load_tis.sql 
 v_es.ZID := v_ZID; v_es.ZVER := v_ZVER; v_es.errcode := v_errcode;
 v_es.p1 := v_p1; v_es.p2 := v_p2; v_es.p3 := v_p3;
 v_es.errnum := 2; v_es.errdesc := v_es.errcode;
 RETURN v_es;
END $$;
REVOKE ALL ON FUNCTION SAM.make_execstatus(v_ZID bigint, v_ZVER bigint,
	v_errcode varchar(32), v_p1 text, v_p2 text, v_p3 text) FROM PUBLIC;

CREATE OR REPLACE FUNCTION SAM.make_execstatus(
	v_ZID bigint, v_ZVER bigint, v_e SAM.execstatus
	) RETURNS SAM.execstatus LANGUAGE plpgSQL SECURITY DEFINER AS $$
DECLARE
 v_es SAM.execstatus;
BEGIN
 v_es.ZID := v_ZID; v_es.ZVER := v_ZVER; v_es.errcode := v_e.errcode;
 v_es.p1 := v_e.p1; v_es.p2 := v_e.p2; v_es.p3 := v_e.p3;
 v_es.errnum := v_e.errnum; v_es.errdesc := v_e.errdesc;
 RETURN v_es;
END $$;
REVOKE ALL ON FUNCTION SAM.make_execstatus(v_ZID bigint, v_ZVER bigint,
	v_e SAM.execstatus ) FROM PUBLIC;

CREATE OR REPLACE FUNCTION SAM.make_execstatus(
	v_ZID bigint, v_ZVER bigint, v_errcode varchar(32), v_p1 text, v_p2 text
	) RETURNS SAM.execstatus LANGUAGE SQL SECURITY INVOKER AS
	'select SAM.make_execstatus($1,$2,$3,$4,$5,null)';

CREATE OR REPLACE FUNCTION SAM.make_execstatus(
	v_ZID bigint, v_ZVER bigint, v_errcode varchar(32), v_p1 text
	) RETURNS SAM.execstatus LANGUAGE SQL SECURITY INVOKER AS
	'select SAM.make_execstatus($1,$2,$3,$4,null,null)';

CREATE OR REPLACE FUNCTION SAM.make_execstatus(
	v_ZID bigint, v_ZVER bigint, v_errcode varchar(32)
	) RETURNS SAM.execstatus LANGUAGE SQL SECURITY INVOKER AS
	'select SAM.make_execstatus($1,$2,$3,null,null,null)';

CREATE OR REPLACE FUNCTION SAM.make_execstatus(
	v_errcode varchar(32)
	) RETURNS SAM.execstatus LANGUAGE SQL SECURITY INVOKER AS
	'select SAM.make_execstatus(null,null,$1,null,null,null)';

CREATE OR REPLACE FUNCTION SAM.make_es_success( v_ZID bigint,
	v_ZVER bigint)
	RETURNS SAM.execstatus LANGUAGE SQL SECURITY INVOKER AS
	'select SAM.make_execstatus($1,$2,''I_SUCCESS'',null,null,null)';

CREATE OR REPLACE FUNCTION SAM.make_es_notimplemented( v_ZID bigint,
	v_ZVER bigint )
	RETURNS SAM.execstatus LANGUAGE SQL SECURITY INVOKER AS
	'select SAM.make_execstatus($1,$2,''E_NOTIMPLEMENTED'',null,null,null)';

CREATE OR REPLACE FUNCTION SAM.make_es_transisolation( v_ZID bigint,
	v_ZVER bigint )
	RETURNS SAM.execstatus LANGUAGE SQL SECURITY INVOKER AS
	'select SAM.make_execstatus($1,$2,''E_TRANSISOLATION'',
	pg_catalog.current_setting(''transaction_isolation''),
	''read committed'')';
CREATE OR REPLACE FUNCTION SAM.make_es_invalidcode(v_ZID bigint, v_ZVER bigint,v_code text,
	v_field text,v_dic text)
	RETURNS SAM.execstatus LANGUAGE SQL SECURITY INVOKER AS
	'select SAM.make_execstatus($1,$2,''E_INVALIDCODE'',$3,$4,$5)';

CREATE OR REPLACE FUNCTION SAM.make_es_notnull(v_ZID bigint, v_ZVER bigint,v_table text,
	v_field text)
	RETURNS SAM.execstatus LANGUAGE SQL SECURITY INVOKER AS
	'select SAM.make_execstatus($1,$2,''E_NOTNULL'',$3,$4)';

