-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE at the project's root directory
--
-- $Id$
--

CREATE OR REPLACE FUNCTION SAM.update_User(
	v_id	bigint,
	v_sid	bigint,
	v_slevel	smallint,
	v_locked	char(1),
	v_lang	char(2),
	v_login	varchar(32),
	v_db_user	varchar(64),
	v_full_name	varchar(99)
) RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_es SAM.execstatus;
BEGIN
 v_es.errnum :=1; v_es.errcode :='E_NOTIMPLEMENTED';
 v_es.errdesc :=v_es.errcode; RETURN v_es;
END $$;
