-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE at the project's root directory
--
-- $Id$
--

CREATE OR REPLACE FUNCTION SAM.update_UserGroup(
	v_uid	bigint,
	v_gid	bigint
) RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_es SAM.execstatus;
BEGIN
 v_es.errnum :=1; v_es.errcode :='E_NOTIMPLEMENTED';
 v_es.errdesc :=v_es.errcode; RETURN v_es;
END $$;
