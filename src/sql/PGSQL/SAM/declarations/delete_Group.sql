-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE at the project's root directory
--
-- $Id$
--

CREATE OR REPLACE FUNCTION SAM.delete_Group(v_id bigint)
 RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_es SAM.execstatus;
BEGIN
 v_es.errnum :=1; v_es.errcode :='E_NOTIMPLEMENTED';
 v_es.errdesc :=v_es.errcode; RETURN v_es;
END $$;
