-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE at the project's root directory
--
--
-- purpose: returns false for invalid transaction isolation (not read committed)

CREATE OR REPLACE FUNCTION SAM.check_isolation()
  RETURNS boolean LANGUAGE plpgSQL SECURITY DEFINER STABLE AS $$
DECLARE
 v_s boolean;
BEGIN
 v_s := pg_catalog.current_setting('transaction_isolation') = 'read committed';
 RETURN v_s;
END $$;
REVOKE ALL ON FUNCTION SAM.check_isolation() FROM PUBLIC;
