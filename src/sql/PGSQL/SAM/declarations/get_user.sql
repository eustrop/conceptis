-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

-- REMEMBER! All functions execulable by PUBLIC by default!
-- this set of functions are safe but another one are not.

-- 0. bind db owner (or some other trusted user) to specified UID and SLEVEL
CREATE OR REPLACE FUNCTION sam.bind_dbo(bigint,smallint) RETURNS VOID
   AS 'insert into SAM.SessionBind values(pg_backend_pid(),session_user,$1,0,$2,''N'',NOW(),null,pg_postmaster_start_time(),null,null)'
    LANGUAGE SQL SECURITY INVOKER;

-- 1. get_xu_user* - inner functions to resolve system user by SAM.User table
CREATE OR REPLACE FUNCTION sam.get_xu_user() RETURNS bigint STABLE
  AS 'select id from SAM.User where db_user = session_user and locked=''N'''
    LANGUAGE SQL SECURITY DEFINER;
--GRANT EXECUTE on function sam.get_xu_user() to tis_users;

CREATE OR REPLACE FUNCTION sam.get_xu_slevel() RETURNS smallint STABLE
  AS 'select slevel from SAM.User where db_user = session_user and locked=''N'''
    LANGUAGE SQL SECURITY DEFINER;
--GRANT EXECUTE on function sam.get_xu_slevel() to tis_users;

CREATE OR REPLACE FUNCTION sam.get_xu_login() RETURNS varchar STABLE
  AS 'select login from SAM.User where db_user = session_user'
    LANGUAGE SQL SECURITY DEFINER;
--GRANT EXECUTE on function sam.get_xu_login() to tis_users;

CREATE OR REPLACE FUNCTION sam.get_xu_lang() RETURNS char(2) STABLE
  AS 'select lang from SAM.User where db_user = session_user'
    LANGUAGE SQL SECURITY DEFINER;
--GRANT EXECUTE on function sam.get_xu_lang() to tis_users;

-- 2. get_xb_user* - inner functions to resolse TIS uset by SAM.SessionBind table
CREATE OR REPLACE FUNCTION sam.get_xb_user() RETURNS bigint STABLE
  AS 'select uid from SAM.SessionBind where db_user = session_user and locked=''N''
      AND pg_pid = pg_backend_pid() AND pg_s_time = pg_postmaster_start_time()
      AND COALESCE(cl_addr = inet_client_addr(),(inet_client_addr() is null))
      AND COALESCE(cl_port = inet_client_port(),(inet_client_port() is null))'
    LANGUAGE SQL SECURITY DEFINER;
--GRANT EXECUTE on function sam.get_xb_user() to tis_users;

CREATE OR REPLACE FUNCTION sam.get_xb_slevel() RETURNS smallint STABLE
  AS 'select slevel from SAM.SessionBind where db_user = session_user and locked=''N''
      AND pg_pid = pg_backend_pid() AND pg_s_time = pg_postmaster_start_time()
      AND COALESCE(cl_addr = inet_client_addr(),(inet_client_addr() is null))
      AND COALESCE(cl_port = inet_client_port(),(inet_client_port() is null))'
    LANGUAGE SQL SECURITY DEFINER;
--GRANT EXECUTE on function sam.get_xb_slevel() to tis_users;

CREATE OR REPLACE FUNCTION sam.get_xb_login() RETURNS varchar STABLE
  AS 'select XU.login from SAM.SessionBind XSB ,SAM.User XU where XU.id=XSB.uid
      AND XSB.db_user = session_user and XSB.locked=''N''
      AND XSB.pg_pid = pg_backend_pid() AND XSB.pg_s_time = pg_postmaster_start_time()
      AND COALESCE(XSB.cl_addr = inet_client_addr(),(inet_client_addr() is null))
      AND COALESCE(XSB.cl_port = inet_client_port(),(inet_client_port() is null))'
    LANGUAGE SQL SECURITY DEFINER;
--GRANT EXECUTE on function sam.get_xb_login() to tis_users;

CREATE OR REPLACE FUNCTION sam.get_xb_lang() RETURNS varchar STABLE
  AS 'select XU.lang from SAM.SessionBind XSB ,SAM.User XU where XU.id=XSB.uid
      AND XSB.db_user = session_user and XSB.locked=''N''
      AND XSB.pg_pid = pg_backend_pid() AND XSB.pg_s_time = pg_postmaster_start_time()
      AND COALESCE(XSB.cl_addr = inet_client_addr(),(inet_client_addr() is null))
      AND COALESCE(XSB.cl_port = inet_client_port(),(inet_client_port() is null))'
    LANGUAGE SQL SECURITY DEFINER;
--GRANT EXECUTE on function sam.get_xb_lang() to tis_users;

-- 3. final step - define get_user* functions for regular use
--    based on previously defined get_x[bu]_user* functions as
--    SELECT COALESCE(get_xb_user(),get_xu_user());
CREATE OR REPLACE FUNCTION sam.get_user() RETURNS bigint STABLE
    AS 'SELECT COALESCE(SAM.get_xb_user(),SAM.get_xu_user())'
    LANGUAGE SQL SECURITY DEFINER;
--  AS 'select id from SAM.User where db_user = session_user and locked=''N'''
--GRANT EXECUTE on function sam.get_user() to tis_users;

CREATE OR REPLACE FUNCTION sam.get_user_slevel() RETURNS smallint STABLE
    AS 'SELECT COALESCE(SAM.get_xb_slevel(),SAM.get_xu_slevel())'
    LANGUAGE SQL SECURITY DEFINER;
--GRANT EXECUTE on function sam.get_user_slevel() to tis_users;

CREATE OR REPLACE FUNCTION sam.get_user_login() RETURNS char(16) STABLE
    AS 'SELECT COALESCE(SAM.get_xb_login(),SAM.get_xu_login())'
    LANGUAGE SQL SECURITY DEFINER;
--GRANT EXECUTE on function sam.get_user_login() to tis_users;

CREATE OR REPLACE FUNCTION sam.get_user_lang() RETURNS char(3) STABLE
  AS 'SELECT COALESCE(SAM.get_xb_lang(),SAM.get_xu_lang())'
    LANGUAGE SQL SECURITY DEFINER;
--GRANT EXECUTE on function sam.get_user_lang() to tis_users;

CREATE OR REPLACE FUNCTION sam.get_user_sid() RETURNS bigint STABLE
  AS 'SELECT sid from SAM.User where id = COALESCE(SAM.get_xb_user(),SAM.get_xu_user())'
    LANGUAGE SQL SECURITY DEFINER;
--GRANT EXECUTE on function sam.get_user_sid() to tis_users;

CREATE OR REPLACE FUNCTION sam.get_user_sid_root() RETURNS bigint STABLE
  AS 'SELECT XS.sid from SAM.User XU, SAM.Scope XS where
       XU.id = COALESCE(SAM.get_xb_user(),SAM.get_xu_user())
       AND XS.id = XU.sid'
    LANGUAGE SQL SECURITY DEFINER;
--GRANT EXECUTE on function sam.get_user_sid_root() to tis_users;

CREATE OR REPLACE FUNCTION sam.get_user_slevel_min() RETURNS smallint STABLE
  AS 'SELECT slevel_min from SAM.User where id = SAM.get_user()'
    LANGUAGE SQL SECURITY DEFINER;
--GRANT EXECUTE on function sam.get_user_slevel_min() to tis_users;

CREATE OR REPLACE FUNCTION sam.get_user_slevel_max() RETURNS smallint STABLE
  AS 'SELECT slevel_max from SAM.User where id = SAM.get_user()'
    LANGUAGE SQL SECURITY DEFINER;
--GRANT EXECUTE on function sam.get_user_slevel_min() to tis_users;
