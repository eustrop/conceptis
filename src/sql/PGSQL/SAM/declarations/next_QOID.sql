-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
-- 2023-04-40 22:27 SAM.next_QOID(sid,qtype) done and tested (39 lines)
-- 2023-04-56 22:59 SAM.check_QOID(v_r.lastid) implemented
--

CREATE OR REPLACE FUNCTION SAM.next_QOID( v_sid	bigint, v_qtype	varchar(14)) RETURNS bigint VOLATILE
  LANGUAGE plpgSQL SECURITY INVOKER as $$
DECLARE
 v_r SAM.RangeSeq%ROWTYPE;
BEGIN
 -- 1) lock required tables
 LOCK TABLE SAM.RangeSeq IN EXCLUSIVE MODE;
 SELECT * from SAM.RangeSeq XRS into v_r WHERE
    XRS.sid=v_sid AND XRS.qtype=v_qtype and (XRS.lastid is NULL OR XRS.lastid<XRS.rend)
    ORDER by XRS.sorder;
 -- 2) return null if no sequence
 IF NOT FOUND THEN
  RETURN null;
 END IF;
 -- 3) create next id
 IF v_r.lastid IS NULL THEN
  v_r.lastid := v_r.start;
 ELSE
  v_r.lastid := v_r.lastid + 1;
 END IF;
 -- 3) check that new id fit onto range (can be disabled for more performance)
 IF (v_r.lastid < v_r.start OR v_r.lastid > v_r.rend) THEN
  RETURN null;
 END IF;
 -- 4) update sequence
 UPDATE SAM.RangeSeq XRS SET lastid = v_r.lastid, ts = NOW()
  WHERE XRS.start=v_r.start and XRS.rend=v_r.rend ;
 -- 5) finaly - return new id
 RETURN  SAM.check_QOID(v_r.lastid);
END $$;
