-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE at the project's root directory
--
-- check_loglvl() check eclass and sid for "write it or not"
-- write_auditlog() - write real record to log store
-- do_auditlog_enter() - inital record
-- do_auditlog_capuse() - capability usage event
-- do_auditlog_capuse_sole() - main capuse, logging delyed to .._exit()
-- do_auditlog_da() - data access record
-- do_auditlog_da_sole() - main da event, logging delyed to .._exit()
-- do_auditlog_rename() - object renamed
-- do_auditlog_field() - significant field changed
-- do_auditlog_msg() - some record with textual part
-- do_auditlog_exit() - final record
--
-- cast_auditstate2auditevent() convert SAm.auditstate to SAM.auditevent

BEGIN TRANSACTION;
-- ****************************************************************************
-- * check event class for write event or skip off.
-- * messages at unmaskable classes will logged in any case; at highest
-- * debug classes - never; other cases formally should be looked up over
-- * the SAM.Scope table (this is simpler then per each object type
-- * ScopePolicy from Manifesto)
CREATE OR REPLACE FUNCTION SAM.check_loglvl(v_eclass char(1),v_sid bigint)
	RETURNS BOOLEAN LANGUAGE plpgSQL STABLE SECURITY DEFINER AS $$
BEGIN
 IF v_eclass <= '4' THEN RETURN TRUE; END IF;
 IF v_eclass >= '5' THEN RETURN FALSE; END IF;
 PERFORM id FROM SAM.Scope where id = COALESCE(v_sid,0)
  AND auditlvl >= v_eclass;
 RETURN FOUND;
 RETURN TRUE;
END $$;

-- ****************************************************************************
-- * select topmost eclass. '0' is topmost, '4' is the least interesting
-- *
CREATE OR REPLACE FUNCTION SAM.get_topmost_eclass(v_e1 char(1),v_e2 char(1))
	RETURNS char(1) LANGUAGE plpgSQL IMMUTABLE AS $$
BEGIN
 IF v_e1 IS NULL THEN RETURN v_e2; END IF;
 IF v_e2 IS NULL THEN RETURN v_e1; END IF;
 IF v_e2 > v_e1 THEN RETURN v_e1; END IF;
 RETURN v_e2;
END $$;

CREATE OR REPLACE FUNCTION SAM.get_topmost_eclass(v_e1 char(1),v_e2 char(1),
 v_e3 char(1)) RETURNS char(1) LANGUAGE plpgSQL IMMUTABLE AS $$
BEGIN
 RETURN SAM.get_topmost_eclass(SAM.get_topmost_eclass(v_e1,v_e2),v_e3);
END $$;

-- ****************************************************************************
-- * write prepared event into the log table and possible somewhere else.
-- * all writes must come over this point
CREATE OR REPLACE FUNCTION SAM.write_auditlog( v_a SAM.AuditEvent )
	RETURNS VOID LANGUAGE plpgSQL SECURITY DEFINER AS $$
BEGIN
 IF NOT SAM.check_loglvl(v_a.ec,v_a.sid) THEN RETURN; END IF;
 v_a.ts := clock_timestamp(); -- NOW() trans-stable! use clock_timestamp() for real time
 /* fill mandatory fields
 v_a.id := COALESCE(v_a.id,nextval('SAM.AuditEvent_seq'));
 v_a.sn := COALESCE(v_a.sn,0);
 v_a.ec := COALESCE(v_a.ec,'?');
 v_a.ea := COALESCE(v_a.ea,'?');
 v_a.subsys := COALESCE(v_a.subsys,'?');
 v_a.obj_type := COALESCE(v_a.obj_type,'?');
 v_a.errnum := COALESCE(v_a.errnum,-1);
 */
-- write to server's log
 RAISE LOG 'SAM.AuditEvent:%,%,%,%,%,%,%,%,%,%,%,%,%,%,%,%,%,%,%,%',
  v_a.id, v_a.sn, v_a.ts, v_a.ec, v_a.ea, v_a.subsys,
  v_a.obj_type, v_a.errnum, v_a.uid, v_a.slevel, v_a.capcode,v_a.capsid,
  v_a.sid, v_a.ZOID, v_a.ZVER, v_a.ZTYPE, v_a.ZID, v_a.ZLVL,
  v_a.proc, v_a.einfo;
-- write to DB table
  INSERT INTO SAM.AuditEvent VALUES(v_a.*);
END $$;
REVOKE ALL ON FUNCTION SAM.write_auditlog( v_a SAM.AuditEvent ) FROM PUBLIC;

-- ****************************************************************************
-- * CAST SAM.AuditState as SAM.AuditEvent
CREATE OR REPLACE FUNCTION SAM.cast_auditstate2auditevent
  (v_as SAM.auditstate)
  RETURNS SAM.AuditEvent LANGUAGE plpgSQL SECURITY DEFINER AS $$
DECLARE
 v_ae SAM.AuditEvent;
BEGIN
	-- eventrecord fields :
	v_ae.id := v_as.id;
	v_ae.sn := v_as.sn;
	v_ae.ea := v_as.eaction;
	v_ae.subsys := v_as.subsys;
	v_ae.obj_type := v_as.obj_type;
	-- user info
	v_ae.uid := v_as.uid;
	v_ae.slevel := v_as.slevel;
	v_ae.capcode := v_as.capcode;
	v_ae.capsid := v_as.capsid;
	-- object info;
	v_ae.sid := v_as.sid;
	v_ae.ZOID := v_as.ZOID;
	v_ae.ZVER := v_as.ZVER;
	v_ae.ZTYPE := v_as.ZTYPE;
	v_ae.ZID := v_as.ZID;
	v_ae.ZLVL := v_as.ZLVL;
	-- debug info
	v_ae.proc := v_as.proc;
 RETURN v_ae;
END $$;
REVOKE ALL ON FUNCTION SAM.cast_auditstate2auditevent
  (v_as SAM.auditstate) FROM PUBLIC;

-- ****************************************************************************
-- * write some message to auditlog
-- *
CREATE OR REPLACE FUNCTION SAM.do_auditlog_msg(v_as INOUT SAM.auditstate,
	v_eclass char(1), v_errnum smallint, v_einfo text)
   RETURNS SAM.auditstate LANGUAGE plpgSQL SECURITY DEFINER AS $$
DECLARE
 v_ae SAM.AuditEvent;
BEGIN
 IF v_as.id IS NULL THEN
  v_as.id := nextval('SAM.AuditEvent_seq');
  v_as.sn := 1;
 END IF;
 --
 v_ae := SAM.cast_auditstate2auditevent(v_as);
 v_ae.ec := COALESCE(v_eclass,'4'); -- debug
 v_ae.errnum := COALESCE(v_errnum,-1); -- info
 v_ae.einfo := v_einfo;
 PERFORM SAM.write_auditlog(v_ae);
 --
 v_as.sn = v_as.sn + 1;
 RETURN;
END $$;
REVOKE ALL ON FUNCTION SAM.do_auditlog_msg(v_as INOUT SAM.auditstate,
        v_eclass char(1), v_errnum smallint, v_einfo text) FROM PUBLIC;

CREATE OR REPLACE FUNCTION SAM.do_auditlog_msg(v_as INOUT SAM.auditstate,
	v_eclass char(1), v_errnum integer, v_einfo text)
   RETURNS SAM.auditstate LANGUAGE plpgSQL SECURITY DEFINER AS $$
BEGIN
 v_as:=SAM.do_auditlog_msg(v_as,v_eclass,CAST(v_errnum as smallint),v_einfo);
 RETURN;
END $$;
REVOKE ALL ON FUNCTION  SAM.do_auditlog_msg(v_as INOUT SAM.auditstate,
        v_eclass char(1), v_errnum integer, v_einfo text) FROM PUBLIC;

-- ****************************************************************************
-- * inital log record
-- *
CREATE OR REPLACE FUNCTION SAM.do_auditlog_enter(v_as INOUT SAM.auditstate)
   RETURNS SAM.auditstate LANGUAGE plpgSQL SECURITY DEFINER AS $$
DECLARE
 i integer;
 v_ae SAM.AuditEvent;
BEGIN
 v_as.id := nextval('SAM.AuditEvent_seq');
 v_as.sn := 1;
 SELECT id,slevel INTO v_as.uid, v_as.slevel FROM SAM.User WHERE db_user=session_user; 
 --
 v_ae := SAM.cast_auditstate2auditevent(v_as);
 v_ae.ec := '4'; -- debug
 v_ae.errnum := -1; -- info
 v_ae.einfo := 'enter'; -- noise 
 PERFORM SAM.write_auditlog(v_ae);
 --
 v_as.sn = v_as.sn + 1;
 RETURN;
END $$;
REVOKE ALL ON FUNCTION SAM.do_auditlog_enter(v_as INOUT SAM.auditstate)
 FROM PUBLIC;

-- ****************************************************************************
-- * capability used
-- *
CREATE OR REPLACE FUNCTION SAM.do_auditlog_capuse(v_as INOUT SAM.auditstate,
	v_eclass char(1), v_capcode char(16),v_capsid bigint)
   RETURNS SAM.auditstate LANGUAGE plpgSQL SECURITY DEFINER AS $$
DECLARE
 i integer;
 v_ae SAM.AuditEvent;
BEGIN
 v_ae := SAM.cast_auditstate2auditevent(v_as);
 v_ae.capcode := v_capcode;
 v_ae.capsid := COALESCE(v_capsid,0); -- global capability by default
 -- eaction
 v_ae.ea := 'XC'; -- capability used (instead of default v_as.eaction)
 -- eclass
 v_ae.ec := COALESCE(v_eclass,'3'); -- msg at "warning, maskable"
 v_as.da_eclass := sam.get_topmost_eclass(v_as.da_eclass,v_ae.ec);
 -- result
 v_ae.errnum := 0; -- success use
 v_ae.einfo := 'capability used:'||COALESCE(v_capcode,'')||',scope:'||
  COALESCE(''||v_capsid,'')||';orig ea:'||COALESCE(v_as.eaction,''); -- noise 
 --
 PERFORM SAM.write_auditlog(v_ae);
 --
 v_as.sn = v_as.sn + 1;
 RETURN;
END $$;
REVOKE ALL ON FUNCTION SAM.do_auditlog_capuse(v_as INOUT SAM.auditstate,
        v_eclass char(1), v_capcode char(16),v_capsid bigint) FROM PUBLIC;

-- ****************************************************************************
-- * capability use failed
-- *
CREATE OR REPLACE FUNCTION SAM.do_auditlog_capuse_fail(v_as INOUT SAM.auditstate,
	v_eclass char(1), v_capcode char(16),v_capsid bigint)
   RETURNS SAM.auditstate LANGUAGE plpgSQL SECURITY DEFINER AS $$
DECLARE
 i integer;
 v_ae SAM.AuditEvent;
BEGIN
 v_ae := SAM.cast_auditstate2auditevent(v_as);
 v_ae.capcode := v_capcode;
 v_ae.capsid := COALESCE(v_capsid,0); -- global capability by default
 -- eaction
 v_ae.ea := 'XC'; -- capability used (instead of default v_as.eaction)
 -- eclass
 v_ae.ec := COALESCE(v_eclass,'3'); -- msg at "warning, maskable"
 v_as.da_eclass := sam.get_topmost_eclass(v_as.da_eclass,v_ae.ec);
 -- result
 v_ae.errnum := 13; -- E_NOCAPABILITY
 v_ae.einfo := 'capability use faild:'||COALESCE(v_capcode,'')||',scope:'||
  COALESCE(''||v_capsid,'')||';orig ea:'||COALESCE(v_as.eaction,''); -- noise 
 --
 PERFORM SAM.write_auditlog(v_ae);
 --
 v_as.sn = v_as.sn + 1;
 RETURN;
END $$;
REVOKE ALL ON FUNCTION SAM.do_auditlog_capuse_fail(v_as INOUT SAM.auditstate,
        v_eclass char(1), v_capcode char(16),v_capsid bigint) FROM PUBLIC;

-- ****************************************************************************
-- * capability used. this is only used and main capability for request
-- * Logging delayed to the final SAM.do_auditlog_exit() (normally)
-- *
CREATE OR REPLACE FUNCTION SAM.do_auditlog_capuse_sole(v_as INOUT
   SAM.auditstate, v_eclass char(1), v_capcode char(16),v_capsid bigint)
   RETURNS SAM.auditstate LANGUAGE plpgSQL SECURITY DEFINER AS $$
DECLARE
 i integer;
-- v_ae SAM.AuditEvent;
BEGIN
 IF NOT v_as.capcode IS NULL THEN -- non sole use!
  v_as := SAM.do_auditlog_capuse(v_as,v_eclass,v_capcode,v_sid);
 ELSE
  v_as.capcode := v_capcode;
  v_as.capsid := COALESCE(v_capsid,0);
  v_as.da_eclass := SAM.get_topmost_eclass(v_as.da_eclass,v_eclass,'3');
  -- debug noise:
  v_as:=SAM.do_auditlog_msg(v_as,'4',-1,'capuse_sole:'||COALESCE(v_capcode,'')||
  	',scope:'||COALESCE(''||v_capsid,'')||',eclass:'||COALESCE(v_eclass,''));
 END IF;
 RETURN;
END $$;
REVOKE ALL ON FUNCTION SAM.do_auditlog_capuse_sole(v_as INOUT
   SAM.auditstate, v_eclass char(1), v_capcode char(16),v_capsid bigint)
   FROM PUBLIC;

-- ****************************************************************************
-- * data access. nondefault data object used.
-- *
CREATE OR REPLACE FUNCTION SAM.do_auditlog_da(v_as INOUT SAM.auditstate,
	v_eclass char(1), v_eaction char(2), v_obj_type char(20),
	v_sid bigint, v_ZID bigint, v_msg text)
   RETURNS SAM.auditstate LANGUAGE plpgSQL SECURITY DEFINER AS $$
DECLARE
 v_ae SAM.AuditEvent;
BEGIN
 v_ae := SAM.cast_auditstate2auditevent(v_as);
 -- eclass
 v_ae.ec := COALESCE(v_eclass,'2'); -- msg at "data access maskable"
  v_as.da_eclass := SAM.get_topmost_eclass(v_as.da_eclass,v_ae.ec);
 -- eaction
 v_ae.ea := COALESCE(v_eaction,v_ae.ea);
 -- object identification
 v_ae.obj_type := COALESCE(v_obj_type,v_ae.obj_type);
 v_ae.sid := COALESCE(v_sid,v_ae.sid);
 v_ae.ZID := COALESCE(v_ZID,v_ae.ZID);
 -- result
 v_ae.errnum := 0; -- success use
 v_ae.einfo := v_msg; -- noise 
 --
 PERFORM SAM.write_auditlog(v_ae);
 --
 v_as.sn = v_as.sn + 1;
 RETURN;
END $$;
REVOKE ALL ON FUNCTION SAM.do_auditlog_da(v_as INOUT SAM.auditstate,
        v_eclass char(1), v_eaction char(2), v_obj_type char(20),
		v_sid bigint, v_ZID bigint, v_msg text)
	FROM PUBLIC;

CREATE OR REPLACE FUNCTION SAM.do_auditlog_da(v_as INOUT SAM.auditstate,
	v_eclass char(1), v_eaction char(2), v_obj_type char(20),
	v_sid bigint, v_ZID bigint)
   RETURNS SAM.auditstate LANGUAGE plpgSQL SECURITY DEFINER AS $$
BEGIN
 v_as := SAM.do_auditlog_da(v_as,v_eclass,v_eaction,v_obj_type,
  v_sid,v_ZID,CAST(null as text)); RETURN;
END $$;
REVOKE ALL ON FUNCTION SAM.do_auditlog_da(v_as INOUT SAM.auditstate,
        v_eclass char(1), v_eaction char(2), v_obj_type char(20),
		v_sid bigint, v_ZID bigint) FROM PUBLIC;

CREATE OR REPLACE FUNCTION SAM.do_auditlog_da(v_as INOUT SAM.auditstate,
	v_eclass char(1))
   RETURNS SAM.auditstate LANGUAGE plpgSQL SECURITY DEFINER AS $$
BEGIN
 v_as := SAM.do_auditlog_da(v_as,v_eclass,null,null,null,null); RETURN;
END $$;
REVOKE ALL ON FUNCTION SAM.do_auditlog_da(v_as INOUT SAM.auditstate,
        v_eclass char(1)) FROM PUBLIC;

-- ****************************************************************************
-- * data access. default data object used. Only this object accessed and
-- * it's main target of this request. Logging delayed to the final
-- * SAM.do_auditlog_exit() call which normally performed closely 
-- *
CREATE OR REPLACE FUNCTION SAM.do_auditlog_da_sole(v_as INOUT SAM.auditstate,
	v_eclass char(1), v_eaction char(2), v_obj_type char(20),
	v_sid bigint, v_ZID bigint, v_msg text)
   RETURNS SAM.auditstate LANGUAGE plpgSQL SECURITY DEFINER AS $$
DECLARE
-- v_ae SAM.AuditEvent;
BEGIN
 IF v_as.da_sole THEN -- none-sole use
  v_as := SAM.do_auditlog_da(v_as,v_eclass,v_eaction,
	v_obj_type,v_sid,v_ZID,v_msg);
 ELSE
  v_as.da_eclass := SAM.get_topmost_eclass(v_as.da_eclass,v_eclass,'2');
  v_as.eaction := COALESCE(v_eaction,v_as.eaction);
  -- object identification
  v_as.obj_type := COALESCE(v_obj_type,v_as.obj_type);
  v_as.sid := COALESCE(v_sid,v_as.sid);
  v_as.ZID := COALESCE(v_ZID,v_as.ZID);
  v_as.da_msg := v_msg;
  v_as:=SAM.do_auditlog_msg(v_as,'4',-1,'da_sole:eclass='||
      COALESCE(v_eclass,'')||COALESCE(':msg=' || v_msg,''));
 END IF;
 RETURN;
END $$;
REVOKE ALL ON FUNCTION SAM.do_auditlog_da_sole(v_as INOUT SAM.auditstate,
        v_eclass char(1), v_eaction char(2), v_obj_type char(20),
	v_sid bigint, v_ZID bigint, v_msg text) FROM PUBLIC;

CREATE OR REPLACE FUNCTION SAM.do_auditlog_da_sole(v_as INOUT SAM.auditstate,
	v_eclass char(1), v_eaction char(2), v_obj_type char(20),
	v_sid bigint, v_ZID bigint)
   RETURNS SAM.auditstate LANGUAGE plpgSQL SECURITY DEFINER AS $$
BEGIN
 v_as := SAM.do_auditlog_da_sole(v_as,v_eclass,v_eaction,
	v_obj_type,v_sid,v_ZID,null);
END $$;
REVOKE ALL ON FUNCTION SAM.do_auditlog_da_sole(v_as INOUT SAM.auditstate,
        v_eclass char(1), v_eaction char(2), v_obj_type char(20),
	v_sid bigint, v_ZID bigint) FROM PUBLIC;

-- ****************************************************************************
-- * final log record
-- *
CREATE OR REPLACE FUNCTION SAM.do_auditlog_exit(v_as INOUT SAM.auditstate,
	v_es SAM.execstatus)
   RETURNS SAM.auditstate LANGUAGE plpgSQL SECURITY DEFINER AS $$
DECLARE
 i integer;
 v_ae SAM.AuditEvent;
BEGIN
 v_as.sn := 0;
 v_ae := SAM.cast_auditstate2auditevent(v_as);
 v_ae.errnum := v_es.errnum;
 v_ae.einfo := v_es.errdesc || COALESCE('; '||v_as.da_msg,'');
 IF v_ae.errnum <= 0 and NOT v_as.da_sole THEN
  v_ae.ec := '4'; -- msg at debug level
 ELSE
  v_ae.ec := SAM.get_topmost_eclass('3',v_as.da_eclass);
 END IF;
 PERFORM SAM.write_auditlog(v_ae);
 RETURN;
END $$;
REVOKE ALL ON FUNCTION SAM.do_auditlog_exit(v_as INOUT SAM.auditstate,
        v_es SAM.execstatus) FROM PUBLIC;

-- ****************************************************************************
-- * write information about field's changes to auditlog
-- *
CREATE OR REPLACE FUNCTION SAM.do_auditlog_fchanges(v_as INOUT SAM.auditstate,
	v_eclass char(1), v_errnum smallint,
	v_field text, v_oldvalue text, v_newvalue text)
   RETURNS SAM.auditstate LANGUAGE plpgSQL SECURITY DEFINER AS $$
DECLARE
 v_ae SAM.AuditEvent;
BEGIN
 IF v_as.id IS NULL THEN
  v_as.id := nextval('SAM.AuditEvent_seq');
  v_as.sn := 1;
 END IF;
 --
 v_ae := SAM.cast_auditstate2auditevent(v_as);
 v_ae.ec := COALESCE(v_eclass,'4'); -- debug
 v_ae.ea := 'F'; -- field changed (instead of default)
 v_ae.errnum := COALESCE(v_errnum,-1); -- info
 v_ae.einfo := 'changed field :' || COALESCE(v_field,'%unknown%') ||
 ' from: ' || COALESCE(v_oldvalue,'null') || ' to:' ||
 COALESCE(v_newvalue,'null') ;
 -- thinking about quote_literal(COALESCE(v_newvalue,'null'));
 PERFORM SAM.write_auditlog(v_ae);
 --
 v_as.sn = v_as.sn + 1;
 RETURN;
END $$;
REVOKE ALL ON FUNCTION SAM.do_auditlog_fchanges(v_as INOUT SAM.auditstate,
        v_eclass char(1), v_errnum smallint,
	v_field text, v_oldvalue text, v_newvalue text) FROM PUBLIC;

-- ****************************************************************************
-- * data access check.
-- *
CREATE OR REPLACE FUNCTION SAM.do_auditlog_ac(v_as INOUT SAM.auditstate,
	v_eclass char(1), v_eaction char(2), v_obj_type char(20),
	v_errnum smallint, v_sid bigint, v_ZID bigint, v_ZLVL smallint,
	v_msg text)
   RETURNS SAM.auditstate LANGUAGE plpgSQL SECURITY DEFINER AS $$
DECLARE
 v_ae SAM.AuditEvent;
BEGIN
 v_ae := SAM.cast_auditstate2auditevent(v_as);
 -- eclass
 v_ae.ec := COALESCE(v_eclass,'4'); -- msg at "debug"
  v_as.da_eclass := SAM.get_topmost_eclass(v_as.da_eclass,v_ae.ec);
 -- eaction
 v_ae.ea := COALESCE(v_eaction,'A?');
 -- object identification
 v_ae.obj_type := COALESCE(v_obj_type,v_ae.obj_type);
 v_ae.sid := COALESCE(v_sid,v_ae.sid);
 v_ae.ZID := COALESCE(v_ZID,v_ae.ZID);
 v_ae.ZLVL := COALESCE(v_ZLVL,v_ae.ZLVL);
 -- result
 v_ae.errnum := COALESCE(v_errnum,'-1'); -- info
 v_ae.einfo := v_msg; -- noise 
 --
 PERFORM SAM.write_auditlog(v_ae);
 --
 v_as.sn = v_as.sn + 1;
 RETURN;
END $$;
REVOKE ALL ON FUNCTION SAM.do_auditlog_ac(v_as INOUT SAM.auditstate,
        v_eclass char(1), v_eaction char(2), v_obj_type char(20),
	v_errnum smallint, v_sid bigint, v_ZID bigint, v_ZLVL smallint,
	v_msg text) FROM PUBLIC;

CREATE OR REPLACE FUNCTION SAM.do_auditlog_ac(v_as INOUT SAM.auditstate,
	v_eclass char(1), v_eaction char(2), v_obj_type char(20),
	v_errnum smallint, v_sid bigint, v_ZID bigint, v_ZLVL smallint)
   RETURNS SAM.auditstate LANGUAGE plpgSQL SECURITY DEFINER AS $$
BEGIN
 v_as := SAM.do_auditlog_ac(v_as,v_eclass,v_eaction,v_obj_type,v_errnum,
	v_sid,v_ZID,v_ZLVL,CAST(null as text)); RETURN;
END $$;
REVOKE ALL ON FUNCTION SAM.do_auditlog_ac(v_as INOUT SAM.auditstate,
        v_eclass char(1), v_eaction char(2), v_obj_type char(20),
	v_errnum smallint, v_sid bigint, v_ZID bigint, v_ZLVL smallint)
	FROM PUBLIC;

CREATE OR REPLACE FUNCTION SAM.do_auditlog_ac(v_as INOUT SAM.auditstate,
	v_eclass char(1), v_eaction char(2), v_obj_type char(20),
	v_sid bigint, v_ZID bigint, v_ZLVL smallint, v_ps SAM.procstate)
   RETURNS SAM.auditstate LANGUAGE plpgSQL SECURITY DEFINER AS $$
DECLARE
 v_es SAM.execstatus;
BEGIN
 IF v_ps.success THEN
  v_as := SAM.do_auditlog_ac(v_as,v_eclass,v_eaction,v_obj_type,
	CAST(0 AS smallint),v_sid,v_ZID,v_ZLVL,'access allowed');
 ELSE
  v_es := v_ps.e;
  v_as := SAM.do_auditlog_ac(v_as,v_eclass,v_eaction,v_obj_type,
	CAST(v_es.errnum as smallint),v_sid,v_ZID,v_ZLVL,
	CAST(v_es.errdesc AS text));
 END IF;
RETURN;
END $$;
REVOKE ALL ON FUNCTION SAM.do_auditlog_ac(v_as INOUT SAM.auditstate,
        v_eclass char(1), v_eaction char(2), v_obj_type char(20),
	v_sid bigint, v_ZID bigint, v_ZLVL smallint, v_ps SAM.procstate)
	FROM PUBLIC;
COMMIT TRANSACTION;
