-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE at the project's root directory
--
-- $Id$
--

CREATE OR REPLACE FUNCTION SAM.QC_UserGroup(
	v_as	SAM.auditstate,
	v_r	SAM.UserGroup
) RETURNS SAM.procstate VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 v_es SAM.execstatus;
BEGIN
 v_es.errnum :=1; v_es.errcode :='E_NOTIMPLEMENTED';
 v_es.errdesc := v_es.errcode || ',QC_UserGroup()'; v_ps.e := v_es;
 v_ps.a := v_as; v_ps.success := FALSE; RETURN v_ps;
END $$;
