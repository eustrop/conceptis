-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE at the project's root directory
--
-- $Id$
--
-- purpose: returns true if current user has requested capability at target scope
--	or at MAIN scope with sid=0 if omitted

CREATE OR REPLACE FUNCTION SAM.check_capability(v_capcode char(16), v_sid bigint)
  RETURNS boolean LANGUAGE plpgSQL SECURITY DEFINER STABLE AS $$
BEGIN
 -- this dummy will be overloaded by load_tis next stage
 RETURN FALSE;
END $$;

CREATE OR REPLACE FUNCTION SAM.check_capability(v_capcode char(16))
  RETURNS boolean LANGUAGE plpgSQL SECURITY DEFINER STABLE AS $$
BEGIN
 -- this dummy will be overloaded by load_tis next stage
 RETURN FALSE;
END $$;
