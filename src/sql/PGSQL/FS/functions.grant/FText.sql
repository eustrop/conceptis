REVOKE ALL ON FUNCTION FS.create_FText(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_titlekey	varchar(255),
	v_chunk	text
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION FS.create_FText(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_titlekey	varchar(255),
	v_chunk	text
) TO tis_users;
GRANT EXECUTE ON FUNCTION FS.update_FText(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_titlekey	varchar(255),
	v_chunk	text
) TO tis_users;
REVOKE EXECUTE ON FUNCTION FS.delete_FText(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION FS.delete_FText(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) TO tis_users;
REVOKE ALL ON FUNCTION FS.QC_FT(
	v_as	SAM.auditstate,
	v_r	FS.FText
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION FS.commit_FT(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION FS.rollback_FT(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION FS.move_FT(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION FS.delete_object_FT(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION FS.cmp_FT(
	v_r	FS.FText,
	v_ro	FS.FText
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION FS.rdelete_FT(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) FROM PUBLIC;
