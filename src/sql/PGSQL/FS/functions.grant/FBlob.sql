REVOKE ALL ON FUNCTION FS.create_FBlob(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_chunk	bytea,
	v_no	bigint,
	v_size	bigint,
	v_crc32	int
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION FS.create_FBlob(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_chunk	bytea,
	v_no	bigint,
	v_size	bigint,
	v_crc32	int
) TO tis_users;
GRANT EXECUTE ON FUNCTION FS.update_FBlob(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_chunk	bytea,
	v_no	bigint,
	v_size	bigint,
	v_crc32	int
) TO tis_users;
REVOKE EXECUTE ON FUNCTION FS.delete_FBlob(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION FS.delete_FBlob(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) TO tis_users;
REVOKE ALL ON FUNCTION FS.QC_FB(
	v_as	SAM.auditstate,
	v_r	FS.FBlob
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION FS.commit_FB(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION FS.rollback_FB(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION FS.move_FB(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION FS.delete_object_FB(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION FS.cmp_FB(
	v_r	FS.FBlob,
	v_ro	FS.FBlob
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION FS.rdelete_FB(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) FROM PUBLIC;
