REVOKE ALL ON FUNCTION FS.create_FFile(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_name	varchar(255),
	v_type	char(1),
	v_extstore	char(1),
	v_mimetype	varchar(127),
	v_descr	varchar(1024),
	v_b_size	bigint,
	v_b_chcnt	bigint,
	v_b_algo	varchar(6),
	v_b_digest	varchar(127),
	v_t_size	bigint,
	v_t_chcnt	bigint,
	v_t_algo	varchar(6),
	v_t_digest	varchar(127)
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION FS.create_FFile(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_name	varchar(255),
	v_type	char(1),
	v_extstore	char(1),
	v_mimetype	varchar(127),
	v_descr	varchar(1024),
	v_b_size	bigint,
	v_b_chcnt	bigint,
	v_b_algo	varchar(6),
	v_b_digest	varchar(127),
	v_t_size	bigint,
	v_t_chcnt	bigint,
	v_t_algo	varchar(6),
	v_t_digest	varchar(127)
) TO tis_users;
GRANT EXECUTE ON FUNCTION FS.update_FFile(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_name	varchar(255),
	v_type	char(1),
	v_extstore	char(1),
	v_mimetype	varchar(127),
	v_descr	varchar(1024),
	v_b_size	bigint,
	v_b_chcnt	bigint,
	v_b_algo	varchar(6),
	v_b_digest	varchar(127),
	v_t_size	bigint,
	v_t_chcnt	bigint,
	v_t_algo	varchar(6),
	v_t_digest	varchar(127)
) TO tis_users;
REVOKE EXECUTE ON FUNCTION FS.delete_FFile(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION FS.delete_FFile(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) TO tis_users;
REVOKE ALL ON FUNCTION FS.QC_FF(
	v_as	SAM.auditstate,
	v_r	FS.FFile
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION FS.commit_FF(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION FS.rollback_FF(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION FS.move_FF(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION FS.delete_object_FF(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION FS.cmp_FF(
	v_r	FS.FFile,
	v_ro	FS.FFile
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION FS.rdelete_FF(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) FROM PUBLIC;
