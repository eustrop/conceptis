REVOKE ALL ON FUNCTION FS.create_FDir(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_f_id	bigint,
	v_fname	varchar(255),
	v_mimetype	varchar(127),
	v_descr	varchar(1024)
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION FS.create_FDir(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_f_id	bigint,
	v_fname	varchar(255),
	v_mimetype	varchar(127),
	v_descr	varchar(1024)
) TO tis_users;
GRANT EXECUTE ON FUNCTION FS.update_FDir(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_f_id	bigint,
	v_fname	varchar(255),
	v_mimetype	varchar(127),
	v_descr	varchar(1024)
) TO tis_users;
REVOKE EXECUTE ON FUNCTION FS.delete_FDir(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION FS.delete_FDir(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) TO tis_users;
REVOKE ALL ON FUNCTION FS.QC_FD(
	v_as	SAM.auditstate,
	v_r	FS.FDir
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION FS.commit_FD(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION FS.rollback_FD(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION FS.move_FD(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION FS.delete_object_FD(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION FS.cmp_FD(
	v_r	FS.FDir,
	v_ro	FS.FDir
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION FS.rdelete_FD(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) FROM PUBLIC;
