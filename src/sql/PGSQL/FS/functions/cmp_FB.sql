-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: FS
-- OBJECT: File (F) RECORD: FBlob (FB)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION FS.cmp_FB(
	v_r	FS.FBlob,
	v_ro	FS.FBlob
) RETURNS boolean STABLE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
BEGIN
 IF v_r.chunk <> v_ro.chunk THEN RETURN FALSE; END IF;
  IF (v_r.chunk IS NULL OR v_ro.chunk IS NULL) AND
  NOT COALESCE(v_r.chunk,v_ro.chunk) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.no <> v_ro.no THEN RETURN FALSE; END IF;
  IF (v_r.no IS NULL OR v_ro.no IS NULL) AND
  NOT COALESCE(v_r.no,v_ro.no) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.size <> v_ro.size THEN RETURN FALSE; END IF;
  IF (v_r.size IS NULL OR v_ro.size IS NULL) AND
  NOT COALESCE(v_r.size,v_ro.size) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.crc32 <> v_ro.crc32 THEN RETURN FALSE; END IF;
  IF (v_r.crc32 IS NULL OR v_ro.crc32 IS NULL) AND
  NOT COALESCE(v_r.crc32,v_ro.crc32) IS NULL THEN RETURN FALSE; END IF;
 RETURN TRUE; -- both records equal
END $$;
COMMIT TRANSACTION;
