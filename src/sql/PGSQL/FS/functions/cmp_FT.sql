-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: FS
-- OBJECT: File (F) RECORD: FText (FT)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION FS.cmp_FT(
	v_r	FS.FText,
	v_ro	FS.FText
) RETURNS boolean STABLE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
BEGIN
 IF v_r.titlekey <> v_ro.titlekey THEN RETURN FALSE; END IF;
  IF (v_r.titlekey IS NULL OR v_ro.titlekey IS NULL) AND
  NOT COALESCE(v_r.titlekey,v_ro.titlekey) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.chunk <> v_ro.chunk THEN RETURN FALSE; END IF;
  IF (v_r.chunk IS NULL OR v_ro.chunk IS NULL) AND
  NOT COALESCE(v_r.chunk,v_ro.chunk) IS NULL THEN RETURN FALSE; END IF;
 RETURN TRUE; -- both records equal
END $$;
COMMIT TRANSACTION;
