-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: FS
-- OBJECT: File (F) RECORD: FDir (FD)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION FS.cmp_FD(
	v_r	FS.FDir,
	v_ro	FS.FDir
) RETURNS boolean STABLE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
BEGIN
 IF v_r.f_id <> v_ro.f_id THEN RETURN FALSE; END IF;
  IF (v_r.f_id IS NULL OR v_ro.f_id IS NULL) AND
  NOT COALESCE(v_r.f_id,v_ro.f_id) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.fname <> v_ro.fname THEN RETURN FALSE; END IF;
  IF (v_r.fname IS NULL OR v_ro.fname IS NULL) AND
  NOT COALESCE(v_r.fname,v_ro.fname) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.mimetype <> v_ro.mimetype THEN RETURN FALSE; END IF;
  IF (v_r.mimetype IS NULL OR v_ro.mimetype IS NULL) AND
  NOT COALESCE(v_r.mimetype,v_ro.mimetype) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.descr <> v_ro.descr THEN RETURN FALSE; END IF;
  IF (v_r.descr IS NULL OR v_ro.descr IS NULL) AND
  NOT COALESCE(v_r.descr,v_ro.descr) IS NULL THEN RETURN FALSE; END IF;
 RETURN TRUE; -- both records equal
END $$;
COMMIT TRANSACTION;
