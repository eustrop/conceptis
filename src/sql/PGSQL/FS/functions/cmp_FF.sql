-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: FS
-- OBJECT: File (F) RECORD: FFile (FF)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION FS.cmp_FF(
	v_r	FS.FFile,
	v_ro	FS.FFile
) RETURNS boolean STABLE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
BEGIN
 IF v_r.name <> v_ro.name THEN RETURN FALSE; END IF;
  IF (v_r.name IS NULL OR v_ro.name IS NULL) AND
  NOT COALESCE(v_r.name,v_ro.name) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.type <> v_ro.type THEN RETURN FALSE; END IF;
  IF (v_r.type IS NULL OR v_ro.type IS NULL) AND
  NOT COALESCE(v_r.type,v_ro.type) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.extstore <> v_ro.extstore THEN RETURN FALSE; END IF;
  IF (v_r.extstore IS NULL OR v_ro.extstore IS NULL) AND
  NOT COALESCE(v_r.extstore,v_ro.extstore) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.mimetype <> v_ro.mimetype THEN RETURN FALSE; END IF;
  IF (v_r.mimetype IS NULL OR v_ro.mimetype IS NULL) AND
  NOT COALESCE(v_r.mimetype,v_ro.mimetype) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.descr <> v_ro.descr THEN RETURN FALSE; END IF;
  IF (v_r.descr IS NULL OR v_ro.descr IS NULL) AND
  NOT COALESCE(v_r.descr,v_ro.descr) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.b_size <> v_ro.b_size THEN RETURN FALSE; END IF;
  IF (v_r.b_size IS NULL OR v_ro.b_size IS NULL) AND
  NOT COALESCE(v_r.b_size,v_ro.b_size) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.b_chcnt <> v_ro.b_chcnt THEN RETURN FALSE; END IF;
  IF (v_r.b_chcnt IS NULL OR v_ro.b_chcnt IS NULL) AND
  NOT COALESCE(v_r.b_chcnt,v_ro.b_chcnt) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.b_algo <> v_ro.b_algo THEN RETURN FALSE; END IF;
  IF (v_r.b_algo IS NULL OR v_ro.b_algo IS NULL) AND
  NOT COALESCE(v_r.b_algo,v_ro.b_algo) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.b_digest <> v_ro.b_digest THEN RETURN FALSE; END IF;
  IF (v_r.b_digest IS NULL OR v_ro.b_digest IS NULL) AND
  NOT COALESCE(v_r.b_digest,v_ro.b_digest) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.t_size <> v_ro.t_size THEN RETURN FALSE; END IF;
  IF (v_r.t_size IS NULL OR v_ro.t_size IS NULL) AND
  NOT COALESCE(v_r.t_size,v_ro.t_size) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.t_chcnt <> v_ro.t_chcnt THEN RETURN FALSE; END IF;
  IF (v_r.t_chcnt IS NULL OR v_ro.t_chcnt IS NULL) AND
  NOT COALESCE(v_r.t_chcnt,v_ro.t_chcnt) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.t_algo <> v_ro.t_algo THEN RETURN FALSE; END IF;
  IF (v_r.t_algo IS NULL OR v_ro.t_algo IS NULL) AND
  NOT COALESCE(v_r.t_algo,v_ro.t_algo) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.t_digest <> v_ro.t_digest THEN RETURN FALSE; END IF;
  IF (v_r.t_digest IS NULL OR v_ro.t_digest IS NULL) AND
  NOT COALESCE(v_r.t_digest,v_ro.t_digest) IS NULL THEN RETURN FALSE; END IF;
 RETURN TRUE; -- both records equal
END $$;
COMMIT TRANSACTION;
