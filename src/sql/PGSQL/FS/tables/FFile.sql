-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: FS
-- OBJECT: File (F) RECORD: FFile (FF)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

CREATE TABLE FS.FFile (
	ZOID	bigint NOT NULL,
	ZRID	bigint NOT NULL,
	ZVER	bigint NOT NULL,
	ZTOV	bigint NOT NULL,
	ZSID	bigint NOT NULL,
	ZLVL	smallint NOT NULL,
	ZPID	bigint NOT NULL,
	name	varchar(255) NULL,
	type	char(1) NOT NULL,
	extstore	char(1) NOT NULL,
	mimetype	varchar(127) NULL,
	descr	varchar(1024) NULL,
	b_size	bigint NULL,
	b_chcnt	bigint NULL,
	b_algo	varchar(6) NULL,
	b_digest	varchar(127) NULL,
	t_size	bigint NULL,
	t_chcnt	bigint NULL,
	t_algo	varchar(6) NULL,
	t_digest	varchar(127) NULL,
	PRIMARY KEY (ZOID,ZRID,ZVER)
	);
CREATE INDEX FFile_idx1 on FS.FFile(mimetype);
