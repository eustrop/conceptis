-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: FS
-- OBJECT: File (F) RECORD: FBlob (FB)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

CREATE TABLE FS.FBlob (
	ZOID	bigint NOT NULL,
	ZRID	bigint NOT NULL,
	ZVER	bigint NOT NULL,
	ZTOV	bigint NOT NULL,
	ZSID	bigint NOT NULL,
	ZLVL	smallint NOT NULL,
	ZPID	bigint NOT NULL,
	chunk	bytea NULL,
	no	bigint NULL,
	size	bigint NULL,
	crc32	int NULL,
	PRIMARY KEY (ZOID,ZRID,ZVER)
	);
