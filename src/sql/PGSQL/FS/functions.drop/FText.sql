DROP FUNCTION FS.create_FText(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_titlekey	varchar(255),
	v_chunk	text
) CASCADE;
DROP FUNCTION FS.update_FText(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_titlekey	varchar(255),
	v_chunk	text
) CASCADE;
DROP FUNCTION FS.delete_FText(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) CASCADE;
DROP FUNCTION IF EXISTS FS.QC_FT(
	v_as	SAM.auditstate,
	v_r	FS.FText
	) CASCADE;
DROP FUNCTION IF EXISTS FS.commit_FT(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS FS.rollback_FT(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS FS.move_FT(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS FS.delete_object_FT(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS FS.cmp_FT(
	v_r	FS.FText,
	v_ro	FS.FText
	) CASCADE;
DROP FUNCTION IF EXISTS FS.rdelete_FT(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) CASCADE;
