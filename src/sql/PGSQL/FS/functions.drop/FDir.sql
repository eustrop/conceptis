DROP FUNCTION FS.create_FDir(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_f_id	bigint,
	v_fname	varchar(255),
	v_mimetype	varchar(127),
	v_descr	varchar(1024)
) CASCADE;
DROP FUNCTION FS.update_FDir(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_f_id	bigint,
	v_fname	varchar(255),
	v_mimetype	varchar(127),
	v_descr	varchar(1024)
) CASCADE;
DROP FUNCTION FS.delete_FDir(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) CASCADE;
DROP FUNCTION IF EXISTS FS.QC_FD(
	v_as	SAM.auditstate,
	v_r	FS.FDir
	) CASCADE;
DROP FUNCTION IF EXISTS FS.commit_FD(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS FS.rollback_FD(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS FS.move_FD(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS FS.delete_object_FD(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS FS.cmp_FD(
	v_r	FS.FDir,
	v_ro	FS.FDir
	) CASCADE;
DROP FUNCTION IF EXISTS FS.rdelete_FD(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) CASCADE;
