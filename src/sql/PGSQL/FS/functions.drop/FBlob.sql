DROP FUNCTION FS.create_FBlob(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_chunk	bytea,
	v_no	bigint,
	v_size	bigint,
	v_crc32	int
) CASCADE;
DROP FUNCTION FS.update_FBlob(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_chunk	bytea,
	v_no	bigint,
	v_size	bigint,
	v_crc32	int
) CASCADE;
DROP FUNCTION FS.delete_FBlob(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) CASCADE;
DROP FUNCTION IF EXISTS FS.QC_FB(
	v_as	SAM.auditstate,
	v_r	FS.FBlob
	) CASCADE;
DROP FUNCTION IF EXISTS FS.commit_FB(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS FS.rollback_FB(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS FS.move_FB(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS FS.delete_object_FB(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS FS.cmp_FB(
	v_r	FS.FBlob,
	v_ro	FS.FBlob
	) CASCADE;
DROP FUNCTION IF EXISTS FS.rdelete_FB(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) CASCADE;
