DROP FUNCTION FS.create_FFile(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_name	varchar(255),
	v_type	char(1),
	v_extstore	char(1),
	v_mimetype	varchar(127),
	v_descr	varchar(1024),
	v_b_size	bigint,
	v_b_chcnt	bigint,
	v_b_algo	varchar(6),
	v_b_digest	varchar(127),
	v_t_size	bigint,
	v_t_chcnt	bigint,
	v_t_algo	varchar(6),
	v_t_digest	varchar(127)
) CASCADE;
DROP FUNCTION FS.update_FFile(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_name	varchar(255),
	v_type	char(1),
	v_extstore	char(1),
	v_mimetype	varchar(127),
	v_descr	varchar(1024),
	v_b_size	bigint,
	v_b_chcnt	bigint,
	v_b_algo	varchar(6),
	v_b_digest	varchar(127),
	v_t_size	bigint,
	v_t_chcnt	bigint,
	v_t_algo	varchar(6),
	v_t_digest	varchar(127)
) CASCADE;
DROP FUNCTION FS.delete_FFile(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) CASCADE;
DROP FUNCTION IF EXISTS FS.QC_FF(
	v_as	SAM.auditstate,
	v_r	FS.FFile
	) CASCADE;
DROP FUNCTION IF EXISTS FS.commit_FF(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS FS.rollback_FF(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS FS.move_FF(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS FS.delete_object_FF(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS FS.cmp_FF(
	v_r	FS.FFile,
	v_ro	FS.FFile
	) CASCADE;
DROP FUNCTION IF EXISTS FS.rdelete_FF(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) CASCADE;
