REVOKE ALL ON FUNCTION TISC.create_AArea(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_name	varchar(32),
	v_scope_id	bigint,
	v_descr	varchar(99)
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION TISC.create_AArea(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_name	varchar(32),
	v_scope_id	bigint,
	v_descr	varchar(99)
) TO tis_users;
GRANT EXECUTE ON FUNCTION TISC.update_AArea(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_name	varchar(32),
	v_scope_id	bigint,
	v_descr	varchar(99)
) TO tis_users;
REVOKE EXECUTE ON FUNCTION TISC.delete_AArea(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION TISC.delete_AArea(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) TO tis_users;
REVOKE ALL ON FUNCTION TISC.QC_AA(
	v_as	SAM.auditstate,
	v_r	TISC.AArea
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.commit_AA(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.rollback_AA(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.move_AA(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.delete_object_AA(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.cmp_AA(
	v_r	TISC.AArea,
	v_ro	TISC.AArea
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.rdelete_AA(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) FROM PUBLIC;
