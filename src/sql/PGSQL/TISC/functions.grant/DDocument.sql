REVOKE ALL ON FUNCTION TISC.create_DDocument(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_symbol	varchar(32),
	v_type	char(4),
	v_title	varchar(99),
	v_auth	varchar(255),
	v_abstr	varchar(255)
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION TISC.create_DDocument(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_symbol	varchar(32),
	v_type	char(4),
	v_title	varchar(99),
	v_auth	varchar(255),
	v_abstr	varchar(255)
) TO tis_users;
GRANT EXECUTE ON FUNCTION TISC.update_DDocument(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_symbol	varchar(32),
	v_type	char(4),
	v_title	varchar(99),
	v_auth	varchar(255),
	v_abstr	varchar(255)
) TO tis_users;
REVOKE EXECUTE ON FUNCTION TISC.delete_DDocument(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION TISC.delete_DDocument(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) TO tis_users;
REVOKE ALL ON FUNCTION TISC.QC_DD(
	v_as	SAM.auditstate,
	v_r	TISC.DDocument
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.commit_DD(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.rollback_DD(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.move_DD(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.delete_object_DD(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.cmp_DD(
	v_r	TISC.DDocument,
	v_ro	TISC.DDocument
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.rdelete_DD(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) FROM PUBLIC;
