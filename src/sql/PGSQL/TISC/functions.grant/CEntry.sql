REVOKE ALL ON FUNCTION TISC.create_CEntry(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_obj_id	bigint
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION TISC.create_CEntry(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_obj_id	bigint
) TO tis_users;
GRANT EXECUTE ON FUNCTION TISC.update_CEntry(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_obj_id	bigint
) TO tis_users;
REVOKE EXECUTE ON FUNCTION TISC.delete_CEntry(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION TISC.delete_CEntry(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) TO tis_users;
REVOKE ALL ON FUNCTION TISC.QC_CE(
	v_as	SAM.auditstate,
	v_r	TISC.CEntry
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.commit_CE(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.rollback_CE(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.move_CE(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.delete_object_CE(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.cmp_CE(
	v_r	TISC.CEntry,
	v_ro	TISC.CEntry
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.rdelete_CE(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) FROM PUBLIC;
