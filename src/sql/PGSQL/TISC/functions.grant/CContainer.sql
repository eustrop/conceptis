REVOKE ALL ON FUNCTION TISC.create_CContainer(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_num	varchar(32),
	v_type	char(4),
	v_descr	varchar(99)
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION TISC.create_CContainer(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_num	varchar(32),
	v_type	char(4),
	v_descr	varchar(99)
) TO tis_users;
GRANT EXECUTE ON FUNCTION TISC.update_CContainer(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_num	varchar(32),
	v_type	char(4),
	v_descr	varchar(99)
) TO tis_users;
REVOKE EXECUTE ON FUNCTION TISC.delete_CContainer(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION TISC.delete_CContainer(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) TO tis_users;
REVOKE ALL ON FUNCTION TISC.QC_CC(
	v_as	SAM.auditstate,
	v_r	TISC.CContainer
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.commit_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.rollback_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.move_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.delete_object_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.cmp_CC(
	v_r	TISC.CContainer,
	v_ro	TISC.CContainer
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.rdelete_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) FROM PUBLIC;
