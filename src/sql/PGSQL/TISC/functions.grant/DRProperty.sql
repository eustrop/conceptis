REVOKE ALL ON FUNCTION TISC.create_DRProperty(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_type	char(4),
	v_nvalue	numeric,
	v_tvalue	varchar(99)
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION TISC.create_DRProperty(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_type	char(4),
	v_nvalue	numeric,
	v_tvalue	varchar(99)
) TO tis_users;
GRANT EXECUTE ON FUNCTION TISC.update_DRProperty(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_type	char(4),
	v_nvalue	numeric,
	v_tvalue	varchar(99)
) TO tis_users;
REVOKE EXECUTE ON FUNCTION TISC.delete_DRProperty(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION TISC.delete_DRProperty(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) TO tis_users;
REVOKE ALL ON FUNCTION TISC.QC_DP(
	v_as	SAM.auditstate,
	v_r	TISC.DRProperty
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.commit_DP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.rollback_DP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.move_DP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.delete_object_DP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.cmp_DP(
	v_r	TISC.DRProperty,
	v_ro	TISC.DRProperty
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.rdelete_DP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) FROM PUBLIC;
