REVOKE ALL ON FUNCTION TISC.create_DRef(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_doc_id	bigint,
	v_reltype	char(2)
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION TISC.create_DRef(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_doc_id	bigint,
	v_reltype	char(2)
) TO tis_users;
GRANT EXECUTE ON FUNCTION TISC.update_DRef(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_doc_id	bigint,
	v_reltype	char(2)
) TO tis_users;
REVOKE EXECUTE ON FUNCTION TISC.delete_DRef(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION TISC.delete_DRef(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) TO tis_users;
REVOKE ALL ON FUNCTION TISC.QC_DR(
	v_as	SAM.auditstate,
	v_r	TISC.DRef
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.commit_DR(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.rollback_DR(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.move_DR(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.delete_object_DR(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.cmp_DR(
	v_r	TISC.DRef,
	v_ro	TISC.DRef
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION TISC.rdelete_DR(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) FROM PUBLIC;
