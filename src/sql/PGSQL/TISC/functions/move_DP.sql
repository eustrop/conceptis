-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: TISC
-- OBJECT: Document (D) RECORD: DRProperty (DP)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION TISC.move_DP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
) RETURNS SAM.procstate VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
BEGIN
  -- copy existing records with new ZSID & ZLVL
  INSERT INTO TISC.DRProperty SELECT
      ZOID,ZRID,v_ZO.ZVER,0,v_ZO.ZSID,v_ZO.ZLVL,ZPID,
      type,nvalue,tvalue
    FROM TISC.DRProperty ZR WHERE ZR.ZTOV = 0 AND ZR.ZOID = v_ZO.ZOID;
  -- obsolete existing records
  UPDATE TISC.DRProperty ZR SET ZTOV = v_ZO.ZVER WHERE ZR.ZTOV = 0 AND
   ZR.ZOID = v_ZO.ZOID AND ZR.ZVER <> v_ZO.ZVER;
-- FINALLY:
 v_ps.success := TRUE;
 v_ps.a := v_as;
 RETURN v_ps;
END $$;
COMMIT TRANSACTION;
