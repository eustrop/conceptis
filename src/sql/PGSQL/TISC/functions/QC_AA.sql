-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: TISC
-- OBJECT: Area (A) RECORD: AArea (AA)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION TISC.QC_AA(
	v_as	SAM.auditstate,
	v_r	TISC.AArea
) RETURNS SAM.procstate VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 i	integer;
 ZC_ZTOV_UPDATED CONSTANT bigint := -1;
 ZC_ZTOV_DELETED CONSTANT bigint := -2;
 ZC_ZTOV_NOCHANGES CONSTANT bigint := -3;
 ZC_ZTOV_RDELETED CONSTANT bigint := -4;
BEGIN
 v_ps.success := FALSE;
<<try>>
BEGIN
 -- 1) check NOT NULLs
 IF v_r.name IS NULL THEN v_ps.e := SAM.make_es_notnull(v_r.ZRID,v_r.ZVER,
	'TISC.AArea','name'); EXIT try; END IF;
 -- 2) check dictionary fields
 -- 3) check uniqueness
  IF TIS.check_ZNAME('A',v_r.ZOID,v_r.name) THEN
  v_ps.e := SAM.make_execstatus(null,null,'E_DUPRECORD',
  'TISC.AArea','ZNAME',v_r.name); EXIT try;
  END IF;
 SELECT COUNT(*) INTO i FROM TISC.AArea ZR WHERE scope_id = v_r.scope_id AND
	 ZTOV = 0 AND (ZRID <> v_r.ZRID OR v_r.ZRID IS NULL)
	  AND (ZOID <> v_r.ZOID OR v_r.ZOID IS NULL);
 IF i <> 0 THEN
  v_ps.e :=SAM.make_execstatus(v_r.ZRID,v_r.ZVER,'E_DUPRECORD','TISC.AArea',
  'scope_id',''||v_r.scope_id);
  EXIT try; END IF;
 -- 4) check references
 -- 4.1) check std parent/child references
   -- not required
 -- 4.2) check scope references from data fields
  IF v_r.scope_id <> 0 THEN
   SELECT COUNT(*) INTO i FROM SAM.Scope WHERE id = v_r.scope_id;
   IF i = 0 THEN v_ps.e := SAM.make_execstatus(v_r.ZRID,v_r.ZVER,'E_NOSCOPE',
	CAST(v_r.scope_id AS text)); EXIT try; END IF;
  END IF;
 -- 99) all tests passed
 v_ps.success := TRUE;
END;
-- FINALLY:
 v_ps.a := v_as;
 RETURN v_ps;
END $$;
COMMIT TRANSACTION;
