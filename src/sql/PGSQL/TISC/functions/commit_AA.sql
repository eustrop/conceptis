-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: TISC
-- OBJECT: Area (A) RECORD: AArea (AA)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION TISC.commit_AA(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
) RETURNS SAM.procstate VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 v_r  TISC.AArea%ROWTYPE; -- for FQC
 v_recount bigint;
 v_ZNAME varchar(32);
 v_is_ZNAME boolean := false;
 ZC_ZTOV_UPDATED CONSTANT bigint := -1;
 ZC_ZTOV_DELETED CONSTANT bigint := -2;
 ZC_ZTOV_NOCHANGES CONSTANT bigint := -3;
 ZC_ZTOV_RDELETED CONSTANT bigint := -4;
BEGIN
 v_ps.success := FALSE;
<<try>>
BEGIN
 -- 1) make FQC (final QC)
 -- 1.1) check NOT NULLs
  -- not necessary
 -- 1.2) check dictionary fields
  -- may be in the future...
 -- 1.3) check uniqueness
 LOCK TABLE TISC.AArea IN EXCLUSIVE MODE;
 SELECT * INTO v_r FROM TISC.AArea ZR, TISC.AArea ZR2 WHERE ZR.ZOID = v_ZO.ZOID AND
   ZR.scope_id = ZR2.scope_id AND ZR2.ZTOV = 0 AND
   ( ZR.ZTOV IN (ZC_ZTOV_UPDATED,ZC_ZTOV_NOCHANGES)
     OR ( ZR.ZTOV = 0 AND NOT EXISTS
        (SELECT ZRID FROM TISC.AArea ZR3 WHERE
         ZR3.ZOID = ZR.ZOID AND ZR3.ZRID = ZR.ZRID AND ZR3.ZVER = v_ZO.ZVER)
   ) ) AND
   (ZR.ZRID <> ZR2.ZRID OR ZR.ZOID <> ZR2.ZOID);
 IF FOUND THEN
  v_ps.e :=SAM.make_execstatus(v_r.ZRID,v_r.ZVER,'E_DUPRECORD','TISC.AArea',
  'scope_id',''||v_r.scope_id);
  EXIT try; END IF;
 -- 1.4) check references
  -- may be in the future...
 -- 1.5) check MAXREC and MINREC
 -- 1.5.1) get count(*) for active records in the object
 SELECT count(*) INTO v_recount FROM TISC.AArea ZR WHERE ZR.ZOID = v_ZO.ZOID
   AND (ZR.ZTOV IN (ZC_ZTOV_UPDATED, ZC_ZTOV_NOCHANGES) OR ( ZR.ZTOV = 0 AND
     NOT EXISTS (SELECT ZRID FROM TISC.AArea ZR2 WHERE
       ZR2.ZOID = ZR.ZOID AND ZR2.ZRID = ZR.ZRID AND ZR2.ZVER = v_ZO.ZVER) ) );
 IF v_recount > 1 THEN
  v_ps.e := SAM.make_execstatus(null,null,'E_2MANYRECORDS',
  'TISC.AArea','Area','1'); EXIT try;
 END IF;
 IF v_recount < 1 THEN
  v_ps.e := SAM.make_execstatus(null,null,'E_NOTENOUGHRECS',
  'TISC.AArea','Area','1'); EXIT try;
 END IF;
 -- 1.6) check ZNAME
 SELECT name INTO v_ZNAME FROM TISC.AArea WHERE ZOID = v_ZO.ZOID AND
   ZVER = v_ZO.ZVER AND ZTOV = ZC_ZTOV_UPDATED;
 IF FOUND THEN
  v_is_ZNAME := TRUE;
  IF TIS.check_ZNAME('TISC.A',v_ZO.ZOID,v_ZNAME) THEN
  v_ps.e := SAM.make_execstatus(null,null,'E_DUPRECORD',
  'TISC.AArea','ZNAME',v_ZNAME); EXIT try;
  END IF;
 ELSE
   PERFORM name FROM TISC.AArea WHERE ZOID = v_ZO.ZOID AND
   ZVER = v_ZO.ZVER AND ZTOV IN (ZC_ZTOV_DELETED,ZC_ZTOV_RDELETED);
   v_is_ZNAME := FOUND; v_ZNAME := NULL; -- delete ZNAME if so
 END IF;
 -- 1.7) doing final quality control manually (FQCM)
  -- may be in the future...
 -- 2) update table
  UPDATE TISC.AArea ZR SET ZTOV = v_ZO.ZVER WHERE ZR.ZTOV = 0 AND ZR.ZOID = v_ZO.ZOID AND EXISTS
    (SELECT ZRID FROM TISC.AArea ZR2 WHERE 
      ZR2.ZTOV IN (ZC_ZTOV_UPDATED, ZC_ZTOV_DELETED, ZC_ZTOV_RDELETED) AND
      ZR2.ZOID = ZR.ZOID AND ZR2.ZRID = ZR.ZRID AND ZR2.ZVER = v_ZO.ZVER);
  UPDATE TISC.AArea ZR SET ZTOV = 0 WHERE ZTOV = ZC_ZTOV_UPDATED AND ZR.ZOID = v_ZO.ZOID
                                  AND ZR.ZVER = v_ZO.ZVER;
  DELETE FROM TISC.AArea ZR WHERE ZR.ZTOV < 0 AND ZR.ZOID = v_ZO.ZOID AND ZR.ZVER = v_ZO.ZVER;
  -- set ZNAME
  IF v_is_ZNAME THEN
   v_ps := TIS.set_ZNAME(v_as,v_ZO,v_ZNAME); v_as := v_ps.a;
   IF NOT v_ps.success THEN EXIT try; END IF;
  END IF;
 -- 3) all tests passed
 v_ps.success := TRUE;
END;
-- FINALLY:
 v_ps.a := v_as;
 RETURN v_ps;
END $$;
COMMIT TRANSACTION;
