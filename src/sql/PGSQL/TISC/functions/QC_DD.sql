-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: TISC
-- OBJECT: Document (D) RECORD: DDocument (DD)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION TISC.QC_DD(
	v_as	SAM.auditstate,
	v_r	TISC.DDocument
) RETURNS SAM.procstate VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 i	integer;
 ZC_ZTOV_UPDATED CONSTANT bigint := -1;
 ZC_ZTOV_DELETED CONSTANT bigint := -2;
 ZC_ZTOV_NOCHANGES CONSTANT bigint := -3;
 ZC_ZTOV_RDELETED CONSTANT bigint := -4;
BEGIN
 v_ps.success := FALSE;
<<try>>
BEGIN
 -- 1) check NOT NULLs
 IF v_r.type IS NULL THEN v_ps.e := SAM.make_es_notnull(v_r.ZRID,v_r.ZVER,
	'TISC.DDocument','type'); EXIT try; END IF;
 -- 2) check dictionary fields
 IF NOT dic.check_code('DD02',v_r.type) THEN
   v_ps.e := SAM.make_es_invalidcode(v_r.ZRID,v_r.ZVER,v_r.type,
   'TISC.DDocument.type','DD02'); EXIT try; END IF;
 -- 3) check uniqueness
  IF TIS.check_ZNAME('D',v_r.ZOID,v_r.symbol) THEN
  v_ps.e := SAM.make_execstatus(null,null,'E_DUPRECORD',
  'TISC.DDocument','ZNAME',v_r.symbol); EXIT try;
  END IF;
 -- 4) check references
 -- 4.1) check std parent/child references
   -- not required
 -- 4.2) check scope references from data fields
 -- 99) all tests passed
 v_ps.success := TRUE;
END;
-- FINALLY:
 v_ps.a := v_as;
 RETURN v_ps;
END $$;
COMMIT TRANSACTION;
