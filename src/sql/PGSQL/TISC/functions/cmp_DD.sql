-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: TISC
-- OBJECT: Document (D) RECORD: DDocument (DD)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION TISC.cmp_DD(
	v_r	TISC.DDocument,
	v_ro	TISC.DDocument
) RETURNS boolean STABLE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
BEGIN
 IF v_r.symbol <> v_ro.symbol THEN RETURN FALSE; END IF;
  IF (v_r.symbol IS NULL OR v_ro.symbol IS NULL) AND
  NOT COALESCE(v_r.symbol,v_ro.symbol) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.type <> v_ro.type THEN RETURN FALSE; END IF;
  IF (v_r.type IS NULL OR v_ro.type IS NULL) AND
  NOT COALESCE(v_r.type,v_ro.type) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.title <> v_ro.title THEN RETURN FALSE; END IF;
  IF (v_r.title IS NULL OR v_ro.title IS NULL) AND
  NOT COALESCE(v_r.title,v_ro.title) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.auth <> v_ro.auth THEN RETURN FALSE; END IF;
  IF (v_r.auth IS NULL OR v_ro.auth IS NULL) AND
  NOT COALESCE(v_r.auth,v_ro.auth) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.abstr <> v_ro.abstr THEN RETURN FALSE; END IF;
  IF (v_r.abstr IS NULL OR v_ro.abstr IS NULL) AND
  NOT COALESCE(v_r.abstr,v_ro.abstr) IS NULL THEN RETURN FALSE; END IF;
 RETURN TRUE; -- both records equal
END $$;
COMMIT TRANSACTION;
