-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: TISC
-- OBJECT: Area (A) RECORD: AArea (AA)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION TISC.cmp_AA(
	v_r	TISC.AArea,
	v_ro	TISC.AArea
) RETURNS boolean STABLE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
BEGIN
 IF v_r.name <> v_ro.name THEN RETURN FALSE; END IF;
  IF (v_r.name IS NULL OR v_ro.name IS NULL) AND
  NOT COALESCE(v_r.name,v_ro.name) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.scope_id <> v_ro.scope_id THEN RETURN FALSE; END IF;
  IF (v_r.scope_id IS NULL OR v_ro.scope_id IS NULL) AND
  NOT COALESCE(v_r.scope_id,v_ro.scope_id) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.descr <> v_ro.descr THEN RETURN FALSE; END IF;
  IF (v_r.descr IS NULL OR v_ro.descr IS NULL) AND
  NOT COALESCE(v_r.descr,v_ro.descr) IS NULL THEN RETURN FALSE; END IF;
 RETURN TRUE; -- both records equal
END $$;
COMMIT TRANSACTION;
