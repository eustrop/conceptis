-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: TISC
-- OBJECT: Container (C) RECORD: CContainer (CC)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION TISC.cmp_CC(
	v_r	TISC.CContainer,
	v_ro	TISC.CContainer
) RETURNS boolean STABLE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
BEGIN
 IF v_r.num <> v_ro.num THEN RETURN FALSE; END IF;
  IF (v_r.num IS NULL OR v_ro.num IS NULL) AND
  NOT COALESCE(v_r.num,v_ro.num) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.type <> v_ro.type THEN RETURN FALSE; END IF;
  IF (v_r.type IS NULL OR v_ro.type IS NULL) AND
  NOT COALESCE(v_r.type,v_ro.type) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.descr <> v_ro.descr THEN RETURN FALSE; END IF;
  IF (v_r.descr IS NULL OR v_ro.descr IS NULL) AND
  NOT COALESCE(v_r.descr,v_ro.descr) IS NULL THEN RETURN FALSE; END IF;
 RETURN TRUE; -- both records equal
END $$;
COMMIT TRANSACTION;
