-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: TISC
-- OBJECT: Container (C) RECORD: CEntry (CE)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION TISC.cmp_CE(
	v_r	TISC.CEntry,
	v_ro	TISC.CEntry
) RETURNS boolean STABLE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
BEGIN
 IF v_r.obj_id <> v_ro.obj_id THEN RETURN FALSE; END IF;
  IF (v_r.obj_id IS NULL OR v_ro.obj_id IS NULL) AND
  NOT COALESCE(v_r.obj_id,v_ro.obj_id) IS NULL THEN RETURN FALSE; END IF;
 RETURN TRUE; -- both records equal
END $$;
COMMIT TRANSACTION;
