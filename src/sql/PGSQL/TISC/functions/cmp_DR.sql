-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: TISC
-- OBJECT: Document (D) RECORD: DRef (DR)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION TISC.cmp_DR(
	v_r	TISC.DRef,
	v_ro	TISC.DRef
) RETURNS boolean STABLE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
BEGIN
 IF v_r.doc_id <> v_ro.doc_id THEN RETURN FALSE; END IF;
  IF (v_r.doc_id IS NULL OR v_ro.doc_id IS NULL) AND
  NOT COALESCE(v_r.doc_id,v_ro.doc_id) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.reltype <> v_ro.reltype THEN RETURN FALSE; END IF;
  IF (v_r.reltype IS NULL OR v_ro.reltype IS NULL) AND
  NOT COALESCE(v_r.reltype,v_ro.reltype) IS NULL THEN RETURN FALSE; END IF;
 RETURN TRUE; -- both records equal
END $$;
COMMIT TRANSACTION;
