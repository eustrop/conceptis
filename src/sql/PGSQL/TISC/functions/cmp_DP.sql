-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: TISC
-- OBJECT: Document (D) RECORD: DRProperty (DP)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION TISC.cmp_DP(
	v_r	TISC.DRProperty,
	v_ro	TISC.DRProperty
) RETURNS boolean STABLE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
BEGIN
 IF v_r.type <> v_ro.type THEN RETURN FALSE; END IF;
  IF (v_r.type IS NULL OR v_ro.type IS NULL) AND
  NOT COALESCE(v_r.type,v_ro.type) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.nvalue <> v_ro.nvalue THEN RETURN FALSE; END IF;
  IF (v_r.nvalue IS NULL OR v_ro.nvalue IS NULL) AND
  NOT COALESCE(v_r.nvalue,v_ro.nvalue) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.tvalue <> v_ro.tvalue THEN RETURN FALSE; END IF;
  IF (v_r.tvalue IS NULL OR v_ro.tvalue IS NULL) AND
  NOT COALESCE(v_r.tvalue,v_ro.tvalue) IS NULL THEN RETURN FALSE; END IF;
 RETURN TRUE; -- both records equal
END $$;
COMMIT TRANSACTION;
