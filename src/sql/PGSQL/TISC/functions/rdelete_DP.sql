-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: TISC
-- OBJECT: Document (D) RECORD: DRProperty (DP)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION TISC.rdelete_DP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
) RETURNS SAM.procstate
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 v_r TISC.DRProperty;
 ZC_ZTOV_UPDATED CONSTANT bigint := -1;
 ZC_ZTOV_DELETED CONSTANT bigint := -2;
 ZC_ZTOV_NOCHANGES CONSTANT bigint := -3;
 ZC_ZTOV_RDELETED CONSTANT bigint := -4;
BEGIN
 v_ps.a := v_as;
 v_ps.success := TRUE;
 FOR v_r IN SELECT * FROM TISC.DRProperty ZR WHERE ZR.ZOID = v_ZO.ZOID AND
             ZR.ZPID = v_ZPID AND ZR.ZTOV IN (0,ZC_ZTOV_UPDATED)
  LOOP
   -- delete found row 
   UPDATE TISC.DRProperty SET ZTOV = ZC_ZTOV_RDELETED WHERE
    ZOID = v_r.ZOID AND ZRID = v_r.ZRID AND ZVER = v_ZO.ZVER;
   IF NOT FOUND THEN
    v_r.ZVER := v_ZO.ZVER;
    v_r.ZTOV := ZC_ZTOV_RDELETED;
    INSERT INTO TISC.DRProperty VALUES(v_r.*);
   END IF;
 END LOOP;
 RETURN v_ps;
END $$;
COMMIT TRANSACTION;
