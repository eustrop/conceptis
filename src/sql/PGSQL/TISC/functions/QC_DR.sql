-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: TISC
-- OBJECT: Document (D) RECORD: DRef (DR)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION TISC.QC_DR(
	v_as	SAM.auditstate,
	v_r	TISC.DRef
) RETURNS SAM.procstate VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 i	integer;
 ZC_ZTOV_UPDATED CONSTANT bigint := -1;
 ZC_ZTOV_DELETED CONSTANT bigint := -2;
 ZC_ZTOV_NOCHANGES CONSTANT bigint := -3;
 ZC_ZTOV_RDELETED CONSTANT bigint := -4;
BEGIN
 v_ps.success := FALSE;
<<try>>
BEGIN
 -- 1) check NOT NULLs
 IF v_r.doc_id IS NULL THEN v_ps.e := SAM.make_es_notnull(v_r.ZRID,v_r.ZVER,
	'TISC.DRef','doc_id'); EXIT try; END IF;
 IF v_r.reltype IS NULL THEN v_ps.e := SAM.make_es_notnull(v_r.ZRID,v_r.ZVER,
	'TISC.DRef','reltype'); EXIT try; END IF;
 -- 2) check dictionary fields
 IF NOT dic.check_code('DR02',v_r.reltype) THEN
   v_ps.e := SAM.make_es_invalidcode(v_r.ZRID,v_r.ZVER,v_r.reltype,
   'TISC.DRef.reltype','DR02'); EXIT try; END IF;
 -- 3) check uniqueness
 -- 4) check references
 -- 4.1) check std parent/child references
  SELECT COUNT(*) INTO i FROM TISC.DDocument ZR WHERE ZR.ZOID = v_r.ZOID AND
   ZR.ZRID = v_r.ZPID AND ZR.ZTOV <= 0 AND NOT EXISTS (
    SELECT * FROM TISC.DDocument ZR2 WHERE ZR2.ZOID = v_r.ZOID AND ZR2.ZRID = v_r.ZPID
    AND ZR2.ZTOV IN (ZC_ZTOV_DELETED,ZC_ZTOV_RDELETED) );
  IF i = 0 THEN v_ps.e := SAM.make_execstatus(v_r.ZRID,v_r.ZVER,'E_NORECORD',
    'TISC.DDocument','ZRID',CAST(v_r.ZPID AS text)); EXIT try; END IF;
 -- 4.2) check scope references from data fields
 -- 99) all tests passed
 v_ps.success := TRUE;
END;
-- FINALLY:
 v_ps.a := v_as;
 RETURN v_ps;
END $$;
COMMIT TRANSACTION;
