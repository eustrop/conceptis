-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: TISC

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen_subsys.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION TISC.rollback_object(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
) RETURNS SAM.procstate VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
BEGIN
  v_ps.success := TRUE;
  v_ps.a := v_as;
  IF v_ZO.ZTYPE = 'TISC.A' THEN
   v_ps := TISC.rollback_A(v_as,v_ZO); v_as := v_ps.a;
  ELSIF v_ZO.ZTYPE = 'TISC.C' THEN
   v_ps := TISC.rollback_C(v_as,v_ZO); v_as := v_ps.a;
  ELSIF v_ZO.ZTYPE = 'TISC.D' THEN
   v_ps := TISC.rollback_D(v_as,v_ZO); v_as := v_ps.a;
  ELSE
   v_ps.e := SAM.make_execstatus(v_ZO.ZOID,v_ZO.ZVER,'E_INVALIDZTYPE',v_ZO.ZTYPE);
   v_ps.success := FALSE;
  END IF;
 IF NOT v_ps.success THEN RAISE EXCEPTION 'TISC.rollback rolled back'; END IF;
 RETURN v_ps;
EXCEPTION
  WHEN RAISE_EXCEPTION THEN
   RETURN v_ps;
END $$;
REVOKE ALL ON FUNCTION TISC.rollback_object(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
) FROM PUBLIC;
COMMIT TRANSACTION;
