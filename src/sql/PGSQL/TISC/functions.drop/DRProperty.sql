DROP FUNCTION TISC.create_DRProperty(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_type	char(4),
	v_nvalue	numeric,
	v_tvalue	varchar(99)
) CASCADE;
DROP FUNCTION TISC.update_DRProperty(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_type	char(4),
	v_nvalue	numeric,
	v_tvalue	varchar(99)
) CASCADE;
DROP FUNCTION TISC.delete_DRProperty(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) CASCADE;
DROP FUNCTION IF EXISTS TISC.QC_DP(
	v_as	SAM.auditstate,
	v_r	TISC.DRProperty
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.commit_DP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.rollback_DP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.move_DP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.delete_object_DP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.cmp_DP(
	v_r	TISC.DRProperty,
	v_ro	TISC.DRProperty
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.rdelete_DP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) CASCADE;
