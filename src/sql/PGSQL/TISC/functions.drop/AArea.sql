DROP FUNCTION TISC.create_AArea(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_name	varchar(32),
	v_scope_id	bigint,
	v_descr	varchar(99)
) CASCADE;
DROP FUNCTION TISC.update_AArea(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_name	varchar(32),
	v_scope_id	bigint,
	v_descr	varchar(99)
) CASCADE;
DROP FUNCTION TISC.delete_AArea(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) CASCADE;
DROP FUNCTION IF EXISTS TISC.QC_AA(
	v_as	SAM.auditstate,
	v_r	TISC.AArea
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.commit_AA(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.rollback_AA(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.move_AA(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.delete_object_AA(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.cmp_AA(
	v_r	TISC.AArea,
	v_ro	TISC.AArea
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.rdelete_AA(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) CASCADE;
