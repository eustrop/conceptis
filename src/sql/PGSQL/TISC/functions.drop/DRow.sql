DROP FUNCTION TISC.create_DRow(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_num	int,
	v_item	varchar(64)
) CASCADE;
DROP FUNCTION TISC.update_DRow(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_num	int,
	v_item	varchar(64)
) CASCADE;
DROP FUNCTION TISC.delete_DRow(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) CASCADE;
DROP FUNCTION IF EXISTS TISC.QC_DW(
	v_as	SAM.auditstate,
	v_r	TISC.DRow
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.commit_DW(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.rollback_DW(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.move_DW(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.delete_object_DW(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.cmp_DW(
	v_r	TISC.DRow,
	v_ro	TISC.DRow
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.rdelete_DW(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) CASCADE;
