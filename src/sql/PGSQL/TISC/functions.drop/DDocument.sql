DROP FUNCTION TISC.create_DDocument(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_symbol	varchar(32),
	v_type	char(4),
	v_title	varchar(99),
	v_auth	varchar(255),
	v_abstr	varchar(255)
) CASCADE;
DROP FUNCTION TISC.update_DDocument(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_symbol	varchar(32),
	v_type	char(4),
	v_title	varchar(99),
	v_auth	varchar(255),
	v_abstr	varchar(255)
) CASCADE;
DROP FUNCTION TISC.delete_DDocument(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) CASCADE;
DROP FUNCTION IF EXISTS TISC.QC_DD(
	v_as	SAM.auditstate,
	v_r	TISC.DDocument
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.commit_DD(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.rollback_DD(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.move_DD(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.delete_object_DD(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.cmp_DD(
	v_r	TISC.DDocument,
	v_ro	TISC.DDocument
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.rdelete_DD(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) CASCADE;
