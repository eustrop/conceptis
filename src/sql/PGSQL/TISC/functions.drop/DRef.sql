DROP FUNCTION TISC.create_DRef(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_doc_id	bigint,
	v_reltype	char(2)
) CASCADE;
DROP FUNCTION TISC.update_DRef(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_doc_id	bigint,
	v_reltype	char(2)
) CASCADE;
DROP FUNCTION TISC.delete_DRef(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) CASCADE;
DROP FUNCTION IF EXISTS TISC.QC_DR(
	v_as	SAM.auditstate,
	v_r	TISC.DRef
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.commit_DR(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.rollback_DR(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.move_DR(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.delete_object_DR(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.cmp_DR(
	v_r	TISC.DRef,
	v_ro	TISC.DRef
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.rdelete_DR(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) CASCADE;
