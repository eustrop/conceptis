DROP FUNCTION TISC.create_CEntry(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_obj_id	bigint
) CASCADE;
DROP FUNCTION TISC.update_CEntry(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_obj_id	bigint
) CASCADE;
DROP FUNCTION TISC.delete_CEntry(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) CASCADE;
DROP FUNCTION IF EXISTS TISC.QC_CE(
	v_as	SAM.auditstate,
	v_r	TISC.CEntry
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.commit_CE(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.rollback_CE(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.move_CE(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.delete_object_CE(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.cmp_CE(
	v_r	TISC.CEntry,
	v_ro	TISC.CEntry
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.rdelete_CE(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) CASCADE;
