DROP FUNCTION TISC.create_CContainer(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_num	varchar(32),
	v_type	char(4),
	v_descr	varchar(99)
) CASCADE;
DROP FUNCTION TISC.update_CContainer(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_num	varchar(32),
	v_type	char(4),
	v_descr	varchar(99)
) CASCADE;
DROP FUNCTION TISC.delete_CContainer(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) CASCADE;
DROP FUNCTION IF EXISTS TISC.QC_CC(
	v_as	SAM.auditstate,
	v_r	TISC.CContainer
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.commit_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.rollback_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.move_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.delete_object_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.cmp_CC(
	v_r	TISC.CContainer,
	v_ro	TISC.CContainer
	) CASCADE;
DROP FUNCTION IF EXISTS TISC.rdelete_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) CASCADE;
