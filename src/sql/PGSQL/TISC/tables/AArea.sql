-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: TISC
-- OBJECT: Area (A) RECORD: AArea (AA)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

CREATE TABLE TISC.AArea (
	ZOID	bigint NOT NULL,
	ZRID	bigint NOT NULL,
	ZVER	bigint NOT NULL,
	ZTOV	bigint NOT NULL,
	ZSID	bigint NOT NULL,
	ZLVL	smallint NOT NULL,
	ZPID	bigint NOT NULL,
	name	varchar(32) NOT NULL,
	scope_id	bigint NULL,
	descr	varchar(99) NULL,
	PRIMARY KEY (ZOID,ZRID,ZVER)
	);
CREATE INDEX AArea_idx1 on TISC.AArea(name);
CREATE INDEX AArea_idx2 on TISC.AArea(scope_id);
