-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: TISC
-- OBJECT: Document (D) RECORD: DDocument (DD)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

CREATE TABLE TISC.DDocument (
	ZOID	bigint NOT NULL,
	ZRID	bigint NOT NULL,
	ZVER	bigint NOT NULL,
	ZTOV	bigint NOT NULL,
	ZSID	bigint NOT NULL,
	ZLVL	smallint NOT NULL,
	ZPID	bigint NOT NULL,
	symbol	varchar(32) NULL,
	type	char(4) NOT NULL,
	title	varchar(99) NULL,
	auth	varchar(255) NULL,
	abstr	varchar(255) NULL,
	PRIMARY KEY (ZOID,ZRID,ZVER)
	);
CREATE INDEX DDocument_idx1 on TISC.DDocument(symbol);
