-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE at the project's root directory
--

-- dictionary filling assistance. db admin use only
CREATE OR REPLACE FUNCTION dic.set_code(
   v_dic char(16), v_code char(16), v_value varchar(99), v_descr varchar(255)
	) RETURNS VOID LANGUAGE plpgSQL SECURITY DEFINER AS $$
  BEGIN
  	update dic.ZCode as ZC set value = v_value, descr = v_descr
	where ZC.dic = v_dic and ZC.code = v_code;
	IF NOT FOUND THEN
	 insert into dic.ZCode values($1,$2,$3,$4);
	END IF;
  END $$;

