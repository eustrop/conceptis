-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

DROP VIEW IF EXISTS DIC.V_QField CASCADE;
CREATE VIEW DIC.V_QField AS SELECT * FROM DIC.QField;
--GRANT SELECT ON DIC.V_QField to tis_users;
