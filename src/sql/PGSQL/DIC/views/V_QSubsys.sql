-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

DROP VIEW IF EXISTS DIC.V_QSubsys CASCADE;
CREATE VIEW DIC.V_QSubsys AS SELECT * FROM DIC.QSubsys;
--GRANT SELECT ON DIC.V_QSubsys to tis_users;
