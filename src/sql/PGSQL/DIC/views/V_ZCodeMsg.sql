-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE.ConcepTIS at the project's root directory
--
-- view for localized messages of the main dictionary table (dic.ZCode)

DROP VIEW IF EXISTS dic.V_ZCodeMsg;
CREATE VIEW dic.V_ZCodeMsg as SELECT * FROM dic.ZCodeMsg;
--GRANT SELECT ON dic.V_ZCodeMsg to tis_users;
