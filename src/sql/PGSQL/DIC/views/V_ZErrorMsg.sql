-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE.ConcepTIS at the project's root directory
--

DROP VIEW IF EXISTS dic.V_ZErrorMsg;
CREATE VIEW dic.V_ZErrorMsg as SELECT * FROM dic.ZErrorMsg;
--GRANT SELECT ON dic.V_ZErrorMsg to tis_users;
