-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE.ConcepTIS at the project's root directory
--
-- view for main dictionary table (dic.ZCode)

DROP VIEW IF EXISTS dic.V_ZDic;
CREATE VIEW dic.V_ZDic as SELECT * FROM dic.ZCode;
--GRANT SELECT ON dic.V_ZDic to tis_users;
