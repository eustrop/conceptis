-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

DROP VIEW IF EXISTS DIC.V_QOType CASCADE;
CREATE VIEW DIC.V_QOType AS SELECT * FROM DIC.QOType;
--GRANT SELECT ON DIC.V_QOType to tis_users;
