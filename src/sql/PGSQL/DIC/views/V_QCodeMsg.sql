-- QTIS project
--
-- (c) Alex V Eustrop & EustroSoft.org 2022
-- project's home: qtis.eustrosoft.org
--
-- LICENSE (whole project): see LICENSE.QTIS file at root directory of project
-- LICENSE (this file): BALES, BSD or MIT on your choice.
-- LICENSE (this file): see LICENSE.BALES file at the root of this project
-- LICENSE (this file): see bales.eustrosoft.org if no LICENSE.BALES file found
--
-- this file based on code from
-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE.ConcepTIS at the project's root directory
--
-- view for localized messages of the main dictionary table (dic.QCode)

DROP VIEW IF EXISTS dic.V_QCodeMsg;
CREATE VIEW dic.V_QCodeMsg as SELECT * FROM dic.QCodeMsg;
--GRANT SELECT ON dic.V_QCodeMsg to qtis_users;
