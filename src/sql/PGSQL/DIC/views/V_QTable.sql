-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

DROP VIEW IF EXISTS DIC.V_QTable CASCADE;
CREATE VIEW DIC.V_QTable AS SELECT * FROM DIC.QTable ;
--GRANT SELECT ON DIC.V_QTable to tis_users;
