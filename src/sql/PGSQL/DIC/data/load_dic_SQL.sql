-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2020
-- see LICENSE at the project's root directory
--
-- purpose: load data into SQL dictionary - collection of useful SQL queries

-- useful SQL requests
select dic.set_code('SQL','0000000000000000','default SQL ping query','select pg_backend_pid();');
select dic.set_code('SQL','CURRENT_DBINFO','information about current database', 'select current_database(), version();');
select dic.set_code('SQL','CURRENT_USERINFO','information about current user','select SAM.get_user(),SAM.get_user_login(),SAM.get_user_slevel(),user;');
select dic.set_code('SQL','USEFUL_VIEWSNOTE','note about useful views', 'select ''useful views could be looked at pg_views, tables at pg_tables and so on''');
select dic.set_code('SQL','LIST_TIS_VIEWS','list of all TIS/SQL views','select schemaname, viewname from pg_views where schemaname in(''dic'',''sam'',''tis'',''tisc'') order by schemaname');
select dic.set_code('SQL','NULL_BODY','null request body (desc is null)',null);
select dic.set_code('SQL','SELECT_NULL','null request body (desc is null)', 'select null');
select dic.set_code('SQL','RANGES_INFO','show info about ranges', 'select startid_hex || ''/'' || bitl::text as startid_hex_b, endid_hex, prange_hex || ''/'' || pbitl::text as prange_hex_b, ownerid,descr from SAM.V_RangesInfo order by startid');
