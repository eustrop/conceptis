-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- purpose: load data into main dictionary (dic.ZCode)

select dic.drop_all_dic();
select dic.set_code('YESNO','Y','YES/TRUE',null);
select dic.set_code('YESNO','N','NO/FALSE',null);
select dic.set_code('YESNOUNK','Y','YES/TRUE',null);
select dic.set_code('YESNOUNK','N','NO/FALSE',null);
select dic.set_code('YESNOUNK','?','UNKNOWN',null);
select dic.set_code('LANG','*','Default',null);
select dic.set_code('LANG','EN','English',null);
select dic.set_code('LANG','RU','Russian',null);
select dic.set_code('LANG','UK','Ukrainian',null);
select dic.set_code('LANG','FR','French',null);
select dic.set_code('LANG','DE','German',null);
select dic.set_code('LANG','ES','Español',null);
select dic.set_code('LANG','PT','Português',null);
select dic.set_code('LANG','EO','Esperanto',null);
select dic.set_code('LANG','NL','Nederlands',null);
select dic.set_code('LANG','EL','Hellenic',null);
select dic.set_code('LANG','ZH','汉语',null);
select dic.set_code('LANG','TW','漢語',null);
select dic.set_code('LANG','JP','日本語',null);
-- security levels model designed 2022-05-11 by Eustrop
-- seven conceptual levels 1X-7X fo regular use
-- one special level (0X) for system data
-- each level has 16 sublevels 0-F (X0..XF)
-- NOTE: real storage for SLEVEL is smallint (int2)
-- it can store 16 bits number from 0x0000 to 0xFFFF
-- levels from 80..FF are not used now, but can be defined in the future
-- the highest 8 bits are planned to non-hierarchical MAC
-- currently defined levels from 00 up to 7F are fits in single byte
-- with no problem with signed/unsigned arithmetic
select dic.set_code('SLEVEL','15','(0F) System','objects with SLEVEL System (00..0F) or less are writable only by subjects with SLEVEL not greater than system');
select dic.set_code('SLEVEL','31','(1F) Public','objects with SLEVEL Public (10..1F) are readable by anyone including guests');
select dic.set_code('SLEVEL','47','(2F) Public+','objects with SLEVEL Public+ (20..2F) are readable by identified users only with SLEVEL greater or equal its SLEVEL');
select dic.set_code('SLEVEL','63','(3F) Restricted/FOUO (DSP)','objects with SLEVEL Restrited/FOUO (30..3F) are readable only by authorized users with SLEVEL greater or equal its SLEVEL');
select dic.set_code('SLEVEL','64','(40) PRE-CONFIDENTIAL (DSP+)','objects and subjects with SLEVEL PRE-CONFIDENTIAL (0x40) must be treated under CONFIDENTIAL policy');
select dic.set_code('SLEVEL','79','(4F) CONFIDENTIAL (S)','objects with SLEVEL CONFIDENTIAL (40..4F) are under goverment regulation and are managed in accordance to strict MAC model (Bell–LaPadula)');
select dic.set_code('SLEVEL','95','(5F) SECRET (SS)','objects with SLEVEL SECRET (50..5F) are under goverment regulation and are managed in accordance to strict MAC model (Bell–LaPadula)');
select dic.set_code('SLEVEL','111','(6F) TOP SECRET (SSOV)','objects with SLEVEL TOP SECRET (60..6F) are under goverment regulation and are managed in accordance to strict MAC model (Bell–LaPadula)');
select dic.set_code('SLEVEL','127','7F - God-use-only','SLEVEL God-use-only (70..7F) can be used for objects and subjects that are more guarded than any legally defined levels');
-- SLEVEL in Russian by default. comment next lines for international use (english version of captions above)
select dic.set_code('SLEVEL','15','(0F) Системный','objects with SLEVEL System (00..0F) or less are writable only by subjects with SLEVEL not greater than system');
select dic.set_code('SLEVEL','31','(1F) Публичный','objects with SLEVEL Public (10..1F) are readable by anyone including guests');
select dic.set_code('SLEVEL','47','(2F) Публичный+ (ограниченный)','objects with SLEVEL Public+ (20..2F) are readable by identified users only with SLEVEL greater or equal its SLEVEL');
select dic.set_code('SLEVEL','63','(3F) Корпоративный (ДСП)','objects with SLEVEL Restrited/FOUO (30..3F) are readable only by authorized users with SLEVEL greater or equal its SLEVEL');
select dic.set_code('SLEVEL','64','(40) Пред-Секретно (ДСП+)','objects and subjects with SLEVEL PRE-CONFIDENTIAL (0x40) must be treated under CONFIDENTIAL policy');
select dic.set_code('SLEVEL','79','(4F) Секретно (С)','objects with SLEVEL CONFIDENTIAL (40..4F) are under goverment regulation and are managed in accordance to strict MAC model (Bell–LaPadula)');
select dic.set_code('SLEVEL','95','(5F) Сов-секретно (СС)','objects with SLEVEL SECRET (50..5F) are under goverment regulation and are managed in accordance to strict MAC model (Bell–LaPadula)');
select dic.set_code('SLEVEL','111','(6F) Сов-секретно ОВ (СС-ОВ)','objects with SLEVEL TOP SECRET (60..6F) are under goverment regulation and are managed in accordance to strict MAC model (Bell–LaPadula)');
select dic.set_code('SLEVEL','127','7F - Только для мертвых (ТДМ)','SLEVEL God-use-only (70..7F) can be used for objects and subjects that are more guarded than any legally defined levels');

--
select dic.set_code('SLEVEL_CODE','0F','System','objects with SLEVEL System (00..0F) or less are writable only by subjects with SLEVEL not greater than system');
select dic.set_code('SLEVEL_CODE','1F','Public','objects with SLEVEL Public (10..1F) are readable by anyone including guests');
select dic.set_code('SLEVEL_CODE','2F','Public+','objects with SLEVEL Public+ (20..2F) are readable by identified users only with SLEVEL greater or equal its SLEVEL');
select dic.set_code('SLEVEL_CODE','3F','Restricted/FOUO (DSP)','objects with SLEVEL Restrited/FOUO (30..3F) are readable only by authorized users with SLEVEL greater or equal its SLEVEL');
select dic.set_code('SLEVEL_CODE','40','PRE-CONFIDENTIAL (DSP+)','objects and subjects with SLEVEL PRE-CONFIDENTIAL (0x40) must be treated under CONFIDENTIAL policy');
select dic.set_code('SLEVEL_CODE','4F','CONFIDENTIAL (S)','objects with SLEVEL CONFIDENTIAL (40..4F) are under goverment regulation and are managed in accordance to strict MAC model (Bell–LaPadula)');
select dic.set_code('SLEVEL_CODE','5F','SECRET (SS)','objects with SLEVEL SECRET (50..5F) are under goverment regulation and are managed in accordance to strict MAC model (Bell–LaPadula)');
select dic.set_code('SLEVEL_CODE','6F','TOP SECRET (SSOV)','objects with SLEVEL TOP SECRET (60..6F) are under goverment regulation and are managed in accordance to strict MAC model (Bell–LaPadula)');
select dic.set_code('SLEVEL_CODE','7F','God-use-only','SLEVEL God-use-only (70..7F) can be used for objects and subjects that are more guarded than any legally defined levels');
-- old security levels, not used since 2023-04-35 
--select dic.set_code('SLEVEL','0','unclassified',null);
--select dic.set_code('SLEVEL','10','restricted',null);
--select dic.set_code('SLEVEL','20','classified',null);
--select dic.set_code('SLEVEL','30','top secret',null);
--select dic.set_code('SLEVEL','40','more then top secret',null);
select dic.set_code('CAPABILITY','SAM_MANAGE','SECURITY RISK! manage any SAM objects',null);
select dic.set_code('CAPABILITY','SAM_MANAGE_SLEVEL_MAX','SECURITY SLEVEL RISK! assign slevel up to slevel_max to other users',null);
select dic.set_code('CAPABILITY','SAM_DBOWNER','this user able to use DB passing over TIS',null);
select dic.set_code('CAPABILITY','SAM_VIEWAUDITLOG','this user able to view SAM.AuditEvent data',null);
select dic.set_code('CAPABILITY','SLVL_MANAGE','change SLEVEL of objects',null);
select dic.set_code('CAPABILITY','SLVL_DECREASE','decrease SLEVEL of objects',null);
select dic.set_code('CAPABILITY','FORCE_ROLLBACK','rollback logical transactions owned by others',null);
select dic.set_code('CAPABILITY','MOVE_SEND','send data object to another scope',null);
select dic.set_code('CAPABILITY','MOVE_RECEIVE','receive data object from "transit" state',null);
select dic.set_code('CAPABILITY','V0_BYPASS_ACL','SECURITY BYPASS! view data over V0_ (since 2020-12-04)',null);
select dic.set_code('CAPABILITY','WRITE_SYSEM_OBJECTS','SECURITY RISK! write system objects that can have indirect access to information per another users (since 2023-04-43)',null);
select dic.set_code('CAPABILITY','SVC_SQL_ADHOC','SQL Ad-hoc requests allowed. Controlled at the level of web services providing access to ad-hoc SQL queries (since 2024-02-07)',null);
select dic.set_code('EVENT_CLASS','0','system, unmaskable',null);
select dic.set_code('EVENT_CLASS','1','data access, unmaskable',null);
select dic.set_code('EVENT_CLASS','2','data access, maskable',null);
select dic.set_code('EVENT_CLASS','3','warning, maskable',null);
select dic.set_code('EVENT_CLASS','4','debug, maskable',null);
select dic.set_code('EVENT_ACTION','C','create',null);
select dic.set_code('EVENT_ACTION','U','update',null);
select dic.set_code('EVENT_ACTION','F','field changed',null);
select dic.set_code('EVENT_ACTION','D','delete',null);
select dic.set_code('EVENT_ACTION','V','read',null);
select dic.set_code('EVENT_ACTION','H','history read',null);
select dic.set_code('EVENT_ACTION','X','execute',null);
select dic.set_code('EVENT_ACTION','XC','capability used',null);
select dic.set_code('EVENT_ACTION','R','rollback',null);
select dic.set_code('EVENT_ACTION','M','metadata',null);
select dic.set_code('EVENT_ACTION','UD','undelete',null);
select dic.set_code('EVENT_ACTION','N','rename',null);
select dic.set_code('EVENT_ACTION','L','lock',null);
select dic.set_code('EVENT_ACTION','UL','unlock',null);
select dic.set_code('EVENT_ACTION','TS','send',null);
select dic.set_code('EVENT_ACTION','TT','send to',null);
select dic.set_code('EVENT_ACTION','TR','receive',null);
select dic.set_code('EVENT_ACTION','SR','slevel request',null);
select dic.set_code('EVENT_ACTION','SI','slevel increase',null);
select dic.set_code('EVENT_ACTION','SD','slevel decrease',null);
select dic.set_code('EVENT_ACTION','OO','object open',null);
select dic.set_code('EVENT_ACTION','OC','object commit',null);
select dic.set_code('EVENT_ACTION','OR','object rollback',null);
select dic.set_code('EVENT_ACTION','OM','object move',null);
select dic.set_code('EVENT_ACTION','AW','access write',null);
select dic.set_code('EVENT_ACTION','AC','access create',null);
select dic.set_code('EVENT_ACTION','AD','access delete',null);
select dic.set_code('EVENT_ACTION','AR','access read',null);
select dic.set_code('EVENT_ACTION','AL','access lock',null);
select dic.set_code('EVENT_ACTION','AH','access history',null);
select dic.set_code('ACCESS_ACTION','W','write',null);
select dic.set_code('ACCESS_ACTION','C','create',null);
select dic.set_code('ACCESS_ACTION','D','delete',null);
select dic.set_code('ACCESS_ACTION','R','read',null);
select dic.set_code('ACCESS_ACTION','H','history read',null);
select dic.set_code('ACCESS_ACTION','L','lock',null);
select dic.set_code('ZSTA','N','New','Current version');
select dic.set_code('ZSTA','C','Corrected','corrected version of object');
select dic.set_code('ZSTA','R','Rollback','(reserved) rolled back');
select dic.set_code('ZSTA','P','Prepared','(reserved) prepared for commit, QC passed');
select dic.set_code('ZSTA','D','Deleted','Deleted object');
select dic.set_code('ZSTA','U','Undeleted','Undeleted object');
select dic.set_code('ZSTA','I','Intermediate','Incomplete logical transaction');
select dic.set_code('ZSTA','L','Locked','Object is locked');
select dic.set_code('ZSTA','F','Free','(reserved) object been freed after locking');
select dic.set_code('ZSTA','T','Transit','object in transit state');
select dic.set_code('ZSTA','A','Accepted','(reserved) Accepted from transit');
select dic.set_code('ZSTA','W','Withdrawn','(reserved)Withdrawn from transit');
select dic.set_code('ZSTA','M','Moved','Object has been moved or it''s ZLVL changed');
select dic.set_code('ZSTA','Q','QRSQ','Object in QRID sequence state (generation of sequental numbers from QRSQ)');
--select dic.set_code('TIS_OBJECT_TYPE','A','Area','Area of responsibility');
--select dic.set_code('TIS_OBJECT_TYPE','C','Container','Container with various objects');
--select dic.set_code('TIS_OBJECT_TYPE','D','Document','Some documental object');
select dic.set_code('TIS_OBJECT_TYPE','TISC.A','Area','Area of responsibility');
select dic.set_code('TIS_OBJECT_TYPE','TISC.C','Container','Container with various objects');
select dic.set_code('TIS_OBJECT_TYPE','TISC.D','Document','Some documental object');
select dic.set_code('TIS_OBJECT_TYPE','FS.F','File','Some file or directory object');
select dic.set_code('TIS_OBJECT_TYPE','X','SAM','SAM subsystem tables prefix');
select dic.set_code('TIS_OBJECT_TYPE','Y','YETANOTHER','Prefix for multicharacter codes of objects types');
select dic.set_code('TIS_OBJECT_TYPE','Z','SYSTEM','SYSTEM tables prefix');
select dic.set_code('TIS_OBJECT_TYPE','TIS.Q','Sequence','Sequnce generator via QSRQ field of ZObject/QObject and ZSTA=Q');
select dic.set_code('TIS_OBJECT_TYPE','QR.M','Member','');
select dic.set_code('TIS_OBJECT_TYPE','QR.P','Product','');
select dic.set_code('TIS_OBJECT_TYPE','QR.C','Card','');
select dic.set_code('TIS_OBJECT_TYPE','QR.Q','QR-Card','');
select dic.set_code('TIS_OBJECT_TYPE','MSG.C','Message Channel','');
--select dic.set_code('ACL_OBJECT_TYPE','A','Area','Area of responsibility');
--select dic.set_code('ACL_OBJECT_TYPE','C','Container','Container with various objects');
--select dic.set_code('ACL_OBJECT_TYPE','D','Document','Some documental object');
select dic.set_code('ACL_OBJECT_TYPE','TIS.Q','Sequence','Sequnce generator via QSRQ field of ZObject/QObject and ZSTA=Q');
select dic.set_code('ACL_OBJECT_TYPE','TISC.A','Area','Area of responsibility');
select dic.set_code('ACL_OBJECT_TYPE','TISC.C','Container','Container with various objects');
select dic.set_code('ACL_OBJECT_TYPE','TISC.D','Document','Some documental object');
select dic.set_code('ACL_OBJECT_TYPE','FS.F','File','Some file or directory object');
select dic.set_code('ACL_OBJECT_TYPE','QR.M','Member','');
select dic.set_code('ACL_OBJECT_TYPE','QR.P','Product','');
select dic.set_code('ACL_OBJECT_TYPE','QR.C','Card','');
select dic.set_code('ACL_OBJECT_TYPE','QR.Q','QR-Card','');
select dic.set_code('ACL_OBJECT_TYPE','MSG.C','Message Channel','');
select dic.set_code('ACLA','Y','Allowed','access allowed (if not rejected)');
select dic.set_code('ACLA','N','Not Allowed','access not allowed (but not rejected)');
select dic.set_code('ACLA','R','Rejected','access rejected (even if allowed by another ACL)');
select dic.set_code('DIGEST_ALGO','PLAIN','PLAIN','plain (nonecrypted)');
select dic.set_code('DIGEST_ALGO','MD5','MD5','MD5 hash');
-- TISC subsystem
select dic.set_code('DD02','MANU','manuscript',null);
select dic.set_code('DD02','ARTL','article',null);
select dic.set_code('DD02','BOOK','book',null);
select dic.set_code('DD02','DISS','dissertation',null);
select dic.set_code('DD02','ENC','encyclopedia',null);
select dic.set_code('DD02','REF','reference book',null);
select dic.set_code('DD02','LIST','list (catalogue)',null);
select dic.set_code('DD02','ANNX','annex',null);
select dic.set_code('DD02','SITE','site (Internet)',null);
select dic.set_code('DR02','U','use (cite)',null);
select dic.set_code('DR02','R','replace',null);
select dic.set_code('DR02','A','add to (supply)',null);
select dic.set_code('DR02','I','include',null);
select dic.set_code('DP01','NUM','number','any numeric value');
select dic.set_code('DP01','TEXT','text','any textual value');
select dic.set_code('DP01','MIX','mixed','mixed value (both NUM & TEXT parts)');
select dic.set_code('CC02','STOR','container storage','must contain containers only');
select dic.set_code('CC02','FILE','file for documents','must contain documents only');
select dic.set_code('CC02','MIX','mixed storage','can contain any type of objects');
-- FS subsystem
-- 'B' - бинарный файл, 'D' - директория, 'T' - текстовый файл, 'R' - Корень иерархии, 'M' - смешанный тип, 'C' договор, 'W' wiki
select dic.set_code('FILE_TYPE','B','binary file','binary media (pdf,doc,jpeg,... an so on)');
select dic.set_code('FILE_TYPE','D','directory file','');
select dic.set_code('FILE_TYPE','T','text file','textual content store, use mime-type for details');
select dic.set_code('FILE_TYPE','R','root of FS','');
select dic.set_code('FILE_TYPE','M','mixed-content file','see mime-type field for detail');
select dic.set_code('FILE_TYPE','C','Contract-type file','Contract beetwin organizations');
select dic.set_code('FILE_TYPE','W','Wiki-content file','');
-- QR subsystem
select dic.set_code('LEI_TYPE','NONE','none','');
select dic.set_code('LEI_TYPE','INN','ИНН','');
select dic.set_code('LEI_TYPE','OGRN','ОГРН','');
select dic.set_code('LET_TYPE','NONE','нет','');
--select dic.set_code('LET_TYPE','RU.OOO','ООО','');
--select dic.set_code('LET_TYPE','RU.IP','ИП','');
--select dic.set_code('LET_TYPE','RU.AO','АО','');
--select dic.set_code('LET_TYPE','RU.PAO','ПАО','');
--MSG subsystem
select dic.set_code('MSG_CHANNEL_STATUS','N','new','');
select dic.set_code('MSG_CHANNEL_STATUS','W','work in process','');
select dic.set_code('MSG_CHANNEL_STATUS','I','internal','');
select dic.set_code('MSG_CHANNEL_STATUS','C','closed','');

select dic.set_code('MSG_MESSAGE_TYPE','M','Simple','');
select dic.set_code('MSG_MESSAGE_TYPE','L','Like','');
select dic.set_code('MSG_MESSAGE_TYPE','P','Survey','');
select dic.set_code('MSG_MESSAGE_TYPE','A','Answer','');

select dic.set_code('MSG_PARTY_ROLE','C','Creator','');
select dic.set_code('MSG_PARTY_ROLE','U','User','');
-- useful SQL requests
select dic.set_code('SQL','0000000000000000','default SQL ping query','select pg_backend_pid();');
--
select dic.set_code('TIS_FIELD','ZSID','Область','Область данных, содержащая документы');
select dic.set_code('TIS_FIELD','ZLVL','Секретность','Уровень секретности документа');
--
select dic.set_code('TIS_FIELD','XU01','User id','Unique identifier of User');
select dic.set_code('TIS_FIELD','XU02','Scope','Scope id where this user definition located');
select dic.set_code('TIS_FIELD','XU03','slevel','Secrecy access level of this person');
select dic.set_code('TIS_FIELD','XU04','MIN slevel','write minimum secrecy access level of this person');
select dic.set_code('TIS_FIELD','XU05','MAX slevel','write maximum secrecy access level of this person');
select dic.set_code('TIS_FIELD','XU06','Disabled','This person unallowed to access system at present time');
select dic.set_code('TIS_FIELD','XU07','Language','Default language that this user prefer to obtain messages at');
select dic.set_code('TIS_FIELD','XU08','Login','Unique login name of this user');
select dic.set_code('TIS_FIELD','XU09','DB Username','DB user that this TIS user corresponding to ');
select dic.set_code('TIS_FIELD','XU10','Full name','Full name of this person or textual specification of robot');
--
select dic.set_code('TIS_FIELD','MM01','Кодовое имя','Уникальное символическое имя');

--
select dic.set_ErrorMsg(-4,'I_DEBUG','*','%1');
select dic.set_ErrorMsg(-3,'I_WARNING','*','%1');
select dic.set_ErrorMsg(-2,'I_INFO2','*','%1');
select dic.set_ErrorMsg(-1,'I_INFO','*','%1');
--lect dic.set_ErrorMsg(0,'1234567890123456','*','');
select dic.set_ErrorMsg(0,'I_SUCCESS','*','success access');
select dic.set_ErrorMsg(1,'E_NOTIMPLEMENTED','*','not implemented');
select dic.set_ErrorMsg(2,'E_UNKNOWN','*','unknown error occured : %1');
select dic.set_ErrorMsg(3,'E_SQL','*','SQL error %1 : %2');
select dic.set_ErrorMsg(4,'E_TRANSISOLATION','*','Invalid transaction isolation "%1". "%2" required');
select dic.set_ErrorMsg(5,'E_OBJECTCHANED','*','data object %1 changed since version %2 (current version=%3)');
select dic.set_ErrorMsg(6,'E_OBJECTLOCKED','*','data object locked');
select dic.set_ErrorMsg(7,'E_NOCHANGES','*','no changes done');
--lect dic.set_ErrorMsg(10,'1234567890123456','*','');
select dic.set_ErrorMsg(11,'E_NOACCESS','*','no access');
select dic.set_ErrorMsg(12,'E_ACCESSDENIED','*','access denied by %1');
select dic.set_ErrorMsg(13,'E_NOCAPABILITY','*','no required capability %1 for scope %2');
select dic.set_ErrorMsg(14,'E_SLVL_LOW','*','object secrecy level (%1) too low for user''s one (%2) BLM-MAC deny writing.');
select dic.set_ErrorMsg(15,'E_SLVL_HIGH','*','object secrecy level (%1) too high for user''n one (%2) BIBA MAC deny writing.');
select dic.set_ErrorMsg(16,'E_NOUSER','*','there are no current user for you (DBMS user is %1)');
select dic.set_ErrorMsg(17,'E_USERLOCKED','*','account of user %1 is locked');
select dic.set_ErrorMsg(18,'E_USERNOTGRANTED','*','you have not permission to use account %1');
select dic.set_ErrorMsg(19,'E_USERSESSIONBOUND','*','This session already bound to user %1');
select dic.set_ErrorMsg(20,'E_USERINVALIDPASSWORD','*','Invalid password for user %1');
select dic.set_ErrorMsg(21,'E_SECRETTOOSHORT','*','Passed secret too short. must be at least %1 symbols (from %2)');
--
select dic.set_ErrorMsg(31,'E_NORECORD','*','record "%1" with %2="%3" does not exists');
select dic.set_ErrorMsg(32,'E_NOBJECT','*','object "%1" with %2="%3" does not exists');
select dic.set_ErrorMsg(33,'E_NOSCOPE','*','scope %1 does not exists');
select dic.set_ErrorMsg(34,'E_NOCODE','*','there are no code "%1" at dictionary "%2"');
select dic.set_ErrorMsg(35,'E_NOTOPENED','*','object %1 does not opened');
select dic.set_ErrorMsg(36,'E_NOTOWNER','*','you are not owner of %1');
select dic.set_ErrorMsg(37,'E_NOTLOCKED','*','object %1 does not locked');
select dic.set_ErrorMsg(38,'E_NOTRANGESEQ','*','There are no more QOID numbers in SAM.RangeSeq for object type %1 in scope with id %2');
--
select dic.set_ErrorMsg(41,'E_DUPRECORD','*','record "%1" with %2="%3" already exists');
select dic.set_ErrorMsg(42,'E_DUPOBJECT','*','object "%1" with %2="%3" already exists');
select dic.set_ErrorMsg(43,'E_2MANYRECORDS','*','Too many records %1 for %2 (maximum %3 exceeded)');
select dic.set_ErrorMsg(44,'E_2MANYOBJECTS','*','Too many objects %1 for scope %2 (maximum %3 exceeded)');
select dic.set_ErrorMsg(45,'E_NOTENOUGHRECS','*','not enough records %1 for %2 (at least %3 required)');
--lect dic.set_ErrorMsg(10,'1234567890123456','*','');
select dic.set_ErrorMsg(50,'E_NOTNULL','*','unallowed null value for field "%2" of "%1"');
select dic.set_ErrorMsg(51,'E_INVALIDVALUE','*','invalid value "%1" for field "%2"');
select dic.set_ErrorMsg(52,'E_INVALIDCODE','*','invalid code value "%1" for field "%2" (dictionary %3)');
select dic.set_ErrorMsg(53,'E_INVALIDCHPTS','*','invalid date %2 for scope %1 (conflict with CHPTS=%3)');
select dic.set_ErrorMsg(54,'E_INVALIDZTYPE','*','invalid object type code "%1"');
select dic.set_ErrorMsg(55,'E_INVALIDOBJECT','*','invalid object ZTYPE=%1 ZOID=%2');
select dic.set_ErrorMsg(56,'E_INVALIDVERSION','*','invalid object version ZTYPE=%1 ZOID=%2 ZVER=%3');
select dic.set_ErrorMsg(57,'E_INVALIDVERDATE','*','specified version has invalid ZDATE=%1');
select dic.set_ErrorMsg(58,'E_INVALIDRECORD','*','invalid record %1 for object %2');
select dic.set_ErrorMsg(58,'E_INVALIDPARENT','*','invalid ZPID = %1 for record %2 ZRID = %3');
--lect dic.set_ErrorMsg(10,'1234567890123456','*','');
select dic.set_ErrorMsg(61,'E_NONSOLERECORD','*','there are more then one record "%1" with %2="%3" exists');
select dic.set_ErrorMsg(62,'E_NONSOLEOBJECT','*','there are more then one object "%1" with %2="%3" exists');
select dic.set_ErrorMsg(63,'E_NONLOCALSCOPE','*','non local scope sid=%1');
--lect dic.set_ErrorMsg(10,'1234567890123456','*','');
select dic.set_ErrorMsg(71,'E_NARECORD4OBJ','*','not allowed record %1 for object %2');
select dic.set_ErrorMsg(72,'E_NAZTYPE4SCOPE','*','not allowed object %1 for scope %2');
select dic.set_ErrorMsg(73,'E_NASYSZTYPE','*','not allowed system object type %1 for regular operation');
select dic.set_ErrorMsg(74,'E_NASUBSYSTEM','*','not allowed subsystem %1');
--
-- Erros 140-149 - SLVL_LOW errors
select dic.set_ErrorMsg(140,'E_SLVL_SYSLOW','*','object secrecy level (%1) is on SYSTEM slevel. WRITE_SYSTEM_OBJECT capability required and user''s slevel_min (%2) less or equal object''s one. TSRM-MAC deny writing.');
select dic.set_ErrorMsg(141,'E_SLVL_LOW_SYS','*','object secrecy level (%1) too low for system''s slevel_min (%2) TSRM-MAC deny writing.');
select dic.set_ErrorMsg(142,'E_SLVL_LOW_SID','*','object secrecy level (%1) too low for scope''s slevel_min (%2) TSRM-MAC deny writing.');
select dic.set_ErrorMsg(143,'E_SLVL_LOW_USER','*','object secrecy level (%1) too low for users''s slevel_min (%2) TSRM-MAC deny writing.');
-- Erros 150-159 - SLVL_HIGH errors
select dic.set_ErrorMsg(150,'E_SLVL_SYSHIGH','*','object secrecy level (%1) is on SYSHIGH slevel. special capability needed in addition to current user''n slevel_max (%2). TSRM-BIBA MAC deny writing.');
select dic.set_ErrorMsg(151,'E_SLVL_HIGH_SYS','*','object secrecy level (%1) too high for system''s slevel_max (%2) TSRM-MAC deny writing.');
select dic.set_ErrorMsg(152,'E_SLVL_HIGH_SID','*','object secrecy level (%1) too high for scope''s slevel_max (%2) TSRM-MAC deny writing.');
select dic.set_ErrorMsg(153,'E_SLVL_HIGH_USER','*','object secrecy level (%1) too high for users''s slevel_max (%2) TSRM-MAC deny writing.');
--
