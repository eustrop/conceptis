-- QTIS project
--
-- (c) Alex V Eustrop & EustroSoft.org 2022
-- project's home: qtis.eustrosoft.org
--
-- LICENSE (whole project): see LICENSE.QTIS file at root directory of project
-- LICENSE (this file): BALES, BSD or MIT on your choice.
-- LICENSE (this file): see LICENSE.BALES file at the root of this project
-- LICENSE (this file): see bales.eustrosoft.org if no LICENSE.BALES file found
--
-- this file based on code from
-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE.ConcepTIS at the project's root directory
--
-- purpose: check for code existence for dictionary

CREATE OR REPLACE FUNCTION dic.check_code(v_dic char(32), v_code char(32))
	RETURNS boolean LANGUAGE plpgSQL SECURITY DEFINER AS $$
DECLARE
i integer;
BEGIN
 SELECT COUNT(*) INTO i FROM dic.QCode WHERE dic = v_dic and code = v_code;	
 RETURN (i <> 0);
END $$;

-- check code for existence or null (NULl Allowed)
CREATE OR REPLACE FUNCTION dic.check_code_nula(v_dic char(32), v_code char(32))
	RETURNS boolean LANGUAGE plpgSQL SECURITY DEFINER AS $$
DECLARE
i integer;
BEGIN
 IF v_code IS NULL THEN RETURN TRUE; END IF;
 SELECT COUNT(*) INTO i FROM dic.QCode WHERE dic = v_dic and code = v_code;	
 RETURN (i <> 0);
END $$;

