-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

--DROP TABLE IF EXISTS DIC.QTable CASCADE;
CREATE TABLE DIC.QTable (
	subsys	varchar(16) NOT NULL,
	otype	varchar(4) NOT NULL,
	tcode	varchar(4) NOT NULL,
	name	varchar(128) NOT NULL,
	parent	varchar(8) NOT NULL,
	header	varchar(16) NOT NULL,
	maxrec	bigint NULL,
	minrec	bigint NULL,
	QC2	varchar(128) NULL,
	pkey	varchar(255) NULL,
	qname	varchar(128) NULL,
	qdescr	varchar(128) NULL,
	caption	varchar(128) NULL,
	descr	varchar(1024) NULL,
	indexes	varchar(1024) NULL,
	PRIMARY KEY (subsys,otype,tcode)
	);
CREATE INDEX QTable_idx1 on DIC.QTable(parent);
