-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

--DROP TABLE IF EXISTS DIC.QField CASCADE;
CREATE TABLE DIC.QField (
	subsys	varchar(16) NOT NULL,
	otype	varchar(4) NOT NULL,
	tcode	varchar(4) NOT NULL,
	fcode	varchar(2) NOT NULL,
	name	varchar(128) NOT NULL,
	sqltype	varchar(128) NOT NULL,
	tistype	varchar(128) NULL,
	length	numeric NULL,
	clength	bigint NULL,
	nullable	char(1) NOT NULL,
	ACL	char(1) NOT NULL,
	dic	varchar(32) NULL,
	attr	varchar(128) NULL,
	caption	varchar(128) NULL,
	descr	varchar(1024) NULL,
	PRIMARY KEY (subsys,otype,tcode,fcode)
	);
