-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

--DROP TABLE IF EXISTS DIC.QOType CASCADE;
CREATE TABLE DIC.QOType (
	subsys	varchar(16) NOT NULL,
	otype	varchar(4) NOT NULL,
	name	varchar(128) NOT NULL,
	caption	varchar(128) NULL,
	descr	varchar(1024) NULL,
	PRIMARY KEY (subsys,otype)
	);
