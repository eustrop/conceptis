-- QTIS project
--
-- (c) Alex V Eustrop & EustroSoft.org 2022
-- project's home: qtis.eustrosoft.org
--
-- LICENSE (whole project): see LICENSE.QTIS file at root directory of project
-- LICENSE (this file): BALES, BSD or MIT on your choice.
-- LICENSE (this file): see LICENSE.BALES file at the root of this project
-- LICENSE (this file): see bales.eustrosoft.org if no LICENSE.BALES file found
--
-- this file based on code from
-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE.ConcepTIS at the project's root directory
--
-- localized/internationized values and descriptions for main dictionary
-- stored at dic.QCode table. 

-- DROP TABLE IF EXISTS dic.QCodeMsg CASCADE;
CREATE TABLE dic.QCodeMsg (
	dic	char(32) NOT NULL,
	code	char(32) NOT NULL,
	lang	char(3) NOT NULL,
	value	varchar(128) NOT NULL,
	descr	varchar(1024) NULL,
	PRIMARY KEY (dic,code,lang)
	);
CREATE OR REPLACE VIEW dic.ZCodeMsg as SELECT * FROM dic.QCodeMsg;
