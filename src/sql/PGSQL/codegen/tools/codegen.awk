#!/usr/bin/awk -f
# ConcepTIS project/QTIS project
# (c) Alex V Eustrop 2009-2019
# (c) Alex V Eustrop & EustroSoft.org 2023
# see LICENSE.ConcepTIS at the project's root directory
#
#   $Id$
#
# any subsystem codegeneration - tables

BEGIN{
} #/BEGIN
END{
if(is_aborted)exit 1;
is_begun=1; # used by do_abort
WDIR="work/"
STD_HEADER_OFFSET=7;
make_create_table(WDIR "/tables/" SHORT_NAME ".sql");
make_dic4_table(WDIR "/dic/" SHORT_NAME ".sql");
make_table_drop(WDIR "/tables.drop/" SHORT_NAME ".sql");
make_create_view(WDIR "/views/V_" SHORT_NAME ".sql");
make_view_grant(WDIR "/views.grant/V_" SHORT_NAME ".sql");
make_view_drop(WDIR "/views.drop/V_" SHORT_NAME ".sql");
make_proc_grant(WDIR "/functions.grant/" SHORT_NAME ".sql");
make_proc_drop(WDIR "/functions.drop/" SHORT_NAME ".sql");
make_create_sequence(WDIR "/sequences/" SHORT_NAME "_seq.sql");
make_create_proc(WDIR "/functions/create_" SHORT_NAME ".sql");
#make_create_proc_decl(WDIR "/declarations/create_" SHORT_NAME ".sql");
make_update_proc(WDIR "/functions/update_" SHORT_NAME ".sql");
#make_update_proc_decl(WDIR "/declarations/update_" SHORT_NAME ".sql");
make_delete_proc(WDIR "/functions/delete_" SHORT_NAME ".sql");
#make_delete_proc_decl(WDIR "/declarations/delete_" SHORT_NAME ".sql");
make_QC_proc(WDIR "/functions/QC_" CODE ".sql");
#make_QC_proc_decl(WDIR "/declarations/QC_" CODE ".sql");
make_commit_rec_proc(WDIR "/functions/commit_" CODE ".sql");
#make_commit_rec_proc_decl(WDIR "/declarations/commit_" CODE ".sql");
make_rollback_rec_proc(WDIR "/functions/rollback_" CODE ".sql");
make_delete_object_rec_proc(WDIR "/functions/delete_object_" CODE ".sql");
make_move_rec_proc(WDIR "/functions/move_" CODE ".sql");
make_cmp_rec_proc(WDIR "/functions/cmp_" CODE ".sql");
make_rdelete_rec_proc(WDIR "/functions/rdelete_" CODE ".sql");
make_java_record_class(WDIR "/java/zDAO/" SHORT_NAME ".java");
make_java_semiproducts(WDIR "/java/semiproducts/" SHORT_NAME ".java");
} #/END
/^[ \t]*#/{next;} # comments
($1=="NAME"){NAME=$2;CODE=$3;
	n2=split(NAME,tmp_a,".");
	SUBSYS=tmp_a[1];SHORT_NAME=tmp_a[2];
	if(SHORT_NAME==""){SHORT_NAME = NAME; SUBSYS="SAM";}
	next;
	}
($1=="HEADER"){HEADER=$2;
	if(!is_value_in(HEADER,"STD_HEADER","STD_HEANOR"))
	 do_abort("invalid HEADER: " HEADER);
	next;}
($1=="PARENT"){PARENT=$2;PARENT_CODE=$3;
	n2=split(PARENT,tmp_a,".");
	PARENT_SUBSYS=tmp_a[1];PARENT_SHORT_NAME=tmp_a[2];
	next;}
($1=="CHILD"){
	CHILD_COUNT++; # CHILDREN is to long ;)
	CHILD_NAME[CHILD_COUNT]=$2;
	CHILD_CODE[CHILD_COUNT]=$3;
	n2=split(CHILD_NAME[CHILD_COUNT],tmp_a,".");
	CHILD_SUBSYS[CHILD_COUNT]=tmp_a[1];
	CHILD_SHORT_NAME[CHILD_COUNT]=tmp_a[2];
	if(CHILD_SHORT_NAME[CHILD_COUNT] == ""){
	 CHILD_SUBSYS[CHILD_COUNT] = "TISC";
	 CHILD_SHORT_NAME[CHILD_COUNT]=CHILD_NAME[CHILD_COUNT];
	}
	next;
	}
($1=="PKEY"){do_abort("PKEY token unallowed for object records");}
($1=="OBJECT"){OBJECT_NAME=$2;OBJECT=$3;next;}
($1=="MAXREC"){MAXREC=$2;next;}
($1=="MINREC"){MINREC=$2;next;}
#($1=="QCM"){QCM=$2;if(QCM=="")QCM="QCM_" CODE;next;}
#($1=="FQCM"){FQCM=$2;if(FQCM=="")FQCM="FQCM_" CODE;next;}
($1=="INDEX"){INDEX_COUNT++;INDEX[INDEX_COUNT]=$2;next;}
($1~"[0-9][0-9]"){ # FIELD
	FIELDS_COUNT++;
	f_no[FIELDS_COUNT]=$1;
	f_name[FIELDS_COUNT]=$2;
	f_type[FIELDS_COUNT]=$3;
	f_attrib[FIELDS_COUNT]=$4;
	f_caption[FIELDS_COUNT]=$5;
	f_descr[FIELDS_COUNT]=$6;
	f_NN[FIELDS_COUNT]=1; # NOT NULL by default
	n=split($4,tmp_attrib,",");
	for(i=1;i<=n;i++){
	 n2=split(tmp_attrib[i],tmp_a,"=");
	 if(tmp_a[1]=="NN"){f_NN[FIELDS_COUNT]=1;}
	 else if(tmp_a[1]=="NUL"){f_NN[FIELDS_COUNT]=0;}
	 else if(tmp_a[1]=="SEQID"){do_abort_unallowed_attr(tmp_a[1]);}
	 else if(tmp_a[1]=="ZID"){do_abort_unallowed_attr(tmp_a[1]);}
	 else if(tmp_a[1]=="ZNAME"){f_ZNAME[FIELDS_COUNT]=1;
		if(ZNAME=="")ZNAME=f_name[FIELDS_COUNT];
		else do_abort("only one ZNAME field allowed : " \
			f_name[FIELDS_COUNT] ); }
	 else if(tmp_a[1]=="UNIQ"){f_UNIQ[FIELDS_COUNT]=1;
		f_UNIQ_REALM[FIELDS_COUNT]=tmp_a[2];
		if(f_UNIQ_REALM[FIELDS_COUNT] == "")
		 f_UNIQ_REALM[FIELDS_COUNT] = "DB";
		if(!is_value_in(f_UNIQ_REALM[FIELDS_COUNT],"DB","SCOPE",
		 "OBJECT")) do_abort("invalid realm for UNIQ: " tmp_a[2]);
		if(f_UNIQ_REALM[FIELDS_COUNT] != "OBJECT") has_table_uniq=1;
		}
	 else if(tmp_a[1]=="NOEDIT"){f_NOEDIT[FIELDS_COUNT]=1;
		f_NOEDIT_DEFAULT[FIELDS_COUNT] = tmp_a[2];
		if(tmp_a[2] == "")
		f_NOEDIT_DEFAULT[FIELDS_COUNT] = "null";}
	 else if(tmp_a[1]=="SID"){f_SID[FIELDS_COUNT]=1;
		if(SID_FIELD=="")SID_FIELD=f_name[FIELDS_COUNT];}
	 else if(tmp_a[1]=="SLEVEL"){do_abort_unallowed_attr(tmp_a[1]);}
	 else if(tmp_a[1]=="UID"){f_UID[FIELDS_COUNT]=1;}
	 else if(tmp_a[1]=="GID"){f_GID[FIELDS_COUNT]=1;}
	 else if(tmp_a[1]=="REF"){f_REF[FIELDS_COUNT]=1;
		f_REF_OT[FIELDS_COUNT]=tmp_a[2];
		INDEX_COUNT++;INDEX[INDEX_COUNT]=$2;
		}
	 else if(tmp_a[1]=="DIC"){
	  f_DIC[FIELDS_COUNT]=tmp_a[2];
	  if(f_DIC[FIELDS_COUNT]=="")
	   f_DIC[FIELDS_COUNT]= "" CODE f_no[FIELDS_COUNT];
	 }
	# set of QRDB attributes - ignore it
	 else if(tmp_a[1]==""){} #ignore
	 else if(tmp_a[1]=="PUB"){} #ignore
	 else if(tmp_a[1]=="SHOW"){} #ignore
	 else if(tmp_a[1]=="HEX"){} #ignore
	 else if(tmp_a[1]=="QR"){} #ignore
	 else if(tmp_a[1]=="QR_KEY"){} #ignore
	 else if(tmp_a[1]=="QRANGE_WARN"){} #ignore
	 else if(tmp_a[1]=="TODAY"){} #ignore
	 else if(tmp_a[1]=="DECSEQ"){} #ignore
	 else if(tmp_a[1]=="QRMONEY"){} #ignore
	 else if(tmp_a[1]=="QRPRODTYPE"){} #ignore
	 else if(tmp_a[1]=="QRPRODMODEL"){} #ignore
	 else if(tmp_a[1]=="QRMONEYGOT"){} #ignore
	 else if(tmp_a[1]=="TEXTAREA"){} #ignore
	 else if(tmp_a[1]=="EN"){} #ignore
	#some reserved attributes (ignored yet)
	 #else if(tmp_a[1]=="OBJ"){} #ignore
	 else if(tmp_a[1]=="QRPMREVISION"){} #ignore
	 else if(tmp_a[1]=="OWIKI"){} #ignore
	 else{do_abort("Invalid attributes token: " tmp_a[1]);} 
	}
	next;
 } # FIELD
#//{print ;}
//{do_abort("Invalid start token: " $1); }
function do_abort(msg)
{
if(!is_begun)
 print "PARSING ERROR: " msg "\n at line " NR ": >> " $0 " <<";
else
 print "PROCESSING ERROR: " msg;
is_aborted=1;
exit 1;
}

function do_abort_unallowed_attr(attr)
{
do_abort("Unallowed field's attribute: " attr);
}

function is_value_in(value,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10)
{
if(v10 != "") do_abort("too many arguments for is_value_in(). value=" value);
if(value == "") return 0;
if(value == v1) return 1; if(value == v2) return 1; if(value == v3) return 1;
if(value == v4) return 1; if(value == v5) return 1; if(value == v6) return 1;
if(value == v7) return 1; if(value == v8) return 1; if(value == v9) return 1;
return 0;
}

# assistance
function make_no_header(f)
{
 printf("") >f;
}

function make_header(f)
{
 printf("") >f;
 printf("-- ConcepTIS project/QTIS project\n") >>f;
 printf("-- (c) Alex V Eustrop 2009-2019\n") >>f;
 printf("-- (c) Alex V Eustrop & EustroSoft.org 2023\n") >>f;
 printf("-- see LICENSE.ConcepTIS at the project's root directory\n") >>f;
 printf("--\n") >>f;
 printf("-- $Id$\n") >>f;
 printf("--\n") >>f;
 printf("-- SUBSYSTEM: %s\n",SUBSYS) >>f;
 printf("-- OBJECT: %s (%s)",OBJECT_NAME,OBJECT) >>f;
 printf(" RECORD: %s (%s)\n\n",SHORT_NAME,CODE) >>f;
 printf("-- WARNING! this code produced by automatic codegeneration tool\n")>>f;
 printf("--          located at src/sql/PGSQL/codegen/tools/codegen.awk\n")>>f;
 printf("--          do not change this file directly but modify the above one. \n")>>f;
 printf("\n") >>f;
}

function make_table_drop(f)
{
 make_no_header(f);
 printf("DROP TABLE IF EXISTS %s CASCADE;\n",NAME) >>f;
}

# create table
function make_create_table(f)
{
 make_header(f);
 #printf("DROP TABLE IF EXISTS %s CASCADE;\n",NAME) >>f;
 printf("CREATE TABLE %s (\n",NAME) >>f;
 if( HEADER == "STD_HEADER"){
  printf("\tZOID\tbigint NOT NULL,\n") >>f;
  printf("\tZRID\tbigint NOT NULL,\n") >>f;
  printf("\tZVER\tbigint NOT NULL,\n") >>f;
  printf("\tZTOV\tbigint NOT NULL,\n") >>f;
  printf("\tZSID\tbigint NOT NULL,\n") >>f;
  printf("\tZLVL\tsmallint NOT NULL,\n") >>f;
  printf("\tZPID\tbigint NOT NULL,\n") >>f;
 }else if (HEADER == "STD_HEANOR"){
  printf("\tZRID\tbigint NOT NULL,\n") >>f;
  printf("\tZVER\tbigint NOT NULL,\n") >>f;
  printf("\tZSID\tbigint NOT NULL,\n") >>f;
  printf("\tZLVL\tsmallint NOT NULL,\n") >>f;
  printf("\tZPID\tbigint NOT NULL,\n") >>f;
  printf("\tZSTA\tchar(1) NOT NULL,\n") >>f;
  printf("\tZDATE\ttimestamp NOT NULL,\n") >>f;
  printf("\tZUID\tbigint NOT NULL,\n") >>f;
  printf("\tZDATO\ttimestamp NULL,\n") >>f;
  printf("\tZUIDO\tbigint NULL,\n") >>f;
 }else{ do_abort("HEADER=" HEADER " unimplemented (TABLE)"); }
 for(i=1;i<=FIELDS_COUNT;i++)
 {
  NUL="NULL"; if(f_NN[i]) NUL="NOT " NUL;
  printf("\t%s\t%s %s,\n",f_name[i],f_type[i],NUL) >>f;
 }
 if( HEADER == "STD_HEADER"){
  printf("\tPRIMARY KEY (ZOID,ZRID,ZVER)\n") >>f;
 }else if (HEADER == "STD_HEANOR"){
  printf("\tPRIMARY KEY (ZRID,ZVER)\n") >>f;
 }else{ do_abort("HEADER=" HEADER " unimplemented (PKEY)"); }
 printf("\t);\n") >>f;
 for(i=1;i<=INDEX_COUNT;i++)
 {
  printf("CREATE INDEX %s_idx%s on %s(%s);\n",SHORT_NAME,i,NAME,INDEX[i]) >>f;
 }
} # /make_create_table(f)

# make dic for table
function make_dic4_table(f)
{
 make_header(f);
 for(i=1;i<=FIELDS_COUNT;i++)
 {
  #NUL="NULL"; if(f_NN[i]) NUL="NOT " NUL;
  printf( "select dic.set_code('TIS_FIELD','%s.%s%s','%s','%s');\n",
	SUBSYS,CODE,f_no[i],escape_sql(f_caption[i]),escape_sql(f_descr[i])) >>f;
 }
} # /make_create_table(f)

function escape_sql(s,      c,i,size)
{
size=split(s,c,"");
s="";
for(i=1;i<=size;i++)
{
 if(c[i]=="'"){c[i]="''";}
 #if(c[i]=="\\"){c[i]="\\\\";}
 #if(c[i]=="\""){c[i]="\\\"";}
 s=s c[i];
}
return s;
}


# create view
function make_view_grant(f)
{
 make_no_header(f);
 #printf("GRANT SELECT ON %s.V0_%s to tis_users;\n",SUBSYS,SHORT_NAME) >>f;
 #printf("GRANT SELECT ON %s.V_%s to tis_users;\n",SUBSYS,SHORT_NAME) >>f;
 printf("GRANT SELECT ON %s.V_%s to tis_users;\n",SUBSYS,SHORT_NAME) >>f;
 printf("GRANT SELECT ON %s.VI_%s to tis_users;\n",SUBSYS,SHORT_NAME) >>f;
 printf("GRANT SELECT ON %s.VH_%s to tis_users;\n",SUBSYS,SHORT_NAME) >>f;
 printf("GRANT SELECT ON %s.VD_%s to tis_users;\n",SUBSYS,SHORT_NAME) >>f;
}
function make_view_drop(f)
{
 make_no_header(f);
 printf("DROP VIEW IF EXISTS %s.V0_%s CASCADE;\n",SUBSYS,SHORT_NAME) >>f;
 printf("DROP VIEW IF EXISTS %s.V_%s CASCADE;\n",SUBSYS,SHORT_NAME) >>f;
 printf("DROP VIEW IF EXISTS %s.VI_%s CASCADE;\n",SUBSYS,SHORT_NAME) >>f;
 printf("DROP VIEW IF EXISTS %s.VH_%s CASCADE;\n",SUBSYS,SHORT_NAME) >>f;
 printf("DROP VIEW IF EXISTS %s.VD_%s CASCADE;\n",SUBSYS,SHORT_NAME) >>f;
}
function make_create_view(f)
{
 make_header(f);
 #
 printf("-- V0_ - current state of table without ACL/MAC filtring\n") >>f;
 printf("-- destined for internal use only.\n") >>f;
 printf("-- nobody except tables owner should use it\n") >>f;
 printf("DROP VIEW IF EXISTS %s.V0_%s CASCADE;\n",SUBSYS,SHORT_NAME) >>f;
 printf("CREATE VIEW %s.V0_%s AS SELECT\n",SUBSYS,SHORT_NAME) >>f;
  printf(" * ") >>f; printf("FROM %s AS ZR\n",NAME) >>f;
  printf("\tWHERE ZR.ZTOV = 0;\n") >>f;
 printf("\n") >>f;
 if(HEADER != "STD_HEADER"){ do_abort("HEADER=" HEADER " unimplemented");}

 printf("-- V_ - current state of table with ACL/MAC filtring\n") >>f;
 printf("DROP VIEW IF EXISTS %s.V_%s CASCADE;\n",SUBSYS,SHORT_NAME) >>f;
 printf("CREATE VIEW %s.V_%s AS SELECT",SUBSYS,SHORT_NAME) >>f;
 printf(" * ") >>f; printf("FROM %s AS ZR\n",NAME) >>f;
  printf("\tWHERE ZR.ZTOV = 0 AND\n") >>f;
  printf("\t ZR.ZLVL <= SAM.get_user_slevel() -- MAC - check secrecy \n") >>f;
  printf("\t AND\n") >>f;
  printf("\t -- check pemissions for reading records at scope\n") >>f;
  printf("\t exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG\n") >>f;
  printf("\t   WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND\n") >>f;
  printf("\t    XAS.sid = ZR.ZSID AND XAS.obj_type = '%s.%s'\n",SUBSYS,OBJECT) >>f;
  printf("\t    and XAS.reada = 'Y' )\n") >>f;
  printf("\t -- check rejections for reading records at scope\n") >>f;
  printf("\t AND NOT\n") >>f;
  printf("\t exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG\n") >>f;
  printf("\t   WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND\n") >>f;
  printf("\t     XAS.sid = ZR.ZSID AND XAS.obj_type = '%s.%s'\n",SUBSYS,OBJECT) >>f;
  printf("\t     and XAS.reada = 'R' );\n") >>f;
 printf("\n") >>f;

 printf("-- VI_ - intermediate records created by current user\n") >>f;
 printf("DROP VIEW IF EXISTS %s.VI_%s CASCADE;\n",SUBSYS,SHORT_NAME) >>f;
 printf("CREATE VIEW %s.VI_%s AS SELECT",SUBSYS,SHORT_NAME) >>f;
 printf(" ZR.* ") >>f; printf("FROM %s AS ZR, TIS.ZObject AS ZO\n",NAME) >>f;
 printf("\tWHERE ZR.ZTOV < 0 AND ZO.ZUID = SAM.get_user() AND\n") >>f;
 printf("\t ZR.ZOID = ZO.ZOID AND ZR.ZVER = ZO.ZVER AND\n") >>f;
 printf("\t ZR.ZLVL <= SAM.get_user_slevel() -- MAC - check secrecy \n") >>f;
 printf("\t AND\n") >>f;
 printf("\t -- check pemissions for reading records at scope\n") >>f;
 printf("\t exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG\n") >>f;
 printf("\t   WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND\n") >>f;
 printf("\t     XAS.sid = ZR.ZSID AND XAS.obj_type = '%s.%s'\n",SUBSYS,OBJECT) >>f;
 printf("\t     and XAS.reada = 'Y' )\n") >>f;
 printf("\t -- check rejections for reading records at scope\n") >>f;
 printf("\t AND NOT\n") >>f;
 printf("\t exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG\n") >>f;
 printf("\t   WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND\n") >>f;
 printf("\t     XAS.sid = ZR.ZSID AND XAS.obj_type = '%s.%s'\n",SUBSYS,OBJECT) >>f;
 printf("\t     and XAS.reada = 'R' );\n") >>f;
 printf("\n") >>f;

 printf("-- VH_ - historic data in table\n") >>f;
 printf("DROP VIEW IF EXISTS %s.VH_%s CASCADE;\n",SUBSYS,SHORT_NAME) >>f;
 printf("CREATE VIEW %s.VH_%s AS SELECT",SUBSYS,SHORT_NAME) >>f;
 printf(" * ") >>f; printf("FROM %s AS ZR\n",NAME) >>f;
 printf("\tWHERE ZR.ZTOV >= 0 AND\n") >>f;
 printf("\t ZR.ZLVL <= SAM.get_user_slevel() -- MAC - check secrecy \n") >>f;
 printf("\t AND\n") >>f;
 printf("\t -- check pemissions for reading records at scope\n") >>f;
 printf("\t exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG\n") >>f;
 printf("\t   WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND\n") >>f;
 printf("\t     XAS.sid = ZR.ZSID AND XAS.obj_type = '%s.%s'\n",SUBSYS,OBJECT) >>f;
 printf("\t     and XAS.historya = 'Y' )\n") >>f;
 printf("\t -- check rejections for reading records at scope\n") >>f;
 printf("\t AND NOT\n") >>f;
 printf("\t exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG\n") >>f;
 printf("\t   WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND\n") >>f;
 printf("\t     XAS.sid = ZR.ZSID AND XAS.obj_type = '%s.%s'\n",SUBSYS,OBJECT) >>f;
 printf("\t     and XAS.historya = 'R' );\n") >>f;
 printf("\n") >>f;

 printf("-- VD_ - slice of data in table which was actual at point of time\n") >>f;
 printf("-- requested via set_vdate() call\n") >>f;
 printf("DROP VIEW IF EXISTS %s.VD_%s CASCADE;\n",SUBSYS,SHORT_NAME) >>f;
 printf("CREATE VIEW %s.VD_%s AS SELECT",SUBSYS,SHORT_NAME) >>f;
 printf(" ZR.* ") >>f; printf("FROM %s AS ZR, TIS.ZObject AS ZO\n",NAME) >>f;
 printf("\tWHERE ZR.ZOID = ZO.ZOID AND ZR.ZVER <= ZO.ZVER AND\n") >>f;
 printf("\t(ZR.ZTOV = 0 OR (ZR.ZTOV > 0 AND ZR.ZTOV > ZO.ZVER )) AND\n") >>f;
 printf("\t ( -- check date range\n") >>f;
 printf("\t  ZO.ZDATE <= tis.get_vdate()\n") >>f;
 printf("\t  AND ( ZO.ZDATO IS NULL OR ZO.ZDATO > tis.get_vdate() )\n") >>f;
 printf("\t  )\n") >>f;
 printf("\t AND\n") >>f;
 printf("\t ZR.ZLVL <= SAM.get_user_slevel() -- MAC - check secrecy \n") >>f;
 printf("\t AND\n") >>f;
 printf("\t -- check pemissions for reading records at scope\n") >>f;
 printf("\t exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG\n") >>f;
 printf("\t   WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND\n") >>f;
 printf("\t     XAS.sid = ZR.ZSID AND XAS.obj_type = '%s.%s'\n",SUBSYS,OBJECT) >>f;
 printf("\t     and XAS.historya = 'Y' )\n") >>f;
 printf("\t -- check rejections for reading records at scope\n") >>f;
 printf("\t AND NOT\n") >>f;
 printf("\t exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG\n") >>f;
 printf("\t   WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND\n") >>f;
 printf("\t     XAS.sid = ZR.ZSID AND XAS.obj_type = '%s.%s'\n",SUBSYS,OBJECT) >>f;
 printf("\t     and XAS.historya = 'R' );\n") >>f;
} # /make_create_view(f)

# create GRANT/REVOKE for functions
function make_proc_grant(f)
{
 make_no_header(f);
 #printf("GRANT SELECT ON %s.V_%s to tis_users;\n",SUBSYS,SHORT_NAME) >>f;
 # create
 printf("REVOKE ALL ON ") >>f;
  create_proc_definition(f);
 printf(" FROM PUBLIC;\n") >>f;
 printf("GRANT EXECUTE ON ") >>f;
  create_proc_definition(f);
 printf(" TO tis_users;\n") >>f;
 # update
 printf("GRANT EXECUTE ON ") >>f;
  update_proc_definition(f);
 printf(" TO tis_users;\n") >>f;
 # delete
 printf("REVOKE EXECUTE ON ") >>f;
  delete_proc_definition(f);
 printf(" FROM PUBLIC;\n") >>f;
 printf("GRANT EXECUTE ON ") >>f;
  delete_proc_definition(f);
 printf(" TO tis_users;\n") >>f;
 # QC
 printf("REVOKE ALL ON FUNCTION %s.QC_%s(\n",SUBSYS,CODE) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_r\t%s\n",NAME) >>f;
 printf("\t) FROM PUBLIC;\n") >>f;
 #printf("GRANT EXECUTE ON FUNCTION %s.QC_%s(\n",SUBSYS,CODE) >>f;
 #printf("\tv_as\tSAM.auditstate,\n") >>f;
 #printf("\tv_r\t%s\n",NAME) >>f;
 #printf("\t) TO tis_users;\n") >>f;
 # commit
 printf("REVOKE ALL ON FUNCTION %s.commit_%s(\n",SUBSYS,CODE) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_ZO\tTIS.ZObject\n") >>f;
 printf("\t) FROM PUBLIC;\n") >>f;
 #printf("GRANT EXECUTE ON FUNCTION %s.commit_%s(\n",SUBSYS,CODE) >>f;
 #printf("\tv_as\tSAM.auditstate,\n") >>f;
 #printf("\tv_ZO\tTIS.ZObject\n") >>f;
 #printf("\t) TO tis_users;\n") >>f;
 # rollback
 printf("REVOKE ALL ON FUNCTION %s.rollback_%s(\n",SUBSYS,CODE) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_ZO\tTIS.ZObject\n") >>f;
 printf("\t) FROM PUBLIC;\n") >>f;
 # move
 printf("REVOKE ALL ON FUNCTION %s.move_%s(\n",SUBSYS,CODE) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_ZO\tTIS.ZObject\n") >>f;
 printf("\t) FROM PUBLIC;\n") >>f;
 # delete_object_
 printf("REVOKE ALL ON FUNCTION %s.delete_object_%s(\n",SUBSYS,CODE) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_ZO\tTIS.ZObject\n") >>f;
 printf("\t) FROM PUBLIC;\n") >>f;
 #
 printf("REVOKE ALL ON FUNCTION %s.cmp_%s(\n",SUBSYS,CODE) >>f;
 printf("\tv_r\t%s,\n",NAME) >>f;
 printf("\tv_ro\t%s\n",NAME) >>f;
 printf("\t) FROM PUBLIC;\n") >>f;
 #
 printf("REVOKE ALL ON FUNCTION %s.rdelete_%s(\n",SUBSYS,CODE) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_ZO\tTIS.ZObject,\n") >>f;
 printf("\tv_ZPID\tbigint\n") >>f;
 printf("\t) FROM PUBLIC;\n") >>f;
} #make_proc_grant

# create DROP for functions
function make_proc_drop(f)
{
 make_no_header(f);
 #printf("GRANT SELECT ON %s.V_%s to tis_users;\n",SUBSYS,SHORT_NAME) >>f;
 # create
 printf("DROP ") >>f;
  create_proc_definition(f);
 printf(" CASCADE;\n") >>f;
 # update
 printf("DROP ") >>f;
  update_proc_definition(f);
 printf(" CASCADE;\n") >>f;
 # delete
 printf("DROP ") >>f;
  delete_proc_definition(f);
 printf(" CASCADE;\n") >>f;
 # QC
 printf("DROP FUNCTION IF EXISTS %s.QC_%s(\n",SUBSYS,CODE) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_r\t%s\n",NAME) >>f;
 printf("\t) CASCADE;\n") >>f;
 # commit
 printf("DROP FUNCTION IF EXISTS %s.commit_%s(\n",SUBSYS,CODE) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_ZO\tTIS.ZObject\n") >>f;
 printf("\t) CASCADE;\n") >>f;
 # rollback
 printf("DROP FUNCTION IF EXISTS %s.rollback_%s(\n",SUBSYS,CODE) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_ZO\tTIS.ZObject\n") >>f;
 printf("\t) CASCADE;\n") >>f;
 # move
 printf("DROP FUNCTION IF EXISTS %s.move_%s(\n",SUBSYS,CODE) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_ZO\tTIS.ZObject\n") >>f;
 printf("\t) CASCADE;\n") >>f;
 # delete_object_
 printf("DROP FUNCTION IF EXISTS %s.delete_object_%s(\n",SUBSYS,CODE) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_ZO\tTIS.ZObject\n") >>f;
 printf("\t) CASCADE;\n") >>f;
 #
 printf("DROP FUNCTION IF EXISTS %s.cmp_%s(\n",SUBSYS,CODE) >>f;
 printf("\tv_r\t%s,\n",NAME) >>f;
 printf("\tv_ro\t%s\n",NAME) >>f;
 printf("\t) CASCADE;\n") >>f;
 #
 printf("DROP FUNCTION IF EXISTS %s.rdelete_%s(\n",SUBSYS,CODE) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_ZO\tTIS.ZObject,\n") >>f;
 printf("\tv_ZPID\tbigint\n") >>f;
 printf("\t) CASCADE;\n") >>f;
} #make_proc_grant

# create sequence <<NAME>>_seq
function make_create_sequence(f)
{
 make_header(f);
 printf("--DROP SEQUENCE IF EXISTS %s_seq;\n",NAME) >>f;
 printf("--CREATE SEQUENCE %s_seq;\n",NAME) >>f;
}

# create_<<NAME>>

# update_<<NAME>>


# QC_<<NAME>>


# assistance

function make_copy_v2r(shift,target_action,f){
 target_action=toupper(substr(target_action,1,1));
 if(target_action == "D"){ printf(" -- nothing to do\n") >>f; return;}
 if(target_action != "C" && target_action != "U")
  do_abort("Invalid target_action for make_copy_v2r():" target_action);
 for(i=1;i<=FIELDS_COUNT;i++)
 {
   if(f_NOEDIT[i]){
    printf(" -- v_r.%s := v_%s; -- std editing unallowed\n",f_name[i],f_name[i]) >>f;
    if( target_action == "C" ) printf(" v_r.%s := %s; -- std editing unallowed\n",
       f_name[i],f_NOEDIT_DEFAULT[i]) >>f; }
   else
    printf(" v_r.%s := v_%s;\n",f_name[i],f_name[i]) >>f;
 }
} #/make_copy_v2r

function make_enum_fields(prefix,target_action,f){
 target_action=toupper(substr(target_action,1,1));
 if(target_action != "C" && target_action != "U")
  do_abort("Invalid target_action for make_copy_v2r():" target_action);
 tmp_s="" prefix "ZTOV";
 for(i=1;i<=FIELDS_COUNT;i++)
 {
  if(tmp_s != "") tmp_s=tmp_s ",";
  if(!(i-int(i/6)*6)) tmp_s=tmp_s "\n\t";
  tmp_s = tmp_s prefix f_name[i];
 }
   printf("%s",tmp_s) >>f;
} #/make_enum_fields

function create_proc_definition(f){
 make_proc_definition("create",f)a;
} #/create_proc_definition
function update_proc_definition(f){
 make_proc_definition("update",f)a;
} #/update_proc_definition
function delete_proc_definition(f){
 make_proc_definition("delete",f)a;
} #/delete_proc_definition

function make_proc_definition(proc,f){
 if(!is_value_in(proc,"create","update","delete"))
  do_abort("make_proc_definition for '" proc "' not implemented");
 printf("FUNCTION %s.%s_%s(\n",SUBSYS,proc,SHORT_NAME) >>f;
 if(HEADER == "STD_HEADER"){
  printf("\tv_ZOID bigint,\n") >>f;
  printf("\tv_ZVER bigint,\n") >>f;
  if(proc == "create")
   printf("\tv_ZPID bigint, -- ZRID of parent record or null\n") >>f;
  else{
   printf("\tv_ZRID bigint") >>f;
   if(proc!="delete") printf(",") >>f;
   printf(" -- ZRID of target record\n") >>f;
   } #/else if(proc == "create")
 }else if(HEADER == "STD_HEANOR"){
  if(proc == "create"){
   printf("\tv_ZSID bigint,\n") >>f;
   printf("\tv_ZPID bigint,\n") >>f;
   printf("\tv_ZLVL smallint,") >>f;
  } else {
   printf("\tv_ZRID bigint,\n") >>f;
   printf("\tv_ZVER bigint") >>f;
   if(proc!="delete") printf(",\n") >>f;
   else printf("\n") >>f;
  } #/ else if(proc == "create"){
 } else { do_abort("HEADER=" HEADER " unimplemented");}
 if(proc != "delete"){
 for(i=1;i<=FIELDS_COUNT;i++)
  {
   tmp_s=","; if(i==FIELDS_COUNT) tmp_s="";
   if(!f_NOEDIT[i]) # yes I know, it's wrong for f_NOEDIT[FIELDS_COUNT]
   printf("\tv_%s\t%s%s\n",f_name[i],f_type[i],tmp_s) >>f;
  } #/for
 } #/if(proc != "delete"){
 printf(")") >>f;
} #/make_proc_definition

function make_return_notimplemented(f){
 printf("DECLARE\n") >>f;
 printf(" v_es SAM.execstatus;\n") >>f;
 printf("BEGIN\n") >>f;
 printf(" v_es.errnum :=1; v_es.errcode :='E_NOTIMPLEMENTED';\n") >>f;
 printf(" v_es.errdesc :=v_es.errcode; RETURN v_es;\n") >>f;
 printf("END $$;\n") >>f;
}

# create_<<NAME>>

function make_create_proc_decl(f){
 make_header(f);
 printf("CREATE OR REPLACE ") >>f;
  create_proc_definition(f);
 printf(" RETURNS SAM.execstatus VOLATILE\n") >>f;
 printf("  LANGUAGE plpgSQL SECURITY DEFINER as $$\n") >>f;
 make_return_notimplemented(f);
} #/make_create_proc_decl

function make_create_proc(f){ make_proc("create",f); }
function make_update_proc(f){ make_proc("update",f); }
function make_delete_proc(f){ make_proc("delete",f); }

#
# universal create/update/delete code generator
# make_proc("create|update|delete",<filename>)
#

function make_proc(proc, f){
 if(HEADER != "STD_HEADER"){ do_abort("HEADER=" HEADER " unimplemented");}
 if(!is_value_in(proc,"create","update","delete"))
  do_abort("make_proc for '" proc "' not implemented");
 make_header(f);
 printf("BEGIN TRANSACTION;\n") >>f;
 printf("CREATE OR REPLACE ") >>f;
  make_proc_definition(proc,f);
 printf(" RETURNS SAM.execstatus VOLATILE\n") >>f;
 printf("  LANGUAGE plpgSQL SECURITY DEFINER as $$\n") >>f;
 printf("DECLARE\n") >>f;
 printf(" v_ps SAM.procstate;\n") >>f;
 printf(" v_as SAM.auditstate;\n") >>f;
 printf(" v_es SAM.execstatus;\n") >>f;
 printf(" v_ZO TIS.ZObject%%ROWTYPE;\n") >>f;
 printf(" v_r %s%%ROWTYPE; -- this version\n",NAME) >>f;
 if(proc != "create") {
  printf(" v_ro %s%%ROWTYPE; -- previous version\n",NAME) >>f;
  printf(" v_ri %s%%ROWTYPE; -- intermediate version\n",NAME) >>f;
 }
 printf(" v_user bigint;\n") >>f;
 if(proc == "create" && MAXREC != "")
  printf(" v_recount bigint; -- records count(*) for MAXREC\n") >>f;
 printf(" ZC_ZTOV_UPDATED CONSTANT bigint := -1;\n") >>f;
 printf(" ZC_ZTOV_DELETED CONSTANT bigint := -2;\n") >>f;
 printf(" ZC_ZTOV_NOCHANGES CONSTANT bigint := -3;\n") >>f;
 printf(" ZC_ZTOV_RDELETED CONSTANT bigint := -4;\n") >>f;
 printf("BEGIN\n") >>f;
 printf(" -- 0) enter into procedure\n") >>f;
 printf(" v_as.subsys := '%s';\n",SUBSYS) >>f;
 printf(" v_as.eaction :='%s';\n",toupper(substr(proc,1,1))) >>f;
 printf(" v_as.obj_type :='%s';\n",CODE) >>f;
 if(proc == "create")
  printf(" -- v_as.ZID := null; -- not ready yet\n") >>f;
 else
  printf(" v_as.ZID := v_ZRID;\n") >>f;
 printf(" v_as.ZTYPE := '%s.%s';\n",SUBSYS,OBJECT) >>f;
 printf(" v_as.ZOID := v_ZOID;\n") >>f;
 printf(" v_as.ZVER := v_ZVER;\n") >>f;
 printf(" v_as.proc := '%s_%s';\n",proc,SHORT_NAME) >>f;
 printf(" -- 0.1) get target ZO record and extract missings from\n") >>f;
 printf(" SELECT * INTO v_ZO FROM TIS.ZObject as ZO WHERE\n") >>f;
 printf("   ZO.ZOID = v_ZOID AND ZO.ZVER = v_ZVER AND ZO.ZTYPE = '%s.%s'\n",SUBSYS,OBJECT) >>f;
 printf("   FOR SHARE; -- lock row from concurrent updates \n") >>f;
 printf(" v_as.ZLVL := v_ZO.ZLVL; -- FOUND or NOT - indiscriminately\n") >>f; 
 printf(" v_as.sid := v_ZO.ZSID;\n") >>f; 
 printf(" v_as := SAM.do_auditlog_enter(v_as);\n") >>f;
 printf(" -- 0.2) copy parameters to v_r\n") >>f;
 printf(" v_r.ZOID = v_ZOID;\n") >>f;
 printf(" v_r.ZVER = v_ZVER;\n") >>f;
 if(proc == "create")
  printf(" v_r.ZPID = v_ZPID; -- revise at 4.2\n") >>f;
 else
  printf(" v_r.ZRID = v_ZRID;\n") >>f;
 make_copy_v2r("",proc,f);
 printf("<<try>>\n") >>f;
 printf("BEGIN\n") >>f;
 printf(" -- 1) pre access control checks\n") >>f;
 printf(" -- 1.1) check isolation level\n") >>f;
 printf(" IF NOT SAM.check_isolation() THEN\n") >>f;
 printf("    v_es := SAM.make_es_transisolation(null,null); EXIT try;\n") >>f;
 printf(" END IF;\n") >>f;
 printf(" -- 1.2) identify user\n") >>f;
 printf(" IF SAM.get_user() IS NULL THEN\n") >>f;
 printf("    v_es := sam.make_execstatus(null,null,'E_NOUSER',session_user); EXIT try;\n") >>f;
 printf(" END IF;\n") >>f;
 printf(" -- 1.3) unallowed nulls\n") >>f;
 printf(" IF v_r.ZOID IS NULL THEN v_es := SAM.make_es_notnull(null,null,\n") >>f;
 printf("\t'%s_%s','ZOID'); EXIT try; END IF;\n",proc,SHORT_NAME) >>f;
 printf(" IF v_r.ZVER IS NULL THEN v_es := SAM.make_es_notnull(null,null,\n") >>f;
 printf("\t'%s_%s','ZVER'); EXIT try; END IF;\n",proc,SHORT_NAME) >>f;
 if(PARENT != "" && proc == "create") {
  printf(" IF v_r.ZPID IS NULL THEN v_es := SAM.make_es_notnull(null,null,\n") >>f;
  printf("\t'%s_%s','ZPID'); EXIT try; END IF;\n",proc,SHORT_NAME) >>f;
 } else if (PARENT == "" && proc == "create"){
  printf(" IF NOT v_r.ZPID IS NULL THEN v_es := SAM.make_execstatus(null,null,\n") >>f;
  printf("\t'E_INVALIDVALUE','<NOT NULL>','%s_%s.ZPID'); EXIT try; END IF;\n",proc,SHORT_NAME) >>f;
 } #/if(PARENT != "" && proc == "create"))

 printf(" -- 1.4) check object's verion (ZO record)\n") >>f;
 printf(" IF v_ZO.ZOID IS NULL THEN -- aka NOT FOUND after (0.1)\n") >>f;
 printf("   v_es := SAM.make_execstatus(null,null,'E_INVALIDVERSION',\n") >>f;
 printf("   '%s.%s',CAST(v_ZOID AS text),CAST(v_ZVER AS text)); EXIT try; END IF;\n",SUBSYS,OBJECT) >>f;
 printf(" -- 1.5) check ZSTA\n") >>f;
 printf(" IF v_ZO.ZSTA <> 'I' THEN\n") >>f;
 printf("  v_es := SAM.make_execstatus(null,null,'E_NOTOPENED',\n") >>f;
 printf("  '(%s,'||v_r.ZOID||','||v_r.ZVER||')'); EXIT try;\n",OBJECT_NAME) >>f;
 printf(" END IF;\n") >>f;
 printf(" -- 1.6) check ZUID\n") >>f;
 printf(" IF v_ZO.ZUID <> SAM.get_user() THEN\n") >>f;
 printf("  v_es := SAM.make_execstatus(null,null,'E_NOTOWNER',\n") >>f;
 printf("  '(%s,'||v_r.ZOID||','||v_r.ZVER||')'); EXIT try;\n",OBJECT_NAME) >>f;
 printf(" END IF;\n") >>f;
 printf(" -- 2) check access\n") >>f;
 printf(" IF v_ZO.ZVER = 1 THEN \n") >>f;
 printf("  v_ps := SAM.check_access_create(v_as,v_ZO.ZSID,v_ZO.ZTYPE,v_ZO.ZLVL);\n") >>f;
 printf(" ELSE\n") >>f;
 printf("  v_ps := SAM.check_access_write(v_as,v_ZO.ZSID,v_ZO.ZTYPE,v_ZO.ZOID,v_ZO.ZLVL);\n") >>f;
 printf(" END IF;\n") >>f;
 printf(" v_as := v_ps.a;\n") >>f;

 printf(" -- 3) lock required tables\n") >>f;
 if(has_table_uniq){
  printf(" LOCK TABLE %s IN EXCLUSIVE MODE;\n",NAME) >>f;
 }else{
  printf(" -- LOCK TABLE %s NOT REQUIRED;\n",NAME) >>f;
 }
 printf(" -- 4) check data\n") >>f;
 printf(" -- 4.1) get requested record version\n") >>f;
 if(proc == "create") {
  printf("  -- no record yet\n") >>f;
 } else {
  printf(" -- 4.1.1) get intermediate version\n") >>f;
  printf(" SELECT * INTO v_ri FROM %s ZR WHERE ZR.ZOID = v_r.ZOID AND ZR.ZRID = v_r.ZRID\n",NAME) >>f;
  printf("   AND ZR.ZVER = v_r.ZVER AND ZR.ZTOV < 0 FOR UPDATE;\n") >>f;
  printf(" -- 4.1.2) get current version \n") >>f;
  if( proc == "delete" )  printf(" IF NOT FOUND THEN -- if no intermediate one\n") >>f;
  printf("  SELECT * INTO v_ro FROM %s ZR WHERE ZR.ZOID = v_r.ZOID\n",NAME) >>f;
  printf("  AND ZR.ZRID = v_r.ZRID AND ZR.ZTOV = 0 FOR SHARE;\n") >>f;
  if( proc == "delete" ) printf(" END IF;\n") >>f;
  printf(" -- abort if no record\n") >>f;
  printf(" IF COALESCE(v_ri.ZOID, v_ro.ZOID) IS NULL OR\n") >>f;
  printf("  v_ri.ZTOV IN (ZC_ZTOV_DELETED, ZC_ZTOV_RDELETED) THEN\n") >>f;
  printf("    v_es := SAM.make_execstatus(null,null,'E_NORECORD',\n") >>f;
  printf("    '%s','ZRID',''||v_r.ZRID); EXIT try;\n",NAME) >>f;
  printf(" END IF;\n") >>f;
 }#/ else if(proc == "create")
 printf(" -- 4.2) set record header\n") >>f;
 printf(" v_r.ZSID := v_ZO.ZSID;\n") >>f;
 printf(" v_r.ZLVL := v_ZO.ZLVL;\n") >>f;
 if(proc == "create")
  printf(" v_r.ZPID := COALESCE(v_r.ZPID,0); -- NOT NULL, using explicit 0 for 'no parent'\n") >>f;
 else
  printf(" v_r.ZPID := v_ro.ZPID;\n") >>f;
 if(proc == "delete") {
  printf(" v_r.ZTOV := ZC_ZTOV_DELETED;\n",NAME) >>f;
 } else {
  printf(" v_r.ZTOV := ZC_ZTOV_UPDATED;\n",NAME) >>f;
 }
 if( MAXREC != "" && proc == "create" ) {
  printf(" -- 4.98) let's check MAXRECords before creating new one\n") >>f;
  printf(" SELECT count(*) INTO v_recount FROM %s ZR WHERE ZR.ZOID = v_r.ZOID\n",NAME) >>f;
  printf("   AND ( ZR.ZTOV IN (ZC_ZTOV_UPDATED, ZC_ZTOV_NOCHANGES) OR ( ZR.ZTOV = 0 AND\n") >>f;
  printf("     NOT EXISTS (SELECT ZRID FROM %s ZR2 WHERE\n",NAME) >>f;
  printf("       ZR2.ZOID = ZR.ZOID AND ZR2.ZRID = ZR.ZRID AND ZR2.ZVER = v_ZO.ZVER) ) );\n") >>f;
  printf(" IF v_recount >= %s THEN\n",MAXREC) >>f;
  printf("  v_es := SAM.make_execstatus(null,null,'E_2MANYRECORDS',\n") >>f;
  printf("  '%s','%s','%s'); EXIT try;\n",NAME,OBJECT_NAME,MAXREC) >>f;
  printf(" END IF;\n") >>f;
 }
 printf(" -- 4.99) Quality Control\n") >>f;
 if(proc != "delete"){
  printf(" v_ps := %s.QC_%s(v_as,v_r); v_as := v_ps.a;\n",SUBSYS,CODE) >>f;
  printf("   IF NOT v_ps.success THEN v_es := v_ps.e; EXIT try; END IF;\n") >>f;
 }else{
  printf("  -- not applicable\n") >>f;
 } #/if(proc != "delete")
 printf(" -- 5) add or update record\n") >>f;
 if(proc == "create") {
  printf(" v_r.ZRID := TIS.next_QRID(v_r.ZOID,v_r.ZVER);\n") >>f;
 }
 if(proc == "create") {
  printf(" INSERT INTO %s values(v_r.*);\n",NAME) >>f;
 }
 else if (proc == "update") {
  printf(" IF v_ri.ZVER IS NULL THEN\n") >>f;
  printf("  IF NOT %s.cmp_%s(v_r,v_ro) THEN\n",SUBSYS,CODE) >>f;
  printf("   INSERT INTO %s values(v_r.*);\n",NAME) >>f;
  printf("  END IF;\n") >>f;
  printf(" ELSE\n") >>f;
  printf("  IF %s.cmp_%s(v_r,v_ro) THEN v_r.ZTOV = ZC_ZTOV_NOCHANGES; END IF;\n",SUBSYS,CODE) >>f;
  printf("  UPDATE %s AS ZR SET (",NAME) >>f;
   make_enum_fields("","U",f);
   printf(")\n") >>f;
  printf("   = (") >>f;
   make_enum_fields("v_r.","U",f);
   printf(")\n") >>f;
   printf("   WHERE ZR.ZOID = v_r.ZOID AND ZR.ZVER = v_r.ZVER AND ZR.ZRID = v_r.ZRID;\n") >>f;
  printf(" END IF;\n") >>f;
 }
 else { # proc == "delete"
  if(CHILD_COUNT>=1)
  {
  printf(" BEGIN\n") >>f;
  for(i=1;i<=CHILD_COUNT;i++)
  {
   printf("   -- delete %s children of target row\n",CHILD_CODE[i]) >>f;
   printf("   v_ps := %s.rdelete_%s(v_as,v_ZO,v_r.ZRID);\n",CHILD_SUBSYS[i],CHILD_CODE[i]) >>f;
   printf("   v_as := v_ps.a;\n") >>f;
   printf("   IF NOT v_ps.success THEN\n") >>f;
   printf("    v_es := v_ps.e;\n") >>f;
   printf("    RAISE EXCEPTION '%s.%s_%s rolled back';\n",SUBSYS,proc,NAME) >>f;
   printf("   END IF;\n") >>f;
  }
  printf(" EXCEPTION\n") >>f;
  printf("  WHEN RAISE_EXCEPTION THEN EXIT try; -- subtransaction rolled back\n") >>f;
  printf(" END;\n") >>f;
  printf(" IF NOT v_ps.success THEN EXIT try; END IF;\n") >>f;
  } #/if(CHILD_COUNT>=1)
  printf(" IF v_ri.ZVER IS NULL THEN\n") >>f;
  printf("  v_ro.ZVER := v_r.ZVER;\n",NAME) >>f;
  printf("  v_ro.ZTOV := ZC_ZTOV_DELETED;\n",NAME) >>f;
  printf("  INSERT INTO %s values(v_ro.*);\n",NAME) >>f;
  printf(" ELSE\n") >>f;
  printf("  UPDATE %s AS ZR SET ZTOV = ZC_ZTOV_DELETED WHERE\n",NAME) >>f;
  printf("    ZR.ZOID = v_r.ZOID AND ZR.ZVER = v_r.ZVER AND ZR.ZRID = v_r.ZRID;\n") >>f;
  printf(" END IF;\n") >>f;
 }
 printf(" v_as := SAM.do_auditlog_da_sole(v_as,'2',null,null,v_r.ZSID,v_r.ZRID);\n") >>f;
 printf(" -- 6) exit\n") >>f;
 printf(" v_es := sam.make_execstatus(v_r.ZRID,v_r.ZVER,'I_SUCCESS');\n") >>f;
 printf("EXCEPTION\n") >>f;
 printf(" WHEN OTHERS THEN\n") >>f;
 printf("\tv_es:=sam.make_execstatus(v_r.ZRID,v_r.ZVER,'E_SQL',SQLSTATE,SQLERRM);\n") >>f;
 printf("END;\n") >>f;
 printf("-- FINALLY:\n") >>f;
 printf(" PERFORM SAM.do_auditlog_exit(v_as,v_es);\n") >>f;
 printf(" RETURN v_es;\n") >>f;
 printf("END $$;\n") >>f;
# printf("REVOKE ALL ON ") >>f;
#  create_proc_definition(f);
# printf(" FROM PUBLIC;\n") >>f;
# printf("GRANT EXECUTE ON ") >>f;
#  create_proc_definition(f);
# printf(" TO tis_users;\n") >>f;
 printf("COMMIT TRANSACTION;\n") >>f;
} # /create_proc

# update_<<NAME>>

function make_update_proc_decl(f){
 make_header(f);
 printf("CREATE OR REPLACE ") >>f;
  update_proc_definition(f);
 printf(" RETURNS SAM.execstatus VOLATILE\n") >>f;
 printf("  LANGUAGE plpgSQL SECURITY DEFINER as $$\n") >>f;
 make_return_notimplemented(f);
} #/make_update_proc_decl


#
# delete_<<NAME>>

function make_delete_proc_decl(f){
 make_header(f);
 printf("CREATE OR REPLACE ") >>f;
  delete_proc_definition(f);
 printf(" RETURNS SAM.execstatus VOLATILE\n") >>f;
 printf("  LANGUAGE plpgSQL SECURITY DEFINER as $$\n") >>f;
 make_return_notimplemented(f);
} #/make_delete_proc_decl


#
# make_QC

function make_QC_proc_decl(f){
 make_header(f);
 printf("CREATE OR REPLACE FUNCTION %s.QC_%s(\n",SUBSYS,CODE) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_r\t%s\n",NAME) >>f;
 printf(") RETURNS SAM.procstate VOLATILE\n") >>f;
 printf("  LANGUAGE plpgSQL SECURITY DEFINER as $$\n") >>f;
 printf("DECLARE\n") >>f;
 printf(" v_ps SAM.procstate;\n") >>f;
 printf(" v_es SAM.execstatus;\n") >>f;
 printf("BEGIN\n") >>f;
 printf(" v_es.errnum :=1; v_es.errcode :='E_NOTIMPLEMENTED';\n") >>f;
 printf(" v_es.errdesc := v_es.errcode || ',QC_%s()'; v_ps.e := v_es;\n",SHORT_NAME) >>f;
 printf(" v_ps.a := v_as; v_ps.success := FALSE; RETURN v_ps;\n") >>f;
 printf("END $$;\n") >>f;
} #/make_QC_proc_decl


function make_QC_proc(f){
 make_header(f);
 printf("BEGIN TRANSACTION;\n") >>f;
 printf("CREATE OR REPLACE FUNCTION %s.QC_%s(\n",SUBSYS,CODE) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_r\t%s\n",NAME) >>f;
 printf(") RETURNS SAM.procstate VOLATILE\n") >>f;
 printf("  LANGUAGE plpgSQL SECURITY DEFINER as $$\n") >>f;
 printf("DECLARE\n") >>f;
 printf(" v_ps SAM.procstate;\n") >>f;
 printf(" i\tinteger;\n") >>f;
 printf(" ZC_ZTOV_UPDATED CONSTANT bigint := -1;\n") >>f;
 printf(" ZC_ZTOV_DELETED CONSTANT bigint := -2;\n") >>f;
 printf(" ZC_ZTOV_NOCHANGES CONSTANT bigint := -3;\n") >>f;
 printf(" ZC_ZTOV_RDELETED CONSTANT bigint := -4;\n") >>f;
 printf("BEGIN\n") >>f;
 printf(" v_ps.success := FALSE;\n") >>f;
 printf("<<try>>\n") >>f;
 printf("BEGIN\n") >>f;
 printf(" -- 1) check NOT NULLs\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 if(!f_NN[i]) continue;
 printf(" IF v_r.%s IS NULL THEN v_ps.e := SAM.make_es_notnull(v_r.ZRID,v_r.ZVER,\n",f_name[i]) >>f;
 printf("\t'%s','%s'); EXIT try; END IF;\n",NAME,f_name[i]) >>f;
 }
 printf(" -- 2) check dictionary fields\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 if(f_DIC[i] == "") continue;
 tmp_s = ""; if(!f_NN[i]) tmp_s = "_nula";
 printf(" IF NOT dic.check_code%s('%s',v_r.%s) THEN\n",tmp_s,f_DIC[i],f_name[i]) >>f;
 printf("   v_ps.e := SAM.make_es_invalidcode(v_r.ZRID,v_r.ZVER,v_r.%s,\n",f_name[i]) >>f;
 printf("   '%s.%s','%s'); EXIT try; END IF;\n",NAME,f_name[i],f_DIC[i]) >>f;
 }
 for(i=1;i<=FIELDS_COUNT;i++) {
 if(!f_SLEVEL[i]) continue;
 printf(" IF NOT dic.check_code('SLEVEL',CAST(v_r.%s as text)) THEN\n",f_name[i]) >>f;
 printf("   v_ps.e := SAM.make_es_invalidcode(v_r.ZRID,v_r.ZVER,CAST(v_r.%s as text),\n",f_name[i]) >>f;
 printf("   '%s.%s','SLEVEL'); EXIT try; END IF;\n",NAME,f_name[i]) >>f;
 }
 printf(" -- 3) check uniqueness\n") >>f;
 if(ZNAME != ""){
 printf("  IF TIS.check_ZNAME('%s',v_r.ZOID,v_r.%s) THEN\n",OBJECT,ZNAME) >>f;
 printf("  v_ps.e := SAM.make_execstatus(null,null,'E_DUPRECORD',\n") >>f;
 printf("  '%s','ZNAME',v_r.%s); EXIT try;\n",NAME,ZNAME) >>f;
 printf("  END IF;\n") >>f;
 }
 for(i=1;i<=FIELDS_COUNT;i++) {
 if(!f_UNIQ[i]) continue;
 printf(" SELECT COUNT(*) INTO i FROM %s ZR WHERE %s = v_r.%s AND\n",NAME,f_name[i],f_name[i]) >>f;
 if (f_UNIQ_REALM[i] == "DB" ) # DB-wide uniqueness
 {
  printf("\t ZTOV = 0 AND (ZRID <> v_r.ZRID OR v_r.ZRID IS NULL)\n") >>f;
  printf("\t  AND (ZOID <> v_r.ZOID OR v_r.ZOID IS NULL);\n") >>f;
 }
 else if (f_UNIQ_REALM[i] == "SCOPE" ) # scope-wide uniqueness
 {
  printf("\t ZTOV = 0 AND ZSID = v_r.ZSID AND (ZRID <> v_r.ZRID OR v_r.ZRID IS NULL)\n") >>f;
  printf("\t  AND (ZOID <> v_r.ZOID OR v_r.ZOID IS NULL);\n") >>f;
 }
 else if (f_UNIQ_REALM[i] == "OBJECT" ) # object-wide uniqueness
 {
  printf("\t ZTOV <= 0 AND ZOID = v_r.ZOID AND (ZRID <> v_r.ZRID OR v_r.ZRID IS NULL)\n") >>f;
  printf("\t  AND NOT EXISTS ( SELECT * FROM %s ZR2 WHERE ZR2.ZOID = ZR.ZOID\n",NAME) >>f;
  printf("\t   AND ZR.ZRID = ZR2.ZRID AND ( (ZR2.ZTOV IN (ZC_ZTOV_DELETED,ZC_ZTOV_RDELETED))\n") >>f;
  printf("\t                                OR (ZR.ZTOV = 0 AND ZR2.ZTOV < 0) ) );\n") >>f;
 }
 else do_abort("unknown realm of uniqueness '" f_UNIQ_REALM[i] "' at field '" f_name[i] "'");
 printf(" IF i <> 0 THEN\n") >>f;
 printf("  v_ps.e :=SAM.make_execstatus(v_r.ZRID,v_r.ZVER,'E_DUPRECORD','%s',\n",NAME) >>f;
 printf("  '%s',''||v_r.%s);\n",f_name[i],f_name[i]) >>f;
 printf("  EXIT try; END IF;\n") >>f;
 }
 printf(" -- 4) check references\n") >>f;
 printf(" -- 4.1) check std parent/child references\n") >>f;
 if(PARENT == "")
  printf("   -- not required\n") >>f;
 else{
 printf("  SELECT COUNT(*) INTO i FROM %s ZR WHERE ZR.ZOID = v_r.ZOID AND\n",PARENT) >>f;
 printf("   ZR.ZRID = v_r.ZPID AND ZR.ZTOV <= 0 AND NOT EXISTS (\n") >>f;
 printf("    SELECT * FROM %s ZR2 WHERE ZR2.ZOID = v_r.ZOID AND ZR2.ZRID = v_r.ZPID\n",PARENT) >>f;
 printf("    AND ZR2.ZTOV IN (ZC_ZTOV_DELETED,ZC_ZTOV_RDELETED) );\n") >>f;
 printf("  IF i = 0 THEN v_ps.e := SAM.make_execstatus(v_r.ZRID,v_r.ZVER,'E_NORECORD',\n") >>f;
 printf("    '%s','ZRID',CAST(v_r.ZPID AS text)); EXIT try; END IF;\n",PARENT) >>f;
 }
 printf(" -- 4.2) check scope references from data fields\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 if(!f_SID[i]) continue;
 printf("  IF v_r.%s <> 0 THEN\n",f_name[i]) >>f;
 printf("   SELECT COUNT(*) INTO i FROM SAM.Scope WHERE id = v_r.%s;\n",f_name[i]) >>f;
 printf("   IF i = 0 THEN v_ps.e := SAM.make_execstatus(v_r.ZRID,v_r.ZVER,'E_NOSCOPE',\n") >>f;
 printf("\tCAST(v_r.%s AS text)); EXIT try; END IF;\n",f_name[i]) >>f;
 printf("  END IF;\n") >>f;
 }
 printf(" -- 99) all tests passed\n") >>f;
 printf(" v_ps.success := TRUE;\n") >>f;
 printf("END;\n") >>f;
 printf("-- FINALLY:\n") >>f;
 printf(" v_ps.a := v_as;\n") >>f;
 printf(" RETURN v_ps;\n") >>f;
 printf("END $$;\n") >>f;
 printf("COMMIT TRANSACTION;\n") >>f;
} #/make_QC_proc

#
# commit record proc
#

function make_commit_rec_proc_decl(f){
 make_header(f);
 printf("CREATE OR REPLACE FUNCTION %s.commit_%s(\n",SUBSYS,CODE) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_ZO\tTIS.ZObject\n") >>f;
 printf(") RETURNS SAM.procstate VOLATILE\n") >>f;
 printf("  LANGUAGE plpgSQL SECURITY DEFINER as $$\n") >>f;
 printf("DECLARE\n") >>f;
 printf(" v_ps SAM.procstate;\n") >>f;
 printf(" v_es SAM.execstatus;\n") >>f;
 printf("BEGIN\n") >>f;
 printf(" v_es.errnum :=1; v_es.errcode :='E_NOTIMPLEMENTED';\n") >>f;
 printf(" v_es.errdesc := v_es.errcode || ',commit_%s()'; v_ps.e := v_es;\n",CODE) >>f;
 printf(" v_ps.a := v_as; v_ps.success := FALSE; RETURN v_ps;\n") >>f;
 printf("END $$;\n") >>f;
} #/make_commit_rec_proc_decl

function make_commit_rec_proc(f){
 make_header(f);
 printf("BEGIN TRANSACTION;\n") >>f;
 printf("CREATE OR REPLACE FUNCTION %s.commit_%s(\n",SUBSYS,CODE) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_ZO\tTIS.ZObject\n") >>f;
 printf(") RETURNS SAM.procstate VOLATILE\n") >>f;
 printf("  LANGUAGE plpgSQL SECURITY DEFINER as $$\n") >>f;
 printf("DECLARE\n") >>f;
 printf(" v_ps SAM.procstate;\n") >>f;
 printf(" v_r  %s%%ROWTYPE; -- for FQC\n",NAME) >>f;
 printf(" v_recount bigint;\n") >>f;
 if(ZNAME != ""){
 printf(" v_ZNAME varchar(255);\n") >>f;
 printf(" v_is_ZNAME boolean := false;\n") >>f;
 }
 printf(" ZC_ZTOV_UPDATED CONSTANT bigint := -1;\n") >>f;
 printf(" ZC_ZTOV_DELETED CONSTANT bigint := -2;\n") >>f;
 printf(" ZC_ZTOV_NOCHANGES CONSTANT bigint := -3;\n") >>f;
 printf(" ZC_ZTOV_RDELETED CONSTANT bigint := -4;\n") >>f;
 printf("BEGIN\n") >>f;
 printf(" v_ps.success := FALSE;\n") >>f;
 printf("<<try>>\n") >>f;
 printf("BEGIN\n") >>f;
 printf(" -- 1) make FQC (final QC)\n") >>f;
 printf(" -- 1.1) check NOT NULLs\n") >>f;
 printf("  -- not necessary\n") >>f;
 printf(" -- 1.2) check dictionary fields\n") >>f;
 printf("  -- may be in the future...\n") >>f;
 printf(" -- 1.3) check uniqueness\n") >>f;
 if(has_table_uniq) printf(" LOCK TABLE %s IN EXCLUSIVE MODE;\n",NAME) >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 if(!f_UNIQ[i]) continue;
 if( is_value_in(f_UNIQ_REALM[i],"DB","SCOPE") ) {
  printf(" SELECT * INTO v_r FROM %s ZR, %s ZR2 WHERE ZR.ZOID = v_ZO.ZOID AND\n",NAME, NAME) >>f;
  printf("   ZR.%s = ZR2.%s AND ZR2.ZTOV = 0 AND\n",f_name[i],f_name[i]) >>f;
  printf("   ( ZR.ZTOV IN (ZC_ZTOV_UPDATED,ZC_ZTOV_NOCHANGES)\n") >>f;
  printf("     OR ( ZR.ZTOV = 0 AND NOT EXISTS\n") >>f;
  printf("        (SELECT ZRID FROM %s ZR3 WHERE\n",NAME) >>f;
  printf("         ZR3.ZOID = ZR.ZOID AND ZR3.ZRID = ZR.ZRID AND ZR3.ZVER = v_ZO.ZVER)\n") >>f;
  printf("   ) ) AND\n") >>f;
  if (f_UNIQ_REALM[i] == "DB" ) # DB-wide uniqueness
   printf("   (ZR.ZRID <> ZR2.ZRID OR ZR.ZOID <> ZR2.ZOID);\n") >>f;
  else if (f_UNIQ_REALM[i] == "SCOPE" ) # scope-wide uniqueness
   printf("   (ZR.ZRID <> ZR2.ZRID OR ZR.ZOID <> ZR2.ZOID) AND ZR.ZSID = ZR2.ZSID;\n") >>f;
 } # /if( is_value_in(f_UNIQ_REALM[i],"DB","SCOPE" )
 else if (f_UNIQ_REALM[i] == "OBJECT" ) # object-wide uniqueness
 {
  printf(" SELECT * INTO v_r FROM %s ZR, %s ZR2 WHERE ZR.ZOID = v_ZO.ZOID AND\n",NAME, NAME) >>f;
  printf("   ZR.ZOID = ZR2.ZOID AND ZR.%s = ZR2.%s AND ZR.ZRID <> ZR2.ZRID AND \n",f_name[i],f_name[i]) >>f;
  printf("   ZR2.ZTOV IN (0,ZC_ZTOV_UPDATED,ZC_ZTOV_NOCHANGES) AND\n") >>f;
  printf("   ZR.ZTOV IN (0,ZC_ZTOV_UPDATED,ZC_ZTOV_NOCHANGES)  AND\n") >>f;
  printf("     NOT EXISTS (SELECT ZRID FROM %s ZR3 WHERE ZR3.ZOID = ZR.ZOID AND\n",NAME) >>f;
  printf("         ( (ZR3.ZRID = ZR.ZRID AND ZR3.ZTOV < 0 AND ZR.ZTOV = 0) OR\n") >>f;
  printf("           (ZR3.ZRID = ZR2.ZRID AND ZR3.ZTOV < 0 AND ZR2.ZTOV = 0)\n") >>f;
  printf("         ) );\n") >>f;
 } #/ else if (f_UNIQ_REALM[i] == "OBJECT" )
 else do_abort("unknown realm of uniqueness '" f_UNIQ_REALM[i] "' at field '" f_name[i] "'");
 printf(" IF FOUND THEN\n") >>f;
 printf("  v_ps.e :=SAM.make_execstatus(v_r.ZRID,v_r.ZVER,'E_DUPRECORD','%s',\n",NAME) >>f;
 printf("  '%s',''||v_r.%s);\n",f_name[i],f_name[i]) >>f;
 printf("  EXIT try; END IF;\n") >>f;
 }
 printf(" -- 1.4) check references\n") >>f;
 printf("  -- may be in the future...\n") >>f;
 printf(" -- 1.5) check MAXREC and MINREC\n") >>f;
 if( MAXREC == "" && MINREC == "")
  printf("  -- not applicable for %s\n",NAME) >>f;
 else{
  printf(" -- 1.5.1) get count(*) for active records in the object\n") >>f;
  printf(" SELECT count(*) INTO v_recount FROM %s ZR WHERE ZR.ZOID = v_ZO.ZOID\n",NAME) >>f;
  printf("   AND (ZR.ZTOV IN (ZC_ZTOV_UPDATED, ZC_ZTOV_NOCHANGES) OR ( ZR.ZTOV = 0 AND\n") >>f;
  printf("     NOT EXISTS (SELECT ZRID FROM %s ZR2 WHERE\n",NAME) >>f;
  printf("       ZR2.ZOID = ZR.ZOID AND ZR2.ZRID = ZR.ZRID AND ZR2.ZVER = v_ZO.ZVER) ) );\n") >>f;
  if( MAXREC != "" ){
   printf(" IF v_recount > %s THEN\n",MAXREC) >>f;
   printf("  v_ps.e := SAM.make_execstatus(null,null,'E_2MANYRECORDS',\n") >>f;
   printf("  '%s','%s','%s'); EXIT try;\n",NAME,OBJECT_NAME,MAXREC) >>f;
   printf(" END IF;\n") >>f;
  }
  if( MINREC != "" ){
   printf(" IF v_recount < %s THEN\n",MINREC) >>f;
   printf("  v_ps.e := SAM.make_execstatus(null,null,'E_NOTENOUGHRECS',\n") >>f;
   printf("  '%s','%s','%s'); EXIT try;\n",NAME,OBJECT_NAME,MINREC) >>f;
   printf(" END IF;\n") >>f;
  }
 }
 printf(" -- 1.6) check ZNAME\n") >>f;
 if(ZNAME != ""){
 printf(" SELECT %s INTO v_ZNAME FROM %s WHERE ZOID = v_ZO.ZOID AND\n",ZNAME,NAME) >>f;
 printf("   ZVER = v_ZO.ZVER AND ZTOV = ZC_ZTOV_UPDATED;\n") >>f;
 printf(" IF FOUND THEN\n") >>f;
 printf("  v_is_ZNAME := TRUE;\n") >>f;
 printf("  IF TIS.check_ZNAME('%s.%s',v_ZO.ZOID,v_ZNAME) THEN\n",SUBSYS,OBJECT) >>f;
 printf("  v_ps.e := SAM.make_execstatus(null,null,'E_DUPRECORD',\n") >>f;
 printf("  '%s','ZNAME',v_ZNAME); EXIT try;\n",NAME) >>f;
 printf("  END IF;\n") >>f;
 printf(" ELSE\n") >>f;
 printf("   PERFORM %s FROM %s WHERE ZOID = v_ZO.ZOID AND\n",ZNAME,NAME) >>f;
 printf("   ZVER = v_ZO.ZVER AND ZTOV IN (ZC_ZTOV_DELETED,ZC_ZTOV_RDELETED);\n") >>f;
 printf("   v_is_ZNAME := FOUND; v_ZNAME := NULL; -- delete ZNAME if so\n") >>f;
 printf(" END IF;\n") >>f;
 }
 printf(" -- 1.7) doing final quality control manually (FQCM)\n") >>f;
 printf("  -- may be in the future...\n") >>f;
 printf(" -- 2) update table\n") >>f;
 printf("  UPDATE %s ZR SET ZTOV = v_ZO.ZVER WHERE ZR.ZTOV = 0 AND ZR.ZOID = v_ZO.ZOID AND EXISTS\n",NAME) >>f;
 printf("    (SELECT ZRID FROM %s ZR2 WHERE \n",NAME) >>f;
 printf("      ZR2.ZTOV IN (ZC_ZTOV_UPDATED, ZC_ZTOV_DELETED, ZC_ZTOV_RDELETED) AND\n") >>f;
 printf("      ZR2.ZOID = ZR.ZOID AND ZR2.ZRID = ZR.ZRID AND ZR2.ZVER = v_ZO.ZVER);\n") >>f;
 printf("  UPDATE %s ZR SET ZTOV = 0 WHERE ZTOV = ZC_ZTOV_UPDATED AND ZR.ZOID = v_ZO.ZOID\n",NAME) >>f;
 printf("                                  AND ZR.ZVER = v_ZO.ZVER;\n") >>f;
 printf("  DELETE FROM %s ZR WHERE ZR.ZTOV < 0 AND ZR.ZOID = v_ZO.ZOID AND ZR.ZVER = v_ZO.ZVER;\n",NAME) >>f;
 if(ZNAME != ""){
 printf("  -- set ZNAME\n") >>f;
 printf("  IF v_is_ZNAME THEN\n") >>f;
 printf("   v_ps := TIS.set_ZNAME(v_as,v_ZO,v_ZNAME); v_as := v_ps.a;\n") >>f;
 printf("   IF NOT v_ps.success THEN EXIT try; END IF;\n") >>f;
 printf("  END IF;\n") >>f;
 }
 printf(" -- 3) all tests passed\n") >>f;
 printf(" v_ps.success := TRUE;\n") >>f;
 printf("END;\n") >>f;
 printf("-- FINALLY:\n") >>f;
 printf(" v_ps.a := v_as;\n") >>f;
 printf(" RETURN v_ps;\n") >>f;
 printf("END $$;\n") >>f;
 printf("COMMIT TRANSACTION;\n") >>f;
} #/make_commit_rec_proc

function make_rollback_rec_proc(f){
 make_header(f);
 printf("BEGIN TRANSACTION;\n") >>f;
 printf("CREATE OR REPLACE FUNCTION %s.rollback_%s(\n",SUBSYS,CODE) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_ZO\tTIS.ZObject\n") >>f;
 printf(") RETURNS SAM.procstate VOLATILE\n") >>f;
 printf("  LANGUAGE plpgSQL SECURITY DEFINER as $$\n") >>f;
 printf("DECLARE\n") >>f;
 printf(" v_ps SAM.procstate;\n") >>f;
 printf("BEGIN\n") >>f;
 printf("  DELETE FROM %s ZR WHERE ZR.ZTOV < 0 AND\n",NAME) >>f;
 printf("   ZR.ZOID = v_ZO.ZOID AND ZR.ZVER = v_ZO.ZVER;\n") >>f;
 printf("-- FINALLY:\n") >>f;
 printf(" v_ps.success := TRUE;\n") >>f;
 printf(" v_ps.a := v_as;\n") >>f;
 printf(" RETURN v_ps;\n") >>f;
 printf("END $$;\n") >>f;
 printf("COMMIT TRANSACTION;\n") >>f;
} #/make_rollback_rec_proc

function make_move_rec_proc(f){
 make_header(f);
 printf("BEGIN TRANSACTION;\n") >>f;
 printf("CREATE OR REPLACE FUNCTION %s.move_%s(\n",SUBSYS,CODE) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_ZO\tTIS.ZObject\n") >>f;
 printf(") RETURNS SAM.procstate VOLATILE\n") >>f;
 printf("  LANGUAGE plpgSQL SECURITY DEFINER as $$\n") >>f;
 printf("DECLARE\n") >>f;
 printf(" v_ps SAM.procstate;\n") >>f;
 printf("BEGIN\n") >>f;
 printf("  -- copy existing records with new ZSID & ZLVL\n") >>f;
 printf("  INSERT INTO %s SELECT\n",NAME) >>f;
  printf("      ZOID,ZRID,v_ZO.ZVER,0,v_ZO.ZSID,v_ZO.ZLVL,ZPID,\n") >>f;
 printf("      ") >>f;
 for(i=1;i<=FIELDS_COUNT;i++)
 {
  printf("%s",f_name[i]) >>f;
  if(i != FIELDS_COUNT) {
   printf(",") >>f;
   if(int(i/6)*6 - i == 0) 
    printf("\n      ") >>f;
  }
  else printf("\n") >>f;
 }
 printf("    FROM %s ZR WHERE ZR.ZTOV = 0 AND ZR.ZOID = v_ZO.ZOID;\n",NAME) >>f;
 printf("  -- obsolete existing records\n") >>f;
 printf("  UPDATE %s ZR SET ZTOV = v_ZO.ZVER WHERE ZR.ZTOV = 0 AND\n",NAME) >>f;
 printf("   ZR.ZOID = v_ZO.ZOID AND ZR.ZVER <> v_ZO.ZVER;\n") >>f;
 printf("-- FINALLY:\n") >>f;
 printf(" v_ps.success := TRUE;\n") >>f;
 printf(" v_ps.a := v_as;\n") >>f;
 printf(" RETURN v_ps;\n") >>f;
 printf("END $$;\n") >>f;
 printf("COMMIT TRANSACTION;\n") >>f;
} #/make_move_rec_proc

function make_delete_object_rec_proc(f){
 make_header(f);
 printf("BEGIN TRANSACTION;\n") >>f;
 printf("CREATE OR REPLACE FUNCTION %s.delete_object_%s(\n",SUBSYS,CODE) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_ZO\tTIS.ZObject\n") >>f;
 printf(") RETURNS SAM.procstate VOLATILE\n") >>f;
 printf("  LANGUAGE plpgSQL SECURITY DEFINER as $$\n") >>f;
 printf("DECLARE\n") >>f;
 printf(" v_ps SAM.procstate;\n") >>f;
 printf("BEGIN\n") >>f;
 printf("  UPDATE %s ZR SET ZTOV = v_ZO.ZVER WHERE ZR.ZTOV = 0 AND\n",NAME) >>f;
 printf("   ZR.ZOID = v_ZO.ZOID;\n") >>f;
 printf("-- FINALLY:\n") >>f;
 printf(" v_ps.success := TRUE;\n") >>f;
 printf(" v_ps.a := v_as;\n") >>f;
 printf(" RETURN v_ps;\n") >>f;
 printf("END $$;\n") >>f;
 printf("COMMIT TRANSACTION;\n") >>f;
} #/make_delete_object_rec_proc

function make_cmp_rec_proc(f){
 make_header(f);
 printf("BEGIN TRANSACTION;\n") >>f;
 printf("CREATE OR REPLACE FUNCTION %s.cmp_%s(\n",SUBSYS,CODE) >>f;
 printf("\tv_r\t%s,\n",NAME) >>f;
 printf("\tv_ro\t%s\n",NAME) >>f;
 printf(") RETURNS boolean STABLE\n") >>f;
 printf("  LANGUAGE plpgSQL SECURITY DEFINER as $$\n") >>f;
 printf("BEGIN\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++)
 {
  printf(" IF v_r.%s <> v_ro.%s THEN RETURN FALSE; END IF;\n",f_name[i],f_name[i]) >>f;
  printf("  IF (v_r.%s IS NULL OR v_ro.%s IS NULL) AND\n",f_name[i],f_name[i]) >>f;
  printf("  NOT COALESCE(v_r.%s,v_ro.%s) IS NULL THEN RETURN FALSE; END IF;\n",f_name[i],f_name[i]) >>f;
 }
 printf(" RETURN TRUE; -- both records equal\n") >>f;
 printf("END $$;\n") >>f;
 printf("COMMIT TRANSACTION;\n") >>f;
} #/make_cmp_rec_proc

function make_rdelete_rec_proc(f){
 make_header(f);
 printf("BEGIN TRANSACTION;\n") >>f;
 printf("CREATE OR REPLACE FUNCTION %s.rdelete_%s(\n",SUBSYS,CODE) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_ZO\tTIS.ZObject,\n") >>f;
 printf("\tv_ZPID\tbigint\n") >>f;
 printf(") RETURNS SAM.procstate\n") >>f;
 printf("  LANGUAGE plpgSQL SECURITY DEFINER as $$\n") >>f;
 printf("DECLARE\n") >>f;
 printf(" v_ps SAM.procstate;\n") >>f;
 printf(" v_r %s;\n",NAME) >>f;
 printf(" ZC_ZTOV_UPDATED CONSTANT bigint := -1;\n") >>f;
 printf(" ZC_ZTOV_DELETED CONSTANT bigint := -2;\n") >>f;
 printf(" ZC_ZTOV_NOCHANGES CONSTANT bigint := -3;\n") >>f;
 printf(" ZC_ZTOV_RDELETED CONSTANT bigint := -4;\n") >>f;
 printf("BEGIN\n") >>f;
 printf(" v_ps.a := v_as;\n") >>f;
 printf(" v_ps.success := TRUE;\n") >>f;
 printf(" FOR v_r IN SELECT * FROM %s ZR WHERE ZR.ZOID = v_ZO.ZOID AND\n",NAME) >>f;
 printf("             ZR.ZPID = v_ZPID AND ZR.ZTOV IN (0,ZC_ZTOV_UPDATED)\n",NAME) >>f;
 printf("  LOOP\n",NAME) >>f;
 for(i=1;i<=CHILD_COUNT;i++)
 {
 printf("   -- delete %s children of found row\n",CHILD_CODE[i]) >>f;
 printf("   v_ps := %s.rdelete_%s(v_ps.a,v_ZO,v_r.ZRID);\n",CHILD_SUBSYS[i],CHILD_CODE[i]) >>f;
 printf("   IF NOT v_ps.success THEN EXIT; END IF;\n") >>f;
 }
 printf("   -- delete found row \n") >>f;
 printf("   UPDATE %s SET ZTOV = ZC_ZTOV_RDELETED WHERE\n",NAME) >>f;
 printf("    ZOID = v_r.ZOID AND ZRID = v_r.ZRID AND ZVER = v_ZO.ZVER;\n") >>f;
 printf("   IF NOT FOUND THEN\n") >>f;
 printf("    v_r.ZVER := v_ZO.ZVER;\n") >>f;
 printf("    v_r.ZTOV := ZC_ZTOV_RDELETED;\n") >>f;
 printf("    INSERT INTO %s VALUES(v_r.*);\n",NAME) >>f;
 printf("   END IF;\n") >>f;
 printf(" END LOOP;\n") >>f;
 printf(" RETURN v_ps;\n") >>f;
 printf("END $$;\n") >>f;
 printf("COMMIT TRANSACTION;\n") >>f;
} #/make_rdelete_rec_proc

#
# Java classes generation
#

# true if datatype is CHAR (not VARCHAR)
# and rtrim required in some cases
function is_CHAR_datatype(field_no)
{
 if(f_DIC[field_no] == "YESNO") return(0);
 if( toupper(f_type[field_no]) ~ "^CHAR") return(1);
 return(0);
}

function javatype4fieldno(field_no)
{
 if(f_DIC[field_no] == "YESNO") return("YesNo");
 return(java4sqltype(f_type[field_no]));
} // javatype4fieldno(field_no)

function java4sqltype(sql_type,		st_name, st_length, tmp)
{
 st_name = toupper(sql_type);
 if (st_name == "INT") { return("Integer"); }
 if (st_name == "BIGINT") { return("Long"); }
 if (st_name == "SMALLINT") { return("Short"); }
 if (st_name ~ "CHAR" ) { return("String"); }
 if (st_name == "TEXT" ) { return("String"); }
 if (st_name == "BYTEA" ) { return("String"); }
 if (st_name == "TIMESTAMP") { return("DateTime"); }
 if (st_name == "TIMESTAMPTZ") { return("DateTime"); }
 if (st_name == "DATE") { return("DateOnly"); }
 if (st_name == "REAL") { return("Float"); }
 if (st_name == "FLOAT") { return("Float"); }
 if (st_name == "FLOAT8") { return("Double"); }
 if (st_name == "DOUBLE PRECISION") { return("Double"); }
 if (st_name ~ "NUMERIC" || st_name ~ "DECIMAL") { return("BigDecimal"); }
 return("Object");
}

function get_proc_questionmarks(	sz, i)
{
 sz=""; for(i=1;i<=FIELDS_COUNT;i++) {
  if(sz == "") sz="?"; else sz=sz ",?";
 }
 return(sz);
}

function make_java_header(f){
 printf("") >f;
 printf("// ConcepTIS project/QTIS project\n") >>f;
 printf("// (c) Alex V Eustrop 2009-2011\n") >>f;
 printf("// (c) Alex V Eustrop & EustroSoft.org 2023\n") >>f;
 printf("// see LICENSE.ConcepTIS at the project's root directory\n") >>f;
 printf("// WARNING! this code produced by automatic codegeneration tool\n")>>f;
 printf("//          located at src/sql/PGSQL/codegen/tools/codegen.awk\n")>>f;
 printf("//          In most cases you should'n change this file, but the above one.\n")>>f;
 printf("//\n") >>f;
 printf("// $Id$\n") >>f;
 printf("//\n") >>f;
 printf("\n") >>f;
}

function make_java_record_class(f){
 make_java_header(f);
 printf("package ru.mave.ConcepTIS.dao.%s;\n",SUBSYS) >>f;
 printf("\n") >>f;
 printf("import ru.mave.ConcepTIS.dao.*;\n") >>f;
 printf("import java.math.BigDecimal;\n") >>f;
 printf("\n") >>f;
 printf("/** %s table.\n",NAME) >>f;
 printf(" *\n") >>f;
 printf(" */\n") >>f;
 printf("public class %s extends DORecord\n",SHORT_NAME) >>f;
 printf("{\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 printf("public %s\t%s;\n", javatype4fieldno(i), f_name[i] ) >>f;
 }
 printf("\n") >>f;
 if(ZNAME != "") printf("public String getCaption(){return(%s);}\n",ZNAME) >>f;
 else printf("public String getCaption(){return(null);}\n") >>f;
 printf("\n") >>f;
 printf("// Strings\n") >>f;
 printf("\n") >>f;
 printf("public final static String TAB_CODE = \"%s\";\n",CODE) >>f;
 printf("public final static String TAB_NAME = \"%s\";\n",SHORT_NAME) >>f;
 printf("public final static String SUBSYS_CODE = \"%s\";\n",SUBSYS) >>f;
 printf("\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 printf("public final static String F_%s = \"%s\";\n",
	toupper(f_name[i]),f_name[i]) >>f;
 }
 printf("\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 printf("public final static String FC_%s = \"%s.%s%s\";\n",
	toupper(f_name[i]),SUBSYS,CODE,f_no[i]) >>f;
 printf("public final static String FCS_%s = \"%s\";\n",
	toupper(f_name[i]),f_no[i]) >>f;
 }
 printf("\n") >>f;
 printf("// children management\n") >>f;
 printf("\n") >>f;
 printf("public DORecord[] createChildrenFactory()\n") >>f;
 printf("{\n") >>f;
 if(CHILD_COUNT>=1){
 printf(" DORecord dor[]=new DORecord[%s];\n",CHILD_COUNT) >>f;
 for(i=1;i<=CHILD_COUNT;i++) {
  printf(" dor[%u]=new %s();\n",(i-1),CHILD_SHORT_NAME[i]) >>f;
 }
 printf(" return(dor);\n") >>f;
 }else{printf(" return(new DORecord[0]);\n") >>f;}
 printf("}\n") >>f;
 printf("\n") >>f;
 printf("public String[] getChildTabCodes()\n") >>f;
 printf("{\n") >>f;
 if(CHILD_COUNT>=1){
 printf(" String c[]=new String[%u];\n",CHILD_COUNT) >>f;
 for(i=1;i<=CHILD_COUNT;i++) {
  printf(" c[%u]=%s.TAB_CODE;\n",(i-1),CHILD_SHORT_NAME[i]) >>f;
 }
 printf(" return(c);\n") >>f;
 }else{printf(" return(new String[0]);\n") >>f;}
 printf("}\n") >>f;
 printf("\n") >>f;
 printf("// record processing methods\n") >>f;
 printf("\n") >>f;
 printf("public void getFromRS(java.sql.ResultSet rs)\n") >>f;
 printf(" throws java.sql.SQLException\n") >>f;
 printf("{\n") >>f;
 printf(" super.getFromRS(rs);\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
  if(is_CHAR_datatype(i))
    printf(" %s = rtrimString(getRS%s(rs,%u));\n",
	f_name[i],javatype4fieldno(i),i + STD_HEADER_OFFSET) >>f;
  else
    printf(" %s = getRS%s(rs,%u);\n",
	f_name[i],javatype4fieldno(i),i + STD_HEADER_OFFSET) >>f;
 }
 printf("} //\n") >>f;
 printf("\n") >>f;
 printf("public void getFromFS(ZFieldSet fs)\n") >>f;
 printf(" throws ZException\n") >>f;
 printf("{\n") >>f;
 printf(" super.getFromFS(fs);\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 printf(" %s = getFS%s(fs,F_%s);\n",
	f_name[i],javatype4fieldno(i),toupper(f_name[i])) >>f;
 }
 printf("} //\n") >>f;
 printf("\n") >>f;
 printf("public void putToPS(java.sql.PreparedStatement ps)\n") >>f;
 printf(" throws java.sql.SQLException\n") >>f;
 printf("{\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 printf(" setPS%s(ps,%u,%s);\n",
	javatype4fieldno(i), i + 3, f_name[i]) >>f;
 }
 printf("} //\n") >>f;
 printf("\n") >>f;
 printf("public ZFieldSet toFieldSet()\n") >>f;
 printf("{\n") >>f;
 printf(" ZFieldSet fs = super.toFieldSet();\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 printf(" fs.add(new ZField(F_%s,%s,FC_%s,ZField.T_UNKNOWN,%u));\n",
	toupper(f_name[i]),f_name[i],toupper(f_name[i]),f_size[i]) >>f;
 }
 printf(" return(fs);\n") >>f;
 printf("} //\n") >>f;
 printf("\n") >>f;
 printf("public String getSubsysCode(){return(SUBSYS_CODE);}\n") >>f;
 printf("public String getTabCode(){return(TAB_CODE);}\n") >>f;
 printf("public String getTabName(){return(TAB_NAME);}\n") >>f;
 if(PARENT == "")
  { printf("public String getParentTabCode(){return(null);}\n") >>f; }
 else
  { printf("public String getParentTabCode(){return(%s.TAB_CODE);}\n",
    PARENT_SHORT_NAME) >>f; }
 printf("public int getNumOfFields() {return(%u);}\n",FIELDS_COUNT) >>f;
 printf("\n") >>f;
 printf("public DORecord createFromRS(java.sql.ResultSet rs)\n") >>f;
 printf(" throws java.sql.SQLException\n") >>f;
 printf(" {return(new %s(rs));}\n",SHORT_NAME) >>f;
 printf("public DORecord createFromFS(ZFieldSet fs)\n") >>f;
 printf(" throws ZException\n") >>f;
 printf(" {return(new %s(fs));};\n",SHORT_NAME) >>f;
 printf("public DORecord createInstance(){return(new %s());}\n",SHORT_NAME) >>f;
 printf("\n") >>f;
 printf("// constructors\n") >>f;
 printf("public %s() { }\n",SHORT_NAME) >>f;
 printf("public %s(ZFieldSet fs) throws ZException { this.getFromFS(fs); }\n", \
	 SHORT_NAME) >>f;
 printf("public %s(java.sql.ResultSet rs)\n",SHORT_NAME) >>f;
 printf(" throws java.sql.SQLException { this.getFromRS(rs); }\n") >>f;
 printf("\n") >>f;
 printf("} //%s\n",SHORT_NAME) >>f;
} #/make_java_record_class()

function make_java_semiproducts(f){
 printf("// %s table semiproducts\n",NAME) >f;
 printf("\n// view record %s\n", NAME) >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 if(f_DIC[i] == "") 
 {
  if(f_REF[i] == 1 )
  {
   printf("was.printTabFieldZOID2Descr(fs,%s.F_%s,getZNames4Field(%s.FC_%s),wam.SUBSYS_%s);\n",SHORT_NAME,toupper(f_name[i]),SHORT_NAME,toupper(f_name[i]),SUBSYS) >>f;
   #was.printTabFieldZOID2Descr(fs,MMember.F_ORG_ID,getZNames4Field(MMember.FC_ORG_ID),wam.SUBSYS_QR);
  }
  else if( f_SID[i] == 1)
  {
   printf("was.printTabFieldList(fs,%s.F_%s,getSAMScopes(),wam.SUBSYS_SAM,wam.TARGET_SAMSCOPE);\n",SHORT_NAME,toupper(f_name[i])) >>f;
   #was.printTabFieldList(fs,MMember.F_SCOPE_ID,rl_scopes,wam.SUBSYS_SAM, wam.TARGET_SAMSCOPE);
  }
  else {
   printf("was.printTabField(fs,%s.F_%s);\n",SHORT_NAME,toupper(f_name[i])) >>f;
  }
 }
 else
 printf("was.printTabFieldDic(fs,%s.F_%s,getDic(\"%s\"));\n",SHORT_NAME,toupper(f_name[i]),f_DIC[i]) >>f;
 }
 printf("\n// edit record %s\n", NAME) >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 if(f_DIC[i] == "") 
 {
  if(f_REF[i] == 1 )
  {
   printf("was.printTabFieldInputList(fs%s.get(%s.F_%s),rl_%s);\n",CODE,CODE,toupper(f_name[i]),toupper(f_name[i])) >>f;
   #printf("was.printTabFieldZOID2Descr(fs,%s.F_%s,getZNames4Field(%s.FC_%s),wam.SUBSYS_%s);\n",SHORT_NAME,toupper(f_name[i]),SHORT_NAME,toupper(f_name[i]),SUBSYS) >>f;
   #was.printTabFieldZOID2Descr(fs,MMember.F_ORG_ID,getZNames4Field(MMember.FC_ORG_ID),wam.SUBSYS_QR);
  }
  else if( f_SID[i] == 1)
  {
   printf("was.printTabFieldInputList(fs%s.get(%s.F_%s),getSAMScopes());\n",CODE,CODE,toupper(f_name[i])) >>f;
   #was.printTabFieldInputList(fsMM.get(MM.F_SCOPE_ID),rl_scopes);
   #was.printTabFieldList(fs,MMember.F_SCOPE_ID,rl_scopes,wam.SUBSYS_SAM, wam.TARGET_SAMSCOPE);
  }
  else {
   printf("was.printTabFieldInput(fs%s.get(%s.F_%s));\n",CODE,CODE,toupper(f_name[i])) >>f;
  }
 }
 else
 printf("was.printTabFieldInputDic(fs%s.get(%s.F_%s),getDic(\"%s\"));\n",CODE,CODE,toupper(f_name[i]),f_DIC[i]) >>f;
 }
 printf("\n// table caption\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 printf("was.printHCellCaption(fs.get(%s.F_%s));\n",
	SHORT_NAME,toupper(f_name[i])) >>f;
 }
 printf("\n// table row from ZFieldSet\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 printf("was.printCellField(fs.get(%s.F_%s));\n",
	SHORT_NAME,toupper(f_name[i])) >>f;
 }
 printf("\n// table row from zDAO Record object\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 printf("was.printCellValue(wam.obj2string(r2.%s));\n",
	f_name[i]) >>f;
 }
 printf("\n// edit form from ZFieldSet\n") >>f;
 for(i=1;i<=FIELDS_COUNT;i++) {
 printf("was.printTabFieldInput( fs, ACLScope.F_%s);\n",
	toupper(f_name[i])) >>f;
 }
} #/make_java_semiproducts()
# END OF codegen.awk

