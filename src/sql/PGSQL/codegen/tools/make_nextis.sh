#!/bin/sh
# ConcepTIS project/QTIS project
# (c) Alex V Eustrop & EustroSoft.org 2020
# see LICENSE.ConcepTIS at the root of sources tree
#
# purpose: fork new TIS project from ConcepTIS
#	use it to start your own TIS-SQL based project
# HOWTO USE: this script should be run from src/sql/PGSQL/ directory:
#	sh codegen/tools/make_nextis.sh
#

NEXTIS_CONF=nextis.conf # any NEXTIS* valiables should be defined here/ see examples below
# START NEXTIS* variables
WORKDIR=work/
FILES4NEXTIS_LIST=00files4nextis.list	# list of files to copy with replace tis_users to $NEXTIS_SQL_USERS_ROLE
FILES4NEXTIS_WITH_ROLES_LIST=00files4nextis.with.roles.list	# files to replace all SQL ROLES listed below
NEXTIS=NEXTIS				# name of new project
CONCEPTIS_LICENSE_FILE=../../../LICENSE	# name of new project
NEXTIS_SQL_DB=nextisdb			# name of default database for new project
NEXTIS_SQL_DBO_ROLE=nextis_dbo		# name of dbo role instead of tisc
NEXTIS_SQL_USERS_ROLE=nextis_users	# main SQL role for regular system users instead of tis_users
NEXTIS_SQL_USER=nextis_user		# base name of new system test users instead of tiscuser
#NEXTISROOT=${WORKDIR}/${NEXTIS}	# root of new project will be constructed after NEXTIS_CONF reading
# END NEXTIS* variables

# load configuration for new project
if [  -r $NEXTIS_CONF ]; then
 . $NEXTIS_CONF
fi
NEXTISROOT=${WORKDIR}/${NEXTIS}		# root of new project

# check critical files
if [ ! -r $FILES4NEXTIS_LIST ]; then
 @echo $FILES4NEXTIS_LIST not exists > /dev/stderr
 exit 1;
fi
if [ ! -r $FILES4NEXTIS_WITH_ROLES_LIST ]; then
 @echo $FILES4NEXTIS_WITH_ROLES_LIST not exists > /dev/stderr
 exit 1;
fi

# preapare directory tree 

mkdir -p ${NEXTISROOT}
FILES4NEXTIS=`cat $FILES4NEXTIS_LIST` 

for F in $FILES4NEXTIS
do
 D=`dirname $F`
 if [ "$D"x != ".x" ]; then
  if [ "$D_PREV"x != "$D"x ]; then
   echo mkdir -p ${NEXTISROOT}/$D
  fi
  D_PREV=$D
 fi
done

# copy LICENCE of ConcepTIS
cp $CONCEPTIS_LICENSE_FILE ${NEXTISROOT}/LICENSE.ConcepTIS

# copy all files with replace tis_users tp $NEXTIS_SQL_USERS_ROLE
FILES4NEXTIS=`cat $FILES4NEXTIS_LIST` 

for F in $FILES4NEXTIS
do
 echo "cat $F | sed 's/tis_users/$NEXTIS_SQL_USERS_ROLE/g' | cat > ${NEXTISROOT}/$F"
done

# copy some files again with replace all SQL ROLES

FILES4NEXTIS=`cat $FILES4NEXTIS_WITH_ROLES_LIST` 

for F in $FILES4NEXTIS
do
 echo "cat $F | sed 's/tis_users/$NEXTIS_SQL_USERS_ROLE/g' | \
  sed 's/tiscuser/$NEXTIS_SQL_USER/g' | \
  sed 's/conceptisdb/$NEXTIS_SQL_DB/g' | \
  sed 's/tisc/$NEXTIS_SQL_DBO_ROLE/g' | cat > ${NEXTISROOT}/$F"
done

# prepare initdb
NEXTIS_INITDB=initdb/4_init_db.sql
F=$NEXTIS_INITDB
 echo "cat $F | sed 's/tis_users/$NEXTIS_SQL_USERS_ROLE/g' | \
  sed 's/ConcepTISDB/$NEXTIS_SQL_DB/g' | \
  sed 's/\"tisc\"/\"${NEXTIS_SQL_DBO_ROLE}\"/g' | cat > ${NEXTISROOT}/$F"
