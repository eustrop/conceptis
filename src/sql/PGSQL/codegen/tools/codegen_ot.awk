#!/usr/bin/awk -f
# ConcepTIS project/QTIS project
# (c) Alex V Eustrop 2009-2011
# (c) Alex V Eustrop & EustroSoft.org 2023
# see LICENSE.ConcepTIS at the project's root directory
#
#   $Id$
#
# any subsystem codegeneration - object type

BEGIN{
} #/BEGIN
END{
if(is_aborted)exit 1;
is_begun=1; # used by do_abort
WDIR="work/"
make_proc("commit",WDIR "/functions/commit_" OBJECT ".sql");
make_proc("delete",WDIR "/functions/delete_" OBJECT ".sql");
make_proc("move",WDIR "/functions/move_" OBJECT ".sql");
make_proc("rollback",WDIR "/functions/rollback_" OBJECT ".sql");
make_java_dotprocessor_class(WDIR "/java/zDAO/" OBJECT_NAME ".java");
} #/END
/^[ \t]*#/{next;} # comments
($1=="OBJECT"){OBJECT_NAME=$2;OBJECT=$3;next;}
($1=="SUBSYS"){SUBSYS=$2;next;}
($1=="RECORD"){
	TAB_COUNT++;
	RECORD_NAME[TAB_COUNT]=$2;
	RECORD_CODE[TAB_COUNT]=$3;
	INDEX_BY_CODE[RECORD_CODE[TAB_COUNT]]=TAB_COUNT;
	n2=split(RECORD_NAME[TAB_COUNT],tmp_a,".");
	RECORD_SUBSYS[TAB_COUNT]=tmp_a[1];
	REC_SHORT_NAME[TAB_COUNT]=tmp_a[2];
	if(REC_SHORT_NAME[TAB_COUNT]=="")
	{do_abort("Missing subsystem for record : " $2); }
	# get record's attributes
	n=split($4,tmp_attrib,",");
	for(i=1;i<=n;i++){
	 n2=split(tmp_attrib[i],tmp_a,"=");
	 if(tmp_a[1]=="PARENT"){RECORD_PARENT[TAB_COUNT]=tmp_a[2];}
	 else if(tmp_a[1]=="HEAD"){RECORD_IS_HEAD[TAB_COUNT]=1;
				HEAD_TAB_NO=TAB_COUNT;}
	 else if(tmp_a[1]=="NOHEAD"){RECORD_IS_NOT_HEAD[TAB_COUNT]=1;}
	 else{do_abort("Invalid record's attributes token: " tmp_a[1]);} 
	} #/for(tmp_attrib)
	if(TAB_COUNT==1){ # this is first record, so it's HEAD by default
	 if(RECORD_PARENT[1]=="" && RECORD_IS_NOT_HEAD[1]==""){HEAD_TAB_NO=1;}
	} #/if(TAB_COUNT==1)
	next;
	}
#//{print ;}
//{do_abort("Invalid start token: " $1); }
function do_abort(msg)
{
if(!is_begun)
 print "PARSING ERROR: " msg "\n at line " NR ": >> " $0 " <<";
else
 print "PROCESSING ERROR: " msg;
is_aborted=1;
exit 1;
}

function do_abort_unallowed_attr(attr)
{
do_abort("Unallowed field's attribute: " attr);
}

function is_value_in(value,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10)
{
if(v10 != "") do_abort("too many arguments for is_value_in(). value=" value);
if(value == "") return 0;
if(value == v1) return 1; if(value == v2) return 1; if(value == v3) return 1;
if(value == v4) return 1; if(value == v5) return 1; if(value == v6) return 1;
if(value == v7) return 1; if(value == v8) return 1; if(value == v9) return 1;
return 0;
}

# assistance

function make_header(f)
{
 printf("") >f;
 printf("-- ConcepTIS project/QTIS project\n") >>f;
 printf("-- (c) Alex V Eustrop 2009\n") >>f;
 printf("-- (c) Alex V Eustrop & EustroSoft.org 2023\n") >>f;
 printf("-- see LICENSE.ConcepTIS at the project's root directory\n") >>f;
 printf("--\n") >>f;
 printf("-- $Id$\n") >>f;
 printf("--\n") >>f;
 printf("-- SUBSYSTEM: %s\n",SUBSYS) >>f;
 printf("-- OBJECT: %s (%s)\n\n",OBJECT_NAME,OBJECT) >>f;
 printf("-- WARNING! this code produced by automatic codegeneration tool\n")>>f;
 printf("--          located at src/sql/PGSQL/codegen/tools/codegen_ot.awk\n")>>f;
 printf("--          do not change this file directly but modify the above one. \n")>>f;
 printf("\n") >>f;
}

#
# universal commit/rollback/delete/move code generator
# Usage: make_proc("commit/rollback/delete/move",<filename>)
#

function make_proc(proc, f){
 if(!is_value_in(proc,"commit","rollback","move","delete"))
  do_abort("make_proc for '" proc "' not implemented");
 make_header(f);
 printf("BEGIN TRANSACTION;\n") >>f;
 printf("CREATE OR REPLACE FUNCTION %s.%s_%s(\n",SUBSYS,proc,OBJECT) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_ZO\tTIS.ZObject\n") >>f;
 printf(") RETURNS SAM.procstate VOLATILE\n") >>f;
 printf("  LANGUAGE plpgSQL SECURITY DEFINER as $$\n") >>f;
 printf("DECLARE\n") >>f;
 printf(" v_ps SAM.procstate;\n") >>f;
 printf(" i    integer;\n") >>f;
 if(proc == "commit") {
  printf(" v_ZPID    bigint; -- for parent/child checks\n") >>f;
  printf(" v_ZRID    bigint; \n") >>f;
 }
 printf("BEGIN\n") >>f;
 printf(" v_ps.success := FALSE;\n") >>f;
 printf(" v_ps.a := v_as;\n") >>f;
 printf("<<try>>\n") >>f;
 printf("BEGIN\n") >>f;
 printf(" -- 1) per table %s with FQC\n",proc) >>f;
 for(i=1;i<=TAB_COUNT;i++){
  if(proc=="delete")
   printf(" v_ps := %s.delete_object_%s(v_as,v_ZO);\n",
      RECORD_SUBSYS[i],RECORD_CODE[i]) >>f;
  else
   printf(" v_ps := %s.%s_%s(v_as,v_ZO);\n",
     RECORD_SUBSYS[i],proc,RECORD_CODE[i]) >>f;
 printf("  IF NOT v_ps.success THEN EXIT try; END IF;\n") >>f;
 }
 printf(" -- 2) global FQC\n") >>f;
 if(proc == "commit")
 {
  for(i=1;i<=TAB_COUNT;i++){
   if(RECORD_PARENT[i] == "") continue;
   printf("  SELECT ZPID,ZRID INTO v_ZPID,v_ZRID FROM %s ZR WHERE ZOID = v_ZO.ZOID\n",
     RECORD_NAME[i]) >>f;
   printf("   AND ZTOV = 0 AND NOT EXISTS ( SELECT * FROM %s ZP WHERE\n",
     RECORD_NAME[INDEX_BY_CODE[RECORD_PARENT[i]]]) >>f;
   printf("    ZP.ZOID = ZR.ZOID AND ZP.ZRID = ZR.ZPID AND ZP.ZTOV = 0);\n") >>f;
   printf("   IF FOUND THEN\n") >>f;
   printf("    v_ps.success = FALSE;\n") >>f;
   printf("    v_ps.e := SAM.make_execstatus(v_ZO.ZOID,v_ZO.ZVER,'E_INVALIDPARENT',\n") >>f;
   printf("    CAST(v_ZPID AS text),'%s',CAST(v_ZRID AS text)); EXIT try;\n",RECORD_NAME[i] ) >>f;
   printf("   END IF;\n") >>f;
  } #/for
 }
 else
  printf("  -- not necessary or not implemented\n") >>f;
 printf(" -- 3) all tests passed\n") >>f;
 printf(" v_ps.success := TRUE;\n") >>f;
 printf("END;\n") >>f;
 printf("-- FINALLY:\n") >>f;
 printf(" RETURN v_ps;\n") >>f;
 printf("END $$;\n") >>f;
 printf("REVOKE ALL ON FUNCTION %s.%s_%s(\n",SUBSYS,proc,OBJECT) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_ZO\tTIS.ZObject\n") >>f;
 printf("\t) FROM PUBLIC;\n") >>f;
 printf("COMMIT TRANSACTION;\n") >>f;
} #/make_proc

function make_java_header(f){
 printf("") >f;
 printf("// ConcepTIS project/QTIS project\n") >>f;
 printf("// (c) Alex V Eustrop 2009-2011\n") >>f;
 printf("// (c) Alex V Eustrop & EustroSoft.org 2023\n") >>f;
 printf("// see LICENSE.ConcepTIS at the project's root directory\n") >>f;
 printf("// WARNING! this code produced by automatic codegeneration tool\n")>>f;
 printf("//          located at src/sql/PGSQL/codegen/tools/codegen_ot.awk\n")>>f;
 printf("//          In most cases you should'n change this file, but the above one.\n")>>f;
 printf("//\n") >>f;
 printf("// $Id$\n") >>f;
 printf("//\n") >>f;
 printf("\n") >>f;
}

function make_java_dotprocessor_class(f,	i)
{
 printf("") >f;
 make_java_header(f);
 printf("package ru.mave.ConcepTIS.dao.%s;\n",SUBSYS) >>f;
 printf("\n") >>f;
 printf("import ru.mave.ConcepTIS.dao.*;\n") >>f;
 printf("\n") >>f;
 printf("/** %s.%s data object.\n",SUBSYS,OBJECT_NAME) >>f;
 printf(" *\n") >>f;
 printf(" */\n") >>f;
 printf("public class %s extends DOTProcessor\n",OBJECT_NAME) >>f;
 printf("{\n") >>f;
 printf("\n") >>f;
 printf("public final static String DOBJECT_CODE = \"%s.%s\";\n",SUBSYS,OBJECT) >>f;
 printf("public final static String DOBJECT_CODE_SHORT = \"%s\";\n",OBJECT) >>f;
 printf("public final static String DOBJECT_NAME = \"%s\";\n",OBJECT_NAME) >>f;
 printf("public final static String SUBSYS_CODE = \"%s\";\n",SUBSYS) >>f;
 printf("\n") >>f;
 printf("public String getDOTypeCodeShort(){return(DOBJECT_CODE_SHORT);}\n") >>f;
 printf("public String getDOTypeCode(){return(DOBJECT_CODE);}\n") >>f;
 printf("public String getDOTypeName(){return(DOBJECT_NAME);}\n") >>f;
 printf("public String getDOTypeSubsys(){return(SUBSYS_CODE);}\n") >>f;
 printf("\n") >>f;
 printf("private static DOTProcessor dotp;\n") >>f;
 printf("\n") >>f;
 printf("/** get common instance of this class. */\n") >>f;
 printf("public static synchronized DOTProcessor getDOTProcessor()\n") >>f;
 printf("{\n") >>f;
 printf("if(dotp == null) dotp = new %s();\n",OBJECT_NAME) >>f;
 printf("return(dotp);\n") >>f;
 printf("}\n") >>f;
 printf("\n") >>f;
 printf("/** dobject with this DOTProcessor set. */\n") >>f;
 printf("public static DObject newDObject()\n") >>f;
 printf("{ return(new DObject(getDOTProcessor()) ); }\n") >>f;
 printf("\n") >>f;
 printf("public DORecord[] createMembersFactory()\n") >>f;
 printf("{\n") >>f;
 printf("  DORecord[] dor = new DORecord[%s];\n",TAB_COUNT) >>f;
 for(i=1;i<=TAB_COUNT;i++)
 {
  printf("  dor[%s] = new %s();\n",(i-1),REC_SHORT_NAME[i]) >>f;
 }
 printf("  return(dor);\n") >>f;
 printf("}\n") >>f;
 printf("\n") >>f;
 printf("public String makeCaption(DObject o)\n") >>f;
 if(HEAD_TAB_NO != "" && HEAD_TAB_NO > 0){
  printf("{ return(DObject.make_headrecord_caption(o,%s.TAB_CODE));}\n",
  REC_SHORT_NAME[HEAD_TAB_NO]) >>f;
 }
 else { printf("{ return(null); }\n") >>f; }
 printf("\n") >>f;
 printf("// constructors\n") >>f;
 printf("private %s(){}\n",OBJECT_NAME) >>f;
 printf("} //%s\n",OBJECT_NAME) >>f;
}
# END OF codegen_ot.awk FILE
