#!/usr/bin/awk -f
# ConcepTIS project/QTIS project
# (c) Alex V Eustrop 2009
# (c) Alex V Eustrop & EustroSoft.org 2023
# see LICENSE.ConcepTIS at the project's root directory
#
#   $Id$
#
# subsystem "glue" codegeneration

BEGIN{
} #/BEGIN
END{
if(is_aborted)exit 1;
is_begun=1; # used by do_abort
WDIR="work/"
make_proc("commit",WDIR "/functions/commit_object.sql");
make_proc("delete",WDIR "/functions/delete_object.sql");
make_proc("move",WDIR "/functions/move_object.sql");
make_proc("rollback",WDIR "/functions/rollback_object.sql");
} #/END
/^[ \t]*#/{next;} # comments
($1=="OBJECT"){OBJECT_COUNT++;
	OBJECT_NAME[OBJECT_COUNT]=$2;
	OBJECT[OBJECT_COUNT]=$3;
	next;}
($1=="SUBSYS"){
	OBJECT_SUBSYS[OBJECT_COUNT]=$2;
	if(SUBSYS=="") SUBSYS=OBJECT_SUBSYS[OBJECT_COUNT];
	if(SUBSYS!=OBJECT_SUBSYS[OBJECT_COUNT]){
	do_abort("all objects must be at the same subsystem: " \
	 OBJECT_NAME[OBJECT_COUNT] " " OBJECT_SUBSYS[OBJECT_COUNT]);
	 }
	next;}
($1=="RECORD"){
	# ignore
	next;
	}
#//{print ;}
//{do_abort("Invalid start token: " $1); }
function do_abort(msg)
{
if(!is_begun)
 print "PARSING ERROR: " msg "\n at line " NR ": >> " $0 " <<";
else
 print "PROCESSING ERROR: " msg;
is_aborted=1;
exit 1;
}

function do_abort_unallowed_attr(attr)
{
do_abort("Unallowed field's attribute: " attr);
}

function is_value_in(value,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10)
{
if(v10 != "") do_abort("too many arguments for is_value_in(). value=" value);
if(value == "") return 0;
if(value == v1) return 1; if(value == v2) return 1; if(value == v3) return 1;
if(value == v4) return 1; if(value == v5) return 1; if(value == v6) return 1;
if(value == v7) return 1; if(value == v8) return 1; if(value == v9) return 1;
return 0;
}

# assistance

function make_header(f)
{
 printf("") >f;
 printf("-- ConcepTIS project/QTIS project\n") >>f;
 printf("-- (c) Alex V Eustrop 2009\n") >>f;
 printf("-- (c) Alex V Eustrop & EustroSoft.org 2023\n") >>f;
 printf("-- see LICENSE.ConcepTIS at the project's root directory\n") >>f;
 printf("--\n") >>f;
 printf("-- $Id$\n") >>f;
 printf("--\n") >>f;
 printf("-- SUBSYSTEM: %s\n\n",SUBSYS) >>f;
 printf("-- WARNING! this code produced by automatic codegeneration tool\n")>>f;
 printf("--          located at src/sql/PGSQL/codegen/tools/codegen_subsys.awk\n")>>f;
 printf("--          do not change this file directly but modify the above one. \n")>>f;
 printf("\n") >>f;
}

#
# universal commit/rollback/delete/move code generator
# Usage: make_proc("commit/rollback/delete/move",<filename>)
#

function make_proc(proc, f){
 if(!is_value_in(proc,"commit","rollback","move","delete"))
  do_abort("make_proc for '" proc "' not implemented");
 make_header(f);
 printf("BEGIN TRANSACTION;\n") >>f;
 printf("CREATE OR REPLACE FUNCTION %s.%s_object(\n",SUBSYS,proc) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_ZO\tTIS.ZObject\n") >>f;
 printf(") RETURNS SAM.procstate VOLATILE\n") >>f;
 printf("  LANGUAGE plpgSQL SECURITY DEFINER as $$\n") >>f;
 printf("DECLARE\n") >>f;
 printf(" v_ps SAM.procstate;\n") >>f;
 printf("BEGIN\n") >>f;
 printf("  v_ps.success := TRUE;\n") >>f;
 printf("  v_ps.a := v_as;\n") >>f;
 for(i=1;i<=OBJECT_COUNT;i++){
  tmp_sz="ELSIF"; if(i==1)tmp_sz="IF";
  printf("  %s v_ZO.ZTYPE = '%s.%s' THEN\n",tmp_sz,SUBSYS,OBJECT[i]) >>f;
  printf("   v_ps := %s.%s_%s(v_as,v_ZO); v_as := v_ps.a;\n",SUBSYS,
   proc,OBJECT[i]) >>f;
 }
 printf("  ELSE\n") >>f;
 printf("   v_ps.e := SAM.make_execstatus(v_ZO.ZOID,v_ZO.ZVER,'E_INVALIDZTYPE',v_ZO.ZTYPE);\n") >>f;
 printf("   v_ps.success := FALSE;\n") >>f;
 printf("  END IF;\n") >>f;
 printf(" IF NOT v_ps.success THEN RAISE EXCEPTION '%s.%s rolled back'; END IF;\n",SUBSYS,proc) >>f;
 printf(" RETURN v_ps;\n") >>f;
 printf("EXCEPTION\n") >>f;
 printf("  WHEN RAISE_EXCEPTION THEN\n") >>f;
 printf("   RETURN v_ps;\n") >>f;
 printf("END $$;\n") >>f;
 printf("REVOKE ALL ON FUNCTION %s.%s_object(\n",SUBSYS,proc) >>f;
 printf("\tv_as\tSAM.auditstate,\n") >>f;
 printf("\tv_ZO\tTIS.ZObject\n") >>f;
 printf(") FROM PUBLIC;\n") >>f;
 printf("COMMIT TRANSACTION;\n") >>f;
} #/make_proc
