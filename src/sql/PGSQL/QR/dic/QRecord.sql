-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: QR
-- OBJECT: QR (Q) RECORD: QRecord (QR)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

select dic.set_code('TIS_FIELD','QR.QR01','QR код','QR код должен содержать ровно 8(16) символов, алфавит [0-9,A-F], первые 5(12) - это диапазон, оставшиеся 3(4) - номер внутри диапазона в 16-ричном виде');
select dic.set_code('TIS_FIELD','QR.QR02','Действие','Код действия : STD,REDIRECT,LIST,...');
select dic.set_code('TIS_FIELD','QR.QR03','Перенаправить','Перенаправить запрос на внешний заданный URL');
select dic.set_code('TIS_FIELD','QR.QR04','Объект','ССылка на объект, связанный с карточкой и QR-кодом (сейчас ссылка на другую карточку)');
select dic.set_code('TIS_FIELD','QR.QR06','№ договора','Для отображения в карточке. допустимо несколько карточек с одним номером договора');
select dic.set_code('TIS_FIELD','QR.QR07','дата договора','дата заключения договора');
select dic.set_code('TIS_FIELD','QR.QR08','Цена','цена изделия, оффициальная');
select dic.set_code('TIS_FIELD','QR.QR09','НДС','НДС в цене изделия (UNKNOW,NOVAT,VAT20,VAT18,VAT10,VAT0)');
select dic.set_code('TIS_FIELD','QR.QR10','поддержка','ссылка на объект Участник, информация из которой будет показана вместо владельца диапазона');
select dic.set_code('TIS_FIELD','QR.QR17','Тип продукта','Код типа продукта. типы продуктов описываются в словаре и сопровождаются описанием, отображаемым к QR-карточке');
select dic.set_code('TIS_FIELD','QR.QR18','Модель продукта','Модель продукта, в рамках типа продукта, указанного выше. описывается в словаре продуктов');
select dic.set_code('TIS_FIELD','QR.QR19','Ревизия модели','Ревизия модели  продукта, для указания дополнительных характеристик. описывается в словаре продуктов');
select dic.set_code('TIS_FIELD','QR.QR20','SN','Серийный номер изделия. Возможно - серийные номера агрегатов через запятую. Потом разберемся');
select dic.set_code('TIS_FIELD','QR.QR21','Дата производства','Дата производства изделия');
select dic.set_code('TIS_FIELD','QR.QR22','Номер ГТД','Сейчас - номер ГТД. Изначально хотели указывать дату ввоза в Россию, или дату поступления на склад.');
select dic.set_code('TIS_FIELD','QR.QR23','Дата продажи','Дата продажи - видимо дата поступления денег или гарантийного письма об оплате ');
select dic.set_code('TIS_FIELD','QR.QR24','Дата отправки клиенту','Дата отправки клиенту/отгрузки со склада. Обычно - это-же дата начала гарантии');
select dic.set_code('TIS_FIELD','QR.QR25','Дата начала гарантии','Дата начала гарантии для конечного пользователя. т.е. при продажи дилером - задается им');
select dic.set_code('TIS_FIELD','QR.QR26','Дата окончания гарантии','Дата окончания гарантии. Обычно + 1 год от продажи, но нет правил без исключений');
select dic.set_code('TIS_FIELD','QR.QR27','Долгота°','Долгота в градусах (в системе EPSG:4326 WGS 84)');
select dic.set_code('TIS_FIELD','QR.QR28','Широта°','Широта в градусах (в системе EPSG:4326 WGS 84)');
select dic.set_code('TIS_FIELD','QR.QR29','Высота[m]','Высота в метрах над уровнем Моря или геоида (видимо в системе EPSG:4979 WGS 84)');
select dic.set_code('TIS_FIELD','QR.QR30','Комментарий (публичный)','Это общедоступный комментарий, он виден в публичное QR-карточке всем! конфиденциальное пишите только в (ДСП) поля.');
select dic.set_code('TIS_FIELD','QR.QR31','CSV-запись','расширенные характеристики объекта/дополнительные поля (зарезервировано)');
select dic.set_code('TIS_FIELD','QR.QR32','OWiki','Wiki-описание объекта');
