-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: QR
-- OBJECT: Product (P) RECORD: PProduct (PP)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

select dic.set_code('TIS_FIELD','QR.PP01','Продукт','Кодовое имя типа(класса) продукта(ов)');
select dic.set_code('TIS_FIELD','QR.PP02','Модель','Кодовое имя модели, в рамках типа продукта');
select dic.set_code('TIS_FIELD','QR.PP03','Ревизия','кодовое имя ревизии/версии модели/комплектации');
select dic.set_code('TIS_FIELD','QR.PP04','Запчасть','артикул запчасти или расходного материала для изделия, идентифицированного кодами выше');
select dic.set_code('TIS_FIELD','QR.PP05','Директория','Ссылка на директорию, содержимое которой прилагается к описанию (зарезервировано)');
select dic.set_code('TIS_FIELD','QR.PP06','Язык','Язык описания');
select dic.set_code('TIS_FIELD','QR.PP07','Название','Название документа');
select dic.set_code('TIS_FIELD','QR.PP08','OWiki','OWiki-описание продукта, модели, ревизии модели или запчасти');
