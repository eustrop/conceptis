-- QR subsystem
-- mb move to global dictionary
select dic.set_code('LEI_TYPE','NONE','none','');
select dic.set_code('LEI_TYPE','INN','ИНН','');
select dic.set_code('LEI_TYPE','OGRN','ОГРН','');
select dic.set_code('LEI_TYPE','OGRNIP','ОГРН','');
select dic.set_code('LEI_TYPE','LIE','ОГРН','');
select dic.set_code('LET_TYPE','NONE','нет','');
-- mb move to global dictionary
select dic.set_code('LET_TYPE','NONE','Нет','');
select dic.set_code('LET_TYPE','OOO','ООО','');
select dic.set_code('LET_TYPE','ODO','ОДО','');
select dic.set_code('LET_TYPE','IP','ИП','');
select dic.set_code('LET_TYPE','PP','Физ-лицо','');
select dic.set_code('LET_TYPE','AO','АО','');
select dic.set_code('LET_TYPE','PAO','ПАО','');
select dic.set_code('LET_TYPE','ZAO','ЗАО','');
select dic.set_code('LET_TYPE','OAO','ОАО','');
-- mb move to global dictionary
select dic.set_code('VAT_CODES','UNKNOW','неизвестно','требует уточнения');
select dic.set_code('VAT_CODES','NOVAT','Без НДС','товар/услуга без НДС (УСН)');
select dic.set_code('VAT_CODES','VAT20','20%','20% НДС в цене');
select dic.set_code('VAT_CODES','VAT18','18%','18% НДС в цене');
select dic.set_code('VAT_CODES','VAT10','10%','10% НДС в цене');
select dic.set_code('VAT_CODES','VAT0','0%','0% НДС в цене (для товаров определенных законом и организаций на ОСНО освобожденных от НДС)');
-- mb move to global dictionary
select dic.set_code('CURRENCY_CODE','UNKN','неизвестно','требует уточнения');
select dic.set_code('CURRENCY_CODE','RUB','Рубль','Российский рубль');
select dic.set_code('CURRENCY_CODE','USD','Доллар','Доллар США');
select dic.set_code('CURRENCY_CODE','EUR','Евро','Евро');
select dic.set_code('CURRENCY_CODE','CNY','Юань','Китайский юань');
--
select dic.set_code('QR_MEMBER_STATUS','Y','Действующий','Действующий участник системы/контрагент');
select dic.set_code('QR_MEMBER_STATUS','N','Нействующий','Нействующий участник системы/контрагент (можно не показывать в списках выбора)');
select dic.set_code('QR_MEMBER_STATUS','O','Наша организация','показывать в списке исполнителей с нашей стороны');
--
select dic.set_code('QR_RANGE_STATUS','Y','Действующий','Действующий диапазон');
select dic.set_code('QR_RANGE_STATUS','N','Нействующий','Нействующий диапазон');
-- mb move to global dictionary
select dic.set_code('RANGE_TYPE','QR','QR-коды','');
select dic.set_code('RANGE_TYPE','QOID','QOID-идентификаторы','');
select dic.set_code('RANGE_TYPE','CNUM','Номера договоров','');
select dic.set_code('RANGE_TYPE','VLAN','Номера VLAN','');
select dic.set_code('RANGE_TYPE','IPv4','IPv4 адреса','');
select dic.set_code('RANGE_TYPE','IPv6','IPv6 адреса','');
select dic.set_code('RANGE_TYPE','MAC48','MAC48 адреса','');
select dic.set_code('RANGE_TYPE','MAC64','MAC64 адреса','');
--
select dic.set_code('QR_ACTION','STD','Стандартная обработка','');
select dic.set_code('QR_ACTION','REDIRECT','Перенаправление на указанную страницу','');
select dic.set_code('QR_ACTION','REDIRECT_QR','Перенаправление на другую карточку QR','');
select dic.set_code('QR_ACTION','REDIRECT_QR_SVC','Перенаправление на другой qr-сервис','');
select dic.set_code('QR_ACTION','REDIRECT_QOID','Перенаправление на документ','');
select dic.set_code('QR_ACTION','HIDE','Скрыть','Не показывать карточку');
select dic.set_code('QR_ACTION','LIST','Список','Показать список доступных карточек (даже если она одна)');
--

--CCard.tab:02    autoexport      char(1) NN,DIC=YESNO    Публиковать автоматически       Автоматически публиковать выборку из карточки в публичную систему qr. Публикуется все, кроме ДСП полей
--CCard.tab:09    cmoney_vat      varchar(6)      NN,DIC=VAT_CODES        НДС(ДСП) НДС для цены (UNKNOW,NOVAT,VAT20,VAT18,VAT10,VAT0)
--CCard.tab:#10   supplier        text    NUL,DIC,        Юр-лицо поставщик       кто исполнитель по договору, если у нас более одного юр-лица или ИП
--CCard.tab:#12   prodtype        text    PUB,NUL,DIC,SHOW,QRPRODTYPE,    Тип продукта    Тип продукта.
--CCard.tab:#13   prodmodel       text    PUB,NUL,DIC,SHOW,QRPRODMODEL,   Модель продукта Модель продукта
--MMember.tab:04  status  char(1) NN,DIC=QR_MEMBER_STATUS Действующий     Статус участника
--MMember.tab:05  lei_type        char(8) NN,DIC=LEI_TYPE Тип LEI Тип идентификатора (INN/OGRN/INN.KZ/../LEI)
--MMember.tab:07  let     char(8) NUL,DIC=LET_TYPE        ОПФ     Организационно-правовая форма (ООО,ИП,ФЛ,...)
--MRange.tab:03   rtype   varchar(8)      NN,DIC=RANGE_TYPE       Класс диапазона QR,QOID,CNUM,VLAN,IPv4,IPv6,IP64,MAC48,MAC64,...
--MRange.tab:04   status  char(1) NN,DIC=QR_RANGE_STATUS  статус  статус диапазона
--MRange.tab:05   action  varchar(16)     NUL,DIC=QR_ACTION       Действие        Код действия : STD,REDIRECT,LIST,...
--PProduct.tab:06 lang    varchar(3)      NN,DIC=LANG     Язык    Язык описания   Язык данного описания
--QRecord.tab:02  action  varchar(16)     NUL,DIC=QR_ACTION       Действие        Код действия : STD,REDIRECT,LIST,...
--QRecord.tab:09  price_vat       varchar(6)      NUL,DIC=VAT_CODES       НДС НДС в цене изделия (UNKNOW,NOVAT,VAT20,VAT18,VAT10,VAT0)
--
