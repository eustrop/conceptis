-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: QR
-- OBJECT: Member (M) RECORD: MMember (MM)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

select dic.set_code('TIS_FIELD','QR.MM01','Кодовое имя','Уникальное символическое имя');
select dic.set_code('TIS_FIELD','QR.MM02','Организация','Ссылка на объект "Организация", соответствующий участнику');
select dic.set_code('TIS_FIELD','QR.MM03','Область данных','Где содержаться данные подсистемы QR');
select dic.set_code('TIS_FIELD','QR.MM04','Статус','Статус участника');
select dic.set_code('TIS_FIELD','QR.MM05','Тип LEI','Тип идентификатора (INN/OGRN/INN.KZ/../LEI)');
select dic.set_code('TIS_FIELD','QR.MM06','ИНН/ОГРН/LEI','ИНН/ОГРН/LEI участника');
select dic.set_code('TIS_FIELD','QR.MM07','ОПФ','Организационно-правовая форма (ООО,ИП,ФЛ,...)');
select dic.set_code('TIS_FIELD','QR.MM08','Название','Название организации');
select dic.set_code('TIS_FIELD','QR.MM09','Адрес','Адрес организации, фактический');
select dic.set_code('TIS_FIELD','QR.MM10','Сайт','сайт организации');
select dic.set_code('TIS_FIELD','QR.MM11','Тел.','телефоны организации (официальные)');
select dic.set_code('TIS_FIELD','QR.MM12','E-Mail','E-Mail (официальные)');
select dic.set_code('TIS_FIELD','QR.MM13','Описание','Описание организации');
select dic.set_code('TIS_FIELD','QR.MM14','OWiki Описание','Описание организации в формате OWiki');
