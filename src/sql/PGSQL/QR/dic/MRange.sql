-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: QR
-- OBJECT: Member (M) RECORD: MRange (MR)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

select dic.set_code('TIS_FIELD','QR.MR01','Диапазон','Начало диапазона');
select dic.set_code('TIS_FIELD','QR.MR02','длина в битах','Длина диапазона в битах');
select dic.set_code('TIS_FIELD','QR.MR03','Класс диапазона','QR,QOID,CNUM,VLAN,IPv4,IPv6,IP64,MAC48,MAC64,...');
select dic.set_code('TIS_FIELD','QR.MR04','статус','статус диапазона');
select dic.set_code('TIS_FIELD','QR.MR05','Действие','Код действия : STD,REDIRECT,LIST,...');
select dic.set_code('TIS_FIELD','QR.MR06','Перенаправить','Перенаправить запрос на внешний заданный URL');
select dic.set_code('TIS_FIELD','QR.MR07','дата выделения','дата, когда диапазон был выделен');
select dic.set_code('TIS_FIELD','QR.MR08','Участник','Участник, которому [пере-]выделен диапазон');
select dic.set_code('TIS_FIELD','QR.MR09','Основание выделения','Документ, на основании которого выделен диапазон');
select dic.set_code('TIS_FIELD','QR.MR10','описание','описание диапазона и его применения');
select dic.set_code('TIS_FIELD','QR.MR11','OWiki','Wiki-описание диапазона');
