-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: QR
-- OBJECT: Member (M) RECORD: MMember (MM)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

-- V0_ - current state of table without ACL/MAC filtring
-- destined for internal use only.
-- nobody except tables owner should use it
DROP VIEW IF EXISTS QR.V0_MMember CASCADE;
CREATE VIEW QR.V0_MMember AS SELECT
 * FROM QR.MMember AS ZR
	WHERE ZR.ZTOV = 0;

-- V_ - current state of table with ACL/MAC filtring
DROP VIEW IF EXISTS QR.V_MMember CASCADE;
CREATE VIEW QR.V_MMember AS SELECT * FROM QR.MMember AS ZR
	WHERE ZR.ZTOV = 0 AND
	 ZR.ZLVL <= SAM.get_user_slevel() -- MAC - check secrecy 
	 AND
	 -- check pemissions for reading records at scope
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	   WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
	    XAS.sid = ZR.ZSID AND XAS.obj_type = 'QR.M'
	    and XAS.reada = 'Y' )
	 -- check rejections for reading records at scope
	 AND NOT
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	   WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
	     XAS.sid = ZR.ZSID AND XAS.obj_type = 'QR.M'
	     and XAS.reada = 'R' );

-- VI_ - intermediate records created by current user
DROP VIEW IF EXISTS QR.VI_MMember CASCADE;
CREATE VIEW QR.VI_MMember AS SELECT ZR.* FROM QR.MMember AS ZR, TIS.ZObject AS ZO
	WHERE ZR.ZTOV < 0 AND ZO.ZUID = SAM.get_user() AND
	 ZR.ZOID = ZO.ZOID AND ZR.ZVER = ZO.ZVER AND
	 ZR.ZLVL <= SAM.get_user_slevel() -- MAC - check secrecy 
	 AND
	 -- check pemissions for reading records at scope
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	   WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
	     XAS.sid = ZR.ZSID AND XAS.obj_type = 'QR.M'
	     and XAS.reada = 'Y' )
	 -- check rejections for reading records at scope
	 AND NOT
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	   WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
	     XAS.sid = ZR.ZSID AND XAS.obj_type = 'QR.M'
	     and XAS.reada = 'R' );

-- VH_ - historic data in table
DROP VIEW IF EXISTS QR.VH_MMember CASCADE;
CREATE VIEW QR.VH_MMember AS SELECT * FROM QR.MMember AS ZR
	WHERE ZR.ZTOV >= 0 AND
	 ZR.ZLVL <= SAM.get_user_slevel() -- MAC - check secrecy 
	 AND
	 -- check pemissions for reading records at scope
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	   WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
	     XAS.sid = ZR.ZSID AND XAS.obj_type = 'QR.M'
	     and XAS.historya = 'Y' )
	 -- check rejections for reading records at scope
	 AND NOT
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	   WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
	     XAS.sid = ZR.ZSID AND XAS.obj_type = 'QR.M'
	     and XAS.historya = 'R' );

-- VD_ - slice of data in table which was actual at point of time
-- requested via set_vdate() call
DROP VIEW IF EXISTS QR.VD_MMember CASCADE;
CREATE VIEW QR.VD_MMember AS SELECT ZR.* FROM QR.MMember AS ZR, TIS.ZObject AS ZO
	WHERE ZR.ZOID = ZO.ZOID AND ZR.ZVER <= ZO.ZVER AND
	(ZR.ZTOV = 0 OR (ZR.ZTOV > 0 AND ZR.ZTOV > ZO.ZVER )) AND
	 ( -- check date range
	  ZO.ZDATE <= tis.get_vdate()
	  AND ( ZO.ZDATO IS NULL OR ZO.ZDATO > tis.get_vdate() )
	  )
	 AND
	 ZR.ZLVL <= SAM.get_user_slevel() -- MAC - check secrecy 
	 AND
	 -- check pemissions for reading records at scope
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	   WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
	     XAS.sid = ZR.ZSID AND XAS.obj_type = 'QR.M'
	     and XAS.historya = 'Y' )
	 -- check rejections for reading records at scope
	 AND NOT
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	   WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
	     XAS.sid = ZR.ZSID AND XAS.obj_type = 'QR.M'
	     and XAS.historya = 'R' );
