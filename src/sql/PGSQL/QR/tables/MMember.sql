-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: QR
-- OBJECT: Member (M) RECORD: MMember (MM)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

CREATE TABLE QR.MMember (
	ZOID	bigint NOT NULL,
	ZRID	bigint NOT NULL,
	ZVER	bigint NOT NULL,
	ZTOV	bigint NOT NULL,
	ZSID	bigint NOT NULL,
	ZLVL	smallint NOT NULL,
	ZPID	bigint NOT NULL,
	code	varchar(255) NOT NULL,
	org_id	bigint NULL,
	scope_id	bigint NULL,
	status	char(1) NOT NULL,
	lei_type	char(8) NOT NULL,
	lei	char(12) NULL,
	let	char(8) NULL,
	name	char(512) NULL,
	addr	char(512) NULL,
	site	varchar(255) NULL,
	phone	varchar(255) NULL,
	email	char(255) NULL,
	descr	varchar(1024) NULL,
	owiki	varchar(10240) NULL,
	PRIMARY KEY (ZOID,ZRID,ZVER)
	);
CREATE INDEX MMember_idx1 on QR.MMember(code);
CREATE INDEX MMember_idx2 on QR.MMember(scope_id);
CREATE INDEX MMember_idx3 on QR.MMember(lei);
CREATE INDEX MMember_idx4 on QR.MMember(org_id);
