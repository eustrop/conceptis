-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: QR
-- OBJECT: QR (Q) RECORD: QRecord (QR)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

CREATE TABLE QR.QRecord (
	ZOID	bigint NOT NULL,
	ZRID	bigint NOT NULL,
	ZVER	bigint NOT NULL,
	ZTOV	bigint NOT NULL,
	ZSID	bigint NOT NULL,
	ZLVL	smallint NOT NULL,
	ZPID	bigint NOT NULL,
	QR	bigint NULL,
	action	varchar(16) NULL,
	redirect	varchar(127) NULL,
	obj_id	bigint NULL,
	cnum	varchar(32) NULL,
	cdate	date NULL,
	price_gpl	numeric NULL,
	price_vat	varchar(6) NULL,
	support_id	bigint NULL,
	prodtype	varchar(32) NULL,
	prodmodel	varchar(32) NULL,
	pmrevision	varchar(32) NULL,
	sn	varchar(64) NULL,
	prodate	date NULL,
	GTD	varchar(127) NULL,
	saledate	date NULL,
	sendate	date NULL,
	wstart	date NULL,
	wend	date NULL,
	gis_long	float8 NULL,
	gis_lat	float8 NULL,
	gis_alt	float8 NULL,
	comment	varchar(1024) NULL,
	csvcard	varchar(1024) NULL,
	owiki	varchar(10240) NULL,
	PRIMARY KEY (ZOID,ZRID,ZVER)
	);
CREATE INDEX QRecord_idx1 on QR.QRecord(QR);
CREATE INDEX QRecord_idx2 on QR.QRecord(prodtype,prodmodel);
CREATE INDEX QRecord_idx3 on QR.QRecord(obj_id);
CREATE INDEX QRecord_idx4 on QR.QRecord(support_id);
