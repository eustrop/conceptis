-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: QR
-- OBJECT: Product (P) RECORD: PProduct (PP)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

CREATE TABLE QR.PProduct (
	ZOID	bigint NOT NULL,
	ZRID	bigint NOT NULL,
	ZVER	bigint NOT NULL,
	ZTOV	bigint NOT NULL,
	ZSID	bigint NOT NULL,
	ZLVL	smallint NOT NULL,
	ZPID	bigint NOT NULL,
	prodtype	varchar(32) NOT NULL,
	prodmodel	varchar(32) NULL,
	pmrevision	varchar(32) NULL,
	prodpart	varchar(32) NULL,
	dir_id	bigint NULL,
	lang	varchar(3) NOT NULL,
	title	varchar(255) NULL,
	owiki	varchar(10485760) NULL,
	PRIMARY KEY (ZOID,ZRID,ZVER)
	);
CREATE INDEX PProduct_idx1 on QR.PProduct(prodtype,prodmodel);
CREATE INDEX PProduct_idx2 on QR.PProduct(dir_id);
