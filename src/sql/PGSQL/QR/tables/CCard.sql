-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: QR
-- OBJECT: Card (C) RECORD: CCard (CC)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

CREATE TABLE QR.CCard (
	ZOID	bigint NOT NULL,
	ZRID	bigint NOT NULL,
	ZVER	bigint NOT NULL,
	ZTOV	bigint NOT NULL,
	ZSID	bigint NOT NULL,
	ZLVL	smallint NOT NULL,
	ZPID	bigint NOT NULL,
	QR	bigint NULL,
	autoexport	char(1) NULL,
	pubqr_id	bigint NULL,
	cardate	date NULL,
	cnum_id	bigint NULL,
	cnum	varchar(32) NULL,
	cdate	date NULL,
	cmoney	numeric NULL,
	cmoney_cur	varchar(4) NULL,
	cmoney_vat	varchar(6) NULL,
	cmoney_desc	varchar(255) NULL,
	supplier_id	bigint NULL,
	supplier	varchar(64) NULL,
	client_id	bigint NULL,
	client	varchar(512) NULL,
	claddr	varchar(127) NULL,
	prodtype	varchar(32) NULL,
	prodmodel	varchar(32) NULL,
	pmrevision	varchar(32) NULL,
	sn	varchar(64) NULL,
	prodate	date NULL,
	GTD	varchar(127) NULL,
	saledate	date NULL,
	sendate	date NULL,
	wstart	date NULL,
	wend	date NULL,
	gis_long	float8 NULL,
	gis_lat	float8 NULL,
	gis_alt	float8 NULL,
	comment	varchar(1024) NULL,
	csvcard	varchar(1024) NULL,
	PRIMARY KEY (ZOID,ZRID,ZVER)
	);
CREATE INDEX CCard_idx1 on QR.CCard(QR);
CREATE INDEX CCard_idx2 on QR.CCard(prodtype,prodmodel);
CREATE INDEX CCard_idx3 on QR.CCard(pubqr_id);
CREATE INDEX CCard_idx4 on QR.CCard(cnum_id);
CREATE INDEX CCard_idx5 on QR.CCard(supplier_id);
CREATE INDEX CCard_idx6 on QR.CCard(client_id);
