-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: QR
-- OBJECT: Member (M) RECORD: MRange (MR)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

CREATE TABLE QR.MRange (
	ZOID	bigint NOT NULL,
	ZRID	bigint NOT NULL,
	ZVER	bigint NOT NULL,
	ZTOV	bigint NOT NULL,
	ZSID	bigint NOT NULL,
	ZLVL	smallint NOT NULL,
	ZPID	bigint NOT NULL,
	rstart	bigint NOT NULL,
	rbitl	smallint NOT NULL,
	rtype	varchar(8) NOT NULL,
	status	char(1) NOT NULL,
	action	varchar(16) NULL,
	redirect	varchar(127) NULL,
	alloc	timestamptz NULL,
	member_id	bigint NULL,
	doc_id	bigint NULL,
	descr	varchar(1024) NULL,
	owiki	varchar(10240) NULL,
	PRIMARY KEY (ZOID,ZRID,ZVER)
	);
CREATE INDEX MRange_idx1 on QR.MRange(rstart,rbitl,rtype);
CREATE INDEX MRange_idx2 on QR.MRange(member_id);
CREATE INDEX MRange_idx3 on QR.MRange(doc_id);
