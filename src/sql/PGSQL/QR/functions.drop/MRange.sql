DROP FUNCTION QR.create_MRange(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_rstart	bigint,
	v_rbitl	smallint,
	v_rtype	varchar(8),
	v_status	char(1),
	v_action	varchar(16),
	v_redirect	varchar(127),
	v_alloc	timestamptz,
	v_member_id	bigint,
	v_doc_id	bigint,
	v_descr	varchar(1024),
	v_owiki	varchar(10240)
) CASCADE;
DROP FUNCTION QR.update_MRange(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_rstart	bigint,
	v_rbitl	smallint,
	v_rtype	varchar(8),
	v_status	char(1),
	v_action	varchar(16),
	v_redirect	varchar(127),
	v_alloc	timestamptz,
	v_member_id	bigint,
	v_doc_id	bigint,
	v_descr	varchar(1024),
	v_owiki	varchar(10240)
) CASCADE;
DROP FUNCTION QR.delete_MRange(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) CASCADE;
DROP FUNCTION IF EXISTS QR.QC_MR(
	v_as	SAM.auditstate,
	v_r	QR.MRange
	) CASCADE;
DROP FUNCTION IF EXISTS QR.commit_MR(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS QR.rollback_MR(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS QR.move_MR(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS QR.delete_object_MR(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS QR.cmp_MR(
	v_r	QR.MRange,
	v_ro	QR.MRange
	) CASCADE;
DROP FUNCTION IF EXISTS QR.rdelete_MR(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) CASCADE;
