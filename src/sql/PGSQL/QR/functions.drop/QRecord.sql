DROP FUNCTION QR.create_QRecord(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_QR	bigint,
	v_action	varchar(16),
	v_redirect	varchar(127),
	v_obj_id	bigint,
	v_cnum	varchar(32),
	v_cdate	date,
	v_price_gpl	numeric,
	v_price_vat	varchar(6),
	v_support_id	bigint,
	v_prodtype	varchar(32),
	v_prodmodel	varchar(32),
	v_pmrevision	varchar(32),
	v_sn	varchar(64),
	v_prodate	date,
	v_GTD	varchar(127),
	v_saledate	date,
	v_sendate	date,
	v_wstart	date,
	v_wend	date,
	v_gis_long	float8,
	v_gis_lat	float8,
	v_gis_alt	float8,
	v_comment	varchar(1024),
	v_csvcard	varchar(1024),
	v_owiki	varchar(10240)
) CASCADE;
DROP FUNCTION QR.update_QRecord(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_QR	bigint,
	v_action	varchar(16),
	v_redirect	varchar(127),
	v_obj_id	bigint,
	v_cnum	varchar(32),
	v_cdate	date,
	v_price_gpl	numeric,
	v_price_vat	varchar(6),
	v_support_id	bigint,
	v_prodtype	varchar(32),
	v_prodmodel	varchar(32),
	v_pmrevision	varchar(32),
	v_sn	varchar(64),
	v_prodate	date,
	v_GTD	varchar(127),
	v_saledate	date,
	v_sendate	date,
	v_wstart	date,
	v_wend	date,
	v_gis_long	float8,
	v_gis_lat	float8,
	v_gis_alt	float8,
	v_comment	varchar(1024),
	v_csvcard	varchar(1024),
	v_owiki	varchar(10240)
) CASCADE;
DROP FUNCTION QR.delete_QRecord(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) CASCADE;
DROP FUNCTION IF EXISTS QR.QC_QR(
	v_as	SAM.auditstate,
	v_r	QR.QRecord
	) CASCADE;
DROP FUNCTION IF EXISTS QR.commit_QR(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS QR.rollback_QR(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS QR.move_QR(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS QR.delete_object_QR(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS QR.cmp_QR(
	v_r	QR.QRecord,
	v_ro	QR.QRecord
	) CASCADE;
DROP FUNCTION IF EXISTS QR.rdelete_QR(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) CASCADE;
