REVOKE ALL ON FUNCTION QR.create_MMember(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_code	varchar(255),
	v_org_id	bigint,
	v_scope_id	bigint,
	v_status	char(1),
	v_lei_type	char(8),
	v_lei	char(12),
	v_let	char(8),
	v_name	char(512),
	v_addr	char(512),
	v_site	varchar(255),
	v_phone	varchar(255),
	v_email	char(255),
	v_descr	varchar(1024),
	v_owiki	varchar(10240)
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION QR.create_MMember(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_code	varchar(255),
	v_org_id	bigint,
	v_scope_id	bigint,
	v_status	char(1),
	v_lei_type	char(8),
	v_lei	char(12),
	v_let	char(8),
	v_name	char(512),
	v_addr	char(512),
	v_site	varchar(255),
	v_phone	varchar(255),
	v_email	char(255),
	v_descr	varchar(1024),
	v_owiki	varchar(10240)
) TO tis_users;
GRANT EXECUTE ON FUNCTION QR.update_MMember(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_code	varchar(255),
	v_org_id	bigint,
	v_scope_id	bigint,
	v_status	char(1),
	v_lei_type	char(8),
	v_lei	char(12),
	v_let	char(8),
	v_name	char(512),
	v_addr	char(512),
	v_site	varchar(255),
	v_phone	varchar(255),
	v_email	char(255),
	v_descr	varchar(1024),
	v_owiki	varchar(10240)
) TO tis_users;
REVOKE EXECUTE ON FUNCTION QR.delete_MMember(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION QR.delete_MMember(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) TO tis_users;
REVOKE ALL ON FUNCTION QR.QC_MM(
	v_as	SAM.auditstate,
	v_r	QR.MMember
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION QR.commit_MM(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION QR.rollback_MM(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION QR.move_MM(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION QR.delete_object_MM(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION QR.cmp_MM(
	v_r	QR.MMember,
	v_ro	QR.MMember
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION QR.rdelete_MM(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) FROM PUBLIC;
