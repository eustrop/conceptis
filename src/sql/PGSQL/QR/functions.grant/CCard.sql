REVOKE ALL ON FUNCTION QR.create_CCard(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_QR	bigint,
	v_autoexport	char(1),
	v_pubqr_id	bigint,
	v_cardate	date,
	v_cnum_id	bigint,
	v_cnum	varchar(32),
	v_cdate	date,
	v_cmoney	numeric,
	v_cmoney_cur	varchar(4),
	v_cmoney_vat	varchar(6),
	v_cmoney_desc	varchar(255),
	v_supplier_id	bigint,
	v_supplier	varchar(64),
	v_client_id	bigint,
	v_client	varchar(512),
	v_claddr	varchar(127),
	v_prodtype	varchar(32),
	v_prodmodel	varchar(32),
	v_pmrevision	varchar(32),
	v_sn	varchar(64),
	v_prodate	date,
	v_GTD	varchar(127),
	v_saledate	date,
	v_sendate	date,
	v_wstart	date,
	v_wend	date,
	v_gis_long	float8,
	v_gis_lat	float8,
	v_gis_alt	float8,
	v_comment	varchar(1024),
	v_csvcard	varchar(1024)
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION QR.create_CCard(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_QR	bigint,
	v_autoexport	char(1),
	v_pubqr_id	bigint,
	v_cardate	date,
	v_cnum_id	bigint,
	v_cnum	varchar(32),
	v_cdate	date,
	v_cmoney	numeric,
	v_cmoney_cur	varchar(4),
	v_cmoney_vat	varchar(6),
	v_cmoney_desc	varchar(255),
	v_supplier_id	bigint,
	v_supplier	varchar(64),
	v_client_id	bigint,
	v_client	varchar(512),
	v_claddr	varchar(127),
	v_prodtype	varchar(32),
	v_prodmodel	varchar(32),
	v_pmrevision	varchar(32),
	v_sn	varchar(64),
	v_prodate	date,
	v_GTD	varchar(127),
	v_saledate	date,
	v_sendate	date,
	v_wstart	date,
	v_wend	date,
	v_gis_long	float8,
	v_gis_lat	float8,
	v_gis_alt	float8,
	v_comment	varchar(1024),
	v_csvcard	varchar(1024)
) TO tis_users;
GRANT EXECUTE ON FUNCTION QR.update_CCard(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_QR	bigint,
	v_autoexport	char(1),
	v_pubqr_id	bigint,
	v_cardate	date,
	v_cnum_id	bigint,
	v_cnum	varchar(32),
	v_cdate	date,
	v_cmoney	numeric,
	v_cmoney_cur	varchar(4),
	v_cmoney_vat	varchar(6),
	v_cmoney_desc	varchar(255),
	v_supplier_id	bigint,
	v_supplier	varchar(64),
	v_client_id	bigint,
	v_client	varchar(512),
	v_claddr	varchar(127),
	v_prodtype	varchar(32),
	v_prodmodel	varchar(32),
	v_pmrevision	varchar(32),
	v_sn	varchar(64),
	v_prodate	date,
	v_GTD	varchar(127),
	v_saledate	date,
	v_sendate	date,
	v_wstart	date,
	v_wend	date,
	v_gis_long	float8,
	v_gis_lat	float8,
	v_gis_alt	float8,
	v_comment	varchar(1024),
	v_csvcard	varchar(1024)
) TO tis_users;
REVOKE EXECUTE ON FUNCTION QR.delete_CCard(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION QR.delete_CCard(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) TO tis_users;
REVOKE ALL ON FUNCTION QR.QC_CC(
	v_as	SAM.auditstate,
	v_r	QR.CCard
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION QR.commit_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION QR.rollback_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION QR.move_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION QR.delete_object_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION QR.cmp_CC(
	v_r	QR.CCard,
	v_ro	QR.CCard
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION QR.rdelete_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) FROM PUBLIC;
