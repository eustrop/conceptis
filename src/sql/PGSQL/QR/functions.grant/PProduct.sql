REVOKE ALL ON FUNCTION QR.create_PProduct(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_prodtype	varchar(32),
	v_prodmodel	varchar(32),
	v_pmrevision	varchar(32),
	v_prodpart	varchar(32),
	v_dir_id	bigint,
	v_lang	varchar(3),
	v_title	varchar(255),
	v_owiki	varchar(10485760)
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION QR.create_PProduct(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_prodtype	varchar(32),
	v_prodmodel	varchar(32),
	v_pmrevision	varchar(32),
	v_prodpart	varchar(32),
	v_dir_id	bigint,
	v_lang	varchar(3),
	v_title	varchar(255),
	v_owiki	varchar(10485760)
) TO tis_users;
GRANT EXECUTE ON FUNCTION QR.update_PProduct(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_prodtype	varchar(32),
	v_prodmodel	varchar(32),
	v_pmrevision	varchar(32),
	v_prodpart	varchar(32),
	v_dir_id	bigint,
	v_lang	varchar(3),
	v_title	varchar(255),
	v_owiki	varchar(10485760)
) TO tis_users;
REVOKE EXECUTE ON FUNCTION QR.delete_PProduct(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION QR.delete_PProduct(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) TO tis_users;
REVOKE ALL ON FUNCTION QR.QC_PP(
	v_as	SAM.auditstate,
	v_r	QR.PProduct
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION QR.commit_PP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION QR.rollback_PP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION QR.move_PP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION QR.delete_object_PP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION QR.cmp_PP(
	v_r	QR.PProduct,
	v_ro	QR.PProduct
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION QR.rdelete_PP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) FROM PUBLIC;
