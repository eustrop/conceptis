-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: QR
-- OBJECT: QR (Q)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen_ot.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION QR.delete_Q(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
) RETURNS SAM.procstate VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 i    integer;
BEGIN
 v_ps.success := FALSE;
 v_ps.a := v_as;
<<try>>
BEGIN
 -- 1) per table delete with FQC
 v_ps := QR.delete_object_QR(v_as,v_ZO);
  IF NOT v_ps.success THEN EXIT try; END IF;
 -- 2) global FQC
  -- not necessary or not implemented
 -- 3) all tests passed
 v_ps.success := TRUE;
END;
-- FINALLY:
 RETURN v_ps;
END $$;
REVOKE ALL ON FUNCTION QR.delete_Q(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
COMMIT TRANSACTION;
