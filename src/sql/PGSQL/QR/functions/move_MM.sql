-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: QR
-- OBJECT: Member (M) RECORD: MMember (MM)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION QR.move_MM(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
) RETURNS SAM.procstate VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
BEGIN
  -- copy existing records with new ZSID & ZLVL
  INSERT INTO QR.MMember SELECT
      ZOID,ZRID,v_ZO.ZVER,0,v_ZO.ZSID,v_ZO.ZLVL,ZPID,
      code,org_id,scope_id,status,lei_type,lei,
      let,name,addr,site,phone,email,
      descr,owiki
    FROM QR.MMember ZR WHERE ZR.ZTOV = 0 AND ZR.ZOID = v_ZO.ZOID;
  -- obsolete existing records
  UPDATE QR.MMember ZR SET ZTOV = v_ZO.ZVER WHERE ZR.ZTOV = 0 AND
   ZR.ZOID = v_ZO.ZOID AND ZR.ZVER <> v_ZO.ZVER;
-- FINALLY:
 v_ps.success := TRUE;
 v_ps.a := v_as;
 RETURN v_ps;
END $$;
COMMIT TRANSACTION;
