-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: QR
-- OBJECT: QR (Q) RECORD: QRecord (QR)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION QR.create_QRecord(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_QR	bigint,
	v_action	varchar(16),
	v_redirect	varchar(127),
	v_obj_id	bigint,
	v_cnum	varchar(32),
	v_cdate	date,
	v_price_gpl	numeric,
	v_price_vat	varchar(6),
	v_support_id	bigint,
	v_prodtype	varchar(32),
	v_prodmodel	varchar(32),
	v_pmrevision	varchar(32),
	v_sn	varchar(64),
	v_prodate	date,
	v_GTD	varchar(127),
	v_saledate	date,
	v_sendate	date,
	v_wstart	date,
	v_wend	date,
	v_gis_long	float8,
	v_gis_lat	float8,
	v_gis_alt	float8,
	v_comment	varchar(1024),
	v_csvcard	varchar(1024),
	v_owiki	varchar(10240)
) RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 v_as SAM.auditstate;
 v_es SAM.execstatus;
 v_ZO TIS.ZObject%ROWTYPE;
 v_r QR.QRecord%ROWTYPE; -- this version
 v_user bigint;
 v_recount bigint; -- records count(*) for MAXREC
 ZC_ZTOV_UPDATED CONSTANT bigint := -1;
 ZC_ZTOV_DELETED CONSTANT bigint := -2;
 ZC_ZTOV_NOCHANGES CONSTANT bigint := -3;
 ZC_ZTOV_RDELETED CONSTANT bigint := -4;
BEGIN
 -- 0) enter into procedure
 v_as.subsys := 'QR';
 v_as.eaction :='C';
 v_as.obj_type :='QR';
 -- v_as.ZID := null; -- not ready yet
 v_as.ZTYPE := 'QR.Q';
 v_as.ZOID := v_ZOID;
 v_as.ZVER := v_ZVER;
 v_as.proc := 'create_QRecord';
 -- 0.1) get target ZO record and extract missings from
 SELECT * INTO v_ZO FROM TIS.ZObject as ZO WHERE
   ZO.ZOID = v_ZOID AND ZO.ZVER = v_ZVER AND ZO.ZTYPE = 'QR.Q'
   FOR SHARE; -- lock row from concurrent updates 
 v_as.ZLVL := v_ZO.ZLVL; -- FOUND or NOT - indiscriminately
 v_as.sid := v_ZO.ZSID;
 v_as := SAM.do_auditlog_enter(v_as);
 -- 0.2) copy parameters to v_r
 v_r.ZOID = v_ZOID;
 v_r.ZVER = v_ZVER;
 v_r.ZPID = v_ZPID; -- revise at 4.2
 v_r.QR := v_QR;
 v_r.action := v_action;
 v_r.redirect := v_redirect;
 v_r.obj_id := v_obj_id;
 v_r.cnum := v_cnum;
 v_r.cdate := v_cdate;
 v_r.price_gpl := v_price_gpl;
 v_r.price_vat := v_price_vat;
 v_r.support_id := v_support_id;
 v_r.prodtype := v_prodtype;
 v_r.prodmodel := v_prodmodel;
 v_r.pmrevision := v_pmrevision;
 v_r.sn := v_sn;
 v_r.prodate := v_prodate;
 v_r.GTD := v_GTD;
 v_r.saledate := v_saledate;
 v_r.sendate := v_sendate;
 v_r.wstart := v_wstart;
 v_r.wend := v_wend;
 v_r.gis_long := v_gis_long;
 v_r.gis_lat := v_gis_lat;
 v_r.gis_alt := v_gis_alt;
 v_r.comment := v_comment;
 v_r.csvcard := v_csvcard;
 v_r.owiki := v_owiki;
<<try>>
BEGIN
 -- 1) pre access control checks
 -- 1.1) check isolation level
 IF NOT SAM.check_isolation() THEN
    v_es := SAM.make_es_transisolation(null,null); EXIT try;
 END IF;
 -- 1.2) identify user
 IF SAM.get_user() IS NULL THEN
    v_es := sam.make_execstatus(null,null,'E_NOUSER',session_user); EXIT try;
 END IF;
 -- 1.3) unallowed nulls
 IF v_r.ZOID IS NULL THEN v_es := SAM.make_es_notnull(null,null,
	'create_QRecord','ZOID'); EXIT try; END IF;
 IF v_r.ZVER IS NULL THEN v_es := SAM.make_es_notnull(null,null,
	'create_QRecord','ZVER'); EXIT try; END IF;
 IF NOT v_r.ZPID IS NULL THEN v_es := SAM.make_execstatus(null,null,
	'E_INVALIDVALUE','<NOT NULL>','create_QRecord.ZPID'); EXIT try; END IF;
 -- 1.4) check object's verion (ZO record)
 IF v_ZO.ZOID IS NULL THEN -- aka NOT FOUND after (0.1)
   v_es := SAM.make_execstatus(null,null,'E_INVALIDVERSION',
   'QR.Q',CAST(v_ZOID AS text),CAST(v_ZVER AS text)); EXIT try; END IF;
 -- 1.5) check ZSTA
 IF v_ZO.ZSTA <> 'I' THEN
  v_es := SAM.make_execstatus(null,null,'E_NOTOPENED',
  '(QR,'||v_r.ZOID||','||v_r.ZVER||')'); EXIT try;
 END IF;
 -- 1.6) check ZUID
 IF v_ZO.ZUID <> SAM.get_user() THEN
  v_es := SAM.make_execstatus(null,null,'E_NOTOWNER',
  '(QR,'||v_r.ZOID||','||v_r.ZVER||')'); EXIT try;
 END IF;
 -- 2) check access
 IF v_ZO.ZVER = 1 THEN 
  v_ps := SAM.check_access_create(v_as,v_ZO.ZSID,v_ZO.ZTYPE,v_ZO.ZLVL);
 ELSE
  v_ps := SAM.check_access_write(v_as,v_ZO.ZSID,v_ZO.ZTYPE,v_ZO.ZOID,v_ZO.ZLVL);
 END IF;
 v_as := v_ps.a;
 -- 3) lock required tables
 -- LOCK TABLE QR.QRecord NOT REQUIRED;
 -- 4) check data
 -- 4.1) get requested record version
  -- no record yet
 -- 4.2) set record header
 v_r.ZSID := v_ZO.ZSID;
 v_r.ZLVL := v_ZO.ZLVL;
 v_r.ZPID := COALESCE(v_r.ZPID,0); -- NOT NULL, using explicit 0 for 'no parent'
 v_r.ZTOV := ZC_ZTOV_UPDATED;
 -- 4.98) let's check MAXRECords before creating new one
 SELECT count(*) INTO v_recount FROM QR.QRecord ZR WHERE ZR.ZOID = v_r.ZOID
   AND ( ZR.ZTOV IN (ZC_ZTOV_UPDATED, ZC_ZTOV_NOCHANGES) OR ( ZR.ZTOV = 0 AND
     NOT EXISTS (SELECT ZRID FROM QR.QRecord ZR2 WHERE
       ZR2.ZOID = ZR.ZOID AND ZR2.ZRID = ZR.ZRID AND ZR2.ZVER = v_ZO.ZVER) ) );
 IF v_recount >= 1 THEN
  v_es := SAM.make_execstatus(null,null,'E_2MANYRECORDS',
  'QR.QRecord','QR','1'); EXIT try;
 END IF;
 -- 4.99) Quality Control
 v_ps := QR.QC_QR(v_as,v_r); v_as := v_ps.a;
   IF NOT v_ps.success THEN v_es := v_ps.e; EXIT try; END IF;
 -- 5) add or update record
 v_r.ZRID := TIS.next_QRID(v_r.ZOID,v_r.ZVER);
 INSERT INTO QR.QRecord values(v_r.*);
 v_as := SAM.do_auditlog_da_sole(v_as,'2',null,null,v_r.ZSID,v_r.ZRID);
 -- 6) exit
 v_es := sam.make_execstatus(v_r.ZRID,v_r.ZVER,'I_SUCCESS');
EXCEPTION
 WHEN OTHERS THEN
	v_es:=sam.make_execstatus(v_r.ZRID,v_r.ZVER,'E_SQL',SQLSTATE,SQLERRM);
END;
-- FINALLY:
 PERFORM SAM.do_auditlog_exit(v_as,v_es);
 RETURN v_es;
END $$;
COMMIT TRANSACTION;
