-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: QR
-- OBJECT: Member (M) RECORD: MRange (MR)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION QR.QC_MR(
	v_as	SAM.auditstate,
	v_r	QR.MRange
) RETURNS SAM.procstate VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 i	integer;
 ZC_ZTOV_UPDATED CONSTANT bigint := -1;
 ZC_ZTOV_DELETED CONSTANT bigint := -2;
 ZC_ZTOV_NOCHANGES CONSTANT bigint := -3;
 ZC_ZTOV_RDELETED CONSTANT bigint := -4;
BEGIN
 v_ps.success := FALSE;
<<try>>
BEGIN
 -- 1) check NOT NULLs
 IF v_r.rstart IS NULL THEN v_ps.e := SAM.make_es_notnull(v_r.ZRID,v_r.ZVER,
	'QR.MRange','rstart'); EXIT try; END IF;
 IF v_r.rbitl IS NULL THEN v_ps.e := SAM.make_es_notnull(v_r.ZRID,v_r.ZVER,
	'QR.MRange','rbitl'); EXIT try; END IF;
 IF v_r.rtype IS NULL THEN v_ps.e := SAM.make_es_notnull(v_r.ZRID,v_r.ZVER,
	'QR.MRange','rtype'); EXIT try; END IF;
 IF v_r.status IS NULL THEN v_ps.e := SAM.make_es_notnull(v_r.ZRID,v_r.ZVER,
	'QR.MRange','status'); EXIT try; END IF;
 -- 2) check dictionary fields
 IF NOT dic.check_code('RANGE_TYPE',v_r.rtype) THEN
   v_ps.e := SAM.make_es_invalidcode(v_r.ZRID,v_r.ZVER,v_r.rtype,
   'QR.MRange.rtype','RANGE_TYPE'); EXIT try; END IF;
 IF NOT dic.check_code('QR_RANGE_STATUS',v_r.status) THEN
   v_ps.e := SAM.make_es_invalidcode(v_r.ZRID,v_r.ZVER,v_r.status,
   'QR.MRange.status','QR_RANGE_STATUS'); EXIT try; END IF;
 IF NOT dic.check_code_nula('QR_ACTION',v_r.action) THEN
   v_ps.e := SAM.make_es_invalidcode(v_r.ZRID,v_r.ZVER,v_r.action,
   'QR.MRange.action','QR_ACTION'); EXIT try; END IF;
 -- 3) check uniqueness
 -- 4) check references
 -- 4.1) check std parent/child references
  SELECT COUNT(*) INTO i FROM QR.MMember ZR WHERE ZR.ZOID = v_r.ZOID AND
   ZR.ZRID = v_r.ZPID AND ZR.ZTOV <= 0 AND NOT EXISTS (
    SELECT * FROM QR.MMember ZR2 WHERE ZR2.ZOID = v_r.ZOID AND ZR2.ZRID = v_r.ZPID
    AND ZR2.ZTOV IN (ZC_ZTOV_DELETED,ZC_ZTOV_RDELETED) );
  IF i = 0 THEN v_ps.e := SAM.make_execstatus(v_r.ZRID,v_r.ZVER,'E_NORECORD',
    'QR.MMember','ZRID',CAST(v_r.ZPID AS text)); EXIT try; END IF;
 -- 4.2) check scope references from data fields
 -- 99) all tests passed
 v_ps.success := TRUE;
END;
-- FINALLY:
 v_ps.a := v_as;
 RETURN v_ps;
END $$;
COMMIT TRANSACTION;
