-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: QR
-- OBJECT: QR (Q) RECORD: QRecord (QR)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION QR.cmp_QR(
	v_r	QR.QRecord,
	v_ro	QR.QRecord
) RETURNS boolean STABLE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
BEGIN
 IF v_r.QR <> v_ro.QR THEN RETURN FALSE; END IF;
  IF (v_r.QR IS NULL OR v_ro.QR IS NULL) AND
  NOT COALESCE(v_r.QR,v_ro.QR) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.action <> v_ro.action THEN RETURN FALSE; END IF;
  IF (v_r.action IS NULL OR v_ro.action IS NULL) AND
  NOT COALESCE(v_r.action,v_ro.action) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.redirect <> v_ro.redirect THEN RETURN FALSE; END IF;
  IF (v_r.redirect IS NULL OR v_ro.redirect IS NULL) AND
  NOT COALESCE(v_r.redirect,v_ro.redirect) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.obj_id <> v_ro.obj_id THEN RETURN FALSE; END IF;
  IF (v_r.obj_id IS NULL OR v_ro.obj_id IS NULL) AND
  NOT COALESCE(v_r.obj_id,v_ro.obj_id) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.cnum <> v_ro.cnum THEN RETURN FALSE; END IF;
  IF (v_r.cnum IS NULL OR v_ro.cnum IS NULL) AND
  NOT COALESCE(v_r.cnum,v_ro.cnum) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.cdate <> v_ro.cdate THEN RETURN FALSE; END IF;
  IF (v_r.cdate IS NULL OR v_ro.cdate IS NULL) AND
  NOT COALESCE(v_r.cdate,v_ro.cdate) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.price_gpl <> v_ro.price_gpl THEN RETURN FALSE; END IF;
  IF (v_r.price_gpl IS NULL OR v_ro.price_gpl IS NULL) AND
  NOT COALESCE(v_r.price_gpl,v_ro.price_gpl) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.price_vat <> v_ro.price_vat THEN RETURN FALSE; END IF;
  IF (v_r.price_vat IS NULL OR v_ro.price_vat IS NULL) AND
  NOT COALESCE(v_r.price_vat,v_ro.price_vat) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.support_id <> v_ro.support_id THEN RETURN FALSE; END IF;
  IF (v_r.support_id IS NULL OR v_ro.support_id IS NULL) AND
  NOT COALESCE(v_r.support_id,v_ro.support_id) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.prodtype <> v_ro.prodtype THEN RETURN FALSE; END IF;
  IF (v_r.prodtype IS NULL OR v_ro.prodtype IS NULL) AND
  NOT COALESCE(v_r.prodtype,v_ro.prodtype) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.prodmodel <> v_ro.prodmodel THEN RETURN FALSE; END IF;
  IF (v_r.prodmodel IS NULL OR v_ro.prodmodel IS NULL) AND
  NOT COALESCE(v_r.prodmodel,v_ro.prodmodel) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.pmrevision <> v_ro.pmrevision THEN RETURN FALSE; END IF;
  IF (v_r.pmrevision IS NULL OR v_ro.pmrevision IS NULL) AND
  NOT COALESCE(v_r.pmrevision,v_ro.pmrevision) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.sn <> v_ro.sn THEN RETURN FALSE; END IF;
  IF (v_r.sn IS NULL OR v_ro.sn IS NULL) AND
  NOT COALESCE(v_r.sn,v_ro.sn) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.prodate <> v_ro.prodate THEN RETURN FALSE; END IF;
  IF (v_r.prodate IS NULL OR v_ro.prodate IS NULL) AND
  NOT COALESCE(v_r.prodate,v_ro.prodate) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.GTD <> v_ro.GTD THEN RETURN FALSE; END IF;
  IF (v_r.GTD IS NULL OR v_ro.GTD IS NULL) AND
  NOT COALESCE(v_r.GTD,v_ro.GTD) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.saledate <> v_ro.saledate THEN RETURN FALSE; END IF;
  IF (v_r.saledate IS NULL OR v_ro.saledate IS NULL) AND
  NOT COALESCE(v_r.saledate,v_ro.saledate) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.sendate <> v_ro.sendate THEN RETURN FALSE; END IF;
  IF (v_r.sendate IS NULL OR v_ro.sendate IS NULL) AND
  NOT COALESCE(v_r.sendate,v_ro.sendate) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.wstart <> v_ro.wstart THEN RETURN FALSE; END IF;
  IF (v_r.wstart IS NULL OR v_ro.wstart IS NULL) AND
  NOT COALESCE(v_r.wstart,v_ro.wstart) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.wend <> v_ro.wend THEN RETURN FALSE; END IF;
  IF (v_r.wend IS NULL OR v_ro.wend IS NULL) AND
  NOT COALESCE(v_r.wend,v_ro.wend) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.gis_long <> v_ro.gis_long THEN RETURN FALSE; END IF;
  IF (v_r.gis_long IS NULL OR v_ro.gis_long IS NULL) AND
  NOT COALESCE(v_r.gis_long,v_ro.gis_long) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.gis_lat <> v_ro.gis_lat THEN RETURN FALSE; END IF;
  IF (v_r.gis_lat IS NULL OR v_ro.gis_lat IS NULL) AND
  NOT COALESCE(v_r.gis_lat,v_ro.gis_lat) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.gis_alt <> v_ro.gis_alt THEN RETURN FALSE; END IF;
  IF (v_r.gis_alt IS NULL OR v_ro.gis_alt IS NULL) AND
  NOT COALESCE(v_r.gis_alt,v_ro.gis_alt) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.comment <> v_ro.comment THEN RETURN FALSE; END IF;
  IF (v_r.comment IS NULL OR v_ro.comment IS NULL) AND
  NOT COALESCE(v_r.comment,v_ro.comment) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.csvcard <> v_ro.csvcard THEN RETURN FALSE; END IF;
  IF (v_r.csvcard IS NULL OR v_ro.csvcard IS NULL) AND
  NOT COALESCE(v_r.csvcard,v_ro.csvcard) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.owiki <> v_ro.owiki THEN RETURN FALSE; END IF;
  IF (v_r.owiki IS NULL OR v_ro.owiki IS NULL) AND
  NOT COALESCE(v_r.owiki,v_ro.owiki) IS NULL THEN RETURN FALSE; END IF;
 RETURN TRUE; -- both records equal
END $$;
COMMIT TRANSACTION;
