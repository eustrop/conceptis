-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: QR
-- OBJECT: Product (P) RECORD: PProduct (PP)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION QR.update_PProduct(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_prodtype	varchar(32),
	v_prodmodel	varchar(32),
	v_pmrevision	varchar(32),
	v_prodpart	varchar(32),
	v_dir_id	bigint,
	v_lang	varchar(3),
	v_title	varchar(255),
	v_owiki	varchar(10485760)
) RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 v_as SAM.auditstate;
 v_es SAM.execstatus;
 v_ZO TIS.ZObject%ROWTYPE;
 v_r QR.PProduct%ROWTYPE; -- this version
 v_ro QR.PProduct%ROWTYPE; -- previous version
 v_ri QR.PProduct%ROWTYPE; -- intermediate version
 v_user bigint;
 ZC_ZTOV_UPDATED CONSTANT bigint := -1;
 ZC_ZTOV_DELETED CONSTANT bigint := -2;
 ZC_ZTOV_NOCHANGES CONSTANT bigint := -3;
 ZC_ZTOV_RDELETED CONSTANT bigint := -4;
BEGIN
 -- 0) enter into procedure
 v_as.subsys := 'QR';
 v_as.eaction :='U';
 v_as.obj_type :='PP';
 v_as.ZID := v_ZRID;
 v_as.ZTYPE := 'QR.P';
 v_as.ZOID := v_ZOID;
 v_as.ZVER := v_ZVER;
 v_as.proc := 'update_PProduct';
 -- 0.1) get target ZO record and extract missings from
 SELECT * INTO v_ZO FROM TIS.ZObject as ZO WHERE
   ZO.ZOID = v_ZOID AND ZO.ZVER = v_ZVER AND ZO.ZTYPE = 'QR.P'
   FOR SHARE; -- lock row from concurrent updates 
 v_as.ZLVL := v_ZO.ZLVL; -- FOUND or NOT - indiscriminately
 v_as.sid := v_ZO.ZSID;
 v_as := SAM.do_auditlog_enter(v_as);
 -- 0.2) copy parameters to v_r
 v_r.ZOID = v_ZOID;
 v_r.ZVER = v_ZVER;
 v_r.ZRID = v_ZRID;
 v_r.prodtype := v_prodtype;
 v_r.prodmodel := v_prodmodel;
 v_r.pmrevision := v_pmrevision;
 v_r.prodpart := v_prodpart;
 v_r.dir_id := v_dir_id;
 v_r.lang := v_lang;
 v_r.title := v_title;
 v_r.owiki := v_owiki;
<<try>>
BEGIN
 -- 1) pre access control checks
 -- 1.1) check isolation level
 IF NOT SAM.check_isolation() THEN
    v_es := SAM.make_es_transisolation(null,null); EXIT try;
 END IF;
 -- 1.2) identify user
 IF SAM.get_user() IS NULL THEN
    v_es := sam.make_execstatus(null,null,'E_NOUSER',session_user); EXIT try;
 END IF;
 -- 1.3) unallowed nulls
 IF v_r.ZOID IS NULL THEN v_es := SAM.make_es_notnull(null,null,
	'update_PProduct','ZOID'); EXIT try; END IF;
 IF v_r.ZVER IS NULL THEN v_es := SAM.make_es_notnull(null,null,
	'update_PProduct','ZVER'); EXIT try; END IF;
 -- 1.4) check object's verion (ZO record)
 IF v_ZO.ZOID IS NULL THEN -- aka NOT FOUND after (0.1)
   v_es := SAM.make_execstatus(null,null,'E_INVALIDVERSION',
   'QR.P',CAST(v_ZOID AS text),CAST(v_ZVER AS text)); EXIT try; END IF;
 -- 1.5) check ZSTA
 IF v_ZO.ZSTA <> 'I' THEN
  v_es := SAM.make_execstatus(null,null,'E_NOTOPENED',
  '(Product,'||v_r.ZOID||','||v_r.ZVER||')'); EXIT try;
 END IF;
 -- 1.6) check ZUID
 IF v_ZO.ZUID <> SAM.get_user() THEN
  v_es := SAM.make_execstatus(null,null,'E_NOTOWNER',
  '(Product,'||v_r.ZOID||','||v_r.ZVER||')'); EXIT try;
 END IF;
 -- 2) check access
 IF v_ZO.ZVER = 1 THEN 
  v_ps := SAM.check_access_create(v_as,v_ZO.ZSID,v_ZO.ZTYPE,v_ZO.ZLVL);
 ELSE
  v_ps := SAM.check_access_write(v_as,v_ZO.ZSID,v_ZO.ZTYPE,v_ZO.ZOID,v_ZO.ZLVL);
 END IF;
 v_as := v_ps.a;
 -- 3) lock required tables
 -- LOCK TABLE QR.PProduct NOT REQUIRED;
 -- 4) check data
 -- 4.1) get requested record version
 -- 4.1.1) get intermediate version
 SELECT * INTO v_ri FROM QR.PProduct ZR WHERE ZR.ZOID = v_r.ZOID AND ZR.ZRID = v_r.ZRID
   AND ZR.ZVER = v_r.ZVER AND ZR.ZTOV < 0 FOR UPDATE;
 -- 4.1.2) get current version 
  SELECT * INTO v_ro FROM QR.PProduct ZR WHERE ZR.ZOID = v_r.ZOID
  AND ZR.ZRID = v_r.ZRID AND ZR.ZTOV = 0 FOR SHARE;
 -- abort if no record
 IF COALESCE(v_ri.ZOID, v_ro.ZOID) IS NULL OR
  v_ri.ZTOV IN (ZC_ZTOV_DELETED, ZC_ZTOV_RDELETED) THEN
    v_es := SAM.make_execstatus(null,null,'E_NORECORD',
    'QR.PProduct','ZRID',''||v_r.ZRID); EXIT try;
 END IF;
 -- 4.2) set record header
 v_r.ZSID := v_ZO.ZSID;
 v_r.ZLVL := v_ZO.ZLVL;
 v_r.ZPID := v_ro.ZPID;
 v_r.ZTOV := ZC_ZTOV_UPDATED;
 -- 4.99) Quality Control
 v_ps := QR.QC_PP(v_as,v_r); v_as := v_ps.a;
   IF NOT v_ps.success THEN v_es := v_ps.e; EXIT try; END IF;
 -- 5) add or update record
 IF v_ri.ZVER IS NULL THEN
  IF NOT QR.cmp_PP(v_r,v_ro) THEN
   INSERT INTO QR.PProduct values(v_r.*);
  END IF;
 ELSE
  IF QR.cmp_PP(v_r,v_ro) THEN v_r.ZTOV = ZC_ZTOV_NOCHANGES; END IF;
  UPDATE QR.PProduct AS ZR SET (ZTOV,prodtype,prodmodel,pmrevision,prodpart,dir_id,
	lang,title,owiki)
   = (v_r.ZTOV,v_r.prodtype,v_r.prodmodel,v_r.pmrevision,v_r.prodpart,v_r.dir_id,
	v_r.lang,v_r.title,v_r.owiki)
   WHERE ZR.ZOID = v_r.ZOID AND ZR.ZVER = v_r.ZVER AND ZR.ZRID = v_r.ZRID;
 END IF;
 v_as := SAM.do_auditlog_da_sole(v_as,'2',null,null,v_r.ZSID,v_r.ZRID);
 -- 6) exit
 v_es := sam.make_execstatus(v_r.ZRID,v_r.ZVER,'I_SUCCESS');
EXCEPTION
 WHEN OTHERS THEN
	v_es:=sam.make_execstatus(v_r.ZRID,v_r.ZVER,'E_SQL',SQLSTATE,SQLERRM);
END;
-- FINALLY:
 PERFORM SAM.do_auditlog_exit(v_as,v_es);
 RETURN v_es;
END $$;
COMMIT TRANSACTION;
