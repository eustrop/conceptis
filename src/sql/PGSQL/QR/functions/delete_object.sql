-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: QR

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen_subsys.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION QR.delete_object(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
) RETURNS SAM.procstate VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
BEGIN
  v_ps.success := TRUE;
  v_ps.a := v_as;
  IF v_ZO.ZTYPE = 'QR.M' THEN
   v_ps := QR.delete_M(v_as,v_ZO); v_as := v_ps.a;
  ELSIF v_ZO.ZTYPE = 'QR.P' THEN
   v_ps := QR.delete_P(v_as,v_ZO); v_as := v_ps.a;
  ELSIF v_ZO.ZTYPE = 'QR.C' THEN
   v_ps := QR.delete_C(v_as,v_ZO); v_as := v_ps.a;
  ELSIF v_ZO.ZTYPE = 'QR.Q' THEN
   v_ps := QR.delete_Q(v_as,v_ZO); v_as := v_ps.a;
  ELSE
   v_ps.e := SAM.make_execstatus(v_ZO.ZOID,v_ZO.ZVER,'E_INVALIDZTYPE',v_ZO.ZTYPE);
   v_ps.success := FALSE;
  END IF;
 IF NOT v_ps.success THEN RAISE EXCEPTION 'QR.delete rolled back'; END IF;
 RETURN v_ps;
EXCEPTION
  WHEN RAISE_EXCEPTION THEN
   RETURN v_ps;
END $$;
REVOKE ALL ON FUNCTION QR.delete_object(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
) FROM PUBLIC;
COMMIT TRANSACTION;
