-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: QR
-- OBJECT: Member (M) RECORD: MMember (MM)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION QR.cmp_MM(
	v_r	QR.MMember,
	v_ro	QR.MMember
) RETURNS boolean STABLE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
BEGIN
 IF v_r.code <> v_ro.code THEN RETURN FALSE; END IF;
  IF (v_r.code IS NULL OR v_ro.code IS NULL) AND
  NOT COALESCE(v_r.code,v_ro.code) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.org_id <> v_ro.org_id THEN RETURN FALSE; END IF;
  IF (v_r.org_id IS NULL OR v_ro.org_id IS NULL) AND
  NOT COALESCE(v_r.org_id,v_ro.org_id) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.scope_id <> v_ro.scope_id THEN RETURN FALSE; END IF;
  IF (v_r.scope_id IS NULL OR v_ro.scope_id IS NULL) AND
  NOT COALESCE(v_r.scope_id,v_ro.scope_id) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.status <> v_ro.status THEN RETURN FALSE; END IF;
  IF (v_r.status IS NULL OR v_ro.status IS NULL) AND
  NOT COALESCE(v_r.status,v_ro.status) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.lei_type <> v_ro.lei_type THEN RETURN FALSE; END IF;
  IF (v_r.lei_type IS NULL OR v_ro.lei_type IS NULL) AND
  NOT COALESCE(v_r.lei_type,v_ro.lei_type) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.lei <> v_ro.lei THEN RETURN FALSE; END IF;
  IF (v_r.lei IS NULL OR v_ro.lei IS NULL) AND
  NOT COALESCE(v_r.lei,v_ro.lei) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.let <> v_ro.let THEN RETURN FALSE; END IF;
  IF (v_r.let IS NULL OR v_ro.let IS NULL) AND
  NOT COALESCE(v_r.let,v_ro.let) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.name <> v_ro.name THEN RETURN FALSE; END IF;
  IF (v_r.name IS NULL OR v_ro.name IS NULL) AND
  NOT COALESCE(v_r.name,v_ro.name) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.addr <> v_ro.addr THEN RETURN FALSE; END IF;
  IF (v_r.addr IS NULL OR v_ro.addr IS NULL) AND
  NOT COALESCE(v_r.addr,v_ro.addr) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.site <> v_ro.site THEN RETURN FALSE; END IF;
  IF (v_r.site IS NULL OR v_ro.site IS NULL) AND
  NOT COALESCE(v_r.site,v_ro.site) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.phone <> v_ro.phone THEN RETURN FALSE; END IF;
  IF (v_r.phone IS NULL OR v_ro.phone IS NULL) AND
  NOT COALESCE(v_r.phone,v_ro.phone) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.email <> v_ro.email THEN RETURN FALSE; END IF;
  IF (v_r.email IS NULL OR v_ro.email IS NULL) AND
  NOT COALESCE(v_r.email,v_ro.email) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.descr <> v_ro.descr THEN RETURN FALSE; END IF;
  IF (v_r.descr IS NULL OR v_ro.descr IS NULL) AND
  NOT COALESCE(v_r.descr,v_ro.descr) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.owiki <> v_ro.owiki THEN RETURN FALSE; END IF;
  IF (v_r.owiki IS NULL OR v_ro.owiki IS NULL) AND
  NOT COALESCE(v_r.owiki,v_ro.owiki) IS NULL THEN RETURN FALSE; END IF;
 RETURN TRUE; -- both records equal
END $$;
COMMIT TRANSACTION;
