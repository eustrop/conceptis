-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: QR
-- OBJECT: Card (C) RECORD: CCard (CC)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION QR.move_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
) RETURNS SAM.procstate VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
BEGIN
  -- copy existing records with new ZSID & ZLVL
  INSERT INTO QR.CCard SELECT
      ZOID,ZRID,v_ZO.ZVER,0,v_ZO.ZSID,v_ZO.ZLVL,ZPID,
      QR,autoexport,pubqr_id,cardate,cnum_id,cnum,
      cdate,cmoney,cmoney_cur,cmoney_vat,cmoney_desc,supplier_id,
      supplier,client_id,client,claddr,prodtype,prodmodel,
      pmrevision,sn,prodate,GTD,saledate,sendate,
      wstart,wend,gis_long,gis_lat,gis_alt,comment,
      csvcard
    FROM QR.CCard ZR WHERE ZR.ZTOV = 0 AND ZR.ZOID = v_ZO.ZOID;
  -- obsolete existing records
  UPDATE QR.CCard ZR SET ZTOV = v_ZO.ZVER WHERE ZR.ZTOV = 0 AND
   ZR.ZOID = v_ZO.ZOID AND ZR.ZVER <> v_ZO.ZVER;
-- FINALLY:
 v_ps.success := TRUE;
 v_ps.a := v_as;
 RETURN v_ps;
END $$;
COMMIT TRANSACTION;
