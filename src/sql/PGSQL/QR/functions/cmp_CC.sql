-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: QR
-- OBJECT: Card (C) RECORD: CCard (CC)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION QR.cmp_CC(
	v_r	QR.CCard,
	v_ro	QR.CCard
) RETURNS boolean STABLE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
BEGIN
 IF v_r.QR <> v_ro.QR THEN RETURN FALSE; END IF;
  IF (v_r.QR IS NULL OR v_ro.QR IS NULL) AND
  NOT COALESCE(v_r.QR,v_ro.QR) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.autoexport <> v_ro.autoexport THEN RETURN FALSE; END IF;
  IF (v_r.autoexport IS NULL OR v_ro.autoexport IS NULL) AND
  NOT COALESCE(v_r.autoexport,v_ro.autoexport) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.pubqr_id <> v_ro.pubqr_id THEN RETURN FALSE; END IF;
  IF (v_r.pubqr_id IS NULL OR v_ro.pubqr_id IS NULL) AND
  NOT COALESCE(v_r.pubqr_id,v_ro.pubqr_id) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.cardate <> v_ro.cardate THEN RETURN FALSE; END IF;
  IF (v_r.cardate IS NULL OR v_ro.cardate IS NULL) AND
  NOT COALESCE(v_r.cardate,v_ro.cardate) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.cnum_id <> v_ro.cnum_id THEN RETURN FALSE; END IF;
  IF (v_r.cnum_id IS NULL OR v_ro.cnum_id IS NULL) AND
  NOT COALESCE(v_r.cnum_id,v_ro.cnum_id) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.cnum <> v_ro.cnum THEN RETURN FALSE; END IF;
  IF (v_r.cnum IS NULL OR v_ro.cnum IS NULL) AND
  NOT COALESCE(v_r.cnum,v_ro.cnum) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.cdate <> v_ro.cdate THEN RETURN FALSE; END IF;
  IF (v_r.cdate IS NULL OR v_ro.cdate IS NULL) AND
  NOT COALESCE(v_r.cdate,v_ro.cdate) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.cmoney <> v_ro.cmoney THEN RETURN FALSE; END IF;
  IF (v_r.cmoney IS NULL OR v_ro.cmoney IS NULL) AND
  NOT COALESCE(v_r.cmoney,v_ro.cmoney) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.cmoney_cur <> v_ro.cmoney_cur THEN RETURN FALSE; END IF;
  IF (v_r.cmoney_cur IS NULL OR v_ro.cmoney_cur IS NULL) AND
  NOT COALESCE(v_r.cmoney_cur,v_ro.cmoney_cur) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.cmoney_vat <> v_ro.cmoney_vat THEN RETURN FALSE; END IF;
  IF (v_r.cmoney_vat IS NULL OR v_ro.cmoney_vat IS NULL) AND
  NOT COALESCE(v_r.cmoney_vat,v_ro.cmoney_vat) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.cmoney_desc <> v_ro.cmoney_desc THEN RETURN FALSE; END IF;
  IF (v_r.cmoney_desc IS NULL OR v_ro.cmoney_desc IS NULL) AND
  NOT COALESCE(v_r.cmoney_desc,v_ro.cmoney_desc) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.supplier_id <> v_ro.supplier_id THEN RETURN FALSE; END IF;
  IF (v_r.supplier_id IS NULL OR v_ro.supplier_id IS NULL) AND
  NOT COALESCE(v_r.supplier_id,v_ro.supplier_id) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.supplier <> v_ro.supplier THEN RETURN FALSE; END IF;
  IF (v_r.supplier IS NULL OR v_ro.supplier IS NULL) AND
  NOT COALESCE(v_r.supplier,v_ro.supplier) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.client_id <> v_ro.client_id THEN RETURN FALSE; END IF;
  IF (v_r.client_id IS NULL OR v_ro.client_id IS NULL) AND
  NOT COALESCE(v_r.client_id,v_ro.client_id) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.client <> v_ro.client THEN RETURN FALSE; END IF;
  IF (v_r.client IS NULL OR v_ro.client IS NULL) AND
  NOT COALESCE(v_r.client,v_ro.client) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.claddr <> v_ro.claddr THEN RETURN FALSE; END IF;
  IF (v_r.claddr IS NULL OR v_ro.claddr IS NULL) AND
  NOT COALESCE(v_r.claddr,v_ro.claddr) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.prodtype <> v_ro.prodtype THEN RETURN FALSE; END IF;
  IF (v_r.prodtype IS NULL OR v_ro.prodtype IS NULL) AND
  NOT COALESCE(v_r.prodtype,v_ro.prodtype) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.prodmodel <> v_ro.prodmodel THEN RETURN FALSE; END IF;
  IF (v_r.prodmodel IS NULL OR v_ro.prodmodel IS NULL) AND
  NOT COALESCE(v_r.prodmodel,v_ro.prodmodel) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.pmrevision <> v_ro.pmrevision THEN RETURN FALSE; END IF;
  IF (v_r.pmrevision IS NULL OR v_ro.pmrevision IS NULL) AND
  NOT COALESCE(v_r.pmrevision,v_ro.pmrevision) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.sn <> v_ro.sn THEN RETURN FALSE; END IF;
  IF (v_r.sn IS NULL OR v_ro.sn IS NULL) AND
  NOT COALESCE(v_r.sn,v_ro.sn) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.prodate <> v_ro.prodate THEN RETURN FALSE; END IF;
  IF (v_r.prodate IS NULL OR v_ro.prodate IS NULL) AND
  NOT COALESCE(v_r.prodate,v_ro.prodate) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.GTD <> v_ro.GTD THEN RETURN FALSE; END IF;
  IF (v_r.GTD IS NULL OR v_ro.GTD IS NULL) AND
  NOT COALESCE(v_r.GTD,v_ro.GTD) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.saledate <> v_ro.saledate THEN RETURN FALSE; END IF;
  IF (v_r.saledate IS NULL OR v_ro.saledate IS NULL) AND
  NOT COALESCE(v_r.saledate,v_ro.saledate) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.sendate <> v_ro.sendate THEN RETURN FALSE; END IF;
  IF (v_r.sendate IS NULL OR v_ro.sendate IS NULL) AND
  NOT COALESCE(v_r.sendate,v_ro.sendate) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.wstart <> v_ro.wstart THEN RETURN FALSE; END IF;
  IF (v_r.wstart IS NULL OR v_ro.wstart IS NULL) AND
  NOT COALESCE(v_r.wstart,v_ro.wstart) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.wend <> v_ro.wend THEN RETURN FALSE; END IF;
  IF (v_r.wend IS NULL OR v_ro.wend IS NULL) AND
  NOT COALESCE(v_r.wend,v_ro.wend) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.gis_long <> v_ro.gis_long THEN RETURN FALSE; END IF;
  IF (v_r.gis_long IS NULL OR v_ro.gis_long IS NULL) AND
  NOT COALESCE(v_r.gis_long,v_ro.gis_long) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.gis_lat <> v_ro.gis_lat THEN RETURN FALSE; END IF;
  IF (v_r.gis_lat IS NULL OR v_ro.gis_lat IS NULL) AND
  NOT COALESCE(v_r.gis_lat,v_ro.gis_lat) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.gis_alt <> v_ro.gis_alt THEN RETURN FALSE; END IF;
  IF (v_r.gis_alt IS NULL OR v_ro.gis_alt IS NULL) AND
  NOT COALESCE(v_r.gis_alt,v_ro.gis_alt) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.comment <> v_ro.comment THEN RETURN FALSE; END IF;
  IF (v_r.comment IS NULL OR v_ro.comment IS NULL) AND
  NOT COALESCE(v_r.comment,v_ro.comment) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.csvcard <> v_ro.csvcard THEN RETURN FALSE; END IF;
  IF (v_r.csvcard IS NULL OR v_ro.csvcard IS NULL) AND
  NOT COALESCE(v_r.csvcard,v_ro.csvcard) IS NULL THEN RETURN FALSE; END IF;
 RETURN TRUE; -- both records equal
END $$;
COMMIT TRANSACTION;
