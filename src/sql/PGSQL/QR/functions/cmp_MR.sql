-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: QR
-- OBJECT: Member (M) RECORD: MRange (MR)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION QR.cmp_MR(
	v_r	QR.MRange,
	v_ro	QR.MRange
) RETURNS boolean STABLE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
BEGIN
 IF v_r.rstart <> v_ro.rstart THEN RETURN FALSE; END IF;
  IF (v_r.rstart IS NULL OR v_ro.rstart IS NULL) AND
  NOT COALESCE(v_r.rstart,v_ro.rstart) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.rbitl <> v_ro.rbitl THEN RETURN FALSE; END IF;
  IF (v_r.rbitl IS NULL OR v_ro.rbitl IS NULL) AND
  NOT COALESCE(v_r.rbitl,v_ro.rbitl) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.rtype <> v_ro.rtype THEN RETURN FALSE; END IF;
  IF (v_r.rtype IS NULL OR v_ro.rtype IS NULL) AND
  NOT COALESCE(v_r.rtype,v_ro.rtype) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.status <> v_ro.status THEN RETURN FALSE; END IF;
  IF (v_r.status IS NULL OR v_ro.status IS NULL) AND
  NOT COALESCE(v_r.status,v_ro.status) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.action <> v_ro.action THEN RETURN FALSE; END IF;
  IF (v_r.action IS NULL OR v_ro.action IS NULL) AND
  NOT COALESCE(v_r.action,v_ro.action) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.redirect <> v_ro.redirect THEN RETURN FALSE; END IF;
  IF (v_r.redirect IS NULL OR v_ro.redirect IS NULL) AND
  NOT COALESCE(v_r.redirect,v_ro.redirect) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.alloc <> v_ro.alloc THEN RETURN FALSE; END IF;
  IF (v_r.alloc IS NULL OR v_ro.alloc IS NULL) AND
  NOT COALESCE(v_r.alloc,v_ro.alloc) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.member_id <> v_ro.member_id THEN RETURN FALSE; END IF;
  IF (v_r.member_id IS NULL OR v_ro.member_id IS NULL) AND
  NOT COALESCE(v_r.member_id,v_ro.member_id) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.doc_id <> v_ro.doc_id THEN RETURN FALSE; END IF;
  IF (v_r.doc_id IS NULL OR v_ro.doc_id IS NULL) AND
  NOT COALESCE(v_r.doc_id,v_ro.doc_id) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.descr <> v_ro.descr THEN RETURN FALSE; END IF;
  IF (v_r.descr IS NULL OR v_ro.descr IS NULL) AND
  NOT COALESCE(v_r.descr,v_ro.descr) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.owiki <> v_ro.owiki THEN RETURN FALSE; END IF;
  IF (v_r.owiki IS NULL OR v_ro.owiki IS NULL) AND
  NOT COALESCE(v_r.owiki,v_ro.owiki) IS NULL THEN RETURN FALSE; END IF;
 RETURN TRUE; -- both records equal
END $$;
COMMIT TRANSACTION;
