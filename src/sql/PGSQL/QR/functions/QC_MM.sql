-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: QR
-- OBJECT: Member (M) RECORD: MMember (MM)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION QR.QC_MM(
	v_as	SAM.auditstate,
	v_r	QR.MMember
) RETURNS SAM.procstate VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 i	integer;
 ZC_ZTOV_UPDATED CONSTANT bigint := -1;
 ZC_ZTOV_DELETED CONSTANT bigint := -2;
 ZC_ZTOV_NOCHANGES CONSTANT bigint := -3;
 ZC_ZTOV_RDELETED CONSTANT bigint := -4;
BEGIN
 v_ps.success := FALSE;
<<try>>
BEGIN
 -- 1) check NOT NULLs
 IF v_r.code IS NULL THEN v_ps.e := SAM.make_es_notnull(v_r.ZRID,v_r.ZVER,
	'QR.MMember','code'); EXIT try; END IF;
 IF v_r.status IS NULL THEN v_ps.e := SAM.make_es_notnull(v_r.ZRID,v_r.ZVER,
	'QR.MMember','status'); EXIT try; END IF;
 IF v_r.lei_type IS NULL THEN v_ps.e := SAM.make_es_notnull(v_r.ZRID,v_r.ZVER,
	'QR.MMember','lei_type'); EXIT try; END IF;
 -- 2) check dictionary fields
 IF NOT dic.check_code('QR_MEMBER_STATUS',v_r.status) THEN
   v_ps.e := SAM.make_es_invalidcode(v_r.ZRID,v_r.ZVER,v_r.status,
   'QR.MMember.status','QR_MEMBER_STATUS'); EXIT try; END IF;
 IF NOT dic.check_code('LEI_TYPE',v_r.lei_type) THEN
   v_ps.e := SAM.make_es_invalidcode(v_r.ZRID,v_r.ZVER,v_r.lei_type,
   'QR.MMember.lei_type','LEI_TYPE'); EXIT try; END IF;
 IF NOT dic.check_code_nula('LET_TYPE',v_r.let) THEN
   v_ps.e := SAM.make_es_invalidcode(v_r.ZRID,v_r.ZVER,v_r.let,
   'QR.MMember.let','LET_TYPE'); EXIT try; END IF;
 -- 3) check uniqueness
  IF TIS.check_ZNAME('M',v_r.ZOID,v_r.code) THEN
  v_ps.e := SAM.make_execstatus(null,null,'E_DUPRECORD',
  'QR.MMember','ZNAME',v_r.code); EXIT try;
  END IF;
 -- 4) check references
 -- 4.1) check std parent/child references
   -- not required
 -- 4.2) check scope references from data fields
  IF v_r.scope_id <> 0 THEN
   SELECT COUNT(*) INTO i FROM SAM.Scope WHERE id = v_r.scope_id;
   IF i = 0 THEN v_ps.e := SAM.make_execstatus(v_r.ZRID,v_r.ZVER,'E_NOSCOPE',
	CAST(v_r.scope_id AS text)); EXIT try; END IF;
  END IF;
 -- 99) all tests passed
 v_ps.success := TRUE;
END;
-- FINALLY:
 v_ps.a := v_as;
 RETURN v_ps;
END $$;
COMMIT TRANSACTION;
