-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: QR
-- OBJECT: QR (Q) RECORD: QRecord (QR)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION QR.commit_QR(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
) RETURNS SAM.procstate VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 v_r  QR.QRecord%ROWTYPE; -- for FQC
 v_recount bigint;
 ZC_ZTOV_UPDATED CONSTANT bigint := -1;
 ZC_ZTOV_DELETED CONSTANT bigint := -2;
 ZC_ZTOV_NOCHANGES CONSTANT bigint := -3;
 ZC_ZTOV_RDELETED CONSTANT bigint := -4;
BEGIN
 v_ps.success := FALSE;
<<try>>
BEGIN
 -- 1) make FQC (final QC)
 -- 1.1) check NOT NULLs
  -- not necessary
 -- 1.2) check dictionary fields
  -- may be in the future...
 -- 1.3) check uniqueness
 -- 1.4) check references
  -- may be in the future...
 -- 1.5) check MAXREC and MINREC
 -- 1.5.1) get count(*) for active records in the object
 SELECT count(*) INTO v_recount FROM QR.QRecord ZR WHERE ZR.ZOID = v_ZO.ZOID
   AND (ZR.ZTOV IN (ZC_ZTOV_UPDATED, ZC_ZTOV_NOCHANGES) OR ( ZR.ZTOV = 0 AND
     NOT EXISTS (SELECT ZRID FROM QR.QRecord ZR2 WHERE
       ZR2.ZOID = ZR.ZOID AND ZR2.ZRID = ZR.ZRID AND ZR2.ZVER = v_ZO.ZVER) ) );
 IF v_recount > 1 THEN
  v_ps.e := SAM.make_execstatus(null,null,'E_2MANYRECORDS',
  'QR.QRecord','QR','1'); EXIT try;
 END IF;
 IF v_recount < 1 THEN
  v_ps.e := SAM.make_execstatus(null,null,'E_NOTENOUGHRECS',
  'QR.QRecord','QR','1'); EXIT try;
 END IF;
 -- 1.6) check ZNAME
 -- 1.7) doing final quality control manually (FQCM)
  -- may be in the future...
 -- 2) update table
  UPDATE QR.QRecord ZR SET ZTOV = v_ZO.ZVER WHERE ZR.ZTOV = 0 AND ZR.ZOID = v_ZO.ZOID AND EXISTS
    (SELECT ZRID FROM QR.QRecord ZR2 WHERE 
      ZR2.ZTOV IN (ZC_ZTOV_UPDATED, ZC_ZTOV_DELETED, ZC_ZTOV_RDELETED) AND
      ZR2.ZOID = ZR.ZOID AND ZR2.ZRID = ZR.ZRID AND ZR2.ZVER = v_ZO.ZVER);
  UPDATE QR.QRecord ZR SET ZTOV = 0 WHERE ZTOV = ZC_ZTOV_UPDATED AND ZR.ZOID = v_ZO.ZOID
                                  AND ZR.ZVER = v_ZO.ZVER;
  DELETE FROM QR.QRecord ZR WHERE ZR.ZTOV < 0 AND ZR.ZOID = v_ZO.ZOID AND ZR.ZVER = v_ZO.ZVER;
 -- 3) all tests passed
 v_ps.success := TRUE;
END;
-- FINALLY:
 v_ps.a := v_as;
 RETURN v_ps;
END $$;
COMMIT TRANSACTION;
