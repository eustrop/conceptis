-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: QR
-- OBJECT: Product (P) RECORD: PProduct (PP)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION QR.cmp_PP(
	v_r	QR.PProduct,
	v_ro	QR.PProduct
) RETURNS boolean STABLE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
BEGIN
 IF v_r.prodtype <> v_ro.prodtype THEN RETURN FALSE; END IF;
  IF (v_r.prodtype IS NULL OR v_ro.prodtype IS NULL) AND
  NOT COALESCE(v_r.prodtype,v_ro.prodtype) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.prodmodel <> v_ro.prodmodel THEN RETURN FALSE; END IF;
  IF (v_r.prodmodel IS NULL OR v_ro.prodmodel IS NULL) AND
  NOT COALESCE(v_r.prodmodel,v_ro.prodmodel) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.pmrevision <> v_ro.pmrevision THEN RETURN FALSE; END IF;
  IF (v_r.pmrevision IS NULL OR v_ro.pmrevision IS NULL) AND
  NOT COALESCE(v_r.pmrevision,v_ro.pmrevision) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.prodpart <> v_ro.prodpart THEN RETURN FALSE; END IF;
  IF (v_r.prodpart IS NULL OR v_ro.prodpart IS NULL) AND
  NOT COALESCE(v_r.prodpart,v_ro.prodpart) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.dir_id <> v_ro.dir_id THEN RETURN FALSE; END IF;
  IF (v_r.dir_id IS NULL OR v_ro.dir_id IS NULL) AND
  NOT COALESCE(v_r.dir_id,v_ro.dir_id) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.lang <> v_ro.lang THEN RETURN FALSE; END IF;
  IF (v_r.lang IS NULL OR v_ro.lang IS NULL) AND
  NOT COALESCE(v_r.lang,v_ro.lang) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.title <> v_ro.title THEN RETURN FALSE; END IF;
  IF (v_r.title IS NULL OR v_ro.title IS NULL) AND
  NOT COALESCE(v_r.title,v_ro.title) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.owiki <> v_ro.owiki THEN RETURN FALSE; END IF;
  IF (v_r.owiki IS NULL OR v_ro.owiki IS NULL) AND
  NOT COALESCE(v_r.owiki,v_ro.owiki) IS NULL THEN RETURN FALSE; END IF;
 RETURN TRUE; -- both records equal
END $$;
COMMIT TRANSACTION;
