-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: QR
-- OBJECT: Member (M)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen_ot.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION QR.commit_M(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
) RETURNS SAM.procstate VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 i    integer;
 v_ZPID    bigint; -- for parent/child checks
 v_ZRID    bigint; 
BEGIN
 v_ps.success := FALSE;
 v_ps.a := v_as;
<<try>>
BEGIN
 -- 1) per table commit with FQC
 v_ps := QR.commit_MM(v_as,v_ZO);
  IF NOT v_ps.success THEN EXIT try; END IF;
 v_ps := QR.commit_MR(v_as,v_ZO);
  IF NOT v_ps.success THEN EXIT try; END IF;
 -- 2) global FQC
  SELECT ZPID,ZRID INTO v_ZPID,v_ZRID FROM QR.MRange ZR WHERE ZOID = v_ZO.ZOID
   AND ZTOV = 0 AND NOT EXISTS ( SELECT * FROM QR.MMember ZP WHERE
    ZP.ZOID = ZR.ZOID AND ZP.ZRID = ZR.ZPID AND ZP.ZTOV = 0);
   IF FOUND THEN
    v_ps.success = FALSE;
    v_ps.e := SAM.make_execstatus(v_ZO.ZOID,v_ZO.ZVER,'E_INVALIDPARENT',
    CAST(v_ZPID AS text),'QR.MRange',CAST(v_ZRID AS text)); EXIT try;
   END IF;
 -- 3) all tests passed
 v_ps.success := TRUE;
END;
-- FINALLY:
 RETURN v_ps;
END $$;
REVOKE ALL ON FUNCTION QR.commit_M(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
COMMIT TRANSACTION;
