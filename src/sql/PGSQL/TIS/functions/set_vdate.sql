-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE at the project's root directory
--
-- $Id$
--

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION TIS.set_vdate(
	v_ZDATO	timestamp,
	v_force	char(1) -- 'Y' for ignore existing value
) RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 -- v_ps SAM.procstate;
 v_as SAM.auditstate;
 v_es SAM.execstatus;
 v_r TIS.ZDateView%ROWTYPE;
BEGIN
 -- 0) enter into procedure
 v_as.subsys := 'TIS';
 v_as.eaction :='M';
 v_as.obj_type := 'Z';
 v_as.sid := null;
 -- v_as.ZID := v_id;
 v_as.ZTYPE := null;
 v_as.ZLVL := null;
 v_as.proc := 'set_vdate';
 v_as := SAM.do_auditlog_enter(v_as);
<<try>>
BEGIN
 -- 1) pre access control checks
 -- 1.1) check transaction isolation level
 IF NOT SAM.check_isolation() THEN
    v_es := SAM.make_es_transisolation(null,null); EXIT try;
 END IF;
 -- 1.2) check identified user
 IF SAM.get_user() IS NULL THEN
    v_es := sam.make_execstatus(null,null,'E_NOUSER',session_user); EXIT try;
 END IF;
 -- 1.3) unallowed nulls
  -- none
-- 2) access control
  -- none
-- 3) lock required tables
 SELECT * INTO v_r FROM TIS.ZDateView WHERE uid = sam.get_user() FOR UPDATE;
-- 4) check data
IF FOUND THEN
 IF v_force = 'Y' OR v_ZDATO IS NULL OR v_r.ZDATO IS NULL THEN
  -- 4.1) update record
  UPDATE TIS.ZDateView SET ts = NOW(), ZDATO = v_ZDATO
  WHERE uid = sam.get_user();
 ELSE 
  -- 4.2) raise error (inform user about existing settings)
  v_es := sam.make_execstatus(null,null,'E_DUPRECORD',
     'TIS.ZDateView','ZDATO',CAST(v_r.ZDATO AS text));
  EXIT try;
 END IF;
ELSE
 -- 5) add record
 v_r.uid := sam.get_user();
 v_r.ts := NOW();
 v_r.ZDATO := v_ZDATO;
 INSERT INTO TIS.ZDateView values(v_r.*);
END IF;
 -- 6) exit
 v_es := sam.make_execstatus(null,null,'I_SUCCESS');
EXCEPTION
 WHEN OTHERS THEN
	v_es:=sam.make_execstatus(null,null,'E_SQL',SQLSTATE,SQLERRM);
END;
-- FINALLY:
 PERFORM SAM.do_auditlog_exit(v_as,v_es);
 RETURN v_es;
END $$;
COMMIT TRANSACTION;
