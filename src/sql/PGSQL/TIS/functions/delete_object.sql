-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE at the project's root directory
--
-- $Id$
--

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION TIS.delete_object(
	v_ZTYPE	varchar(14),
	v_ZOID	bigint,
	v_ZVER	bigint
) RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
  -- v_ZTYPE,v_ZOID - mandatory parameters
  -- v_ZVER - optionl, used for deletening object only if v_ZVER is last version.
DECLARE
 v_ps SAM.procstate;
 v_as SAM.auditstate;
 v_es SAM.execstatus;
 v_r TIS.ZObject%ROWTYPE; -- new version of the data object
 v_ro TIS.ZObject%ROWTYPE; -- current version of the data object
 v_XS SAM.Scope%ROWTYPE;
 v_XSP SAM.ScopePolicy%ROWTYPE;
BEGIN
 -- 0) enter into procedure
 v_as.subsys := 'TIS';
 v_as.eaction :='OD';
 v_as.obj_type :=COALESCE(v_ZTYPE,'ZO');
 v_as.sid := null; -- unknown
 v_as.ZID := null;
 v_as.ZOID := v_ZOID;
 v_as.ZVER := v_ZVER; -- requested version. will changed later
 v_as.ZTYPE := v_ZTYPE;
 v_as.ZLVL := null; -- unknown
 v_as.proc := 'delete_object';
 v_as := SAM.do_auditlog_enter(v_as);
 -- 0.1) copy parameters to v_r
 v_r.ZOID := v_ZOID; -- obtain later
 -- v_r.ZVER := v_ZVER; -- obtain late as v_ro.ZVER + 1
 v_r.ZTYPE := v_ZTYPE;
 v_r.ZSID := null; -- unknown
 v_r.ZLVL := null; -- unknown
 v_r.ZUID := SAM.get_user(); -- identify user
 v_r.ZSTA := 'D'; -- deletion version 
 v_r.ZDATE := NOW(); -- current time for new object version
 v_r.ZDATO := NULL; -- actual (latest) version of object
<<try>>
BEGIN
 -- 1) pre access control checks
 -- 1.1) check transaction isolation level
 IF NOT SAM.check_isolation() THEN
    v_es := SAM.make_es_transisolation(null,null); EXIT try;
 END IF;
 -- 1.2) check identified user
 IF SAM.get_user() IS NULL THEN
    v_es := sam.make_execstatus(null,null,'E_NOUSER',session_user); EXIT try;
 END IF;
 -- 1.3) unallowed nulls
 IF v_r.ZTYPE IS NULL THEN v_es := SAM.make_es_notnull(null,null,
	'delete_object','ZTYPE'); EXIT try; END IF;
 IF v_r.ZOID IS NULL THEN v_es := SAM.make_es_notnull(null,null,
	'delete_object','ZOID'); EXIT try; END IF;
-- 2) access control
-- 2.1) get ZObject record
SELECT * INTO v_ro FROM TIS.ZObject ZO WHERE ZO.ZOID = v_r.ZOID AND
	ZO.ZTYPE = v_r.ZTYPE AND ZSTA = 'N' FOR UPDATE;
IF NOT FOUND THEN
   v_es := SAM.make_execstatus(null,null,'E_NOBJECT',
   v_ZTYPE,'ZOID',CAST(v_ZOID AS text)); EXIT try; END IF;
 -- 2.1.1) set unknown auditstate fields
 v_as.sid := v_ro.ZSID; v_as.ZLVL := v_ro.ZLVL;
-- 2.2) get unknown object's properties:
v_r.ZSID := v_ro.ZSID;
v_r.ZLVL := v_ro.ZLVL;
v_r.ZVER := v_ro.ZVER + 1;
v_r.QRSQ := v_ro.QRSQ;
-- 2.3) check ZVER if present
IF v_ro.ZVER <> v_ZVER AND NOT v_ZVER IS NULL  THEN
   v_es := SAM.make_execstatus(v_ZOID,v_ZVER,'E_OBJECTCHANED','('||v_ZTYPE||
   ','||CAST(v_ZOID AS text)||')',CAST(v_ZVER AS text),
   CAST(v_ro.ZVER AS text)); EXIT try; END IF;
v_as.ZVER := v_r.ZVER;
-- 2.4) check locking
PERFORM ZOID FROM TIS.ZObject ZO WHERE ZO.ZOID = v_r.ZOID AND
	ZO.ZTYPE = v_r.ZTYPE AND ZVER = v_r.ZVER; 
IF FOUND THEN
   v_es := SAM.make_execstatus(null,null,'E_OBJECTLOCKED'); EXIT try;
END IF;
-- 2.99) check access
v_ps := SAM.check_access_delete(v_as,v_r.ZSID,v_r.ZTYPE,v_r.ZOID,v_r.ZLVL);
v_as := v_ps.a;
IF NOT v_ps.success THEN v_es := v_ps.e; EXIT try; END IF;
-- 3) lock required tables
-- NO LOCK TABLE TIS.ZObject IN EXCLUSIVE MODE; -- updating/inserting record
LOCK TABLE SAM.Scope IN SHARE MODE;	-- for looking at chpts
-- NO LOCK TABLE SAM.ScopePolicy IN SHARE MODE; -- for maxcount/ocount
-- 4) check data
-- 4.0) check ZDATE
IF v_ro.ZDATE >= v_r.ZDATE THEN
  v_es := SAM.make_execstatus(null,null,'E_INVALIDVERDATE',
  CAST(v_ro.ZDATE as text)); EXIT try; END IF;
-- 4.1) System object types
IF v_r.ZTYPE IN ('X','Y','Z') THEN
 v_es := sam.make_execstatus(null,null,'E_NASYSZTYPE',v_r.ZTYPE);
 EXIT try; END IF;
-- 4.2) unknown codes
IF NOT dic.check_code('TIS_OBJECT_TYPE',v_r.ZTYPE) THEN
  v_es := SAM.make_execstatus(null,null,'E_INVALIDZTYPE',v_r.ZTYPE);
  EXIT try; END IF;
-- 4.3) check scope
SELECT * INTO v_XS FROM SAM.Scope XS WHERE XS.id = v_r.ZSID;
IF NOT FOUND THEN
   v_es := SAM.make_execstatus(null,null,'E_NOSCOPE',
   CAST(v_r.ZSID AS text)); EXIT try; END IF;
 -- 4.3.1) check scope chpts
IF NOT (v_XS.chpts IS NULL) THEN
 IF v_XS.chpts >= v_r.ZDATE THEN
   v_es := SAM.make_execstatus(null,null,'E_INVALIDCHPTS',
   CAST(v_r.ZSID AS text),
   CAST(v_r.ZDATE AS text), CAST(v_XS.chpts AS text)); EXIT try; END IF;
END IF;
 -- 4.3.2) scope must be local
 IF v_XS.is_local <> 'Y' OR v_XS.is_local IS NULL THEN
   v_es := SAM.make_execstatus(null,null,'E_NONLOCALSCOPE',
   CAST(v_r.ZSID AS text)); EXIT try; END IF;
-- 4.4) check ScopePolicy for allowed object type
SELECT * INTO v_XSP FROM SAM.ScopePolicy XSP WHERE
	XSP.sid = v_r.ZSID AND XSP.obj_type = v_r.ZTYPE;
IF NOT FOUND THEN
   v_es := SAM.make_execstatus(null,null,'E_NAZTYPE4SCOPE', v_r.ZTYPE,
   CAST(v_r.ZSID AS text)); EXIT try; END IF;
-- 5) delete object
 -- 5.1) object specific delete
  IF v_r.ZTYPE LIKE 'TISC.%' THEN
   v_ps := tisc.delete_object(v_as,v_r);
  ELSIF v_r.ZTYPE LIKE 'FS.%' THEN
   v_ps := FS.delete_object(v_as,v_r);
  ELSIF v_r.ZTYPE LIKE 'MSG.%' THEN
   v_ps := MSG.delete_object(v_as,v_r);
  ELSIF v_r.ZTYPE LIKE 'QR.%' THEN
   v_ps := QR.delete_object(v_as,v_r);
  ELSIF v_r.ZTYPE LIKE 'COP.%' THEN
   v_ps := COP.delete_object(v_as,v_r);
  ELSIF v_r.ZTYPE = 'TIS.Q' THEN
   update TIS.QSeq SET QTOV=v_r.ZVER where QOID = v_r.ZOID AND QTOV = 0;
   v_ps.success= TRUE; v_ps.a = v_as; -- ok by default
  ELSE
   v_es := SAM.make_execstatus(null,null,'E_NASUBSYSTEM', v_r.ZTYPE);
   EXIT try;
  END IF;
  v_as := v_ps.a;
  --v_ps := tisc.delete_object(v_as,v_r); v_as := v_ps.a;
  IF NOT v_ps.success THEN v_es := v_ps.e; EXIT try; END IF;
 -- 5.2) common action - insert new ZObject record
 INSERT INTO TIS.ZObject values(v_r.*);
 UPDATE TIS.ZObject ZO SET
  ZSTA = 'C', ZDATO = v_r.ZDATE WHERE ZO.ZOID = v_r.ZOID
  AND ZO.ZVER = v_r.ZVER - 1 AND ZO.ZTYPE = v_r.ZTYPE AND ZO.ZSTA = 'N';
  IF NOT FOUND THEN
   RAISE EXCEPTION 'TIS_EXCEPTION: current version record lost'; END IF;
 -- delete data form index tables
 DELETE FROM TIS.ZNAME ZN WHERE ZN.ZOID = v_r.ZOID AND ZN.ZTYPE = v_r.ZTYPE;
 -- decrement ocount on scope
 UPDATE SAM.ScopePolicy XSP SET ocount = COALESCE(XSP.ocount,0) - 1
   WHERE XSP.sid = v_ro.ZSID AND XSP.obj_type = v_r.ZTYPE;
 v_as := SAM.do_auditlog_da_sole(v_as,'2','OD',v_r.ZTYPE,v_r.ZSID,v_r.ZOID);
 -- 6) exit
 v_es := sam.make_execstatus(v_r.ZOID,v_r.ZVER,'I_SUCCESS');
EXCEPTION
 WHEN OTHERS THEN
	v_es:=sam.make_execstatus(v_r.ZOID,v_r.ZVER,'E_SQL',SQLSTATE,SQLERRM);
END;
-- FINALLY:
 PERFORM SAM.do_auditlog_exit(v_as,v_es);
 RETURN v_es;
END $$;
COMMIT TRANSACTION;
