-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION TIS.commit_object(
	v_ZTYPE	varchar(14),
	v_ZOID	bigint,
	v_ZVER	bigint
) RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 v_as SAM.auditstate;
 v_es SAM.execstatus;
 v_r TIS.ZObject%ROWTYPE;
 v_XS SAM.Scope%ROWTYPE;
 v_XSP SAM.ScopePolicy%ROWTYPE;
 ZC_ZTOV_UPDATED CONSTANT bigint := -1;
 ZC_ZTOV_DELETED CONSTANT bigint := -2;
 ZC_ZTOV_NOCHANGES CONSTANT bigint := -3;
 ZC_ZTOV_RDELETED CONSTANT bigint := -4;
BEGIN
 -- 0) enter into procedure
 v_as.subsys := 'TIS';
 v_as.eaction :='OC';
 v_as.obj_type :=COALESCE(v_ZTYPE,'ZO');
 v_as.sid := null; -- unknown
 v_as.ZID := null;
 v_as.ZOID := v_ZOID;
 v_as.ZVER := v_ZVER;
 v_as.ZTYPE := v_ZTYPE;
 v_as.ZLVL := null; -- unknown
 v_as.proc := 'commit_object';
 v_as := SAM.do_auditlog_enter(v_as);
 -- 0.1) copy parameters to v_r
 v_r.ZOID := v_ZOID; -- obtain later
 v_r.ZVER := v_ZVER; 
 v_r.ZTYPE := v_ZTYPE;
 v_r.ZSID := null; -- unknown
 v_r.ZLVL := null; -- unknown
 v_r.ZUID := SAM.get_user(); -- identify user
 v_r.ZSTA := 'I'; -- intermediate version 
 -- v_r.ZDATE := NOW(); -- obtain later
 v_r.ZDATO := NULL; -- actual (latest) version of object
<<try>>
BEGIN
 -- 1) pre access control checks
 -- 1.1) check transaction isolation level
 IF NOT SAM.check_isolation() THEN
    v_es := SAM.make_es_transisolation(null,null); EXIT try;
 END IF;
 -- 1.2) check identified user
 IF v_r.ZUID IS NULL THEN
    v_es := sam.make_execstatus(null,null,'E_NOUSER',session_user); EXIT try;
 END IF;
 -- 1.3) unallowed nulls
 IF v_r.ZTYPE IS NULL THEN v_es := SAM.make_es_notnull(null,null,
	'commit_object','ZTYPE'); EXIT try; END IF;
 IF v_r.ZOID IS NULL THEN v_es := SAM.make_es_notnull(null,null,
	'commit_object','ZOID'); EXIT try; END IF;
 IF v_r.ZVER IS NULL THEN v_es := SAM.make_es_notnull(null,null,
	'commit_object','ZVER'); EXIT try; END IF;
-- 2) access control
-- 2.1) get ZObject record
SELECT * INTO v_r FROM TIS.ZObject ZO WHERE ZO.ZOID = v_r.ZOID AND
	ZO.ZVER = v_r.ZVER AND ZO.ZTYPE= v_r.ZTYPE FOR UPDATE;
IF NOT FOUND THEN
   v_es := SAM.make_execstatus(null,null,'E_INVALIDVERSION',
   v_ZTYPE,CAST(v_ZOID AS text),CAST(v_ZVER AS text)); EXIT try; END IF;
 -- 2.1.1) set unknown auditstate fields
 v_as.sid := v_r.ZSID; v_as.ZLVL := v_r.ZLVL;
-- 2.3) check ZSTA
IF v_r.ZSTA <> 'I' THEN
  v_es := SAM.make_execstatus(null,null,'E_NOTOPENED',
  '('||v_r.ZTYPE||','||v_r.ZOID||','||v_r.ZVER||')'); EXIT try;
END IF;
-- 2.2) check ZUID
IF v_r.ZUID <> SAM.get_user() THEN
  v_es := SAM.make_execstatus(null,null,'E_NOTOWNER',
  '('||v_r.ZTYPE||','||v_r.ZOID||','||v_r.ZVER||')'); EXIT try;
END IF;
-- 2.99) check access
IF v_r.ZVER = 1 THEN 
 v_ps := SAM.check_access_create(v_as,v_r.ZSID,v_r.ZTYPE,v_r.ZLVL);
ELSE
 v_ps := SAM.check_access_write(v_as,v_r.ZSID,v_r.ZTYPE,v_r.ZOID,v_r.ZLVL);
END IF;
v_as := v_ps.a;
IF NOT v_ps.success THEN v_es := v_ps.e; EXIT try; END IF;
-- 3) lock required tables
-- NO LOCK TABLE TIS.ZObject IN EXCLUSIVE MODE; -- updating record only
LOCK TABLE SAM.Scope IN SHARE MODE;	-- for looking at chpts
LOCK TABLE SAM.ScopePolicy IN SHARE MODE; -- for maxcount/ocount
-- 4) check data
-- 4.0) check ZDATE
IF v_r.ZDATE > NOW() THEN
  v_es := SAM.make_execstatus(null,null,'E_INVALIDVERDATE',
  CAST(v_r.ZDATE as text) || ' NOW=' || NOW()::text); EXIT try; END IF;
-- 4.1) System object types
IF v_r.ZTYPE IN ('X','Y','Z') THEN
 v_es := sam.make_execstatus(null,null,'E_NASYSZTYPE',v_r.ZTYPE);
 EXIT try; END IF;
-- 4.2) unknown codes
IF NOT dic.check_code('TIS_OBJECT_TYPE',v_r.ZTYPE) THEN
  v_es := SAM.make_execstatus(null,null,'E_INVALIDZTYPE',v_r.ZTYPE);
  EXIT try; END IF;
-- 4.3) check scope
SELECT * INTO v_XS FROM SAM.Scope XS WHERE XS.id = v_r.ZSID;
IF NOT FOUND THEN
   v_es := SAM.make_execstatus(null,null,'E_NOSCOPE',
   CAST(v_r.ZSID AS text)); EXIT try; END IF;
 -- 4.3.1) check scope chpts
v_r.ZDATE = NOW();
IF NOT (v_XS.chpts IS NULL) THEN
 IF v_XS.chpts >= v_r.ZDATE THEN
   v_es := SAM.make_execstatus(null,null,'E_INVALIDCHPTS',
   CAST(v_r.ZSID AS text),
   CAST(v_r.ZDATE AS text), CAST(v_XS.chpts AS text)); EXIT try; END IF;
END IF;
 -- 4.3.2) scope must be local
 IF v_XS.is_local <> 'Y' OR v_XS.is_local IS NULL THEN
   v_es := SAM.make_execstatus(null,null,'E_NONLOCALSCOPE',
   CAST(v_r.ZSID AS text)); EXIT try; END IF;
-- 4.4) check ScopePolicy for allowed object type
SELECT * INTO v_XSP FROM SAM.ScopePolicy XSP WHERE
	XSP.sid = v_r.ZSID AND XSP.obj_type = v_r.ZTYPE;
IF NOT FOUND THEN
   v_es := SAM.make_execstatus(null,null,'E_NAZTYPE4SCOPE', v_r.ZTYPE,
   CAST(v_r.ZSID AS text)); EXIT try; END IF;
-- 5) commit object
 v_r.ZDATE := NOW(); -- current time
 -- 5.1) object specific commit
  -- don't work, more research needed:
  --EXECUTE 'SELECT TISC.commit_object($1,$2)' INTO v_ps USING v_as,v_r;
  IF v_r.ZTYPE LIKE 'TISC.%' THEN
   v_ps := tisc.commit_object(v_as,v_r);
  ELSIF v_r.ZTYPE LIKE 'FS.%' THEN
   v_ps := FS.commit_object(v_as,v_r);
  ELSIF v_r.ZTYPE LIKE 'MSG.%' THEN
   v_ps := MSG.commit_object(v_as,v_r);
  ELSIF v_r.ZTYPE LIKE 'QR.%' THEN
   v_ps := QR.commit_object(v_as,v_r);
  ELSIF v_r.ZTYPE LIKE 'COP.%' THEN
   v_ps := COP.commit_object(v_as,v_r);
  ELSIF v_r.ZTYPE = 'TIS.Q' THEN
  -- SIC! no QC implemented for duplicate qname. since 2023-04-53 23:58
   update TIS.QSeq SET QTOV=v_r.ZVER where QOID = v_r.ZOID AND QTOV = 0
       AND EXISTS (SELECT QOID from TIS.QSeq QS2
        WHERE QS2.QOID = v_r.ZOID and QS2.QVER = v_r.ZVER AND QS2.QTOV < 0 );
   update TIS.QSeq SET QTOV=0 where QOID = v_r.ZOID AND QVER=v_r.ZVER AND QTOV<0;
   v_ps.success= TRUE; v_ps.a = v_as; -- ok by default
  ELSE
   v_es := SAM.make_execstatus(null,null,'E_NASUBSYSTEM', v_r.ZTYPE);
   EXIT try;
  END IF;
  v_as := v_ps.a;
  IF NOT v_ps.success THEN v_es := v_ps.e; EXIT try; END IF;
  -- IF NOT v_ps.success THEN v_es := v_ps.e; EXIT try; END IF;
 -- 5.2) common action - update ZObject record
 UPDATE TIS.ZObject ZO SET
  ZSTA = 'N', ZDATE = v_r.ZDATE 
  WHERE ZO.ZOID = v_r.ZOID AND ZO.ZVER = v_r.ZVER AND ZO.ZTYPE = v_r.ZTYPE
  AND ZO.ZSTA = 'I';
 IF v_r.ZVER <> 1 THEN
  UPDATE TIS.ZObject ZO SET
  ZSTA = 'C', ZDATO = v_r.ZDATE WHERE ZO.ZOID = v_r.ZOID
  AND ZO.ZVER = v_r.ZVER - 1 AND ZO.ZTYPE = v_r.ZTYPE AND ZO.ZSTA = 'N';
  IF NOT FOUND THEN
   RAISE EXCEPTION 'TIS_EXCEPTION: current version record lost'; END IF;
 END IF;
 v_as := SAM.do_auditlog_da_sole(v_as,'2','OC',v_r.ZTYPE,v_r.ZSID,v_r.ZOID);
 -- 6) exit
 v_es := sam.make_execstatus(v_r.ZOID,v_r.ZVER,'I_SUCCESS');
EXCEPTION
 WHEN OTHERS THEN
	v_es:=sam.make_execstatus(v_r.ZOID,v_r.ZVER,'E_SQL',SQLSTATE,SQLERRM);
END;
-- FINALLY:
 PERFORM SAM.do_auditlog_exit(v_as,v_es);
 RETURN v_es;
END $$;
COMMIT TRANSACTION;
