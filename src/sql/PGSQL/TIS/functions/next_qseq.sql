-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
-- 2023-04-40 22:27 SAM.next_QOID(sid,qtype) done and tested (39 lines)
-- 2023-04-45 20:36 TIS.next_QOID(sid,qtype) copy/paste from the SAM.next_QOID(..)
-- 2023-04-45 21:43 TIS.next_QRID(v_QOID,v_QVER) from TIS.next_QOID(..) (27 lines)
-- 2023-04-54 21:43 TIS.next_QRID_qseq(v_QOID) from TIS.next_QOID(..) (27 lines)
--

BEGIN TRANSACTION;
-- inner function, without any access control
CREATE OR REPLACE FUNCTION TIS.next_QRID_qseq(v_QOID bigint) RETURNS bigint VOLATILE
  LANGUAGE plpgSQL SECURITY INVOKER as $$
DECLARE
 --v_r SAM.ZObject%ROWTYPE;
 v_QRSQ bigint;
 v_es SAM.execstatus;
 v_QS TIS.QSeq%ROWTYPE;
BEGIN
 -- 1) lock required tables
 --LOCK TABLE SAM.ZObject IN EXCLUSIVE MODE;
 update TIS.ZObject set QRSQ = QRSQ + 1 where ZOID = v_QOID and ZTYPE = 'TIS.Q' and ZSTA = 'Q';
 IF NOT FOUND THEN return null; END IF;
 --PERFORM pg_sleep(10); -- tested, locks after update works fine!
 SELECT QRSQ from TIS.ZObject into v_QRSQ WHERE ZOID = v_QOID and ZTYPE = 'TIS.Q' and ZSTA = 'Q';
 SELECT * from TIS.QSeq into v_QS where QOID= v_QOID and QTOV=0;
 IF v_QS.qend < v_QRSQ THEN
  RAISE EXCEPTION 'TIS_EXCEPTION: no more numbers in sequence';
 END IF;
 RETURN v_QRSQ;
END $$;

CREATE OR REPLACE FUNCTION TIS.next_qseq( v_QOID bigint) RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
  -- v_ZOID - ZOID/QOID of 'TIS.Q' object
  -- RETURNS SAM.execstatus.ZVER - new value from sequence or null
  --         other fields of SAM.execstatus contain error status
DECLARE
 v_ps SAM.procstate;
 v_as SAM.auditstate;
 v_es SAM.execstatus;
 v_r TIS.ZObject%ROWTYPE; -- new version of the data object
 v_ro TIS.ZObject%ROWTYPE; -- current version of the data object
 v_XS SAM.Scope%ROWTYPE;
 v_XSP SAM.ScopePolicy%ROWTYPE;
 v_ZOID bigint;
 v_ZVER bigint;
BEGIN
 -- 0) enter into procedure
 v_as.subsys := 'TIS';
 v_as.eaction :='OO';
 v_as.obj_type := 'TIS.Q';
 v_as.sid := null; -- unknown
 v_as.ZID := null;
 v_as.ZOID := v_QOID;
 v_as.ZVER := null;
 v_as.ZTYPE := 'TIS.Q';
 v_as.ZLVL := null; -- unknown
 v_as.proc := 'next_qseq';
 v_as := SAM.do_auditlog_enter(v_as);
 -- 0.1) copy parameters to v_r
 v_r.ZOID := v_QOID; -- obtain later
 -- v_r.ZVER := v_ZVER; -- obtain late as v_ro.ZVER + 1
 v_r.ZTYPE := 'TIS.Q';
 v_r.ZSID := null; -- unknown
 v_r.ZLVL := null; -- unknown
 v_r.ZUID := SAM.get_user(); -- identify user
 v_r.ZSTA := 'Q'; -- intermediate version 
 v_r.ZDATE := NOW(); -- current time for new object version
 v_r.ZDATO := NULL; -- actual (latest) version of object
<<try>>
BEGIN
 -- 1) pre access control checks
 -- 1.1) check transaction isolation level
 IF NOT SAM.check_isolation() THEN
    v_es := SAM.make_es_transisolation(null,null);
    v_es.ZVER := null; -- this field used for new QRSQ value
    EXIT try;
 END IF;
 -- 1.2) check identified user
 IF SAM.get_user() IS NULL THEN
    v_es := sam.make_execstatus(null,null,'E_NOUSER',session_user);
    v_es.ZVER := null; -- this field used for new QRSQ value
    EXIT try;
 END IF;
 -- 1.3) unallowed nulls
 IF v_r.ZTYPE IS NULL THEN v_es := SAM.make_es_notnull(null,null,
	'next_qseq','ZTYPE');
    v_es.ZVER := null; -- this field used for new QRSQ value
    EXIT try;
 END IF;
 IF v_r.ZOID IS NULL THEN v_es := SAM.make_es_notnull(null,null,
	'next_qseq','ZOID');
    v_es.ZVER := null; -- this field used for new QRSQ value
    EXIT try;
 END IF;
-- 2) access control
-- 2.1) get ZObject record
SELECT * INTO v_ro FROM TIS.ZObject ZO WHERE ZO.ZOID = v_r.ZOID AND
	ZO.ZTYPE = v_r.ZTYPE AND ZSTA = 'Q' FOR UPDATE;
IF NOT FOUND THEN
  SELECT * INTO v_ro FROM TIS.ZObject ZO WHERE ZO.ZOID = v_r.ZOID AND
	ZO.ZTYPE = v_r.ZTYPE AND ZSTA = 'N' FOR UPDATE;
  IF NOT FOUND THEN
   v_es := SAM.make_execstatus(null,null,'E_NOBJECT',
   v_ZTYPE,'ZOID',CAST(v_ZOID AS text));
   v_es.ZVER := null; -- this field used for new QRSQ value
   EXIT try;
  END IF;
 END IF;
 -- 2.1.1) set unknown auditstate fields
 v_as.sid := v_ro.ZSID; v_as.ZLVL := v_ro.ZLVL;
-- 2.2) get unknown object's properties:
v_r.ZSID := v_ro.ZSID;
v_r.ZLVL := v_ro.ZLVL;
v_r.ZVER := v_ro.ZVER;
v_r.QRSQ := v_ro.QRSQ;
--
v_as.ZVER := v_r.ZVER;
-- 2.4) check locking (SIC! review required)
PERFORM ZOID FROM TIS.ZObject ZO WHERE ZO.ZOID = v_r.ZOID AND
	ZO.ZTYPE = v_r.ZTYPE AND ZVER >= v_r.ZVER AND ZSTA='L'; 
IF FOUND THEN
   v_es := SAM.make_execstatus(null,null,'E_OBJECTLOCKED');
   v_es.ZVER := null; -- this field used for new QRSQ value
   EXIT try;
END IF;
-- 2.99) check access
v_ps := SAM.check_access_write(v_as,v_r.ZSID,v_r.ZTYPE,v_r.ZOID,v_r.ZLVL);
v_as := v_ps.a;
IF NOT v_ps.success THEN v_es := v_ps.e;
 v_es.ZVER := null; -- this field used for new QRSQ value
 EXIT try;
END IF;
-- 3) lock required tables
-- NO LOCK TABLE TIS.ZObject IN EXCLUSIVE MODE; -- updating/inserting record
LOCK TABLE SAM.Scope IN SHARE MODE;	-- for looking at chpts
-- NO LOCK TABLE SAM.ScopePolicy IN SHARE MODE; -- for maxcount/ocount
-- 4) insert ZSTA='Q' record if required
IF v_ro.ZSTA = 'N' THEN
 v_ro.ZVER := v_ro.ZVER + 1;
 v_ro.ZSTA= 'Q';
 INSERT into TIS.ZObject values( v_ro.* );
END IF;
-- 5) get next QRSQ
v_es.ZVER := TIS.next_QRID_qseq(v_r.ZOID);
-- 6) check range (not implemented)
--
-- 7) return
 v_es := sam.make_execstatus(v_r.ZOID,v_es.ZVER,'I_SUCCESS');
EXCEPTION
 WHEN OTHERS THEN
	v_es:=sam.make_execstatus(v_r.ZOID,v_r.ZVER,'E_SQL',SQLSTATE,SQLERRM);
	v_es.ZVER := null; -- this field used for new QRSQ value
END;
-- FINALLY:
 PERFORM SAM.do_auditlog_exit(v_as,v_es);
 RETURN v_es;
END $$;

CREATE OR REPLACE FUNCTION TIS.next_qseq( v_QSID bigint, v_QNAME varchar(32))
 RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
  -- v_QSID - scope id
  -- v_QNAME - name of sequense, unique in scope
DECLARE
 v_QOID bigint;
 v_es SAM.execstatus;
BEGIN
  SELECT QOID FROM TIS.QSeq INTO v_QOID WHERE QNAME = v_QNAME AND QSID = v_QSID AND QTOV=0;
  v_es := TIS.next_qseq(v_QOID);
 RETURN v_es;
END $$;
COMMIT TRANSACTION;
