-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION TIS.create_object(
	v_ZTYPE	varchar(14),
	v_ZSID	bigint,
	v_ZLVL	smallint
) RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 v_as SAM.auditstate;
 v_es SAM.execstatus;
 v_r TIS.ZObject%ROWTYPE;
 v_XS SAM.Scope%ROWTYPE;
 v_XSP SAM.ScopePolicy%ROWTYPE;
BEGIN
 -- 0) enter into procedure
 -- 0.1) guest mandatory v_ZLVL and v_ZSID if null
 IF v_ZSID IS NULL THEN
   v_ZSID =  COALESCE(v_ZSID,SAM.get_user_sid()); -- get users scope instead of null
   --v_ZSID =  COALESCE(v_ZSID,SAM.get_user_sid(v_ZTYPE)); --mb to think about guessing SID by TYPE
 END IF;
 IF v_ZLVL IS NULL THEN
   v_ZLVL =  COALESCE(v_ZLVL,SAM.get_user_slevel()); -- get users slevel instead of null
   v_ZLVL = LEAST(v_ZLVL,SAM.get_scope_slevel_max(v_ZSID)); -- try to downgrade slevel to scope max one
 END IF;
 -- 0.2) copy parameters to auditstate and v_r
 v_as.subsys := 'TIS';
 v_as.eaction :='C';
 v_as.obj_type :=COALESCE(v_ZTYPE,'ZO');
 v_as.sid := v_ZSID;
 -- v_as.ZID := v_id;
 v_as.ZTYPE := v_ZTYPE;
 v_as.ZLVL := v_ZLVL; -- COALESCE(v_ZLVL,SAM.get_user_slevel());
 v_as.proc := 'create_object';
 v_as := SAM.do_auditlog_enter(v_as);
 -- 0.1) copy parameters to v_r
 -- v_r.ZOID := v_ZOID; -- obtain later
 v_r.ZVER := 1; 
 v_r.ZTYPE := v_ZTYPE;
 v_r.ZSID := v_ZSID;
 v_r.ZLVL := v_ZLVL; -- COALESCE(v_ZLVL,SAM.get_user_slevel());
 v_r.ZUID := SAM.get_user(); -- identify user
 v_r.ZSTA := 'I'; -- intermediate version 
 v_r.QRSQ := 0; 
 v_r.ZDATE := NOW();
 v_r.ZDATO := NULL; -- actual (latest) version of object
<<try>>
BEGIN
 -- 1) pre access control checks
 -- 1.1) check transaction isolation level
 IF NOT SAM.check_isolation() THEN
    v_es := SAM.make_es_transisolation(null,null); EXIT try;
 END IF;
 -- 1.2) check identified user
 IF v_r.ZUID IS NULL THEN
    v_es := sam.make_execstatus(null,null,'E_NOUSER',session_user); EXIT try;
 END IF;
 -- 1.3) unallowed nulls
 IF v_r.ZTYPE IS NULL THEN v_es := SAM.make_es_notnull(null,null,
	'TIS.ZObject','ZTYPE'); EXIT try; END IF;
 IF v_r.ZSID IS NULL THEN v_es := SAM.make_es_notnull(null,null,
	'TIS.ZObject','ZSID'); EXIT try; END IF;
-- 2) access control
v_ps := SAM.check_access_create(v_as,v_r.ZSID,v_r.ZTYPE,v_r.ZLVL);
v_as := v_ps.a;
IF NOT v_ps.success THEN v_es := v_ps.e; EXIT try; END IF;
-- 3) lock required tables
-- NO LOCK TABLE TIS.ZObject IN EXCLUSIVE MODE; -- inserting new record only
LOCK TABLE SAM.Scope IN SHARE MODE;	-- for looking at chpts
LOCK TABLE SAM.ScopePolicy IN EXCLUSIVE MODE; -- for maxcount/ocount
-- 4) check data
-- 4.1) System object types
IF v_r.ZTYPE IN ('X','Y','Z') THEN
 v_es := sam.make_execstatus(null,null,'E_NASYSZTYPE',v_r.ZTYPE);
 EXIT try; END IF;
-- 4.2) unknown codes
IF NOT dic.check_code('TIS_OBJECT_TYPE',v_r.ZTYPE) THEN
  v_es := SAM.make_execstatus(null,null,'E_INVALIDZTYPE',v_r.ZTYPE);
  EXIT try; END IF;
IF NOT dic.check_code('SLEVEL',CAST(v_r.ZLVL as text)) THEN
  v_es := SAM.make_es_invalidcode(null,null,CAST(v_r.ZLVL as text),
  'ZLVL','SLEVEL'); EXIT try; END IF;
-- 4.3) check scope
SELECT * INTO v_XS FROM SAM.Scope XS WHERE XS.id = v_r.ZSID;
IF NOT FOUND THEN
   v_es := SAM.make_execstatus(null,null,'E_NOSCOPE',
   CAST(v_r.ZSID AS text)); EXIT try; END IF;
 -- 4.3.1) check scope chpts
IF NOT (v_XS.chpts IS NULL) THEN
 IF v_XS.chpts >= v_r.ZDATE THEN
   v_es := SAM.make_execstatus(null,null,'E_INVALIDCHPTS',
   CAST(v_r.ZSID AS text),
   CAST(v_r.ZDATE AS text), CAST(v_XS.chpts AS text)); EXIT try; END IF;
END IF;
 -- 4.3.2) scope must be local
 IF v_XS.is_local <> 'Y' OR v_XS.is_local IS NULL THEN
   v_es := SAM.make_execstatus(null,null,'E_NONLOCALSCOPE',
   CAST(v_r.ZSID AS text)); EXIT try; END IF;
-- 4.4) check ScopePolicy for allowed object type
SELECT * INTO v_XSP FROM SAM.ScopePolicy XSP WHERE
	XSP.sid = v_r.ZSID AND XSP.obj_type = v_r.ZTYPE;
IF NOT FOUND THEN
   v_es := SAM.make_execstatus(null,null,'E_NAZTYPE4SCOPE', v_r.ZTYPE,
   CAST(v_r.ZSID AS text)); EXIT try; END IF;
IF NOT v_XSP.maxcount IS NULL THEN
  IF COALESCE(v_XSP.ocount,0) >= v_XSP.maxcount THEN
   v_es := SAM.make_execstatus(null,null,'E_2MANYOBJECTS', v_r.ZTYPE,
   CAST(v_r.ZSID AS text),CAST(v_XSP.maxcount AS text)); EXIT try; END IF;
END IF;
-- 4.4.2) update XSP for objects counter
 UPDATE SAM.ScopePolicy XSP SET ocount = COALESCE(v_XSP.ocount,0) + 1
   WHERE XSP.sid = v_r.ZSID AND XSP.obj_type = v_r.ZTYPE;
-- 5) add record
 v_r.ZOID := TIS.next_QOID(v_r.ZSID,v_r.ZTYPE);
 -- SIC! check for null v_r.ZOID
 IF v_r.ZOID IS NULL THEN
    v_es := sam.make_execstatus(null,null,'E_NOTRANGESEQ',v_r.ZTYPE::text,v_r.ZSID::text); EXIT try;
 END IF;
 INSERT INTO TIS.ZObject values(v_r.*);
 v_as := SAM.do_auditlog_da_sole(v_as,'2','C',v_r.ZTYPE,v_r.ZSID,v_r.ZOID);
 -- 6) exit
 v_es := sam.make_execstatus(v_r.ZOID,v_r.ZVER,'I_SUCCESS');
EXCEPTION
 WHEN OTHERS THEN
	v_es:=sam.make_execstatus(v_r.ZOID,v_r.ZVER,'E_SQL',SQLSTATE,SQLERRM);
END;
-- FINALLY:
 PERFORM SAM.do_auditlog_exit(v_as,v_es);
 RETURN v_es;
END $$;
COMMIT TRANSACTION;
