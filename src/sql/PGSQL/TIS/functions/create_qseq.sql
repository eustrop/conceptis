-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--


BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION TIS.create_qseq(
	v_QSID	bigint,
	v_QLVL	smallint,
	v_qstart	bigint,
	v_qend	bigint,
	v_qname	varchar(32),
	v_descr	varchar(127)
) RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 v_as SAM.auditstate;
 v_es SAM.execstatus;
 v_r TIS.QSeq%ROWTYPE;
 v_ZO TIS.ZObject%ROWTYPE;
BEGIN
 -- 0) enter into procedure
 v_as.subsys := 'TIS';
 v_as.eaction :='C';
 v_as.obj_type :='TIS.Q';
 v_as.sid := v_QSID;
 -- v_as.ZID := v_id;
 v_as.ZTYPE := 'TIS.Q';
 v_as.ZLVL := COALESCE(v_QLVL,SAM.get_user_slevel());
 v_as.proc := 'create_qseq';
 v_as := SAM.do_auditlog_enter(v_as);
 -- 0.1) copy parameters to v_r
 -- v_r.ZOID := v_ZOID; -- obtain later
 v_r.QOID := null; --    bigint NOT NULL,
 v_r.QVER := null; --   bigint NOT NULL,
 v_r.QTOV := -1; --   bigint NOT NULL,
 v_r.QSID := v_QSID; --  bigint NOT NULL,
 v_r.QLVL := COALESCE(v_QLVL,SAM.get_user_slevel()); --   smallint NOT NULL,
 v_r.qstart := v_qstart; -- bigint NOT NULL,
 v_r.qend := v_qend; --   bigint NOT NULL,
 v_r.QNAME := v_qname; --  varchar(32) NOT NULL,
 v_r.descr := v_descr; --  varchar(127) NOT NULL,
 
<<try>>
BEGIN
 -- 1) pre access control checks
 -- 1.1) check transaction isolation level
 IF NOT SAM.check_isolation() THEN
    v_es := SAM.make_es_transisolation(null,null); EXIT try;
 END IF;
 -- 1.2) create object (access control inside)
 v_es := TIS.create_object('TIS.Q',v_QSID,v_QLVL);
 IF v_es.errnum > 0 THEN EXIT try; END IF;
 v_r.QOID := v_es.ZID;
 v_r.QVER := v_es.ZVER;
 -- 1.3) create TIS.QSeq row
 LOCK TABLE TIS.QSeq;
 perform from TIS.QSeq where QSID = v_QSID AND QNAME = v_QNAME AND QTOV=0;
 IF FOUND THEN
  RAISE EXCEPTION 'TIS_EXCEPTION: duplicate QSeq name for scope';
 END IF;
 INSERT INTO TIS.QSeq values(v_r.*);
 -- 1.4) set object's QRSQ
 UPDATE TIS.ZObject set QRSQ=v_r.qstart-1 where ZOID=v_r.QOID AND ZVER = v_r.QVER AND ZTYPE='TIS.Q' AND ZSTA='I';
 -- 1.5) commit object
 v_es := TIS.commit_object('TIS.Q',v_r.QOID,v_r.QVER);
 IF v_es.errnum > 0 THEN
   RAISE EXCEPTION 'TIS_EXCEPTION: TIS.commit_object() : %',  (v_es.errcode || ' ' || v_es.errdesc);
 END IF;
 -- 1.6) open sequence
 select * from TIS.ZObject into v_ZO WHERE ZOID=v_r.QOID AND ZVER=v_r.QVER;
 v_ZO.ZVER=v_ZO.ZVER+1;
 v_ZO.ZSTA='Q';
 insert into TIS.ZObject values(v_ZO.*);
EXCEPTION
 WHEN OTHERS THEN
	v_es:=sam.make_execstatus(v_r.QOID,v_r.QVER,'E_SQL',SQLSTATE,SQLERRM);
END;
-- FINALLY:
 PERFORM SAM.do_auditlog_exit(v_as,v_es);
 RETURN v_es;
END $$;
COMMIT TRANSACTION;
