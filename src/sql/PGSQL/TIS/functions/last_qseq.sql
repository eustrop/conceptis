-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
-- purpose: returns last (current) value of sequence
--	(for showning some historic state of data)

CREATE OR REPLACE FUNCTION TIS.last_qseq(QOID bigint) RETURNS bigint VOLATILE
  AS $$ SELECT QRSQ from TIS.ZObject ZO where ZO.ZOID=QOID AND ZO.ZSTA='Q' AND
	 ZO.ZLVL <= SAM.get_user_slevel() -- MAC - check secrecy 
	 AND
	 -- check pemissions for reading records at scope
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = ZO.ZSID AND XAS.obj_type = 'TIS.Q'
		and XAS.reada = 'Y' )
	 -- check rejections for reading records at scope
	 AND NOT
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = ZO.ZSID AND XAS.obj_type = 'TIS.Q'
		and XAS.reada = 'R' );
     $$
    LANGUAGE SQL SECURITY DEFINER;
REVOKE ALL ON FUNCTION TIS.last_qseq(QOID bigint) FROM PUBLIC;

CREATE OR REPLACE FUNCTION TIS.last_qseq(v_QSID bigint, v_QNAME varchar(32)) RETURNS bigint VOLATILE
  AS $$ SELECT TIS.last_qseq(QOID) from TIS.QSeq QS where QS.QTOV=0 and QS.QSID= v_QSID AND QS.QNAME = v_QNAME $$
    LANGUAGE SQL SECURITY DEFINER;
REVOKE ALL ON FUNCTION TIS.last_qseq(v_QSID bigint, v_QNAME varchar(32)) FROM PUBLIC;
