-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

--DROP TABLE IF EXISTS TIS.ZObject CASCADE;
CREATE TABLE TIS.ZObject (
	ZOID	bigint NOT NULL,
	ZVER	bigint NOT NULL,
	ZTYPE	varchar(14) NOT NULL,
	ZSID	bigint NOT NULL,
	ZLVL	smallint NOT NULL,
	ZUID	bigint NOT NULL,
	ZSTA	"char" NOT NULL,
	ZDATE	timestamptz NOT NULL,
	ZDATO	timestamptz NULL,
	QRSQ	bigint NOT NULL,
	PRIMARY KEY (ZOID,ZVER)
	);
CREATE INDEX ZObject_idx1 on TIS.ZObject(ZDATE);
CREATE INDEX ZObject_idx2 on TIS.ZObject(ZDATO);

-- QObject as view at this moment

CREATE OR REPLACE VIEW TIS.QObject as
 SELECT
	ZOID	QOID,
	ZVER	QVER,
	ZTYPE	QTYPE,
	ZSID	QSID,
	ZLVL	QLVL,
	ZUID	QUID,
	ZSTA	QSTA,
	ZDATE	QDATE,
	ZDATO	QDATO,
	QRSQ
 FROM TIS.ZObject;

