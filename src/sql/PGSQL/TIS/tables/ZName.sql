-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

--DROP TABLE IF EXISTS TIS.ZName CASCADE;
CREATE TABLE TIS.ZName (
	ZOID	bigint NOT NULL,
	ZTYPE	varchar(14) NOT NULL,
	ZSID	bigint NOT NULL,
	ZLVL	smallint NOT NULL,
	ZNAME	varchar(255) NOT NULL,
	PRIMARY KEY (ZOID),
	UNIQUE (ZNAME,ZTYPE,ZSID)
	);
CREATE INDEX ZName_idx1 on TIS.ZName(ZNAME);

-- TIS.QName as view at this moment

CREATE OR REPLACE VIEW TIS.QName AS
 SELECT
	ZOID	QOID,
	ZTYPE	QTYPE,
	ZSID	QSID,
	ZLVL	QLVL,
	ZNAME	QNAME
 FROM TIS.ZName;
