-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

--DROP TABLE IF EXISTS TIS.QSeq CASCADE;
CREATE TABLE TIS.QSeq (
	QOID	bigint NOT NULL,
	QVER	bigint NOT NULL,
	QTOV	bigint NOT NULL,
	QSID	bigint NOT NULL,
	QLVL	smallint NOT NULL,
	qstart	bigint NOT NULL,
	qend	bigint NOT NULL,
	QNAME	varchar(32) NOT NULL,
	descr	varchar(127) NULL,
	PRIMARY KEY (QOID,QVER)
	);
CREATE INDEX QSeq_idx1 on TIS.QSeq(QNAME,QSID);
