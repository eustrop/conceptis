-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

--DROP TABLE IF EXISTS TIS.ZDateView CASCADE;
CREATE TABLE TIS.ZDateView (
	uid	bigint NOT NULL, -- id of owner of this record
	ts	timestamp NOT NULL, -- when this record installed
	ZDATO	timestamp NULL, -- date slice to look over the VD_ views 
	PRIMARY KEY (uid)
	);

-- TIS.QDateView as view at this moment

CREATE OR REPLACE VIEW TIS.QDateView AS
 SELECT
  uid,ts,ZDATO QDATO
 FROM TIS.ZDateView;
