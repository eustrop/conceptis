REVOKE ALL ON FUNCTION TIS.set_vdate(
	v_ZDATO	timestamp,
	v_force	char(1) -- 'Y' for ignore existing value
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION TIS.set_vdate(
	v_ZDATO	timestamp,
	v_force	char(1) -- 'Y' for ignore existing value
) TO tis_users;
