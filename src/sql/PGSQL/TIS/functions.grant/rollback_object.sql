REVOKE ALL ON FUNCTION TIS.rollback_object(
	v_ZTYPE	varchar(14),
	v_ZOID	bigint,
	v_ZVER	bigint,
	v_force char(1)
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION TIS.rollback_object(
	v_ZTYPE	varchar(14),
	v_ZOID	bigint,
	v_ZVER	bigint,
	v_force char(1)
) TO tis_users;
