REVOKE ALL ON FUNCTION TIS.create_object(
	v_ZTYPE	varchar(14),
	v_ZSID	bigint,
	v_ZLVL	smallint
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION TIS.create_object(
	v_ZTYPE	varchar(14),
	v_ZSID	bigint,
	v_ZLVL	smallint
) TO tis_users;
