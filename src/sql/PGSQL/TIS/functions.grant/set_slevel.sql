REVOKE ALL ON FUNCTION TIS.set_slevel(
	v_ZTYPE	varchar(14),
	v_ZOID	bigint,
	v_ZVER	bigint,
	v_ZLVL	smallint
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION TIS.set_slevel(
	v_ZTYPE	varchar(14),
	v_ZOID	bigint,
	v_ZVER	bigint,
	v_ZLVL	smallint
) TO tis_users;
