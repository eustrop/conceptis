REVOKE ALL ON FUNCTION TIS.delete_object(
	v_ZTYPE	varchar(14),
	v_ZOID	bigint,
	v_ZVER	bigint
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION TIS.delete_object(
	v_ZTYPE	varchar(14),
	v_ZOID	bigint,
	v_ZVER	bigint
) TO tis_users;
