REVOKE ALL ON FUNCTION TIS.move_object(
	v_ZTYPE	varchar(14),
	v_ZOID	bigint,
	v_ZVER	bigint,
	v_ZSID	bigint
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION TIS.move_object(
	v_ZTYPE	varchar(14),
	v_ZOID	bigint,
	v_ZVER	bigint,
	v_ZSID	bigint
) TO tis_users;
