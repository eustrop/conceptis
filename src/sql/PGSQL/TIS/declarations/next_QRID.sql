-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
-- 2023-04-40 22:27 SAM.next_QOID(sid,qtype) done and tested (39 lines)
-- 2023-04-45 20:36 TIS.next_QOID(sid,qtype) copy/paste from the SAM.next_QOID(..)
-- 2023-04-45 21:43 TIS.next_QRID(v_QOID,v_QVER) from TIS.next_QOID(..) (27 lines)
--

CREATE OR REPLACE FUNCTION TIS.next_QRID(v_QOID bigint, v_QVER bigint) RETURNS bigint VOLATILE
  LANGUAGE plpgSQL SECURITY INVOKER as $$
DECLARE
 --v_r SAM.ZObject%ROWTYPE;
 v_QRSQ bigint;
BEGIN
 -- 1) lock required tables
 --LOCK TABLE SAM.ZObject IN EXCLUSIVE MODE;
 update TIS.ZObject set QRSQ = QRSQ + 1 where ZOID = v_QOID and ZVER = v_QVER and ZSTA = 'I';
 IF NOT FOUND THEN
  return null ;
 END IF;
 --PERFORM pg_sleep(10); -- tested, locks after update works fine!
 SELECT QRSQ from TIS.ZObject into v_QRSQ WHERE ZOID = v_QOID and ZVER = v_QVER and ZSTA = 'I';
 RETURN v_QRSQ;
END $$;
