-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
-- purpose: returns slice timestamp for VD_ family views
--	(for showning some historic state of data)

CREATE OR REPLACE FUNCTION TIS.get_vdate() RETURNS timestamp STABLE
  AS 'select ZDATO from TIS.ZDateView ZDV where ZDV.uid = SAM.get_user()'
    LANGUAGE SQL SECURITY DEFINER;
REVOKE ALL ON FUNCTION TIS.get_vdate() FROM PUBLIC;
--GRANT EXECUTE on function TIS.get_vdate() to tis_users;

