DROP FUNCTION IF EXISTS TIS.delete_object(
	v_ZTYPE	char(1),
	v_ZOID	bigint,
	v_ZVER	bigint
) CASCADE;
