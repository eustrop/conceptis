DROP FUNCTION IF EXISTS TIS.rollback_object(
	v_ZTYPE	char(1),
	v_ZOID	bigint,
	v_ZVER	bigint,
	v_force char(1)
) CASCADE;
