DROP FUNCTION IF EXISTS TIS.commit_object(
	v_ZTYPE	char(1),
	v_ZOID	bigint,
	v_ZVER	bigint
) CASCADE;
