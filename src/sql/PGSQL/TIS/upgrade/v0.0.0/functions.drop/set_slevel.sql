DROP FUNCTION IF EXISTS TIS.set_slevel(
	v_ZTYPE	char(1),
	v_ZOID	bigint,
	v_ZVER	bigint,
	v_ZLVL	smallint
) CASCADE;
