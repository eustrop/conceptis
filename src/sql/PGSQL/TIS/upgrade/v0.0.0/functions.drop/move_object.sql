DROP FUNCTION IF EXISTS TIS.move_object(
	v_ZTYPE	char(1),
	v_ZOID	bigint,
	v_ZVER	bigint,
	v_ZSID	bigint
) CASCADE;
