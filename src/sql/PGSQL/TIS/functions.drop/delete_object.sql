DROP FUNCTION IF EXISTS TIS.delete_object(
	v_ZTYPE	varchar(14),
	v_ZOID	bigint,
	v_ZVER	bigint
) CASCADE;
