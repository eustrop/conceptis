DROP FUNCTION IF EXISTS tis.cast2zname(text) CASCADE;
DROP FUNCTION IF EXISTS tis.check_zname(character,bigint,text) CASCADE;
DROP FUNCTION IF EXISTS tis.get_zname(character,bigint) CASCADE;

DROP FUNCTION IF EXISTS TIS.set_ZNAME(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZNAME	varchar(255)
	) CASCADE;
