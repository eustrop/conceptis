DROP FUNCTION IF EXISTS TIS.rollback_object(
	v_ZTYPE	varchar(14),
	v_ZOID	bigint,
	v_ZVER	bigint,
	v_force char(1)
) CASCADE;
