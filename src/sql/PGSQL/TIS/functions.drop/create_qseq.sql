DROP FUNCTION IS EXISTS TIS.create_qseq(
	v_QSID	bigint,
	v_QLVL	smallint,
	v_qstart	bigint,
	v_qend	bigint,
	v_qname	varchar(32),
	v_descr	varchar(127)
) CASCADE;
