DROP FUNCTION IF EXISTS TIS.set_slevel(
	v_ZTYPE	varchar(14),
	v_ZOID	bigint,
	v_ZVER	bigint,
	v_ZLVL	smallint
) CASCADE;
