-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

-- V0_ - current state of table without ACL/MAC filtring
-- for internal use only.
-- nobody except tables owner can use it
DROP VIEW IF EXISTS TIS.V0_QSeq CASCADE;
CREATE VIEW TIS.V0_QSeq AS SELECT * FROM TIS.QSeq;

-- V_ - current state of table with ACL/MAC filtring
DROP VIEW IF EXISTS TIS.V_QSeq CASCADE;
CREATE VIEW TIS.V_QSeq AS SELECT * FROM TIS.QSeq AS QS
	WHERE 
	 QS.QLVL <= SAM.get_user_slevel() -- MAC - check secrecy 
	 AND QS.QTOV=0
	 AND
	 -- check pemissions for reading records at scope
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = QS.QSID AND XAS.obj_type = 'TIS.Q'
		and XAS.reada = 'Y' )
	 -- check rejections for reading records at scope
	 AND NOT
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = QS.QSID AND XAS.obj_type = 'TIS.Q'
		and XAS.reada = 'R' );
--GRANT SELECT ON TIS.V_QSeq to tis_users;

-- VH_ - current state of table with ACL/MAC filtring
DROP VIEW IF EXISTS TIS.VH_QSeq CASCADE;
CREATE VIEW TIS.VH_QSeq AS SELECT * FROM TIS.QSeq AS QS
	WHERE 
	 QS.QLVL <= SAM.get_user_slevel() -- MAC - check secrecy 
	 AND QS.QTOV>=0
	 AND
	 -- check pemissions for reading records at scope
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = QS.QSID AND XAS.obj_type = 'TIS.Q'
		and XAS.historya = 'Y' )
	 -- check rejections for reading records at scope
	 AND NOT
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = QS.QSID AND XAS.obj_type = 'TIS.Q'
		and XAS.historya = 'R' );
--GRANT SELECT ON TIS.VH_QSeq to tis_users;
