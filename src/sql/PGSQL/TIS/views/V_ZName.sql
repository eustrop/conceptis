-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

-- V0_ - current state of table without ACL/MAC filtring
-- for internal use only.
-- nobody except tables owner can use it
DROP VIEW IF EXISTS TIS.V0_ZName CASCADE;
CREATE VIEW TIS.V0_ZName AS SELECT * FROM TIS.ZName;

-- V_ - current state of table with ACL/MAC filtring
DROP VIEW IF EXISTS TIS.V_ZName CASCADE;
CREATE VIEW TIS.V_ZName AS SELECT * FROM TIS.ZName AS ZN
	WHERE 
	 ZN.ZLVL <= SAM.get_user_slevel() -- MAC - check secrecy 
	 AND
	 -- check pemissions for reading records at scope
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = ZN.ZSID AND XAS.obj_type = ZN.ZTYPE
		and XAS.reada = 'Y' )
	 -- check rejections for reading records at scope
	 AND NOT
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = ZN.ZSID AND XAS.obj_type = ZN.ZTYPE
		and XAS.reada = 'R' );
--GRANT SELECT ON TIS.V_ZName to tis_users;
