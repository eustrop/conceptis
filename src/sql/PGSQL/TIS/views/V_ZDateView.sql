-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

DROP VIEW IF EXISTS TIS.V_ZDateView CASCADE;
CREATE VIEW TIS.V_ZDateView AS SELECT * FROM TIS.ZDateView
	WHERE uid = SAM.get_user();
--GRANT SELECT ON TIS.V_ZDateView to tis_users;
