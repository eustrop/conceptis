-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

-- V0_ - current state of table without ACL/MAC filtring
-- for internal use only.
-- nobody except tables owner can use it
DROP VIEW IF EXISTS TIS.V0_QObject CASCADE;
CREATE VIEW TIS.V0_QObject AS SELECT * FROM TIS.QObject
	WHERE TIS.QObject.QSTA = 'N';

-- V_ - current state of table with ACL/MAC filtring
DROP VIEW IF EXISTS TIS.V_QObject CASCADE;
CREATE VIEW TIS.V_QObject AS SELECT * FROM TIS.QObject AS QO
	WHERE QO.QSTA = 'N' AND
	 QO.QLVL <= SAM.get_user_slevel() -- MAC - check secrecy 
	 AND
	 -- check pemissions for reading records at scope
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = QO.QSID AND XAS.obj_type = QO.QTYPE
		and XAS.reada = 'Y' )
	 -- check rejections for reading records at scope
	 AND NOT
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = QO.QSID AND XAS.obj_type = QO.QTYPE
		and XAS.reada = 'R' );
--GRANT SELECT ON TIS.V_QObject to tis_users;

-- VI_ - intermediate records created by current user
DROP VIEW IF EXISTS TIS.VI_QObject CASCADE;
CREATE VIEW TIS.VI_QObject AS SELECT * FROM TIS.QObject AS QO
	WHERE QO.QSTA IN ('I','L') AND QO.QUID = SAM.get_user() AND
	 QO.QLVL <= SAM.get_user_slevel() -- MAC - check secrecy 
	 AND
	 -- check pemissions for reading records at scope
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = QO.QSID AND XAS.obj_type = QO.QTYPE
		and XAS.reada = 'Y' )
	 -- check rejections for reading records at scope
	 AND NOT
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = QO.QSID AND XAS.obj_type = QO.QTYPE
		and XAS.reada = 'R' );
--GRANT SELECT ON TIS.VI_QObject to tis_users;

-- VL_ - loacked objects
DROP VIEW IF EXISTS TIS.VL_QObject CASCADE;
CREATE VIEW TIS.VL_QObject AS SELECT * FROM TIS.QObject AS QO
	WHERE QO.QSTA IN ('I','L') AND
	 QO.QLVL <= SAM.get_user_slevel() -- MAC - check secrecy 
	 AND
	 -- check pemissions for reading records at scope
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = QO.QSID AND XAS.obj_type = QO.QTYPE
		and XAS.reada = 'Y' )
	 -- check rejections for reading records at scope
	 AND NOT
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = QO.QSID AND XAS.obj_type = QO.QTYPE
		and XAS.reada = 'R' );
--GRANT SELECT ON TIS.VL_QObject to tis_users;

-- VH_ - historic data in table
DROP VIEW IF EXISTS TIS.VH_QObject CASCADE;
CREATE VIEW TIS.VH_QObject AS SELECT * FROM TIS.QObject AS QO
	WHERE QO.QSTA NOT IN ('I','L') AND
	 QO.QLVL <= SAM.get_user_slevel() -- MAC - check secrecy 
	 AND
	 -- check pemissions for reading records at scope
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = QO.QSID AND XAS.obj_type = QO.QTYPE
		and XAS.historya = 'Y' )
	 -- check rejections for reading records at scope
	 AND NOT
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = QO.QSID AND XAS.obj_type = QO.QTYPE
		and XAS.historya = 'R' );
--GRANT SELECT ON TIS.VH_QObject to tis_users;

-- VD_ - slice of data in table which was actual for requested TIS.QDateView.QDATO
DROP VIEW IF EXISTS TIS.VD_QObject CASCADE;
CREATE VIEW TIS.VD_QObject AS SELECT * FROM TIS.QObject AS QO
	WHERE QO.QSTA <> 'I' AND
	 ( -- check date range
	  QO.QDATE <= (SELECT QDATO FROM  TIS.QDateView WHERE uid = SAM.get_user())
	  AND ( QO.QDATO IS NULL OR QO.QDATO >
		(SELECT QDATO FROM  TIS.QDateView WHERE uid = SAM.get_user()) )
	  )
	 AND
	 QO.QLVL <= SAM.get_user_slevel() -- MAC - check secrecy 
	 AND
	 -- check pemissions for reading records at scope
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = QO.QSID AND XAS.obj_type = QO.QTYPE
		and XAS.historya = 'Y' )
	 -- check rejections for reading records at scope
	 AND NOT
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = QO.QSID AND XAS.obj_type = QO.QTYPE
		and XAS.historya = 'R' );
--GRANT SELECT ON TIS.VD_QObject to tis_users;

