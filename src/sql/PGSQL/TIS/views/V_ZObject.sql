-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--

-- V0_ - current state of table without ACL/MAC filtring
-- for internal use only.
-- nobody except tables owner can use it
DROP VIEW IF EXISTS TIS.V0_ZObject CASCADE;
CREATE OR REPLACE VIEW TIS.V0_ZObject AS SELECT * FROM TIS.ZObject ZO
	WHERE ZO.ZSTA = 'N'
	AND ZLVL <= sam.get_user_slevel()
        AND EXISTS
        (select sid from SAM.UserCapability XUC where XUC.uid=sam.get_user()
		AND (XUC.sid = 0 OR XUC.sid = ZO.ZSID) and XUC.capcode = 'V0_BYPASS_ACL');

DROP VIEW IF EXISTS TIS.VH0_ZObject CASCADE;
CREATE OR REPLACE VIEW TIS.VH0_ZObject AS SELECT * FROM TIS.ZObject ZO
	WHERE ZO.ZSTA NOT IN ('L','I')
	AND ZLVL <= sam.get_user_slevel()
        AND EXISTS
        (select sid from SAM.UserCapability XUC where XUC.uid=sam.get_user()
		AND (XUC.sid = 0 OR XUC.sid = ZO.ZSID) and XUC.capcode = 'V0_BYPASS_ACL');

-- V_ - current state of table with ACL/MAC filtring
DROP VIEW IF EXISTS TIS.V_ZObject CASCADE;
CREATE VIEW TIS.V_ZObject AS SELECT * FROM TIS.ZObject AS ZO
	WHERE ZO.ZSTA = 'N' AND
	 ZO.ZLVL <= SAM.get_user_slevel() -- MAC - check secrecy 
	 AND
	 -- check pemissions for reading records at scope
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = ZO.ZSID AND XAS.obj_type = ZO.ZTYPE
		and XAS.reada = 'Y' )
	 -- check rejections for reading records at scope
	 AND NOT
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = ZO.ZSID AND XAS.obj_type = ZO.ZTYPE
		and XAS.reada = 'R' );
--GRANT SELECT ON TIS.V_ZObject to tis_users;

-- VI_ - intermediate records created by current user
DROP VIEW IF EXISTS TIS.VI_ZObject CASCADE;
CREATE VIEW TIS.VI_ZObject AS SELECT * FROM TIS.ZObject AS ZO
	WHERE ZO.ZSTA IN ('I','L') AND ZO.ZUID = SAM.get_user() AND
	 ZO.ZLVL <= SAM.get_user_slevel() -- MAC - check secrecy 
	 AND
	 -- check pemissions for reading records at scope
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = ZO.ZSID AND XAS.obj_type = ZO.ZTYPE
		and XAS.reada = 'Y' )
	 -- check rejections for reading records at scope
	 AND NOT
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = ZO.ZSID AND XAS.obj_type = ZO.ZTYPE
		and XAS.reada = 'R' );
--GRANT SELECT ON TIS.VI_ZObject to tis_users;

-- VL_ - loacked objects
DROP VIEW IF EXISTS TIS.VL_ZObject CASCADE;
CREATE VIEW TIS.VL_ZObject AS SELECT * FROM TIS.ZObject AS ZO
	WHERE ZO.ZSTA IN ('I','L') AND
	 ZO.ZLVL <= SAM.get_user_slevel() -- MAC - check secrecy 
	 AND
	 -- check pemissions for reading records at scope
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = ZO.ZSID AND XAS.obj_type = ZO.ZTYPE
		and XAS.reada = 'Y' )
	 -- check rejections for reading records at scope
	 AND NOT
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = ZO.ZSID AND XAS.obj_type = ZO.ZTYPE
		and XAS.reada = 'R' );
--GRANT SELECT ON TIS.VL_ZObject to tis_users;

-- VH_ - historic data in table
DROP VIEW IF EXISTS TIS.VH_ZObject CASCADE;
CREATE VIEW TIS.VH_ZObject AS SELECT * FROM TIS.ZObject AS ZO
	WHERE ZO.ZSTA NOT IN ('I','L') AND
	 ZO.ZLVL <= SAM.get_user_slevel() -- MAC - check secrecy 
	 AND
	 -- check pemissions for reading records at scope
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = ZO.ZSID AND XAS.obj_type = ZO.ZTYPE
		and XAS.historya = 'Y' )
	 -- check rejections for reading records at scope
	 AND NOT
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = ZO.ZSID AND XAS.obj_type = ZO.ZTYPE
		and XAS.historya = 'R' );
--GRANT SELECT ON TIS.VH_ZObject to tis_users;

-- VD_ - slice of data in table which was actual for requested TIS.ZDateView.ZDATO
DROP VIEW IF EXISTS TIS.VD_ZObject CASCADE;
CREATE VIEW TIS.VD_ZObject AS SELECT * FROM TIS.ZObject AS ZO
	WHERE ZO.ZSTA <> 'I' AND
	 ( -- check date range
	  ZO.ZDATE <= (SELECT ZDATO FROM  TIS.ZDateView WHERE uid = SAM.get_user())
	  AND ( ZO.ZDATO IS NULL OR ZO.ZDATO >
		(SELECT ZDATO FROM  TIS.ZDateView WHERE uid = SAM.get_user()) )
	  )
	 AND
	 ZO.ZLVL <= SAM.get_user_slevel() -- MAC - check secrecy 
	 AND
	 -- check pemissions for reading records at scope
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = ZO.ZSID AND XAS.obj_type = ZO.ZTYPE
		and XAS.historya = 'Y' )
	 -- check rejections for reading records at scope
	 AND NOT
	 exists (select sid from SAM.ACLScope XAS, SAM.UserGroup XUG
	        WHERE XUG.uid = SAM.get_user() AND XAS.gid = XUG.gid AND
		XAS.sid = ZO.ZSID AND XAS.obj_type = ZO.ZTYPE
		and XAS.historya = 'R' );
--GRANT SELECT ON TIS.VD_ZObject to tis_users;
