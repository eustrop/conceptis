REVOKE ALL ON FUNCTION MSG.create_CChannel(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_subject	varchar(256),
	v_status	char(1),
	v_obj_id	bigint
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION MSG.create_CChannel(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_subject	varchar(256),
	v_status	char(1),
	v_obj_id	bigint
) TO tis_users;
GRANT EXECUTE ON FUNCTION MSG.update_CChannel(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_subject	varchar(256),
	v_status	char(1),
	v_obj_id	bigint
) TO tis_users;
REVOKE EXECUTE ON FUNCTION MSG.delete_CChannel(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION MSG.delete_CChannel(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) TO tis_users;
REVOKE ALL ON FUNCTION MSG.QC_CC(
	v_as	SAM.auditstate,
	v_r	MSG.CChannel
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION MSG.commit_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION MSG.rollback_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION MSG.move_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION MSG.delete_object_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION MSG.cmp_CC(
	v_r	MSG.CChannel,
	v_ro	MSG.CChannel
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION MSG.rdelete_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) FROM PUBLIC;
