REVOKE ALL ON FUNCTION MSG.create_CMsg(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_content	varchar(4096),
	v_msg_id	bigint,
	v_type	varchar(3)
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION MSG.create_CMsg(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_content	varchar(4096),
	v_msg_id	bigint,
	v_type	varchar(3)
) TO tis_users;
GRANT EXECUTE ON FUNCTION MSG.update_CMsg(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_content	varchar(4096),
	v_msg_id	bigint,
	v_type	varchar(3)
) TO tis_users;
REVOKE EXECUTE ON FUNCTION MSG.delete_CMsg(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION MSG.delete_CMsg(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) TO tis_users;
REVOKE ALL ON FUNCTION MSG.QC_CM(
	v_as	SAM.auditstate,
	v_r	MSG.CMsg
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION MSG.commit_CM(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION MSG.rollback_CM(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION MSG.move_CM(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION MSG.delete_object_CM(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION MSG.cmp_CM(
	v_r	MSG.CMsg,
	v_ro	MSG.CMsg
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION MSG.rdelete_CM(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) FROM PUBLIC;
