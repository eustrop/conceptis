REVOKE ALL ON FUNCTION MSG.create_CParty(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_uid	bigint,
	v_role	char(1),
	v_last_read	bigint
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION MSG.create_CParty(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_uid	bigint,
	v_role	char(1),
	v_last_read	bigint
) TO tis_users;
GRANT EXECUTE ON FUNCTION MSG.update_CParty(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_uid	bigint,
	v_role	char(1),
	v_last_read	bigint
) TO tis_users;
REVOKE EXECUTE ON FUNCTION MSG.delete_CParty(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION MSG.delete_CParty(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) TO tis_users;
REVOKE ALL ON FUNCTION MSG.QC_CP(
	v_as	SAM.auditstate,
	v_r	MSG.CParty
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION MSG.commit_CP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION MSG.rollback_CP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION MSG.move_CP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION MSG.delete_object_CP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION MSG.cmp_CP(
	v_r	MSG.CParty,
	v_ro	MSG.CParty
	) FROM PUBLIC;
REVOKE ALL ON FUNCTION MSG.rdelete_CP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) FROM PUBLIC;
