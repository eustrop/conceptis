-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: MSG
-- OBJECT: Channel (C) RECORD: CParty (CP)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

select dic.set_code('TIS_FIELD','MSG.CP01','Пользователь','Айди пользователя');
select dic.set_code('TIS_FIELD','MSG.CP02','Роль','Роль в канале');
select dic.set_code('TIS_FIELD','MSG.CP03','Прочитанное','Последнее прочитанное сообщение');
