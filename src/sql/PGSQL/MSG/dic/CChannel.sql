-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: MSG
-- OBJECT: Channel (C) RECORD: CChannel (CC)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

select dic.set_code('TIS_FIELD','MSG.CC01','Предмет','Предмет обсуждения');
select dic.set_code('TIS_FIELD','MSG.CC02','Состояние Состояние обсуждения','');
select dic.set_code('TIS_FIELD','MSG.CC03','Документ','Ссылка на объект к которому относится обсуждение');
