-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: MSG
-- OBJECT: Channel (C) RECORD: CMsg (CM)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

select dic.set_code('TIS_FIELD','MSG.CM01','Текст','Текст сообщения');
select dic.set_code('TIS_FIELD','MSG.CM02','Ссылка','Ссылка на сообщение в чате');
select dic.set_code('TIS_FIELD','MSG.CM03','Тип','Тип сообщения (M - обычное, L - лайк, P - опрос, A - ответ,...)');
