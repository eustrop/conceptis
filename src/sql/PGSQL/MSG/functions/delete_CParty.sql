-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: MSG
-- OBJECT: Channel (C) RECORD: CParty (CP)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION MSG.delete_CParty(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) RETURNS SAM.execstatus VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 v_as SAM.auditstate;
 v_es SAM.execstatus;
 v_ZO TIS.ZObject%ROWTYPE;
 v_r MSG.CParty%ROWTYPE; -- this version
 v_ro MSG.CParty%ROWTYPE; -- previous version
 v_ri MSG.CParty%ROWTYPE; -- intermediate version
 v_user bigint;
 ZC_ZTOV_UPDATED CONSTANT bigint := -1;
 ZC_ZTOV_DELETED CONSTANT bigint := -2;
 ZC_ZTOV_NOCHANGES CONSTANT bigint := -3;
 ZC_ZTOV_RDELETED CONSTANT bigint := -4;
BEGIN
 -- 0) enter into procedure
 v_as.subsys := 'MSG';
 v_as.eaction :='D';
 v_as.obj_type :='CP';
 v_as.ZID := v_ZRID;
 v_as.ZTYPE := 'MSG.C';
 v_as.ZOID := v_ZOID;
 v_as.ZVER := v_ZVER;
 v_as.proc := 'delete_CParty';
 -- 0.1) get target ZO record and extract missings from
 SELECT * INTO v_ZO FROM TIS.ZObject as ZO WHERE
   ZO.ZOID = v_ZOID AND ZO.ZVER = v_ZVER AND ZO.ZTYPE = 'MSG.C'
   FOR SHARE; -- lock row from concurrent updates 
 v_as.ZLVL := v_ZO.ZLVL; -- FOUND or NOT - indiscriminately
 v_as.sid := v_ZO.ZSID;
 v_as := SAM.do_auditlog_enter(v_as);
 -- 0.2) copy parameters to v_r
 v_r.ZOID = v_ZOID;
 v_r.ZVER = v_ZVER;
 v_r.ZRID = v_ZRID;
 -- nothing to do
<<try>>
BEGIN
 -- 1) pre access control checks
 -- 1.1) check isolation level
 IF NOT SAM.check_isolation() THEN
    v_es := SAM.make_es_transisolation(null,null); EXIT try;
 END IF;
 -- 1.2) identify user
 IF SAM.get_user() IS NULL THEN
    v_es := sam.make_execstatus(null,null,'E_NOUSER',session_user); EXIT try;
 END IF;
 -- 1.3) unallowed nulls
 IF v_r.ZOID IS NULL THEN v_es := SAM.make_es_notnull(null,null,
	'delete_CParty','ZOID'); EXIT try; END IF;
 IF v_r.ZVER IS NULL THEN v_es := SAM.make_es_notnull(null,null,
	'delete_CParty','ZVER'); EXIT try; END IF;
 -- 1.4) check object's verion (ZO record)
 IF v_ZO.ZOID IS NULL THEN -- aka NOT FOUND after (0.1)
   v_es := SAM.make_execstatus(null,null,'E_INVALIDVERSION',
   'MSG.C',CAST(v_ZOID AS text),CAST(v_ZVER AS text)); EXIT try; END IF;
 -- 1.5) check ZSTA
 IF v_ZO.ZSTA <> 'I' THEN
  v_es := SAM.make_execstatus(null,null,'E_NOTOPENED',
  '(Channel,'||v_r.ZOID||','||v_r.ZVER||')'); EXIT try;
 END IF;
 -- 1.6) check ZUID
 IF v_ZO.ZUID <> SAM.get_user() THEN
  v_es := SAM.make_execstatus(null,null,'E_NOTOWNER',
  '(Channel,'||v_r.ZOID||','||v_r.ZVER||')'); EXIT try;
 END IF;
 -- 2) check access
 IF v_ZO.ZVER = 1 THEN 
  v_ps := SAM.check_access_create(v_as,v_ZO.ZSID,v_ZO.ZTYPE,v_ZO.ZLVL);
 ELSE
  v_ps := SAM.check_access_write(v_as,v_ZO.ZSID,v_ZO.ZTYPE,v_ZO.ZOID,v_ZO.ZLVL);
 END IF;
 v_as := v_ps.a;
 -- 3) lock required tables
 -- LOCK TABLE MSG.CParty NOT REQUIRED;
 -- 4) check data
 -- 4.1) get requested record version
 -- 4.1.1) get intermediate version
 SELECT * INTO v_ri FROM MSG.CParty ZR WHERE ZR.ZOID = v_r.ZOID AND ZR.ZRID = v_r.ZRID
   AND ZR.ZVER = v_r.ZVER AND ZR.ZTOV < 0 FOR UPDATE;
 -- 4.1.2) get current version 
 IF NOT FOUND THEN -- if no intermediate one
  SELECT * INTO v_ro FROM MSG.CParty ZR WHERE ZR.ZOID = v_r.ZOID
  AND ZR.ZRID = v_r.ZRID AND ZR.ZTOV = 0 FOR SHARE;
 END IF;
 -- abort if no record
 IF COALESCE(v_ri.ZOID, v_ro.ZOID) IS NULL OR
  v_ri.ZTOV IN (ZC_ZTOV_DELETED, ZC_ZTOV_RDELETED) THEN
    v_es := SAM.make_execstatus(null,null,'E_NORECORD',
    'MSG.CParty','ZRID',''||v_r.ZRID); EXIT try;
 END IF;
 -- 4.2) set record header
 v_r.ZSID := v_ZO.ZSID;
 v_r.ZLVL := v_ZO.ZLVL;
 v_r.ZPID := v_ro.ZPID;
 v_r.ZTOV := ZC_ZTOV_DELETED;
 -- 4.99) Quality Control
  -- not applicable
 -- 5) add or update record
 IF v_ri.ZVER IS NULL THEN
  v_ro.ZVER := v_r.ZVER;
  v_ro.ZTOV := ZC_ZTOV_DELETED;
  INSERT INTO MSG.CParty values(v_ro.*);
 ELSE
  UPDATE MSG.CParty AS ZR SET ZTOV = ZC_ZTOV_DELETED WHERE
    ZR.ZOID = v_r.ZOID AND ZR.ZVER = v_r.ZVER AND ZR.ZRID = v_r.ZRID;
 END IF;
 v_as := SAM.do_auditlog_da_sole(v_as,'2',null,null,v_r.ZSID,v_r.ZRID);
 -- 6) exit
 v_es := sam.make_execstatus(v_r.ZRID,v_r.ZVER,'I_SUCCESS');
EXCEPTION
 WHEN OTHERS THEN
	v_es:=sam.make_execstatus(v_r.ZRID,v_r.ZVER,'E_SQL',SQLSTATE,SQLERRM);
END;
-- FINALLY:
 PERFORM SAM.do_auditlog_exit(v_as,v_es);
 RETURN v_es;
END $$;
COMMIT TRANSACTION;
