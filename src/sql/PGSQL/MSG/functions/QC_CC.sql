-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: MSG
-- OBJECT: Channel (C) RECORD: CChannel (CC)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION MSG.QC_CC(
	v_as	SAM.auditstate,
	v_r	MSG.CChannel
) RETURNS SAM.procstate VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 i	integer;
 ZC_ZTOV_UPDATED CONSTANT bigint := -1;
 ZC_ZTOV_DELETED CONSTANT bigint := -2;
 ZC_ZTOV_NOCHANGES CONSTANT bigint := -3;
 ZC_ZTOV_RDELETED CONSTANT bigint := -4;
BEGIN
 v_ps.success := FALSE;
<<try>>
BEGIN
 -- 1) check NOT NULLs
 IF v_r.subject IS NULL THEN v_ps.e := SAM.make_es_notnull(v_r.ZRID,v_r.ZVER,
	'MSG.CChannel','subject'); EXIT try; END IF;
 IF v_r.status IS NULL THEN v_ps.e := SAM.make_es_notnull(v_r.ZRID,v_r.ZVER,
	'MSG.CChannel','status'); EXIT try; END IF;
 -- 2) check dictionary fields
 IF NOT dic.check_code('MSG_CHANNEL_STATUS',v_r.status) THEN
   v_ps.e := SAM.make_es_invalidcode(v_r.ZRID,v_r.ZVER,v_r.status,
   'MSG.CChannel.status','MSG_CHANNEL_STATUS'); EXIT try; END IF;
 -- 3) check uniqueness
 -- 4) check references
 -- 4.1) check std parent/child references
   -- not required
 -- 4.2) check scope references from data fields
 -- 99) all tests passed
 v_ps.success := TRUE;
END;
-- FINALLY:
 v_ps.a := v_as;
 RETURN v_ps;
END $$;
COMMIT TRANSACTION;
