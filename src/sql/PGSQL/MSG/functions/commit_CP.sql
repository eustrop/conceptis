-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: MSG
-- OBJECT: Channel (C) RECORD: CParty (CP)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION MSG.commit_CP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
) RETURNS SAM.procstate VOLATILE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
 v_ps SAM.procstate;
 v_r  MSG.CParty%ROWTYPE; -- for FQC
 v_recount bigint;
 ZC_ZTOV_UPDATED CONSTANT bigint := -1;
 ZC_ZTOV_DELETED CONSTANT bigint := -2;
 ZC_ZTOV_NOCHANGES CONSTANT bigint := -3;
 ZC_ZTOV_RDELETED CONSTANT bigint := -4;
BEGIN
 v_ps.success := FALSE;
<<try>>
BEGIN
 -- 1) make FQC (final QC)
 -- 1.1) check NOT NULLs
  -- not necessary
 -- 1.2) check dictionary fields
  -- may be in the future...
 -- 1.3) check uniqueness
 -- 1.4) check references
  -- may be in the future...
 -- 1.5) check MAXREC and MINREC
  -- not applicable for MSG.CParty
 -- 1.6) check ZNAME
 -- 1.7) doing final quality control manually (FQCM)
  -- may be in the future...
 -- 2) update table
  UPDATE MSG.CParty ZR SET ZTOV = v_ZO.ZVER WHERE ZR.ZTOV = 0 AND ZR.ZOID = v_ZO.ZOID AND EXISTS
    (SELECT ZRID FROM MSG.CParty ZR2 WHERE 
      ZR2.ZTOV IN (ZC_ZTOV_UPDATED, ZC_ZTOV_DELETED, ZC_ZTOV_RDELETED) AND
      ZR2.ZOID = ZR.ZOID AND ZR2.ZRID = ZR.ZRID AND ZR2.ZVER = v_ZO.ZVER);
  UPDATE MSG.CParty ZR SET ZTOV = 0 WHERE ZTOV = ZC_ZTOV_UPDATED AND ZR.ZOID = v_ZO.ZOID
                                  AND ZR.ZVER = v_ZO.ZVER;
  DELETE FROM MSG.CParty ZR WHERE ZR.ZTOV < 0 AND ZR.ZOID = v_ZO.ZOID AND ZR.ZVER = v_ZO.ZVER;
 -- 3) all tests passed
 v_ps.success := TRUE;
END;
-- FINALLY:
 v_ps.a := v_as;
 RETURN v_ps;
END $$;
COMMIT TRANSACTION;
