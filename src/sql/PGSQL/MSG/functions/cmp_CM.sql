-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: MSG
-- OBJECT: Channel (C) RECORD: CMsg (CM)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION MSG.cmp_CM(
	v_r	MSG.CMsg,
	v_ro	MSG.CMsg
) RETURNS boolean STABLE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
BEGIN
 IF v_r.content <> v_ro.content THEN RETURN FALSE; END IF;
  IF (v_r.content IS NULL OR v_ro.content IS NULL) AND
  NOT COALESCE(v_r.content,v_ro.content) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.msg_id <> v_ro.msg_id THEN RETURN FALSE; END IF;
  IF (v_r.msg_id IS NULL OR v_ro.msg_id IS NULL) AND
  NOT COALESCE(v_r.msg_id,v_ro.msg_id) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.type <> v_ro.type THEN RETURN FALSE; END IF;
  IF (v_r.type IS NULL OR v_ro.type IS NULL) AND
  NOT COALESCE(v_r.type,v_ro.type) IS NULL THEN RETURN FALSE; END IF;
 RETURN TRUE; -- both records equal
END $$;
COMMIT TRANSACTION;
