-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009-2019
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- $Id$
--
-- SUBSYSTEM: MSG
-- OBJECT: Channel (C) RECORD: CParty (CP)

-- WARNING! this code produced by automatic codegeneration tool
--          located at src/sql/PGSQL/codegen/tools/codegen.awk
--          do not change this file directly but modify the above one. 

BEGIN TRANSACTION;
CREATE OR REPLACE FUNCTION MSG.cmp_CP(
	v_r	MSG.CParty,
	v_ro	MSG.CParty
) RETURNS boolean STABLE
  LANGUAGE plpgSQL SECURITY DEFINER as $$
BEGIN
 IF v_r.uid <> v_ro.uid THEN RETURN FALSE; END IF;
  IF (v_r.uid IS NULL OR v_ro.uid IS NULL) AND
  NOT COALESCE(v_r.uid,v_ro.uid) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.role <> v_ro.role THEN RETURN FALSE; END IF;
  IF (v_r.role IS NULL OR v_ro.role IS NULL) AND
  NOT COALESCE(v_r.role,v_ro.role) IS NULL THEN RETURN FALSE; END IF;
 IF v_r.last_read <> v_ro.last_read THEN RETURN FALSE; END IF;
  IF (v_r.last_read IS NULL OR v_ro.last_read IS NULL) AND
  NOT COALESCE(v_r.last_read,v_ro.last_read) IS NULL THEN RETURN FALSE; END IF;
 RETURN TRUE; -- both records equal
END $$;
COMMIT TRANSACTION;
