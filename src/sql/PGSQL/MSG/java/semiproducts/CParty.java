// MSG.CParty table semiproducts

// view record MSG.CParty
was.printTabField(fs,CParty.F_UID);
was.printTabFieldDic(fs,CParty.F_ROLE,getDic("MSG_PARTY_ROLE"));
was.printTabField(fs,CParty.F_LAST_READ);

// edit record MSG.CParty
was.printTabFieldInput(fsCP.get(CP.F_UID));
was.printTabFieldInputDic(fsCP.get(CP.F_ROLE),getDic("MSG_PARTY_ROLE"));
was.printTabFieldInput(fsCP.get(CP.F_LAST_READ));

// table caption
was.printHCellCaption(fs.get(CParty.F_UID));
was.printHCellCaption(fs.get(CParty.F_ROLE));
was.printHCellCaption(fs.get(CParty.F_LAST_READ));

// table row from ZFieldSet
was.printCellField(fs.get(CParty.F_UID));
was.printCellField(fs.get(CParty.F_ROLE));
was.printCellField(fs.get(CParty.F_LAST_READ));

// table row from zDAO Record object
was.printCellValue(wam.obj2string(r2.uid));
was.printCellValue(wam.obj2string(r2.role));
was.printCellValue(wam.obj2string(r2.last_read));

// edit form from ZFieldSet
was.printTabFieldInput( fs, ACLScope.F_UID);
was.printTabFieldInput( fs, ACLScope.F_ROLE);
was.printTabFieldInput( fs, ACLScope.F_LAST_READ);
