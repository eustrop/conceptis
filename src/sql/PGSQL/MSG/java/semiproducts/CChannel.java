// MSG.CChannel table semiproducts

// view record MSG.CChannel
was.printTabField(fs,CChannel.F_SUBJECT);
was.printTabFieldDic(fs,CChannel.F_STATUS,getDic("MSG_CHANNEL_STATUS"));
was.printTabField(fs,CChannel.F_OBJ_ID);

// edit record MSG.CChannel
was.printTabFieldInput(fsCC.get(CC.F_SUBJECT));
was.printTabFieldInputDic(fsCC.get(CC.F_STATUS),getDic("MSG_CHANNEL_STATUS"));
was.printTabFieldInput(fsCC.get(CC.F_OBJ_ID));

// table caption
was.printHCellCaption(fs.get(CChannel.F_SUBJECT));
was.printHCellCaption(fs.get(CChannel.F_STATUS));
was.printHCellCaption(fs.get(CChannel.F_OBJ_ID));

// table row from ZFieldSet
was.printCellField(fs.get(CChannel.F_SUBJECT));
was.printCellField(fs.get(CChannel.F_STATUS));
was.printCellField(fs.get(CChannel.F_OBJ_ID));

// table row from zDAO Record object
was.printCellValue(wam.obj2string(r2.subject));
was.printCellValue(wam.obj2string(r2.status));
was.printCellValue(wam.obj2string(r2.obj_id));

// edit form from ZFieldSet
was.printTabFieldInput( fs, ACLScope.F_SUBJECT);
was.printTabFieldInput( fs, ACLScope.F_STATUS);
was.printTabFieldInput( fs, ACLScope.F_OBJ_ID);
