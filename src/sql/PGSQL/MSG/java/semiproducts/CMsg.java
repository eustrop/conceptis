// MSG.CMsg table semiproducts

// view record MSG.CMsg
was.printTabField(fs,CMsg.F_CONTENT);
was.printTabField(fs,CMsg.F_MSG_ID);
was.printTabFieldDic(fs,CMsg.F_TYPE,getDic("MSG_MESSAGE_TYPE"));

// edit record MSG.CMsg
was.printTabFieldInput(fsCM.get(CM.F_CONTENT));
was.printTabFieldInput(fsCM.get(CM.F_MSG_ID));
was.printTabFieldInputDic(fsCM.get(CM.F_TYPE),getDic("MSG_MESSAGE_TYPE"));

// table caption
was.printHCellCaption(fs.get(CMsg.F_CONTENT));
was.printHCellCaption(fs.get(CMsg.F_MSG_ID));
was.printHCellCaption(fs.get(CMsg.F_TYPE));

// table row from ZFieldSet
was.printCellField(fs.get(CMsg.F_CONTENT));
was.printCellField(fs.get(CMsg.F_MSG_ID));
was.printCellField(fs.get(CMsg.F_TYPE));

// table row from zDAO Record object
was.printCellValue(wam.obj2string(r2.content));
was.printCellValue(wam.obj2string(r2.msg_id));
was.printCellValue(wam.obj2string(r2.type));

// edit form from ZFieldSet
was.printTabFieldInput( fs, ACLScope.F_CONTENT);
was.printTabFieldInput( fs, ACLScope.F_MSG_ID);
was.printTabFieldInput( fs, ACLScope.F_TYPE);
