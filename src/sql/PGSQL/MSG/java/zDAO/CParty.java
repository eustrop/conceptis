// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2011
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
// WARNING! this code produced by automatic codegeneration tool
//          located at src/sql/PGSQL/codegen/tools/codegen.awk
//          In most cases you should'n change this file, but the above one.
//
// $Id$
//

package ru.mave.ConcepTIS.dao.MSG;

import ru.mave.ConcepTIS.dao.*;
import java.math.BigDecimal;

/** MSG.CParty table.
 *
 */
public class CParty extends DORecord
{
public Long	uid;
public String	role;
public Long	last_read;

public String getCaption(){return(null);}

// Strings

public final static String TAB_CODE = "CP";
public final static String TAB_NAME = "CParty";
public final static String SUBSYS_CODE = "MSG";

public final static String F_UID = "uid";
public final static String F_ROLE = "role";
public final static String F_LAST_READ = "last_read";

public final static String FC_UID = "MSG.CP01";
public final static String FCS_UID = "01";
public final static String FC_ROLE = "MSG.CP02";
public final static String FCS_ROLE = "02";
public final static String FC_LAST_READ = "MSG.CP03";
public final static String FCS_LAST_READ = "03";

// children management

public DORecord[] createChildrenFactory()
{
 return(new DORecord[0]);
}

public String[] getChildTabCodes()
{
 return(new String[0]);
}

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 super.getFromRS(rs);
 uid = getRSLong(rs,8);
 role = rtrimString(getRSString(rs,9));
 last_read = getRSLong(rs,10);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 super.getFromFS(fs);
 uid = getFSLong(fs,F_UID);
 role = getFSString(fs,F_ROLE);
 last_read = getFSLong(fs,F_LAST_READ);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSLong(ps,4,uid);
 setPSString(ps,5,role);
 setPSLong(ps,6,last_read);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = super.toFieldSet();
 fs.add(new ZField(F_UID,uid,FC_UID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_ROLE,role,FC_ROLE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_LAST_READ,last_read,FC_LAST_READ,ZField.T_UNKNOWN,0));
 return(fs);
} //

public String getSubsysCode(){return(SUBSYS_CODE);}
public String getTabCode(){return(TAB_CODE);}
public String getTabName(){return(TAB_NAME);}
public String getParentTabCode(){return(CChannel.TAB_CODE);}
public int getNumOfFields() {return(3);}

public DORecord createFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
 {return(new CParty(rs));}
public DORecord createFromFS(ZFieldSet fs)
 throws ZException
 {return(new CParty(fs));};
public DORecord createInstance(){return(new CParty());}

// constructors
public CParty() { }
public CParty(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public CParty(java.sql.ResultSet rs)
 throws java.sql.SQLException { this.getFromRS(rs); }

} //CParty
