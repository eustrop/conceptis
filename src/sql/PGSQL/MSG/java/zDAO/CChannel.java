// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2011
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
// WARNING! this code produced by automatic codegeneration tool
//          located at src/sql/PGSQL/codegen/tools/codegen.awk
//          In most cases you should'n change this file, but the above one.
//
// $Id$
//

package ru.mave.ConcepTIS.dao.MSG;

import ru.mave.ConcepTIS.dao.*;
import java.math.BigDecimal;

/** MSG.CChannel table.
 *
 */
public class CChannel extends DORecord
{
public String	subject;
public String	status;
public Long	obj_id;

public String getCaption(){return(null);}

// Strings

public final static String TAB_CODE = "CC";
public final static String TAB_NAME = "CChannel";
public final static String SUBSYS_CODE = "MSG";

public final static String F_SUBJECT = "subject";
public final static String F_STATUS = "status";
public final static String F_OBJ_ID = "obj_id";

public final static String FC_SUBJECT = "MSG.CC01";
public final static String FCS_SUBJECT = "01";
public final static String FC_STATUS = "MSG.CC02";
public final static String FCS_STATUS = "02";
public final static String FC_OBJ_ID = "MSG.CC03";
public final static String FCS_OBJ_ID = "03";

// children management

public DORecord[] createChildrenFactory()
{
 DORecord dor[]=new DORecord[2];
 dor[0]=new CMsg();
 dor[1]=new CParty();
 return(dor);
}

public String[] getChildTabCodes()
{
 String c[]=new String[2];
 c[0]=CMsg.TAB_CODE;
 c[1]=CParty.TAB_CODE;
 return(c);
}

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 super.getFromRS(rs);
 subject = getRSString(rs,8);
 status = rtrimString(getRSString(rs,9));
 obj_id = getRSLong(rs,10);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 super.getFromFS(fs);
 subject = getFSString(fs,F_SUBJECT);
 status = getFSString(fs,F_STATUS);
 obj_id = getFSLong(fs,F_OBJ_ID);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSString(ps,4,subject);
 setPSString(ps,5,status);
 setPSLong(ps,6,obj_id);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = super.toFieldSet();
 fs.add(new ZField(F_SUBJECT,subject,FC_SUBJECT,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_STATUS,status,FC_STATUS,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_OBJ_ID,obj_id,FC_OBJ_ID,ZField.T_UNKNOWN,0));
 return(fs);
} //

public String getSubsysCode(){return(SUBSYS_CODE);}
public String getTabCode(){return(TAB_CODE);}
public String getTabName(){return(TAB_NAME);}
public String getParentTabCode(){return(null);}
public int getNumOfFields() {return(3);}

public DORecord createFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
 {return(new CChannel(rs));}
public DORecord createFromFS(ZFieldSet fs)
 throws ZException
 {return(new CChannel(fs));};
public DORecord createInstance(){return(new CChannel());}

// constructors
public CChannel() { }
public CChannel(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public CChannel(java.sql.ResultSet rs)
 throws java.sql.SQLException { this.getFromRS(rs); }

} //CChannel
