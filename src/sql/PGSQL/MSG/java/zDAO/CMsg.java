// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2011
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
// WARNING! this code produced by automatic codegeneration tool
//          located at src/sql/PGSQL/codegen/tools/codegen.awk
//          In most cases you should'n change this file, but the above one.
//
// $Id$
//

package ru.mave.ConcepTIS.dao.MSG;

import ru.mave.ConcepTIS.dao.*;
import java.math.BigDecimal;

/** MSG.CMsg table.
 *
 */
public class CMsg extends DORecord
{
public String	content;
public Long	msg_id;
public String	type;

public String getCaption(){return(null);}

// Strings

public final static String TAB_CODE = "CM";
public final static String TAB_NAME = "CMsg";
public final static String SUBSYS_CODE = "MSG";

public final static String F_CONTENT = "content";
public final static String F_MSG_ID = "msg_id";
public final static String F_TYPE = "type";

public final static String FC_CONTENT = "MSG.CM01";
public final static String FCS_CONTENT = "01";
public final static String FC_MSG_ID = "MSG.CM02";
public final static String FCS_MSG_ID = "02";
public final static String FC_TYPE = "MSG.CM03";
public final static String FCS_TYPE = "03";

// children management

public DORecord[] createChildrenFactory()
{
 return(new DORecord[0]);
}

public String[] getChildTabCodes()
{
 return(new String[0]);
}

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 super.getFromRS(rs);
 content = getRSString(rs,8);
 msg_id = getRSLong(rs,9);
 type = getRSString(rs,10);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 super.getFromFS(fs);
 content = getFSString(fs,F_CONTENT);
 msg_id = getFSLong(fs,F_MSG_ID);
 type = getFSString(fs,F_TYPE);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSString(ps,4,content);
 setPSLong(ps,5,msg_id);
 setPSString(ps,6,type);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = super.toFieldSet();
 fs.add(new ZField(F_CONTENT,content,FC_CONTENT,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_MSG_ID,msg_id,FC_MSG_ID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_TYPE,type,FC_TYPE,ZField.T_UNKNOWN,0));
 return(fs);
} //

public String getSubsysCode(){return(SUBSYS_CODE);}
public String getTabCode(){return(TAB_CODE);}
public String getTabName(){return(TAB_NAME);}
public String getParentTabCode(){return(CChannel.TAB_CODE);}
public int getNumOfFields() {return(3);}

public DORecord createFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
 {return(new CMsg(rs));}
public DORecord createFromFS(ZFieldSet fs)
 throws ZException
 {return(new CMsg(fs));};
public DORecord createInstance(){return(new CMsg());}

// constructors
public CMsg() { }
public CMsg(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public CMsg(java.sql.ResultSet rs)
 throws java.sql.SQLException { this.getFromRS(rs); }

} //CMsg
