DROP FUNCTION MSG.create_CChannel(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_subject	varchar(256),
	v_status	char(1),
	v_obj_id	bigint
) CASCADE;
DROP FUNCTION MSG.update_CChannel(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_subject	varchar(256),
	v_status	char(1),
	v_obj_id	bigint
) CASCADE;
DROP FUNCTION MSG.delete_CChannel(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) CASCADE;
DROP FUNCTION IF EXISTS MSG.QC_CC(
	v_as	SAM.auditstate,
	v_r	MSG.CChannel
	) CASCADE;
DROP FUNCTION IF EXISTS MSG.commit_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS MSG.rollback_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS MSG.move_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS MSG.delete_object_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS MSG.cmp_CC(
	v_r	MSG.CChannel,
	v_ro	MSG.CChannel
	) CASCADE;
DROP FUNCTION IF EXISTS MSG.rdelete_CC(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) CASCADE;
