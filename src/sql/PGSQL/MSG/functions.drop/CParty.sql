DROP FUNCTION MSG.create_CParty(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_uid	bigint,
	v_role	char(1),
	v_last_read	bigint
) CASCADE;
DROP FUNCTION MSG.update_CParty(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_uid	bigint,
	v_role	char(1),
	v_last_read	bigint
) CASCADE;
DROP FUNCTION MSG.delete_CParty(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) CASCADE;
DROP FUNCTION IF EXISTS MSG.QC_CP(
	v_as	SAM.auditstate,
	v_r	MSG.CParty
	) CASCADE;
DROP FUNCTION IF EXISTS MSG.commit_CP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS MSG.rollback_CP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS MSG.move_CP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS MSG.delete_object_CP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS MSG.cmp_CP(
	v_r	MSG.CParty,
	v_ro	MSG.CParty
	) CASCADE;
DROP FUNCTION IF EXISTS MSG.rdelete_CP(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) CASCADE;
