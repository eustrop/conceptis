DROP FUNCTION MSG.create_CMsg(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZPID bigint, -- ZRID of parent record or null
	v_content	varchar(4096),
	v_msg_id	bigint,
	v_type	varchar(3)
) CASCADE;
DROP FUNCTION MSG.update_CMsg(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint, -- ZRID of target record
	v_content	varchar(4096),
	v_msg_id	bigint,
	v_type	varchar(3)
) CASCADE;
DROP FUNCTION MSG.delete_CMsg(
	v_ZOID bigint,
	v_ZVER bigint,
	v_ZRID bigint -- ZRID of target record
) CASCADE;
DROP FUNCTION IF EXISTS MSG.QC_CM(
	v_as	SAM.auditstate,
	v_r	MSG.CMsg
	) CASCADE;
DROP FUNCTION IF EXISTS MSG.commit_CM(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS MSG.rollback_CM(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS MSG.move_CM(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS MSG.delete_object_CM(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject
	) CASCADE;
DROP FUNCTION IF EXISTS MSG.cmp_CM(
	v_r	MSG.CMsg,
	v_ro	MSG.CMsg
	) CASCADE;
DROP FUNCTION IF EXISTS MSG.rdelete_CM(
	v_as	SAM.auditstate,
	v_ZO	TIS.ZObject,
	v_ZPID	bigint
	) CASCADE;
