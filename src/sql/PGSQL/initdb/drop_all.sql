-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE at the project's root directory
--
-- purpose: drop all dbs and users from cluster

DROP DATABASE ConcepTISDB;
drop USER tisc;
select 'tis_users ROLE can be used by other ConcepTIS derived systems, drop it manually if you need'
"WARNING: on noDROP ROLE tis_users";
--drop ROLE tis_users;
drop USER tiscuser1;
drop USER tiscuser2;
drop USER tiscuser3;

