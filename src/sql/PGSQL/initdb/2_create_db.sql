-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE at the project's root directory
--

-- 2. creating ConcepTIS database. must be done by superuser
CREATE DATABASE conceptisdb OWNER tisc ENCODING 'UTF8';

