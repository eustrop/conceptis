-- ConcepTIS project/QTIS project
-- (c) Alex V Eustrop 2009
-- (c) Alex V Eustrop & EustroSoft.org 2023
-- see LICENSE.ConcepTIS at the project's root directory
--
-- purpose: create DBMS users needed for system initialising and testing
--	tisc - owner of all DB objects (tables,views,functions)
--	tiscuser1,tiscuser2,... - accounts for regular users
--	tis_users - group role for joining all ConcepTIS users
--

-- 1. creating users. must be done at postegres db, by superuser
CREATE USER tisc LOGIN INHERIT;
CREATE ROLE tis_users NOLOGIN NOINHERIT;
CREATE USER tiscuser1 LOGIN INHERIT IN ROLE tis_users;
CREATE USER tiscuser2 LOGIN INHERIT IN ROLE tis_users;
CREATE USER tiscuser3 LOGIN INHERIT IN ROLE tis_users;
CREATE USER tiscuser4 LOGIN INHERIT IN ROLE tis_users;
-- dbpool users (for services who can act as any other users)
CREATE USER tiscpool1 LOGIN INHERIT IN ROLE tis_users;
CREATE USER tiscpool2 LOGIN INHERIT IN ROLE tis_users;


-- 2. QTIS users
--CREATE USER qtis_dbo LOGIN INHERIT;
--CREATE ROLE qtis_users NOLOGIN NOINHERIT;
--
CREATE USER qtisadmin LOGIN INHERIT IN ROLE tis_users;
CREATE USER qtisoperator LOGIN INHERIT IN ROLE tis_users;
CREATE USER qtisreplicator LOGIN INHERIT IN ROLE tis_users;
CREATE USER qtiswww LOGIN INHERIT IN ROLE tis_users;
CREATE USER qtisguest LOGIN INHERIT IN ROLE tis_users;
CREATE USER qtisnobody LOGIN INHERIT IN ROLE tis_users;
--
CREATE USER qtisuser1 LOGIN INHERIT IN ROLE tis_users;
CREATE USER qtisuser2 LOGIN INHERIT IN ROLE tis_users;
CREATE USER qtisuser3 LOGIN INHERIT IN ROLE tis_users;
