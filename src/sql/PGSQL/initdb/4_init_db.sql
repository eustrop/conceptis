-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE at the project's root directory
--

-- 3. initial configuration of ConcepTISDB. must be done by "tisc"
--    user which is owner of that database

-- 3.2. deny connection to this DB anyone except tis_users
REVOKE ALL ON DATABASE ConcepTISDB FROM PUBLIC;
GRANT CONNECT ON DATABASE ConcepTISDB TO tis_users;

-- 3.3. create separate schemas for dictionary (dic), Security Access
--      Management (SAM), general-purpose TIS/SQL objects and
--	functions (TIS).
--	Application-specific ConcepTIS objects placed at separate
--	schema (TISC).

CREATE SCHEMA dic;
GRANT USAGE ON SCHEMA dic TO PUBLIC;

CREATE SCHEMA sam;
GRANT USAGE ON SCHEMA sam TO PUBLIC;

CREATE SCHEMA tis;
GRANT USAGE ON SCHEMA tis TO PUBLIC;

CREATE SCHEMA tisc;
GRANT USAGE ON SCHEMA tisc TO PUBLIC;

CREATE SCHEMA fs;
GRANT USAGE ON SCHEMA fs TO PUBLIC;

CREATE SCHEMA cop;
GRANT USAGE ON SCHEMA cop TO PUBLIC;

CREATE SCHEMA qr;
GRANT USAGE ON SCHEMA qr TO PUBLIC;

CREATE SCHEMA msg;
GRANT USAGE ON SCHEMA msg TO PUBLIC;

-- 3.4. install PLPgSQL.
--      this could be used at old versions of PostgeSQL:
-- CREATE FUNCTION plpgsql_call_handler() RETURNS language_handler AS
--    '$libdir/plpgsql' LANGUAGE C;
-- CREATE FUNCTION plpgsql_validator(oid) RETURNS void AS
--  '$libdir/plpgsql' LANGUAGE C;
-- CREATE TRUSTED PROCEDURAL LANGUAGE plpgsql
--   HANDLER plpgsql_call_handler VALIDATOR plpgsql_validator;

CREATE PROCEDURAL LANGUAGE plpgsql;
