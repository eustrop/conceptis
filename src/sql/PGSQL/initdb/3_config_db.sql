-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE at the project's root directory
--

-- 2. configure ConcepTIS database. must be done by superuser at ConcepTISDB
-- 2.1. disabling usage of "public" schema complitely
-- REVOKE CREATE ON SCHEMA public FROM PUBLIC;
-- REVOKE USAGE ON SCHEMA public FROM PUBLIC;
-- 2.2 ... or just drop it at all.
DROP SCHEMA public;

