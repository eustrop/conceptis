-- 5. Создание процедуры для манипулирования данными
-- 5.1 Тип данных для хранения результата исполнения функции
CREATE TYPE retstatus AS (id int, errcode int, errdesc varchar(255));
-- 5.2 Функция для добавления записи Somedata
CREATE OR REPLACE FUNCTION create_Somedata(scope_id int, value varchar(32))
RETURNS retstatus LANGUAGE plpgSQL SECURITY DEFINER as $$
DECLARE
rets retstatus;
v tis.Somedata%ROWTYPE;
i int;
BEGIN
v.scope_id := scope_id; v.value = value;
-- 1) Проверить уровень изоляции транзакции
if pg_catalog.current_setting('transaction_isolation') <> 'read committed' then
rets.id:=null; rets.errcode:=1;
rets.errdesc:='Требуется уровень изоляции READ COMMITTED';
RETURN rets ;
end if;
-- 2) Идентифицировать пользователя
v.user_id := tis.sam_get_user();
if v.user_id is null then
rets.id:=null; rets.errcode:=2; rets.errdesc:='Пользователь не определен';
RETURN rets ;
end if;
-- 3) Проверить права доступа
SELECT INTO i count(*) FROM tis.SAMACL acl where acl.user_id = v.user_id
and acl.scope_id = v.scope_id and acl.write_allowed='Y';
if i = 0 then
rets.id:=null; rets.errcode:=3; rets.errdesc:='Отказ в доступе';
RETURN rets ;
end if;
-- Выполнить добавление записи
v.id=nextval('seq_Somedata');
INSERT INTO tis.Somedata values(v.id,v.scope_id,v.user_id,v.value);
-- Вернуться
rets.id:=v.id; rets.errcode:=0; rets.errdesc:='Сделано';
RETURN rets ;
END $$;
GRANT EXECUTE ON FUNCTION tis.create_Somedata(int,varchar(32)) TO PUBLIC;
