-- 4. view для чтения прикладных данных (таблица была создана при инициализации)
CREATE VIEW tis.V_Somedata as select * from tis.Somedata where exists
(select user_id from tis.SAMACL where user_id = tis.sam_get_user() and
Somedata.scope_id = scope_id and read_allowed = 'Y');
GRANT SELECT ON tis.V_Somedata TO PUBLIC;
