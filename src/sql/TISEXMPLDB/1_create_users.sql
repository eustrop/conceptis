-- 1. Создать пользователей и БД (выполняет администратор СУБД (pgsql/postgres)
CREATE USER tis WITH PASSWORD '' ;
CREATE USER tisuser1 WITH PASSWORD '';
CREATE USER tisuser2 WITH PASSWORD '';
CREATE DATABASE tisexmpldb OWNER tis ENCODING 'UTF8';
-- 1.1. Выполнить в БД tisexmpldb от имени пользователя pgsql или postgres,
-- чтобы запретить ординарным пользователям создавать таблицы
-- и функции, доступные всем остальным пользователям в схеме public
-- REVOKE CREATE ON SCHEMA public FROM PUBLIC;
-- REVOKE USAGE ON SCHEMA public FROM PUBLIC;
-- 1.2 или просто удалите схему public, в созданной БД
-- DROP SCHEMA public;
