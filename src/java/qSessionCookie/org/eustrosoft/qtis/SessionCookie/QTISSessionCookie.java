/*
 ConcepTIS project/QTIS project
 (c) Alex V Eustrop & eustrosoft.org 2009,2023
 LICENSE (this file) : BALES, BSD, MIT on your choice (see bales.eustrosoft.org)
 also see LICENSE at the project's root directory for the whole project license

 Purpose: manage QTIS session cookies (get/set/delete)

 History
  2023/04/25 implemened based on dbpool.jsp research 65 lines
*/

package org.eustrosoft.qtis.SessionCookie;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Cookie;

// QDBPool qdbp = QDBPool.get("MyPool");
// session_cookie = QTISSessionCookie(request,response);
// cookie_v = session_cookie.value();
// session_cookie.delete();
// QDBPSession qdbs = qdbp.logon(login,password);
// session_cookie.set(qdbs.getSecretSessionCookie(),qdbs.getSessionCookieMaxAge());

public final class QTISSessionCookie {
 // static version fields & methods
 private static int ver_major = 0;
 private static int ver_minor = 1;
 private static int ver_build = 2023043201;
 private static String ver_status = "ALPHA4";
 public static int getVerMajor(){return(ver_major);}
 public static int getVerMinor(){return(ver_minor);}
 public static int getVerBuild(){return(ver_build);}
 public static String getVerStatus(){return(ver_status);}
 public static String getVer(){return(""+ver_major+"."+ver_minor+"-"+ver_status+"-b"+ver_build);}
 // instance fields
 private final HttpServletRequest request;
 private final HttpServletResponse response;
 private String session_cookie_value = null;
 private String QTIS_SESSION_COOKIE= "QTIS_SESSION"; // cookie name for session
 private String QTIS_SESSION_COOKIE_PATH = "/";

 public synchronized void setCookieName(String newName) { this.QTIS_SESSION_COOKIE = newName; }
 public synchronized void setPath(String path) { this.QTIS_SESSION_COOKIE_PATH = path; }
 public synchronized String getCookieValue() { return(value()); }
 public String value(){return(session_cookie_value); }

 public synchronized void setCookie(String cookieValue, int MaxAge) {setCookie(cookieValue,MaxAge,true,true);}
 public synchronized void setCookie(String cookieValue, int MaxAge, boolean httpOnly, boolean secure) {set(cookieValue,MaxAge,httpOnly,secure,QTIS_SESSION_COOKIE_PATH); }
 public void set(String value, int MaxAge){set(value,MaxAge,true,true,QTIS_SESSION_COOKIE_PATH);}
 public void set(String value, int MaxAge, boolean isHttpOnly, boolean isSecure, String path)
 {
    Cookie session_cookie = new Cookie(QTIS_SESSION_COOKIE,value);
    session_cookie.setHttpOnly(isHttpOnly); // true for not JS
    session_cookie.setSecure(isSecure); // true for only over https
    session_cookie.setPath(path); // whole site
    session_cookie.setMaxAge(MaxAge); // one day
    response.addCookie(session_cookie);
    session_cookie_value=value;
 }

 public synchronized void deleteCookie() { delete();}
 public void delete(){
    Cookie session_cookie = new Cookie(QTIS_SESSION_COOKIE,"delete");
    session_cookie.setMaxAge(0); //delete
    session_cookie.setPath(QTIS_SESSION_COOKIE_PATH); // SIC! dublicate cookies with different path are not cleared
    response.addCookie(session_cookie);
 }

 public QTISSessionCookie(HttpServletRequest request, HttpServletResponse response){this.request = request; this.response = response; findCookieValue();}
 public QTISSessionCookie(HttpServletRequest request, HttpServletResponse response, String cookieName)
 { if(cookieName!=null) this.QTIS_SESSION_COOKIE = cookieName; this.request = request; this.response = response; findCookieValue(); }
 private synchronized void findCookieValue()
 {
  // yadzuka@ purposed line with 2 found errors (1) request.getCookies() == null on no cookies, (2) Cookie::getName can dublicate
  //Map<String, String> cookies = Arrays.stream(request.getCookies()).collect(java.util.stream.Collectors.toMap(Cookie::getName, Cookie::getValue));
  Cookie[] cookies = request.getCookies();
  if(QTIS_SESSION_COOKIE==null) return; // no cookie name - nothing to search
  if(QTIS_SESSION_COOKIE_PATH==null) return; // no path - nothing to trust
  Cookie session_cookie=null;
  if(cookies != null)
   for(int i=0;i<cookies.length;i++){
    if(cookies[i]==null)continue; // don't panic on null pointer
    if(QTIS_SESSION_COOKIE.equals(cookies[i].getName()))
    {
     //if(session_cookie != null) {session_cookie=null; break;} // do not trust if dublicate cookie found
     session_cookie=cookies[i];
     // comment next line to use latest found cookie:
     //break; // do not search for the next cookie with the same name 
    }
   }
  if(session_cookie!=null) session_cookie_value =  session_cookie.getValue();
 }
 // main
 /** this method report version. */
 public static void main(String[] varg)
 {
  System.out.println(getVer());
 } // main()
} //QTISSessionCookie
