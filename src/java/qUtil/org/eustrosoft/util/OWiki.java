package org.eustrosoft.util;

import java.util.*;
import java.io.*;
import java.text.*;

//BEGIN_OWIKI
// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
    public class OWiki {
        // static version fields & methods
        private static int ver_major = 0;
        private static int ver_minor = 6;
        private static int ver_build = 2023121901;
        private static String ver_status = "BETA6";
        private static String COPYRIGHT = "Alex V Eustrop & EustroSoft.org 2019-2023";
        private static String LICENSE = "BALES, MIT or BSD on your choice - see http://bales.eustrosoft.org";
        private static String LICENSE_DETAIL = "you can do with this source code anything that you want while keeping the list of its authors until this code will be rewritten by you";
        public static int getVerMajor(){return(ver_major);}
        public static int getVerMinor(){return(ver_minor);}
        public static int getVerBuild(){return(ver_build);}
        public static String getVerStatus(){return(ver_status);}
        public static String getVer(){return(""+ver_major+"."+ver_minor+"-"+ver_status+"-b"+ver_build);}
        // instance fields
        private java.io.BufferedReader in;
        private java.io.PrintWriter out;
        private final static String SZ_NULL = "null"; //WAMessages compatible constant
        private int line_no = 0; //current line number
	boolean is_TOC =false;
	public boolean isTOC(){return(is_TOC);}
	public void setTOC(boolean value){is_TOC = value;}

        public void setIn(BufferedReader in) {
            this.in = in;
        }

        public void setOut(PrintWriter out) {
            this.out = out;
        }

        public String next() {
            if (in == null) return (null);
            try {
                line_no++;
                return (in.readLine());
            } catch (IOException ioe) {
                return (null);
            }
        } // next line from stream

        public final static String[] OWIKI_MARKUP_INLINE_UNSAFE_CHAR = {"<", ">", "&", "\n"};
        public final static String[] OWIKI_MARKUP_INLINE_UNSAFE_SUBST = {"&lt;", "&gt;", "&amp;", "<br>"};
        public final static String[] OWIKI_MARKUP_INLINE_FONT = {"**", "''", "``", "__", "~~"};
        public final static String[] OWIKI_MARKUP_INLINE_FONT_HTML_ODD = {"<b>", "<i>", "<tt>", "<u>", "<strike>"};
        public final static String[] OWIKI_MARKUP_INLINE_FONT_HTML_EVEN = {"</b>", "</i>", "</tt>", "</u>", "</strike>"};
        public final static String[] OWIKI_MARKUP_INLINE_SEQ_CUT = {"[[", "{{"};
        public final static String[] OWIKI_MARKUP_INLINE_SEQ_CUT_TO = {"]]", "}}"};
        public final static String[] OWIKI_MARKUP_INLINE_SEQ_REF = {"http://", "https://", "tel:", "mailto:", "telnet:", "ssh:", "ftpub:"};

        //url_regexp="(http://|https://|tel:|mailto:|telnet:|ssh:)[_a-zA-Z0-9\.@+\(\)\-\/]*"
        public void print(String sz) {
            String[] from = OWIKI_MARKUP_INLINE_UNSAFE_CHAR;
            String[] to = OWIKI_MARKUP_INLINE_UNSAFE_SUBST;
            //boolean odd_state[] = {false,false,false};
            boolean[] odd_state = new boolean[OWIKI_MARKUP_INLINE_FONT.length];
            if (sz == null) {
                w(SZ_NULL);
                return;
            }
            StringBuffer sb = new StringBuffer(sz.length() + 256);
            int p = 0;
            //if(len>to.length) len=to.length; // let's
            while (p < sz.length()) {
                int len = from.length;
                int i = 0;
                boolean char_ignore = false;
                while (i < len) // search for token
                {
                    if (sz.startsWith(from[i], p)) {
                        sb.append(to[i]);
                        p = --p + from[i].length();
                        break;
                    }
                    i++;
                }
                if (!(i >= len)) char_ignore = true;
                i = 0;
                len = OWIKI_MARKUP_INLINE_FONT.length;
                while (i < len) // search for token
                {
                    if (sz.startsWith(OWIKI_MARKUP_INLINE_FONT[i], p)) {
                        if (odd_state[i]) {
                            sb.append(OWIKI_MARKUP_INLINE_FONT_HTML_EVEN[i]);
                            p = --p + OWIKI_MARKUP_INLINE_FONT[i].length();
                        } else {
                            sb.append(OWIKI_MARKUP_INLINE_FONT_HTML_ODD[i]);
                            p = --p + OWIKI_MARKUP_INLINE_FONT[i].length();
                        }
                        odd_state[i] = !odd_state[i];
                        break;
                    }
                    i++;
                }
                if (!(i >= len)) char_ignore = true;
		int content_end = 0;
                String content = null;
                if (sz.startsWith("[[", p)) {
                    p = p + 2;
                    content_end = sz.indexOf("]]", p);
                    if (content_end == -1) {
                        content = sz.substring(p);
                        p = sz.length();
                    } else {
                        content = sz.substring(p, content_end);
                        p = content_end + 2;
                    }
                    //if (content.startsWith("img:")) {
                    //    p = renderImage(content);
                    //} if (content.startsWith("youtube:")) {
                    //    p = renderYoutubeVideo(content);
                    //} else {
                        sb.append(renderInlineRefBlock(content));
                    //}
                    continue;
                }
		boolean href_found = false;
		for(int k=0;k<OWIKI_MARKUP_INLINE_SEQ_REF.length;k++)
		{
		 if(sz.startsWith(OWIKI_MARKUP_INLINE_SEQ_REF[k],p))
		 {
		  content_end=0; content = null;
		  href_found = true;
                    content_end = sz.indexOf(" ", p);
                    if (content_end == -1) {
                        content = sz.substring(p);
                        p = sz.length();
                    } else {
                        content = sz.substring(p, content_end);
                        p = content_end;
                    }
		    sb.append(renderInlineRefBlock(content + "|" + content));
		    //sb.append("<a href='"); sb.append(text2href(content)); sb.append("'>"); sb.append(text2html(content)); sb.append("</a>");
		    break;
		 }
		}
		if(href_found) continue;
		/*
                 */
                //if(i>=len) sb.append(sz.charAt(p)); // not found
                if (!char_ignore) sb.append(sz.charAt(p)); // not found
                p++;
            }
            // close all opened <b> <u> and so on
            for (int k = 0; k < odd_state.length; k++) {
                if (odd_state[k]) sb.append(OWIKI_MARKUP_INLINE_FONT_HTML_EVEN[k]);
            }
            wln(sb.toString());
            //wln(t2h(sz));
        }

        //

/* SIC! this code not needed and marked for removal. Eustrop 2023-12-19
        private int renderYoutubeVideo(String content) {
            String[] params = content.substring(8).split("\\|");
            String[] imageParams = new String[] {"src", "width", "height"};
            int paramIndex = 0;
            StringBuilder paramsBuilder = new StringBuilder();

            while (paramIndex < params.length) {
                paramsBuilder.append(imageParams[paramIndex]);
                paramsBuilder.append("='");
                paramsBuilder.append(params[paramIndex]);
                paramsBuilder.append("' ");
                paramIndex++;
            }

            String video =
                    "<iframe " + paramsBuilder.toString() +
                            "title=\"YouTube video player\" frameborder=\"0\" " +
                            "allow=\"accelerometer; autoplay; clipboard-write; " +
                            "encrypted-media; gyroscope; picture-in-picture; web-share\" allowfullscreen>" +
                            "</iframe>";
            wln(video);
            return video.length() + 2;
        }
        private int renderImage(String content) {
            String[] params = content.substring(4).split("\\|");
            String[] imageParams = new String[] {"src", "alt", "width", "height"};
            int paramIndex = 0;
            StringBuilder paramsBuilder = new StringBuilder();

            while (paramIndex < params.length) {
                paramsBuilder.append(imageParams[paramIndex]);
                paramsBuilder.append("='");
                paramsBuilder.append(params[paramIndex]);
                paramsBuilder.append("' ");
                paramIndex++;
            }

            String image = "<img " + paramsBuilder.toString() + ">";
            wln(image);
            return image.length() + 2;
        }
// 41 lines for removal.
*/
	public static final String OW_FTPUB="ftpub:";
	public static final String OW_IMG="img:";
	public static final String OW_IMG_FTPUB=OW_IMG + OW_FTPUB;
	public static final String OW_FTPUB_URL_DEFAULT="/ftpub/";
	public static final String OW_YOUTUBE="youtube:";
	public static final String OW_YOUTUBE_SHARE=OW_YOUTUBE + "https://youtu.be/";
	public static final String OW_YOUTUBE_EMBED=OW_YOUTUBE + "https://www.youtube.com/embed/";

	private String OW_FTPUB_URL=OW_FTPUB_URL_DEFAULT;
	public String getFTPubUrl(){return(OW_FTPUB_URL);}
	public void setFTPubUrl(String url){OW_FTPUB_URL=url;}

        public String renderInlineRefBlock(String sz) {
            if (sz == null) {
                return (SZ_NULL);
            }
            if (sz.length() == 0) return ("");
            if (sz.startsWith(" ")) return (t2h(sz));
            if (sz.startsWith("\t")) return (t2h(sz));
            String[] parts = sz.split("\\|");
            //if(sz.startsWith("http://") || sz.startsWith("http://") ))
            StringBuffer sb = new StringBuffer(sz.length() + 32);
            if(parts[0].startsWith(OW_IMG))
            {
	     sb.append("<img src='");
	     if(parts[0].startsWith(OW_IMG_FTPUB))
	      {sb.append(OW_FTPUB_URL); sb.append(text2href(parts[0].substring(OW_IMG_FTPUB.length()))); }
	     else
	      sb.append(text2href(parts[0].substring(OW_IMG.length())));
	     sb.append("'");
	     if(parts.length>1) //title presend
	     {
	      sb.append(" title='"); sb.append(text2value(parts[1])); sb.append("' ");
	      sb.append(" alt='"); sb.append(text2value(parts[1])); sb.append("' ");
	     }
	     if(parts.length>2) //width (and height?) present as 000 (or width/height as 000x000?)
	     {
	      sb.append(" width='"); sb.append(text2value(parts[2])); sb.append("' ");
	     }
	     sb.append(">");
	     return(sb.toString());
            }
            if(parts[0].startsWith(OW_YOUTUBE))
	    {
	     String youtube_src = null;
	     if(parts[0].startsWith(OW_YOUTUBE_EMBED)) { youtube_src=parts[0].substring(OW_YOUTUBE.length()); }
	     else if (parts[0].startsWith(OW_YOUTUBE_SHARE))
	     { youtube_src=OW_YOUTUBE_EMBED.substring(OW_YOUTUBE.length()) + parts[0].substring(OW_YOUTUBE_SHARE.length()); }
	     else { return(t2h(sz)); } // invalid youtube ref found
	     sb.append("<iframe width='560' height='315' src='");
	     sb.append(youtube_src);
	     sb.append("' title='YouTube video player' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share' allowfullscreen></iframe>");
	     return(sb.toString());
	    }
            if (parts.length == 1) return (t2h(sz));
            sb.append("<a href='");
	    if(parts[0].startsWith(OW_FTPUB))
	    {
	     sb.append(OW_FTPUB_URL); sb.append(text2href(parts[0].substring(OW_FTPUB.length())));
	    }
	    else sb.append(text2href(parts[0]));
            sb.append("'>");
            sb.append(t2h(parts[1]));
            sb.append("</a>");
            return (sb.toString());
        }
//public void println(String s){w(t2h(s));wln("<br>");}

        // TagStack routines
        private final Stack TagStack = new Stack();
        private int TagStackCount = 0;

        public int getTagStackCount() {
            return (TagStackCount);
        }

        private static final String TAG_DOCUMENT = "Document";
        private static final String TAG_RECORD = "Record";
        private static final String TAG_P = "p";
        private static final String TAG_DIV = "div";
        private static final String TAG_TABLE = "table";
        private static final String TAG_TBODY = "tbody";
        private static final String TAG_TR = "tr";
        private static final String TAG_TH = "th";
        private static final String TAG_TD = "td";
        private static final String TAG_UL = "ul";

        public int openTag(String tag, String parameters) {
            if (TAG_DOCUMENT.equals(tag)) wln("<!-- Document -->");
            else {
                w("<");
                w(tag);
                if (parameters != null) {
                    w(" ");
                    w(parameters);
                }
                w(">");
            }
            TagStack.push(tag);
            return (++TagStackCount);
        }

        public int openTag(String tag) {
            return (openTag(tag, null));
        }

        public int closeTag(String tag) {
            String t = null;
            while (TagStackCount > 0) {
                t = (String) TagStack.pop();
                if (TAG_DOCUMENT.equals(t)) wln("<!-- /Document -->");
                else {
                    w("</");
                    w(t);
                    wln(">");
                }
                TagStackCount--;
                if (tag.equals(t)) break;
            }
            if (!tag.equals(t)) {
                wln("<!-- missing open tag " + tag + " -->");
            }
            return (TagStackCount);
        } // int closeTag(String tag)

        public int closeAllTags() {
            if (TagStackCount == 0) return (0);
            return (closeTag(1));
        }

        public int closeTag(int tag) {
            if (tag > TagStackCount) return (TagStackCount);
            while (TagStackCount >= tag) {
                String t = (String) TagStack.pop();
                if (TAG_DOCUMENT.equals(t)) wln("<!-- /Document -->");
                else {
                    w("</");
                    w(t);
                    wln(">");
                }
                TagStackCount--;
            } //while
            return (TagStackCount);
        } // closeTag(int tag)

        public int beginDocument() {
            return (openTag(TAG_DOCUMENT));
        }

        public int closeDocument() {
            return (closeTag(TAG_DOCUMENT));
        }

        // TOC methods
        private final Vector TOCHeaders = new Vector();
        private final Vector TOCLevels = new Vector();
        private final Vector TOCTags = new Vector();

        private int getTOCSectionCount() {
            return (TOCHeaders.size());
        }

        private int addTOCSection(int lvl, String title, int tag_id) {
            TOCHeaders.add(title);
            TOCLevels.add(new Integer(lvl));
            TOCTags.add(new Integer(tag_id));
            return (TOCHeaders.size() - 1);
        }

        private void printTOC() {
            int i = 0;
            int start_lvl = 1;
            int current_lvl = 1;
            int TOC_tag_id = openTag(TAG_DIV, "class='owTOC' id='owTOC'");
            openTag(TAG_UL);
            for (i = 0; i < TOCHeaders.size(); i++) {
                int hlvl = ((Integer) TOCLevels.get(i)).intValue();
                if (hlvl > current_lvl) {
                    while (hlvl > current_lvl) {
                        openTag(TAG_UL);
                        current_lvl++;
                    }
                }
                if (hlvl < current_lvl) {
                    while (hlvl < current_lvl) {
                        closeTag(TAG_UL);
                        current_lvl--;
                    }
                }
                w("<li><a href='#sec" + i + "'>");
                //w("H" + hlvl + ":");
                w(t2h((String) TOCHeaders.get(i)));
                wln("</a>");
            }
            while (current_lvl > start_lvl) {
                closeTag(TAG_UL);
                current_lvl--;
            }
            closeTag(TAG_UL);
            closeTag(TOC_tag_id);
        }

        public int printHeader(int hlvl, String title) {
            if (hlvl > 6) hlvl = 6;
            title = title.trim();
            int last_section_id = getTOCSectionCount();
            if (last_section_id != 0) {
                // close previouse sections divs
                Integer prev_section_tag = (Integer) TOCTags.get(last_section_id - 1);
                closeTag(prev_section_tag.intValue());
            }
            int section_id = addTOCSection(hlvl, title, (getTagStackCount() + 1)); //SIC! mb getTagStackCount()++?
            w("<a name='sec" + section_id + "' id='sec" + section_id + "'></a>");
            w("<h" + hlvl + ">");
            print(title.trim());
            wln("</h" + hlvl + ">");
            int tag_id = openTag(TAG_DIV, "class='owsection' id='dsec" + section_id + "'");
            //openTag(TAG_TABLE); openTag(TAG_TR); openTag(TAG_TD);
            return (tag_id);
        }

        public void renderHTMLOWiki() //OWiki as HTML (safe view source owiki text in html)
        {
            wln("<pre>");
            String line = null;
            line = next();
            while (line != null) {
                wln(t2h(line));
                line = next();
            }
            wln("</pre>");
        } //renderHTMLOWiki()

        public void renderHTMLInput() //OWiki as HTML input inside <textarea> tag
        {
            String line = null;
            line = next();
            while (line != null) {
                wln(t2h(line));
                line = next();
            }
        }//renderHTMLInput()

        public void renderHTML() // OWiki to HTML
        {
            try {
                openTag(TAG_DIV, "class='owiki'");
                String line = null;
                int current_p_id = 0;
                int current_ul_id = 0;
                int current_table_id = 0;
                boolean isTable = false;
                // TODO: create statistics class and put all variables inside
                boolean[] table_commented_columns = null;
                while ((line = next()) != null) {
                    //comment - ignored
                    if (line.startsWith("#")) continue;
                    if (line.startsWith("{!")) {
                        String makroname = line.substring(1).trim();
                        String[] macro_parts = makroname.split("[ }]", 2);
                        makroname = macro_parts[0];
                        String stop_makro = makroname + "}";
                        //wln(makroname);
                        boolean do_print = false;
                        boolean do_rawprint = false;
                        boolean do_pre = false;
                        boolean do_macro_print = false;
                        if ("!example".equals(makroname)) {
                            do_print = true;
                            do_pre = true;
                            do_macro_print = true;
                        }
                        if ("!TOC".equals(makroname)) {
                            do_print = true;
                            do_pre = true;
                            do_macro_print = true;
                        }
                        if ("!cat".equals(makroname)) do_print = true;
                        if ("!html".equals(makroname)) do_rawprint = true;
                        if ("!rawhtml".equals(makroname)) do_rawprint = true;
                        if (do_pre) wln("<pre>");
                        if (do_macro_print) wln(t2h(line));
                        if (!line.trim().endsWith("}"))
                            while (true) {
                                line = next();
                                if (line == null) break; // close document
                                if (line.startsWith("!}") || line.startsWith(stop_makro)) {
                                    if (do_macro_print) wln(t2h(line));
                                    break;
                                }
                                if (do_print) wln(t2h(line));
                                if (do_rawprint) wln(line);
                            }
                        if (do_pre) wln("</pre>");
                        continue;
                    }
                    if (line.startsWith("{")) {
                        wln("<pre>");
                        while (true) {
                            line = next();
                            if (line == null) break; // close document
                            if (line.startsWith("}")) break;
                            wln(t2h(line));
                        }
                        wln("</pre>");
                        continue;
                    }

                    // header
                    if (line.startsWith("=")) // next section header
                    {
                        if (current_p_id != 0) {
                            closeTag(current_p_id);
                            current_p_id = 0;
                        }
                        if (current_ul_id != 0) {
                            closeTag(current_ul_id);
                            current_ul_id = 0;
                        }
                        if (current_table_id != 0) {
                            closeTag(current_table_id);
                            current_table_id = 0;
                        }
                        int h_level = 1;
                        while (line.startsWith("=", h_level)) {
                            h_level++;
                        }
                        int l = line.length();
                        l--;
                        while (l >= h_level) {
                            char c = line.charAt(l);
                            if (c == '=' || c == ' ' || c == '\t') {
                                l--;
                                continue;
                            }
                            l++;
                            break;
                        }
                        if (l >= h_level) line = line.substring(h_level, l);
                        else line = "";
                        printHeader(h_level, line);
                        continue;
                    }
                    //multiline loading
                    /*
                     */
                    StringBuffer sb_line = null;
                    if (line.endsWith("\\") || line.endsWith("\\n")) sb_line = new StringBuffer();
                    while (line.endsWith("\\") || line.endsWith("\\n")) {
                        if (line.endsWith("\\n")) {
                            sb_line.append(line, 0, line.length() - 2);
                            sb_line.append("\n");
                        } else sb_line.append(line, 0, line.length() - 1);
                        sb_line.append(" ");
                        line = next();
                        if (line == null) break;
                    }
                    if (sb_line != null) {
                        if (line != null) sb_line.append(line);
                        line = sb_line.toString();
                    }
                    // list
                    if (line.startsWith("*")) // list
                    {
                        if (current_p_id != 0) {
                            closeTag(current_p_id);
                            current_p_id = 0;
                        }
                        if (current_table_id != 0) {
                            closeTag(current_table_id);
                            current_table_id = 0;
                        }
                        if (current_ul_id == 0) current_ul_id = openTag(TAG_UL);
                        w("<li>"); // ((?<!\[\[http|s.{0,100}\]\])\|&(?!.{0,100}\]\])\|)
                        print(line.substring(1));
                        continue;
                    }
                    // table
                    if (line.startsWith(";")) // table
                    {
                        if (current_table_id == 0) {
                            current_table_id = openTag(TAG_TABLE);
                        }
                        if (line.length() < 2) continue;
                        openTag(TAG_TR);
                        String FS = "" + line.charAt(1);

                        //String[] cells = getTableCells(line.substring(1), FS);
                        String[] cells = line.substring(1).split(FS);
			if(!isTable) //this is first row of table
			{
			 table_commented_columns = new boolean[cells.length];
			}
                        for (int i = 1; i < cells.length; i++) {
			    if(!isTable) // first row
			    {
                             if (cells[i].startsWith("#") || cells[i].startsWith("=#")) { table_commented_columns[i] = true; }
			    }
			    if(i<table_commented_columns.length) if (table_commented_columns[i]) continue; // skip commented columns
                            String cell_tag = TAG_TD;
			    if(cells[i].startsWith("=")) { cell_tag = TAG_TH; cells[i]=cells[i].substring(1); } // set TH instead of TD if so
                            openTag(cell_tag);
                            print(cells[i]);
                            closeTag(cell_tag);
                        }
			closeTag(TAG_TR);
                        isTable = true;
                        continue;
                    } else {
		      if(isTable) { closeTag(TAG_TABLE); isTable = false; }
                    }
                    // code
                    // html
                    // sql
                    // svg
                    // video
                    // paragraph
                    if (line.trim().length() == 0) {
                        if (current_p_id != 0) {
                            closeTag(current_p_id);
                            current_p_id = 0;
                        }
                        if (current_table_id != 0) {
                            closeTag(current_table_id);
                            current_table_id = 0;
                        }
                        if (current_ul_id != 0) {
                            closeTag(current_ul_id);
                            current_ul_id = 0;
                        }
                        continue;
                    }
                    if (current_table_id != 0) {
                        closeTag(current_table_id);
                        current_table_id = 0;
                    }
                    if (current_p_id == 0 && current_ul_id == 0) current_p_id = openTag(TAG_P);
                    print(line);
                    flush();
                }
                closeAllTags();
                if(is_TOC) printTOC();
                flush();
            } catch (Exception e) {
                try {
                    closeAllTags();
                } catch (Exception e2) {
                }
                w("<div class='owerror'>Exception:");
                wln(t2h(e.toString()));
                //e.printStackTrace(out);
                wln("</div>");
            }
        }

	/* SIC! я не понял, зачем весь этот код (вопрос к yadzuka@). Eustrop 2023-12-18
	 * комментирую до понимания
        ///
        ///  Table processing
        ///
        private String[] getTableCells(String row, String FS) {
            List<String> processedCells = new ArrayList<>();
            String[] cells = row.split(processFS(FS));
            boolean isLink = false;
            int linkIndex = -1;
            for (int i = 0; i < cells.length; i++) {
                String splitStr = cells[i];

                if (isLink) {
                    String link = processedCells.get(linkIndex);

                    processedCells.remove(linkIndex);
                    processedCells.add(linkIndex, link + processFS(FS) + splitStr);

                    if (splitStr.contains("]]")) {
                        isLink = false;
                        continue;
                    }
                } else {
                    if (splitStr.contains("[[") && !splitStr.contains("]]")) {
                        if (i + 1 < cells.length) {
                            processedCells.add(cells[i]);
                            isLink = true;
                            linkIndex = processedCells.size() - 1;
                        }
                    }
                }

                if (!isLink) {
                    processedCells.add(splitStr);
                }
            }
            return processedCells.toArray(new String[0]);
        }

        private String processFS(String fs) {
            if (fs == null || fs.isEmpty()) {
                return "";
            }
            if (fs.equalsIgnoreCase("|")) {
                return "\\|";
            }
            return fs;
        }
	// 47 lines above commented until clarified. Eustrop 2023-12-18 
	*/
//

        // ConcepTIS WASkin/WAMessages imported section
// WASkin compatible methods and fields
        private boolean is_error = false;
        private Exception last_exception = null;

        // error control methods
        public boolean checkError() {
            return (is_error);
        }

        public Exception getLastException() {
            return (last_exception);
        }

        /** obj2html */
        private static String t2h(String s) {
            return (obj2html(s));
        }

        /** obj2value */
        private static String o2v(Object o) {
            return (obj2value(o));
        }

        /** obj2urlvalue (WARNING: must be rewritten) */
        private static String o2uv(Object o) {
            return (obj2value(o));
        }

        /** obj2text */
        private static String o2t(Object o) {
            return (obj2text(o));
        }

        /** obj2string */
        private static String o2s(Object o) {
            return (obj2string(o));
        }

        /** main raw text writing method (all writes comes through). */
        private void w(String s) {
            is_error = false;
            try {
                out.print(s);
            } catch (Exception e) {
                is_error = true;
                last_exception = e;
            }
        } // w(String s)

        private void wln(String s) {
            w(s);
            w("\n");
        }
        public void flush() { out.flush(); }

        private void wln() {
            w("\n");
        }

// BEGIN WAMessages imported (next 112 lines) 2023-10-09
//
// static conversion helpful functions
// obj2text(), obj2html(), obj2value() - useful functions
// translate_tokens() - background work for them
//

        /** convert object to text even if object is null.
         */
        public static String obj2text(Object o) {
            if (o == null) return (SZ_NULL);
            return (o.toString());
        }

        /** convert object to text but preserve null value if so.
         * @see obj2text
         */
        public static String obj2string(Object o) {
            if (o == null) return (null);
            return (obj2text(o));
        }

        /** convert object to html text even if object is null.
         * @see #obj2text
         * @see #text2html
         */
        public static String obj2html(Object o) {
            if (o == null) return ("<STRIKE><small>null</small></STRIKE>");
            else return (text2html(obj2text(o)));
        }

        /** convert szValue to Long object it's represents or into null value
         * if conversion failed.
         */
        public static Long string2Long(String szValue) {
            if (szValue == null) return (null);
            Long v = null;
            try {
                v = Long.valueOf(szValue);
            } catch (NumberFormatException e) {
            }
            return (v);
        }

        //
        public final static String[] HTML_UNSAFE_CHARACTERS = {"<", ">", "&", "\n"};
        public final static String[] HTML_UNSAFE_CHARACTERS_SUBST = {"&lt;", "&gt;", "&amp;", "<br>\n"};
        public final static String[] VALUE_CHARACTERS = {"<", ">", "&", "\"", "'"};
        public final static String[] VALUE_CHARACTERS_SUBST = {"&lt;", "&gt;", "&amp;", "&quot;", "&#039;"};
        public final static String[] HREF_CHARACTERS = {"<", ">", " ", "\"", "'"}; //< >'" %3C%20%3E%27%22
        public final static String[] HREF_CHARACTERS_SUBST = {"%3C", "%3E", "%20", "%22", "%27"};
        public final static String[] JSON_VALUE_CHARACTERS = {"\n", "\r", "\"", "\\"};
        public final static String[] JSON_VALUE_CHARACTERS_SUBST = {"\\n", "\\r", "\\", "\\\\"};

        /** convert plain textual data into html code with escaping unsafe symbols.
         * @param text - plain text
         * @return html escaped text
         */
        public static String text2html(String text) {
            return (translate_tokens(text, HTML_UNSAFE_CHARACTERS, HTML_UNSAFE_CHARACTERS_SUBST));
        } // text2html()

        /** convert plain textual data into html form value suitable for input or textarea fields.
         * @param text - plain text
         * @return escaped text
         */
        public static String text2value(String text) {
            return (translate_tokens(text, VALUE_CHARACTERS, VALUE_CHARACTERS_SUBST));
        } // text2value()

        public static String text2href(String text) {
            return (translate_tokens(text, HREF_CHARACTERS, HREF_CHARACTERS_SUBST));
        } // text2value()

        public static String text2json(String text) {
            return ("\"" + translate_tokens(text, JSON_VALUE_CHARACTERS, JSON_VALUE_CHARACTERS_SUBST) + "\"");
        }

        public static String obj2value(Object o) {
            return (text2value(obj2text(o)));
        }


        /** replace all sz's occurrences of 'from[x]' onto 'to[x]' and return the result.
         * Each occurence processed once and result depend on token's order at 'from'.
         * For instance: translate_tokens("hello",new String[]{"he","hel","hl"}, new String[]{"eh","leh","lh"})
         * give "ehllo", not "lehlo" or "elhlo" (in fact "hel" to "leh" translation never be done).
         * @param sz - string for translation
         * @param from - array of tokens to search
         * @param to - array of tokens to translate to
         * @param len - the number of tokens at "from" to look. use -1 to look for all
         *   actually len = min(len,from.length) if len >=0 and len = from.length otherwise
         *
         */
        public static String translate_tokens(String sz, String[] from, String[] to, int len) {
            if (sz == null) return (sz);
            StringBuffer sb = new StringBuffer(sz.length() + 256);
            int p = 0;
            if (len < 0) len = from.length;
            //if(len>to.length) len=to.length; // let's
            while (p < sz.length()) {
                int i = 0;
                while (i < len) // search for token
                {
                    if (sz.startsWith(from[i], p)) {
                        sb.append(to[i]);
                        p = --p + from[i].length();
                        break;
                    }
                    i++;
                }
                if (i >= len) sb.append(sz.charAt(p)); // not found
                p++;
            }
            return (sb.toString());
        } // translate_tokens

        public static String translate_tokens(String sz, String[] from, String[] to) {
            return (translate_tokens(sz, from, to, -1));
        }
//END WAMessages imported code (112 lines above)
// main method for using from console 
public static void main(String[] args) throws IOException
{
System.out.println("<!-- OWiki  (" + getVer() + "  -->");
OWiki ow = new OWiki();
ow.setIn(new BufferedReader(new InputStreamReader(System.in)));
ow.setOut(new PrintWriter(new OutputStreamWriter(System.out)));
ow.renderHTML();
}


    }//class OWiki
//END_OWIKI
