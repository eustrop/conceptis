<%--
 ConcepTIS project
 (c) Alex V Eustrop 2009
 see LICENSE at the project's root directory

 $Id$

 Purpose: This JSP is some kind of nutshell for ru.mave.ConcepTIS.webapps.* classes
	  which processes user's http requests and construct html UI documents
	  just without bounding <html> and <body> tags. Those documents
	  will be embedded into output of this JSP.

	  NOTE: The webapps.* classes above uses the same Postgres "Trust
	  authentication feature" based technique for users authentication
	  such as psql.jsp

 History:
  2009/11/28 started from psql.jsp
  2018/06/12 started from tisc.jsp
--%>
<%@
  page contentType="text/html; charset=UTF-8"
  import="java.util.*"
  import="java.io.*"
  import="java.sql.*"
  import="ru.mave.ConcepTIS.webapps.*"
%>
<%!
//
// Global parameters
//
private final static String CGI_NAME = "tiscui_ajaxed_by_vulpes.jsp";
private final static String CGI_TITLE = "ConcepTIS/Common UI";
//private final static String DBSERVER_URL = "jdbc:postgresql:conceptisdb";
private final static String JSP_VERSION = "$Id$";

private final static String SZ_EMPTY = "";
private final static String SZ_NULL = "<<NULL>>";
private final static String SZ_UNKNOWN = "<<UNKNOWN>>";

%>
<%
 long enter_time = System.currentTimeMillis();
 WARHMain warh = new WARHMain(request,response,out);
 warh.cache_expire_hints(); // some hints for old and buggy browsers like NN4.x
 String DBSERVER_URL = this.getServletContext().getInitParameter("DBSERVER_URL");
 warh.setJDBCURL(DBSERVER_URL);
 warh.setCGI(CGI_NAME);

%>
<!DOCTYPE html>
<html>

<head>
  <title><%= CGI_TITLE %></title>
  
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link rel="stylesheet" type="text/css" href="css/tiscui_ajaxed_by_vulpes.css">
  <script
    src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"
    type="text/javascript">
  </script>
</head>

<body>
<!-- SCRIPT SECTION -->

	<script >

		$(document).ready(function(){
			set_listeners();
		});
		
		$(document).ready(function(){
			window.onpopstate = function(event) {
				console.log(location.href.match("<%= CGI_NAME %>.*")[0]);
			    load_from_server(location.href.match("<%= CGI_NAME %>.*")[0]);
			};
		});
		
		function set_listeners(){
			$("a").unbind("click");
			$("a").bind('click', function(event) { 
				event.preventDefault(); 
				load_from_server($(this).attr('href'));
			    history.pushState("", '', $(this).attr('href')); 
			});
			execute_sql_query();
		}

		function load_from_server(input_url){
			input_url = "server_"+input_url;
			$.ajax({
				method:'GET',
				url: input_url,
				success: function(data){
					console.log(data);
					$("#tiscui_main").html(data);
					set_listeners();
				}
			})
		}
		</script>
		<script>
		//Скрипты, отвечающие за вкладку PSQL
		function execute_sql_query(){
			$("input[name='btn_exec']").unbind("click");
			$("input[name='btn_exec']").bind('click', function(event){
					event.preventDefault();
					$.ajax({
						mathod:"POST",
						url: "server_<%= CGI_NAME %>?subsys=psql&request=exec",
						dataType:"json",
						data:{
							SQLRequest : $("textarea[name='SQLRequest']").val(),
							flagJS : "flag",
							btn_exec : "Execute",
						},
						success: function(data){
							console.log(data);
							parseJSON(data);
							set_listeners();
						}
					});
			});				
		}
		
		function parseJSON(data){
			if (data == null) return;
			var lengthOfData = 0;
			for (i in data) lengthOfData++;
			var amountOfTables = 0;
			$("table").remove();
			$("hr[class = 'js']").remove();
			$("form").after("<hr id = \"hr"+amountOfTables+"\" class=js>");
			for (jsonNumber in data){
				$("#hr"+amountOfTables).after("<table id =table"+amountOfTables+">\
					<tbody id =tbody"+amountOfTables+"></tbody></table>");
				var selectJson = data[jsonNumber];
				// firstLine
				var firstLine = "<tr>";
				for(i in selectJson[0]){
					firstLine+="<th bgcolor='#CCCCCC'>"+i+"<br>("+selectJson[0][i]+")</th>";
				}
				$("#tbody"+amountOfTables).append(firstLine+"</tr>");
				// other lines
				for (i in selectJson){
					if (i == 0) continue;
					var tempObj = selectJson[i];
					var str = "<tr>";
					for (j in tempObj){
						str+="<td>"+tempObj[j]+"</td>";
					}
					$("#tbody"+amountOfTables).append(str+"</tr>");
				}
				if (amountOfTables != lengthOfData-1) 
					$("#table"+amountOfTables).after("<hr id = \"hr"+(++amountOfTables)+"\" class=js>");
			}
		}	
	</script>
	
	<!-- HEADER -->
	<div id="tiscui_header" class="TISCUIHeader">
		ConcepTIS v0.2 - frozen research example of SPA UI using AJAX (developed by vulpes@ aug-oct 2018, <a href="index.html">see for other UI</a>)
		<div id="tiscui_hmenu" class="TISCUIHeaderMenu">
			<div class="TISCUIHMenuItem"><a id=tiscui_hmi1 href="<%= CGI_NAME %>?subsys=main">Home</a></div>
			<div class="TISCUIHMenuItem"><a id=tiscui_hmi2 href="<%= CGI_NAME %>?subsys=SAM">SAM</a></div>
			<div class="TISCUIHMenuItem"><a id=tiscui_hmi3 href="<%= CGI_NAME %>?subsys=TISC">TISC</a></div>
			<div class="TISCUIHMenuItem"><a id=tiscui_hmi4 href="<%= CGI_NAME %>?subsys=TIS&request=menu">Tools</a></div>
			<div class="TISCUIHMenuItem"><a id=tiscui_hmi5 href="<%= CGI_NAME %>?subsys=psql">PSQL</a></div>
			<div class="TISCUIHMenuItemLast">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<a id=tiscui_hmi6 href="<%= CGI_NAME %>?subsys=help">Help</a></div>
			<div class="TISCUIRight"></div>
		</div> <!-- /id="tiscui_hmenu" -->
	 	<hr>
	 </div> <!-- /id="tiscui_header" -->
	<!-- LEFT -->
	 <div id="tiscui_left" class="TISCUILeft" >
	 	<div id="tiscui_lmenu" class="TISCUILeftMenu">
			<div ><br></div>
			<div id=tiscui_lmi0><b>Index:</b></div>
			<div><hr></div>
			<div>+<a id=tiscui_lmi1 href="<%= CGI_NAME %>?subsys=SAM">SAM</a></div>
			<div><a id=tiscui_lmi2 href="<%= CGI_NAME %>?subsys=SAM&request=list&target=XU">&nbsp;&nbsp;&nbsp;Users</a></div>
			<div><a id=tiscui_lmi3 href="<%= CGI_NAME %>?subsys=SAM&request=list&target=XG">&nbsp;&nbsp;&nbsp;Groups</a></div>
			<div><a id=tiscui_lmi4 href="<%= CGI_NAME %>?subsys=SAM&request=list&target=XS">&nbsp;&nbsp;&nbsp;Scopes</a></div>
			<div><hr></div>
			<div>+<a id=tiscui_lmi5 href="<%= CGI_NAME %>?subsys=TISC">TISC</a></div>
			<div><a id=tiscui_lmi6 href="<%= CGI_NAME %>?subsys=TISC&request=list&target=A">&nbsp;&nbsp;&nbsp;Area</a></div>
			<div><a id=tiscui_lmi7 href="<%= CGI_NAME %>?subsys=TISC&request=list&target=C">&nbsp;&nbsp;&nbsp;Container</a></div>
			<div><a id=tiscui_lmi8 href="<%= CGI_NAME %>?subsys=TISC&request=list&target=D">&nbsp;&nbsp;&nbsp;Document</a></div>
			<div><hr></div>
			<div>+<a id=tiscui_lmi9 href="<%= CGI_NAME %>?subsys=TIS&request=menu">Tools</a></div>
			<div><a id=tiscui_lmi10 href="<%= CGI_NAME %>?subsys=TIS&request=menu">&nbsp;&nbsp;&nbsp;TISGeneric</a></div>
			<div><a id=tiscui_lmi11 href="<%= CGI_NAME %>?subsys=psql">&nbsp;&nbsp;&nbsp;PSQL</a></div>
			<div><hr></div>
			<div><a id=tiscui_lmi12 href="<%= CGI_NAME %>?subsys=DIC">Dic</a></div>
			<div><hr></div>
			<div><a id=tiscui_lmi13 href="#">Doc</a></div>
			<div><hr></div>
			<div><a  id=tiscui_lmi14 href="#">About</a></div>
		</div> <!-- /id="tiscui_lmenu" -->
	 </div> <!-- id="tiscui_left" -->
	<!-- MAIN -->
	 <div id="tiscui_main" class="TISCUIMain" >
	    <%
	      warh.process();
	    %>
	 </div> <!-- id="tiscui_main" -->
	<!-- RIGHT -->
	 <div id="tiscui_right" class="TISCUIRight" >
	   <!-- nothing at the right now -->
	   &nbsp;e
	 </div> <!-- id="tiscui_right" -->
	<!-- FOOTER -->
	 <div id="tiscui_footer" class="TISCUIFooter">
		 <hr>
		 <i>timing : <%= ((System.currentTimeMillis() - enter_time) + " ms") %></i>
		 <br>
	     <i><%= JSP_VERSION %></i><br>
	     <small>your web-server is <%= application.getServerInfo() %></small>
	     <!-- Привет this is just for UTF-8 testing (must be russian word "Privet") -->
	 </div> <!-- /id="tiscui_footer" -->

		<!-- HIDDEN -->
		<!-- Post footer page space - use it for various hidden objects (float windows, modal dialogs and so on
		 +++ place short memo about tham here:
		 +++ div tiscui_login - login box
		 +++ div tiscui_msgbox - message box
		 -->
	<div id="tiscui_hidden" class="TISCUIHidden" >
		Hidden text
	</div> <!-- /id="tiscui_hidden" -->
	<div id="tiscui_help" class="TISCUIHidden" >
		<h1>Index</h1>
		<hr>
		<div>
			Help will be here.
		</div>
		<hr>
		<div id="tiscui_helpClose">
		    <button 
		        onclick="tiscuiCloseHelp()">
		        Back to page
		    </button>
		</div>
	</div> <!-- /id="tiscui_help" -->
	</body>
</html>
<!-- END OF THE PAGE -->
