"use strict"
var response_json;
var default_url = location.hostname;
var protocol = location.protocol;
var port = location.port;
//console.log(default_url);
//document.getElementById("server_ip").value = protocol + "//" + default_url + ":3000";
function clicked_server(){
    if(rest_page_input.value == "" ) rest_page_input.value = "REST.jsp";
    if(method_input.value == "" ) method_input.value = "POST";
    var server_url = rest_page_input.value; 
    var method = method_input.value;
    var subsys = subsys_input.value;
    var req = req_input.value; 
    var target = target_input.value;
    var ZID = ZID_input.value;
    var mimetype =mimetype_input.value + "; charset=UTF-8"; 
	var tail = tail_input.value;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if ((this.readyState == 4)) { // what is "4"?
            document.getElementById("server_response").value = this.responseText;
            response_json = this.responseText;
        }
    };
    var get_param_string = "subsys=" + encodeURIComponent(subsys)
	 + "&request=" + encodeURIComponent(req) + "&target=" + encodeURIComponent(target)
	+ "&ZID=" + encodeURIComponent(ZID);
	if (tail.length > 0){
		get_param_string += "&";
		get_param_string += tail;
	}
    var request_url = server_url + "?" + get_param_string;
	// get_request_link.innerHTML = request_url;
	get_request_link.href = request_url;
    xhttp.open(method, request_url, true);
    if( method == "GET"){ xhttp.send(); }
    else{ // method ==  POST, PUT and so on
      xhttp.setRequestHeader("Content-type", mimetype);
      var request_body = request_area.value;
	  //if(mode=="as_form") request_body="body=" + encodeURIComponent(request_body);
      xhttp.send(request_body);
    }
}

function load_ajax_test_page(){

    var tiscui_ajax=document.getElementById("tiscui_ajax");
    if( tiscui_ajax != null ) {
      if( !confirm("AJAX testing page already loaded.\nWhould you like to reload it?\n(Warning! all input will be lost)") ) return;
    }
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if ((this.readyState == 4)) {
            document.getElementById("tiscui_main").innerHTML = this.responseText;
            //console.log(this.responseText);
        }
    };
    xhttp.open("GET", "ajax/ajax_test.html", true);
    xhttp.send();
}

function test_json(){
	json_area.style.visibility = "visible";
	try {
		JSON.parse(document.getElementById("server_response").value);
		json_check.value = "OK";
	} catch (e) {
		json_check.value = e.name;
		json_check.value += "\n" + e.message;
	}
}

function hide_jsonarea(){
	json_area.style.visibility = "hidden";
}

function if_option(elem){
	var items = elem.parentElement.parentElement.children;
	console.log(items);
	for (var i=1; i < items.length; i++){
		if (elem.checked == true)
			items[i].style.visibility = "visible";
		else 
			items[i].style.visibility = "hidden";
	}
}
	
