<!-- START OF TISCUI EMBEDDED PAGE -->
<%--
 ConcepTIS project
 (c) Alex V Eustrop 2009
 see LICENSE at the project's root directory

 $Id$

 Purpose: This JSP is some kind of nutshell for ru.mave.ConcepTIS.webapps.* classes
	  which processes user's http requests and construct html UI documents
	  just without bounding <html> and <body> tags. Those documents
	  will be embedded into output of this JSP.

	  NOTE: The webapps.* classes above uses the same Postgres "Trust
	  authentication feature" based technique for users authentication
	  such as psql.jsp

 History:
  2009/11/28 started from psql.jsp
  2018/06/12 started from tisc.jsp
  2018/06/17 started from tiscui.jsp
--%>
<%@
  page contentType="text/html; charset=UTF-8"
  import="java.util.*"
  import="java.io.*"
  import="java.sql.*"
  import="ru.mave.ConcepTIS.webapps.*"
%>
<%!
//
// Global parameters
//
private final static String CGI_NAME = "tiscui.jsp";
private final static String CGI_TITLE = "ConcepTIS/Common UI";
//private final static String DBSERVER_URL = "jdbc:postgresql:conceptisdb";
private final static String JSP_VERSION = "$Id$";

private final static String SZ_EMPTY = "";
private final static String SZ_NULL = "<<NULL>>";
private final static String SZ_UNKNOWN = "<<UNKNOWN>>";

%>
<%

 long enter_time = System.currentTimeMillis();
 WARHMain warh = new WARHMain(request,response,out);
 warh.cache_expire_hints(); // some hints for old and buggy browsers like NN4.x
 String DBSERVER_URL = this.getServletContext().getInitParameter("DBSERVER_URL");
 warh.setJDBCURL(DBSERVER_URL);
 warh.setCGI(CGI_NAME);

%>
<!-- MAIN -->
 <div id="tiscui_main" class="TISCUIMain" >
    <%
      warh.process();
    %>
 </div> <!-- id="tiscui_main" -->
<!-- END OF THE TISCUI EMBEDDED PAGE -->
