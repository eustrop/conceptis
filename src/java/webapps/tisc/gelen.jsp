<%@
  page contentType="text/html; charset=UTF-8"
  import="java.util.*"
  import="java.io.*"
  import="java.sql.*"
  import="ru.mave.ConcepTIS.webapps.*"
%>
<%!
//
// Global parameters
//
private final static String CGI_NAME = "gelen.jsp";
private final static String CGI_TITLE = "ConcepTIS/Common UI";
//private final static String DBSERVER_URL = "jdbc:postgresql:conceptisdb";
private final static String JSP_VERSION = "$Id$";

private final static String SZ_EMPTY = "";
private final static String SZ_NULL = "<<NULL>>";
private final static String SZ_UNKNOWN = "<<UNKNOWN>>";

%>
<%
 long enter_time = System.currentTimeMillis();
 WARHMain warh = new WARHMain(request,response,out);
 warh.cache_expire_hints(); // some hints for old and buggy browsers like NN4.x
 String DBSERVER_URL = this.getServletContext().getInitParameter("DBSERVER_URL");
 warh.setJDBCURL(DBSERVER_URL);
 warh.setCGI(CGI_NAME);

%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="gelen/index.css">
	<!-- link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,600,700&display=swap&subset=cyrillic" rel="stylesheet" -->
  	<title><%= CGI_TITLE %></title>
</head>
<body>
	<header>Concept_TIS_UI v0.001</header>
	<div class="wrapper">
		<nav class="header_nav">
			<div class="header_home">
				<span class="header_logo"><a href="<%= CGI_NAME %>">Home</a></span>
			</div>
			<div class="header_sam">
				<span class="header_logo"><a href="<%= CGI_NAME %>?subsys=SAM">SAM</a></span>
			</div>
			<div class="header_tisc">
				<span class="header_logo">TISC</span>
			</div>
			<div class="header_tools">
				<span class="header_logo">Tools</span>
			</div>
			<div class="heder_psql">
				<span class="header_logo">PSQL</span>
			</div>
			<div class="header_help">
				<span class="header_logo">Help</span>
			</div>
		</nav>
		<aside class="left_side">	
			<div class="left_nav">
				<div class="left_sam">
					<span class="main_logo">SAM</span>
					<span class="sam_hover user_link">Users</span>
					<span class="sam_hover group_link">Group</span>
					<span class="sam_hover scopes_link">Scopes</span>
				</div>
				<div class="left_tisc">
					<span class="main_logo">TISC</span>
				</div>
				<div class="left_tools">
					<span class="main_logo">Tools</span>
				</div>
				<div class="left_dic">
					<span class="main_logo">Dic</span>
				</div>
				<div class="left_doc">
					<span class="main_logo">Doc</span>
				</div>
				<div class="left_about">
					<span class="main_logo">About</span>
				</div>
			</div>			
		</aside>
		<aside class="right_side">
			
		</aside>
		<div class="main_content">
	    <%
	      warh.process();
	    %>
		</div>
	</div>
</body>
</html>
