<%@
  page contentType="text/html; charset=UTF-8"
  import="java.util.*"
  import="java.io.*"
  import="java.sql.*"
  import="ru.mave.ConcepTIS.webapps.*"
%>

<%!
	//
	// Global parameters
	//
	private final static String CGI_NAME = "tiscui_ajaxed_by_vulpes.jsp";
	private final static String CGI_TITLE = "ConcepTIS/Common UI";
	//private final static String DBSERVER_URL = "jdbc:postgresql:conceptisdb";
	private final static String JSP_VERSION = "$Id$";
	
	private final static String SZ_EMPTY = "";
	private final static String SZ_NULL = "<<NULL>>";
	private final static String SZ_UNKNOWN = "<<UNKNOWN>>";
%>
	
<%
	long enter_time = System.currentTimeMillis();
	WARHMain warh = new WARHMain(request,response,out);
	warh.cache_expire_hints(); // some hints for old and buggy browsers like NN4.x\	
	String DBSERVER_URL = this.getServletContext().getInitParameter("DBSERVER_URL");
	warh.setJDBCURL(DBSERVER_URL);
	warh.setCGI(CGI_NAME);
 	warh.process();
%>
