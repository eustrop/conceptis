<%--
 ConcepTIS project
 (c) EustroSoft.org 2019
 see LICENSE at the project's root directory

 Purpose: This JSP is REST service with the same set of requests as tisc.jsp/tiscui.jsp
	it return application/json

 History:
  2019/09/03 started from tisc.jsp
--%><%@
  page contentType="application/json; charset=UTF-8"
  import="ru.mave.ConcepTIS.webapps.*"
%><%!
// Global parameters
//private final static String DBSERVER_URL = "jdbc:postgresql:conceptisdb";

%><%
 long enter_time = System.currentTimeMillis();
 WARHMain warh = new WARHMain(request,response,out);
 String DBSERVER_URL = this.getServletContext().getInitParameter("DBSERVER_URL");
 warh.setJDBCURL(DBSERVER_URL);
 warh.setCGI(null);
%><%
   warh.processREST();
  %>

