<%--
 ConcepTIS project
 (c) Alex V Eustrop 2009,2023
 LICENSE : BALES, BSD, MIT on your choice see bales.eustrosoft.org
 also see LICENSE at the project's root directory

 Purpose: qr.qxyz.ru or qxyz.ru/qr/ service page.
	  lookup QR subsystem data (see QR.QRecord table) for passed "q" parameter
	  and construct html version of public card for object described by QRecord found
	  request: qr.qxyz.ru/?q=012345678ABCD
	  SQL request (concept): select * from QR.V_QRecord where QR = x'012345678ABCD'::int8
	  NOTE: real SQL request(s) much complex then concept above
 History (qr.jsp):
  2023/12/14 15:31 (128 lines) switched to use org.eustrosoft.qtis.webapps.* per new inner WARHQRService class. unnecessary inner code removed;
  2023/12/13 (261 lines) qr.jsp started from the scratch again. qsql.jsp based now (this is third version of qr-service)
--%><%@
  page contentType="text/html; charset=UTF-8"
  import="java.util.*"
  import="java.io.*"
  import="java.sql.*"
  import="org.eustrosoft.qdbp.*"
  import="org.eustrosoft.qtis.webapps.*"
  import="org.eustrosoft.qtis.webapps.QR.*"
  import="ru.mave.ConcepTIS.dao.*"
  import="ru.mave.ConcepTIS.dao.TIS.*"
  import="ru.mave.ConcepTIS.dao.QR.*"
%><%@ page isThreadSafe="true"  %><%!
//
// Global parameters
//
private final static String CGI_NAME = "";
private final static String CGI_TITLE = "QR.jsp - QR service ";

private final static String JSP_VERSION = "3.0-20231227";

private final static String SZ_EMPTY = "";

public void jspDestroy(  ) {System.gc(); try { Thread.sleep(1000); } catch (Exception e){}  System.runFinalization(); }

//
// static conversion helpful functions
//
 public static Long QR2Long(String qr){if(qr==null)return(null);Long n=null;try{n=Long.parseLong(qr,16);}catch(NumberFormatException nfe){} return(n);}
 public static long QR2long(String qr){Long l=QR2Long(qr); if(l==null)return(0);return(l.longValue());}
 public static String Long2QR(Long qr){if(qr==null)return(null); return(long2QR(qr.longValue())); }
 public static String long2QR(long qr){return(String.format("%X",qr));}
 public static Long String2Long(String qr){if(qr==null)return(null);Long n=null;try{n=Long.parseLong(qr,10);}catch(NumberFormatException nfe){} return(n);}
 public static long String2long(String qr){Long l=QR2Long(qr); if(l==null)return(0);return(l.longValue());}
 
 public static class WARHQRService extends WARequestHandler
 {
    private String CGI_NAME_QR_PRINT = "/qr/print/";
    private String CGI_NAME_QR_IMG = "/qr/qrget/";
    private String CGI_TITLE = "QR.QXYZ";
    private Long QR_MEMBERS_SID = 1099511627776L; // this sid of qxyz.ru scope which is the default to search qr routing data
    public void setCGIQRPrint(String cgi) { CGI_NAME_QR_PRINT = cgi; }
    public String getCGIQRPrint() { return (CGI_NAME_QR_PRINT); }
    public void setQRMembersSID(Long sid) { QR_MEMBERS_SID = sid; }
    public Long getQRMembersSID() { return (QR_MEMBERS_SID); }

    public void process()
    {
     String q=request.getParameter("q");
     String ZID=request.getParameter("ZID");
     Long qr = QR2Long(q);
     Long ZOID = String2Long(ZID);
     //1. if no qr passed - show search form and exit
     //2. else if qr passed:
     //2.1. extract /12 range and lookup for memeber and scope where QRecord search to
     //2.1.1. if not found - lookup for upper ranges /16,/20,/24,/32 and follow it
     //2.2. if hard redirection found - send redirect and exit
     //2.3. if soft redirection found - show redirecting page and exit
     //3. search for QRecord
     //3.1. if multiple records found - print WARNING and list of them.
     //3.1.1 select one by ZID if passed. if no ZID or not found by it - not found
     //3.2 if no record found - create temporary stub QRecord object
     //3.3. if redirection found - send it
     //4. print QR-code page
     //4.1. show QRecord brief card
     //4.2. show blue-button menu if so
     //4.3. show QRecord card
     //4.4. show QR.owiki
     //4.5. search for all QR.PProduct using QRecord.prodtype and QRecord.prodmodel
     //4.6. show all found QR.PProduct.owiki
     //4.7. search for all files for found QR.PProduct and show them
     //4.8. show member info
     
     //1. if no qr passed - show search form and exit
      if(qr == null)
      {
       printThisPageHtmlHeader();
       printQRSearchForm(q,null);
       printThisPageHtmlFooter();
       return;
      }
      CGI_TITLE = Long2QR(qr) + " " + CGI_TITLE;
     //2. else if qr passed:
      java.sql.Connection dbc = null;
      String szSQL = null;
      RList list_ranges = null;
      RList list_members = null;
      RList list_products = null;
      RList list_qr = null;
      QRecord qrec = null;
      MRange MR = null;
      MRange MR_global = null;
      MMember MM = null;
      MMember MM_global = null;
      Long member_scope_id = null;
      // prepare connection
      try{
         dbc = dbps.getSQLConnection();
         if(dbc == null){throw new Exception("No session!");}
         setZSystem(new ZSystem(dbc));
         was.setFieldDic(getDic(WAMessages.DIC_TIS_FIELD)); //SIC! optimize!
      }
      catch(Exception e){addException(e);}
     //2.1. extract /12 range and lookup for memeber and scope where QRecord search to
      long qr_range12 = (qr >>> 12) << 12 ;
      long qr_range16 = (qr >>> 16) << 16 ;
      long qr_range20 = (qr >>> 20) << 20 ;
      long qr_range24 = (qr >>> 24) << 24 ;
      long qr_range32 = (qr >>> 32) << 32 ;
     //2.1.1. if not found - lookup for upper ranges /16,/20,/24,/32 and follow it
      // not implemend yet
      try{
       szSQL = "select MR.* from QR.V_MRange MR where rtype ='QR' AND MR.ZSID = " + QR_MEMBERS_SID
       + " AND ( "
       +"( MR.rstart = " + qr_range12 + " AND MR.rbitl = 12)"
       +") ORDER BY MR.rbitl ASC" ;
	list_ranges = loadDORList(dbc,szSQL,new MRange());
	if(list_ranges.size() > 0 ) MR = (MRange)list_ranges.get(0);
      }
      catch(Exception e){addException(e);}
      MR_global = MR;
     //2.2. if hard redirection found - send redirect and exit
     //2.3. if soft redirection found - show redirecting page and exit
      //
      if(MR!=null) { if(doRedirection(MR.action,MR.redirect)) return; }
      else {qrec = new QRecord();  qrec.QR = qr; qrec.owiki = "диапазон не выделен"; } // fake error record
     //2.4. search for MMember at QR_MEMBERS_SID
      if(MR != null)
      {
      try{
       szSQL = "select MM.* from QR.V_MMember MM where MM.ZSID = " + QR_MEMBERS_SID
       + " and MM.ZOID = " + MR.ZOID;
	list_members = loadDORList(dbc,szSQL,new MMember());
	if(list_members.size() > 0 ) MM = (MMember)list_members.get(0);
      }
      catch(Exception e){addException(e);}
      MM_global = MM;
      if(MM != null){ member_scope_id = MM.scope_id; }
     //2.5. search for MRange at members scope
      if(MM.scope_id != null)
      {
      try{
       szSQL = "select MR.* from QR.V_MRange MR where rtype ='QR' AND MR.ZSID = " + MM.scope_id
       + " AND ( "
       +"( MR.rstart = " + qr_range12 + " AND MR.rbitl = 12)"
       +") ORDER BY MR.rbitl ASC" ;
	list_ranges = loadDORList(dbc,szSQL,new MRange());
	if(list_ranges.size() > 0 ) MR = (MRange)list_ranges.get(0);
      }
      catch(Exception e){addException(e);}
      }
      if(MR!=null) { if(doRedirection(MR.action,MR.redirect)) return; }
     //2.6. search for MMember at members scope
      if(!MR.ZOID.equals(MM.ZOID))
      {
      try{
       szSQL = "select MM.* from QR.V_MMember MM where MM.ZSID = " + member_scope_id
       + " and MM.ZOID = " + MR.ZOID;
	list_members = loadDORList(dbc,szSQL,new MMember());
	if(list_members.size() > 0 ) MM = (MMember)list_members.get(0);
      }
      catch(Exception e){addException(e);}
      }

      } //if(MR != null)
     //3. search for QRecord
      if(qrec == null && MR != null)
      {
       szSQL = "select QR.* from QR.V_QRecord QR where QR.qr = " + qr +
       " AND ZSID = " + member_scope_id;
       try{
	 list_qr = loadDORList(dbc,szSQL,new QRecord());
         //WARHPSQL warhsql = new WARHPSQL(this);
         //warhsql.exec_sql(dbc,"select QR.* from QR.V_QRecord QR where QR.qr = " + qr);
        }
        catch(Exception e){addException(e);}
     //3.1. if multiple records found - print WARNING and list of them.
     //3.1.1 select one by ZID if passed. if no ZID or not found by it - not found
     if(ZOID != null && list_qr != null) //search for QRecord with ZOID == ZID
     { 
      for(int i=0; i < list_qr.size(); i++){ qrec = (QRecord)list_qr.get(i); if(ZOID.equals(qrec.ZOID)) break; qrec=null; }
     }
     else  if(list_qr.size() == 1 ) { qrec = (QRecord)list_qr.get(0); }
      }
     //3.2 if no record found - create temporary stub QRecord object
       boolean qr_found = false;
       if(qrec == null) {qrec = new QRecord();  qrec.QR = qr; qrec.owiki = "информация не найдена, обратитесь к ее поставщику";  } // fake record
       else qr_found = true;
     //3.3. if redirection found - send it
     if(doRedirection(qrec.action,qrec.redirect)) return;
     //4. print QR-code page
     //4.1. show QRecord brief card
     //4.2. show blue-button menu if so
     //4.3. show QRecord card
     //4.8. show member info
       printThisPageHtmlHeader();
       if(!qr_found) printQRecordListForChoice(list_qr);
       if(MR != null) if(MR.owiki != null)
       {
       printQRecordBrief(qrec);
       printOWiki(MR.owiki);
       }
       //printQRCodeInfoBlueMenu();
       printQRecordView(qrec);
     //4.4. show QR.owiki
       printOWiki(qrec.owiki);
     //4.5. search for all QR.PProduct using QRecord.prodtype and QRecord.prodmodel
     if(qrec.prodtype != null && qrec.ZSID != null && qrec.ZOID != null)
     {
       szSQL = "select PP.* from QR.V_PProduct PP,QR.V_QRecord QR where " +
       " QR.ZSID = PP.ZSID and PP.prodtype = QR.prodtype and "
       + " ( (PP.prodmodel is NULL and PP.pmrevision IS NULL) or (PP.prodmodel = QR.prodmodel and "
       + "   (PP.pmrevision IS NULL or PP.pmrevision = QR.pmrevision ))) " 
       + " and PP.prodpart is NULL "
       + " and QR.ZOID= " + qrec.ZOID 
       + " ORDER by PP.prodtype, PP.prodmodel ASC";
       try{
	 list_products = loadDORList(dbc,szSQL,new PProduct());
        }
        catch(Exception e){addException(e);}
     }
     //4.6. show all found QR.PProduct.owiki
     if(list_products != null)
     {
      for(int i=0;i<list_products.size(); i++)
      {
       PProduct PP = (PProduct)list_products.get(i);
       //wln("" + PP.prodtype + PP.prodmodel);
       printOWiki(PP.owiki);
      }
     }
     //4.7. search for all files for found QR.PProduct and show them
       printQRFilesList(member_scope_id, qrec);
     //4.8. show member info
       printQRMemberCard(MM);
       printExceptions();
       printThisPageHtmlFooter();
    } //process()
    public void processREST() {}
    private Vector exceptions = null;
    private void addException(Exception e)
    {
     if(exceptions == null) exceptions = new Vector();
     exceptions.add(e);
    }
    private RList loadDORList(java.sql.Connection dbc, String sql, DORecord factory) throws java.sql.SQLException
    {
     RList list = new RList();
     java.sql.ResultSet rs = null;
     try{
      rs = execSQL(dbc,sql);
      while(rs.next())
      {
       DORecord r = factory.createFromRS(rs);
       list.add(r);
      }
     }
     finally{ if(rs != null){try{rs.close();}catch(java.sql.SQLException sqle){}}}
     return(list);
    }
    public java.sql.ResultSet execSQL(java.sql.Connection dbc, String sql) throws java.sql.SQLException
    {
      return( dbc.createStatement().executeQuery(sql) );
    }
    // mk* methods
    String mkQRHrefWithZOID(Long QR, Long ZOID)
    {
     String href_base= CGI_NAME + "?q=" + Long2QR(QR);
     String href= href_base + "&ZID=" + ZOID;
     return(href);
    }
    String mkQRHref(Long QR)
    {
     String href_base= CGI_NAME + "?q=" + Long2QR(QR);
     return(href_base);
    }
    String mkQRHrefPrint(Long QR)
    {
     String href_base= CGI_NAME_QR_PRINT + "?q=" + Long2QR(QR);
     return(href_base);
    }
    
    // do* methods
    boolean doRedirection(String action, String redirect)
    {
     if(action == null) return(false);
     if(redirect == null) return(false);
     String qr_redirect=null;
      if("REDIRECT".equals(action)) // hard redirection
      {
       //check for inactive memeber and show soft redirection page (SIC! not implemented yet)
       qr_redirect = redirect;
      }
      else if("REDIRECT_QR".equals(action)) // qr redirection
      { qr_redirect=CGI_NAME + "?q=" + redirect; }
      else if("REDIRECT_QR_SVC".equals(action)) // qr redirection
      { qr_redirect=redirect + "?" + request.getQueryString(); }
      // SIC! other redirections not implemented yet. m.b. not required at all?
      if(qr_redirect != null)
      {
       was.sendHTTPRedirect(response,qr_redirect);
       was.wln(qr_redirect);
       outFlush();
       return(true);
      }
     return(false);
    } //doRedirection()
    // print* methods
    public void printQRFilesList(Long member_scope_id, QRecord qrec)
    {
/*
select XS.name || '/' || FF.name || '/' || FD.fname || '/' || FD2.fname || '/' || FD3.fname, FD3.descr from FS.V_FFile FF, SAM.V_Scope XS,  FS.V_FDir FD,
FS.V_FDir FD2,
FS.V_FDir FD3,
FS.V_FFile FF2 
 where  
FF.ZSID =  1048576 and XS.id = FF.ZSID and FF.type ='R' and FF.name = 'pub' 
and FD.ZOID = FF.ZOID  and FD.fname = 'product' and FD2.ZOID = FD.f_id and FD3.ZOID = FD2.f_id AND  FD2.fname = 'QC' AND FF2.ZOID = FD3.f_id and FF2.type = 'B'
*/
/*
       String szSQL = "select XS.name || '/' || FF.name || '/' || FD.fname || '/' || FD2.fname || '/' || FD3.fname, FD3.descr" 
                      + " from FS.V_FFile FF, SAM.V_Scope XS,  FS.V_FDir FD, "
+"FS.V_FDir FD2,"
+"FS.V_FDir FD3,"
+"FS.V_FFile FF2 "
+" where  "
+"FF.ZSID =  ? and XS.id = FF.ZSID and FF.type ='R' and FF.name = 'pub' "
+"and FD.ZOID = FF.ZOID  and FD.fname = 'product' and FD2.ZOID = FD.f_id and FD3.ZOID = FD2.f_id AND "
+"  FD2.fname = ? AND FF2.ZOID = FD3.f_id and FF2.type = 'B' ";
       try{
	 list_products = loadDORList(dbc,szSQL,new PProduct());
        }
        catch(Exception e){addException(e);}
     RList list = new RList();
     java.sql.ResultSet rs = null;
     try{
      rs = execSQL(dbc,sql);
      while(rs.next())
      {
       DORecord r = factory.createFromRS(rs);
       list.add(r);
      }
     }
     finally{ if(rs != null){try{rs.close();}catch(java.sql.SQLException sqle){}}}
     return(list);
*/
    }
    public boolean printExceptions()
    {
    if(exceptions == null) return(false);
    if(exceptions.size() == 0 ) return(false);
    wln("Exceptions count: " + exceptions.size());
    for(int i=0;i<exceptions.size();i++)
    {
     Exception e = (Exception)exceptions.get(i);
     this.printerrmsg(e.toString());
    }
    return(true);
    }
    public void printQRecordListForChoice(RList list)
    {
     for(int i=0;i<list.size();i++)
     {
     QRecord r = (QRecord)list.get(i);
     String href= mkQRHrefWithZOID(r.QR,r.ZOID);
     was.w("<a href='" +  href + "'>");
     was.w(Long2QR(r.QR));
     was.w("</a><br>");
     }
    }
    public void printQRecordBrief(QRecord qrec)
    {
      if(qrec == null) return;
      if(qrec.QR == null) return;
      String prodmodel = "";
      String sn = qrec.sn;
      if(sn==null) sn="";
      if(qrec.prodtype != null) prodmodel = prodmodel + qrec.prodtype;
      if(qrec.prodmodel != null) prodmodel = prodmodel + qrec.prodmodel;
      was.beginTable("border='1' width='98%'");
         //printQRCodeInfoCardTabField(o,QRCode.F_QR);
         printQRTabFieldQRPrint(qrec.QR);
         if(prodmodel.length() != 0)
         {
          wln("<tr>");
          wln("<td colspan='2'>");
          wln("<b>Модель:</b>");
          wln(obj2html(prodmodel));
          wln(" <b>SN:</b>");
          wln(obj2html(sn));
          wln("</td>");
          wln("</tr>");
         }
      was.closeTable();
    }

    public void printQRMemberCard(MMember MM)
    {
     if(MM == null) return;
     if(MM.owiki != null) { printOWiki(MM.owiki); return; }
    ZFieldSet fs=MM.toFieldSet();
    wln("<h2>Владелец информации</h2>");
    was.beginTable("border='1'");
     printQRTabField(fs,MMember.F_CODE);
     //printQRTabField(fs,MMember.F_ORG_ID);
     //printQRTabFieldZOID2Descr(fs,MMember.F_ORG_ID,getZNames4Field(MMember.FC_ORG_ID),wam.SUBSYS_QR);
     //printQRTabField(fs,MMember.F_SCOPE_ID);
     //printQRTabFieldList(fs,MMember.F_SCOPE_ID,rl_scopes,wam.SUBSYS_SAM, wam.TARGET_SAMSCOPE);
     //printQRTabFieldList(fs,MMember.F_SCOPE_ID,getSAMScopes(),wam.SUBSYS_SAM,wam.TARGET_SAMSCOPE);
     //printQRTabFieldDic(fs,MMember.F_STATUS,getDic("QR_MEMBER_STATUS"));
     //printQRTabFieldDic(fs,MMember.F_LEI_TYPE,getDic("LEI_TYPE"));
     printQRTabField(fs,MMember.F_LEI);
     //printQRTabFieldDic(fs,MMember.F_LET,getDic("LET_TYPE"));
     printQRTabField(fs,MMember.F_NAME);
     printQRTabField(fs,MMember.F_ADDR);
     printQRTabField(fs,MMember.F_SITE);
     printQRTabField(fs,MMember.F_PHONE);
     printQRTabField(fs,MMember.F_EMAIL);
     //printQRTabFieldTextarea(fs,MMember.F_DESCR);
     //printQRTabFieldOWiki(fs,MMember.F_OWIKI);

    was.closeTable();
    }
    public void printQRecordView(QRecord qrec)
    {
    ZFieldSet fs=qrec.toFieldSet();
    was.beginTable("border='1' width='98%'");
    // view record QR.QRecord
     printQRTabFieldQRPrint(qrec.QR);
     //printQRTabFieldDic(fs,QRecord.F_ACTION,getDic("QR_ACTION"));
     //printQRTabField(fs,QRecord.F_REDIRECT);
     //printQRTabFieldZOID2Descr(fs,QRecord.F_OBJ_ID,getZNames4Field(QRecord.FC_OBJ_ID),wam.SUBSYS_QR);
     printQRTabField(fs,QRecord.F_CNUM);
     printQRTabField(fs,QRecord.F_CDATE);
     //printQRTabField(fs,QRecord.F_PRICE_GPL);
     //printQRTabFieldDic(fs,QRecord.F_PRICE_VAT,getDic("VAT_CODES"));
     //printQRTabFieldZOID2Descr(fs,QRecord.F_SUPPORT_ID,getZNames4Field(QRecord.FC_SUPPORT_ID),wam.SUBSYS_QR);
     printQRTabField(fs,QRecord.F_PRODTYPE);
     printQRTabField(fs,QRecord.F_PRODMODEL);
     printQRTabField(fs,QRecord.F_PMREVISION);
     printQRTabField(fs,QRecord.F_SN);
     printQRTabField(fs,QRecord.F_PRODATE);
     printQRTabField(fs,QRecord.F_GTD);
     printQRTabField(fs,QRecord.F_SALEDATE);
     printQRTabField(fs,QRecord.F_SENDATE);
     printQRTabField(fs,QRecord.F_WSTART);
     printQRTabField(fs,QRecord.F_WEND);
     printQRTabField(fs,QRecord.F_GIS_LONG);
     printQRTabField(fs,QRecord.F_GIS_LAT);
     printQRTabField(fs,QRecord.F_GIS_ALT);
     printQRTabField(fs,QRecord.F_COMMENT);
     //printQRTabFieldTextarea(fs,QRecord.F_CSVCARD);
     //printQRTabFieldOWiki(fs,QRecord.F_OWIKI);
    was.closeTable();
    //
    }
    public boolean isQRFieldPrintable(ZFieldSet fs,String fname)
    {
     ZField f = fs.get(fname); if(f==null) return(false); if(f.getValue() == null) return(false);
     return(true);
    }
    public void printQRTabFieldQRPrint(Long QR)
    {
     String qr_field = "<a href='" + mkQRHrefPrint(QR) + "'>" + Long2QR(QR) + "</a>";
     was.printTabFieldRaw("QR-код",qr_field);
    }

    public void printQRTabField(ZFieldSet fs, String fname)
    {
     if(!isQRFieldPrintable(fs,fname)) return;
     was.printTabField(fs,fname);
    }
    public void printQRTabFieldTextarea(ZFieldSet fs, String fname)
    {
     if(!isQRFieldPrintable(fs,fname)) return;
     was.printTabFieldTextarea(fs,fname);
    }
    public void printQRTabFieldOWiki(ZFieldSet fs, String fname)
    {
     if(!isQRFieldPrintable(fs,fname)) return;
     was.printTabFieldOWiki(fs,fname);
    }
    public void printOWiki(String owiki)
    {
     if(owiki==null) return;
     OWiki ow = new OWiki();
     ow.setIn(new BufferedReader(new StringReader(owiki)));
     ow.setOut(out);
     ow.renderHTML();
    }

    // BEGIN WASkinQR code imported from old qr/index.jsp from qxyz.ru project
     public void wln(String line){was.wln(line);} //SIC! for compatibility with code below
     public void w(String line){was.w(line);} //SIC! for compatibility with code below
     public void outFlush(){out.flush();}
     public static final String QRINFO_A_TOP = "#top"; //    - верх страницы
     public static final String QRINFO_A_BOTTOM = "#bottom"; // - низ страницы
     public static final String QRINFO_A_QRCODE_BREEF = "#qrcode.breef"; //   - краткая карточка изделия (порождается всегда перед меню, но только если оно есть)
     public static final String QRINFO_A_QRCODE_INFO = "#qrcode.info"; //    - Информационная карточка изделия (порождается после меню, если оно есть)
     public static final String QRINFO_A_QRCODE_WIKI = "#qrcode.wiki"; //    - собственное описания для qr-кода (если есть) порождается после полной карточки
     public static final String QRINFO_A_RANGE_WIKI = "#range.wiki"; //     - собственное описание диапазона (если есть) пока его нет
     public static final String QRINFO_A_PRODMODEL_WIKI = "#prodmodel.wiki"; // - описание продукта-модели, после qrcode.wiki
     public static final String QRINFO_A_MEMBER_WIKI = "#member.wiki"; //    - описание участника системы, владельца информации, внизу страницы
     public static final String QRINFO_A_PRINTQR_PAGE = "#printqr.page"; //   - не
     public static final String QRINFO_A_PRODMODEL_WIKI_INFO = "#prodmodel.wiki.info"; // // Х;i++арактеристики
     public static final String QRINFO_A_PRODMODEL_WIKI_SERVICE = "#prodmodel.wiki.service"; // //  Обслуживание
     public static final String QRINFO_A_PRODMODEL_WIKI_REPAIR = "#prodmodel.wiki.repair"; // Ремонт
     public void printAnchor(String name)
     {
      w("<a name='"); //
      w(name); //
      w("'>"); //
      wln("</a>");
     }
     private void printQRSearchForm(String q, String p) {
        //if(q==null) q ="101A117";
        if(q==null) q ="100D001";
        if(p==null) p="";
        wln("<form method='GET' name='search' action='" + CGI_NAME +"'>");
        wln("q:");
        w("<input type='text' size='8' name='" + wam.PARAM_Q + "' value='");
        w(wam.obj2value(q));
        wln("'/>");
        wln("p:");
        w("<input type='text' size='8' name='" + wam.PARAM_P + "' value='");
        w(wam.obj2value(p));
        wln("' />");
/*
*/
        wln("<input type='submit' title='Запросить информацию по глобальному QR-коду (q), с опциональным PIN (p)' value='Lookup!'/>");
        wln("</form>");
     }
     private void printQRCodeInfoBlueMenu()
     {
     wln("<div class='info'>");
     wln("    <a href='#" + QRINFO_A_QRCODE_INFO + "' class='info__btn'>Характеристики</a>");
     wln("    <a href='#" + QRINFO_A_PRODMODEL_WIKI_SERVICE + "' class='info__btn'>Обслуживание</a>");
     wln("    <a href='#" + QRINFO_A_PRODMODEL_WIKI_REPAIR + "' id='register-open' class='info__btn'>Ремонт</a>");
     wln("</div>");
     }
    // END WASkinQR code imported from old qr/index.jsp from qxyz.ru project
    // BEGIN header/footer section imported from old qr/index.jsp from qxyz.ru project
     private Exception error_while_header=null; // чтобы посмотреть в конце, если не удалось, то footer делаем тоже локальный, соотв. header
     private long header_enter_time = 0;
     public void printThisPageHtmlHeader(){
      error_while_header=null;
      header_enter_time = System.currentTimeMillis();
     if(response == null) {wln("null"); return;}
     try{
      request.setAttribute("SSIP_TITLE",CGI_TITLE);
      request.getRequestDispatcher("/header.jsp").include(request, response);
     }
     catch(ServletException se) {error_while_header=se;}
     catch(IOException ioe) {error_while_header=ioe;}
     //catch(NullPointerException npe) {error_while_header=npe;}
     if(error_while_header != null)
     {
      wln("<html>");
      wln(" <head>");
      wln("  <meta charset='UTF-8'>");
      wln("  <title>");
      wln("   " + CGI_TITLE);
      wln("  </title>");
      wln(" </head>");
      wln("<body>");
     }
      wln("<div class='section'>");
     }
     public void printThisPageHtmlHeaderMenu(){}
     public void printThisPageHtmlFooter()
     {
      wln("</div>"); // close <div class='section'>");
      // timing/version BEGIN
      wln("<hr>");
      wln("<i>timing : " + ((System.currentTimeMillis() - header_enter_time)) + " ms</i>");
      wln("<br>");
      wln("<i>ver : "+ JSP_VERSION + " QDBPool ver: " + QDBPool.getVer() + " QDBPOOL_NAME = " + getQDBPoolName() + " </i>");
      // timing/version END
     try{
wln("<!--footer-->");
outFlush();
      request.setAttribute("SSIP_TITLE",CGI_TITLE);
      request.getRequestDispatcher("/footer.jsp").include(request, response);
wln("<!--footer-->");
      return;
     }
     catch(ServletException se) {error_while_header=se;}
     catch(IOException ioe) {error_while_header=ioe;}
       wln("</div>");
       wln("<!-- Привет this is just for UTF-8 testing (must be russian word \"Privet\") -->");
       wln("</body></html>");
     }
    // END header/footer section imported from old qr/index.jsp from qxyz.ru project
    // constructors
    public WARHQRService(HttpServletRequest request,HttpServletResponse response,java.io.PrintWriter out){
        super(request,response,out);
    }

    public WARHQRService(WARequestHandler warh) { super(warh); }
 } // class WARHQRService

%><%
// REQUEST PROCESSIN STARTED HERE:
 // 0. get configuration from web-application parameters (WEB-INF/web.xml)
 String QDBPOOL_NAME = this.getServletContext().getInitParameter(WARequestHandler.PARAM_QDBPOOL_NAME);
 String QDBPOOL_URL = this.getServletContext().getInitParameter(WARequestHandler.PARAM_QDBPOOL_URL);
 String QDBPOOL_JDBC_CLASS = this.getServletContext().getInitParameter(WARequestHandler.PARAM_QDBPOOL_JDBC_CLASS);
 long enter_time = System.currentTimeMillis();
 // 1. get or create DB pool 
 QDBPool dbp = QDBPool.get(QDBPOOL_NAME);
 QDBPSession dbps = null;
 if(dbp == null){dbp = new QDBPool(QDBPOOL_NAME,QDBPOOL_URL,QDBPOOL_JDBC_CLASS); QDBPool.add(dbp);}
 // 2. get session
 // 2.1. get session cookie and its value if so
 //QTISSessionCookie session_cookie = new QTISSessionCookie(request, response);
 // 2.2. get session by cookie
 //dbps = dbp.logon(session_cookie.value());
 dbps = dbp.logon();
 // 2.3. renew session if ready (reserved for future use)
 //if(dbps != null && dbps.isSessionRenewReady()) {dbps.renewSession(); session_cookie.set(dbps.getSessionSecretCookie(),dbps.getSessionCookieMaxAge()); }
 if(dbps == null) dbps = dbp.createSession(); // temporary session stub, not stored in the pool
 // END get session
 // 3. process request
 request.setCharacterEncoding("UTF-8");
 WARequestHandler warh = new WARHQRService(request,response,new java.io.PrintWriter(out));
 //warh.setQDBPool(QDBPOOL_NAME,QDBPOOL_URL,QDBPOOL_JDBC_CLASS);
 warh.setQDBPSession(dbps);
 warh.cache_expire_hints(); // some hints for old and buggy browsers like NN4.x
 warh.setCGI(CGI_NAME);

if(true){ // ready for non-html answer
  synchronized(dbps) { warh.process(); }
}
%>
