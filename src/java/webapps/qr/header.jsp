<%@
  page contentType="text/html; charset=UTF-8"
  import="java.util.*"
  import="java.io.*"
%><%@ page isThreadSafe="true"  %><%!
static final String DEFAULT_SSIP_TITLE="QXYZ - Locum tuum in universo.";
public String getRequestAttributeString(javax.servlet.http.HttpServletRequest request, String attrib_name,String default_value)
{
 String a = getRequestAttributeString(request,attrib_name);
 if(a==null) a = default_value;
 return(a);
}
public String getRequestAttributeString(javax.servlet.http.HttpServletRequest request, String attrib_name)
  {String a=null;try{ a=(String)request.getAttribute(attrib_name); } catch(Exception e){} return(a);}

%><%
 //
 // some hints for old and buggy browsers like NN4.x
 //

 long enter_time = System.currentTimeMillis();
 long expire_time = enter_time + 24*60*60*1000;
 response.setHeader("Cache-Control","No-cache, no-store, must-revalidate");
 response.setHeader("Pragma","no-cache");
 response.setDateHeader("Expires",expire_time);
//SSIP_*
String SSIP_TITLE=getRequestAttributeString(request,"SSIP_TITLE", DEFAULT_SSIP_TITLE);
String SSIP_DESCRIPTION=getRequestAttributeString(request,"SSIP_DESCRIPTION", SSIP_TITLE);
String SSIP_KEYWORDS=getRequestAttributeString(request,"SSIP_KEYWORDS", SSIP_TITLE);
String SSIP_TIMESTAMP= "" + enter_time;

if(SSIP_TITLE == null) SSIP_TITLE = "none";
%><!DOCTYPE html>
<html>
 <head>
  <meta charset="utf-8">
  <title><%= SSIP_TITLE %></title>
 </head>
<body>
<!-- from header.jsp -->
