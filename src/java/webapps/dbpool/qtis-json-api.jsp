<%@
  page contentType="application/json; charset=UTF-8"
  import="java.util.*"
  import="java.io.*"
  import="java.sql.*"
  import="org.eustrosoft.qdbp.*"
  import="org.eustrosoft.util.*"
  import="org.eustrosoft.qtis.SessionCookie.*"
%><%!
//
// global section
//

%><%
//
// process http request
//
if("PUT".equals(request.getMethod())) { out.print("{ \"method\" : \"PUT\", \"e\" : 500 }"); }
if("POST".equals(request.getMethod()))
{
  request.setCharacterEncoding("UTF-8");
  java.io.BufferedReader input = request.getReader(); // get reader to parse JSON from it
  if("echo".equals(request.getParameter("request"))) // now we can look to some GET parameters
  {
   String Line = null;
   Line = input.readLine();
   while(Line != null)
   {
     out.println(Line);
     Line = input.readLine();
   }
   return;
  }
  QJson json = new QJson();
 try{
  json.parseJSONReader(input);
  json.writeJSONString(out,1);
 }
 catch(Exception e)
 {
  QJson err_json = new QJson();
  err_json.addItem("e",500);
  err_json.addItem("m",e.toString());
  out.print(err_json.toString());
 }

   //out.print(request.getMethod());
}
if("GET".equals(request.getMethod()))
{
response.setHeader("Content-type","text/html");
%>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title>QTIS-JSON-API</title>
    <meta charset="utf-8">
</head>
<body>
<form id='jsonForm'>
API path : <input type='text' name='api_path' value='qtis-json-api.jsp'>
(actual version : <a href='https://bitbucket.org/eustrop/conceptis/src/master/src/java/webapps/dbpool/qtis-json-api.jsp'>ConcepTIS/src/java/webapps/dbpool/qtis-json-api.jsp</a>) <br>
<textarea name="JSONRequest" rows="10" cols="72">
{
 "r":{"s":"au"}, "//": "спрашиваем подсистему Ау! кто я такой",
 "t":0
}
</textarea>
<textarea name="AdHocNotes" rows="10" cols="32">
// ad-hoc notes
</textarea>
</form>
<br>
  <button type="submit" value="POST" onClick="doPost(jsonForm);">POST</button>
<pre>
<div class="output" id='output'></div>
</pre>
<script>
function doPost(formInput)
{
 let formData = new FormData(formInput);
 output.innerText= "//request:\n" + formInput.JSONRequest.value;
 
 var xhttp = new XMLHttpRequest();
 xhttp.onreadystatechange = function() {
        let output = document.getElementById("output");
        if ((this.readyState == 4)) { // what is "4"?
            output.innerText = output.innerText + "//response:\n" + this.responseText;
        }
        else {
            output.innerText = output.innerText + "//response: this.readyState = " + this.readyState + "\n" 
        }
    };
 xhttp.open("POST", formInput.api_path.value, true);
 //if( method == "GET"){ xhttp.send(); }
 // method ==  POST, PUT and so on
 xhttp.setRequestHeader("Content-type", "application/json");
 xhttp.send(formInput.JSONRequest.value);
//output.innerText= output.innerText + "//response:\n" + formInput.JSONRequest.value;
return(false);
}
</script>
<div id="api_help">
<h2>QTIS JSON call examples</h2>
<h3>0. Generic request format</h3>
<pre>
{
  "//":"in this manner compatible with generic JSON format",
  "s": "some-session-cookie-value", "//" : " id сессии, если не указан, заполняется из кук, может быть не реализовано или отключено",
  "r": [{},{}], "//":" массив requests/responses - массив запросов или ответов, если поддерживается множественность запросов",
  "//":"единичный запрос передается следующим образом:",
  "r" : {
    "s" : "subsystem-name", "//":" название подсистемы, обязательный параметр",
    "any-optional-or-mandatory-parameter" : "its value",
    "//":" последний параметр любого запроса - t - время в ms потраченное на его формирование или исполнение",
    "//":" параметр t - используется для контроля полноты полученного JSON",
    "t" : 0
   },
  "//" : "каждый пакет запросов или ответов должен завершаться параметром t, заполняемым сервисом получившим его",
  "t":0
}
</pre>
<h3>1. Login</h3>
<pre>
// request
{
 "r":{
 "s":"login","r":"login","method":"plain","username":"john@example.com","password":"wrong_password","l":"en"}, "//":" предьявляемся системе",
 "t" : 0
}
// response to invalid login
{
 r:{"s":"login","r":"login","e":401,"m":"Invalid user or password","method":"plain","username":"john@example.com","l":"en"},
 "//" : "!!! коды ответов проработать, в коде использовать константы! (чтобы можно было их перенумеровть, потом)",
 "t":13
}
// response to valid login
{
 r:{"s":"login","r":"login","e":0,"m":"Ok","username":"john@example.com","full_name": "John Smith", "session" : "session_cookie", "l":"en"},
 "t":13
}
</pre>
<h3>999. Unsorted and untested protocol skatch</h3>
<pre>
// Запрос:
{
 "r":{"s":"au"}, "//": "спрашиваем подсистему Ау! кто я такой",
 "t":0
}
// Ответ:
{
 "r":{"s:"au","e":401,"m"="No session","l":"en"}, "//" : "над кодами ошибок надо еще подумать",
 "t":10
}
// Запрос logon (invalid)
{
 r:{"s":"login","r":"login","method":"plain","username":"john@example.com","wrong_password":"password"[,l="en"]}, // предьявляемся системе
}
// Ответ:
{
 r:{"s":"login","r":"login",e=401,m="Invalid user or password",["method":"plain","username":"john@example.com",][,l="en"]}, // 
 //!!! коды ответов проработать, в коде использовать константы! (чтобы можно было их перенумеровть, потом)
}
// Запрос logon (valid)
{
 r:{"s":"login","r":"login","method":"plain","username":"john@example.com","valid_password":"password"[,l="en"]}, // предьявляемся системе
}
// Ответ:
{
 r:{"s":"login","r":"login",e=0,m="Ok","user_id":1234,full_name:"John Doe","username":"john@example.com"[,"method":"plain",,][,l="en"]}, // Ok
}
// Запрос:
{
 r:{"s":"au"[,"l":"en"]}, // спрашиваем подсистему Ау! кто я такой
 t:0
}
// Ответ:
{
 r:{s="au",e=0,m="Ok","user_id":1234,"username":"john@example.com",full_name:"John Doe"[,"session_id":12345, qtisver:1]}, // может быть еще что-то?
 t:10
}
// Запрос logoff (valid)
{
 r:{"s":"login","r":"logoff"[,"session_id":12345,l="en"]}, // отключаемся от системы
}
// Ответ:
{
 r:{"s":"login","r":"logoff",e=0,m="Ok"[,"session_id":12345,l="en"]}, // отключаемся от системы
}
// Запрос logoff (invalid)
{
 r:{"s":"login","r":"logoff"[,"session_id":12345,l="en"]}, // отключаемся от системы
}
// Ответ:
{
 r:{"s":"login","r":"logoff",e=401,m="No session"[,"session_id":12345,l="en"]}, // отключаемся от системы
}
//  повторяем вход в систему, см. выше
//..

// запросы к подсистеме SQL
// Запрос 
{
  "r" : [{
      "s": "sql",
      "r": "sql",
	  "query": "select * from tis.v_users;"
    },
	{
      "s": "sql",
      "r": "sql",
	  "query": "select * from tis.v_users where id = ? or name like ?;",
	  "parameters": [1234,"%john%"],
	  "param_types": ["bigint","text"]	
	},
	{
      "s": "sql",
      "r": "ps", 
	  "query": "SQL_WHO_AM_I", 
	  "parameters": [1234],
	  "param_types": ["bigint"]	
	}
	],
  "t":0
}
// Ответ
{
  "r" : [{
      "s": "sql",
      "r": "sql",
	  ["query": "select * from tis.v_users;",]
	  "sets": [
          {
            "columns": [
              "",
              ...
            ],
            "data_types": [
              "",
              ...
            ],
            "rows": [
              [
                "",
                ...
              ]
              ...
            ],
            "rows_count": 1,
            "status": "",
            //
            "e": 0,
            //1,2
            "m": "Ok"
          }[,  ...]
    "e": 0, // ?? 
    "m": "Ok"
    },
	{
      "s": "sql",
      "r": "sql",
	  ["query": "select * from tis.v_users where id = ? or name like ?;",
	  parameters: [1234,"%john%"],
	  param_types: [bigint,text],]
      "sets" : [ { } ]	 
    "e": 0, // ?? 
    "m": "Ok"	  
	},
	{
      "s": "sql",
      "r": "ps", // prepared statment - предопределенный запрос
	  ["query": "SQL_WHO_AM_I", // на сервере ему соответстует "select * from tis.v_users where id = ?;"
	  parameters: [1234],
	  param_types: [bigint]	]
    "e": 555, // ?? 
    "m": "Not implemented"
	}
	],
  "t":1000
}
</pre>
</div>
<hr>
<div>end of page</div>

</body></html>
<%}%>
