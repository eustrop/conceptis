<%--
 ConcepTIS project
 (c) Alex V Eustrop 2009,2023
 LICENSE : BALES, BSD, MIT on your choice see bales.eustrosoft.org
 also see LICENSE at the project's root directory

 Purpose: PostgreSQL DB access via tiny JSP-based application using
          inner DBBool implementation
 History (psql.jsp & dbpool.jsp):
  2009/11/21 started from the TISExmlDB.java,v 1.1 2009/10/04 14:28:00 eustrop Exp
  2009/11/25 done. size: 271 lines
  2009/11/28 some finishing. CVS import. size 277 line.
  ...
  2023/04/07 dbpool.jsp started from psql.jsp
--%><%@
  page contentType="text/html; charset=UTF-8"
  import="java.util.*"
  import="java.io.*"
  import="java.sql.*"
%><%@ page isThreadSafe="true"  %><%!
//
// Global parameters
//
private final static String CGI_NAME = "dbpool.jsp";
private final static String CGI_TITLE = "DBPool with PSQL-like tool via JSP and JDBC";

private final static String JSP_VERSION = "0.3";

private final static String SZ_EMPTY = "";
private final static String SZ_NULL = "<<NULL>>";
private final static String SZ_UNKNOWN = "<<UNKNOWN>>";
// CMD codes
public final static String CMD_HOME="home";
public final static String CMD_LOGIN="login";
public final static String CMD_PSQL="psql";
public final static String CMD_PSQL_JSON="psql_json";
public final static String CMD_WHOAMI="whoami";
public final static String CMD_STATUS="status";
public final static String CMD_GC="gc"; // run garbage collector
public final static String CMD_FREECONNECTION="freecon"; // free sql connection
public final static String CMD_PASSWORD_DIGEST="pwdigest"; // free sql connection

private static int jsp_instance_count = 0;
private int jsp_instance = 0;

private final static String QTIS_SESSION_COOKIE= "QTIS_SESSION"; // cookie name for session
//BEGIN_QDBSP
//package org.eustrosoft.qdbsp;

//DBPOOL configuration
private final static String DBPOOL_NAME = CGI_NAME; // name of QDBPoll
private final static String DBPOOL_URL = "jdbc:postgresql://conceptis-pg-server:5432/conceptisdb"; // url of database
private final static String DBPOOL_JDBC_CLASS = "org.postgresql.Driver"; // java class to register
// parameters below not used yet:
private final static String DBPOOL_USER = ""; // name of database user. if not set - logon with passed session user/password to DB
private final static String DBPOOL_USER_PASSWORD = ""; //
private final static String DBPOOL_TISQL_LOGON = ""; // "select sam.logon(?,?,?);" (user,password,session_key)
private final static String DBPOOL_TISQL_BINDSESSION = ""; // "select sam.bind_session(?,?);" (session_id,session_key) 
private final static String DBPOOL_OPT_ALLOWTRUSTED = "NO"; // default NO
private final static String DBPOOL_OPT_MINPASSWORD = "8"; // minimun password length (default 8)
// QDBPool.java - это пул соединений с БД и фабрика порождения объектов иных классов
// QDBPException.java - это общий класс исключений пакета QDBPool
// QDBPConnection.java - это класс-контейнер для хранения-передачи JDBC соединения с БД
// QDBPSession.java - это класс-контейнер пользовательской сессии web or mobile

// QDBPool.java - это пул соединений с БД и фабрика порождения объектов иных классов
public static class QDBPool
{
 // static section
 private static Hashtable ht_pools; // storage for pools;
 // static version fields & methods
 private static int ver_major = 0;
 private static int ver_minor =3;
 private static int ver_build = 20230422;
 private static String ver_status = "ALPHA3";
 private static int getVerMajor(){return(ver_major);}
 private static int getVerMinor(){return(ver_minor);}
 private static int getVerBuild(){return(ver_build);}
 private static String getVerStatus(){return(ver_status);}
 private static String getVer(){return(""+ver_major+"."+ver_minor+"-"+ver_status+"-b"+ver_build);}
 // static pool methods
 public static synchronized void add(QDBPool p){add(p,p.name);}
 public static synchronized void add(QDBPool p,String name)
 {
  if(ht_pools == null)ht_pools = new Hashtable();
  Object check_pool = ht_pools.get(name);
  if(check_pool == null) { ht_pools.put(name,p); }
  else{throw new QDBPException("pool with name '" + name + "' exists");}
 }
 public static synchronized QDBPool get(String name){if(ht_pools==null)return(null);return((QDBPool)ht_pools.get(name));}
 public static synchronized void destroyAll(){ht_pools=null;} //!SIC
  /** create driver class by its classname (jdbc_driver parameter)
   * and register it via DriverManager.registerDriver().
   * @param jdbc_driver - "org.postgresql.Driver" for postgres,
   * "oracle.jdbc.driver.OracleDriver" for oracle, etc.
   */ 
 public static void register_jdbc_driver(String jdbc_driver)
	throws Exception
  {
   java.sql.Driver d;
   Class dc;
   // get "Class" object for driver's class
   try
   {
    dc = Class.forName(jdbc_driver);
    d = (java.sql.Driver)dc.newInstance();
   }
   catch(ClassNotFoundException e) {
     throw new Exception("register_jdbc_driver:" + "unable to get Class for "
     + jdbc_driver + ":" + e); }
   catch(Exception e) {
     throw new Exception("register_jdbc_driver: unable to get driver " 
     + jdbc_driver + " : " + e); }
   // register driver
   try { DriverManager.registerDriver(d); }
   catch(SQLException e) { throw new Exception(
   "register_jdbc_driver: unable to register driver "+jdbc_driver+" : "+e);}
  } // register_jdbc_driver()
 // instance section
 private String name;
 private String jdbc_url;
 private String jdbc_driver_class;
 private long seq_sessions = 1;
 private Hashtable ht_sessions; // storage for sessions
 private Hashtable ht_connections; // storage for sessions
 private long connection_timeout_ms = 600000; // 10m
 private long last_gc_run_ts = 0;

 public String getName(){return(name);}
 public String getJDBCUrl(){return(jdbc_url);}
 public synchronized QDBPConnection getConnection(String name){if(ht_connections==null)return(null);return((QDBPConnection)ht_connections.get(name));}
 public Vector<QDBPConnection> listConnections(){ if(ht_connections==null)return(new Vector<QDBPConnection>()); Vector<QDBPConnection> l=new Vector<QDBPConnection>(ht_connections.values()); return(l); }
 public synchronized QDBPSession getSession(Long id, String session_cookie){QDBPSession s = getSession(id); if(s != null) if(s.checkSecretCookie(session_cookie)) return(s); return(null);}
 public synchronized QDBPSession getSession(Long id){if(id==null)return null;if(ht_sessions==null)return(null);return((QDBPSession)ht_sessions.get(id));}
 public Vector<QDBPSession> listSessions() { if(ht_sessions==null)return(new Vector<QDBPSession>()); Vector<QDBPSession> l=new Vector<QDBPSession>(ht_sessions.values()); return(l); }
 public synchronized void gcConnections() // garbage collection of connections not used for connection_timeout_ms
 {
  Vector<QDBPSession> v = listSessions();
  long current_time= System.currentTimeMillis();
  if((current_time-last_gc_run_ts)< connection_timeout_ms/2) return;
  last_gc_run_ts = current_time;
    for(int i=0;i<v.size();i++){
     QDBPSession s = v.get(i);
     if(s.isExclusiveConnection())
     if((current_time-s.getLastUsage()) > connection_timeout_ms) s.freeConnection();
    }
 } // gcConnections()
 public synchronized QDBPSession createSession(){
  QDBPSession s = new QDBPSession(name,null);
  return s;
 }
 public synchronized QDBPSession logon(String secret_cookie_value)
 {
 String session_cookie_secret=null;
 Long session_cookie_id=null;
 gcConnections();
 //2.2. extract session id from secret_cookie_value if present
 if(secret_cookie_value != null)
 {
  String[] scv_parts = secret_cookie_value.split(":",2);
  try{ session_cookie_id = Long.valueOf(scv_parts[0]); } catch (NumberFormatException nfe){} //str2Long(..);
  if(scv_parts.length>1) session_cookie_secret=scv_parts[1];
 }
 //2.3. find session if present
 return(this.getSession(session_cookie_id,session_cookie_secret));
 } // logon(String secret_cookie_value)

 public synchronized QDBPSession logon(String login, String password)
   throws java.sql.SQLException
{return logonSQL(login, password);}
 public synchronized QDBPSession logonTIS(String login, String password){return null;}
 public synchronized QDBPSession logonSQL(String login, String password)
   throws java.sql.SQLException
 {
 gcConnections();
  QDBPSession s = createSession();
  s.setIDOnce(new Long(seq_sessions++));
  QDBPConnection con = createExclusiveConnection(s.getID(), login,password);
  s.setLoginPassword(login,password);
  s.setExclusiveConnection(con);
  addSession(s);
  return s;
 }
 public QDBPConnection createExclusiveConnection(Long session_id,String login,String password)
  throws java.sql.SQLException,QDBPException
 {
 gcConnections();
  QDBPConnection con;
  String jdbc_url = getJDBCUrl();
  // open JDBC connection
  java.sql.Connection  dbc=DriverManager.getConnection(jdbc_url,login,password);
  con = new QDBPConnection(name,session_id,dbc);
  con.setExclusive();
  return(con);
 } //createExclusiveConnection
 public QDBPConnection createSharedConnection(){return(null);}
 public QDBPConnection getSharedConnection(QDBPSession s){return(null);}
 public void freeSharedConnection(QDBPConnection c){throw(new RuntimeException("not implemented (freeSharedConnection())"));}
 public synchronized void addSession(QDBPSession s){addSession(s,s.getID());}
 public synchronized void addSession(QDBPSession s,Long id)
 {
  if(ht_sessions == null)ht_sessions = new Hashtable();
  Object check_session = ht_sessions.get(id);
  if(check_session == null) { ht_sessions.put(id,s); }
  else{throw new QDBPException("session with id '" + id + "' exists");}
 }
 //constructors
 public QDBPool(String name, String jdbc_url, String jdbc_driver_class)
 {
  this.name = name; this.jdbc_url = jdbc_url; this.jdbc_driver_class=jdbc_driver_class;
  try{register_jdbc_driver(jdbc_driver_class);}catch(Exception e){}
 }
} //QDBPool class

// QDBPException.java - это общий класс исключений пакета QDBPool
public static class QDBPException extends RuntimeException {public QDBPException(String msg){super(msg);}}

// QDBPConnection.java - это класс-контейнер для хранения-передачи JDBC соединения с БД
public static class QDBPConnection
{
 public static String SQL_TIS_LOGIN_WITH_COOKIE= "select SAM.login_with_cookie(?,?);";
 public static String SQL_TIS_LOGIN = "select SAM.login(?,?,?);";
 public static String SQL_PG_PING = "select session_user;";
 private String dbpool_name;
 private Long session_id=null;
 private java.sql.Connection dbc;
 private boolean is_exclusive=false;
 //
 public java.sql.Connection get(){return(dbc);}
 public Long getSessionID(){return(session_id);}
 public void bind(QDBPSession s){if(s!=null && !is_exclusive){session_id=s.getID();}} //!SIC add check dbpool_name
 public void unbind(QDBPSession s){if(!is_exclusive){session_id=null;}}
 public void setExclusive(){is_exclusive=true;}
 public boolean isExclusive(){return(is_exclusive);}
 public void free()
 {
  if(isExclusive()) { try{ dbc.close();} catch(SQLException e){} } //SIC
  // else { QDBPool.returnConnction(con); } // SIC! not implemented yet
 } // free()
 public boolean ping()
 {
  if(dbc==null)return(false);
    java.sql.Statement st = null;
    java.sql.ResultSet rs = null;
    try { st = dbc.createStatement(); rs = st.executeQuery(SQL_PG_PING); }
    catch(java.sql.SQLException sqle){return(false);}
    finally{
     try{if(rs != null) rs.close();}catch(SQLException e){}
     try{if(st != null) st.close();}catch(SQLException e){}
    }
  return(true);
 } //!SIC
 public void close()
  throws java.sql.SQLException
 {
   if(dbc != null){ dbc.close(); dbc=null;}
 }
 // destructor
 protected void finalize() throws Throwable
 {
  try{ if(dbc != null){ dbc.close(); dbc=null;} } catch(Exception e){}
  super.finalize();
 } //finalize()
 // constructor
 public QDBPConnection(String dbpool_name,Long session_id,java.sql.Connection dbc){this.dbpool_name=dbpool_name;this.session_id=session_id;this.dbc=dbc;}
} //QDBPConnection

// QDBPSession.java - это класс-контейнер пользовательской сессии web or mobile
public static class QDBPSession
{
 private Long session_id;
 private String dbpool_name;
 private String secret_cookie;
 private long creation_ts; // creation time stamp
 private long lastusage_ts; // last usage time stamp
 public void renewLastUsageTS(){lastusage_ts=System.currentTimeMillis();}
 public long getLastUsage(){return(lastusage_ts);}
 //
 private QDBPConnection con;
 private String login;
 private String password;
 private boolean is_session = false;
 //
 //public void use(){} //SIC! not implemented (lock to exclusive use)
 //public void free(){} //SIC! not implemented
 public void freeConnection() { if(con == null) return; con.free(); con=null; }
 public boolean isExclusiveConnection(){return(true);}// SIC! only exclusive now
 public void setExclusiveConnection(QDBPConnection con){this.con=con; con.setExclusive(); is_session=true; }
 public void setSharedConnection(QDBPConnection con){} //SIC not implementd yet
 public QDBPConnection getConnection() throws java.sql.SQLException, QDBPException
 {
  renewLastUsageTS();
  if(con != null) return(con); if(!is_session) return(null);
  QDBPool dbp = QDBPool.get(dbpool_name);
  if(dbp == null) throw new QDBPException("No DBPOOL '" +dbpool_name+ "' found");
  con=dbp.createExclusiveConnection(session_id,login,password);
  return(con); //SIC! no pool-obtained connections implemented yet
 }
 public String getLogin(){return(login);}
 public void setLoginPassword(String login, String password){ this.login = login; this.password = password; }
 public Long getID(){return(session_id);}
 public void setIDOnce(Long new_session_id){if(session_id==null)session_id = new_session_id;} //!SIC
 public String getSessionSecretCookie(){ return(getID()+":"+getSecretCookie()); }
 public String getSecretCookie(){if(secret_cookie!=null)return(secret_cookie); return(renewSecretCookie()); } //!SIC may be must be getSecretCookieOnce()
 public int getSessionCookieMaxAge(){return(60*60*24);}
 private String renewSecretCookie()
 {
  //secret_cookie = (new Integer(this.hashCode())).toString(); // first insecure version
  java.security.SecureRandom sr = new java.security.SecureRandom();
  byte[] b = new byte[32];
  sr.nextBytes(b);
  secret_cookie = java.util.Base64.getEncoder().withoutPadding().encodeToString(b);
  return(secret_cookie);
 } //SIC! no DB updateed yet
 public boolean checkSecretCookie(String cookie){if(secret_cookie==null)return(false);return(secret_cookie.equals(cookie));}
 public java.sql.Connection getSQLConnection() throws java.sql.SQLException {QDBPConnection con=getConnection();if(con==null)return(null);return(con.get());}
 public void logout() throws java.sql.SQLException
 {
   login=null; password=null;
   if(con != null) con.close(); //SIC! avoid next lines if exception
   con=null; is_session=false;
 } //logout
 //constructors
 public QDBPSession(String dbpool_name,Long id){this.dbpool_name=dbpool_name;this.session_id = id; creation_ts=System.currentTimeMillis();renewLastUsageTS();}
} //QDBPSession

// QDBPWDigest - Password Digest class for creation md5/sha256 digest of password compatible with apache tomcat algorithm
// no salt and iterations (yet?). just simple hash equals to: 
//  # /usr/local/apache-tomcat-9.0/bin/digest.sh -s 0 -a md5 test | awk -F: '{print $2}'
// this class was written looking to tomcat 9.0.74 implementation at:
//  java/org/apache/catalina/realm/RealmBase.java : main(..)
//  java/org/apache/catalina/realm/MessageDigestCredentialHandler.java : String mutate(String inputCredentials, byte[] salt, int iterations)
//  java/org/apache/tomcat/util/security/ConcurrentMessageDigest.java :  byte[] digest(String algorithm, int iterations, byte[]... input)
//  java/org/apache/tomcat/util/buf/HexUtils.java : String toHexString(byte[] bytes)
public static class QDBPWDigest
{
 private static char[] hex = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
 public static String byte2hex(byte b){return("" + hex[(b & 0xf0) >> 4] + hex[b & 0x0f]);}
 public static String digest(String password)
 {
  if(password==null)return(null);
  java.security.MessageDigest md=null;
  try{ md = md.getInstance("MD5"); }catch(java.security.NoSuchAlgorithmException nsae){return(null);}
  //try{
  byte ba[] = password.getBytes(); //password.getBytes("UTF-8");
  ba=md.digest(ba);
  StringBuffer sb = new StringBuffer();
  for(int i=0; i<ba.length; i++) { sb.append(byte2hex(ba[i])); }
  return(sb.toString());
  //} catch(UnsupportedEncodingException uee){return(null);}
 } //digest(String password)
} //QDBPasswordDigest

//END_QDBP

public void jspDestroy(  ) {System.gc(); try { Thread.sleep(1000); } catch (Exception e){}  System.runFinalization(); }

//
// static conversion helpful functions
// obj2text(), obj2html(), obj2value() - useful functions
// translate_tokens() - background work for them
//

 /** convert object to text even if object is null.
 */
 public static String obj2text(Object o)
 {
 if(o == null) return(SZ_NULL); return(o.toString());
 }

 /** convert object to html text even if object is null.
 * @see #obj2text
 * @see #text2html
 */
 public static String obj2html(Object o)
 {
 return(text2html(obj2text(o)));
 }

 //
 public static String[] HTML_UNSAFE_CHARACTERS = {"<",">","&","\n"};
 public static String[] HTML_UNSAFE_CHARACTERS_SUBST = {"&lt;","&gt;","&amp;","<br>\n"};
 public final static String[] VALUE_CHARACTERS = { "<",">","&","\"","'" };
 public final static String[] VALUE_CHARACTERS_SUBST = {"&lt;","&gt;","&amp;","&quot;","&#039;"};

 /** convert plain textual data into html code with escaping unsafe symbols.
  * @param text - plain text
  * @return html escaped text
  */
 public static String text2html(String text)
 {
 return(translate_tokens(text,HTML_UNSAFE_CHARACTERS,HTML_UNSAFE_CHARACTERS_SUBST));
 } // text2html()

 /** convert plain textual data into html form value suitable for input or textarea fields.
  * @param text - plain text
  * @return escaped text
  */
 public static String text2value(String text)
 {
 return(translate_tokens(text,VALUE_CHARACTERS,VALUE_CHARACTERS_SUBST));
 } // text2html()

 /** replace all sz's occurrences of 'from[x]' onto 'to[x]' and return the result.
  * Each occurence processed once and result depend on token's order at 'from'. 
  * For instance: translate_tokens("hello",new String[]{"he","hel","hl"}, new String[]{"eh","leh","lh"})
  * give "ehllo", not "lehlo" or "elhlo" (in fact "hel" to "leh" translation never be done).
  */
 public static String translate_tokens(String sz, String[] from, String[] to)
 {
  if(sz == null) return(sz);
  StringBuffer sb = new StringBuffer(sz.length() + 256);
  int p=0;
  while(p<sz.length())
  {
  int i=0;
  while(i<from.length) // search for token
  {
   if(sz.startsWith(from[i],p)) { sb.append(to[i]); p=--p +from[i].length(); break; }
   i++;
  }
  if(i>=from.length) sb.append(sz.charAt(p)); // not found
  p++;
  }
  return(sb.toString());
 } // translate_tokens

 //
 // DB interaction & result printing methods
 //
public static class WARequestProcessor
{
public JspWriter out;


  /** execute sz_sql and print it's ResultSet as html table.
   */
   public void exec_sql(java.sql.Connection dbc,String sz_sql)
    throws java.sql.SQLException, java.io.IOException
   {
    java.sql.Statement st = null;
    java.sql.ResultSet rs = null;
    try
    {
     st = dbc.createStatement();
     rs = st.executeQuery(sz_sql);
     print_sql_rs(rs);
    }
    catch(SQLException e){
     printerrln("sql error during \"" + sz_sql + "\": " + e ); 
    }
    finally{
     try{if(rs != null) rs.close();}catch(SQLException e){}
     try{if(st != null) st.close();}catch(SQLException e){}
    }
   } //exec_sql()


  /** print the whole of rs as html table/
   */
   public void print_sql_rs(java.sql.ResultSet rs)
    throws java.sql.SQLException,  java.io.IOException
   {
   java.sql.ResultSetMetaData rsmd = rs.getMetaData();
   int column_count=rsmd.getColumnCount();
   int i;
    // print column's titles
    out.println("<table>");
    out.println("<tr>");
    for(i=1;i<=column_count;i++)
    {
    out.print("<th>");
    printmsg(obj2html(rsmd.getColumnName(i) +
      "(" + rsmd.getColumnTypeName(i)) + ")");
    out.println("</th>");
    } // for column's titles
    out.println("</tr>");
    while(rs.next())
    {
     // print columns values
     out.println("<tr>");
     for(i=1;i<=column_count;i++)
     {
      out.print("<td>");
      printmsg(obj2html(rs.getObject(i))); // rs.getObject(i).getClass().getName()
      out.println("</td>");
     } // for column's values
    out.println("</tr>");
    } // while(rs.next())
    out.println("</table>");
   } // print_sql_rs

   /** print message to stdout. TISExmlDB.java legacy where have been wrapper to System.out.print */
   public  void printmsg(String msg) throws java.io.IOException {out.print(msg);}
   public  void printmsgln(String msg) throws java.io.IOException {out.println(msg);}
   public  void printmsgln() throws java.io.IOException {out.println();}

   /** print message to stderror. TISExmlDB.java legacy where have been just a wrapper to System.err.print */
   public  void printerr(String msg) throws java.io.IOException {out.print("<b>" + obj2html(msg) + "</b>");}
   public  void printerrln(String msg) throws java.io.IOException {printerr(msg);out.print("<br>");}
   public  void printerrln() throws java.io.IOException {out.println();}
} // WARequestProcessor
%><%
 if(jsp_instance == 0) {jsp_instance = ++ jsp_instance_count;}
 // 1. get or create DB pool from application settings
 QDBPool dbp = QDBPool.get(DBPOOL_NAME);
 QDBPSession dbps = null;
 if(dbp == null){dbp = new QDBPool(DBPOOL_NAME,DBPOOL_URL,DBPOOL_JDBC_CLASS); QDBPool.add(dbp);}
 // 2. get session
 // BEGIN
 // 2.1. get session cookie and its value if so
 Cookie[] cookies = request.getCookies();
 Cookie session_cookie=null;
 String session_cookie_value=null;
 if(cookies != null)
  for(int i=0;i<cookies.length;i++){if(QTIS_SESSION_COOKIE.equals(cookies[i].getName()))session_cookie=cookies[i];}
 if(session_cookie!=null) session_cookie_value =  session_cookie.getValue();
 // 2.2. get session by cookie
 dbps = dbp.logon(session_cookie_value);
 if(dbps == null) dbps = dbp.createSession(); // temporary session stub, not stored in the pool
 // END get session
 // 3. process request
 long enter_time = System.currentTimeMillis();
 WARequestProcessor wa = new WARequestProcessor();
 request.setCharacterEncoding("UTF-8");
 wa.out = out;
 String cmd=request.getParameter("cmd");
 if(cmd == null)cmd=CMD_HOME;
 //Thread.sleep(4000);

if(true){ // ready for non-html answer
 //
 // some hints for old and buggy browsers like NN4.x
 //

 long expire_time = enter_time + 24*60*60*1000;
 response.setHeader("Cache-Control","No-cache, no-store, must-revalidate");
 response.setHeader("Pragma","no-cache");
 response.setDateHeader("Expires",expire_time);
 request.setCharacterEncoding("UTF-8");
 String szSQLRequest=SZ_EMPTY;

%><!DOCTYPE html>
<html>
 <head>
  <title><%= CGI_TITLE %></title>
 </head>
<body>
  <h2><%= CGI_TITLE %></h2>
<a href="<%=CGI_NAME%>">[home]</a>
<a href="<%=CGI_NAME%>?cmd=login">[login]</a>
<a href="<%=CGI_NAME%>?cmd=pwdigest">[pwdigest]</a>
<a href="<%=CGI_NAME%>?cmd=psql">[psql]</a>
<a href="<%=CGI_NAME%>?cmd=psql_json">[psql_json]</a>
<a href="<%=CGI_NAME%>?cmd=freecon">[freecon]</a>
<a href="<%=CGI_NAME%>?cmd=gc">[GC]</a>
<a href="<%=CGI_NAME%>?cmd=whoami">[whoami]</a>
<a href="<%=CGI_NAME%>?cmd=status">[status]</a>
<br>
<% if(CMD_LOGIN.equals(cmd)) {
  String btn_login=request.getParameter("btn_login");
  String btn_logout=request.getParameter("btn_logout");
  if(btn_logout != null)
  {
   dbps.logout();
    session_cookie = new Cookie(QTIS_SESSION_COOKIE,"delete");
    session_cookie.setMaxAge(0); //delete
    session_cookie.setPath("/"); // whole site
    response.addCookie(session_cookie);
   out.println("Logout!<br>");
  }
  if(btn_login != null)
  {
   String login=request.getParameter("login");
   String password=request.getParameter("password");
   try{
    if(dbps != null) dbps.logout();
    dbps = dbp.logon(login,password);
    // BEGIN set session secret cookie
    session_cookie = new Cookie(QTIS_SESSION_COOKIE,dbps.getSessionSecretCookie());
    session_cookie.setHttpOnly(true); // not JS
    session_cookie.setSecure(true); // only over https
    session_cookie.setPath("/"); // whole site
    session_cookie.setMaxAge(dbps.getSessionCookieMaxAge()); // one day
    response.addCookie(session_cookie);
    // END set session secret cookie
    out.println("login Ok!<br>");
    }
    catch(Exception e){wa.printerrln(e.toString());}
  }
%> 
  <form method="POST" action="<%=CGI_NAME%>?cmd=login">
   Login:<input type="text" name="login" value="tiscuser1"><br>
   Password:<input type="password" name="password" value="password"></br>
   <input type="submit" name="btn_login" value="login">
   <input type="submit" name="btn_logout" value="logout">
  </form>
<% }else if(CMD_PASSWORD_DIGEST.equals(cmd)) {
  String btn_pwdigest=request.getParameter("btn_pwdigest");
  if(btn_pwdigest!=null)
  {
   String password=request.getParameter("password");
   out.println("password: " + password);
   out.println("<br>");
   out.println("Digest: " + QDBPWDigest.digest(password));
   out.println("<br>");
  }
%>
  <form method="POST" action="<%=CGI_NAME%>?cmd=pwdigest">
   Password:<input type="password" name="password" value="password"></br>
   <input type="submit" name="btn_pwdigest" value="digest">
<% }else if(CMD_WHOAMI.equals(cmd)) {
    out.println("You are :" + obj2html(dbps.getLogin()));
    out.println("SESSION cookie:" + obj2html(session_cookie_value));
    out.println("<br>\nConnection:");
    QDBPConnection dbc = dbps.getConnection();
    if(dbc == null) {out.println("null");}
    else {out.println("" + dbc.ping());}
   }else if(CMD_STATUS.equals(cmd)) {
    out.println("DBPOOL_NAME :" + dbp.getName() +"<br>" );
    out.println("Sessions :<br>" );
    out.println("<ul>" );
    Vector<QDBPSession> v = dbp.listSessions();
    for(int i=0;i<v.size();i++){
     QDBPSession s = v.get(i);
     out.println("<li>" + s.getID() + ":" + obj2html(s.getLogin())  );
    }
    out.println("</ul>" );
    out.println("Connections :<br>" );
    out.println("<ul>" );
    out.println("</ul>" );
   }else if(CMD_FREECONNECTION.equals(cmd)) {
    dbps.freeConnection();
    out.println("Connection freed<br>" );
   }else if(CMD_GC.equals(cmd)) {
    // run garbage collector
    System.gc(); try { Thread.sleep(100); } catch (Exception e){}  System.runFinalization();
    out.println("Garbage collector executed<br>" );
   }else if(CMD_PSQL.equals(cmd)) { %> 
  <form method="POST" action="<%=CGI_NAME%>?cmd=psql">
  SQL request:<br>
  <textarea name="SQLRequest" rows="10" cols="72"><%

  //
  // get SQL request from the passed parameters 
  // and display it as <textarea>
  //

  szSQLRequest=request.getParameter("SQLRequest");
  if(SZ_EMPTY.equals(szSQLRequest) || szSQLRequest == null){
   out.println("select SAM.get_user()");
  }else{
   out.print(text2value(szSQLRequest));
  }

  %></textarea><br>
  <input type="submit" value="Execute">
  </form>
 <hr>
 <%

  //
  // passed SQL request executing
  //

  if((!SZ_EMPTY.equals(szSQLRequest)) && szSQLRequest != null)
  {
   try{
     java.sql.Connection dbc = dbps.getSQLConnection();
     if(dbc == null){throw new Exception("No session!");}
     wa.exec_sql(dbc,szSQLRequest);
    }
    catch(Exception e){wa.printerrln(e.toString());}
  }
 %>
<% } // CMD_PSQL %>
  <hr>
  <i>timing : <%= ((System.currentTimeMillis() - enter_time) + " ms") %></i>
 <br>
  Hello! your web-server is <%= application.getServerInfo() %><br>
  <i>ver : <%= JSP_VERSION %> QDBPool ver: <%= QDBPool.getVer() %> jsp_instance = <%=jsp_instance %> <%= this %> </i>
  <!-- Привет this is just for UTF-8 testing (must be russian word "Privet") -->
</body>
</html><%}%>
