<%--
 ConcepTIS project
 (c) Alex V Eustrop 2009,2023
 LICENSE : BALES, BSD, MIT on your choice see bales.eustrosoft.org
 also see LICENSE at the project's root directory

 Purpose: PostgreSQL DB access via tiny JSP-based application using
          ConcepTIS/QTIS QDBBool and QTISSessionCookie
 History (psql.jsp & dbpool.jsp):
  2009/11/21 started from the TISExmlDB.java,v 1.1 2009/10/04 14:28:00 eustrop Exp
  2009/11/25 done. size: 271 lines
  2009/11/28 some finishing. CVS import. size 277 line.
  ...
  2023/04/07 dbpool.jsp started from psql.jsp
  ...
  2023/04/31 qsql.jsp started from qdbpool.jsp (407 lines)
--%><%@
  page contentType="text/html; charset=UTF-8"
  import="java.util.*"
  import="java.io.*"
  import="java.sql.*"
  import="org.eustrosoft.qdbp.*"
  import="org.eustrosoft.qtis.SessionCookie.*"
%><%@ page isThreadSafe="true"  %><%!
//
// Global parameters
//
private final static String CGI_NAME = "qsql.jsp";
private final static String CGI_TITLE = "QSQL.jsp - PSQL-like tool using using qDBPool/qSession";

private final static String JSP_VERSION = "0.6-20230432";

private final static String SZ_EMPTY = "";
private final static String SZ_NULL = "<<NULL>>";
private final static String SZ_UNKNOWN = "<<UNKNOWN>>";
// CMD codes
public final static String CMD_HOME="home";
public final static String CMD_LOGIN="login";
public final static String CMD_PSQL="psql";


//BEGIN_QDBSP

//DBPOOL configuration
//private final static String QDBPOOL_NAME = "QSQL"; // name of QDBPoll
//private final static String QDBPOOL_URL = "jdbc:postgresql://conceptis-pg-server:5432/conceptisdb"; // url of database
//private final static String QDBPOOL_JDBC_CLASS = "org.postgresql.Driver"; // java class to register
private final static String PARAM_QDBPOOL_NAME = "QDBPOOL_NAME"; // name of QDBPoll
private final static String PARAM_QDBPOOL_URL = "QDBPOOL_URL";
private final static String PARAM_QDBPOOL_JDBC_CLASS = "QDBPOOL_JDBC_CLASS";
// parameters below not used yet:
private final static String DBPOOL_USER = ""; // name of database user. if not set - logon with passed session user/password to DB
private final static String DBPOOL_USER_PASSWORD = ""; //
private final static String DBPOOL_TISQL_LOGON = ""; // "select sam.logon(?,?,?);" (user,password,session_key)
private final static String DBPOOL_TISQL_BINDSESSION = ""; // "select sam.bind_session(?,?);" (session_id,session_key) 
private final static String DBPOOL_OPT_ALLOWTRUSTED = "NO"; // default NO
private final static String DBPOOL_OPT_MINPASSWORD = "8"; // minimun password length (default 8)

// code moved to org.eustrosoft.qdbp packages (http://ftp.eustrosoft.org/pub/eustrosoft.org/pkg/ConcepTIS/qDBPool/qDBPool.jar)
//END_QDBP

public void jspDestroy(  ) {System.gc(); try { Thread.sleep(1000); } catch (Exception e){}  System.runFinalization(); }

//
// static conversion helpful functions
// obj2text(), obj2html(), obj2value() - useful functions
// translate_tokens() - background work for them
//

 /** convert object to text even if object is null.
 */
 public static String obj2text(Object o) { if(o == null) return(SZ_NULL); return(o.toString()); }

 /** convert object to html text even if object is null.
 * @see #obj2text
 * @see #text2html
 */
 public static String obj2html(Object o) { return(text2html(obj2text(o))); }

 //
 public static String[] HTML_UNSAFE_CHARACTERS = {"<",">","&","\n"};
 public static String[] HTML_UNSAFE_CHARACTERS_SUBST = {"&lt;","&gt;","&amp;","<br>\n"};
 public final static String[] VALUE_CHARACTERS = { "<",">","&","\"","'" };
 public final static String[] VALUE_CHARACTERS_SUBST = {"&lt;","&gt;","&amp;","&quot;","&#039;"};

 /** convert plain textual data into html code with escaping unsafe symbols.
  * @param text - plain text
  * @return html escaped text
  */
 public static String text2html(String text)
 {
 return(translate_tokens(text,HTML_UNSAFE_CHARACTERS,HTML_UNSAFE_CHARACTERS_SUBST));
 } // text2html()

 /** convert plain textual data into html form value suitable for input or textarea fields.
  * @param text - plain text
  * @return escaped text
  */
 public static String text2value(String text)
 {
 return(translate_tokens(text,VALUE_CHARACTERS,VALUE_CHARACTERS_SUBST));
 } // text2html()

 /** replace all sz's occurrences of 'from[x]' onto 'to[x]' and return the result.
  * Each occurence processed once and result depend on token's order at 'from'. 
  * For instance: translate_tokens("hello",new String[]{"he","hel","hl"}, new String[]{"eh","leh","lh"})
  * give "ehllo", not "lehlo" or "elhlo" (in fact "hel" to "leh" translation never be done).
  */
 public static String translate_tokens(String sz, String[] from, String[] to)
 {
  if(sz == null) return(sz);
  StringBuffer sb = new StringBuffer(sz.length() + 256);
  int p=0;
  while(p<sz.length())
  {
  int i=0;
  while(i<from.length) // search for token
  {
   if(sz.startsWith(from[i],p)) { sb.append(to[i]); p=--p +from[i].length(); break; }
   i++;
  }
  if(i>=from.length) sb.append(sz.charAt(p)); // not found
  p++;
  }
  return(sb.toString());
 } // translate_tokens

 //
 // DB interaction & result printing methods
 //
public static class WARequestProcessor
{
public JspWriter out;
 
  /** execute sz_sql and print it's ResultSet as html table.
   */
   public void exec_sql(java.sql.Connection dbc,String sz_sql)
    throws java.sql.SQLException, java.io.IOException
   {
    java.sql.Statement st = null;
    java.sql.ResultSet rs = null;
    try
    {
     st = dbc.createStatement();
     rs = st.executeQuery(sz_sql);
     print_sql_rs(rs);
    }
    catch(SQLException e){
     printerrln("sql error during \"" + sz_sql + "\": " + e ); 
    }
    finally{
     try{if(rs != null) rs.close();}catch(SQLException e){}
     try{if(st != null) st.close();}catch(SQLException e){}
    }
   } //exec_sql()


  /** print the whole of rs as html table/
   */
   public void print_sql_rs(java.sql.ResultSet rs)
    throws java.sql.SQLException,  java.io.IOException
   {
   java.sql.ResultSetMetaData rsmd = rs.getMetaData();
   int column_count=rsmd.getColumnCount();
   int i;
    // print column's titles
    out.println("<table>");
    out.println("<tr>");
    for(i=1;i<=column_count;i++)
    {
    out.print("<th>");
    printmsg(obj2html(rsmd.getColumnName(i) +
      "(" + rsmd.getColumnTypeName(i)) + ")");
    out.println("</th>");
    } // for column's titles
    out.println("</tr>");
    while(rs.next())
    {
     // print columns values
     out.println("<tr>");
     for(i=1;i<=column_count;i++)
     {
      out.print("<td>");
      printmsg(obj2html(rs.getObject(i))); // rs.getObject(i).getClass().getName()
      out.println("</td>");
     } // for column's values
    out.println("</tr>");
    } // while(rs.next())
    out.println("</table>");
   } // print_sql_rs

   /** print message to stdout. TISExmlDB.java legacy where have been wrapper to System.out.print */
   public  void printmsg(String msg) throws java.io.IOException {out.print(msg);}
   public  void printmsgln(String msg) throws java.io.IOException {out.println(msg);}
   public  void printmsgln() throws java.io.IOException {out.println();}

   /** print message to stderror. TISExmlDB.java legacy where have been just a wrapper to System.err.print */
   public  void printerr(String msg) throws java.io.IOException {out.print("<b>" + obj2html(msg) + "</b>");}
   public  void printerrln(String msg) throws java.io.IOException {printerr(msg);out.print("<br>");}
   public  void printerrln() throws java.io.IOException {out.println();}
} // WARequestProcessor
%><%
// REQUEST PROCESSIN STARTED HERE:
 // 0. get configuration from web-application parameters (WEB-INF/web.xml)
 String QDBPOOL_NAME = this.getServletContext().getInitParameter(PARAM_QDBPOOL_NAME);
 String QDBPOOL_URL = this.getServletContext().getInitParameter(PARAM_QDBPOOL_URL);
 String QDBPOOL_JDBC_CLASS = this.getServletContext().getInitParameter(PARAM_QDBPOOL_JDBC_CLASS);
 long enter_time = System.currentTimeMillis();
 // 1. get or create DB pool 
 QDBPool dbp = QDBPool.get(QDBPOOL_NAME);
 QDBPSession dbps = null;
 if(dbp == null){dbp = new QDBPool(QDBPOOL_NAME,QDBPOOL_URL,QDBPOOL_JDBC_CLASS); QDBPool.add(dbp);}
 // 2. get session
 // 2.1. get session cookie and its value if so
 QTISSessionCookie session_cookie = new QTISSessionCookie(request, response);
 // 2.2. get session by cookie
 dbps = dbp.logon(session_cookie.value());
 // 2.3. renew session if ready (reserved for future use)
 if(dbps != null && dbps.isSessionRenewReady()) {dbps.renewSession(); session_cookie.set(dbps.getSessionSecretCookie(),dbps.getSessionCookieMaxAge()); }
 if(dbps == null) dbps = dbp.createSession(); // temporary session stub, not stored in the pool
 // END get session
 // 3. process request
 WARequestProcessor wa = new WARequestProcessor();
 request.setCharacterEncoding("UTF-8");
 wa.out = out;
 String cmd=request.getParameter("cmd");
 if(cmd == null)cmd=CMD_PSQL;

if(true){ // ready for non-html answer
 //
 // some hints for old and buggy browsers like NN4.x
 //

 long expire_time = enter_time + 24*60*60*1000;
 response.setHeader("Cache-Control","No-cache, no-store, must-revalidate");
 response.setHeader("Pragma","no-cache");
 response.setDateHeader("Expires",expire_time);
 request.setCharacterEncoding("UTF-8");
 String szSQLRequest=SZ_EMPTY;

%><!DOCTYPE html>
<html>
 <head>
  <title><%= CGI_TITLE %></title>
 </head>
<body>
  <h2><%= CGI_TITLE %> (<a href="<%=CGI_NAME %>?cmd=login"><% out.print(obj2html(dbps.getLogin())); %>)</a> </h2>
<a href="<%=CGI_NAME%>?cmd=psql">[psql]</a>
<a href="<%=CGI_NAME%>?cmd=login">[login]</a>
<br>
<%
  if(CMD_LOGIN.equals(cmd)) {
  String btn_login=request.getParameter("btn_login");
  String btn_logout=request.getParameter("btn_logout");
  if(btn_logout != null)
  {
   dbps.logout();
    session_cookie.delete();
   out.println("Logout!<br>");
  }
  if(btn_login != null)
  {
   String login=request.getParameter("login");
   String password=request.getParameter("password");
   try{
    if(dbps != null) dbps.logout();
    dbps = dbp.logon(login,password);
    session_cookie.set(dbps.getSessionSecretCookie(),dbps.getSessionCookieMaxAge());
    out.println("login Ok!<br>");
    }
    catch(Exception e){wa.printerrln(e.toString());}
  }
  out.println("session cookie should expare at (s):" + dbps.getSessionCookieExpire());
  if(!request.isSecure()){
      wa.printerr("Logon over insecure connection depricated");
  }
  else {
%> 
  <form method="POST" action="<%=CGI_NAME%>?cmd=login">
   Login:<input type="text" name="login" value=""><br>
   Password:<input type="password" name="password" value=""></br>
   <input type="submit" name="btn_login" value="login">
   <input type="submit" name="btn_logout" value="logout">
  </form>
<% }}else if(CMD_PSQL.equals(cmd)) { %> 
  <form method="POST" action="<%=CGI_NAME%>?cmd=psql">
  SQL request:<br>
  <textarea name="SQLRequest" rows="10" cols="72"><%

  //
  // get SQL request from the passed parameters 
  // and display it as <textarea>
  //

  szSQLRequest=request.getParameter("SQLRequest");
  if(SZ_EMPTY.equals(szSQLRequest) || szSQLRequest == null){
   out.println("select SAM.get_user()");
  }else{
   out.print(text2value(szSQLRequest));
  }

  %></textarea><br>
  <input type="submit" value="Execute">
  </form>
 <hr>
 <%

  //
  // passed SQL request executing
  //

  if((!SZ_EMPTY.equals(szSQLRequest)) && szSQLRequest != null)
  {
   try{
     java.sql.Connection dbc = dbps.getSQLConnection();
     if(dbc == null){throw new Exception("No session!");}
     wa.exec_sql(dbc,szSQLRequest);
    }
    catch(Exception e){wa.printerrln(e.toString());}
  }
 %>
<% } // CMD_PSQL %>
  <hr>
  <i>timing : <%= ((System.currentTimeMillis() - enter_time) + " ms") %></i>
 <br>
  Hello! web-server is <%= application.getServerInfo() %><br>
  <i>ver : <%= JSP_VERSION %> QDBPool ver: <%= QDBPool.getVer() %> QTISSessionCookie ver= <%= QTISSessionCookie.getVer() %> QDBPOOL_NAME = <%=QDBPOOL_NAME %> </i>
  <!-- Привет this is just for UTF-8 testing (must be russian word "Privet") -->
</body>
</html><%}%>
