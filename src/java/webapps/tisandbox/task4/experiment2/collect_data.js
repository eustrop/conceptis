var data;
function collect_data(){
    var forms = document.getElementsByTagName("form");
    var form = forms[0];
    var formies = form.getElementsByTagName("*");
    var jsonstring = '';
    for(let i = 0; i < formies.length; i++){
        if(is_field4json(formies[i])) {
            jsonstring += field2jstr(formies[i].name,formies[i].value);
        }
    }
    jsonstring = rm_extra_comma(jsonstring);
    jsonstring = '{' + jsonstring + '}';
    //console.log(jsonstring);
    var json = JSON.parse(jsonstring);
    //console.log(json);
    return json;
}

function v2jstr(value)
{
    value = "" + value;
    //value = value.replace(/\n/g, '\\n');
    //value = value.replace(/\r/g, '\\r');
    //return("\"" + value + "\"" );
    var o = {x:""};
    o.x = value;
    o = JSON.stringify(o);
    o = o.replace("{\"x\":", '');
    o = o.substring(0, o.length-1);
    return(o);
}

function field2jstr(name,value)
{
    return (v2jstr(name) + ":" + v2jstr(value) + ",\n");
}
function rm_extra_comma(str)
{return(str.substring(0, str.length-2));}

function is_field4json(field)
{
    if (field.matches('input')){
        if (field.type === "text")  return true ;
        if (field.type === "hidden") return true ;
    }
    if (field.matches('select'))  return true;
    if (field.matches('textarea')) return true;
    return false;
}

/* 
** this function removes all data
** inside the div with id "master"
*/
function removeAllData(){
    //console.log("Delete pressed!");
    data = collect_data();
    //console.log(data);
    master.innerHTML = "";
    // then creating button Backup
    var table = document.createElement("table");
    table.setAttribute("id", "table1");
    var tr = document.createElement("tr");
    var td = document.createElement("td");
    var btn_backup = document.createElement("input", {
        "type": "submit",
        "value": "↻",
        "name": "__REC.1.__DEL",
        "onclick": "backup()",
    });
    /*
    ** For some reasons the input attributes above 
    ** don't work. So set them as below
    */
    btn_backup.type = "submit";
    btn_backup.value = "↻";
    btn_backup.name = "__REC.1.__DEL";
    btn_backup.setAttribute("onclick", "backup()");
    td.append(btn_backup);
    tr.append(td);
    td = document.createElement("td");
    td.innerHTML = "Backup";
    tr.append(td);
    table.append(tr);
    master.append(table);
}

function hasLowerCase(str) {
    return (/[a-z]/.test(str));
}

function unescapeHTML(escapedHTML) {
    return escapedHTML.replace(/&lt;/g,'<').replace(/&gt;/g,'>').replace(/&amp;/g,'&');
}

function backup(){
    //console.log("Backup pressed");
    var base = create_base_doc(data);
    //console.log(base);
    master.innerHTML += base;
    // for (name in data){
        //console.log(name, ": ", data[name]);
        //if (hasLowerCase(name)){
        //     var parts = name.split('.');
        //     var field_name = parts.pop();
        //     var row = "<tr><td>" + field_name + "</td><td><input type='text' value=" + data[name] +"></td></tr>";
        //     table1.innerHTML += row;
        // //}
    // }
    master.innerHTML += "<hr>";
    //then make Backup Delete again
    btn = document.getElementsByName("__REC.1.__DEL");
    btn[0].value = "x";
    btn[0].setAttribute("onclick", "removeAllData()");
    btn[0].parentElement.nextSibling.innerHTML = "Delete record";
}

function reload_page(){
    location.reload();
}