"use strict"

var hidden_names = ["ZOID", "ZRID", "ZVER", "ZTOV", "ZSID", "ZLVL", "ZPID", "ZTYPE", "ZUID", "ZSTA", "ZDATE", "ZDATO"];
var dd02 = ["MANU", "ARTL", "BOOK", "DISS", "ENC", "REF", "LIST", "ANNX", "SITE"];
var dp01 = ["NUM", "TEXT", "MIX"];

function test(){
    /*
    ** here we just want to see what is 
    ** inside the form
    */
    var forms = document.getElementsByTagName("form");
    var form = forms[0];
    console.log(form.innerHTML);
}

function create_base_doc(data){
    var base = "";
    for (var name in data){
        var parts = name.split('.');
        if (parts.length == 2){
            var row = "<input type='hidden' name='" + name + "' value='" + data[name] + "'>";
            base += row;
        }
    }
    return base;
}

function hidden_field(field_name){
	if  (hidden_names.includes(field_name))
		return true;
	return false;
}

/*
** create_tr() can take more than two arguments,
** they will just be put in other <td></td>
*/
function create_tr(innere_1, innere_2){
	var reihe;
	if (hidden_field(innere_1)){
		reihe = innere_2 || "";
	}
	else {
		reihe = `<tr>
		<td><b>${innere_1 || ""}</b></td>
		<td>${innere_2 || ""}</td>
		</tr>`;
		for (let i = 2; i < arguments.length; i++){
			reihe = reihe.slice(0, -5) + "\n" + `<td>${arguments[i] || ""}</td>`;
		}
		reihe += "\n" + `</tr>`;
	}
	return reihe;
}

function create_table(innere){
    var table = `
	<table>
	<tbody>
	${innere || ""}
	</tbody>
	</table>
	`;
    return table;
}

function create_input(name, val, field_name){
	var type;
	if (hidden_field(field_name))
		type = "hidden";
	else type = "text";
	var inpu = `<input name="${name}", type="${type}", value="${val || ""}">`;
	return inpu;
}

function create_select(name, val, field_name, set){
	var selec = `<select name="${name}">`;
	for (let i = 0; i < set.length; i++){
		var optio = `<option value="${set[i]}">${set[i]}</option>`;
		if (set[i] == val)
			optio = optio.slice(0, optio.indexOf("\">") + 1) + " selected" + optio.slice(optio.indexOf("\">") + 1);
		selec += "\n" + optio;
	}
	selec += "\n" + `</select>`;
	return selec;
}

function create_form(key, val, prefix, field_name){
	var reihe;
	if (field_name == "type"){
		var parts = prefix.split(".");
		if (parts.length == 2)
			reihe = create_tr(field_name, create_select(prefix + "." + key  + "." + field_name, val.value, field_name, dd02));
		if (parts.length == 4)
			reihe = create_tr(field_name, create_select(prefix + "." + key + "." + field_name, val.value, field_name, dp01));
	}
	else
		reihe = create_tr(field_name, create_input(prefix + "." + key + "." + field_name, val.value, field_name));
	tt.innerHTML += reihe;
}

function work_with_json(obj, prefix) {
	var key;
	if(obj === undefined) return;
	if( typeof obj.fields === 'object' ){
		for (key in obj.fields) {
			create_form(key, obj.fields[key], prefix, obj.fields[key].name);
		}
	}
	if( typeof obj.fieldsets === 'object' ){
		for (key in obj.fieldsets) {
			//if (typeof obj[key] === 'object') {
			//create_form(key, key);
			work_with_json(obj.fieldsets[key], prefix + "." + key);			
		}
	}
}

function get_json(){
    var xhttp = new XMLHttpRequest();
    var jsony;
    xhttp.onreadystatechange = function(){
        if (this.readyState == 4){
            jsony = JSON.parse(this.responseText);
			work_with_json(jsony, "REC_");
        }
    }
    let ur = "/tisc/REST.jsp?subsys=TISC&request=view&target=D&ZID=" + (doc_id.value || 11);
    xhttp.open("GET", ur, true);
    xhttp.send()
}

function create_obj_json(obj, alle){
	obj = {key: "null", code: "D"};
	for (let i = 0; i < alle.length; i++){
		if (alle[i].name != undefined && alle[i].name[0] == 'R')
		{
			//console.log(alle[i].name, alle[i].value);
			obj[alle[i].name] = alle[i].value;
		}
	}
	return obj;
}

function collect_data(){
	//console.log(document.body.innerHTML);
	var alle = document.getElementsByTagName('*');
	var obj = create_obj_json(obj, alle);
	console.log(obj);
	var json_str = JSON.stringify(obj);
	console.log(json_str);
	JSON.parse(json_str);
}
