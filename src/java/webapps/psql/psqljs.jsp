<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Database</title>
</head>
	<body>  	
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"
				type="text/javascript"></script>
		<script>
			$(document).ready(function(){
				$("#execute").click(function(){
					var type = $("input[name='dataType']:checked").val();
					//var ajaxURL_value = $("input[name='ajaxURL']").val(); //value from user
					var ajaxURL_value = $("#id_ajaxURL").val(); //value from user
					var ajaxURL = null; // real value
					// select real value for ajaxURL by user input ajaxURL_value
					if(ajaxURL_value == "0_serverNotExists.jsp") { ajaxURL = "serverNotExists.jsp"; }
					if(ajaxURL_value == "1_server.jsp") { ajaxURL = "server.jsp"; }
					if(ajaxURL_value == "2_serverDBPool.jsp") { ajaxURL = "serverDBPool.jsp"; }
					if(ajaxURL == null) {alert("invalid ajaxURL value passed:" + ajaxURL_value); return; }
					console.log("yes:" + ajaxURL);
					//console.log(type);
					if ($("#IdRequest").val() == ""){
						$("tbody").remove();
						return;
					}
					$.ajax({
						method:"POST",
						//url:"server.jsp",
						//url:"serverDBPool.jsp",
						url: ajaxURL,
						dataType: type,
						data:{
							ajaxVar: $("#IdRequest").val(),
							dataType: $("input[name='dataType']:checked").val(),
							},
						success: function (data){	
						
							console.log(data);
							if (type == "json") parseJSON(data);
							if (type == "text") parseCSV(data);
						}	
					});	
				});
			});
		</script>   
		<script>
		 function parseJSON(data){
			//first line
			$("tbody").remove();
			var firstLine = "<tr>";
			for(i in data[0]){
				firstLine+="<th bgcolor='#CCCCCC'>"+i.replace(" ","<br>(")+")</th>";
			}
			$("table").append("<tbody></tbody>");
			$("tbody").append(firstLine+"</tr>");
			
			//other lines
			for (i in data){
				var tempObj = data[i];
				var str = "<tr>";
				for (j in tempObj){
					str+="<td>"+tempObj[j]+"</td>";
				}
				$("tbody").append(str+"</tr>");
			}
		}	
		</script>
		<script type="text/javascript">
			function parseCSV(data){
				$("tbody").remove();
				$("table").append("<tbody></tbody>");
				
				data = data.trim();
				var allRows = data.split(/\r?\n|\r/);
				var delimiter = ",";
				var escape ="\"";
				//For each row 
				for (j in allRows){
					//------------------------------
					var characters = allRows[j];
					var oneRow = "<tr>";
					if (j == 0) {
						characters = characters.replace(new RegExp(" ","g"),"<br>");
					}
					for (i = 0; i < characters.length; ) {
						if (characters[i] ==delimiter || i==0){
							//Begining of the row is handled in the first "if"
							if (i!=0) i++;
							var singleField ="";
							if (j==0) singleField+="<th bgcolor='#CCCCCC'>";
							else singleField+= "<td>";
							
							var quotes = false;
							//If we find [,"] then copy characters until [",]
							if ((characters[i] ==escape && i!=0)|| (((i==0)&& characters[i] == escape) && characters[i+1] != escape)) {
								i++;
								while (quotes||(i<characters.length-1) &&(characters[i] != escape || characters[i + 1] != delimiter)) {
									// If we find ["] then set flag
									if (characters[i] == escape && !quotes) quotes =true;
									// If flag was installed and now ["] then drop flag
									else if (characters[i] == escape && quotes ) quotes = false;
									if (!quotes) singleField+=characters[i];
									i++;
								}
							}
							//If we find [,] then copy characters until [,]
							else {
								while ((i<characters.length) && characters[i] !=  delimiter) {
									// If we find ["] then set flag
									if (characters[i] == escape && !quotes) quotes =true;
									// If flag was installed and now ["] then drop flag
									else if (characters[i] == escape && quotes ) quotes = false;
									if (!quotes) singleField+=characters[i];
									i++;
								}
							}
							if (j==0) singleField+="</th>";
							else singleField +="</td>";
							//console.log(singleField);
							oneRow+=singleField;
						}
						else i++;
					}
					//console.log("new row: "+oneRow);
					$("tbody").append(oneRow);
				}
			}
		</script>
		
		SQL request:<br>
		<textarea name="SQLRequest" id="IdRequest" rows="10" cols="72">select SAM.get_user()<%
		
		%></textarea><br>
		AJAX URL: 
		<select name="ajaxURL" id="id_ajaxURL" >
		 <option value="0_serverNotExists.jsp">serverNotExists.jsp</option>
		 <option value="1_server.jsp" selected>server.jsp</option>
		 <option value="2_serverDBPool.jsp">serverDBPool.jsp</option>
		 <option value="Invalid Value">Pass invalid value</option>
		</select>
		Data transfer format:
		<label><input type="radio"  checked name="dataType" value="json"/> JSON</label> 
		<label><input type="radio" name="dataType" value="text"/> CSV</label>
		<br>
		<input type="submit" value="Execute" id="execute">
		
		<table >
		  
		</table>		
	</body> 
</html>


