<%@
  page contentType="text/html; charset=UTF-8"
  import="java.util.*"
  import="java.io.*"
  import="java.sql.*"
  import="org.json.*"
  import="java.lang.*"

%>


<%!
	String delimiter = ",";
	String escape ="\"";
	
	/* Register jdbc_driver	*/
	//private final static String DBSERVER_URL = "jdbc:postgresql:conceptisdb";	
    public static void register_jdbc_driver(String jdbc_driver) throws Exception{
		Driver d;
		Class dc;
		try {
			dc = Class.forName(jdbc_driver);
			d = (java.sql.Driver) dc.newInstance();
		} catch (ClassNotFoundException e) {
			throw new Exception("register_jdbc_driver:" + "unable to get Class for "
				+ jdbc_driver + ":" + e);
		} catch (Exception e) {
			throw new Exception("register_jdbc_driver: unable to get driver "
				+ jdbc_driver + " : " + e);
		} 
		try {
			 DriverManager.registerDriver(d);
		} catch (SQLException e) {
			 throw new Exception(
					  "register_jdbc_driver: unable to register driver " + jdbc_driver + " : " + e);
		}
    }; 
	
   /* execute sz_sql and print it*/
	public JSONArray exec_sql_json(Connection dbc,String sz_sql) throws SQLException,IOException{
		
		Statement st = null;
		ResultSet rs = null;
		
		JSONArray jsonArray = new JSONArray();
		
		try{
			st = dbc.createStatement();
			rs = st.executeQuery(sz_sql);
			
			ResultSetMetaData rsmd = rs.getMetaData();
			int columCount=rsmd.getColumnCount();
			
			ArrayList<String> list = new ArrayList<>();
            for (int i = 1; i <= columCount ; i++) {
               //String temp = rsmd.getColumnName(i)+"<br> ("+rsmd.getColumnTypeName(i)+")";
				String temp = rsmd.getColumnName(i)+" "+rsmd.getColumnTypeName(i);
                list.add(temp);
            }
			
			while(rs.next()){
			    JSONObject object = new JSONObject();
				for(int i=1;i<= columCount;i++){
					object.put(list.get(i-1),rs.getObject(i)) ;
				}
				jsonArray.put(object);
			}
			
			if (jsonArray.length() == 0){
				JSONObject nullObject = new JSONObject();
				for (int i = 0; i <list.size(); i++){
					nullObject.put(list.get(i), "");
				}
				jsonArray.put(nullObject);
			}
		}
		catch(SQLException e){
			//printerrln("sql error during \"" + sz_sql + "\": " + e ); 
		}
		finally{
			try{
				if(rs != null) rs.close();
			}catch(SQLException e){}
			try{
				if(st != null) st.close();
			}catch(SQLException e){}
		}
		return jsonArray;
   } 
   
   	public String exec_sql_csv(Connection dbc,String sz_sql) throws SQLException,IOException{
		
		Statement st = null;
		ResultSet rs = null;
		StringBuilder result = new StringBuilder();
		
		try{
			st = dbc.createStatement();
			rs = st.executeQuery(sz_sql);
			
			ResultSetMetaData rsmd = rs.getMetaData();
			int columCount=rsmd.getColumnCount();
			
            for (int i = 1; i <= columCount ; i++) {
				result.append(rsmd.getColumnName(i)+" ("+rsmd.getColumnTypeName(i)+")");
				if (i!=columCount) result.append(",");
            }
			result.append("\n");
			
			while(rs.next()){
				for(int i=1;i<= columCount;i++){	
					Object ob = rs.getObject(i);
					if (ob instanceof String){
						String str = (String) ob;
						if (str.matches(".*"+escape+".*")) str = str.replaceAll("\"","\"\"");
						if (str.matches(".*"+delimiter+".*")) result.append(escape+str+escape);
						else result.append(str);
					}
					else result.append(ob);
					if (i != columCount) result.append(",");
				}
				result.append("\n");
			}
		}
		catch(SQLException e){
			
		}
		finally{
			try{
				if(rs != null) rs.close();
			}catch(SQLException e){}
			try{
				if(st != null) st.close();
			}catch(SQLException e){}
		}
		return result.toString();
   } 
%>


<%
	String SQLRequest = request.getParameter("ajaxVar");
    response.setContentType("text/html; char set=utf-8");
	
	String data = "1997,Ford,E350\"\"Quotes\"\",\"ac, \"\"abs\"\", moon\",3000.00\n" +
                "\"\"1999\"\",Chevy,\"Venture «Extended Edition»\",\"\",4900.00\r\n" +
                "\"19,96\",Jeep,Grand Cherokee,\"MUST SELL! air, moon roof, loaded\",4799.00\n"+
                "\"2018,\"\"Extra\"\"\",Jeep,Grand Cherokee,\"MUST SELL! air, moon roof, loaded\",4799.00";
	//out.println(data);
	
	String dataType = request.getParameter("dataType");
	
	JSONArray array = null;
	if(!SQLRequest.equals("") && SQLRequest != null){
		Connection dbc;
		try{
			register_jdbc_driver("org.postgresql.Driver");
			String DBSERVER_URL = this.getServletContext().getInitParameter("DBSERVER_URL");
			dbc=DriverManager.getConnection(DBSERVER_URL,request.getRemoteUser(),"");
			try{
				if (dataType.equals("json")){				
					array = exec_sql_json(dbc, SQLRequest);
					out.println(array);
				}else if (dataType.equals("text")){
					String result = exec_sql_csv(dbc, SQLRequest);
					out.println(result);
				}else out.println("ERROR");
			}finally{ 
			dbc.close();
			}
		}catch(Exception e){
			out.println("[Exception!]"); 
		}
	}
	
	%>
 
 
