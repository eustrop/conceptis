<%@
  page contentType="text/html; charset=UTF-8"
  import="java.util.*"
  import="java.io.*"
  import="java.sql.*"
  import="org.json.*"
  import="java.lang.*"
  import="org.eustrosoft.zDBCPool.*"
%>


<%!
	String delimiter = ",";
	String escape ="\"";
	String propertiesPath = "/s/proj/ConcepTIS/src/java/webapps/psql/work/installed_webapps/psql/config.conf"; 

	
	/* Register jdbc_driver	*/
	//private final static String DBSERVER_URL = "jdbc:postgresql:conceptisdb";	
  
   /* execute sz_sql and print it*/
	public JSONArray exec_sql_json(Connection dbc,String sz_sql) throws SQLException,IOException{
		
		Statement st = null;
		ResultSet rs = null;
		JSONArray jsonArray = new JSONArray();
		
		try{
			st = dbc.createStatement();
			rs = st.executeQuery(sz_sql);
			
			ResultSetMetaData rsmd = rs.getMetaData();
			int columCount=rsmd.getColumnCount();
			
			ArrayList<String> list = new ArrayList<>();
            for (int i = 1; i <= columCount ; i++) {
               //String temp = rsmd.getColumnName(i)+"<br> ("+rsmd.getColumnTypeName(i)+")";
				String temp = rsmd.getColumnName(i)+" "+rsmd.getColumnTypeName(i);
                list.add(temp);
            }
			
			while(rs.next()){
			    JSONObject object = new JSONObject();
				for(int i=1;i<= columCount;i++){
					object.put(list.get(i-1),rs.getObject(i)) ;
				}
				jsonArray.put(object);
			}
			
			if (jsonArray.length() == 0){
				JSONObject nullObject = new JSONObject();
				for (int i = 0; i <list.size(); i++){
					nullObject.put(list.get(i), "");
				}
				jsonArray.put(nullObject);
			}
		}
		catch(SQLException e){
			JSONObject err = new JSONObject();
			err.put(e.getMessage(), "");
			jsonArray.put(err);
		}
		finally{
			try{
				if(rs != null) rs.close();
			}catch(SQLException e){}
			try{
				if(st != null) st.close();
			}catch(SQLException e){}
		}
		return jsonArray;
   } 
   
   	public String exec_sql_csv(Connection dbc,String sz_sql) throws SQLException,IOException{
		
		Statement st = null;
		ResultSet rs = null;
		StringBuilder result = new StringBuilder();
		
		try{
			st = dbc.createStatement();
			rs = st.executeQuery(sz_sql);
			
			ResultSetMetaData rsmd = rs.getMetaData();
			int columCount=rsmd.getColumnCount();
			
            for (int i = 1; i <= columCount ; i++) {
				result.append(rsmd.getColumnName(i)+" ("+rsmd.getColumnTypeName(i)+")");
				if (i!=columCount) result.append(",");
            }
			result.append("\n");
			
			while(rs.next()){
				for(int i=1;i<= columCount;i++){	
					Object ob = rs.getObject(i);
					if (ob instanceof String){
						String str = (String) ob;
						if (str.matches(".*"+escape+".*")) str = str.replaceAll("\"","\"\"");
						if (str.matches(".*"+delimiter+".*")) result.append(escape+str+escape);
						else result.append(str);
					}
					else result.append(ob);
					if (i != columCount) result.append(",");
				}
				result.append("\n");
			}
		}
		catch(SQLException e){
			result.append(e.getMessage());
		}
		finally{
			try{
				if(rs != null) rs.close();
			}catch(SQLException e){}
			try{
				if(st != null) st.close();
			}catch(SQLException e){}
		}
		return result.toString();
   } 
%>


<%
	String SQLRequest = request.getParameter("ajaxVar");
    response.setContentType("text/html; char set=utf-8");
	
	String dataType = request.getParameter("dataType");
	JSONArray array = null;
	
	if(!SQLRequest.equals("") && SQLRequest != null){
		DBPoolService service = null;
		DBPool pool = null;
		DBConnection dbcWrap = null;
		Connection dbc = null;
		try{
	
			service = DBPoolService.getInstanse(propertiesPath);
			pool = service.getDBPool("TISC-USERS");

			dbcWrap = pool.allocConnection(request.getRemoteUser());
			dbc = dbcWrap.getSQLConnection();
			
			if (dataType.equals("json")){				
				array = exec_sql_json(dbc, SQLRequest);
				out.println(array);
			}else if (dataType.equals("text")){
				String result = exec_sql_csv(dbc, SQLRequest);
				out.println(result);
			}else out.println("ERROR");
			
			
		}catch(Exception e){
			out.println(e.getMessage());
			StackTraceElement [] t = e.getStackTrace();
            String temp ="";
            for (StackTraceElement st: t) temp+=st+"\n";
			out.println("[Exception!]: "+ temp); 
		}
		finally{
			if(dbcWrap != null) pool.freeConnection(dbcWrap);
		}
	}
	%>
 
 
