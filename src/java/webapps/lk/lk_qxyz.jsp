<%--
 ConcepTIS/QTIS project
 (c) Alex V Eustrop 2009, 2023
 see LICENSE.ConcepTIS at the project's root directory

 Purpose: member's personal office for qxyz.ru system

 History:
  2023/12/30 started from qr.jsp (221 lines)
--%>
<%@
  page contentType="text/html; charset=UTF-8"
  import="java.util.*"
  import="java.io.*"
  import="java.sql.*"
  import="org.eustrosoft.qdbp.*"
  import="org.eustrosoft.qtis.webapps.*"
  import="org.eustrosoft.qtis.webapps.QR.*"
  import="org.eustrosoft.qtis.webapps.TIS.*"
  import="org.eustrosoft.qtis.webapps.DIC.*"
  import="ru.mave.ConcepTIS.dao.*"
  import="ru.mave.ConcepTIS.dao.TIS.*"
  import="ru.mave.ConcepTIS.dao.QR.*"
  import="ru.mave.ConcepTIS.dao.SAM.*"
%>
<%!
//
// Global parameters
//
private final static String CGI_NAME = "lk_qxyz.jsp";
private final static String CGI_TITLE = "Личный Кабинет QXYZ";
private final static String JSP_VERSION = "0.2.1-20241230";

 public static class WARHMainQXYZ extends WARequestHandler
 {
    private String CGI_NAME_QR_PRINT = "/qr/print/";
    private String CGI_NAME_QR_IMG = "/qr/qrget/";
    private Long QR_MEMBERS_SID = 1099511627776L; // this sid of qxyz.ru scope which is the default to search qr routing data
    public void setCGIQRPrint(String cgi) { CGI_NAME_QR_PRINT = cgi; }
    public String getCGIQRPrint() { return (CGI_NAME_QR_PRINT); }
    public void setQRMembersSID(Long sid) { QR_MEMBERS_SID = sid; }
    public Long getQRMembersSID() { return (QR_MEMBERS_SID); }

    Exception logon_exception = null;
    User current_user = null;

    public void process()
    {
        String pSubsys = request.getParameter(wam.PARAM_SUBSYS);
        String pRequest = request.getParameter(wam.PARAM_REQUEST);
        String pTarget = request.getParameter(wam.PARAM_TARGET);
        String pZID = request.getParameter(wam.PARAM_ZID);
        Long ZID = wam.string2Long(pZID);
        WARequestHandler warh = null;
       printThisPageHtmlHeader();
        try{ db_logon(); current_user = getZSystem().getCurrentSAMUser(); } catch(Exception e){logon_exception = e; }
	topMenu();
        if(current_user == null) { warh = new WARHLogin(this); warh.process(); }
        else
        {
        if(wam.SUBSYS_MAIN.equals(pSubsys) && wam.REQUEST_LOGIN.equals(pRequest) ) {warh = new WARHLogin(this);} 
        else if(wam.SUBSYS_QR.equals(pSubsys)) { warh = new WARHQRSubsys(this); }
        else if(wam.SUBSYS_TIS.equals(pSubsys)) { warh = new WARHTISObject(this); }
        else if(wam.SUBSYS_DIC.equals(pSubsys)) warh = new WARHDictionary(this);
        //else if(wam.SUBSYS_PSQL.equals(pSubsys)) warh = new WARHPSQL(this);
        //else if(wam.SUBSYS_FS.equals(pSubsys)) { warh = new WARHFSFile(this); }
        if(warh != null) warh.process();
        else{ printBlueMenu(); }
        }
       printThisPageHtmlFooter();
       return;
    } //process()
    public void processREST() {}
    public void topMenu(){
	 was.beginHMenu();
	 was.printHMenuItem(wam.MNU_MAIN);
	 //was.printHMenuItem(wam.MNU_QR_MAIN);
         was.wln("<a href='" +CGI_NAME + "?subsys=QR' >Подсистема QR</a>");
         if(current_user != null) was.wln("<b>Пользователь:</b>" + current_user.login);
         else was.wln("<b>Пользователь:</b> <strike>null</strike>" );
         was.wln("<a href='" +CGI_NAME + "?subsys=main&request=login' >(login/logout)</a>");
			//was.printHMenuItem("<TISC>",wam.SUBSYS_TISC,null);
			//was.printHMenuItem("<SQL ad hoc>",wam.SUBSYS_PSQL,null);
	 was.closeHMenu();
   } //topMenu()
    private Vector exceptions = null;
    private void addException(Exception e)
    {
     if(exceptions == null) exceptions = new Vector();
     exceptions.add(e);
    }
    private RList loadDORList(java.sql.Connection dbc, String sql, DORecord factory) throws java.sql.SQLException
    {
     RList list = new RList();
     java.sql.ResultSet rs = null;
     try{
      rs = execSQL(dbc,sql);
      while(rs.next())
      {
       DORecord r = factory.createFromRS(rs);
       list.add(r);
      }
     }
     finally{ if(rs != null){try{rs.close();}catch(java.sql.SQLException sqle){}}}
     return(list);
    }
    public java.sql.ResultSet execSQL(java.sql.Connection dbc, String sql) throws java.sql.SQLException
    {
      return( dbc.createStatement().executeQuery(sql) );
    }
    // mk* methods
    
    // do* methods
    public boolean printExceptions()
    {
    if(exceptions == null) return(false);
    if(exceptions.size() == 0 ) return(false);
    wln("Exceptions count: " + exceptions.size());
    for(int i=0;i<exceptions.size();i++)
    {
     Exception e = (Exception)exceptions.get(i);
     this.printerrmsg(e.toString());
    }
    return(true);
    }

    // BEGIN WASkinQR code imported from old qr/index.jsp from qxyz.ru project
     public void wln(String line){was.wln(line);} //SIC! for compatibility with code below
     public void w(String line){was.w(line);} //SIC! for compatibility with code below
     public void outFlush(){out.flush();}
     public static final String QRINFO_A_TOP = "#top"; //    - верх страницы
     public static final String QRINFO_A_BOTTOM = "#bottom"; // - низ страницы
     public static final String QRINFO_A_QRCODE_BREEF = "#qrcode.breef"; //   - краткая карточка изделия (порождается всегда перед меню, но только если оно есть)
     public static final String QRINFO_A_QRCODE_INFO = "#qrcode.info"; //    - Информационная карточка изделия (порождается после меню, если оно есть)
     public static final String QRINFO_A_QRCODE_WIKI = "#qrcode.wiki"; //    - собственное описания для qr-кода (если есть) порождается после полной карточки
     public static final String QRINFO_A_RANGE_WIKI = "#range.wiki"; //     - собственное описание диапазона (если есть) пока его нет
     public static final String QRINFO_A_PRODMODEL_WIKI = "#prodmodel.wiki"; // - описание продукта-модели, после qrcode.wiki
     public static final String QRINFO_A_MEMBER_WIKI = "#member.wiki"; //    - описание участника системы, владельца информации, внизу страницы
     public static final String QRINFO_A_PRINTQR_PAGE = "#printqr.page"; //   - не
     public static final String QRINFO_A_PRODMODEL_WIKI_INFO = "#prodmodel.wiki.info"; // // Х;i++арактеристики
     public static final String QRINFO_A_PRODMODEL_WIKI_SERVICE = "#prodmodel.wiki.service"; // //  Обслуживание
     public static final String QRINFO_A_PRODMODEL_WIKI_REPAIR = "#prodmodel.wiki.repair"; // Ремонт
     public void printAnchor(String name)
     {
      w("<a name='"); //
      w(name); //
      w("'>"); //
      wln("</a>");
     }
     private void printQRSearchForm(String q, String p) {
        if(q==null) q ="101A117";
        if(q==null) q ="100D001";
        if(p==null) p="";
        wln("<form method='GET' name='search' action='" + CGI_NAME +"'>");
        wln("q:");
        w("<input type='text' size='8' name='" + wam.PARAM_Q + "' value='");
        w(wam.obj2value(q));
        wln("'/>");
        wln("p:");
        w("<input type='text' size='8' name='" + wam.PARAM_P + "' value='");
        w(wam.obj2value(p));
        wln("' />");
/*
*/
        wln("<input type='submit' title='Запросить информацию по глобальному QR-коду (q), с опциональным PIN (p)' value='Lookup!'/>");
        wln("</form>");
     }
     private void printBlueMenu()
     {
     wln("<div class='info'>");
     wln("    <a href='" + CGI_NAME + "?subsys=QR&request=list&target=QR.Q' class='info__btn'>Карточки QR</a>");
     wln("    <a href='" + CGI_NAME + "?subsys=QR&request=list&target=QR.P' class='info__btn'>Продукты/модели</a>");
     wln("    <a href='" + CGI_NAME + "?subsys=QR&request=list&target=QR.M' class='info__btn'>Организации</a>");
     wln("    <a href='" + CGI_NAME + "?subsys=QR&request=list&target=QR.C' class='info__btn'>Карточки продаж</a>");
     wln("</div>");
     }
    // END WASkinQR code imported from old qr/index.jsp from qxyz.ru project
    // BEGIN header/footer section imported from old qr/index.jsp from qxyz.ru project
     private Exception error_while_header=null; // чтобы посмотреть в конце, если не удалось, то footer делаем тоже локальный, соотв. header
     private long header_enter_time = 0;
     public void printThisPageHtmlHeader(){
      error_while_header=null;
      header_enter_time = System.currentTimeMillis();
     if(response == null) {wln("null"); return;}
     try{
      request.setAttribute("SSIP_TITLE",CGI_TITLE);
      request.getRequestDispatcher("/header.jsp").include(request, response);
     }
     catch(ServletException se) {error_while_header=se;}
     catch(IOException ioe) {error_while_header=ioe;}
     //catch(NullPointerException npe) {error_while_header=npe;}
     if(error_while_header != null)
     {
      wln("<html>");
      wln(" <head>");
      wln("  <meta charset='UTF-8'>");
      wln("  <title>");
      wln("   " + CGI_TITLE);
      wln("  </title>");
      wln(" </head>");
      wln("<body>");
     }
      wln("<div class='section'>");
     }
     public void printThisPageHtmlHeaderMenu(){}
     public void printThisPageHtmlFooter()
     {
      wln("</div>"); // close <div class='section'>");
      // timing/version BEGIN
      wln("<hr>");
      wln("<i>timing : " + ((System.currentTimeMillis() - header_enter_time)) + " ms</i>");
      wln("<br>");
      wln("<i>ver : "+ JSP_VERSION + " QDBPool ver: " + QDBPool.getVer() + " QDBPOOL_NAME = " + getQDBPoolName() + " </i>");
      // timing/version END
     try{
wln("<!--footer-->");
outFlush();
      request.setAttribute("SSIP_TITLE",CGI_TITLE);
      request.getRequestDispatcher("/footer.jsp").include(request, response);
wln("<!--footer-->");
      return;
     }
     catch(ServletException se) {error_while_header=se;}
     catch(IOException ioe) {error_while_header=ioe;}
       wln("</div>");
       wln("<!-- Привет this is just for UTF-8 testing (must be russian word \"Privet\") -->");
       wln("</body></html>");
     }
    // END header/footer section imported from old qr/index.jsp from qxyz.ru project
    // constructors
    public WARHMainQXYZ(HttpServletRequest request,HttpServletResponse response,java.io.PrintWriter out){
        super(request,response,out);
    }

    public WARHMainQXYZ(WARequestHandler warh) { super(warh); }
 } // class WARHMainQXYZ

%><%

 // 0. get configuration from web-application parameters (WEB-INF/web.xml)
 String QDBPOOL_NAME = this.getServletContext().getInitParameter(WARequestHandler.PARAM_QDBPOOL_NAME);
 String QDBPOOL_URL = this.getServletContext().getInitParameter(WARequestHandler.PARAM_QDBPOOL_URL);
 String QDBPOOL_JDBC_CLASS = this.getServletContext().getInitParameter(WARequestHandler.PARAM_QDBPOOL_JDBC_CLASS);
 long enter_time = System.currentTimeMillis();
 //WARHQRSubsys warh = new WARHQRSubsys(request,response,new java.io.PrintWriter(out));
 WARHMainQXYZ warh = new WARHMainQXYZ(request,response,new java.io.PrintWriter(out));
 warh.setQDBPool(QDBPOOL_NAME,QDBPOOL_URL,QDBPOOL_JDBC_CLASS);
 warh.cache_expire_hints(); // some hints for old and buggy browsers like NN4.x
 //String DBSERVER_URL = this.getServletContext().getInitParameter("DBSERVER_URL");
 //warh.setJDBCURL(DBSERVER_URL);
 warh.setCGI(CGI_NAME);
 //warh.setTopMenu(true);
 Object mutex = warh.getQDBPSession();
 synchronized(mutex)
 {
   warh.process();
 }
%>
