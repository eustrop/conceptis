<%--
 ConcepTIS project
 (c) Alex V Eustrop 2009
 see LICENSE at the project's root directory

 $Id$

 Purpose: This JSP is some kind of nutshell for ru.mave.ConcepTIS.webapps.* classes
	  which processes user's http requests and construct html UI documents
	  just without bounding <html> and <body> tags. Those documents
	  will be embedded into output of this JSP.

	  NOTE: The webapps.* classes above uses the same Postgres "Trust
	  authentication feature" based technique for users authentication
	  such as psql.jsp

 History:
  2009/11/28 started from psql.jsp
--%>
<%@
  page contentType="text/html; charset=UTF-8"
  import="java.util.*"
  import="java.io.*"
  import="java.sql.*"
  import="org.eustrosoft.qtis.webapps.*"
%>
<%!
//
// Global parameters
//
private final static String CGI_NAME = "qtis.jsp";
private final static String CGI_TITLE = "ConcepTIS/Common UI";
//private final static String DBSERVER_URL = "jdbc:postgresql:conceptisdb";
private final static String JSP_VERSION = "$testr$";

private final static String SZ_EMPTY = "";
private final static String SZ_NULL = "<<NULL>>";
private final static String SZ_UNKNOWN = "<<UNKNOWN>>";

%>
<%

 // 0. get configuration from web-application parameters (WEB-INF/web.xml)
 String QDBPOOL_NAME = this.getServletContext().getInitParameter(WARequestHandler.PARAM_QDBPOOL_NAME);
 String QDBPOOL_URL = this.getServletContext().getInitParameter(WARequestHandler.PARAM_QDBPOOL_URL);
 String QDBPOOL_JDBC_CLASS = this.getServletContext().getInitParameter(WARequestHandler.PARAM_QDBPOOL_JDBC_CLASS);
 long enter_time = System.currentTimeMillis();
 WARHMain warh = new WARHMain(request,response,new java.io.PrintWriter(out));
 warh.setQDBPool(QDBPOOL_NAME,QDBPOOL_URL,QDBPOOL_JDBC_CLASS);
 warh.cache_expire_hints(); // some hints for old and buggy browsers like NN4.x
 //String DBSERVER_URL = this.getServletContext().getInitParameter("DBSERVER_URL");
 //warh.setJDBCURL(DBSERVER_URL);
 warh.setCGI(CGI_NAME);
 warh.setTopMenu(true);

%>
<html>
 <head>
  <title><%= CGI_TITLE %></title>
 </head>
<body>
  <%
  Object mutex = warh.getQDBPSession();
  synchronized(mutex)
  {
   warh.process();
  }
  %>
 <hr>
  <i>timing : <%= ((System.currentTimeMillis() - enter_time) + " ms") %></i>
  <i>ts : <%= ((System.currentTimeMillis()) + "" + QDBPOOL_NAME) %></i>
 <br>
  <i><%= JSP_VERSION %></i><br>
  <small>your web-server is <%= application.getServerInfo() %></small>
  <!-- Привет this is just for UTF-8 testing (must be russian word "Privet") -->
</body>
</html>
