/*
 ConcepTIS project/QTIS project
 (c) Alex V Eustrop & eustrosoft.org 2009,2023
 LICENSE (this file) : BALES, BSD, MIT on your choice (see bales.eustrosoft.org)
 also see LICENSE at the project's root directory for the whole project license

 Purpose: pool of sessions and connections with TIS/SQL-compatible JDBC source
          ( PostgreSQL 13 tested but should be suitable for any DBMS)
 History (psql.jsp & dbpool.jsp):
  2023/04/07 dbpool.jsp started from psql.jsp
  2023/04/25 org.eustrosoft.qdbp package started from 330 lines of dbpool.jsp
*/
package org.eustrosoft.qdbp;
import java.util.*;
import java.sql.*;

// QDBPSession.java - это класс-контейнер пользовательской сессии web or mobile
public class QDBPSession
{
 public static final int DEFAULT_COOKIE_MAX_AGE = 60*60*8;//*24; // sec
 private Long session_id;
 private String dbpool_name;
 private String secret_cookie;
 private boolean is_secret_cookie_renewable = true;
 private long creation_ts; // creation time stamp
 private int session_maxage = DEFAULT_COOKIE_MAX_AGE;
 private long cookie_expare_ts; // time of session cookie exparation
 private long lastusage_ts; // last usage time stamp
 public void renewLastUsageTS(){lastusage_ts=System.currentTimeMillis();}
 public long getLastUsage(){return(lastusage_ts);}
 //
 private QDBPConnection con;
 private String login;
 private String password;
 private boolean is_session = false;
 private boolean is_exclusive = true;
 private boolean is_pub = false;
 //
 private Long qtis_uid=null;
 private Long qtis_usi=null;
 public Long getUID(){return(qtis_uid);}
 public Long getUSI(){return(qtis_usi);}
 //
 //public void use(){} //SIC! not implemented (lock to exclusive use)
 //public void free(){} //SIC! not implemented
 public void freeConnection() { if(con == null) return; con.free(); con=null; }
 public boolean isExclusiveConnection(){return(is_exclusive);}// SIC! only exclusive now
 public void setExclusiveConnection(QDBPConnection con){this.con=con; con.setExclusive(); is_session=true; }
 public void setSharedPubConnection(QDBPConnection con){this.con=con; is_session=true; is_exclusive=false; is_pub = true; }
 public void setSharedConnection(QDBPConnection con)
 {
  this.con=con;
  if(this.con!=null)
  {
   qtis_uid = con.getUID();
   qtis_usi = con.getUSI();
   is_session=true;
   is_exclusive=false;
  }
 }
 public QDBPConnection getConnection() throws java.sql.SQLException, QDBPException
 {
  renewLastUsageTS();
  if(con != null) return(con); if(!is_session) return(null);
  QDBPool dbp = QDBPool.get(dbpool_name);
  if(dbp == null) throw new QDBPException("No DBPOOL '" +dbpool_name+ "' found");
  if(is_pub)
  {
    throw( new QDBPException("SIC! pub connection is dead") );
  }
  if(is_exclusive)
  {
   con=dbp.createExclusiveConnection(session_id,login,password);
  }
  else
  {
   con=dbp.createSharedConnection(session_id);
   try{ con.bindUser2Session(getUID(),getUSI(),secret_cookie); }
   catch(java.sql.SQLException sqle){dbp.freeSharedConnection(con); throw(sqle); }
   catch(QDBPException qdbpe){dbp.freeSharedConnection(con); throw(qdbpe); }
  }
  return(con); //SIC! no pool-obtained connections implemented yet
 }
 public String getLogin(){return(login);}
 public void setLoginPassword(String login, String password){ this.login = login; this.password = password; }
 public Long getID(){return(session_id);}
 public void setIDOnce(Long new_session_id){if(session_id==null)session_id = new_session_id;} //!SIC
 public String getSessionSecretCookie(){
   if(isExclusiveConnection()) return(getID()+":"+getSecretCookie());
   return(getUID()+":" + getUSI() + ":" +getSecretCookie());
 }
 public String getSecretCookie(){if(secret_cookie!=null)return(secret_cookie); return(renewSecretCookie()); } //!SIC may be must be getSecretCookieOnce()
 public void setSecretCookie(String secretecookie){secret_cookie=secretecookie; is_secret_cookie_renewable = false;} 
 public int getSessionCookieMaxAge(){return(session_maxage);}
 public int getSessionCookieExpire(){long e=(((cookie_expare_ts-System.currentTimeMillis())/1000));if(e<0)return(0);return((int)e);}
 public boolean isSessionRenewReady(){
  if(!is_secret_cookie_renewable) return(false);
  if(getSessionCookieExpire()<session_maxage/2) return(true);
  return(false);
 }
 public void renewSession(){renewSecretCookie();}
 private String renewSecretCookie()
 {
  if(!is_secret_cookie_renewable) return(secret_cookie);
  //secret_cookie = (new Integer(this.hashCode())).toString(); // first insecure version
  java.security.SecureRandom sr = new java.security.SecureRandom();
  byte[] b = new byte[32];
  sr.nextBytes(b);
  secret_cookie = java.util.Base64.getEncoder().withoutPadding().encodeToString(b);
  cookie_expare_ts=System.currentTimeMillis() + session_maxage*1000;
  return(secret_cookie);
 } //SIC! no DB updateed yet
 public boolean checkSecretCookie(String cookie){if(secret_cookie==null)return(false);return(secret_cookie.equals(cookie));}
 public java.sql.Connection getSQLConnection() throws java.sql.SQLException {QDBPConnection con=getConnection();if(con==null)return(null);return(con.get());}
 public boolean ping()
 {
  QDBPConnection con=null;
  try{con=getConnection();}catch(java.sql.SQLException sqle){}catch(QDBPException qdbpe){}
  if(con==null) return(false);
  return(con.ping());
 }
 public void logout() throws java.sql.SQLException
 {
   login=null; password=null;
   if(con != null) con.close(); //SIC! avoid next lines if exception
   con=null; is_session=false;
 } //logout
 //constructors
 public QDBPSession(String dbpool_name,Long id){this.dbpool_name=dbpool_name;this.session_id = id; creation_ts=System.currentTimeMillis();renewLastUsageTS();}
} //QDBPSession
