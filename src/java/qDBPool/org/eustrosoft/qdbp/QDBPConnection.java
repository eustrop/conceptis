/*
 ConcepTIS project/QTIS project
 (c) Alex V Eustrop & eustrosoft.org 2009,2023
 LICENSE (this file) : BALES, BSD, MIT on your choice (see bales.eustrosoft.org)
 also see LICENSE at the project's root directory for the whole project license

 Purpose: pool of sessions and connections with TIS/SQL-compatible JDBC source
          ( PostgreSQL 13 tested but should be suitable for any DBMS)
 History (psql.jsp & dbpool.jsp):
  2023/04/07 dbpool.jsp started from psql.jsp
  2023/04/25 org.eustrosoft.qdbp package started from 330 lines of dbpool.jsp
*/
package org.eustrosoft.qdbp;
import java.util.*;
import java.sql.*;

// QDBPConnection.java - это класс-контейнер для хранения-передачи JDBC соединения с БД
public class QDBPConnection
{
 public static String SQL_TIS_LOGIN_WITH_COOKIE= "select SAM.bindUser2Session(?,?,?);"; // uid,usi, secretecookie
 public static String SQL_TIS_LOGIN = "select * from SAM.logonMD5(?,?,?,?);"; //login,password (md5 hash), cookie, devicefrom
 public static String SQL_PG_PING = "select session_user;";
 private String dbpool_name;
 private Long session_id=null;
 private java.sql.Connection dbc;
 private boolean is_exclusive=false;
 //
 private Long qtis_uid=null;
 private Long qtis_usi=null;
 public Long getUID(){return(qtis_uid);}
 public Long getUSI(){return(qtis_usi);}
 public boolean logon(String login, String password, String secretecookie, String devicefrom)
  throws java.sql.SQLException, QDBPException
 {
  if(dbc==null)return(false);
    java.sql.PreparedStatement ps = null;
    java.sql.ResultSet rs = null;
    // ConcepTIS SAM.execstatus fields
    Integer errnum = null; // int, -- error number. 0 or less for success (see dic.ErrorMsg)
    String errcode = null; // char(16), -- symbolic code for errnum
    String errdesc = null; // varchar(255), -- description of error
    try {
      ps = dbc.prepareStatement(SQL_TIS_LOGIN);
      setPSString(ps,1,login);
      setPSString(ps,2,QDBPWDigest.digest(password));
      setPSString(ps,3,secretecookie);
      setPSString(ps,4,devicefrom);
      rs = ps.executeQuery();
      if(rs.next())
      {
       qtis_uid = getRSLong(rs,1);
       qtis_usi = getRSLong(rs,2);
       // from zDAO ExecStatus class:
        //ZID = getRSLong(rs,1);
        //ZVER = getRSLong(rs,2);
        errnum = getRSInt(rs,3);
        errcode = getRSString(rs,4);
        errdesc = getRSString(rs,5);
      }
    }
    //catch(java.sql.SQLException sqle){return(false);}
    finally{
     try{if(rs != null) rs.close();}catch(SQLException e){}
     try{if(ps != null) ps.close();}catch(SQLException e){}
    }
  if(errnum != null)
  {
   if(errnum.intValue() > 0)
   {
     throw new QDBPException("Login failed, user : " + login + " ( " + errcode+":"+errdesc + ")");
   }
  }
  if(qtis_uid == null) throw new QDBPException("Login failed, user : " + login);
  return(true);
 }
 public boolean bindUser2Session(Long uid, Long usi, String secretecookie)
  throws java.sql.SQLException, QDBPException
 {
  if(dbc==null) return(false);
  if(uid == null) return(false);
    java.sql.PreparedStatement ps = null;
    java.sql.ResultSet rs = null;
    // ConcepTIS SAM.execstatus fields
    try {
      ps = dbc.prepareStatement(SQL_TIS_LOGIN_WITH_COOKIE);
      setPSLong(ps,1,uid);
      setPSLong(ps,2,usi);
      setPSString(ps,3,secretecookie);
      rs = ps.executeQuery();
      if(rs.next())
      {
       qtis_uid = getRSLong(rs,1);
       qtis_usi = usi;
      }
    }
    //catch(java.sql.SQLException sqle){return(false);}
    finally{
     try{if(rs != null) rs.close();}catch(SQLException e){}
     try{if(ps != null) ps.close();}catch(SQLException e){}
    }
  if(!uid.equals(qtis_uid)) throw new QDBPException("Login failed, user : " + uid + " usi: " + usi);
  return(true);
  }
 //
 public java.sql.Connection get(){return(dbc);}
 public Long getSessionID(){return(session_id);}
 public void bind(QDBPSession s){if(s!=null && !is_exclusive){session_id=s.getID();}} //!SIC add check dbpool_name
 public void unbind(QDBPSession s){if(!is_exclusive){session_id=null;}}
 public void setExclusive(){is_exclusive=true;}
 public boolean isExclusive(){return(is_exclusive);}
 public synchronized void free()
 {
  if(dbc == null) return;
  if(isExclusive()) { try{ dbc.close();} catch(SQLException e){} } //SIC
  else { try{ dbc.close();} catch(SQLException e){} }
  // else { QDBPool.returnConnction(con); } // SIC! not implemented yet
 } // free()
 public boolean ping()
 {
  if(dbc==null)return(false);
    java.sql.Statement st = null;
    java.sql.ResultSet rs = null;
    try { st = dbc.createStatement(); rs = st.executeQuery(SQL_PG_PING); }
    catch(java.sql.SQLException sqle){return(false);}
    finally{
     try{if(rs != null) rs.close();}catch(SQLException e){}
     try{if(st != null) st.close();}catch(SQLException e){}
    }
  return(true);
 } //!SIC
 public void close()
  throws java.sql.SQLException
 {
   if(dbc != null){ dbc.close(); dbc=null;}
 }
 // destructor
 protected void finalize() throws Throwable
 {
  try{ if(dbc != null){ dbc.close(); dbc=null;} } catch(Exception e){}
  super.finalize();
 } //finalize()
 // code imported from zDAO/Record class

 public static Integer getRSInt(java.sql.ResultSet rs, int column)
  throws java.sql.SQLException
 { Integer v = new Integer(rs.getInt(column)); if(rs.wasNull()) v=null; return(v); }

 public static Long getRSLong(java.sql.ResultSet rs, int column)
  throws java.sql.SQLException
 { Long v = new Long(rs.getLong(column)); if(rs.wasNull()) v=null; return(v); }

 public static void setPSLong(java.sql.PreparedStatement ps, int column, Long v)
  throws java.sql.SQLException
 {if(v==null){ps.setNull(column,Types.BIGINT);}else{ps.setLong(column, v.longValue());}}

 public static String getRSString(java.sql.ResultSet rs, int column)
  throws java.sql.SQLException
 { String v = rs.getString(column); if(rs.wasNull()) v=null; return(v); }

 public static void setPSString(java.sql.PreparedStatement ps, int column, String v)
  throws java.sql.SQLException
 {if(v==null){ps.setNull(column,Types.VARCHAR);}else{ps.setString(column, v);}}

 // constructor
 public QDBPConnection(String dbpool_name,Long session_id,java.sql.Connection dbc){this.dbpool_name=dbpool_name;this.session_id=session_id;this.dbc=dbc;}
} //QDBPConnection
