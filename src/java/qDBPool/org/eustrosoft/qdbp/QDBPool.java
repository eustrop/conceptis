/*
 ConcepTIS project/QTIS project
 (c) Alex V Eustrop & eustrosoft.org 2009,2023
 LICENSE (this file) : BALES, BSD, MIT on your choice (see bales.eustrosoft.org)
 also see LICENSE at the project's root directory for the whole project license

 Purpose: pool of sessions and connections with TIS/SQL-compatible JDBC source
          ( PostgreSQL 13 tested but should be suitable for any DBMS)
 History (psql.jsp & dbpool.jsp):
  2023/04/07 dbpool.jsp started from psql.jsp
  2023/04/25 org.eustrosoft.qdbp package started from 330 lines of dbpool.jsp
*/
package org.eustrosoft.qdbp;
import java.util.*;
import java.sql.*;
import java.io.*;

// QDBPool.java - это пул соединений с БД и фабрика порождения объектов иных классов
public class QDBPool
{
 // static section
 private static Hashtable ht_pools; // storage for pools;
 // static version fields & methods
 private static int ver_major = 0;
 private static int ver_minor = 11;
 private static int ver_build = 2023122601;
 private static String ver_status = "ALPHA10";
 public static int getVerMajor(){return(ver_major);}
 public static int getVerMinor(){return(ver_minor);}
 public static int getVerBuild(){return(ver_build);}
 public static String getVerStatus(){return(ver_status);}
 public static String getVer(){return(""+ver_major+"."+ver_minor+"-"+ver_status+"-b"+ver_build);}
 // static pool methods
 public static synchronized void add(QDBPool p){add(p,p.name);}
 public static synchronized void add(QDBPool p,String name)
 {
  if(name == null) return;
  if(ht_pools == null)ht_pools = new Hashtable();
  Object check_pool = ht_pools.get(name);
  if(check_pool == null) { ht_pools.put(name,p); }
  else{throw new QDBPException("pool with name '" + name + "' exists");}
 }
 public static synchronized QDBPool get(String name){if(ht_pools==null || name == null)return(null);return((QDBPool)ht_pools.get(name));}
 public static synchronized void destroyAll(){ht_pools=null;} //!SIC
  /** create driver class by its classname (jdbc_driver parameter)
   * and register it via DriverManager.registerDriver().
   * @param jdbc_driver - "org.postgresql.Driver" for postgres,
   * "oracle.jdbc.driver.OracleDriver" for oracle, etc.
   */ 
 public static void register_jdbc_driver(String jdbc_driver)
	throws Exception
  {
   java.sql.Driver d;
   Class dc;
   // get "Class" object for driver's class
   try
   {
    dc = Class.forName(jdbc_driver);
    d = (java.sql.Driver)dc.newInstance();
   }
   catch(ClassNotFoundException e) {
     throw new Exception("register_jdbc_driver:" + "unable to get Class for "
     + jdbc_driver + ":" + e); }
   catch(Exception e) {
     throw new Exception("register_jdbc_driver: unable to get driver " 
     + jdbc_driver + " : " + e); }
   // register driver
   try { DriverManager.registerDriver(d); }
   catch(SQLException e) { throw new Exception(
   "register_jdbc_driver: unable to register driver "+jdbc_driver+" : "+e);}
  } // register_jdbc_driver()
 // instance section
 private String name;
 private String jdbc_url;
 private String jdbc_driver_class;
 // fromfile configuration if so:
 private String pool_config_file = null;
 private String pool_user = null;
 private String pool_user_password = null;
 private String pub_user = null;
 private String pub_user_password = null;
 private String reject_direct_users = null;
 private String allow_direct_users = null;
 private boolean allow_direct_login = true;
 private boolean allow_qtis_login = true;
 private int min_pool_sessions = 0;
 private int max_pool_sessions = 2;
 // end fromfile configuration
 private long seq_sessions = 1;
 private Hashtable ht_sessions; // storage for all sessions
 private Hashtable ht_sessions_qtis; // storage for sessions abtained with logonQTIS
 private Hashtable ht_connections; // storage for sessions
 private long connection_timeout_ms = 600000; // 10m
 private long last_gc_run_ts = 0;

 public String getName(){return(name);}
 public String getJDBCUrl(){return(jdbc_url);}
 // fromfile
 public String getPoolConfigFile(){return(pool_config_file);}
 public String getPoolUser(){return(pool_user);}
 public String getPubUser(){return(pub_user);}
 public String getAllowedDirectUsers(){return(allow_direct_users);}
 public String getRejectedDirectUsers(){return(reject_direct_users);}
 public boolean isDirectLoginAllowed(){return(allow_direct_login);}
 public boolean isQTISLoginAllowed(){return(allow_qtis_login);}
 public int getMinSessions(){return(min_pool_sessions);}
 public int getMaxSessions(){return(max_pool_sessions);}
 //
 public synchronized QDBPConnection getConnection(String name){if(ht_connections==null)return(null);return((QDBPConnection)ht_connections.get(name));}
 public Vector<QDBPConnection> listConnections(){ if(ht_connections==null)return(new Vector<QDBPConnection>()); Vector<QDBPConnection> l=new Vector<QDBPConnection>(ht_connections.values()); return(l); }
 public synchronized QDBPSession getSession(Long uid, Long usi, String session_cookie)
 {
  QDBPSession s = null;
  if(ht_sessions_qtis == null) return(null);
  Hashtable ht_uid = (Hashtable)ht_sessions_qtis.get(uid);
  if(ht_uid == null) return(null);
  s = (QDBPSession)ht_uid.get(usi);
  if(s != null) if(s.checkSecretCookie(session_cookie)) return(s);
  return(null);
 }
 public synchronized QDBPSession getSession(Long id, String session_cookie){QDBPSession s = getSession(id); if(s != null) if(s.checkSecretCookie(session_cookie)) return(s); return(null);}
 public synchronized QDBPSession getSession(Long id){if(id==null)return null;if(ht_sessions==null)return(null);return((QDBPSession)ht_sessions.get(id));}
 public synchronized void removeSession(Long id)
 {
  if(id==null)return;if(ht_sessions==null)return;
  QDBPSession s = (QDBPSession)ht_sessions.get(id);
  ht_sessions.remove(id);
  if(s==null) return;
  s.freeConnection();
  Long uid = s.getUID();
  Long usi = s.getUSI();
  if(uid == null) return;
  if(usi == null) return;
  if(ht_sessions_qtis == null) return;
  Hashtable ht_uid = (Hashtable)ht_sessions_qtis.get(uid);
  if(ht_uid == null) return;
  ht_uid.remove(usi);
 }
 public Vector<QDBPSession> listSessions() { if(ht_sessions==null)return(new Vector<QDBPSession>()); Vector<QDBPSession> l=new Vector<QDBPSession>(ht_sessions.values()); return(l); }
 public synchronized void gcConnections() // garbage collection of connections not used for connection_timeout_ms
 {
  Vector<QDBPSession> v = listSessions();
  long current_time= System.currentTimeMillis();
  if((current_time-last_gc_run_ts)< connection_timeout_ms/10) return; //SIC! all this method need more testing
  last_gc_run_ts = current_time;
    for(int i=0;i<v.size();i++){
     QDBPSession s = v.get(i);
     if(s.isExclusiveConnection())
     {
      if((current_time-s.getLastUsage()) > connection_timeout_ms) s.freeConnection();
      if(s.getSessionCookieExpire()==0) removeSession(s.getID());
     }
     else
     {
      if((current_time-s.getLastUsage()) > connection_timeout_ms/10) {s.freeConnection(); removeSession(s.getID()); }
     }
    }
 } // gcConnections()
 public synchronized QDBPSession createSession(){
  QDBPSession s = new QDBPSession(name,null);
  return s;
 }
 public synchronized QDBPSession logon(String secret_cookie_value)
 {
 String session_cookie_secret=null;
 Long session_cookie_id=null;
 Long uid=null;
 Long usi=null;
 gcConnections();
 //2.2. extract session id from secret_cookie_value if present
 if(secret_cookie_value != null)
 {
  String[] scv_parts = secret_cookie_value.split(":",3);
  if(scv_parts.length == 2) // local stored session
  {
  try{ session_cookie_id = Long.valueOf(scv_parts[0]); } catch (NumberFormatException nfe){} //str2Long(..);
  if(scv_parts.length>1) session_cookie_secret=scv_parts[1];
  //2.3. find session if present
  return(this.getSession(session_cookie_id,session_cookie_secret));
  }
  else if(scv_parts.length == 3) // QTIS session
  {
   try{ uid = Long.valueOf(scv_parts[0]); } catch (NumberFormatException nfe){} //str2Long(..);
   try{ usi = Long.valueOf(scv_parts[1]); } catch (NumberFormatException nfe){} //str2Long(..);
   session_cookie_secret=scv_parts[2];
   QDBPSession s = this.getSession(uid,usi,session_cookie_secret);
   if(s != null) return(s);
   // else - try to create session and bind it using session_cookie_secret
   try{
   s = logonQTIS(uid,usi,session_cookie_secret);
   } catch(Exception e){} //SIC!
   //} catch(Exception e){throw new QDBPException(e.toString());} //SIC!
   return(s);
  }
 }
  return(null);
 } // logon(String secret_cookie_value)

 public synchronized QDBPSession logon(String login, String password)
   throws java.sql.SQLException
{
 if(login == null) login="";
 login = login.trim();
 if(login.indexOf("@")>=0) // logon using shared QTIS pool user
 {
  if(login.startsWith("@")) login = login.substring(1);
  if(login.endsWith("@")) login = login.substring(0,login.length()-1);
  return logonQTIS(login, password);
 }
 //else
 return logonSQL(login, password);
}
 public synchronized QDBPSession logon() throws java.sql.SQLException
{
 return(logonPub());
} //logon()

 private QDBPSession pub_session = null;
 public synchronized QDBPSession logonPub() throws java.sql.SQLException
{
 if(pub_session != null){
  if(pub_session.ping()) return(pub_session);
  pub_session.freeConnection(); removeSession(pub_session.getID());
  pub_session=null;
 }
 QDBPSession s = createSession();
 s.setIDOnce(new Long(seq_sessions++));
 QDBPConnection con = createPubSharedConnection(s.getID());
 s.setSharedPubConnection(con);
 addSession(s);
 pub_session = s;
 return(pub_session);
} //logon()

 public synchronized QDBPSession logonQTIS(String login, String password)
  throws java.sql.SQLException, QDBPException
 {
 gcConnections();
  QDBPSession s = createSession();
  s.setIDOnce(new Long(seq_sessions++));
  QDBPConnection con = createSharedConnection(s.getID());
  try{
  con.logon(login,password,s.getSecretCookie(),getVer());
  }
  catch(java.sql.SQLException sqle){freeSharedConnection(con); throw(sqle); }
  catch(QDBPException qdbpe){freeSharedConnection(con); throw(qdbpe); }
  //s.setLoginPassword(login,password);
  s.setSharedConnection(con);
  addSession(s);
  return s;
 }
 public synchronized QDBPSession logonQTIS(Long uid, Long usi, String session_secrete_cookie)
  throws java.sql.SQLException, QDBPException
 {
 gcConnections();
  QDBPSession s = createSession();
  s.setIDOnce(new Long(seq_sessions++));
  QDBPConnection con = createSharedConnection(s.getID());
  try{
  con.bindUser2Session(uid,usi,session_secrete_cookie);
  }
  catch(java.sql.SQLException sqle){freeSharedConnection(con); throw(sqle); }
  catch(QDBPException qdbpe){freeSharedConnection(con); throw(qdbpe); }
  //s.setLoginPassword(login,password);
  s.setSharedConnection(con);
  s.setSecretCookie(session_secrete_cookie);
  addSession(s);
  return s;
 }
 public synchronized QDBPSession logonSQL(String login, String password)
   throws java.sql.SQLException
 {
 gcConnections();
  QDBPSession s = createSession();
  s.setIDOnce(new Long(seq_sessions++));
  QDBPConnection con = createExclusiveConnection(s.getID(), login,password);
  s.setLoginPassword(login,password);
  s.setExclusiveConnection(con);
  addSession(s);
  return s;
 }
 public QDBPConnection createExclusiveConnection(Long session_id,String login,String password)
  throws java.sql.SQLException,QDBPException
 {
 gcConnections();
  QDBPConnection con;
  String jdbc_url = getJDBCUrl();
  // open JDBC connection
  java.sql.Connection  dbc=DriverManager.getConnection(jdbc_url,login,password);
  con = new QDBPConnection(name,session_id,dbc);
  con.setExclusive();
  return(con);
 } //createExclusiveConnection
 public QDBPConnection createPubSharedConnection(Long session_id)
  throws java.sql.SQLException,QDBPException
 {
 gcConnections();
  QDBPConnection con;
  String jdbc_url = getJDBCUrl();
  // open JDBC connection
  java.sql.Connection  dbc=DriverManager.getConnection(jdbc_url,pub_user,pub_user_password);
  con = new QDBPConnection(name,session_id,dbc);
  //con.setExclusive();
  return(con);
 }
 public QDBPConnection createSharedConnection(Long session_id)
  throws java.sql.SQLException,QDBPException
 {
 gcConnections();
  QDBPConnection con;
  String jdbc_url = getJDBCUrl();
  // open JDBC connection
  java.sql.Connection  dbc=DriverManager.getConnection(jdbc_url,pool_user,pool_user_password);
  con = new QDBPConnection(name,session_id,dbc);
  //con.setExclusive();
  return(con);
 }
 public QDBPConnection getSharedConnection(QDBPSession s){return(null);}
 public void freeSharedConnection(QDBPConnection c){
  if(c == null) return;
  c.free();
  //throw(new RuntimeException("not implemented (freeSharedConnection())"));
 }
 public synchronized void addSession(QDBPSession s){addSession(s,s.getID());}
 public synchronized void addSession(QDBPSession s,Long id)
 {
  if(ht_sessions == null)ht_sessions = new Hashtable();
  Object check_session = ht_sessions.get(id);
  if(check_session == null) { ht_sessions.put(id,s); }
  else{throw new QDBPException("session with id '" + id + "' exists");}
  if(s.getUID() != null && s.getUSI() != null) // QTIS session
  {
   if(ht_sessions_qtis == null)ht_sessions_qtis = new Hashtable();
   Hashtable ht_uid = (Hashtable)ht_sessions_qtis.get(s.getUID());
   if(ht_uid == null) {ht_uid = new Hashtable(); ht_sessions_qtis.put(s.getUID(),ht_uid); }
   Object session_qtis = ht_uid.get(s.getUSI());
   if(session_qtis == null) { ht_uid.put(s.getUSI(),s); }
   else{throw new QDBPException("session with uid:usi = '" + s.getUID() + ":" + s.getUSI() + "' exists");}
  }
 }
 // configuration parameters
 
 public static final String CONF_FROMFILE = "fromfile:";
 public static final String CONF_URL = "URL=";
 public static final String CONF_POOLUSER = "POOLUSER=";
 public static final String CONF_POOLUSER_PASSWORD = "POOLUSER_PASSWORD=";
 public static final String CONF_PUBUSER = "PUBUSER=";
 public static final String CONF_PUBUSER_PASSWORD = "PUBUSER_PASSWORD=";
 public static final String CONF_QTIS_LOGIN = "QTIS_LOGIN=";
 public static final String CONF_DIRECT_LOGIN = "DIRECT_LOGIN=";
 public static final String CONF_REJECT_DIRECT_USERS = "REJECT_DIRECT_USERS=";
 public static final String CONF_ALLOW_DIRECT_USERS = "ALLOW_DIRECT_USERS=";
 public static final String CONF_MIN_SESSIONS = "MIN_SESSIONS=";
 public static final String CONF_MAX_SESSIONS = "MAX_SESSIONS=";
 // default values
 public static final String DEFAULT_REJECT_DIRECT_USERS = "postgres,pgsql,tisc";
 public static final String DEFAULT_ALLOW_DIRECT_USERS = "qtisadmin,qtisoperator,qtisuser1,qtisuser2,qtisuser3,qtis*";
/**
 * example of file:
 * URL=jdbc:postgresql://qxyz-pg-server/qxyzdb
 * POOLUSER=tiscpool1
 * POOLUSER_PASSWORD=password
 * PUBUSER=tiscpool1
 * PUBUSER_PASSWORD=password
 * # commented parameters below is not supported yet
 * #QTIS_LOGIN=yes
 * #DIRECT_LOGIN=yes
 * #REJECT_DIRECT_USERS=postgres,pgsql,tisc
 * #ALLOW_DIRECT_USERS=qtisadmin,qtisoperator,qtisuser1,qtisuser,qtisuser3,qtis*
 * #MIN_SESSIONS=0
 * #MAX_SESSIONS=10
 */
 private void load_config_from_file(String url)
 {
  this.jdbc_url ="";
  if(url == null) return;
  if(!url.startsWith(CONF_FROMFILE)) {this.jdbc_url = url; return;};
  String file_name = url.substring(CONF_FROMFILE.length()).trim();
  pool_config_file = file_name;
  File f = new File(file_name);
  BufferedReader INPUT = null;
  String line,name,value;
  int i;
  try
  {
   INPUT = new BufferedReader(new FileReader(f));
   while (INPUT.ready())
   {
    line = INPUT.readLine();
    if(line == null) line =""; line.trim();
    if(line.startsWith("#")) continue;
    if(line.length() == 0) continue;
    if(line.startsWith(CONF_URL)) this.jdbc_url = getFromLineParamString(CONF_URL,line); // line.substring(CONF_URL.length()+1).trim();
    if(line.startsWith(CONF_POOLUSER)) this.pool_user = getFromLineParamString(CONF_POOLUSER,line);
    if(line.startsWith(CONF_POOLUSER_PASSWORD)) this.pool_user_password = getFromLineParamString(CONF_POOLUSER_PASSWORD,line);
    if(line.startsWith(CONF_PUBUSER)) this.pub_user = getFromLineParamString(CONF_PUBUSER,line);
    if(line.startsWith(CONF_PUBUSER_PASSWORD)) this.pub_user_password = getFromLineParamString(CONF_PUBUSER_PASSWORD,line);
    if(line.startsWith(CONF_REJECT_DIRECT_USERS)) this.reject_direct_users = getFromLineParamString(CONF_REJECT_DIRECT_USERS,line);
    if(line.startsWith(CONF_ALLOW_DIRECT_USERS)) this.allow_direct_users = getFromLineParamString(CONF_ALLOW_DIRECT_USERS,line);
  //allow_direct_login = true;
  //allow_qtis_login = true;
  //min_pool_sessions = 0;
  //max_pool_sessions = 2;
   }
  }
  catch(IOException e)
  {
    //throw new MngException("Ошибка ввода/вывода при чтении файла анкеты "
    //  + S.getName()+ ":\n" + e);
  }
  try {if (INPUT != null) INPUT.close();}  catch(IOException e) {}
 } //load_config_from_file
 private static String getFromLineParamString(String param_name, String line)
 {
  if(param_name == null) return(null);
  if(line == null) return(null);
  if( line.length() - param_name.length() < 1) return(null);
  return(line.substring(param_name.length()).trim());
 }
 //constructors
 public QDBPool(String name, String jdbc_url, String jdbc_driver_class)
 {
  this.name = name;
  if(jdbc_url == null) jdbc_url = "";
  if(jdbc_url.startsWith(CONF_FROMFILE)) // load configuration from this file
  {
   load_config_from_file(jdbc_url);
  }
  else
  {
   this.jdbc_url = jdbc_url;
  }
  this.jdbc_driver_class=jdbc_driver_class;
  try{register_jdbc_driver(jdbc_driver_class);}catch(Exception e){}
 }
 // main
 /** this method is for testing und debugging purpose only. */
 public static void main(String[] varg)
 {
  System.out.println(getVer());
 } // main()

} //QDBPool class
