/*
 ConcepTIS project/QTIS project
 (c) Alex V Eustrop & eustrosoft.org 2009,2023
 LICENSE (this file) : BALES, BSD, MIT on your choice (see bales.eustrosoft.org)
 also see LICENSE at the project's root directory for the whole project license

 Purpose: pool of sessions and connections with TIS/SQL-compatible JDBC source
          ( PostgreSQL 13 tested but should be suitable for any DBMS)
 History (psql.jsp & dbpool.jsp):
  2023/04/07 dbpool.jsp started from psql.jsp
  2023/04/25 org.eustrosoft.qdbp package started from 330 lines of dbpool.jsp
*/
package org.eustrosoft.qdbp;
import java.util.*;
import java.sql.*;

// QDBPException.java - это общий класс исключений пакета QDBPool
public class QDBPException extends RuntimeException {public QDBPException(String msg){super(msg);}}
