package org.eustrosoft.zDBCPool;

import org.eustrosoft.zDBCPool.DBConnection;
import org.eustrosoft.zDBCPool.DBPool;
import org.eustrosoft.zDBCPool.DBPoolService;
import org.junit.*;

import static org.junit.Assert.*;

public class DBPoolTest {

    private DBPoolService service;
    private DBPool poolForTest;

    @Before
    public void setUp() throws Exception {
        service = new DBPoolService();
        poolForTest = service.getDBPool("TISC-USERS");
    }

    @After
    public void tearDown() throws Exception {
        poolForTest.closeDBPool();
    }

    @Test
    public void getConnection() throws Exception{
        HungryUser hungryUser = new HungryUser();
        WaitingUser waitingUser = new WaitingUser();

        //забираем возвожные 2 коннекта
        Thread user1 = new Thread(hungryUser);
        user1.start();
        Thread.sleep(100);

        //пытаемся получить коннект
        Thread user2 = new Thread(waitingUser);
        user2.start();

        user1.join();
        user2.join();

        assertNotNull(waitingUser.connection);
    }

    public class HungryUser implements Runnable{
        @Override
        public void run() {
            try {
                DBConnection connection1 = poolForTest.allocConnection();
                System.out.println("[HungryUser] received connection1");
                DBConnection connection2 = poolForTest.allocConnection();
                System.out.println("[HungryUser] received connection2");
                Thread.sleep((long) (poolForTest.MaxWaitTime*1.0));
                poolForTest.freeConnection(connection2);
                System.out.println("[HungryUser] free connection2");
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public class WaitingUser implements Runnable{
        DBConnection connection;
        @Override
        public void run() {
            try {
                System.out.println("[WaitingUser] try to get connection");
                connection = poolForTest.allocConnection();
                System.out.println("[WaitingUser] received connection");
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}