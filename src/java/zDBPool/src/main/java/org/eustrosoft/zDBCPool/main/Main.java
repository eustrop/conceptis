package org.eustrosoft.zDBCPool.main;

import org.eustrosoft.zDBCPool.DBConnection;
import org.eustrosoft.zDBCPool.DBPool;
import org.eustrosoft.zDBCPool.DBPoolService;

public class Main {
    public static void main(String[] args) throws Exception{

        DBPoolService service = DBPoolService.getInstanse();
        System.out.println(service);
        DBPool pool = service.getDBPool("TISC-USERS");
        DBConnection dbConnection1 = pool.allocConnection();
        DBConnection dbConnection2 = pool.allocConnection();
        DBConnection dbConnection3 = pool.allocConnection();
        System.out.println(dbConnection1);
        pool.closeDBPool();
        service.closeAllDbPools();

    }
}
