package org.eustrosoft.zDBCPool;


import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DBPool {

    private final List<DBConnection> FreeConnections = new LinkedList<>();
    private final List<DBConnection> AllConnections = new LinkedList<>();
    Map<String, String> mapOfUserPassword = new ConcurrentHashMap<>();
    String dBPoolName = "default";
    String JDBCDriver;
    String JDBCString;
    String JDBCPingQuery;
    int MaxConnections = 10;
    int MinConnections = 1;
    long MaxWaitTime = 200;

    public DBPool(String dBPoolName) {
        this.dBPoolName = dBPoolName;
    }

    /**
     *  Выделение свободного соединения из коллекции FreeConnections.
     *  В случае от
     *
     * @return DBConnection
     */
    public DBConnection allocConnection() throws DBPoolException {
        DBConnection dbc = null;
        long start_time = System.currentTimeMillis();
        long current_time = start_time;
        do {
            synchronized (this) {
                if (FreeConnections.isEmpty()) {
                    System.out.println("waiting...");
                    try {
                        if ((current_time - start_time) >= MaxWaitTime) {
                            throw new DBPoolException(
                                    dBPoolName + " : истекло допустимое время ожидания освобождения " +
                                            " JDBC соединения (" + MaxWaitTime + "ms)");
                        }
                        wait(MaxWaitTime / 10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    if (dbc == null) dbc = FreeConnections.remove(0);
                    else reconect(dbc);
                }
                current_time = System.currentTimeMillis();
                System.out.println(dBPoolName + " notified. time : " + (current_time - start_time));
            }
        } while (!checkConnection(dbc));
        return (dbc);
    }

    public DBConnection allocConnection(String userName) throws DBPoolException {
        DBConnection dbc = null;
        long start_time = System.currentTimeMillis();
        long current_time = start_time;
        boolean found = false;
        do {
            synchronized (this) {
                if (dbc == null) {
                    for (DBConnection con: FreeConnections) {
                        if (con.name.equals(userName)) {
                            dbc = con;
                            found = true;
                            FreeConnections.remove(con);
                            break;
                        }
                    }
                    if (!found){
                        System.out.println("waiting...");
                        try {
                            if ((current_time - start_time) >= MaxWaitTime) {
                                throw new DBPoolException(
                                        dBPoolName + " : истекло допустимое время ожидания освобождения " +
                                                " JDBC соединения (" + MaxWaitTime + "ms)");
                            }
                            wait(MaxWaitTime / 10);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else reconect(dbc);
                current_time = System.currentTimeMillis();
                System.out.println(dBPoolName + " notified. time : " + (current_time - start_time));
            }
        } while (!checkConnection(dbc));
        return (dbc);
    }

    public void freeConnection(DBConnection dbConnection) throws DBPoolException{
        if (dbConnection == null) return;
        if (AllConnections.contains(dbConnection)){
           FreeConnections.add(dbConnection);
        }
        else throw new DBPoolException("connection doesn't belong to this dbPool");
    }

    public void closeDBPool()  throws DBPoolException{
        try {
            for (DBConnection connection : AllConnections) {
                if (connection != null) connection.closeConnection();
            }
        } catch (SQLException e){
            throw new DBPoolException("error occur in closing dbPool",e);
        }
    }

    /**
     * Инициализация пула соединений. Для каждой пары <User, Password>
     * из карты [mapOfUserPassword] создается [MinConnections] соединений
     */
    void initDbPoll() throws DBPoolException {
        try {
            Driver clazz = (Driver) Class.forName(JDBCDriver).getDeclaredConstructor().newInstance();
            DriverManager.registerDriver(clazz);
        } catch (InvocationTargetException e) {
            throw new DBPoolException(e.getTargetException());
        } catch (Exception e) {
            throw new DBPoolException("error occur during registering procedure of DB driver", e);
        }
        makeAllPossibleConnections();
    }

    /**
     * Создает все возможные соединения к БД на основе указанных логинов
     * и паролей в карте mapOfUserPassword
     */
    private void makeAllPossibleConnections() throws DBPoolException {
        for (Map.Entry<String, String> userPassword : mapOfUserPassword.entrySet()) {
            String user = userPassword.getKey();
            String password = userPassword.getValue();
            String url = makeURLforConnection(user, password);
            for (int i = 0; i < MinConnections; i++) {
                makeNewConnection(url, user);
            }
        }
    }

    /**
     * На основе переданного логина и пароля создает URL для
     * подкючения к БД.
     */
    private String makeURLforConnection(String user, String password) {
        StringBuilder url = new StringBuilder();
        url.append(JDBCString)
                .append("user=").append(user)
                .append("&password=").append(password);
        return url.toString();
    }

    /**
     * Создание единичного соединения с БД по указанному url
     */
    private void makeNewConnection(String url,String name) throws DBPoolException {
        try {
            DBConnection dbConnection = new DBConnection(dBPoolName, url, name);
            FreeConnections.add(dbConnection);
            AllConnections.add(dbConnection);
        } catch (SQLException e) {
            throw new DBPoolException("can't create single connection to DB, check url settings", e);
        }
    }


    /**
     * Пытается восстановить соединение на основе URL, сохраненного в
     * объекте DBConnection.
     */
    private void reconect(DBConnection dbc) {
        if (dbc == null) return;
        if (dbc.getSQLConnection() == null) {
            try {
                dbc.reconect();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * В случае успешного пинга соединения вернет true. Для всех остальных
     * случаев возвращает fslse.
     */
    private boolean checkConnection(DBConnection dbc){
        if (dbc == null) return false;
        Connection connection = dbc.getSQLConnection();
        try {
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(JDBCPingQuery);
            while (rs.next()) {
               // System.out.println("ping connection: "+rs.getObject(1));
            }
        } catch (SQLException e){
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
