package org.eustrosoft.zDBCPool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

    private Connection connection;
    String url;
    String dBPoolName;
    String name;

    public DBConnection(String dBPoolName, String url, String name) throws SQLException{
        this.url = url;
        this.dBPoolName = dBPoolName;
        connection = DriverManager.getConnection(url);
        this.name = name;
    }

    public void reconect() throws SQLException{
        connection = DriverManager.getConnection(url);
    }

    public String getdBPoolName() {
        return dBPoolName;
    }

    public Connection getSQLConnection() {
        return connection;
    }

    public void closeConnection() throws SQLException{
        connection.close();
    }
}
