package org.eustrosoft.zDBCPool;

public class DBPoolException extends Exception{
    public DBPoolException(String message, Throwable cause) {
        super(message, cause);
    }

    public DBPoolException(String message) {
        super(message);
    }

    public DBPoolException(Throwable cause) {
        super(cause);
    }
}
