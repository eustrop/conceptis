package org.eustrosoft.zDBCPool;

public enum DBPoolFields {
    JDBCPoolName,
    JDBCDriver,
    JDBCString,
    JDBCPingQuery,
    MaxConnections,
    MinConnections,
    MaxWaitTime,
}
