// DBPool project (it's part of the ConcepTIS project)
// (c) eustrosoft.org 2018
//  see LICENSE at the project's root directory
//  author - vulpes
//

package org.eustrosoft.zDBCPool;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DBPoolService {

	private static DBPoolService instance;
    private Map<String, DBPool> allDBPools = new ConcurrentHashMap<>();
	private String propertiesPath = "/s/proj/ConcepTIS/src/java/webapps/psql/work/installed_webapps/psql/config.conf"; 
	
	public static DBPoolService getInstanse() throws DBPoolException{
		if (instance == null){
			instance = new DBPoolService();
		}
		return instance;
	}
	
	public static DBPoolService getInstanse(String properties) throws DBPoolException{
		if (instance == null){
			instance = new DBPoolService(properties);
		}
		return instance;
	}
	
	private DBPoolService() throws DBPoolException {
        loadConfigs();
    }
	
	private DBPoolService(String propertiesPath) throws DBPoolException {
        if (propertiesPath != null && !propertiesPath.equals(""))
            this.propertiesPath = propertiesPath;
        loadConfigs();
    }
	

    public DBPool getDBPool(String name) throws DBPoolException{
        if (allDBPools.containsKey(name)) return allDBPools.get(name);
        else throw new DBPoolException("there is no such element");
    }

    public void closeAllDbPools() throws DBPoolException{
        for(DBPool pool: allDBPools.values()){
            pool.closeDBPool();
        }
    }

    /**
     * Читает конфиг из файла, расположенного в propertiesPath. Предполагается,
     * что в данном файле находится конфигурация для нескольких пулов соединений.
     * Создает одозначное отображение <Имя, объект пула соединений БД>
     * @throws DBPoolException - при неудачной попытке считывания информации
     *                         из указанного файла.
     */
    private void loadConfigs() throws DBPoolException {
        List<String> configs = null;
        try {
            configs = Files.readAllLines(Paths.get(propertiesPath));
        } catch (IOException e) {
            throw new DBPoolException("wrong format of properties s", e);
        }
        DBPool dbPool = null;
        String nameOfSection;
        for (String line : configs) {
            if (isComment(line)) continue;
            if (isNameOfSection(line)) {
                nameOfSection = getNameOFSection(line);
                dbPool = new DBPool(nameOfSection);
                allDBPools.put(nameOfSection, dbPool);
                continue;
            }
            if (dbPool == null) continue;
            if (isLoginAndPassword(line)) {
                String user = getNameOfUser(line);
                String password = getPasswordOfUser(line);
                dbPool.mapOfUserPassword.put(user, password);
            } else {
                // TODO: 08.08.2018 Сделать исключение "check data format"
                DBPoolFields nameOfField = DBPoolFields.valueOf(getNameOfField(line));
                String value = getValueOfField(line);
                setFieldsOfDBPool(dbPool, nameOfField, value);
            }
        }
        initAllDBPools();
    }

    /**
     * Инициализация всех пулов соединений
     */
    private void initAllDBPools() throws  DBPoolException{
        try {
            for (DBPool dbPool : allDBPools.values())
                dbPool.initDbPoll();
        }catch (DBPoolException e){
            throw new DBPoolException("Error in initialization DBPoll, check properties ",e);
        }
    }

    /**
     * Данные методы (всего 8 вниз по тексту до следующего комментария)
     * используются в методе инициализации пулов соединений.
     * @see DBPoolService#loadConfigs()
     */
    private boolean isComment(String line) {
        line = line.trim();
        if (line.startsWith("#")) return true;
        if (line.length() == 0) return true;
        return false;
    }

    private boolean isNameOfSection(String line) {
        line = line.trim();
        if (line.startsWith("[") && line.endsWith("]")) return true;
        else return false;
    }

    private boolean isLoginAndPassword(String line) {
        int equalsFirst = line.indexOf("=");
        if (line.indexOf("=", equalsFirst + 1) != -1) return true;
        else return false;
    }

    private String getNameOFSection(String line) {
        line = line.trim();
        int length = line.length();
        return line.substring(1, length - 1);
    }

    private String getNameOfField(String line) {
        line = line.trim();
        int equal = line.indexOf("=");
        return line.substring(0, equal);
    }

    private String getValueOfField(String line) {
        line = line.trim();
        int equal = line.indexOf("=");
        if (line.charAt(equal+1) == '\"') return line.substring(equal + 2, line.length()-1);
        else return line.substring(equal + 1);
    }

    private String getNameOfUser(String line) {
        line = line.trim();
        int firstQuote = line.indexOf("\"");
        int secondQuote = line.indexOf("\"", firstQuote + 1);
        return line.substring(firstQuote + 1, secondQuote);
    }

    private String getPasswordOfUser(String line) {
        line = line.trim();
        int secondQuote = line.lastIndexOf("\"");
        int firstQuote = line.lastIndexOf("\"", secondQuote - 1);
        return line.substring(firstQuote + 1, secondQuote);
    }

    /**
     * Инициализация полей объекта dBPool значениями, полученными из
     * файла конфигурации. DBPoolFields - перечисление, содержащее
     * названия полей объекта dBPool.
     * @param dBPool      инициализируемый объект пула соединений
     * @param nameOfField имя поля объекта пула соединений
     * @param value       значение поля объекта пула соединений
     */
    private void setFieldsOfDBPool(DBPool dBPool, DBPoolFields nameOfField, String value) {
        switch (nameOfField) {
            case JDBCDriver:
                dBPool.JDBCDriver = value;
                break;
            case JDBCString:
                dBPool.JDBCString = value;
                break;
            case JDBCPingQuery:
                dBPool.JDBCPingQuery = value;
                break;
            case MaxConnections:
                dBPool.MaxConnections = Integer.parseInt(value);
                break;
            case MinConnections:
                dBPool.MinConnections = Integer.parseInt(value);
                break;
            case MaxWaitTime:
                dBPool.MaxWaitTime = Integer.parseInt(value);
                break;
        }
    }
}
