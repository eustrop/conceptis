/*
 * ConcepTIS project
 * (c) Alex V Eustrop 2009
 * see LICENSE at the project's root directory
 * $Id$
 * Purpose: testing of postgres jdbc functionality via tisexmldb (Manifesto)
 * History:
 *	2009/08/04 started. 168 lines
 *	2009/08/05 now it's interactive psql-like tool. 214 lines
 */

import java.util.*;
import java.io.*;
import java.sql.*;

/** Main application class.
 */

public class TISExmlDB
{
  /** Main routine, for using this class as stand-alone application */
  public static void main(String[] args)
  {
   String server_url = "jdbc:postgresql:tisexmpldb";
   //String server_url = "jdbc:postgresql:tisexmpldb?user=tisuser1&password=";
   String user = "tisuser1";
   String pass = "";
   String sz_in;
   BufferedReader cmd_in = new BufferedReader(new InputStreamReader(System.in));
   int i;
   try
   {
    // app head
    printmsgln( " TISExmlDB - testing functionality of tisexmldb via JDBC");
    // read jdbc url
    printmsg("  default server url is '" + server_url +
	 "'\n  input another one (or just press enter):" );
    sz_in = cmd_in.readLine(); if( sz_in.length() > 0 ) server_url = sz_in;
    printmsg("user (" + user + "):");
    sz_in = cmd_in.readLine(); if( sz_in.length() > 0 ) user = sz_in;
    printmsg("password (" + pass + "):");
    sz_in = cmd_in.readLine(); if( sz_in.length() > 0 ) pass = sz_in;
    // register JDBC driver
    register_jdbc_driver("org.postgresql.Driver");
    // open JDBC connection
    java.sql.Connection dbc=DriverManager.getConnection(server_url,user,pass);
    // type help message
    print_help();
    while(true)
    {
     printmsg( "TISExmlDB> ");
     sz_in = cmd_in.readLine();
     if( sz_in == null ) {printmsgln("EOF"); break;}
     if( sz_in.trim().length() == 0 ) continue;
     if("exit".equals(sz_in.trim().toLowerCase())) break;
     if("help".equals(sz_in.trim().toLowerCase())){print_help(); continue;}
     if("show".equals(sz_in.trim().toLowerCase()))
      {printmsgln(" server_url=" + server_url +"\n user=" + user); continue;}
     if("select".equals(sz_in.trim().toLowerCase()))
      {sz_in="select * from tis.V_Somedata";}
     if(sz_in.trim().toLowerCase().startsWith("create_somedata"))
     {
     try{
       int colon_pos=sz_in.indexOf(":");
       String value=sz_in.substring(colon_pos+1);
       String sz_scope=sz_in.substring(1,colon_pos).trim().toLowerCase();
       sz_scope=sz_scope.substring("create_somedata".length()).trim();
       //printerrln("not implemented. scope=" + sz_scope + " value=" + value);
       int scope=Integer.parseInt(sz_scope);
       // do create
       create_Somedata(dbc,scope,value);
      } catch(Exception e){printerrln("ERROR: " + e);}
      continue;
     } // .startsWith("create_somedata")
     exec_sql(dbc,sz_in);
    }
    // do 
   }
   catch (Exception ex)
   { printerrln("Fatal exception: " + ex); }
   printmsgln("-- end of work --");
  } // main()

  /** create driver class by its classname (jdbc_driver)
   * @param jdbc_driver - "org.postgresql.Driver" for postgres,
   * "oracle.jdbc.driver.OracleDriver" for oracle, etc.
   */ 
  private static void register_jdbc_driver(String jdbc_driver)
	throws Exception
  {
   java.sql.Driver d;
   Class dc;
   // get "Class" object for driver's class
   try
   {
    dc = Class.forName(jdbc_driver);
    d = (java.sql.Driver)dc.newInstance();
   }
   catch(ClassNotFoundException e) {
     throw new Exception("register_jdbc_driver:" + "unable to get Class for "
     + jdbc_driver + ":" + e); }
   catch(Exception e) {
     throw new Exception("register_jdbc_driver: unable to get driver " 
     + jdbc_driver + " : " + e); }
   // register driver
   try { DriverManager.registerDriver(d); }
   catch(SQLException e) { throw new Exception(
   "register_jdbc_driver: unable to register driver "+jdbc_driver+" : "+e);}
  } // register_jdbc_driver()
  /** print into System.out the whole of java.sql.ResultSet
   */
   public static void create_Somedata(java.sql.Connection dbc,
     int scope_id, String value)
    throws java.sql.SQLException
   {
    String sz_sql = "select id,errcode,errdesc from tis.create_Somedata(?,?)";
    java.sql.PreparedStatement pst = null;
    java.sql.ResultSet rs = null;
    try
    {
     pst = dbc.prepareStatement(sz_sql);
     pst.setInt(1,scope_id);
     pst.setString(2,value);
     rs = pst.executeQuery();
     rs.next();
     int id = rs.getInt(1);
     int errcode = rs.getInt(2);
     String errdesc = rs.getString(3);
     //print_sql_rs(rs);
     if(errcode==0) printmsgln("Ok. id=" + id + " with message: " + errdesc);
     else printmsgln("ERROR: " + errcode + " " + errdesc);
    }
    catch(SQLException e){
     printerrln("sql error during \"" + sz_sql + "\": " + e ); 
    }
    finally{
     try{if(rs != null) rs.close();}catch(SQLException e){}
     try{if(pst != null) pst.close();}catch(SQLException e){}
    }
   } //create_Somedata()
  /** print into System.out the whole of java.sql.ResultSet
   */
   public static void exec_sql(java.sql.Connection dbc,String sz_sql)
    throws java.sql.SQLException
   {
    java.sql.Statement st = null;
    java.sql.ResultSet rs = null;
    try
    {
     st = dbc.createStatement();
     rs = st.executeQuery(sz_sql);
     print_sql_rs(rs);
    }
    catch(SQLException e){
     printerrln("sql error during \"" + sz_sql + "\": " + e ); 
    }
    finally{
     try{if(rs != null) rs.close();}catch(SQLException e){}
     try{if(st != null) st.close();}catch(SQLException e){}
    }
   } //exec_sql()

  /** print into System.out the whole of java.sql.ResultSet
   */
   public static void print_sql_rs(java.sql.ResultSet rs)
    throws java.sql.SQLException
   {
   java.sql.ResultSetMetaData rsmd = rs.getMetaData();
   int column_count=rsmd.getColumnCount();
   int i;
    // print column's titles
    for(i=1;i<=column_count;i++)
    {
    printmsg(rsmd.getColumnName(i) +
      "(" + rsmd.getColumnTypeName(i) + ")|");
    } // for column's titles
    printmsgln();
    while(rs.next())
    {
     // print columns values
     for(i=1;i<=column_count;i++)
     {
      printmsg(rs.getObject(i) +  "|"); // rs.getObject(i).getClass().getName()
     } // for column's values
    printmsgln();
    } // while(rs.next())
   } // print_sql_rs
  /** print help message */
   public static void print_help()
   {
    printmsg(" Now you can do any SQL requests to current DB, e.g.:\n" +
    "  select tis.create_Somedata(1,'Record to scope 1')\n" +
    "  select tis.create_Somedata(2,'Record to scope 2')\n" +
    "  select tis.create_Somedata(3,'Record to scope 3')\n" +
    "  select * from tis.V_Somedata\n" +
    "  select * from tis.Somedata\n" +
    "  select tis.sam_get_user()\n" +
    " or use some special commands: \n" +
    "  select - for 'select * from tis.V_Somedata'\n" +
    "  create_somedata <scope>:<value> - calling it via PreparedSatement;\n" +
    "  show - to show session parameters\n" + 
    "  exit - to terminate session\n" + 
    " Some other useful tables: tis.SAMACL, tis.SAMUsers; sequence: tis.seq_Somedata\n" +
    " Type 'help' to repeat this message\n\n");
   }
   /** print message to stdout. just a wrapper to System.out.print */
   public static void printmsg(String msg){System.out.print(msg);}
   public static void printmsgln(String msg){System.out.println(msg);}
   public static void printmsgln(){System.out.println();}
   /** print message to stderror. just a wrapper to System.err.print */
   public static void printerr(String msg){System.err.print(msg);}
   public static void printerrln(String msg){System.err.println(msg);}
   public static void printerrln(){System.err.println();}
} // class TISExmlDB
