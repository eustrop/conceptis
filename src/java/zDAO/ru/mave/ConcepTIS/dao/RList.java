// ConcepTIS project
// (c) Alex V Eustrop 2009-2010
// see LICENSE at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao;

import java.util.Vector;
import java.util.HashSet;
import java.util.Hashtable;

/** Sequential List of Record class objects with two indexes capability.
 *
 * @see #getByID()
 * @see #getByKey()
 * @see Record
 */
public class RList extends Record
{
private Vector list = new Vector();
private HashSet set = new HashSet();
private Hashtable ht_id = new Hashtable();
private Hashtable ht_key = new Hashtable();

private Long id = null;
private String key = null;

public Long getID(){return(id);}
public String getKey(){return(key);}
public String getCaption(){return(getKey());}
//SIC! don't work 2020-11-23
//public ZJson toZJson(){ZJson j = super.toZJson(); j.add("dto",toZJson()); return(j); }
//public ZJson getZJsonRowModel(){ZJson j = super.toZJson(); j.add("dto",toZJson()); return(j); }
public ZJson toZJsonRow()
{
 ZJson j = new ZJson();
 int i;
 int size = size();
 for(i=0;i<size;i++){j.addItem(get(i).toZJson());}
 return(j);
} // toZJson()
protected void setID(Long id){this.id=id;}
protected void setKey(String key){this.key=key;}

protected static void index_insert(Hashtable index, Record r, Object key)
{
 if(index == null) return;
 if(key == null || r == null) throw(new NullPointerException());
 index.put(key,r);
} // add2index(Hashtable index, Record r, Object key)

protected static void index_remove(Hashtable index, Object key)
{
 if(index == null) return;
 index.remove(key);
} // index_remove(Hashtable index, Object key)

protected static Record index_find(Hashtable index, Object key)
{
 if(index == null) return(null);
 Record r = (Record)index.get(key);
 return((Record)index.get(key));
}

/** true if r not in this set (like the java.util.Set). */
public boolean add(Record r)
{
 if(r == null) throw(new NullPointerException());
 if(!set.add(r)) return(false); // throw(new IllegalArgumentException()); ???
 list.add(r);
 if(r.getID() != null && ht_id != null){index_insert(ht_id,r,r.getID());}
 if(r.getKey() != null && ht_key != null){index_insert(ht_key,r,r.getKey());}
 return(true);
}

/** number of records. */
public int size() {return(list.size());}

/** Record at specified position. */
public Record get(int i){return( (Record)list.get(i) );}

/** Recreate all indexes.
 * If contained record's key data changed or some other changes made
 * at the list - indexes could stop conform real data. Use this
 * method to correct that behaviour (but keep in mind heaviness). 
 */
public void reindex()
{
 Vector old_list = list; list = new Vector();
 set = new HashSet();
 if(ht_id == null) ht_id = new Hashtable();
 if(ht_key == null) ht_key = new Hashtable();
 ht_id.clear(); ht_key.clear();
 int i=0;
 while(i<old_list.size()){add((Record)old_list.get(i));i++;}
} // reindex()

/** drop all indexes and prevent future indexing.
 * user reindex() to return into indexing mode.
 * @see #reindex()
 */
public void stopindex()
{
 ht_id=null; ht_key=null;
}

/** find single record with getID() == id.
 * Can return null if index broken even if such record exists.
 * @see #reindex()
 */
public Record getByID(Long id)
{
 if(id == null) return(null);
 Record r = index_find(ht_id,id);
 if(r==null) return(null);
 if(!id.equals(r.getID())) r = null;
 /* //not tested! // search for record bypass index
 if(r == null){
  iterator i = list.iterator();
  while(i.hasNext()){
   r = (Record)i.next();
   if( id.equals( r.getID() ) ) return(r);
  }
 }
 else return(r);
 return(null);
 */
 return(r);
} // getByID(Long id)

/** find single record with getKey() == key.
 * Can return null if index broken even if such record exists.
 * @see #reindex()
 */
public Record getByKey(String key)
{
 if(key == null) return(null);
 Record r = index_find(ht_key,key);
 if(r==null) return(null);
 if(!key.equals( r.getKey() )) r = null;
 return(r);
}

public void remove(Record r)
{
int i = list.indexOf(r);
if(i>=0) remove(i);
}

public void remove(int i)
{
Record o = (Record)list.get(i);
if( o == null) return;
Long id = o.getID(); key = o.getKey();
if(ht_id != null && id != null ) ht_id.remove(id);
if(ht_key != null && key != null) ht_id.remove(key);
set.remove(o);
list.remove(i);
}

public void sort(){java.util.Collections.sort(list); }

/** remove all records, clear indexes and shadow sets.
 */
public void clear()
{
 list.clear();
 set.clear();
 if(ht_id != null) ht_id.clear();
 if(ht_key != null) ht_key.clear();
 ht_id = new Hashtable(); ht_key = new Hashtable();
}

} //Record
