// ConcepTIS project
// (c) Alex V Eustrop 2009-2010
// see LICENSE at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao;

/** single field of some record.
 *
 */
public class ZField extends Record{


public static final int T_UNKNOWN = 0;
public static final int T_LONG = 1;
public static final int T_INT = 2;
public static final int T_SHORT = 3;
public static final int T_STRING = 4;
public static final int T_YESNO = 5;
public static final int T_DATETIME = 6;

protected String code; // TIS code of field: XU05, AA01, XS03 etc
protected String name; // name of field (login,name,is_local)
protected Object value; // value of field
protected int length; // length for String
protected int ftype; // type of field

protected String fcaption; // this caption must be used instead of any others if set

/** must be used by visual UI as "name of field" instead of any other if not null. */
public String getFCaption() {return(fcaption);}
public void setFCaption(String caption) {this.fcaption=caption;}

/** code of this field (XU05, AA01, XS03 etc) for dictionary lookup. */
public String getCode() {return(code);}
public void setCode(String code) {this.code=code;
	if(this.code!=null)this.code=this.code.intern();}

/** name of this field (login,name,is_local etc). */
public String getName() {return(name);}
/** set name of field. */
public void setName(String name) {this.name=name;
	if(this.name!=null)this.name=this.name.intern();}

/** value of field (converted to String). */
public String getValue()
{if(value==null)return(null); return(value.toString());}
/** value of field (as is). */
public Object getValueObject() {return(value);}
/** set value of field. */
public void setValue(Object value) {this.value=value;}

/** maximum length of field (in characters for T_STRING, unuseful otherwise). */
public int getLength() {return(length);}
public void setLength(int length) {this.length=length;}

/** type of field (T_LONG,T_STRING etc).  */
public int getType() {return(ftype);}
public void setType(int ftype) {this.ftype=ftype;}

// Record's class abstract methods implementation
public Long getID(){return(null);}
public String getKey(){return(getName());}
/** this caption shows both name and value of this field as is. */
public String getCaption(){return(getKey()+"="+getValue());}
/** import missed fields and missed metadata of fields from donor FieldSet.
 */
public void importMissedData(ZField f)
{
 this.setCode(f.getCode());
 this.setType(f.getType());
 this.setLength(f.getLength());
}

public ZJson toZJson()
{
 ZJson json = new ZJson(ZJson.ZJSON_TYPE_OBJECT,ZJson.ZJSON_PRINT_MODE_ROW);
 String field_name = getName();
 String field_code = getCode();
 json.addItem("name",getKey());
 if(field_name == null) field_name = "";
 if( !field_name.equals(field_code))
  json.addItem("code",getCode());
 json.addItem("value",getValue());
 //json.addItem("length" , getLength());
 //json.addItem("type" ,  getType() );
 //json.addItem( "fcaption" , getFCaption());
 return(json);
} // toZJson()


// constructors
public ZField(String name, Object value, String code, int ftype, int length, String fcaption)
{
 this.name = name; this.value = value;
 this.code = code; this.ftype = ftype; this.length = length;
 this.fcaption = fcaption;
 if(this.code!=null)this.code=this.code.intern();
 if(this.name!=null)this.name=this.name.intern();
}

public ZField(String name, Object value, String code, int ftype, int length)
{ this(name,value,code,ftype,length,null); }

public ZField(String name, Object value)
{ this(name,value,null,T_UNKNOWN,-1); }

public ZField(ZField f)
{
 this(f.getName(),f.getValueObject(),f.getCode(),
 f.getType(),f.getLength());
}

} //ZField
