// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao;

import java.util.*;
import java.sql.*;
//import ru.mave.ConcepTIS.dao.SAM.*;
//import ru.mave.ConcepTIS.dao.TIS.*;
//import ru.mave.ConcepTIS.dao.TISC.*;

/** (replacement for ZSystem class)
 * Entrance ConcepTIS database access class. Object of this
 * class encapsulate java.sql.Connection and represent all
 * database at TIS-SQL fashion. All underlaying TIS objects 
 * could be accessed only by means of ZSystem directly or
 * indirectly (with the aid of some kind of underlaying
 * objects)
 *
 * @see TISC.Area
 */
public class QSystem extends ZSystem
{
// constructors
public QSystem(java.sql.Connection dbc)
{
 super(dbc);
}
}// QSystem
