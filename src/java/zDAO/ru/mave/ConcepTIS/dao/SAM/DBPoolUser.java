// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao.SAM;

import ru.mave.ConcepTIS.dao.*;

/** .
 *
 */
public class DBPoolUser extends Record
{
public String	db_user;
public Long	sid;
public Short	slevel_min;
public Short	slevel_max;
public String	descr;

public Long getID(){return(null);}
public String getKey(){return(db_user);}
public String getCaption(){return(getKey());}

// Strings

public final static String F_DB_USER = "db_user";
public final static String F_SID = "sid";
public final static String F_SLEVEL_MIN = "slevel_min";
public final static String F_SLEVEL_MAX = "slevel_max";
public final static String F_DESCR = "descr";

public final static String FC_DB_USER = "XP01";
public final static String FC_SID = "XP02";
public final static String FC_SLEVEL_MIN = "XP03";
public final static String FC_SLEVEL_MAX = "XP03";
public final static String FC_DESCR = "XP04";

// SQL requests
public final static String TISQL_LIST =
	"select * from SAM.V_DBPoolUser";
public final static String TISQL_LOAD =
	"select * from SAM.V_DBPoolUser where ? = null";
public final static String TISQL_CREATE =
	"select * from SAM.create_DBPoolUser(?,?,?,?,?)";
public final static String TISQL_UPDATE =
	"select * from SAM.update_DBPoolUser(?,?,?,?,?)";
public final static String TISQL_DELETE =
	"select * from SAM.delete_DBPoolUser(?)";

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 db_user = getRSString(rs,1);
 sid = getRSLong(rs,2);
 slevel_min = getRSShort(rs,3);
 slevel_max = getRSShort(rs,4);
 descr = getRSString(rs,5);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 db_user = getFSString(fs,F_DB_USER);
 sid = getFSLong(fs,F_SID);
 slevel_min = getFSShort(fs,F_SLEVEL_MIN);
 slevel_max = getFSShort(fs,F_SLEVEL_MAX);
 descr = getFSString(fs,F_DESCR);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSString(ps,1,db_user);
 setPSLong(ps,2,sid);
 setPSShort(ps,3,slevel_min);
 setPSShort(ps,4,slevel_max);
 setPSString(ps,5,descr);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = new ZFieldSet();
 fs.add(new ZField(F_DB_USER,db_user,FC_DB_USER,ZField.T_UNKNOWN,64));
 fs.add(new ZField(F_SID,sid,FC_SID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SLEVEL_MIN,slevel_min,FC_SLEVEL_MIN,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SLEVEL_MAX,slevel_max,FC_SLEVEL_MAX,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_DESCR,descr,FC_DESCR,ZField.T_UNKNOWN,99));
 return(fs);
} //

// constructors
public DBPoolUser() { }
public DBPoolUser(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public DBPoolUser(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 this.getFromRS(rs);
} // DBPoolUser(java.sql.ResultSet rs)

} //DBPoolUser
