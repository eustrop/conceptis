// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao.SAM;

import ru.mave.ConcepTIS.dao.*;

/** .
 *
 */
public class Range extends Record
{
public Long	startid;
public Short	bitl;
public Long	endid;
public Long	prange;
public Short	pbitl;
public Long	ownerid;
public String	descr;

public Long getID(){return(null);}
public String getKey(){return(null);}
public String getCaption(){return(getKey());}

// Strings

public final static String F_STARTID = "startid";
public final static String F_BITL = "bitl";
public final static String F_ENDID = "endid";
public final static String F_PRANGE = "prange";
public final static String F_PBITL = "pbitl";
public final static String F_OWNERID = "ownerid";
public final static String F_DESCR = "descr";

public final static String FC_STARTID = "XR01";
public final static String FC_BITL = "XR02";
public final static String FC_ENDID = "XR03";
public final static String FC_PRANGE = "XR04";
public final static String FC_PBITL = "XR05";
public final static String FC_OWNERID = "XR06";
public final static String FC_DESCR = "XR07";

// SQL requests
public final static String TISQL_LIST =
	"select * from SAM.V_Range";
public final static String TISQL_LOAD =
	"select * from SAM.V_Range where ? = null";
public final static String TISQL_CREATE =
	"select * from SAM.create_Range(?,?,?,?,?,?,?)";
public final static String TISQL_UPDATE =
	"select * from SAM.update_Range(?,?,?,?,?,?,?)";
public final static String TISQL_DELETE =
	"select * from SAM.delete_Range(?)";

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 startid = getRSLong(rs,1);
 bitl = getRSShort(rs,2);
 endid = getRSLong(rs,3);
 prange = getRSLong(rs,4);
 pbitl = getRSShort(rs,5);
 ownerid = getRSLong(rs,6);
 descr = getRSString(rs,7);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 startid = getFSLong(fs,F_STARTID);
 bitl = getFSShort(fs,F_BITL);
 endid = getFSLong(fs,F_ENDID);
 prange = getFSLong(fs,F_PRANGE);
 pbitl = getFSShort(fs,F_PBITL);
 ownerid = getFSLong(fs,F_OWNERID);
 descr = getFSString(fs,F_DESCR);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSLong(ps,1,startid);
 setPSShort(ps,2,bitl);
 setPSLong(ps,3,endid);
 setPSLong(ps,4,prange);
 setPSShort(ps,5,pbitl);
 setPSLong(ps,6,ownerid);
 setPSString(ps,7,descr);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = new ZFieldSet();
 fs.add(new ZField(F_STARTID,startid,FC_STARTID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_BITL,bitl,FC_BITL,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_ENDID,endid,FC_ENDID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_PRANGE,prange,FC_PRANGE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_PBITL,pbitl,FC_PBITL,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_OWNERID,ownerid,FC_OWNERID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_DESCR,descr,FC_DESCR,ZField.T_UNKNOWN,99));
 return(fs);
} //

// constructors
public Range() { }
public Range(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public Range(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 this.getFromRS(rs);
} // Range(java.sql.ResultSet rs)

} //Range
