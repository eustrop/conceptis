// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao.SAM;

import ru.mave.ConcepTIS.dao.*;

/** .
 *
 */
public class User extends Record
{
public Long	id;
public Long	sid;
public Short	slevel;
public Short	slevel_min;
public Short	slevel_max;
public YesNo	locked;
public String	lang;
public String	login;
public String	db_user;
public String	full_name;

public Long getID(){return(id);}
public String getKey(){return(login);}
public String getCaption(){return(getKey());}

// Strings

public final static String F_ID = "id";
public final static String F_SID = "sid";
public final static String F_SLEVEL = "slevel";
public final static String F_SLEVEL_MIN = "slevel_min";
public final static String F_SLEVEL_MAX = "slevel_max";
public final static String F_LOCKED = "locked";
public final static String F_LANG = "lang";
public final static String F_LOGIN = "login";
public final static String F_DB_USER = "db_user";
public final static String F_FULL_NAME = "full_name";

public final static String FC_ID = "XU01";
public final static String FC_SID = "XU02";
public final static String FC_SLEVEL = "XU03";
public final static String FC_SLEVEL_MIN = "XU04";
public final static String FC_SLEVEL_MAX = "XU05";
public final static String FC_LOCKED = "XU06";
public final static String FC_LANG = "XU07";
public final static String FC_LOGIN = "XU08";
public final static String FC_DB_USER = "XU09";
public final static String FC_FULL_NAME = "XU10";

// SQL requests
public final static String TISQL_LIST =
	"select * from SAM.V_User";
public final static String TISQL_LOAD =
	"select * from SAM.V_User where id = ?";
public final static String TISQL_CREATE =
	"select * from SAM.create_User(?,?,?,?,?,?,?,?,?,?)";
public final static String TISQL_UPDATE =
	"select * from SAM.update_User(?,?,?,?,?,?,?,?,?,?)";
public final static String TISQL_DELETE =
	"select * from SAM.delete_User(?)";

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 id = getRSLong(rs,1);
 sid = getRSLong(rs,2);
 slevel = getRSShort(rs,3);
 slevel_min = getRSShort(rs,4);
 slevel_max = getRSShort(rs,5);
 locked = getRSYesNo(rs,6);
 lang = getRSString(rs,7);
 login = getRSString(rs,8);
 db_user = getRSString(rs,9);
 full_name = getRSString(rs,10);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 id = getFSLong(fs,F_ID);
 sid = getFSLong(fs,F_SID);
 slevel = getFSShort(fs,F_SLEVEL);
 slevel_min = getFSShort(fs,F_SLEVEL_MIN);
 slevel_max = getFSShort(fs,F_SLEVEL_MAX);
 locked = getFSYesNo(fs,F_LOCKED);
 lang = getFSString(fs,F_LANG);
 login = getFSString(fs,F_LOGIN);
 db_user = getFSString(fs,F_DB_USER);
 full_name = getFSString(fs,F_FULL_NAME);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSLong(ps,1,id);
 setPSLong(ps,2,sid);
 setPSShort(ps,3,slevel);
 setPSShort(ps,4,slevel_min);
 setPSShort(ps,5,slevel_max);
 setPSYesNo(ps,6,locked);
 setPSString(ps,7,lang);
 setPSString(ps,8,login);
 setPSString(ps,9,db_user);
 setPSString(ps,10,full_name);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = new ZFieldSet();
 fs.add(new ZField(F_ID,id,FC_ID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SID,sid,FC_SID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SLEVEL,slevel,FC_SLEVEL,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SLEVEL_MIN,slevel_min,FC_SLEVEL_MIN,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SLEVEL_MAX,slevel_max,FC_SLEVEL_MAX,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_LOCKED,locked,FC_LOCKED,ZField.T_UNKNOWN,1));
 fs.add(new ZField(F_LANG,lang,FC_LANG,ZField.T_UNKNOWN,3));
 fs.add(new ZField(F_LOGIN,login,FC_LOGIN,ZField.T_UNKNOWN,64));
 fs.add(new ZField(F_DB_USER,db_user,FC_DB_USER,ZField.T_UNKNOWN,64));
 fs.add(new ZField(F_FULL_NAME,full_name,FC_FULL_NAME,ZField.T_UNKNOWN,255));
 return(fs);
} //

// constructors
public User() { }
public User(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public User(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 this.getFromRS(rs);
} // User(java.sql.ResultSet rs)

} //User
