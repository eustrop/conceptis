// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao.SAM;

import ru.mave.ConcepTIS.dao.*;

/** .
 *
 */
public class UserSessionSeq extends Record
{
public Long	uid;
public Long	usi;
public Long	usi_max;
public DateTime	ts;

public Long getID(){return(null);}
public String getKey(){return(null);}
public String getCaption(){return(getKey());}

// Strings

public final static String F_UID = "uid";
public final static String F_USI = "usi";
public final static String F_USI_MAX = "usi_max";
public final static String F_TS = "ts";

public final static String FC_UID = "XSQ01";
public final static String FC_USI = "XSQ02";
public final static String FC_USI_MAX = "XSQ02";
public final static String FC_TS = "XSQ03";

// SQL requests
public final static String TISQL_LIST =
	"select * from SAM.V_UserSessionSeq";
public final static String TISQL_LOAD =
	"select * from SAM.V_UserSessionSeq where ? = null";
public final static String TISQL_CREATE =
	"select * from SAM.create_UserSessionSeq(?,?,?,?)";
public final static String TISQL_UPDATE =
	"select * from SAM.update_UserSessionSeq(?,?,?,?)";
public final static String TISQL_DELETE =
	"select * from SAM.delete_UserSessionSeq(?)";

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 uid = getRSLong(rs,1);
 usi = getRSLong(rs,2);
 usi_max = getRSLong(rs,3);
 ts = getRSDateTime(rs,4);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 uid = getFSLong(fs,F_UID);
 usi = getFSLong(fs,F_USI);
 usi_max = getFSLong(fs,F_USI_MAX);
 ts = getFSDateTime(fs,F_TS);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSLong(ps,1,uid);
 setPSLong(ps,2,usi);
 setPSLong(ps,3,usi_max);
 setPSDateTime(ps,4,ts);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = new ZFieldSet();
 fs.add(new ZField(F_UID,uid,FC_UID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_USI,usi,FC_USI,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_USI_MAX,usi_max,FC_USI_MAX,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_TS,ts,FC_TS,ZField.T_UNKNOWN,0));
 return(fs);
} //

// constructors
public UserSessionSeq() { }
public UserSessionSeq(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public UserSessionSeq(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 this.getFromRS(rs);
} // UserSessionSeq(java.sql.ResultSet rs)

} //UserSessionSeq
