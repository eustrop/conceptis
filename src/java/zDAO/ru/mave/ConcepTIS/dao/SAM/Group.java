// ConcepTIS project
// (c) Alex V Eustrop 2009-2010
// see LICENSE at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao.SAM;

import ru.mave.ConcepTIS.dao.*;

/** .
 *
 */
public class Group extends Record
{
public Long	id;
public Long	sid;
public String	name;
public String	descr;

public Long getID(){return(id);}
public String getKey(){return(name);}
public String getCaption(){return(getKey());}

// Strings

public final static String F_ID = "id";
public final static String F_SID = "sid";
public final static String F_NAME = "name";
public final static String F_DESCR = "descr";

public final static String FC_ID = "XG01";
public final static String FC_SID = "XG02";
public final static String FC_NAME = "XG03";
public final static String FC_DESCR = "XG04";

// SQL requests
public final static String TISQL_LIST =
	"select * from SAM.V_Group";
public final static String TISQL_LOAD =
	"select * from SAM.V_Group where id = ?";
public final static String TISQL_CREATE =
	"select * from SAM.create_Group(?,?,?,?)";
public final static String TISQL_UPDATE =
	"select * from SAM.update_Group(?,?,?,?)";
public final static String TISQL_DELETE =
	"select * from SAM.delete_Group(?)";

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 id = getRSLong(rs,1);
 sid = getRSLong(rs,2);
 name = getRSString(rs,3);
 descr = getRSString(rs,4);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 id = getFSLong(fs,F_ID);
 sid = getFSLong(fs,F_SID);
 name = getFSString(fs,F_NAME);
 descr = getFSString(fs,F_DESCR);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSLong(ps,1,id);
 setPSLong(ps,2,sid);
 setPSString(ps,3,name);
 setPSString(ps,4,descr);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = new ZFieldSet();
 fs.add(new ZField(F_ID,id,FC_ID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SID,sid,FC_SID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_NAME,name,FC_NAME,ZField.T_UNKNOWN,32));
 fs.add(new ZField(F_DESCR,descr,FC_DESCR,ZField.T_UNKNOWN,99));
 return(fs);
} //

// constructors
public Group() { }
public Group(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public Group(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 this.getFromRS(rs);
} // Group(java.sql.ResultSet rs)

} //Group
