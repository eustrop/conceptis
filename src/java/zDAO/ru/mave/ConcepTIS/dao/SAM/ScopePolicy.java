// ConcepTIS project
// (c) Alex V Eustrop 2009-2010
// see LICENSE at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao.SAM;

import ru.mave.ConcepTIS.dao.*;

/** .
 *
 */
public class ScopePolicy extends Record
{
public Long	sid;
public String	obj_type;
public Long	maxcount;
public Long	ocount;
public YesNo	doaudit;
public YesNo	dodebug;

public Long getID(){return(null);}
public String getKey(){return(obj_type + sid);}
public String getCaption(){return(getKey());}

// Strings

public final static String F_SID = "sid";
public final static String F_OBJ_TYPE = "obj_type";
public final static String F_MAXCOUNT = "maxcount";
public final static String F_OCOUNT = "ocount";
public final static String F_DOAUDIT = "doaudit";
public final static String F_DODEBUG = "dodebug";

public final static String FC_SID = "XSP01";
public final static String FC_OBJ_TYPE = "XSP02";
public final static String FC_MAXCOUNT = "XSP03";
public final static String FC_OCOUNT = "XSP04";
public final static String FC_DOAUDIT = "XSP05";
public final static String FC_DODEBUG = "XSP06";

// SQL requests
public final static String TISQL_LIST =
	"select * from SAM.V_ScopePolicy";
public final static String TISQL_LOAD =
	"select * from SAM.V_ScopePolicy where ? = sid";
public final static String TISQL_CREATE =
	"select * from SAM.set_ScopePolicy(?,?,?,?,?)";
public final static String TISQL_UPDATE =
	"select * from SAM.set_ScopePolicy(?,?,?,?,?)";
public final static String TISQL_DELETE =
	"select * from SAM.delete_ScopePolicy(?,?)";

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 sid = getRSLong(rs,1);
 obj_type = rtrimString(getRSString(rs,2));
 maxcount = getRSLong(rs,3);
 ocount = getRSLong(rs,4);
 doaudit = getRSYesNo(rs,5);
 dodebug = getRSYesNo(rs,6);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 sid = getFSLong(fs,F_SID);
 obj_type = getFSString(fs,F_OBJ_TYPE);
 maxcount = getFSLong(fs,F_MAXCOUNT);
 ocount = getFSLong(fs,F_OCOUNT);
 doaudit = getFSYesNo(fs,F_DOAUDIT);
 dodebug = getFSYesNo(fs,F_DODEBUG);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSLong(ps,1,sid);
 setPSString(ps,2,obj_type);
 setPSLong(ps,3,maxcount);
 // setPSLong(ps,4,ocount);
 setPSYesNo(ps,4,doaudit);
 setPSYesNo(ps,5,dodebug);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = new ZFieldSet();
 fs.add(new ZField(F_SID,sid,FC_SID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_OBJ_TYPE,obj_type,FC_OBJ_TYPE,ZField.T_UNKNOWN,1));
 fs.add(new ZField(F_MAXCOUNT,maxcount,FC_MAXCOUNT,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_OCOUNT,ocount,FC_OCOUNT,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_DOAUDIT,doaudit,FC_DOAUDIT,ZField.T_UNKNOWN,1));
 fs.add(new ZField(F_DODEBUG,dodebug,FC_DODEBUG,ZField.T_UNKNOWN,1));
 return(fs);
} //

// constructors
public ScopePolicy() { }
public ScopePolicy(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public ScopePolicy(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 this.getFromRS(rs);
} // ScopePolicy(java.sql.ResultSet rs)

} //ScopePolicy
