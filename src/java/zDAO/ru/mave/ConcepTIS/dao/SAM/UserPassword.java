// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao.SAM;

import ru.mave.ConcepTIS.dao.*;

/** .
 *
 */
public class UserPassword extends Record
{
public Long	uid;
public String	algo;
public Integer	iterations;
public DateTime	datefrom;
public DateTime	dateto;
public String	password;
public String	salt;

public Long getID(){return(null);}
public String getKey(){return(null);}
public String getCaption(){return(getKey());}

// Strings

public final static String F_UID = "uid";
public final static String F_ALGO = "algo";
public final static String F_ITERATIONS = "iterations";
public final static String F_DATEFROM = "datefrom";
public final static String F_DATETO = "dateto";
public final static String F_PASSWORD = "password";
public final static String F_SALT = "salt";

public final static String FC_UID = "XUP01";
public final static String FC_ALGO = "XUP02";
public final static String FC_ITERATIONS = "XUP03";
public final static String FC_DATEFROM = "XUP04";
public final static String FC_DATETO = "XUP05";
public final static String FC_PASSWORD = "XUP06";
public final static String FC_SALT = "XUP07";

// SQL requests
public final static String TISQL_LIST =
	"select * from SAM.V_UserPassword";
public final static String TISQL_LOAD =
	"select * from SAM.V_UserPassword where ? = null";
public final static String TISQL_CREATE =
	"select * from SAM.create_UserPassword(?,?,?,?,?,?,?)";
public final static String TISQL_UPDATE =
	"select * from SAM.update_UserPassword(?,?,?,?,?,?,?)";
public final static String TISQL_DELETE =
	"select * from SAM.delete_UserPassword(?)";

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 uid = getRSLong(rs,1);
 algo = getRSString(rs,2);
 iterations = getRSInteger(rs,3);
 datefrom = getRSDateTime(rs,4);
 dateto = getRSDateTime(rs,5);
 password = getRSString(rs,6);
 salt = getRSString(rs,7);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 uid = getFSLong(fs,F_UID);
 algo = getFSString(fs,F_ALGO);
 iterations = getFSInteger(fs,F_ITERATIONS);
 datefrom = getFSDateTime(fs,F_DATEFROM);
 dateto = getFSDateTime(fs,F_DATETO);
 password = getFSString(fs,F_PASSWORD);
 salt = getFSString(fs,F_SALT);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSLong(ps,1,uid);
 setPSString(ps,2,algo);
 setPSInteger(ps,3,iterations);
 setPSDateTime(ps,4,datefrom);
 setPSDateTime(ps,5,dateto);
 setPSString(ps,6,password);
 setPSString(ps,7,salt);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = new ZFieldSet();
 fs.add(new ZField(F_UID,uid,FC_UID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_ALGO,algo,FC_ALGO,ZField.T_UNKNOWN,8));
 fs.add(new ZField(F_ITERATIONS,iterations,FC_ITERATIONS,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_DATEFROM,datefrom,FC_DATEFROM,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_DATETO,dateto,FC_DATETO,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_PASSWORD,password,FC_PASSWORD,ZField.T_UNKNOWN,127));
 fs.add(new ZField(F_SALT,salt,FC_SALT,ZField.T_UNKNOWN,127));
 return(fs);
} //

// constructors
public UserPassword() { }
public UserPassword(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public UserPassword(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 this.getFromRS(rs);
} // UserPassword(java.sql.ResultSet rs)

} //UserPassword
