// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao.SAM;

import ru.mave.ConcepTIS.dao.*;

/** .
 *
 */
public class Scope extends Record
{
public Long	id;
public Long	sid;
public Short	slevel_min;
public Short	slevel_max;
public String	name;
public YesNo	is_local;
public String	auditlvl;
public DateTime	chpts;
public String	descr;

public Long getID(){return(id);}
public String getKey(){return(name);}
public String getCaption(){return(getKey());}

// Strings

public final static String F_ID = "id";
public final static String F_SID = "sid";
public final static String F_SLEVEL_MIN = "slevel_min";
public final static String F_SLEVEL_MAX = "slevel_max";
public final static String F_NAME = "name";
public final static String F_IS_LOCAL = "is_local";
public final static String F_AUDITLVL = "auditlvl";
public final static String F_CHPTS = "chpts";
public final static String F_DESCR = "descr";

public final static String FC_ID = "XS01";
public final static String FC_SID = "XS02";
public final static String FC_SLEVEL_MIN = "XS03";
public final static String FC_SLEVEL_MAX = "XS04";
public final static String FC_NAME = "XS05";
public final static String FC_IS_LOCAL = "XS06";
public final static String FC_AUDITLVL = "XS07";
public final static String FC_CHPTS = "XS08";
public final static String FC_DESCR = "XS09";

// SQL requests
public final static String TISQL_LIST =
	"select * from SAM.V_Scope order by sid,id";
public final static String TISQL_LOAD =
	"select * from SAM.V_Scope where id = ?";
public final static String TISQL_CREATE =
	"select * from SAM.create_Scope(?,?,?,?,?,?,?,?)";
public final static String TISQL_UPDATE =
	"select * from SAM.update_Scope(?,?,?,?,?,?,?,?)";
public final static String TISQL_DELETE =
	"select * from SAM.delete_Scope(?)";

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 id = getRSLong(rs,1);
 sid = getRSLong(rs,2);
 slevel_min = getRSShort(rs,3);
 slevel_max = getRSShort(rs,4);
 name = getRSString(rs,5);
 is_local = getRSYesNo(rs,6);
 auditlvl = rtrimString(getRSString(rs,7));
 chpts = getRSDateTime(rs,8);
 descr = getRSString(rs,9);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 id = getFSLong(fs,F_ID);
 sid = getFSLong(fs,F_SID);
 slevel_min = getFSShort(fs,F_SLEVEL_MIN);
 slevel_max = getFSShort(fs,F_SLEVEL_MAX);
 name = getFSString(fs,F_NAME);
 is_local = getFSYesNo(fs,F_IS_LOCAL);
 auditlvl = getFSString(fs,F_AUDITLVL);
 chpts = getFSDateTime(fs,F_CHPTS);
 descr = getFSString(fs,F_DESCR);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSLong(ps,1,id);
 setPSLong(ps,2,sid);
 setPSShort(ps,3,slevel_min);
 setPSShort(ps,4,slevel_max);
 setPSString(ps,5,name);
 setPSYesNo(ps,6,is_local);
 setPSString(ps,7,auditlvl);
 //setPSDateTime(ps,8,chpts);
 setPSString(ps,8,descr);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = new ZFieldSet();
 fs.add(new ZField(F_ID,id,FC_ID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SID,sid,FC_SID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SLEVEL_MIN,slevel_min,FC_SLEVEL_MIN,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SLEVEL_MAX,slevel_max,FC_SLEVEL_MAX,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_NAME,name,FC_NAME,ZField.T_UNKNOWN,64));
 fs.add(new ZField(F_IS_LOCAL,is_local,FC_IS_LOCAL,ZField.T_UNKNOWN,1));
 fs.add(new ZField(F_AUDITLVL,auditlvl,FC_AUDITLVL,ZField.T_UNKNOWN,1));
 fs.add(new ZField(F_CHPTS,chpts,FC_CHPTS,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_DESCR,descr,FC_DESCR,ZField.T_UNKNOWN,255));
 return(fs);
} //

// constructors
public Scope() { }
public Scope(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public Scope(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 this.getFromRS(rs);
} // Scope(java.sql.ResultSet rs)

} //Scope
