// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao.SAM;

import ru.mave.ConcepTIS.dao.*;

/** .
 *
 */
public class UserSession extends Record
{
public Long	uid;
public Long	usi;
public DateTime	datefrom;
public DateTime	dateto;
public String	secretcookie;
public String	servicefrom;
public String	devicefrom;

public Long getID(){return(null);}
public String getKey(){return(null);}
public String getCaption(){return(getKey());}

// Strings

public final static String F_UID = "uid";
public final static String F_USI = "usi";
public final static String F_DATEFROM = "datefrom";
public final static String F_DATETO = "dateto";
public final static String F_SECRETCOOKIE = "secretcookie";
public final static String F_SERVICEFROM = "servicefrom";
public final static String F_DEVICEFROM = "devicefrom";

public final static String FC_UID = "XUS01";
public final static String FC_USI = "XUS02";
public final static String FC_DATEFROM = "XUS03";
public final static String FC_DATETO = "XUS04";
public final static String FC_SECRETCOOKIE = "XUS05";
public final static String FC_SERVICEFROM = "XUS06";
public final static String FC_DEVICEFROM = "XUS07";

// SQL requests
public final static String TISQL_LIST =
	"select * from SAM.V_UserSession";
public final static String TISQL_LOAD =
	"select * from SAM.V_UserSession where ? = null";
public final static String TISQL_CREATE =
	"select * from SAM.create_UserSession(?,?,?,?,?,?,?)";
public final static String TISQL_UPDATE =
	"select * from SAM.update_UserSession(?,?,?,?,?,?,?)";
public final static String TISQL_DELETE =
	"select * from SAM.delete_UserSession(?)";

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 uid = getRSLong(rs,1);
 usi = getRSLong(rs,2);
 datefrom = getRSDateTime(rs,3);
 dateto = getRSDateTime(rs,4);
 secretcookie = getRSString(rs,5);
 servicefrom = getRSString(rs,6);
 devicefrom = getRSString(rs,7);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 uid = getFSLong(fs,F_UID);
 usi = getFSLong(fs,F_USI);
 datefrom = getFSDateTime(fs,F_DATEFROM);
 dateto = getFSDateTime(fs,F_DATETO);
 secretcookie = getFSString(fs,F_SECRETCOOKIE);
 servicefrom = getFSString(fs,F_SERVICEFROM);
 devicefrom = getFSString(fs,F_DEVICEFROM);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSLong(ps,1,uid);
 setPSLong(ps,2,usi);
 setPSDateTime(ps,3,datefrom);
 setPSDateTime(ps,4,dateto);
 setPSString(ps,5,secretcookie);
 setPSString(ps,6,servicefrom);
 setPSString(ps,7,devicefrom);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = new ZFieldSet();
 fs.add(new ZField(F_UID,uid,FC_UID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_USI,usi,FC_USI,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_DATEFROM,datefrom,FC_DATEFROM,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_DATETO,dateto,FC_DATETO,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SECRETCOOKIE,secretcookie,FC_SECRETCOOKIE,ZField.T_UNKNOWN,127));
 fs.add(new ZField(F_SERVICEFROM,servicefrom,FC_SERVICEFROM,ZField.T_UNKNOWN,63));
 fs.add(new ZField(F_DEVICEFROM,devicefrom,FC_DEVICEFROM,ZField.T_UNKNOWN,127));
 return(fs);
} //

// constructors
public UserSession() { }
public UserSession(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public UserSession(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 this.getFromRS(rs);
} // UserSession(java.sql.ResultSet rs)

} //UserSession
