// ConcepTIS project
// (c) Alex V Eustrop 2009-2010
// see LICENSE at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao.SAM;

import ru.mave.ConcepTIS.dao.*;

/** .
 *
 */
public class UserCapability extends Record
{
public Long	uid;
public Long	sid;
public String	capcode;

public Long getID(){return(null);}
public String getKey(){return(null);}
public String getCaption(){return(getKey());}

// Strings

public final static String F_UID = "uid";
public final static String F_SID = "sid";
public final static String F_CAPCODE = "capcode";

public final static String FC_UID = "XUC01";
public final static String FC_SID = "XUC02";
public final static String FC_CAPCODE = "XUC03";

// SQL requests
public final static String TISQL_LIST =
	"select * from SAM.V_UserCapability";
public final static String TISQL_LOAD =
	"select * from SAM.V_UserCapability where ? = null";
public final static String TISQL_CREATE =
	"select * from SAM.create_UserCapability(?,?,?)";
public final static String TISQL_UPDATE =
	"select * from SAM.update_UserCapability(?,?,?)";
public final static String TISQL_DELETE =
	"select * from SAM.delete_UserCapability(?)";

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 uid = getRSLong(rs,1);
 sid = getRSLong(rs,2);
 capcode = rtrimString(getRSString(rs,3));
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 uid = getFSLong(fs,F_UID);
 sid = getFSLong(fs,F_SID);
 capcode = getFSString(fs,F_CAPCODE);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSLong(ps,1,uid);
 setPSLong(ps,2,sid);
 setPSString(ps,3,capcode);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = new ZFieldSet();
 fs.add(new ZField(F_UID,uid,FC_UID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SID,sid,FC_SID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_CAPCODE,capcode,FC_CAPCODE,ZField.T_UNKNOWN,16));
 return(fs);
} //

// constructors
public UserCapability() { }
public UserCapability(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public UserCapability(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 this.getFromRS(rs);
} // UserCapability(java.sql.ResultSet rs)

} //UserCapability
