// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao.SAM;

import ru.mave.ConcepTIS.dao.*;

/** .
 *
 */
public class RangeSeq extends Record
{
public Long	start;
public Long	rend;
public Long	lastid;
public Long	prange;
public Short	pbitl;
public Long	sid;
public String	qtype;
public Short	sorder;
public DateTime	ts;

public Long getID(){return(null);}
public String getKey(){return(null);}
public String getCaption(){return(getKey());}

// Strings

public final static String F_START = "start";
public final static String F_REND = "rend";
public final static String F_LASTID = "lastid";
public final static String F_PRANGE = "prange";
public final static String F_PBITL = "pbitl";
public final static String F_SID = "sid";
public final static String F_QTYPE = "qtype";
public final static String F_SORDER = "sorder";
public final static String F_TS = "ts";

public final static String FC_START = "XRS01";
public final static String FC_REND = "XRS02";
public final static String FC_LASTID = "XRS03";
public final static String FC_PRANGE = "XRS04";
public final static String FC_PBITL = "XRS05";
public final static String FC_SID = "XRS06";
public final static String FC_QTYPE = "XRS07";
public final static String FC_SORDER = "XRS09";
public final static String FC_TS = "XRS10";

// SQL requests
public final static String TISQL_LIST =
	"select * from SAM.V_RangeSeq";
public final static String TISQL_LOAD =
	"select * from SAM.V_RangeSeq where ? = null";
public final static String TISQL_CREATE =
	"select * from SAM.create_RangeSeq(?,?,?,?,?,?,?,?,?)";
public final static String TISQL_UPDATE =
	"select * from SAM.update_RangeSeq(?,?,?,?,?,?,?,?,?)";
public final static String TISQL_DELETE =
	"select * from SAM.delete_RangeSeq(?)";

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 start = getRSLong(rs,1);
 rend = getRSLong(rs,2);
 lastid = getRSLong(rs,3);
 prange = getRSLong(rs,4);
 pbitl = getRSShort(rs,5);
 sid = getRSLong(rs,6);
 qtype = getRSString(rs,7);
 sorder = getRSShort(rs,8);
 ts = getRSDateTime(rs,9);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 start = getFSLong(fs,F_START);
 rend = getFSLong(fs,F_REND);
 lastid = getFSLong(fs,F_LASTID);
 prange = getFSLong(fs,F_PRANGE);
 pbitl = getFSShort(fs,F_PBITL);
 sid = getFSLong(fs,F_SID);
 qtype = getFSString(fs,F_QTYPE);
 sorder = getFSShort(fs,F_SORDER);
 ts = getFSDateTime(fs,F_TS);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSLong(ps,1,start);
 setPSLong(ps,2,rend);
 setPSLong(ps,3,lastid);
 setPSLong(ps,4,prange);
 setPSShort(ps,5,pbitl);
 setPSLong(ps,6,sid);
 setPSString(ps,7,qtype);
 setPSShort(ps,8,sorder);
 setPSDateTime(ps,9,ts);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = new ZFieldSet();
 fs.add(new ZField(F_START,start,FC_START,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_REND,rend,FC_REND,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_LASTID,lastid,FC_LASTID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_PRANGE,prange,FC_PRANGE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_PBITL,pbitl,FC_PBITL,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SID,sid,FC_SID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_QTYPE,qtype,FC_QTYPE,ZField.T_UNKNOWN,14));
 fs.add(new ZField(F_SORDER,sorder,FC_SORDER,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_TS,ts,FC_TS,ZField.T_UNKNOWN,0));
 return(fs);
} //

// constructors
public RangeSeq() { }
public RangeSeq(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public RangeSeq(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 this.getFromRS(rs);
} // RangeSeq(java.sql.ResultSet rs)

} //RangeSeq
