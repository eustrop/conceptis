// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao.SAM;

import ru.mave.ConcepTIS.dao.*;

/** .
 *
 */
public class ACLScope extends Record
{
public Long	gid;
public Long	sid;
public String	obj_type;
public String	writea;
public String	createa;
public String	deletea;
public String	reada;
public String	locka;
public String	historya;

public Long getID(){return(null);}
public String getKey(){return(gid+":"+sid+":"+obj_type);}
public String getCaption(){
return(getKey()+":"+writea+createa+deletea+reada+locka+historya);}

// Strings

public final static String F_GID = "gid";
public final static String F_SID = "sid";
public final static String F_OBJ_TYPE = "obj_type";
public final static String F_WRITEA = "writea";
public final static String F_CREATEA = "createa";
public final static String F_DELETEA = "deletea";
public final static String F_READA = "reada";
public final static String F_LOCKA = "locka";
public final static String F_HISTORYA = "historya";

public final static String FC_GID = "XAS01";
public final static String FC_SID = "XAS02";
public final static String FC_OBJ_TYPE = "XAS03";
public final static String FC_WRITEA = "XAS04";
public final static String FC_CREATEA = "XAS05";
public final static String FC_DELETEA = "XAS06";
public final static String FC_READA = "XAS07";
public final static String FC_LOCKA = "XAS08";
public final static String FC_HISTORYA = "XAS09";

// SQL requests
public final static String TISQL_LIST =
	"select * from SAM.V_ACLScope";
public final static String TISQL_LOAD =
	"select * from SAM.V_ACLScope where ? = null";
public final static String TISQL_CREATE =
	"select * from SAM.create_ACLScope(?,?,?,?,?,?,?,?,?)";
public final static String TISQL_UPDATE =
	"select * from SAM.update_ACLScope(?,?,?,?,?,?,?,?,?)";
public final static String TISQL_DELETE =
	"select * from SAM.delete_ACLScope(?)";

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 gid = getRSLong(rs,1);
 sid = getRSLong(rs,2);
 obj_type = rtrimString(getRSString(rs,3));
 writea = rtrimString(getRSString(rs,4));
 createa = rtrimString(getRSString(rs,5));
 deletea = rtrimString(getRSString(rs,6));
 reada = rtrimString(getRSString(rs,7));
 locka = rtrimString(getRSString(rs,8));
 historya = rtrimString(getRSString(rs,9));
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 gid = getFSLong(fs,F_GID);
 sid = getFSLong(fs,F_SID);
 obj_type = getFSString(fs,F_OBJ_TYPE);
 writea = getFSString(fs,F_WRITEA);
 createa = getFSString(fs,F_CREATEA);
 deletea = getFSString(fs,F_DELETEA);
 reada = getFSString(fs,F_READA);
 locka = getFSString(fs,F_LOCKA);
 historya = getFSString(fs,F_HISTORYA);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSLong(ps,1,gid);
 setPSLong(ps,2,sid);
 setPSString(ps,3,obj_type);
 setPSString(ps,4,writea);
 setPSString(ps,5,createa);
 setPSString(ps,6,deletea);
 setPSString(ps,7,reada);
 setPSString(ps,8,locka);
 setPSString(ps,9,historya);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = new ZFieldSet();
 fs.add(new ZField(F_GID,gid,FC_GID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SID,sid,FC_SID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_OBJ_TYPE,obj_type,FC_OBJ_TYPE,ZField.T_UNKNOWN,20));
 fs.add(new ZField(F_WRITEA,writea,FC_WRITEA,ZField.T_UNKNOWN,1));
 fs.add(new ZField(F_CREATEA,createa,FC_CREATEA,ZField.T_UNKNOWN,1));
 fs.add(new ZField(F_DELETEA,deletea,FC_DELETEA,ZField.T_UNKNOWN,1));
 fs.add(new ZField(F_READA,reada,FC_READA,ZField.T_UNKNOWN,1));
 fs.add(new ZField(F_LOCKA,locka,FC_LOCKA,ZField.T_UNKNOWN,1));
 fs.add(new ZField(F_HISTORYA,historya,FC_HISTORYA,ZField.T_UNKNOWN,1));
 return(fs);
} //

// constructors
public ACLScope() { }
public ACLScope(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public ACLScope(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 this.getFromRS(rs);
} // ACLScope(java.sql.ResultSet rs)

} //ACLScope
