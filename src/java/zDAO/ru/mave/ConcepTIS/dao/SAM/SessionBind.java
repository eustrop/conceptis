// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao.SAM;

import ru.mave.ConcepTIS.dao.*;

/** .
 *
 */
public class SessionBind extends Record
{
public Integer	pg_pid;
public String	db_user;
public Long	uid;
public Long	usi;
public Short	slevel;
public YesNo	locked;
public DateTime	datefrom;
public DateTime	dateto;
public DateTime	pg_s_time;
public String	cl_addr;
public Integer	cl_port;

public Long getID(){return(null);}
public String getKey(){return(null);}
public String getCaption(){return(getKey());}

// Strings

public final static String F_PG_PID = "pg_pid";
public final static String F_DB_USER = "db_user";
public final static String F_UID = "uid";
public final static String F_USI = "usi";
public final static String F_SLEVEL = "slevel";
public final static String F_LOCKED = "locked";
public final static String F_DATEFROM = "datefrom";
public final static String F_DATETO = "dateto";
public final static String F_PG_S_TIME = "pg_s_time";
public final static String F_CL_ADDR = "cl_addr";
public final static String F_CL_PORT = "cl_port";

public final static String FC_PG_PID = "XB01";
public final static String FC_DB_USER = "XB02";
public final static String FC_UID = "XB03";
public final static String FC_USI = "XB04";
public final static String FC_SLEVEL = "XB05";
public final static String FC_LOCKED = "XB06";
public final static String FC_DATEFROM = "XB07";
public final static String FC_DATETO = "XB08";
public final static String FC_PG_S_TIME = "XB09";
public final static String FC_CL_ADDR = "XB10";
public final static String FC_CL_PORT = "XB11";

// SQL requests
public final static String TISQL_LIST =
	"select * from SAM.V_SessionBind";
public final static String TISQL_LOAD =
	"select * from SAM.V_SessionBind where ? = null";
public final static String TISQL_CREATE =
	"select * from SAM.create_SessionBind(?,?,?,?,?,?,?,?,?,?,?)";
public final static String TISQL_UPDATE =
	"select * from SAM.update_SessionBind(?,?,?,?,?,?,?,?,?,?,?)";
public final static String TISQL_DELETE =
	"select * from SAM.delete_SessionBind(?)";

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 pg_pid = getRSInteger(rs,1);
 db_user = getRSString(rs,2);
 uid = getRSLong(rs,3);
 usi = getRSLong(rs,4);
 slevel = getRSShort(rs,5);
 locked = getRSYesNo(rs,6);
 datefrom = getRSDateTime(rs,7);
 dateto = getRSDateTime(rs,8);
 pg_s_time = getRSDateTime(rs,9);
 cl_addr = getRSString(rs,10);
 cl_port = getRSInteger(rs,11);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 pg_pid = getFSInteger(fs,F_PG_PID);
 db_user = getFSString(fs,F_DB_USER);
 uid = getFSLong(fs,F_UID);
 usi = getFSLong(fs,F_USI);
 slevel = getFSShort(fs,F_SLEVEL);
 locked = getFSYesNo(fs,F_LOCKED);
 datefrom = getFSDateTime(fs,F_DATEFROM);
 dateto = getFSDateTime(fs,F_DATETO);
 pg_s_time = getFSDateTime(fs,F_PG_S_TIME);
 cl_addr = getFSString(fs,F_CL_ADDR);
 cl_port = getFSInteger(fs,F_CL_PORT);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSInteger(ps,1,pg_pid);
 setPSString(ps,2,db_user);
 setPSLong(ps,3,uid);
 setPSLong(ps,4,usi);
 setPSShort(ps,5,slevel);
 setPSYesNo(ps,6,locked);
 setPSDateTime(ps,7,datefrom);
 setPSDateTime(ps,8,dateto);
 setPSDateTime(ps,9,pg_s_time);
 setPSString(ps,10,cl_addr);
 setPSInteger(ps,11,cl_port);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = new ZFieldSet();
 fs.add(new ZField(F_PG_PID,pg_pid,FC_PG_PID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_DB_USER,db_user,FC_DB_USER,ZField.T_UNKNOWN,63));
 fs.add(new ZField(F_UID,uid,FC_UID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_USI,usi,FC_USI,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SLEVEL,slevel,FC_SLEVEL,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_LOCKED,locked,FC_LOCKED,ZField.T_UNKNOWN,1));
 fs.add(new ZField(F_DATEFROM,datefrom,FC_DATEFROM,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_DATETO,dateto,FC_DATETO,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_PG_S_TIME,pg_s_time,FC_PG_S_TIME,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_CL_ADDR,cl_addr,FC_CL_ADDR,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_CL_PORT,cl_port,FC_CL_PORT,ZField.T_UNKNOWN,0));
 return(fs);
} //

// constructors
public SessionBind() { }
public SessionBind(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public SessionBind(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 this.getFromRS(rs);
} // SessionBind(java.sql.ResultSet rs)

} //SessionBind
