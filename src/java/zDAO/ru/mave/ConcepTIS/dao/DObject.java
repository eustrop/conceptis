// ConcepTIS project
// (c) Alex V Eustrop 2009-2011
// see LICENSE at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao;
import ru.mave.ConcepTIS.dao.TIS.*;
import java.util.*;

/** Generic class for handling any TIS-SQL "Data Object".
 * Actual type of dobject support implemented with help of some
 * DOTProcessor subclass instance (Area,Container,Document etc)
 *
 * @see DOTProcessor
 * @see #setDOTProcessor()
 * @see #getDOTProcessor()
 */
public class DObject extends Record
{

public final static String C_ZSTA_I = "I";
public final static String C_ZSTA_N = "N";
public final static String C_ZSTA_D = "D";
public final static String C_ZSTA_L = "L";
public final static String C_ZSTA_C = "C";

/** true if ZSTA is one of intermediate states (I,L).
 * intermediate versions of ZObjects can be seen via VI_* only.
 */
public static final boolean is_VL_ZSTA(String ZSTA)
{
 if(C_ZSTA_I.equals(ZSTA)) return(true);
 if(C_ZSTA_L.equals(ZSTA)) return(true);
 return(false);
}

public static final boolean is_N_ZSTA(String ZSTA)
{
 if(C_ZSTA_N.equals(ZSTA)) return(true);
 return(false);
}

private ZObject zoVersion; // current version we are working with
private String ZNAME;

private RList rlHVersions; // a set of ZObject records for this dobject

private ZObject zoNVersion; // actual version with ZSTA=N
private ZObject zoIVersion; // intermediate version (ZSTA is I or L)
private ZObject zoLVersion; // locking version (ZSTA is I or L)
private DOTProcessor dot_proc; // define behaviour of type-specific methods

/** set behavior of this instance to specified type of data object.
 * This is important for loading/saving of dobjects' data.
 *
 * @see #makeCaption
 */
public void setDOTProcessor(DOTProcessor o)
{dot_proc=o;} // interesting code lost, see ver DObject.java,v 1.20 2011/08/23

public DOTProcessor getDOTProcessor()
 {return(dot_proc);}

// Record class abstract methods implementation
public Long getID(){return(getZOID());}
public String getKey(){return(getZTYPE()+getZOID());}

/** getZNAME() by default, but can be overridden via DOTProcessor.
 * null is recomended for unnamed dobjects, since visual interface
 * implements advanced support for null captions in
 * WASkin.mkHRefTargetZID() (2011/08/23)
 * @see #setDOTProcessor
 */
public final String getCaption(){
 String c = null;
 if(dot_proc != null) c = dot_proc.makeCaption(this);
 if(c == null ) c = make_ZNAME_caption(this);
 return(c);
}

/** make caption from dobject's ZNAME. */
public static String make_ZNAME_caption(DObject o)
{ String c = o.getZNAME(); /*if(c == null) c=o.getKey();*/ return(c); }
//no getKey() used since 2011/08/23.

/** make caption from dobject's header record located in the table with TabCode.
 * In most cases dobject has so called "header record" and its caption is dobject's
 * caption. If more than single record in getTable(TabCode) found - the earlest
 * will used. Only non-deleted records used, if no such - method returns null.
 */
public static String make_headrecord_caption(DObject o, String TabCode)
{
 String caption=null;
 RList rl = o.getTable(TabCode);
 if(rl == null) return(null);
 for(int i=0;i<rl.size();i++)
 {
  DORecord dor = (DORecord)rl.get(i);
  if(!dor.isDeleted()){ caption=dor.getCaption(); break; }
 }
 return(caption);
} //make_headrecord_caption()

// DObject's own methods

/** ZOID of this. */
public Long getZOID() {return(zoVersion.ZOID);}
public void setZOID(Long v) {zoVersion.ZOID=v;}

/** number of DB dobject's version this java object points to and works with. */
public Long getZVER() {return(zoVersion.ZVER);}
public void setZVER(Long v) {zoVersion.ZVER=v;}

public String getZTYPE() {return(zoVersion.ZTYPE);}
public void setZTYPE(String v) {zoVersion.ZTYPE=v;}

public Long getZSID() {return(zoVersion.ZSID);}
public void setZSID(Long v) {zoVersion.ZSID=v;}

public String getZSTA() {return(zoVersion.ZSTA);}
public void setZSTA(String v) {zoVersion.ZSTA=v;}

public Short getZLVL() {return(zoVersion.ZLVL);}
public void setZLVL(Short v) {zoVersion.ZLVL=v;}

public Long getZUID() {return(zoVersion.ZUID);}
public void setZUID(Long v) {zoVersion.ZUID=v;}

public DateTime getZDATE() {return(zoVersion.ZDATE);}
public void setZDATE(DateTime v) {zoVersion.ZDATE=v;}

public DateTime getZDATO() {return(zoVersion.ZDATO);}
public void setZDATO(DateTime v) {zoVersion.ZDATO=v;}

public Long getQRSQ() {return(zoVersion.QRSQ);}
public void setQRSQ(Long v) {zoVersion.QRSQ=v;}

/** current ZNAME of this dobject. */
public String getZNAME() {return(ZNAME);}
public void setZNAME(String v) {ZNAME=v;}

//
// collection of DORecords and methods for them
//

/** collection of RList objects with all records of specified type.
 * one RList per each table
 */
private Hashtable htTables = new Hashtable();
/** collection of RList objects with top-level or orphan records.
*  one RList per each table
*/
private Hashtable htTablesOfParents = new Hashtable();

/** get collection of records (table) for specified tabcode. */
public RList getTable(String tabcode)
{
 return((RList)htTables.get(tabcode));
}

/** get collection (table, set) of parent (or orphan) records (table)
 * for specified tabcode. this collection is the result of prior
 * organizeTree() call.
 * @see organizeTree()
 */
public RList getTableOfParents(String tabcode)
{
 RList rl = (RList)htTablesOfParents.get(tabcode);
 if(rl == null)  rl = (RList)htTables.get(tabcode);
 return(rl);
}

/** create&return new empty collection for tabcode type of records. */
public RList createTable(String tabcode)
{
 RList rl = new RList();
 htTables.put(tabcode,rl);
 return(rl);
}

/** create&return new empty collection for tabcode type of records. */
public RList createTableOfParents(String tabcode)
{
 RList rl = new RList();
 htTablesOfParents.put(tabcode,rl);
 return(rl);
}

/** return enumeration of existing tabcodes. */
public Enumeration enumTabCodes()
{
return(htTables.keys());
}

/** (semi-abstract) returns array of newly created DORecord objects
 * one per each table which is member or this dobject-type.
 * setDOTProcessor() must be used to utilize this functionality
 * of particular dobject type.  return(new DORecord[0]); otherwise
 *
 * @see #setDOTProcessor
 */
public DORecord[] createMembersFactory()
{
 if(dot_proc!=null)return(dot_proc.createMembersFactory());
 return(new DORecord[0]);
}

public void organize(){organizeTables(); organizeTree();}
public void organizeTree()
 {
  //if(createMembersFactory().length > 1) throw(new RuntimeException());
  DORecord[] fac = createMembersFactory();
  htTablesOfParents = new Hashtable();
  for(int f=0;f<fac.length;f++) // over all factory records (all types)
  {
   RList rl = getTable(fac[f].getTabCode());
   String parent_code = fac[f].getParentTabCode();
   if(rl == null || parent_code == null) continue; //all are parents
   RList rlp = getTable(parent_code);
   if(rlp == null) continue; //all are orphans
   RList rlor = createTableOfParents(fac[f].getTabCode()); //some can be orphans
   for(int i=0;i<rl.size();i++)
   {
    DORecord r = (DORecord)rl.get(i);
    if(r.ZPID == null && r.ZRID == null) continue;
    if(r.ZPID == null){rlor.add(r); continue;} // orphan
    if(r.ZPID.longValue() == 0) {rlor.add(r); continue;} // parent
    DORecord pr = (DORecord)rlp.getByID(r.ZPID);
    if(pr != null) pr.addChild(r);
    else {rlor.add(r); continue;} // orphan
   } // for over rl
  } // for over fac
 } //organizeTree()

/** update internal tables of records using DObject's hierarchical tree.
 * all records found over tree will be appended to internal TabCode-specific
 * tables available via getTable(), if they are not in.
 *
 * @see getTable()
 * @see getTableOfParents()
 */
public void organizeTables()
{
 DORecord[] fac = createMembersFactory();
 int f,i,k; // iterators
  //if(createMembersFactory().length <= 1) return;
  //if(createMembersFactory().length > 1) throw(new RuntimeException());
  //DORecord[] f=createMembersFactory();
  for(f=0;f<fac.length;f++) // over all factory records (all types)
  {
   RList rl = getTable(fac[f].getTabCode());
   if(rl == null) continue;
   for(i=0;i<rl.size();i++)
   {
    DORecord pr; // parent record
    pr = (DORecord)rl.get(i);
    for(k=0;k<pr.countChildren();k++)
    {
     DORecord chr=(DORecord)pr.getChild(k); // child record
     try{addRecord(chr);} // table is RList, so it's a kind of java.util.Set, so no dups allowed
     catch(IllegalArgumentException iae){} // ignore if this record already in
    } //for(k=0;k<pr.countChildren();
   } //for(;i<rl.size()
  } // for(;f<fac.length
} //organizeTables

/** import records and deletions from o which is collection
* of this dobject's records gotten from VI_ only.
*/
public void importIntermediate(DObject o)
{
DORecord[] factory = createMembersFactory();
int i,k;
Enumeration enum_o_tabs = o.enumTabCodes();
 while(enum_o_tabs.hasMoreElements())
  //for(i=0;i<factory.length;i++)
  {
   RList rl = o.getTable((String)enum_o_tabs.nextElement());
   if(rl==null) continue;
   k=0; while(k<rl.size())
   {
    DORecord r = (DORecord)rl.get(k),r_my = null;
    long r_ZTOV = 0;
    if(r.ZTOV != null)  r_ZTOV = r.ZTOV.longValue();
    if(DORecord.ZC_ZTOV_DELETED==r_ZTOV ||
	DORecord.ZC_ZTOV_RDELETED == r_ZTOV )
    {
      r_my = deleteRecord(r.getTabCode(),r.getID());
      r_my.commitDeletion();
    }
    else if (DORecord.ZC_ZTOV_NOCHANGES == r_ZTOV ) {}
    else if (DORecord.ZC_ZTOV_UPDATED == r_ZTOV )
    {
     if(r.getID()!=null) r_my = getRecord(r.getTabCode(),r.getID());
     if(r_my == null) addRecord(r);
     else r_my.importData(r);
    }
    k++;
   } // while rl
  }
  removeCommittedDeletions();
  //this.zoIVersion = new ZObject(o.getIVersion());
  this.setZVER(zoIVersion.ZVER);
  this.setZSTA(zoIVersion.ZSTA);
  organizeTree();
} //importIntermediate


/** mark specified record for deletion. */
public DORecord deleteRecord(String tabcode, Long ZRID)
{
RList rl = getTable(tabcode);
if(rl == null) return(null);
DORecord r = (DORecord)rl.getByID(ZRID);
if(r != null){ r.delete(); }
return(r);
}

/** get record by it's unique indobject key. */
public DORecord getRecord(String tabcode, Long ZRID)
{
RList rl = getTable(tabcode);
if(rl == null) return(null);
DORecord r = (DORecord)rl.getByID(ZRID);
return(r);
}

/** add new record to this dobject.
* Record will appended to appropriate table rely on rec.getTabCode().
*/
public void addRecord(DORecord rec)
{
RList rl = getTable(rec.getTabCode());
if(rl == null) rl = createTable(rec.getTabCode());
rl.add(rec);
}

public void removeCommittedDeletions()
{
DORecord[] factory = createMembersFactory();
int i,k;
  for(i=0;i<factory.length;i++)
  {
   RList rl = getTable(factory[i].getTabCode());
   if(rl==null) continue;
   k=0; while(k<rl.size())
   {
    DORecord r = (DORecord)rl.get(k);
    if(r.isCommittedDeletion()) {rl.remove(k); continue; }
    else {} //r.removeCommittedDeletions();
    k++;
   } // while rl
  }
  organizeTree();
} //

//
// Version data manipulation methods
//

/** the set of records from VH_ZObject related to this dobject.
 * null or incomplete list are possible.
 */
public RList getHVersions() { return(rlHVersions); }
public void setHVersions(RList v) { rlHVersions=v; }

/* latest version of dobject with ZSTA (or just latest, if ZSTA=null).*/
public ZObject findLatestVersion(String ZSTA)
{
  RList rl = getHVersions();
  Long latestZVER = null; 
  ZObject ZO, latestZO = null;
  int i;
  if(rl == null) rl = new RList();
  for(i=0; i < rl.size(); i++){
   ZO=(ZObject)rl.get(i);
   if(ZSTA != null) if(!ZSTA.equals(ZO.ZSTA)) continue;
   if(latestZVER==null) {latestZO=ZO; latestZVER=ZO.ZVER;}
   else if (ZO.ZVER==null) continue;
   else if (latestZVER.compareTo(ZO.ZVER)<0) {latestZO=ZO; latestZVER=ZO.ZVER;}
  } // for
  return(latestZO);
} // findLatestVersion(String ZSTA)

public ZObject findLatestVersion() { return(findLatestVersion(null)); }

/* latest version of dobject with passed ZSTA (or just latest if ZSTA=null). */
public Long getLatestZVER(String ZSTA)
{
  ZObject ZO=findLatestVersion(ZSTA);
  if(ZO!=null) return(ZO.ZVER);
  return(null);
}
public Long getLatestZVER(){return(getLatestZVER(null));}

/** working version this java object points to. */
public ZObject getVersion(){ return(zoVersion); }

/** most actual version record of dobject. */
public ZObject getMostActualVersion()
{
 ZObject o = getIVersion();
 if(o==null) o = getNVersion();
 if(o==null) o = findLatestVersion();
 if(o==null) o = getLVersion(); //do not import ZVER data
 return(o);
}

/** true if found ZTYPE equals to preset one (or not preset, or no data found). */
public boolean checkFoundZTYPE()
{
String preset_ZTYPE = getZTYPE();
if(preset_ZTYPE == null) return(true);
ZObject zo = getMostActualVersion();
if(zo==null)return(true);
return(preset_ZTYPE.equals(zo.ZTYPE));
}

/** setZTYPE() to ZTYPE of getMostActualVersion(). */
public void fixFoundZTYPE()
{
ZObject zo = getMostActualVersion();
if(zo!=null) setZTYPE(zo.ZTYPE);
}

/** import missed data from IVersion or NVersion or just from
 * any latest HVersion of target dobject.
 *
 * @see getVersion()
 */
public void importMissedVersionData()
{
 boolean importZVER=false;
 ZObject o = getIVersion();
 if(o==null) o = getNVersion();
 if(o==null) o = findLatestVersion();
 if(o==null) o = getLVersion(); //do not import ZVER data
 else if(getZVER() == null) importZVER = true;
 if(o==null) return;
 if(getZTYPE() == null) setZTYPE(o.ZTYPE); 
 if(getZVER() != null ) if(getZVER().equals(o.ZVER)) importZVER = true;
 if(importZVER)
 { 
  setZVER(o.ZVER);
  setZSTA(o.ZSTA);
  setZUID(o.ZUID);
  setZSID(o.ZSID);
  setZLVL(o.ZLVL);
  setZDATE(o.ZDATE);
  setZDATO(o.ZDATO);
  setQRSQ(o.QRSQ);
 }
} //importMissedVersionData

/** set to null all ZVER related fields of this java object's
 * working version record, so it will point to latest actual
 * version of dobject.
 *
 * @see getVersion()
 */
public void dropVersionZVERData()
{
  setZVER(null);
  setZSTA(null);
  setZUID(null);
  setZLVL(null);
  setZDATE(null);
  setZDATO(null);
  setQRSQ(null); //SIC! to think about
} // dropVersionZVERData

/** true if any ZO record found in the DB. */
public boolean isExists()
{
if(getHVersions() != null)
  if(getHVersions().size() > 0) return(true);
if(getNVersion() != null) return(true);
if(getLVersion() != null) return(true);
if(getIVersion() != null) return(true);
return(false);
} //isExists

/** true if latest ZO record has ZTA = 'D'. */
public boolean isDeleted()
{
if(!isExists()) return(false);
if(findLatestVersion(C_ZSTA_D) != null) return(true);
return(false);
} //isDeleted

/** true if this object in the intermediate state (locked or opened by anyone). */
public boolean isIntermediate()
{
if(getZVER() != null){
 if( C_ZSTA_I.equals(getZSTA()) ) return(true);
 if( C_ZSTA_L.equals(getZSTA()) ) return(true); }
if(getIVersion() != null) return(true);
if(getLVersion() != null) return(true);
return(false);
} //isIntermediate

/** true if intermediate state if dobject available for current user. */
public boolean isMyIntermediate()
{
if(getZVER() != null){
 if( C_ZSTA_I.equals(getZSTA()) ) return(true);
 if( C_ZSTA_L.equals(getZSTA()) ) return(true); }
if(getIVersion() != null) return(true);
return(false);
} //isMyIntermediate

/** true if object is opened by onyone (ZSAT='I'). */
public boolean isOpened()
{
if(getZVER() != null && C_ZSTA_I.equals(getZSTA())) return(true);
ZObject ZO = getIVersion(); if(ZO == null) ZO = getLVersion();
if(ZO != null) if(C_ZSTA_I.equals(ZO.ZSTA)) return(true);
return(false);
} //isOpened

/** true if object is locked by anyone (ZSAT='L'). */
public boolean isLocked()
{
if(getZVER() != null && C_ZSTA_L.equals(getZSTA())) return(true);
ZObject ZO = getIVersion(); if(ZO == null) ZO = getLVersion();
if(ZO != null) if(C_ZSTA_L.equals(ZO.ZSTA)) return(true);
return(false);
} //isLocked

public ZObject getNVersion(){ return(zoNVersion); }
public void setNVersion(ZObject v){ zoNVersion = v; }

/** intermediate version record ZO from VI_ZObject.
 * this kind of intermediate version available only
 * for its owner. use getLVersion() to view locks 
 * from other users.
 * @see #getLVersion
 */
public ZObject getIVersion(){ return(zoIVersion); }
public void setIVersion(ZObject v){ zoIVersion = v; }

/** intermediate version ZO from VL_ZObject.
 * this kind of data available for any user able to
 * read current state of dobject (unlike getIVersion).
 * @see #getIVersion
 */
public ZObject getLVersion(){ return(zoLVersion); }
public void setLVersion(ZObject v){ zoLVersion = v; }


/** export DObject to ZFieldSet hierarchical textual container.
* this method uses getTabParents() to obtain top-level records of
* hierarchy so it' suitable for any typical DObject (e.g. you aren't
* need to redefine).
*
* @see getFromFS()
*/
public ZFieldSet toFieldSet()
{
 int i;
 String key;
 //ZFieldSet fs = new ZFieldSet();
 ZFieldSet fs = getVersion().toFieldSet();
 fs.setCode(getZTYPE());
 /*
 fs.add(new ZField(ZObject.F_ZTYPE,getZTYPE(),ZObject.FC_ZTYPE,ZField.T_STRING,0));
 fs.add(new ZField(ZObject.F_ZOID,getZOID(),ZObject.FC_ZOID,ZField.T_LONG,0));
 fs.add(new ZField(ZObject.F_ZVER,getZVER(),ZObject.FC_ZVER,ZField.T_LONG,0));
 fs.add(new ZField(ZObject.F_ZSTA,getZSTA(),ZObject.FC_ZSTA,ZField.T_STRING,0));
 fs.add(new ZField(ZObject.F_ZSID,getZSID(),ZObject.FC_ZSID,ZField.T_LONG,0));
 fs.add(new ZField(ZObject.F_ZLVL,getZLVL(),ZObject.FC_ZLVL,ZField.T_LONG,0));
 */
 DORecord[] members = createMembersFactory();
 Vector vec_tab_codes = new Vector(members.length);
 for(i=0;i<members.length;i++){vec_tab_codes.add(members[i].getTabCode());}
 Enumeration e = enumTabCodes();
 while(e.hasMoreElements()) { // add codes of unexpected records
  key = (String)e.nextElement();
  if(!vec_tab_codes.contains(key))vec_tab_codes.add(key);
 }
 e = vec_tab_codes.elements();
 while(e.hasMoreElements())
 {
  key = (String)e.nextElement();
  RList rl = getTableOfParents(key);
  if(rl == null) continue; //be paranoid since members instead of enumTabCodes()
  for(i=0;i<rl.size();i++) {
  fs.add(((DORecord)rl.get(i)).toFieldSet());
  }
 }
 return(fs);
} //

/** import DObject's data from fs textual container.
* @see toFieldSet()
* @see organize()
*/
public void getFromFS(ZFieldSet fs) throws ZException
{
 Hashtable htf = new Hashtable();
 DORecord factory,r;
 DORecord[] a;
 String key;
 int i;
 test_do_type_code(fs.getCode());
 zoVersion = new ZObject(fs);
 // prepare factory
 a=createMembersFactory();
 for(i=0;i<a.length;i++){htf.put(a[i].getTabCode(),a[i]);}
 // process top-level records (fieldsets)
 i=0;
 while(i<fs.countFS())
 {
  ZFieldSet fsr=fs.getFS(i++); //get&step fwd
  factory=(DORecord)htf.get(fsr.getCode());
  if(factory == null) continue; // or throw ZException()?
  addRecord(factory.createFromFS(fsr));
 }
 // organize tables
 organize();
} //getFromFS(ZFieldSet fs)

/** throws ZExcetion if code not equals getTabCode(). */
private void test_do_type_code(String code) throws ZException
{
 String this_code = getZTYPE();
 DOTProcessor p = getDOTProcessor();
 if(p!=null) this_code = p.getDOTypeCode();
 if(this_code == null) return;
 if(this_code.equals(code)) return;
 ZMessages zm = new ZMessages();
  throw(new ZException(
    zm.getMsg(zm.ERR_INVALID_CODE_X_REQUIRED_Y,code,this_code)));
}
 
// Constructors
public DObject() {zoVersion= new ZObject(); }
public DObject(Long ZOID) { this(null,ZOID); }
public DObject(String ZTYPE) {this(ZTYPE,null,null);}
public DObject(String ZTYPE, Long ZOID) {this(ZTYPE,ZOID,null);}
public DObject(String ZTYPE, Long ZOID, Long ZVER)
 {this(); setZTYPE(ZTYPE); setZOID(ZOID); setZVER(ZVER); }
public DObject(ZObject ZO) { zoVersion=ZO; }
public DObject(ZFieldSet fs) throws ZException {getFromFS(fs);}
public DObject(DOTProcessor dot)
 {this();if(dot!=null){ setDOTProcessor(dot); setZTYPE(dot.getDOTypeCode()); }}
} //DObject
