// ConcepTIS project
// (c) Alex V Eustrop 2009-2010
// see LICENSE at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao;

/** single dictionary code record with code and value.
 *
 */
public class DCode extends Record
{

protected String code;
protected String value;
protected String descr;

/** index field for dictionary. */
public String getCode() {return(code);}
/** short but must useful interpretation of code. */
public String getValue() {return(value);}
/** long description of code value. */
public String getDescription() {return(descr);}
/** this is on the fly constructed value addressed
 *  to visual user interface.
 */
public String getCaption()
 { return (getCode() + " - " + getValue());}

// Record abstract methods implementation
public Long getID(){return(null);}
public String getKey(){return(getCode());}

// constructors
DCode(String code, String value, String descr)
{
 this.code = rtrimString(code); this.value = value; this.descr = descr;
}
DCode(DCode dc)
{
 code = rtrimString(dc.getCode());
 value = dc.getValue(); descr = dc.getDescription();
}

} //DCode
