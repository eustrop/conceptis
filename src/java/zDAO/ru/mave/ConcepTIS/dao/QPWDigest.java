/*
 ConcepTIS project/QTIS project
 (c) Alex V Eustrop & eustrosoft.org 2009,2023
 LICENSE (this file) : BALES, BSD, MIT on your choice (see bales.eustrosoft.org)
 also see LICENSE at the project's root directory for the whole project license

 Purpose: pool of sessions and connections with TIS/SQL-compatible JDBC source
          ( PostgreSQL 13 tested but should be suitable for any DBMS)
 History (psql.jsp & dbpool.jsp):
  2023/04/07 dbpool.jsp started from psql.jsp
  2023/04/25 org.eustrosoft.qdbp package started from 330 lines of dbpool.jsp
  2023/11/04 QPWDigest imported from org.eustrosoft.qdbp.QDBPWDigest
*/
//package org.eustrosoft.qdbp;
package ru.mave.ConcepTIS.dao;
import java.util.*;

// QDBPWDigest - Password Digest class for creation md5/sha256 digest of password compatible with apache tomcat algorithm
// no salt and iterations (yet?). just simple hash equals to: 
//  # /usr/local/apache-tomcat-9.0/bin/digest.sh -s 0 -a md5 test | awk -F: '{print $2}'
// this class was written looking to tomcat 9.0.74 implementation at:
//  java/org/apache/catalina/realm/RealmBase.java : main(..)
//  java/org/apache/catalina/realm/MessageDigestCredentialHandler.java : String mutate(String inputCredentials, byte[] salt, int iterations)
//  java/org/apache/tomcat/util/security/ConcurrentMessageDigest.java :  byte[] digest(String algorithm, int iterations, byte[]... input)
//  java/org/apache/tomcat/util/buf/HexUtils.java : String toHexString(byte[] bytes)
public class QPWDigest
{
 private static char[] hex = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
 public static String byte2hex(byte b){return("" + hex[(b & 0xf0) >> 4] + hex[b & 0x0f]);}
 public static String digest(String password)
 {
  if(password==null)return(null);
  java.security.MessageDigest md=null;
  try{ md = md.getInstance("MD5"); }catch(java.security.NoSuchAlgorithmException nsae){return(null);}
  //try{
  byte ba[] = password.getBytes(); //password.getBytes("UTF-8");
  ba=md.digest(ba);
  StringBuffer sb = new StringBuffer();
  for(int i=0; i<ba.length; i++) { sb.append(byte2hex(ba[i])); }
  return(sb.toString());
  //} catch(UnsupportedEncodingException uee){return(null);}
 } //digest(String password)
} //QDBPasswordDigest
