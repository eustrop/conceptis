// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2011
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
// WARNING! this code produced by automatic codegeneration tool
//          located at src/sql/PGSQL/codegen/tools/codegen_ot.awk
//          In most cases you should'n change this file, but the above one.
//
// $Id$
//

package ru.mave.ConcepTIS.dao.TISC;

import ru.mave.ConcepTIS.dao.*;

/** TISC.Document data object.
 *
 */
public class Document extends DOTProcessor
{

public final static String DOBJECT_CODE = "TISC.D";
public final static String DOBJECT_CODE_SHORT = "D";
public final static String DOBJECT_NAME = "Document";
public final static String SUBSYS_CODE = "TISC";

public String getDOTypeCodeShort(){return(DOBJECT_CODE_SHORT);}
public String getDOTypeCode(){return(DOBJECT_CODE);}
public String getDOTypeName(){return(DOBJECT_NAME);}
public String getDOTypeSubsys(){return(SUBSYS_CODE);}

private static DOTProcessor dotp;

/** get common instance of this class. */
public static synchronized DOTProcessor getDOTProcessor()
{
if(dotp == null) dotp = new Document();
return(dotp);
}

/** dobject with this DOTProcessor set. */
public static DObject newDObject()
{ return(new DObject(getDOTProcessor()) ); }

public DORecord[] createMembersFactory()
{
  DORecord[] dor = new DORecord[4];
  dor[0] = new DDocument();
  dor[1] = new DRef();
  dor[2] = new DRow();
  dor[3] = new DRProperty();
  return(dor);
}

public String makeCaption(DObject o)
{ return(DObject.make_headrecord_caption(o,DDocument.TAB_CODE));}

// constructors
private Document(){}
} //Document
