// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2011
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
// WARNING! this code produced by automatic codegeneration tool
//          located at src/sql/PGSQL/codegen/tools/codegen.awk
//          In most cases you should'n change this file, but the above one.
//
// $Id$
//

package ru.mave.ConcepTIS.dao.TISC;

import ru.mave.ConcepTIS.dao.*;
import java.math.BigDecimal;

/** TISC.CEntry table.
 *
 */
public class CEntry extends DORecord
{
public Long	obj_id;

public String getCaption(){return(null);}

// Strings

public final static String TAB_CODE = "CE";
public final static String TAB_NAME = "CEntry";
public final static String SUBSYS_CODE = "TISC";

public final static String F_OBJ_ID = "obj_id";

public final static String FC_OBJ_ID = "CE01";

// children management

public DORecord[] createChildrenFactory()
{
 return(new DORecord[0]);
}

public String[] getChildTabCodes()
{
 return(new String[0]);
}

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 super.getFromRS(rs);
 obj_id = getRSLong(rs,8);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 super.getFromFS(fs);
 obj_id = getFSLong(fs,F_OBJ_ID);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSLong(ps,4,obj_id);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = super.toFieldSet();
 fs.add(new ZField(F_OBJ_ID,obj_id,FC_OBJ_ID,ZField.T_UNKNOWN,0));
 return(fs);
} //

public String getSubsysCode(){return(SUBSYS_CODE);}
public String getTabCode(){return(TAB_CODE);}
public String getTabName(){return(TAB_NAME);}
public String getParentTabCode(){return(CContainer.TAB_CODE);}
public int getNumOfFields() {return(1);}

public DORecord createFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
 {return(new CEntry(rs));}
public DORecord createFromFS(ZFieldSet fs)
 throws ZException
 {return(new CEntry(fs));};
public DORecord createInstance(){return(new CEntry());}

// constructors
public CEntry() { }
public CEntry(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public CEntry(java.sql.ResultSet rs)
 throws java.sql.SQLException { this.getFromRS(rs); }

} //CEntry
