// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2011
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
// WARNING! this code produced by automatic codegeneration tool
//          located at src/sql/PGSQL/codegen/tools/codegen.awk
//          In most cases you should'n change this file, but the above one.
//
// $Id$
//

package ru.mave.ConcepTIS.dao.TISC;

import ru.mave.ConcepTIS.dao.*;
import java.math.BigDecimal;

/** TISC.DRow table.
 *
 */
public class DRow extends DORecord
{
public Integer	num;
public String	item;

public String getCaption(){return(null);}

// Strings

public final static String TAB_CODE = "DW";
public final static String TAB_NAME = "DRow";
public final static String SUBSYS_CODE = "TISC";

public final static String F_NUM = "num";
public final static String F_ITEM = "item";

public final static String FC_NUM = "DW01";
public final static String FC_ITEM = "DW02";

// children management

public DORecord[] createChildrenFactory()
{
 DORecord dor[]=new DORecord[1];
 dor[0]=new DRProperty();
 return(dor);
}

public String[] getChildTabCodes()
{
 String c[]=new String[1];
 c[0]=DRProperty.TAB_CODE;
 return(c);
}

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 super.getFromRS(rs);
 num = getRSInteger(rs,8);
 item = getRSString(rs,9);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 super.getFromFS(fs);
 num = getFSInteger(fs,F_NUM);
 item = getFSString(fs,F_ITEM);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSInteger(ps,4,num);
 setPSString(ps,5,item);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = super.toFieldSet();
 fs.add(new ZField(F_NUM,num,FC_NUM,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_ITEM,item,FC_ITEM,ZField.T_UNKNOWN,0));
 return(fs);
} //

public String getSubsysCode(){return(SUBSYS_CODE);}
public String getTabCode(){return(TAB_CODE);}
public String getTabName(){return(TAB_NAME);}
public String getParentTabCode(){return(DDocument.TAB_CODE);}
public int getNumOfFields() {return(2);}

public DORecord createFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
 {return(new DRow(rs));}
public DORecord createFromFS(ZFieldSet fs)
 throws ZException
 {return(new DRow(fs));};
public DORecord createInstance(){return(new DRow());}

// constructors
public DRow() { }
public DRow(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public DRow(java.sql.ResultSet rs)
 throws java.sql.SQLException { this.getFromRS(rs); }

} //DRow
