// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2011
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
// WARNING! this code produced by automatic codegeneration tool
//          located at src/sql/PGSQL/codegen/tools/codegen.awk
//          In most cases you should'n change this file, but the above one.
//
// $Id$
//

package ru.mave.ConcepTIS.dao.TISC;

import ru.mave.ConcepTIS.dao.*;
import java.math.BigDecimal;

/** TISC.CContainer table.
 *
 */
public class CContainer extends DORecord
{
public String	num;
public String	type;
public String	descr;

public String getCaption(){return(num);}

// Strings

public final static String TAB_CODE = "CC";
public final static String TAB_NAME = "CContainer";
public final static String SUBSYS_CODE = "TISC";

public final static String F_NUM = "num";
public final static String F_TYPE = "type";
public final static String F_DESCR = "descr";

public final static String FC_NUM = "CC01";
public final static String FC_TYPE = "CC02";
public final static String FC_DESCR = "CC03";

// children management

public DORecord[] createChildrenFactory()
{
 DORecord dor[]=new DORecord[1];
 dor[0]=new CEntry();
 return(dor);
}

public String[] getChildTabCodes()
{
 String c[]=new String[1];
 c[0]=CEntry.TAB_CODE;
 return(c);
}

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 super.getFromRS(rs);
 num = getRSString(rs,8);
 type = rtrimString(getRSString(rs,9));
 descr = getRSString(rs,10);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 super.getFromFS(fs);
 num = getFSString(fs,F_NUM);
 type = getFSString(fs,F_TYPE);
 descr = getFSString(fs,F_DESCR);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSString(ps,4,num);
 setPSString(ps,5,type);
 setPSString(ps,6,descr);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = super.toFieldSet();
 fs.add(new ZField(F_NUM,num,FC_NUM,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_TYPE,type,FC_TYPE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_DESCR,descr,FC_DESCR,ZField.T_UNKNOWN,0));
 return(fs);
} //

public String getSubsysCode(){return(SUBSYS_CODE);}
public String getTabCode(){return(TAB_CODE);}
public String getTabName(){return(TAB_NAME);}
public String getParentTabCode(){return(null);}
public int getNumOfFields() {return(3);}

public DORecord createFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
 {return(new CContainer(rs));}
public DORecord createFromFS(ZFieldSet fs)
 throws ZException
 {return(new CContainer(fs));};
public DORecord createInstance(){return(new CContainer());}

// constructors
public CContainer() { }
public CContainer(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public CContainer(java.sql.ResultSet rs)
 throws java.sql.SQLException { this.getFromRS(rs); }

} //CContainer
