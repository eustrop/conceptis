// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2011
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
// WARNING! this code produced by automatic codegeneration tool
//          located at src/sql/PGSQL/codegen/tools/codegen_ot.awk
//          In most cases you should'n change this file, but the above one.
//
// $Id$
//

package ru.mave.ConcepTIS.dao.TISC;

import ru.mave.ConcepTIS.dao.*;

/** TISC.Container data object.
 *
 */
public class Container extends DOTProcessor
{

public final static String DOBJECT_CODE = "TISC.C";
public final static String DOBJECT_CODE_SHORT = "C";
public final static String DOBJECT_NAME = "Container";
public final static String SUBSYS_CODE = "TISC";

public String getDOTypeCodeShort(){return(DOBJECT_CODE_SHORT);}
public String getDOTypeCode(){return(DOBJECT_CODE);}
public String getDOTypeName(){return(DOBJECT_NAME);}
public String getDOTypeSubsys(){return(SUBSYS_CODE);}

private static DOTProcessor dotp;

/** get common instance of this class. */
public static synchronized DOTProcessor getDOTProcessor()
{
if(dotp == null) dotp = new Container();
return(dotp);
}

/** dobject with this DOTProcessor set. */
public static DObject newDObject()
{ return(new DObject(getDOTProcessor()) ); }

public DORecord[] createMembersFactory()
{
  DORecord[] dor = new DORecord[2];
  dor[0] = new CContainer();
  dor[1] = new CEntry();
  return(dor);
}

public String makeCaption(DObject o)
{ return(DObject.make_headrecord_caption(o,CContainer.TAB_CODE));}

// constructors
private Container(){}
} //Container
