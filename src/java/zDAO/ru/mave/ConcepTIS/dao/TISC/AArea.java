// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2011
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
// WARNING! this code produced by automatic codegeneration tool
//          located at src/sql/PGSQL/codegen/tools/codegen.awk
//          In most cases you should'n change this file, but the above one.
//
// $Id$
//

package ru.mave.ConcepTIS.dao.TISC;

import ru.mave.ConcepTIS.dao.*;
import java.math.BigDecimal;

/** TISC.AArea table.
 *
 */
public class AArea extends DORecord
{
public String	name;
public Long	scope_id;
public String	descr;

public String getCaption(){return(name);}

// Strings

public final static String TAB_CODE = "AA";
public final static String TAB_NAME = "AArea";
public final static String SUBSYS_CODE = "TISC";

public final static String F_NAME = "name";
public final static String F_SCOPE_ID = "scope_id";
public final static String F_DESCR = "descr";

public final static String FC_NAME = "AA01";
public final static String FC_SCOPE_ID = "AA02";
public final static String FC_DESCR = "AA03";

// children management

public DORecord[] createChildrenFactory()
{
 return(new DORecord[0]);
}

public String[] getChildTabCodes()
{
 return(new String[0]);
}

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 super.getFromRS(rs);
 name = getRSString(rs,8);
 scope_id = getRSLong(rs,9);
 descr = getRSString(rs,10);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 super.getFromFS(fs);
 name = getFSString(fs,F_NAME);
 scope_id = getFSLong(fs,F_SCOPE_ID);
 descr = getFSString(fs,F_DESCR);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSString(ps,4,name);
 setPSLong(ps,5,scope_id);
 setPSString(ps,6,descr);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = super.toFieldSet();
 fs.add(new ZField(F_NAME,name,FC_NAME,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SCOPE_ID,scope_id,FC_SCOPE_ID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_DESCR,descr,FC_DESCR,ZField.T_UNKNOWN,0));
 return(fs);
} //

public String getSubsysCode(){return(SUBSYS_CODE);}
public String getTabCode(){return(TAB_CODE);}
public String getTabName(){return(TAB_NAME);}
public String getParentTabCode(){return(null);}
public int getNumOfFields() {return(3);}

public DORecord createFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
 {return(new AArea(rs));}
public DORecord createFromFS(ZFieldSet fs)
 throws ZException
 {return(new AArea(fs));};
public DORecord createInstance(){return(new AArea());}

// constructors
public AArea() { }
public AArea(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public AArea(java.sql.ResultSet rs)
 throws java.sql.SQLException { this.getFromRS(rs); }

} //AArea
