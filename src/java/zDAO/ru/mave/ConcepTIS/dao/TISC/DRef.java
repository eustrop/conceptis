// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2011
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
// WARNING! this code produced by automatic codegeneration tool
//          located at src/sql/PGSQL/codegen/tools/codegen.awk
//          In most cases you should'n change this file, but the above one.
//
// $Id$
//

package ru.mave.ConcepTIS.dao.TISC;

import ru.mave.ConcepTIS.dao.*;
import java.math.BigDecimal;

/** TISC.DRef table.
 *
 */
public class DRef extends DORecord
{
public Long	doc_id;
public String	reltype;

public String getCaption(){return(null);}

// Strings

public final static String TAB_CODE = "DR";
public final static String TAB_NAME = "DRef";
public final static String SUBSYS_CODE = "TISC";

public final static String F_DOC_ID = "doc_id";
public final static String F_RELTYPE = "reltype";

public final static String FC_DOC_ID = "DR01";
public final static String FC_RELTYPE = "DR02";

// children management

public DORecord[] createChildrenFactory()
{
 return(new DORecord[0]);
}

public String[] getChildTabCodes()
{
 return(new String[0]);
}

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 super.getFromRS(rs);
 doc_id = getRSLong(rs,8);
 reltype = rtrimString(getRSString(rs,9));
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 super.getFromFS(fs);
 doc_id = getFSLong(fs,F_DOC_ID);
 reltype = getFSString(fs,F_RELTYPE);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSLong(ps,4,doc_id);
 setPSString(ps,5,reltype);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = super.toFieldSet();
 fs.add(new ZField(F_DOC_ID,doc_id,FC_DOC_ID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_RELTYPE,reltype,FC_RELTYPE,ZField.T_UNKNOWN,0));
 return(fs);
} //

public String getSubsysCode(){return(SUBSYS_CODE);}
public String getTabCode(){return(TAB_CODE);}
public String getTabName(){return(TAB_NAME);}
public String getParentTabCode(){return(DDocument.TAB_CODE);}
public int getNumOfFields() {return(2);}

public DORecord createFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
 {return(new DRef(rs));}
public DORecord createFromFS(ZFieldSet fs)
 throws ZException
 {return(new DRef(fs));};
public DORecord createInstance(){return(new DRef());}

// constructors
public DRef() { }
public DRef(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public DRef(java.sql.ResultSet rs)
 throws java.sql.SQLException { this.getFromRS(rs); }

} //DRef
