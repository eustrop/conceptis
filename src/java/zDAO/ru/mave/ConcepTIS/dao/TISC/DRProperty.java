// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2011
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
// WARNING! this code produced by automatic codegeneration tool
//          located at src/sql/PGSQL/codegen/tools/codegen.awk
//          In most cases you should'n change this file, but the above one.
//
// $Id$
//

package ru.mave.ConcepTIS.dao.TISC;

import ru.mave.ConcepTIS.dao.*;
import java.math.BigDecimal;

/** TISC.DRProperty table.
 *
 */
public class DRProperty extends DORecord
{
public String	type;
public BigDecimal	nvalue;
public String	tvalue;

public String getCaption(){return(null);}

// Strings

public final static String TAB_CODE = "DP";
public final static String TAB_NAME = "DRProperty";
public final static String SUBSYS_CODE = "TISC";

public final static String F_TYPE = "type";
public final static String F_NVALUE = "nvalue";
public final static String F_TVALUE = "tvalue";

public final static String FC_TYPE = "DP01";
public final static String FC_NVALUE = "DP02";
public final static String FC_TVALUE = "DP03";

// children management

public DORecord[] createChildrenFactory()
{
 return(new DORecord[0]);
}

public String[] getChildTabCodes()
{
 return(new String[0]);
}

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 super.getFromRS(rs);
 type = rtrimString(getRSString(rs,8));
 nvalue = getRSBigDecimal(rs,9);
 tvalue = getRSString(rs,10);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 super.getFromFS(fs);
 type = getFSString(fs,F_TYPE);
 nvalue = getFSBigDecimal(fs,F_NVALUE);
 tvalue = getFSString(fs,F_TVALUE);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSString(ps,4,type);
 setPSBigDecimal(ps,5,nvalue);
 setPSString(ps,6,tvalue);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = super.toFieldSet();
 fs.add(new ZField(F_TYPE,type,FC_TYPE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_NVALUE,nvalue,FC_NVALUE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_TVALUE,tvalue,FC_TVALUE,ZField.T_UNKNOWN,0));
 return(fs);
} //

public String getSubsysCode(){return(SUBSYS_CODE);}
public String getTabCode(){return(TAB_CODE);}
public String getTabName(){return(TAB_NAME);}
public String getParentTabCode(){return(DRow.TAB_CODE);}
public int getNumOfFields() {return(3);}

public DORecord createFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
 {return(new DRProperty(rs));}
public DORecord createFromFS(ZFieldSet fs)
 throws ZException
 {return(new DRProperty(fs));};
public DORecord createInstance(){return(new DRProperty());}

// constructors
public DRProperty() { }
public DRProperty(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public DRProperty(java.sql.ResultSet rs)
 throws java.sql.SQLException { this.getFromRS(rs); }

} //DRProperty
