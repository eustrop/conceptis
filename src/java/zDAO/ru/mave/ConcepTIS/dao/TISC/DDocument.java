// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2011
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
// WARNING! this code produced by automatic codegeneration tool
//          located at src/sql/PGSQL/codegen/tools/codegen.awk
//          In most cases you should'n change this file, but the above one.
//
// $Id$
//

package ru.mave.ConcepTIS.dao.TISC;

import ru.mave.ConcepTIS.dao.*;
import java.math.BigDecimal;

/** TISC.DDocument table.
 *
 */
public class DDocument extends DORecord
{
public String	symbol;
public String	type;
public String	title;
public String	auth;
public String	abstr;

public String getCaption(){return(symbol);}

// Strings

public final static String TAB_CODE = "DD";
public final static String TAB_NAME = "DDocument";
public final static String SUBSYS_CODE = "TISC";

public final static String F_SYMBOL = "symbol";
public final static String F_TYPE = "type";
public final static String F_TITLE = "title";
public final static String F_AUTH = "auth";
public final static String F_ABSTR = "abstr";

public final static String FC_SYMBOL = "DD01";
public final static String FC_TYPE = "DD02";
public final static String FC_TITLE = "DD03";
public final static String FC_AUTH = "DD04";
public final static String FC_ABSTR = "DD05";

// children management

public DORecord[] createChildrenFactory()
{
 DORecord dor[]=new DORecord[2];
 dor[0]=new DRow();
 dor[1]=new DRef();
 return(dor);
}

public String[] getChildTabCodes()
{
 String c[]=new String[2];
 c[0]=DRow.TAB_CODE;
 c[1]=DRef.TAB_CODE;
 return(c);
}

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 super.getFromRS(rs);
 symbol = getRSString(rs,8);
 type = rtrimString(getRSString(rs,9));
 title = getRSString(rs,10);
 auth = getRSString(rs,11);
 abstr = getRSString(rs,12);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 super.getFromFS(fs);
 symbol = getFSString(fs,F_SYMBOL);
 type = getFSString(fs,F_TYPE);
 title = getFSString(fs,F_TITLE);
 auth = getFSString(fs,F_AUTH);
 abstr = getFSString(fs,F_ABSTR);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSString(ps,4,symbol);
 setPSString(ps,5,type);
 setPSString(ps,6,title);
 setPSString(ps,7,auth);
 setPSString(ps,8,abstr);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = super.toFieldSet();
 fs.add(new ZField(F_SYMBOL,symbol,FC_SYMBOL,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_TYPE,type,FC_TYPE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_TITLE,title,FC_TITLE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_AUTH,auth,FC_AUTH,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_ABSTR,abstr,FC_ABSTR,ZField.T_UNKNOWN,0));
 return(fs);
} //

public String getSubsysCode(){return(SUBSYS_CODE);}
public String getTabCode(){return(TAB_CODE);}
public String getTabName(){return(TAB_NAME);}
public String getParentTabCode(){return(null);}
public int getNumOfFields() {return(5);}

public DORecord createFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
 {return(new DDocument(rs));}
public DORecord createFromFS(ZFieldSet fs)
 throws ZException
 {return(new DDocument(fs));};
public DORecord createInstance(){return(new DDocument());}

// constructors
public DDocument() { }
public DDocument(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public DDocument(java.sql.ResultSet rs)
 throws java.sql.SQLException { this.getFromRS(rs); }

} //DDocument
