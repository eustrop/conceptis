// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao;

import java.util.*;
import java.sql.*;
import ru.mave.ConcepTIS.dao.SAM.*;
import ru.mave.ConcepTIS.dao.TIS.*;
import ru.mave.ConcepTIS.dao.TISC.*;
import ru.mave.ConcepTIS.dao.FS.*;
import ru.mave.ConcepTIS.dao.QR.*;

/** Entrance ConcepTIS database access class. Object of this
 * class encapsulate java.sql.Connection and represent all
 * database at TIS-SQL fashion. All underlaying TIS objects 
 * could be accessed only by means of ZSystem directly or
 * indirectly (with the aid of some kind of underlaying
 * objects)
 *
 * @see TISC.Area
 */
public class ZSystem
{
private java.sql.Connection DBC = null; //
private ZMessages zm = new ZMessages();
private Hashtable ht_dic_cache = new Hashtable();

public java.sql.Connection getDBC(){return(DBC);}
public void setDBC(java.sql.Connection dbc){DBC=dbc;}
//

private final static String[] TISQL_TOKENS = { "V%%_", "V%_" };
private static String[] TISQL_TOKENS2 = { "V%_", "V_" };

/** Rewrite SQL expression from TISQL universal syntax form into
 * target DBMS form. All applied SQL requests must be wrote at TISQL
 * form and passed over this method just before execution. This technique
 * primarily addressed to transparent switching from "current state view",
 * visible at "V_" family view, to "snapshot view" visible at "VD_" views
 * or to "not-filtred state view" accesible via "V0_" or VD0_" views by some
 * kinde of trusted users. Once implemented and considerably used over
 * the whole code of system it could be greatly useful to porting to
 * another DBMS later.
 * This method rewrite these special tokens:
 * V%%_ = V%_
 * V%_ = one of {V_ , VD_, ...} depending of global settings
 * ... syntax could be extended at future
 */
public String TISQL2SQL(String sql)
{
 return(ZMessages.translate_tokens(sql,TISQL_TOKENS,TISQL_TOKENS2));
}

public java.sql.ResultSet execSQL(String sql)
 throws java.sql.SQLException
{
 return( getDBC().createStatement().executeQuery(sql) );
}
public java.sql.ResultSet execTISQL(String tisql)
 throws java.sql.SQLException
{return(execSQL(TISQL2SQL(tisql)));}

/** obtain PreparedStatement for given SQL request
 * (possible from a pool of cached statements).
 *
 * @see #getPS4TISQL
 * @see #closePS
 */
public java.sql.PreparedStatement getPS4SQL(String SQL)
 throws java.sql.SQLException
{
   return(getDBC().prepareStatement(SQL));
}

/** obtain PreparedStatement for given TISQL request
 * (possibly from a pool of cached statements).
 *
 * @see #getPS4SQL
 * @see #TISQL2SQL
 * @see #closePS
 */
public java.sql.PreparedStatement getPS4TISQL(String TISQL)
 throws java.sql.SQLException
{
   return( getPS4SQL(TISQL2SQL(TISQL)) );
}

/** obtain CallableStatement for given SQL request
 * (possible from a pool of cached statements).
 *
 * @see #getPS4TISQL
 * @see #closePS
 */
public java.sql.CallableStatement getCS4SQL(String SQL)
 throws java.sql.SQLException
{
   return(getDBC().prepareCall(SQL));
}

/** obtain CallableStatement for given TISQL request
 * (possibly from a pool of cached statements).
 *
 * @see #getPS4SQL
 * @see #TISQL2SQL
 * @see #closePS
 */
public java.sql.CallableStatement getCS4TISQL(String TISQL)
 throws java.sql.SQLException
{
   return( getCS4SQL(TISQL2SQL(TISQL)) );
}

/** close PreparedStatement previously obtined by getPS4SQL()
 * or getPS4TISQL() ( and give back it into the pool if so).
 * All possible errors are ignored.
 *
 * @see #getPS4SQL
 * @see #TISQL2SQL
 * @see #closePS
 */
public void closePS(java.sql.PreparedStatement ps)
{
 if(ps== null)return;
 try{ ps.close();} catch(SQLException sqle){}
}

/** close CallableStatement previously obtined by getCS4SQL()
 * or getPS4TISQL() ( and give back it into the pool if so).
 * (possibly from a pool of cached statements).
 *
 * @see #getCS4SQL
 * @see #TISQL2SQL
 * @see #closePS
 */
public void closeCS(java.sql.CallableStatement cs)
{
 if(cs == null)return;
 try{ cs.close();} catch(SQLException sqle){}
}

/** close ResultSet obtained per ZSystem functionality 
 * as well as it's parent Statement (bypass any errors).
 * This method and closeXS() family are mutually exclusive.
 *
 * @see #getPS4SQL
 * @see #getPS4TISQL
 * @see #executeSQL
 * @see #executeTISQL
 * @see #closePS
 * @see #closeCS
 */
public void closeRS(java.sql.ResultSet rs)
{
 if(rs== null)return;
 java.sql.Statement st = null;
 try{st = rs.getStatement();} catch(SQLException sqle){}
 try{ rs.close();} catch(SQLException sqle){}
 if(st == null) return;
 //else
 try{ st.close();} catch(SQLException sqle){}
}

/** check execute status and throw exception if error occurred. */
public void checkExecStatus(ZExecStatus es)
 throws ZException
{
  if(es.errnum == null) throw new ZException(es);
  if(es.errnum.intValue()>0) throw new ZException(es);
}

//
// DIC subsystem
//

/** get dictionary from cache or load it from the DB, put into the
 * cache and return to caller. On error new empty dictionary
 * will be returned (without caching).
 */
public ZDictionary getDictionary(String dic)
{
 ZDictionary zd = null;
 zd = (ZDictionary)ht_dic_cache.get(dic);
 if(zd != null) return(zd);
 try{ zd = loadDictionary(dic); } catch(Exception e){}
 if(zd == null) zd=new ZDictionary();
 else ht_dic_cache.put(dic,zd);
 return(zd);
} // getDictionary(String dic)

public ZDictionary loadDictionary(String dic)
	throws java.sql.SQLException, ZException
{
 ZDictionary rl = new ZDictionary();
 PreparedStatement ps = getPS4TISQL(zm.getSQL(zm.SQL_LOAD_DICTIONARY));
 Record.setPSString(ps,1,dic);
 try{ ResultSet rs = ps.executeQuery();
  while(rs.next()){
   String code = Record.getRSString(rs,2);
   String value = Record.getRSString(rs,3);
   String descr = Record.getRSString(rs,4);
   DCode r = new DCode(code,value,descr);
   rl.add(r); }
 }finally{closePS(ps);}
 return(rl);
} // loadDictionary(String dic)

//
// SAM subsystem
//

// SAMUser 

public RList listSAMUsers()
 throws java.sql.SQLException,ZException
{
 RList rl = new RList();
 PreparedStatement ps = getPS4TISQL(User.TISQL_LIST);
 try{ ResultSet rs = ps.executeQuery();
  while(rs.next()){ User r = new User(rs); rl.add(r); }
 }finally{closePS(ps);}
 return(rl);
} // listSAMUsers()

public User loadSAMUser(Long id)
 throws java.sql.SQLException,ZException
{
 User r = null;
 PreparedStatement ps = getPS4TISQL(User.TISQL_LOAD);
 Record.setPSLong(ps,1,id);
 try{ ResultSet rs = ps.executeQuery();
  if (rs.next()) r = new User(rs);
 }finally{closePS(ps);}
 return(r);
} // loadSAMUser(Long id)

private User current_user;
public User getCurrentSAMUser()
{
if(current_user!= null) return(current_user);
try{ current_user=loadCurrentSAMUser(); } catch(Exception e){}
return(current_user);
}
public User loadCurrentSAMUser()
 throws java.sql.SQLException,ZException
{
 User r = null;
 PreparedStatement ps = getPS4TISQL("select * from SAM.V_User where id = SAM.get_user()");
 //Record.setPSLong(ps,1,id);
 try{ ResultSet rs = ps.executeQuery();
  if (rs.next()) r = new User(rs);
 }finally{closePS(ps);}
 return(r);
} // loadCurrentSAMUser(Long id)

public void saveSAMUser(User r)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = null;
 if(r.id == null) ps = getPS4TISQL(User.TISQL_CREATE);
 else ps = getPS4TISQL(User.TISQL_UPDATE);
 try{ r.putToPS(ps);
  ResultSet rs = ps.executeQuery();
  ZExecStatus es = new ZExecStatus(rs);
  checkExecStatus(es);
  if(r.id == null) r.id = es.ZID;
 }finally{closePS(ps);}
} // saveSAMUser(User r)

public void deleteSAMUser(Long id)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(User.TISQL_DELETE);
 try{ Record.setPSLong(ps,1,id);
  checkExecStatus( new ZExecStatus(ps.executeQuery()) );
 }finally{closePS(ps);}
} // deleteSAMUser(Long id)

public void setSAMUserPassword(Long uid, String password)
 throws java.sql.SQLException,ZException
{
 if(password != null)
 {
   if(password.length() < 8) throw new ZException("Password too short (from zDAO)");
   password = QPWDigest.digest(password);
 }
 PreparedStatement ps = getPS4TISQL(zm.getSQL(zm.TISQL_SET_SAMUSERPASSWORD));
 try{ Record.setPSLong(ps,1,uid); Record.setPSString(ps,2,password);
  checkExecStatus( new ZExecStatus(ps.executeQuery()) );
 }finally{closePS(ps);}
} // setSAMUserPassword(Long uid, Long gid)

public void revokeSAMUserPassword(Long uid)
 throws java.sql.SQLException,ZException
{
 setSAMUserPassword(uid,null);
} // revokeSAMUserPassword(Long uid)

public void grantSAMUserGroup(Long uid, Long gid)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(zm.getSQL(zm.TISQL_GRANT_SAMUSERGROUP));
 try{ Record.setPSLong(ps,1,uid); Record.setPSLong(ps,2,gid);
  checkExecStatus( new ZExecStatus(ps.executeQuery()) );
 }finally{closePS(ps);}
} // grantSAMUserGroup(Long uid, Long gid)

public void revokeSAMUserGroup(Long uid, Long gid)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(zm.getSQL(zm.TISQL_REVOKE_SAMUSERGROUP));
 try{ Record.setPSLong(ps,1,uid); Record.setPSLong(ps,2,gid);
  checkExecStatus( new ZExecStatus(ps.executeQuery()) );
 }finally{closePS(ps);}
} // revokeSAMUserGroup(Long uid, Long gid)

public RList listSAMUserCapabilities(Long uid)
 throws java.sql.SQLException,ZException
{
 RList rl = new RList();
 PreparedStatement ps = getPS4TISQL(zm.getSQL(zm.TISQL_LISTSAMUSERCAPABILITIES4USER));
 Record.setPSLong(ps,1,uid);
 try{ ResultSet rs = ps.executeQuery();
  while(rs.next()){ UserCapability r = new UserCapability(rs); rl.add(r); }
 }finally{closePS(ps);}
 return(rl);
} // listSAMUserCapabilities4User()

public void grantSAMUserCapability(Long uid, Long sid, String capcode)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(zm.getSQL(zm.TISQL_GRANT_SAMUSERCAPABILITY));
 try{ Record.setPSLong(ps,1,uid); Record.setPSLong(ps,2,sid);
  Record.setPSString(ps,3,capcode);
  checkExecStatus( new ZExecStatus(ps.executeQuery()) );
 }finally{closePS(ps);}
} // grantSAMUserCapability(Long uid, Long gid)

public void revokeSAMUserCapability(Long uid, Long sid, String capcode)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(zm.getSQL(zm.TISQL_REVOKE_SAMUSERCAPABILITY));
 try{ Record.setPSLong(ps,1,uid); Record.setPSLong(ps,2,sid);
  Record.setPSString(ps,3,capcode);
  checkExecStatus( new ZExecStatus(ps.executeQuery()) );
 }finally{closePS(ps);}
} // revokeSAMUserCapability(Long uid, Long gid)

// SAMGroup

public RList listSAMGroups()
 throws java.sql.SQLException,ZException
{
 RList rl = new RList();
 PreparedStatement ps = getPS4TISQL(Group.TISQL_LIST);
 try{ ResultSet rs = ps.executeQuery();
  while(rs.next()){ Group r = new Group(rs); rl.add(r); }
 }finally{closePS(ps);}
 return(rl);
} // listSAMGroups()

public Group loadSAMGroup(Long id)
 throws java.sql.SQLException,ZException
{
 Group r = null;
 PreparedStatement ps = getPS4TISQL(Group.TISQL_LOAD);
 Record.setPSLong(ps,1,id);
 try{ ResultSet rs = ps.executeQuery();
  if (rs.next()) r = new Group(rs);
 }finally{closePS(ps);}
 return(r);
} // loadSAMGroup(Long id)

public void saveSAMGroup(Group r)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = null;
 if(r.id == null) ps = getPS4TISQL(Group.TISQL_CREATE);
 else ps = getPS4TISQL(Group.TISQL_UPDATE);
 try{ r.putToPS(ps);
  ResultSet rs = ps.executeQuery();
  ZExecStatus es = new ZExecStatus(rs);
  checkExecStatus(es);
  if(r.id == null) r.id = es.ZID;
 }finally{closePS(ps);}
} // saveSAMGroup(Group r)

public void deleteSAMGroup(Long id)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(Group.TISQL_DELETE);
 try{ Record.setPSLong(ps,1,id);
  checkExecStatus( new ZExecStatus(ps.executeQuery()) );
 }finally{closePS(ps);}
} // deleteSAMGroup(Long id)

public RList listSAMGroups4User(Long id)
 throws java.sql.SQLException,ZException
{
 RList rl = new RList();
 PreparedStatement ps = getPS4TISQL(zm.getSQL(zm.TISQL_LISTSAMGROUPS4USER));
 Record.setPSLong(ps,1,id);
 try{ ResultSet rs = ps.executeQuery();
  while(rs.next()){ Group r = new Group(rs); rl.add(r); }
 }finally{closePS(ps);}
 return(rl);
} // listSAMGroups4User()

public RList listSAMUsers4Group(Long id)
 throws java.sql.SQLException,ZException
{
 RList rl = new RList();
 PreparedStatement ps = getPS4TISQL(zm.getSQL(zm.TISQL_LISTSAMUSER4GROUP));
 Record.setPSLong(ps,1,id);
 try{ ResultSet rs = ps.executeQuery();
  while(rs.next()){ User r = new User(rs); rl.add(r); }
 }finally{closePS(ps);}
 return(rl);
} // listSAMUsers4Group()

public RList listSAMACLScope4Group(Long id)
 throws java.sql.SQLException,ZException
{
 RList rl = new RList();
 PreparedStatement ps = getPS4TISQL(zm.getSQL(zm.TISQL_LISTSAMACLSCOPE4GROUP));
 Record.setPSLong(ps,1,id);
 try{ ResultSet rs = ps.executeQuery();
  while(rs.next()){ ACLScope r = new ACLScope(rs); rl.add(r); }
 }finally{closePS(ps);}
 return(rl);
} // listSAMACLScope4Group()

public void setSAMACLScope(ACLScope r)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = null;
 ps = getPS4TISQL(zm.getSQL(zm.TISQL_SET_SAMACLSCOPE));
 try{ r.putToPS(ps); ResultSet rs = ps.executeQuery();
  ZExecStatus es = new ZExecStatus(rs); checkExecStatus(es);
 }finally{closePS(ps);}
} // setSAMACLScope(ScopePolicy r)

public void deleteSAMACLScope(Long gid, Long sid, String obj_type)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(zm.getSQL(zm.TISQL_DELETE_SAMACLSCOPE));
 try{ Record.setPSLong(ps,1,gid); Record.setPSLong(ps,2,sid);
      Record.setPSString(ps,3,obj_type);
  checkExecStatus( new ZExecStatus(ps.executeQuery()) );
 }finally{closePS(ps);}
} // deleteSAMACLScope(Long sid, String obj_type)

// SAMScope 

public RList listSAMScopes()
 throws java.sql.SQLException,ZException
{
 RList rl = new RList();
 PreparedStatement ps = getPS4TISQL(Scope.TISQL_LIST);
 try{ ResultSet rs = ps.executeQuery();
  while(rs.next()){ Scope r = new Scope(rs); rl.add(r); }
 }finally{closePS(ps);}
 return(rl);
} // listSAMScopes()

public Scope loadSAMScope(Long id)
 throws java.sql.SQLException,ZException
{
 Scope r = null;
 PreparedStatement ps = getPS4TISQL(Scope.TISQL_LOAD);
 Record.setPSLong(ps,1,id);
 try{ ResultSet rs = ps.executeQuery();
  if (rs.next()) r = new Scope(rs);
 }finally{closePS(ps);}
 return(r);
} // loadSAMScope(Long id)

public void createSAMScope(Scope r)
 throws java.sql.SQLException,ZException
{ saveSAMScope(Scope.TISQL_CREATE,r); }

public void updateSAMScope(Scope r)
 throws java.sql.SQLException,ZException
{ saveSAMScope(Scope.TISQL_UPDATE,r); }

/** save SAM.Scope object using TISQL request. */
private void saveSAMScope(String TISQL, Scope r)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = null; ps = getPS4TISQL(TISQL);
 try{ r.putToPS(ps); ResultSet rs = ps.executeQuery();
  ZExecStatus es = new ZExecStatus(rs); checkExecStatus(es);
 }finally{closePS(ps);}
} // saveSAMScope(Scope r)

public void deleteSAMScope(Long id)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(Scope.TISQL_DELETE);
 try{ Record.setPSLong(ps,1,id);
  checkExecStatus( new ZExecStatus(ps.executeQuery()) );
 }finally{closePS(ps);}
} // deleteSAMScope(Long id)

public RList listSAMScopePolicy(Long id)
 throws java.sql.SQLException,ZException
{
 RList rl = new RList();
 PreparedStatement ps = getPS4TISQL(zm.getSQL(zm.TISQL_LISTSAMSCOPEPOLICY));
 Record.setPSLong(ps,1,id);
 try{ ResultSet rs = ps.executeQuery();
  while(rs.next()){ rl.add(new ScopePolicy(rs)); }
 }finally{closePS(ps);}
 return(rl);
} // listSAMScopePolicy()

public void setSAMScopeCHPTS(Long sid, DateTime chpts)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(zm.getSQL(zm.TISQL_SET_SAMSCOPE_CHPTS));
 try{ Record.setPSLong(ps,1,sid); Record.setPSDateTime(ps,2,chpts);
  checkExecStatus( new ZExecStatus(ps.executeQuery()) );
 }finally{closePS(ps);}
} // setSAMScopeCHPTS(Long sid, DateTime chpts)

public void setSAMScopePolicy(ScopePolicy r)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = null;
 ps = getPS4TISQL(zm.getSQL(zm.TISQL_SET_SAMSCOPEPOLICY));
 try{ r.putToPS(ps); ResultSet rs = ps.executeQuery();
  ZExecStatus es = new ZExecStatus(rs); checkExecStatus(es);
 }finally{closePS(ps);}
} // setSAMScopePolicy(ScopePolicy r)

public void deleteSAMScopePolicy(Long sid, String obj_type)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(zm.getSQL(zm.TISQL_DELETE_SAMSCOPEPOLICY));
 try{ Record.setPSLong(ps,1,sid); Record.setPSString(ps,2,obj_type);
  checkExecStatus( new ZExecStatus(ps.executeQuery()) );
 }finally{closePS(ps);}
} // deleteSAMScopePolicy(Long sid, String obj_type)


//
// TIS - common data objects manipulation methods
//

public DateTime getVDate()
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(zm.getSQL(zm.TISQL_GET_VDATE));
 DateTime vdate=null;
 try{
  ResultSet rs = ps.executeQuery();
  while(rs.next()){ vdate=Record.getRSDateTime(rs,ZObject.F_ZDATO); }
 }finally{closePS(ps);}
 return(vdate);
} // getVDate

/** load list of all active data objects (ZSTA='N' or (ZVER=1 and ZSTA='I'))
 * in the specified  Scope (ZSID) or in whole DB (if ZSID=null).
 * Limit list to specified "type of data object" (ZTYPE), if it's not null.
 * if LIMIT > 0 then get no more than LIMIT objects from recordset
 * if OFFSET > 0 then starting from OFFSET recordset position.
 * LIMIT and OFFSET shoud be used to 
 */
public RList listDObjects(Long ZSID, String ZTYPE, int LIMIT, long OFFSET)
 throws java.sql.SQLException,ZException
{ return(loadDObjectsList(zm.getSQL(zm.TISQL_LIST_ACTIVE_OBJECTS),
	ZSID,ZTYPE,LIMIT,OFFSET));
} //listDObjects()

/** incomlete logical transations on objects visible to current user. */
public RList listDObjectsLocked(Long ZSID, String ZTYPE, int LIMIT, long OFFSET)
 throws java.sql.SQLException,ZException
{ return(loadDObjectsList(zm.getSQL(zm.TISQL_LIST_LOCKED_OBJECTS),
	ZSID,ZTYPE,LIMIT,OFFSET));
} //

/** incomlete logical transations owned by current user. */
public RList listDObjectsIntermediate(Long ZSID, String ZTYPE, int LIMIT, long OFFSET)
 throws java.sql.SQLException,ZException
{ return(loadDObjectsList(zm.getSQL(zm.TISQL_LIST_MY_INTERMEDIATE_OBJECTS),
	ZSID,ZTYPE,LIMIT,OFFSET));
} //

public RList loadDObjectsList(String szSQL, Long ZSID, String ZTYPE, int LIMIT, long OFFSET)
 throws java.sql.SQLException,ZException
{
 RList rl = new RList();
 int last_param=0;
 int P_ZSID=0,P_ZTYPE=0,P_LIMIT=0,P_OFFSET=0;
 StringBuffer sbSQL = new StringBuffer();
 sbSQL.append(szSQL);
 if(ZSID != null || ZTYPE != null)sbSQL.append(zm.getSQL(zm.TISQL_WHERE));
 if(ZSID != null){ if(last_param!=0) sbSQL.append(zm.getSQL(zm.TISQL_AND));
	sbSQL.append(zm.getSQL(zm.TISQLCOND_AT_ZO_ZSID)); P_ZSID=++last_param; }
 if(ZTYPE != null){ if(last_param!=0) sbSQL.append(zm.getSQL(zm.TISQL_AND));
	sbSQL.append(zm.getSQL(zm.TISQLCOND_OF_ZO_ZTYPE)); P_ZTYPE=++last_param; }
 sbSQL.append(zm.getSQL(zm.TISQLORDER_ZOID));
 if(LIMIT > 0){ sbSQL.append(zm.getSQL(zm.TISQL_LIMIT));
	P_LIMIT=++last_param; }
 if(OFFSET > 0){ sbSQL.append(zm.getSQL(zm.TISQL_OFFSET));
	P_OFFSET=++last_param; }
 PreparedStatement ps = getPS4TISQL(sbSQL.toString());
 try{
  // if(true) throw (new ZException(sbSQL.toString()));
  if(P_ZSID!=0) Record.setPSLong(ps,P_ZSID,ZSID);
  if(P_ZTYPE!=0) Record.setPSString(ps,P_ZTYPE,ZTYPE);
  if(P_LIMIT!=0) Record.setPSInt(ps,P_LIMIT,new Integer(LIMIT));
  if(P_OFFSET!=0) Record.setPSLong(ps,P_OFFSET, new Long(OFFSET));
  ResultSet rs = ps.executeQuery();
  while(rs.next()){
   DObject o = new DObject(new ZObject(rs));
   try{ o.setZNAME(Record.getRSString(rs,ZName.F_ZNAME)); }
   catch(java.sql.SQLException sqle){} // ignore
   rl.add(o);
   }
 }finally{closePS(ps);}
 return(rl);
} // loadDObjectsList

/** get list of all dobjects (may be inadmissible huge! ). */
public RList listDObjects(Long ZSID, String ZTYPE)
 throws java.sql.SQLException,ZException
{ return(listDObjects(ZSID,ZTYPE,0,0)); }

public void loadDObject(DObject o)
 throws java.sql.SQLException,ZException
{loadDObject(o,false);}

/** load dobject with its actual data from V_ and metadata from V_/VI_
 * (open it before loading if(o_open)).
 */
public void loadDObject(DObject o, boolean o_open)
 throws java.sql.SQLException,ZException
{
loadDObjectMetadata(o);
if(!o.checkFoundZTYPE())
{
 o.fixFoundZTYPE();
 return;
}
if(o_open) throw(new ZException(zm.getMsg(zm.ERR_NOT_IMPLEMENTED))); //openDObject
loadDObjectData(o);
if(o.isExists()) loadDObjectLVersion(o);
loadDObjectIVersion(o);
//if(!isExists()) o.importMissedVersionData();
o.organize();
}

/** load dobject's intermediate data from VI_ and apply it. */
public void applyDObjectIntermediate(DObject o)
 throws java.sql.SQLException,ZException
{applyDObjectIntermediate(o,null);}

/** load dobject's intermediate data from VI_ and apply it but
 * only if it has the same ZVER (Exception otherwise). */
public void applyDObjectIntermediate(DObject o, Long ZVER)
 throws java.sql.SQLException,ZException
{
DObject io = new DObject(o.getDOTProcessor());
io.setZOID(o.getZOID());
io.setZTYPE(o.getZTYPE());
io.setZVER(ZVER);
loadDObjectDataIntermediate(io);
if(ZVER != null) {
 loadDObjectIVersion(io);
 ZObject ZO_I = io.getIVersion();
 if(ZO_I == null) throw (new ZException(zm,zm.ERR_INVALID_VERSION));
 if(!ZVER.equals(ZO_I.ZVER)) throw (new ZException(zm,zm.ERR_INVALID_VERSION));
}
o.importIntermediate(io);
}

/** find ZTYPE for passed ZOID. */
public String resolveZTYPE(Long ZOID)
 throws java.sql.SQLException,ZException
{
DObject o = new DObject(ZOID);
loadDObjectNVersion(o); o.importMissedVersionData();
return(o.getZTYPE());
}

/** load necessary metadata (NVersion, ZNAME) for typical use. */
public void loadDObjectMetadata(DObject o)
 throws java.sql.SQLException,ZException
{
loadDObjectNVersion(o);
loadDObjectZNAME(o);
o.importMissedVersionData();
}

/** load all dobject's metadata ({N,L,I,H}Version[s], ZNAME)
 * for careful inspection.
 */
public void loadDObjectMetadataFull(DObject o)
 throws java.sql.SQLException,ZException
{
loadDObjectNVersion(o);
loadDObjectLVersion(o);
loadDObjectIVersion(o);
loadDObjectHVersions(o);
loadDObjectZNAME(o);
o.importMissedVersionData();
}

public void loadDObjectHVersions(DObject o)
 throws java.sql.SQLException,ZException
{
 RList rl = new RList();
 StringBuffer sbSQL = new StringBuffer();
 sbSQL.append(zm.getSQL(zm.TISQL_LOAD_ZO_HVERSIONS));
 PreparedStatement ps = getPS4TISQL(sbSQL.toString());
 try{
  Record.setPSLong(ps,1,o.getZOID());
  ResultSet rs = ps.executeQuery();
  while(rs.next()){ rl.add(new ZObject(rs)); }
 }finally{closePS(ps);}
 o.setHVersions(rl);
} //loadDObjectHVersions

public void loadDObjectNVersion(DObject o)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(zm.getSQL(zm.TISQL_LOAD_ZO_NVERSION));
 try{ Record.setPSLong(ps,1,o.getZOID()); ResultSet rs = ps.executeQuery();
  if(rs.next()){ o.setNVersion(new ZObject(rs)); } else o.setNVersion(null);
 }finally{closePS(ps);}
} //

public void loadDObjectLVersion(DObject o)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(zm.getSQL(zm.TISQL_LOAD_ZO_LVERSION));
 try{ Record.setPSLong(ps,1,o.getZOID()); ResultSet rs = ps.executeQuery();
  if(rs.next()){ o.setLVersion(new ZObject(rs)); } else o.setLVersion(null);
 }finally{closePS(ps);}
} //

public void loadDObjectIVersion(DObject o)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(zm.getSQL(zm.TISQL_LOAD_ZO_IVERSION));
 try{ Record.setPSLong(ps,1,o.getZOID()); ResultSet rs = ps.executeQuery();
  if(rs.next()){ o.setIVersion(new ZObject(rs)); } else o.setIVersion(null);
 }finally{closePS(ps);}
} //

public void loadDObjectZNAME(DObject o)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(zm.getSQL(zm.TISQL_LOAD_ZNAME));
 try{ Record.setPSLong(ps,1,o.getZOID()); ResultSet rs = ps.executeQuery();
  if(rs.next()){ o.setZNAME(Record.getRSString(rs,ZName.F_ZNAME)); }
  else o.setZNAME(null);
 }finally{closePS(ps);}
} //

public RList loadDORecords(DORecord factory, Long ZOID)
 throws java.sql.SQLException,ZException
{ RList rl = new RList(); loadDORecords(rl,factory,ZOID); return(rl); }

public void loadDORecords(RList rl, DORecord factory, Long ZOID)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(factory.getTISQL_LOAD());
 try{
  Record.setPSLong(ps,1,ZOID);
  ResultSet rs = ps.executeQuery();
  while(rs.next()){ rl.add(factory.createFromRS(rs)); }
 }finally{closePS(ps);}
} //

public RList loadDORecordsIntermediate(DORecord factory, Long ZOID)
 throws java.sql.SQLException,ZException
{ RList rl = new RList(); loadDORecordsIntermediate(rl,factory,ZOID); return(rl); }

public void loadDORecordsIntermediate(RList rl, DORecord factory, Long ZOID)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(factory.getTISQL_LOAD_INTERMEDIATE());
 try{
  Record.setPSLong(ps,1,ZOID);
  ResultSet rs = ps.executeQuery();
  while(rs.next()){ rl.add(factory.createFromRS(rs)); }
 }finally{closePS(ps);}
} //

public void loadDObjectData(DObject o)
 throws java.sql.SQLException,ZException
{loadDObjectData(o,null);}

public void loadDObjectData(DObject o, Long ZVER)
 throws java.sql.SQLException,ZException
{
 DORecord[] dor = o.createMembersFactory();
 int i;
 if(ZVER != null) throw(new ZException(zm.getMsg(zm.ERR_NOT_IMPLEMENTED)) );
 for(i=0;i<dor.length; i++)
  {
   // if(ZVER != null)
   // loadDORecordsHistory(o.createTable(dor[i].getTabCode()),
   //	dor[i],o.getZOID(),ZVER); 
   // else
   loadDORecords(o.createTable(dor[i].getTabCode()),dor[i],o.getZOID()); 
  }
} //loadDObjectData

public void loadDObjectDataIntermediate(DObject o)
 throws java.sql.SQLException,ZException
{loadDObjectDataIntermediate(o,null);}

public void loadDObjectDataIntermediate(DObject o, Long ZVER)
 throws java.sql.SQLException,ZException
{
 DORecord[] dor = o.createMembersFactory();
 int i;
 if(ZVER != null) throw(new ZException(zm.getMsg(zm.ERR_NOT_IMPLEMENTED)) );
 for(i=0;i<dor.length; i++)
  { loadDORecordsIntermediate(o.createTable(dor[i].getTabCode()),
	dor[i],o.getZOID()); }
} //loadDObjectDataIntermediate

public void saveDObject(DObject o, boolean do_commit)
 throws java.sql.SQLException,ZException
{
 DORecord[] factory;
 DORecord r;
 ZExecStatus es;
 int i,k;
 if(o.getZOID() == null) {createDObject(o);} // new DObject
 if(!o.isOpened()){openDObject(o);}
 Long ZOID = o.getZOID(), ZVER = o.getZVER();
 try{ //save DObject's data
  factory = o.createMembersFactory();
  o.organizeTables();
  for(i=0;i<factory.length;i++)
  {
   RList rl = o.getTable(factory[i].getTabCode());
   if(rl == null) continue;
   Vector v_del = new Vector(), v_upd = new Vector(), v_new = new Vector();
   java.sql.PreparedStatement ps=null;
   java.sql.ResultSet rs=null;
   for(k=0;k<rl.size();k++)
   {
    r = (DORecord)rl.get(k);
    if(r.isMarkedForDeletion()) { v_del.add(r); } //delete record
    else if (r.ZRID != null) { v_upd.add(r); } // update record
    else { v_new.add(r); } // create new record
   } // for rl
   // do deletion
   if(v_del.size() > 0) try{
    ps = getPS4TISQL(factory[i].getTISQL_DELETE());
    for(k=0;k<v_del.size();k++)
    {
     r = (DORecord)v_del.get(k);
     if(r.ZRID == null) { r.commitDeletion(); continue; }
     DORecord.setPSLong(ps,1,ZOID); DORecord.setPSLong(ps,2,ZVER);
     DORecord.setPSLong(ps,3,r.ZRID); //r.putToPS(ps);
     es = new ZExecStatus(ps.executeQuery());
     checkExecStatus(es);
     r.commitDeletion(); closeRS(rs);
    } // for v_del
   } finally{closePS(ps);}
   // do creation
   if(v_new.size() > 0) try{
    ps = getPS4TISQL(factory[i].getTISQL_CREATE());
    for(k=0;k<v_new.size();k++)
    {
     r = (DORecord)v_new.get(k);
     if(r.isDeleted()) { continue; }
     DORecord.setPSLong(ps,1,ZOID); DORecord.setPSLong(ps,2,ZVER);
     DORecord.setPSLong(ps,3,r.ZPID); r.putToPS(ps);
     es = new ZExecStatus(ps.executeQuery());
     checkExecStatus(es);
     r.setZRID(es.ZID); closeRS(rs);
    } //for v_new
   } finally{closePS(ps);}
   // do update
   if(v_upd.size() > 0) try{
    ps = getPS4TISQL(factory[i].getTISQL_UPDATE());
    for(k=0;k<v_upd.size();k++)
    {
     r = (DORecord)v_upd.get(k);
     if(r.isDeleted()) { continue; }
     DORecord.setPSLong(ps,1,ZOID); DORecord.setPSLong(ps,2,ZVER);
     DORecord.setPSLong(ps,3,r.ZRID); r.putToPS(ps);
     es = new ZExecStatus(ps.executeQuery());
     checkExecStatus(es);
     closeRS(rs);
    } //for v_upd
   } finally{closePS(ps);}
  } // for factory
 }
 finally{ o.removeCommittedDeletions(); }
 if(do_commit) commitDObject(o);
} // saveDObject(DObject o, boolean o_close)

public void deleteDObject(DObject o)
 throws java.sql.SQLException,ZException
{ throw(new ZException(zm,zm.ERR_NOT_IMPLEMENTED));}

public void createDObject(DObject o)
 throws java.sql.SQLException,ZException
{
 ZExecStatus es = null;
 es = create_object(o.getZTYPE(),o.getZSID(),o.getZLVL());
 checkExecStatus(es); // throws ZException
 o.setZOID(es.ZID); o.setZVER(es.ZVER);
 o.setZSTA(DObject.C_ZSTA_I);
} //createDObject

public void openDObject(DObject o)
 throws java.sql.SQLException,ZException
{
 ZExecStatus es = null;
 es = open_object(o.getZTYPE(),o.getZOID(),o.getZVER());
 checkExecStatus(es); // throws ZException
 o.setZVER(es.ZVER);
 o.setZSTA(DObject.C_ZSTA_I);
} //openDObject

public void commitDObject(DObject o)
 throws java.sql.SQLException,ZException
{
 ZExecStatus es = null;
 es = commit_object(o.getZTYPE(),o.getZOID(),o.getZVER());
 checkExecStatus(es); // throws ZException
 o.setZVER(es.ZVER);
 o.setZSTA(DObject.C_ZSTA_N);
} //commitDObject

public void rollbackDObject(DObject o)
 throws java.sql.SQLException,ZException
 {throw(new ZException(zm.getMsg(zm.ERR_NOT_IMPLEMENTED)));}

public void lockDObject(DObject o)
 throws java.sql.SQLException,ZException
 {throw(new ZException(zm.getMsg(zm.ERR_NOT_IMPLEMENTED)));}
public void unlockDObject(DObject o)
 throws java.sql.SQLException,ZException
 {throw(new ZException(zm.getMsg(zm.ERR_NOT_IMPLEMENTED)));}

// TIS BackEnd system calls:

/** assistance for open,lock, delete calls. */
private ZExecStatus tis_call_TOV(int SQL_id, String ZTYPE, Long ZOID, Long ZVER)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(zm.getSQL(SQL_id));
 ZExecStatus es;
 try{
    Record.setPSString(ps,1,ZTYPE);
    Record.setPSLong(ps,2,ZOID);
    Record.setPSLong(ps,3,ZVER);
    ResultSet rs = ps.executeQuery();
    es = new ZExecStatus(rs);
 }finally{closePS(ps);}
 return(es);
} // tis_call_TOV

/** assistance for rollback,unlock calls. */
private ZExecStatus tis_call_TOVF(int SQL_id, String ZTYPE, Long ZOID, Long ZVER, YesNo FORCE)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(zm.getSQL(SQL_id));
 ZExecStatus es;
 try{
    Record.setPSString(ps,1,ZTYPE);
    Record.setPSLong(ps,2,ZOID);
    Record.setPSLong(ps,3,ZVER);
    Record.setPSYesNo(ps,4,FORCE);
    ResultSet rs = ps.executeQuery();
    es = new ZExecStatus(rs);
 }finally{closePS(ps);}
 return(es);
} // tis_call_TOVF

/** assistance for move_object call. */
private ZExecStatus tis_call_TOVS(int SQL_id, String ZTYPE, Long ZOID, Long ZVER, Long ZSID)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(zm.getSQL(SQL_id));
 ZExecStatus es;
 try{
    Record.setPSString(ps,1,ZTYPE);
    Record.setPSLong(ps,2,ZOID);
    Record.setPSLong(ps,3,ZVER);
    Record.setPSLong(ps,4,ZSID);
    ResultSet rs = ps.executeQuery();
    es = new ZExecStatus(rs);
 }finally{closePS(ps);}
 return(es);
} // tis_call_TOVS

/** assistance for move_object call. */
private ZExecStatus tis_call_TOVL(int SQL_id, String ZTYPE, Long ZOID, Long ZVER, Short ZLVL)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(zm.getSQL(SQL_id));
 ZExecStatus es;
 try{
    Record.setPSString(ps,1,ZTYPE);
    Record.setPSLong(ps,2,ZOID);
    Record.setPSLong(ps,3,ZVER);
    Record.setPSShort(ps,4,ZLVL);
    ResultSet rs = ps.executeQuery();
    es = new ZExecStatus(rs);
 }finally{closePS(ps);}
 return(es);
} // tis_call_TOVL

/** TIS.create_qseq()
CREATE OR REPLACE FUNCTION TIS.create_qseq(
        v_QSID  bigint,
        v_QLVL  smallint,
        v_qstart        bigint,
        v_qend  bigint,
        v_qname varchar(32),
        v_descr varchar(127)
) RETURNS SAM.execstatus VOLATILE
*/

public ZExecStatus create_qseq(Long QSID, Short QLVL, Long qstart, Long qend, String qname, String descr)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(zm.getSQL(zm.TISQL_QSEQ_CREATE));
 ZExecStatus es;
 try{
    Record.setPSLong(ps,1,QSID);
    Record.setPSShort(ps,2,QLVL);
    Record.setPSLong(ps,3,qstart);
    Record.setPSLong(ps,4,qend);
    Record.setPSString(ps,5,qname);
    Record.setPSString(ps,6,descr);
    ResultSet rs = ps.executeQuery();
    es = new ZExecStatus(rs);
 }finally{closePS(ps);}
 return(es);
}

/** TIS.next_qseq() */
public ZExecStatus next_qseq(Long QOID)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(zm.getSQL(zm.TISQL_QSEQ_NEXT_BY_QOID));
 ZExecStatus es;
 try{
    Record.setPSLong(ps,1,QOID);
    ResultSet rs = ps.executeQuery();
    es = new ZExecStatus(rs);
 }finally{closePS(ps);}
 return(es);
}

public ZExecStatus next_qseq(Long QSID, String qname)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(zm.getSQL(zm.TISQL_QSEQ_NEXT_BY_QSID_QNAME));
 ZExecStatus es;
 try{
    Record.setPSLong(ps,1,QSID);
    Record.setPSString(ps,2,qname);
    ResultSet rs = ps.executeQuery();
    es = new ZExecStatus(rs);
 }finally{closePS(ps);}
 return(es);
}

/** TIS.commit_object() */
public ZExecStatus commit_object(String ZTYPE, Long ZOID, Long ZVER)
 throws java.sql.SQLException,ZException
{ return(tis_call_TOV(zm.TISQL_COMMIT_OBJECT,ZTYPE,ZOID,ZVER)); }

/** TIS.create_object() */
public ZExecStatus create_object(String ZTYPE, Long ZSID, Short ZLVL)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(zm.getSQL(zm.TISQL_CREATE_OBJECT));
 ZExecStatus es;
 try{
    Record.setPSString(ps,1,ZTYPE);
    Record.setPSLong(ps,2,ZSID);
    Record.setPSShort(ps,3,ZLVL);
    ResultSet rs = ps.executeQuery();
    es = new ZExecStatus(rs);
 }finally{closePS(ps);}
 return(es);
} //create_object

/** TIS.delete_object() */
public ZExecStatus delete_object(String ZTYPE, Long ZOID, Long ZVER)
 throws java.sql.SQLException,ZException
{ return(tis_call_TOV(zm.TISQL_DELETE_OBJECT,ZTYPE,ZOID,ZVER)); }

/** TIS.lock_object() */
public ZExecStatus lock_object(String ZTYPE, Long ZOID, Long ZVER)
 throws java.sql.SQLException,ZException
{ return(tis_call_TOV(zm.TISQL_LOCK_OBJECT,ZTYPE,ZOID,ZVER)); }

/** TIS.move_object() */
public ZExecStatus move_object(String ZTYPE, Long ZOID, Long ZVER, Long ZSID)
 throws java.sql.SQLException,ZException
{ return(tis_call_TOVS(zm.TISQL_MOVE_OBJECT,ZTYPE,ZOID,ZVER,ZSID)); }

/** TIS.open_object() */
public ZExecStatus open_object(String ZTYPE, Long ZOID, Long ZVER)
 throws java.sql.SQLException,ZException
{ return(tis_call_TOV(zm.TISQL_OPEN_OBJECT,ZTYPE,ZOID,ZVER)); } //open_object

/** TIS.rollback_object() */
public ZExecStatus rollback_object(String ZTYPE, Long ZOID, Long ZVER, YesNo FORCE)
 throws java.sql.SQLException,ZException
{ return(tis_call_TOVF(zm.TISQL_ROLLBACK_OBJECT,ZTYPE,ZOID,ZVER,FORCE)); } //rollback_object

/** TIS.set_slevel() */
public ZExecStatus set_slevel(String ZTYPE, Long ZOID, Long ZVER, Short ZLVL)
 throws java.sql.SQLException,ZException
{ return(tis_call_TOVL(zm.TISQL_SET_SLEVEL,ZTYPE,ZOID,ZVER,ZLVL)); } //set_slevel

/** TIS.set_vdate() */
public ZExecStatus set_vdate(DateTime ZDATO, YesNo FORCE)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(zm.getSQL(zm.TISQL_SET_VDATE));
 ZExecStatus es;
 try{
    Record.setPSDateTime(ps,1,ZDATO);
    Record.setPSYesNo(ps,2,FORCE);
    ResultSet rs = ps.executeQuery();
    es = new ZExecStatus(rs);
 }finally{closePS(ps);}
 return(es);
} //set_vdate

/** TIS.unlock_object() */
public ZExecStatus unlock_object(String ZTYPE, Long ZOID, Long ZVER, YesNo FORCE)
 throws java.sql.SQLException,ZException
{ return(tis_call_TOVF(zm.TISQL_UNLOCK_OBJECT,ZTYPE,ZOID,ZVER,FORCE)); } //unlock_object

//
// TISC subsystem (Area,Document,Container data objects manipulation)
//

public RList listTISCArea(Long ZSID, int LIMIT, long OFFSET)
 throws java.sql.SQLException,ZException
{
 RList ol,rl; //object list & record list
 ol = loadDObjectsList(zm.getSQL(zm.TISQL_LIST_ACTIVE_OBJECTS_WITH_ZNAME),
	ZSID,zm.DOBJ_TISC_AREA,LIMIT,OFFSET);
 return(ol);
} //

public DObject loadTISCArea(Long ZOID)
 throws java.sql.SQLException,ZException
{
DObject o = Area.newDObject();
o.setZOID(ZOID);
loadDObject(o);
return(o);
}

public RList listTISCContainer(Long ZSID, int LIMIT, long OFFSET)
 throws java.sql.SQLException,ZException
{
 RList ol,rl; //object list & record list
 ol = loadDObjectsList(zm.getSQL(zm.TISQL_LIST_ACTIVE_OBJECTS_WITH_ZNAME),
	ZSID,zm.DOBJ_TISC_CONTAINER,LIMIT,OFFSET);
 return(ol);
} //

public DObject loadTISCContainer(Long ZOID)
 throws java.sql.SQLException,ZException
{
DObject o = Container.newDObject();
o.setZOID(ZOID);
loadDObject(o);
return(o);
}

public RList listTISCDocument(Long ZSID, int LIMIT, long OFFSET)
 throws java.sql.SQLException,ZException
{
 RList ol,rl; //object list & record list
 ol = loadDObjectsList(zm.getSQL(zm.TISQL_LIST_ACTIVE_OBJECTS_WITH_ZNAME),
	ZSID,zm.DOBJ_TISC_DOCUMENT,LIMIT,OFFSET);
 return(ol);
} //

public DObject loadTISCDocument(Long ZOID)
 throws java.sql.SQLException,ZException
{
DObject o = Document.newDObject();
o.setZOID(ZOID);
loadDObject(o);
return(o);
}

//
// FS subsystem (File)
//
public RList listFSFile(Long ZSID, int LIMIT, long OFFSET)
 throws java.sql.SQLException,ZException
{
 RList ol,rl; //object list & record list
 ol = loadDObjectsList(zm.getSQL(zm.TISQL_LIST_ACTIVE_OBJECTS_WITH_ZNAME),
	ZSID,File.DOBJECT_CODE,LIMIT,OFFSET); //zm.DOBJ_FS_FILE
 return(ol);
} //

public DObject loadFSFile(Long ZOID)
 throws java.sql.SQLException,ZException
{
DObject o = File.newDObject();
o.setZOID(ZOID);
loadDObject(o);
return(o);
}

//
// QR subsystem (Member)
//
public RList listQRMember(Long ZSID, int LIMIT, long OFFSET)
 throws java.sql.SQLException,ZException
{
 RList ol; //object list & record list
 ol = loadDObjectsList(zm.getSQL(zm.TISQL_LIST_ACTIVE_OBJECTS_WITH_ZNAME),
	ZSID,Member.DOBJECT_CODE,LIMIT,OFFSET); //zm.DOBJ_FS_FILE
 return(ol);
} //

public DObject loadQRMember(Long ZOID)
 throws java.sql.SQLException,ZException
{
DObject o = Member.newDObject();
o.setZOID(ZOID); loadDObject(o); return(o);
}
//QR.Product
public RList listQRProduct(Long ZSID, int LIMIT, long OFFSET)
 throws java.sql.SQLException,ZException
{
 RList ol,rl; //object list & record list
 ol = loadDObjectsList(zm.getSQL(zm.TISQL_LIST_ACTIVE_OBJECTS_WITH_ZNAME),
	ZSID,Product.DOBJECT_CODE,LIMIT,OFFSET); //zm.DOBJ_FS_FILE
 return(ol);
} //

public DObject loadQRProduct(Long ZOID)
 throws java.sql.SQLException,ZException
{
DObject o = Product.newDObject();
o.setZOID(ZOID); loadDObject(o); return(o);
}
//QR.Card
public RList listQRCard(Long ZSID, int LIMIT, long OFFSET)
 throws java.sql.SQLException,ZException
{
 RList ol,rl; //object list & record list
 ol = loadDObjectsList(zm.getSQL(zm.TISQL_LIST_ACTIVE_OBJECTS_WITH_ZNAME),
	ZSID,Card.DOBJECT_CODE,LIMIT,OFFSET); //zm.DOBJ_FS_FILE
 return(ol);
} //

public DObject loadQRCard(Long ZOID)
 throws java.sql.SQLException,ZException
{
DObject o = Card.newDObject();
o.setZOID(ZOID); loadDObject(o); return(o);
}
//QR.QR
public RList listQRQR(Long ZSID, int LIMIT, long OFFSET)
 throws java.sql.SQLException,ZException
{
 RList ol,rl; //object list & record list
 ol = loadDObjectsList(zm.getSQL(zm.TISQL_LIST_ACTIVE_OBJECTS_WITH_ZNAME),
	ZSID,QR.DOBJECT_CODE,LIMIT,OFFSET); //zm.DOBJ_FS_FILE
 return(ol);
} //

public DObject loadQRQR(Long ZOID)
 throws java.sql.SQLException,ZException
{
DObject o = QR.newDObject();
o.setZOID(ZOID); loadDObject(o); return(o);
}

//
// DObject's references listing methods
//

/** load list of ZNAME records for set of idfield values. */
public RList listZNAMEs4IDFIELD(String subsys,String table,
	String idfield,Long ZOID)
 throws java.sql.SQLException,ZException
{
 PreparedStatement ps = getPS4TISQL(zm.getSQL(zm.TISQL_LOAD_ZNAMES4IDFIELD,
	new String[] {subsys,table,idfield}));
 RList rl = new RList();
 try{
  Record.setPSLong(ps,1,ZOID);
  ResultSet rs = ps.executeQuery();
  while(rs.next()){ rl.add(new ZName(rs)); }
 }finally{closePS(ps);}
 return(rl);
} //listZNAMEs4IDFIELD

/** load list of rowfactory records for set of idfield values. */
public RList listRows4IDFIELD(DORecord rowfactory,
	String subsys,String table, String idfield,Long ZOID)
 throws java.sql.SQLException,ZException
{
 throw(new ZException(zm.getMsg(zm.ERR_NOT_IMPLEMENTED)));
} //listRows4IDFIELD

/** load list of rows for set of idfield values. */
public RList listRows4IDFIELD(String rowsubsys, String rowtable,
	String[] rowfields, String subsys, String table,
	String idfield, Long ZOID)
 throws java.sql.SQLException,ZException
{
 throw(new ZException(zm.getMsg(zm.ERR_NOT_IMPLEMENTED)));
} //listRows4IDFIELD

// constructors
public ZSystem(java.sql.Connection dbc)
{
 setDBC(dbc);
}

} //ZSystem
