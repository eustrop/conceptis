// ConcepTIS project
// (c) Alex V Eustrop 2009-2010
// see LICENSE at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao;

/** Set of ZField objects - representation of single row
 * and it's nested rows - collection of another ZFieldSet objects.
 *
 */
public class ZFieldSet extends Record
{

private String code;
private String key;
private boolean bMarkedForDeletion = false;
private RList flist = new RList();
private RList fslist = new RList();

// Record's class abstract methods implementation
public Long getID(){return(null);}
public String getKey(){return(key);}
public String getCaption(){return(getKey());}
//
public void setKey(String key){this.key=key;}

/** TIS code for this row (code of corresponding table). */
public String getCode(){return(code);}
public void setCode(String code){this.code=code;
	if(this.code!=null)this.code=this.code.intern();}

/** is this record marked for deletion while saving. */
public boolean getMarkedForDeletion(){return(bMarkedForDeletion);}
public void setMarkedForDeletion(boolean value){bMarkedForDeletion=value;}


/** number of fields in this set. */
public int size(){return(flist.size());}

/** number of fieldsets nested in this object. */
public int countFS(){return(fslist.size());}

public void add(ZField f) { flist.add(f); }
public void add(ZFieldSet fs) { fslist.add(fs); }

public ZField get(int i){ return( (ZField)flist.get(i) ); }
public ZField get(String name){ return( getByName(name)); }
public ZField getByName(String name)
 { ZField dc = (ZField)flist.getByKey(name); return(dc); }

/** get named field value as string.
 * #see ZField#getValue()
 */
public String getValue(String name)
{
 ZField f = get(name); if(f == null) return(null); return(f.getValue());
}

/** set named field value
 * #see ZField#setValue()
 */
public void setValue(String name, Object value)
{
 ZField f = get(name);
 if(f == null) add(new ZField(name,value));
 else f.setValue(value);
}

/** setFieldValue() if it's null only. */
public void replaceNullValue(String name, Object value)
{ if(getValue(name) == null) {setValue(name,value);} }

/** get nested ZFieldSet object by index. */
public ZFieldSet getFS(int i){ return( (ZFieldSet)fslist.get(i) ); }
/** get nested ZFieldSet object by key. */
public ZFieldSet getFS(String key){ return( getFSByKey(key)); }
/** get nested ZFieldSet object by key. */
public ZFieldSet getFSByKey(String key)
 { ZFieldSet dc = (ZFieldSet)fslist.getByKey(key); return(dc); }

/** import missed fields and missed metadata of all fields from
 * passed donor FieldSet.
 */
public void importMissedData(ZFieldSet fs)
{
 int i=0;
 for(i=0; i< fs.size(); i++)
 {
  ZField f = fs.get(i);
  ZField f2 = this.get(f.getName());
  if(f2 == null) { f2 = new ZField(f); this.add(f2); }
  else f2.importMissedData(f);
 }
} // importMissedData(ZFieldSet fs)

/** import missed metadata for all children using passed
 * DObject as the factory of master-records of all possible
 * member record types.
 *
 * @see DObject#createMembersFactory()
 */
public void importMissedData(DObject o)
{
 java.util.Hashtable htf = new java.util.Hashtable();
 ZFieldSet ffs;
 DORecord[] a;
 String key;
 int i,i2;
 // prepare factory
 a=o.createMembersFactory();
 for(i=0;i<a.length;i++){htf.put(a[i].getTabCode(),a[i].toFieldSet());}
 htf.put(o.getZTYPE(),o.toFieldSet());
 // process top-level fieldset
 key=this.getCode();
 if(key!=null)
  { ffs=(ZFieldSet)htf.get(key); if(ffs!=null) importMissedData(ffs); }
 // process children
 java.util.Vector allFS = new java.util.Vector();
 allFS.add(this);
 i2=0;
 while(i2<allFS.size())
 {
  ZFieldSet tmpFS = (ZFieldSet)allFS.get(i2++);
  i=0;
  while(i<tmpFS.countFS())
  {
   ZFieldSet fsr=tmpFS.getFS(i++); //get&step fwd
   key=fsr.getCode();
   if(key!=null)
     { ffs=(ZFieldSet)htf.get(key); if(ffs!=null) fsr.importMissedData(ffs); }
   if(fsr.countFS()>0) allFS.add(fsr);
  } //tmpFS
 } //alFS
 allFS.clear();
} //importMissedData(DObject o)

/** sort list of children fieldsets and call the same method on each of them.
*/
public void sortChildrenFSRecursively()
{
if(fslist == null) return;
fslist.sort();
for(int i=0;i<fslist.size();i++)
 {ZFieldSet fs = (ZFieldSet)fslist.get(i); fs.sortChildrenFSRecursively();}
} //sortChildrenFSRecursively()

/** export context of this object including all nested objects to textual
 * representation for debuging.
 */
public String toDebugString()
{
return(toDebugString("0"));
}

public String toDebugString(String prefix)
{
StringBuffer sb = new StringBuffer();
int i;
sb.append("rec: " + prefix + ":");
sb.append("code: " + getCode() + ":");
sb.append("key: " + getKey() + "\n");
sb.append("MakedForDeletion: " + getMarkedForDeletion() + "\n");
for(i=0;i<size();i++)
{
 ZField f = get(i);
 sb.append("f:" + f.getName() + ":" + f.getCode() + ":" + f.getValue() + "\n" );
}
for(i=0;i<countFS();i++)
{
 ZFieldSet fs = getFS(i);
 String fs_prefix = prefix + "." + i;
 sb.append(fs.toDebugString(fs_prefix));
}
return(sb.toString());
//throw(new RuntimeException("NOT IMPLEMENTED"));
}

private static String json_key(String key)
{
StringBuffer sb = new StringBuffer();
sb.append(ZMessages.text2json(key)); sb.append(":");
return(sb.toString());
}

private static String json_keyln(String key, String value)
{
StringBuffer sb = new StringBuffer();
sb.append(ZMessages.text2json(key)); sb.append(":"); sb.append(ZMessages.text2json(value));
sb.append(",\n"); return(sb.toString());
}

private static String json_key(String key, String value)
{
StringBuffer sb = new StringBuffer();
sb.append(ZMessages.text2json(key)); sb.append(":"); sb.append(ZMessages.text2json(value));
sb.append(", "); return(sb.toString());
}
private static String json_key_last(String key, String value)
{
StringBuffer sb = new StringBuffer();
sb.append(ZMessages.text2json(key)); sb.append(":"); sb.append(ZMessages.text2json(value));
sb.append(" "); return(sb.toString());
}

public String toJSONString() { return toJSONString(""); }

public String toJSONString(String prepend)
{
StringBuffer sb = new StringBuffer();
int i;
sb.append(prepend); sb.append("{\n");
sb.append(prepend + json_key("key", getKey()));
sb.append(prepend + json_keyln("code",getCode()));
sb.append(prepend + json_key("fields"));
sb.append(" [\n");
for(i=0;i<size();i++)
{
 ZField f = get(i);
 String field_name = f.getName();
 String field_code = f.getCode();
sb.append(prepend); sb.append("{");
 sb.append(json_key( "name" , field_name ));
 if(field_name == null) field_name = "";
 if( !field_name.equals(field_code))
   sb.append(json_key( "code" , f.getCode() ));
 //sb.append(json_key( "length" , "" + f.getLength() ));
 //sb.append(json_key( "type" , "" + f.getType() ));
 sb.append(json_key_last( "value" , f.getValue() ));
 //sb.append(json_key_last( "fcaption" , f.getFCaption() ));
sb.append("}");
if((i+1)<size())sb.append(",");
sb.append("\n");
}
sb.append(prepend);
sb.append("]");
if(countFS() > 0) {
 sb.append(",\n");
 sb.append(prepend);
 sb.append(json_key("fieldsets"));
 sb.append(" [\n");
 for(i=0;i<countFS();i++)
 {
  ZFieldSet fs = getFS(i);
  sb.append(prepend);
  sb.append(fs.toJSONString(prepend + " "));
  if((i+1)<countFS())sb.append(",");
  sb.append("\n");
 }
 sb.append(prepend);
 sb.append("]\n");
} // if(countFS() > 0)
else { sb.append("\n"); }
//
sb.append(prepend); sb.append("}");
return(sb.toString());
} // toJSONString(String prefix)

public ZJson toZJson()
{
 ZJson json = new ZJson(ZJson.ZJSON_TYPE_OBJECT);
 ZJson fields = new ZJson(ZJson.ZJSON_TYPE_ARRAY);
 ZJson fieldsets = new ZJson(ZJson.ZJSON_TYPE_ARRAY);
 int i;
 json.addItem("key",getKey());
 json.addItem("code",getCode());
 for(i=0;i<size();i++) { fields.addItem(get(i).toZJson()); }
 if(fields.size() > 0) json.addItem("fields",fields);
 for(i=0;i<countFS();i++) { fieldsets.addItem(getFS(i).toZJson()); }
 if(fieldsets.size() > 0) json.addItem("fieldsets",fieldsets);
 return(json);
} // toZJson()

/** import data from current row of SQL ResultSet.
 * Warning! some special field names are uppercased (ZOID,ZRID,...,ZNAME).
 */
public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 java.sql.ResultSetMetaData rsmd = rs.getMetaData();
 int i;
 for(i=1;i<=rsmd.getColumnCount();i++)
 {
  String fname = rsmd.getColumnName(i);
  String fname_up = fname.toUpperCase();
  if(fname_up.startsWith("Z")) fname = fname_up.intern();
  Object fvalue = rs.getObject(i);
  add(new ZField(fname,fvalue));
 }
} //


// Constructors
public ZFieldSet() { }

} //ZFieldSet
