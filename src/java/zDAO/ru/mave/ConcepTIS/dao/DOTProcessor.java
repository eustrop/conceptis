// ConcepTIS project
// (c) Alex V Eustrop 2009-2011
// see LICENSE at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao;

/** Abstract class (may be interface in the future) for definition
 * of methods required by DObject to proper handling of concrete
 * data object type.
 *
 *
 * @see DObject
 * @see #DObject.setDOTProcessor()
 * @see #DObject.getDOTProcessor()
 */

public abstract class DOTProcessor
{
/** code of dobject type (A,C,D etc). */
public abstract String getDOTypeCode();

/** name of dobject type (Area,Container,Document etc. */
public abstract String getDOTypeName();

/** code of subsystem holding this dobject type (TISC for now
 * but SAM,DIC,TIS possible variants in the future and BILL,STOR,
 * ACCNT is reasonable candidates for separate billing, storage and
 * accounting subsystems for complex ERP system).
 */
public abstract String getDOTypeSubsys();

/** (new) array of all types of records allowed for this
  * type of data object.
  */
public abstract DORecord[] createMembersFactory();

/** semi-abstaract semi-static dobject-type-specific caption
 * construction tool.
 */
public abstract String makeCaption(DObject o);

} //DOTProcessor

