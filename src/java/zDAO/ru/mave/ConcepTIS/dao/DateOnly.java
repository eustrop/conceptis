// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao;

/** Storage for Date
 */
public class DateOnly extends java.sql.Date
{
// constructors
public DateOnly(){this(0);}
public DateOnly(long time){super(time);}
public DateOnly(java.util.Date time) {super(time.getDate());}
public DateOnly(java.sql.Date time)
{super(0); if(time!=null){ setTime(time.getTime()); } }
public DateOnly(String time)
 {this(java.sql.Date.valueOf(time));}

} //DateOnly
