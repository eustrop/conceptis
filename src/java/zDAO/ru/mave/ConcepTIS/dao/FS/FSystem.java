// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao.FS;

import java.util.*;
import java.sql.*;
import ru.mave.ConcepTIS.dao.*;
import ru.mave.ConcepTIS.dao.SAM.*;
import ru.mave.ConcepTIS.dao.TIS.*;
//import ru.mave.ConcepTIS.dao.TISC.*;

/** root super-object for FS subsystem
 *
 * @see TISC.Area
 */
public class FSystem
{
//private java.sql.Connection DBC = null; //
//private ZMessages zm = new ZMessages();
private ZSystem qsys = null;


public String[] splitPath(String path){return(null);}

public RList ls(String path){return(null);};
public void ln(Long QOID, String name,String mimetype,String descr){};
public RList lsRoots(){return(null);};
public RList setRoot(Long QOID){return(null);};
public DObject mkdir(String name){return(null);};
public DObject mkfile(String name){return(null);};
public void rm(String name){};
public void cd(String path){};
public DObject open(String name){return(null);};
public void close(DObject f){};
public void commit(DObject f){};
public void rollback(DObject f){};
public byte[] read(DObject f,long row_no){return(null);};
public void write(DObject f, byte[] blob,Long no, Long size, int crc32){};
public void writeHEX(DObject f, String hex_bytea,Long no, Long size, int crc32){};

public java.sql.Connection getDBC(){if(qsys==null)return(null);return(qsys.getDBC());}
//
FSystem(ZSystem s){qsys=s;}
} //FSystem
