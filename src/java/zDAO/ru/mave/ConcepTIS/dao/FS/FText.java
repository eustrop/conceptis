// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2011
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
// WARNING! this code produced by automatic codegeneration tool
//          located at src/sql/PGSQL/codegen/tools/codegen.awk
//          In most cases you should'n change this file, but the above one.
//
// $Id$
//

package ru.mave.ConcepTIS.dao.FS;

import ru.mave.ConcepTIS.dao.*;
import java.math.BigDecimal;

/** FS.FText table.
 *
 */
public class FText extends DORecord
{
public String	titlekey;
public String	chunk;

public String getCaption(){return(null);}

// Strings

public final static String TAB_CODE = "FT";
public final static String TAB_NAME = "FText";
public final static String SUBSYS_CODE = "FS";

public final static String F_TITLEKEY = "titlekey";
public final static String F_CHUNK = "chunk";

public final static String FC_TITLEKEY = "FS.FT01";
public final static String FCS_TITLEKEY = "01";
public final static String FC_CHUNK = "FS.FT02";
public final static String FCS_CHUNK = "02";

// children management

public DORecord[] createChildrenFactory()
{
 return(new DORecord[0]);
}

public String[] getChildTabCodes()
{
 return(new String[0]);
}

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 super.getFromRS(rs);
 titlekey = getRSString(rs,8);
 chunk = getRSString(rs,9);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 super.getFromFS(fs);
 titlekey = getFSString(fs,F_TITLEKEY);
 chunk = getFSString(fs,F_CHUNK);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSString(ps,4,titlekey);
 setPSString(ps,5,chunk);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = super.toFieldSet();
 fs.add(new ZField(F_TITLEKEY,titlekey,FC_TITLEKEY,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_CHUNK,chunk,FC_CHUNK,ZField.T_UNKNOWN,0));
 return(fs);
} //

public String getSubsysCode(){return(SUBSYS_CODE);}
public String getTabCode(){return(TAB_CODE);}
public String getTabName(){return(TAB_NAME);}
public String getParentTabCode(){return(FFile.TAB_CODE);}
public int getNumOfFields() {return(2);}

public DORecord createFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
 {return(new FText(rs));}
public DORecord createFromFS(ZFieldSet fs)
 throws ZException
 {return(new FText(fs));};
public DORecord createInstance(){return(new FText());}

// constructors
public FText() { }
public FText(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public FText(java.sql.ResultSet rs)
 throws java.sql.SQLException { this.getFromRS(rs); }

} //FText
