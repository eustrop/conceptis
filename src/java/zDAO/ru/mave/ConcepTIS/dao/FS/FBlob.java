// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2011
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
// WARNING! this code produced by automatic codegeneration tool
//          located at src/sql/PGSQL/codegen/tools/codegen.awk
//          In most cases you should'n change this file, but the above one.
//
// $Id$
//

package ru.mave.ConcepTIS.dao.FS;

import ru.mave.ConcepTIS.dao.*;
import java.math.BigDecimal;

/** FS.FBlob table.
 *
 */
public class FBlob extends DORecord
{
public String	chunk;
public Long	no;
public Long	size;
public Integer	crc32;

public String getCaption(){return(null);}

// Strings

public final static String TAB_CODE = "FB";
public final static String TAB_NAME = "FBlob";
public final static String SUBSYS_CODE = "FS";

public final static String F_CHUNK = "chunk";
public final static String F_NO = "no";
public final static String F_SIZE = "size";
public final static String F_CRC32 = "crc32";

public final static String FC_CHUNK = "FS.FB01";
public final static String FCS_CHUNK = "01";
public final static String FC_NO = "FS.FB02";
public final static String FCS_NO = "02";
public final static String FC_SIZE = "FS.FB03";
public final static String FCS_SIZE = "03";
public final static String FC_CRC32 = "FS.FB04";
public final static String FCS_CRC32 = "04";

// children management

public DORecord[] createChildrenFactory()
{
 return(new DORecord[0]);
}

public String[] getChildTabCodes()
{
 return(new String[0]);
}

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 super.getFromRS(rs);
 chunk = getRSString(rs,8);
 no = getRSLong(rs,9);
 size = getRSLong(rs,10);
 crc32 = getRSInteger(rs,11);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 super.getFromFS(fs);
 chunk = getFSString(fs,F_CHUNK);
 no = getFSLong(fs,F_NO);
 size = getFSLong(fs,F_SIZE);
 crc32 = getFSInteger(fs,F_CRC32);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSString(ps,4,chunk);
 setPSLong(ps,5,no);
 setPSLong(ps,6,size);
 setPSInteger(ps,7,crc32);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = super.toFieldSet();
 fs.add(new ZField(F_CHUNK,chunk,FC_CHUNK,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_NO,no,FC_NO,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SIZE,size,FC_SIZE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_CRC32,crc32,FC_CRC32,ZField.T_UNKNOWN,0));
 return(fs);
} //

public String getSubsysCode(){return(SUBSYS_CODE);}
public String getTabCode(){return(TAB_CODE);}
public String getTabName(){return(TAB_NAME);}
public String getParentTabCode(){return(FFile.TAB_CODE);}
public int getNumOfFields() {return(4);}

public DORecord createFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
 {return(new FBlob(rs));}
public DORecord createFromFS(ZFieldSet fs)
 throws ZException
 {return(new FBlob(fs));};
public DORecord createInstance(){return(new FBlob());}

// constructors
public FBlob() { }
public FBlob(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public FBlob(java.sql.ResultSet rs)
 throws java.sql.SQLException { this.getFromRS(rs); }

} //FBlob
