// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2011
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
// WARNING! this code produced by automatic codegeneration tool
//          located at src/sql/PGSQL/codegen/tools/codegen.awk
//          In most cases you should'n change this file, but the above one.
//
// $Id$
//

package ru.mave.ConcepTIS.dao.FS;

import ru.mave.ConcepTIS.dao.*;
import java.math.BigDecimal;

/** FS.FFile table.
 *
 */
public class FFile extends DORecord
{
public String	name;
public String	type;
public YesNo	extstore;
public String	mimetype;
public String	descr;
public Long	b_size;
public Long	b_chcnt;
public String	b_algo;
public String	b_digest;
public Long	t_size;
public Long	t_chcnt;
public String	t_algo;
public String	t_digest;

public String getCaption(){return(null);}

// Strings

public final static String TAB_CODE = "FF";
public final static String TAB_NAME = "FFile";
public final static String SUBSYS_CODE = "FS";

public final static String F_NAME = "name";
public final static String F_TYPE = "type";
public final static String F_EXTSTORE = "extstore";
public final static String F_MIMETYPE = "mimetype";
public final static String F_DESCR = "descr";
public final static String F_B_SIZE = "b_size";
public final static String F_B_CHCNT = "b_chcnt";
public final static String F_B_ALGO = "b_algo";
public final static String F_B_DIGEST = "b_digest";
public final static String F_T_SIZE = "t_size";
public final static String F_T_CHCNT = "t_chcnt";
public final static String F_T_ALGO = "t_algo";
public final static String F_T_DIGEST = "t_digest";

public final static String FC_NAME = "FS.FF01";
public final static String FCS_NAME = "01";
public final static String FC_TYPE = "FS.FF02";
public final static String FCS_TYPE = "02";
public final static String FC_EXTSTORE = "FS.FF03";
public final static String FCS_EXTSTORE = "03";
public final static String FC_MIMETYPE = "FS.FF04";
public final static String FCS_MIMETYPE = "04";
public final static String FC_DESCR = "FS.FF05";
public final static String FCS_DESCR = "05";
public final static String FC_B_SIZE = "FS.FF06";
public final static String FCS_B_SIZE = "06";
public final static String FC_B_CHCNT = "FS.FF07";
public final static String FCS_B_CHCNT = "07";
public final static String FC_B_ALGO = "FS.FF08";
public final static String FCS_B_ALGO = "08";
public final static String FC_B_DIGEST = "FS.FF09";
public final static String FCS_B_DIGEST = "09";
public final static String FC_T_SIZE = "FS.FF11";
public final static String FCS_T_SIZE = "11";
public final static String FC_T_CHCNT = "FS.FF12";
public final static String FCS_T_CHCNT = "12";
public final static String FC_T_ALGO = "FS.FF13";
public final static String FCS_T_ALGO = "13";
public final static String FC_T_DIGEST = "FS.FF14";
public final static String FCS_T_DIGEST = "14";

// children management

public DORecord[] createChildrenFactory()
{
 DORecord dor[]=new DORecord[3];
 dor[0]=new FDir();
 dor[1]=new FBlob();
 dor[2]=new FText();
 return(dor);
}

public String[] getChildTabCodes()
{
 String c[]=new String[3];
 c[0]=FDir.TAB_CODE;
 c[1]=FBlob.TAB_CODE;
 c[2]=FText.TAB_CODE;
 return(c);
}

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 super.getFromRS(rs);
 name = getRSString(rs,8);
 type = rtrimString(getRSString(rs,9));
 extstore = getRSYesNo(rs,10);
 mimetype = getRSString(rs,11);
 descr = getRSString(rs,12);
 b_size = getRSLong(rs,13);
 b_chcnt = getRSLong(rs,14);
 b_algo = getRSString(rs,15);
 b_digest = getRSString(rs,16);
 t_size = getRSLong(rs,17);
 t_chcnt = getRSLong(rs,18);
 t_algo = getRSString(rs,19);
 t_digest = getRSString(rs,20);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 super.getFromFS(fs);
 name = getFSString(fs,F_NAME);
 type = getFSString(fs,F_TYPE);
 extstore = getFSYesNo(fs,F_EXTSTORE);
 mimetype = getFSString(fs,F_MIMETYPE);
 descr = getFSString(fs,F_DESCR);
 b_size = getFSLong(fs,F_B_SIZE);
 b_chcnt = getFSLong(fs,F_B_CHCNT);
 b_algo = getFSString(fs,F_B_ALGO);
 b_digest = getFSString(fs,F_B_DIGEST);
 t_size = getFSLong(fs,F_T_SIZE);
 t_chcnt = getFSLong(fs,F_T_CHCNT);
 t_algo = getFSString(fs,F_T_ALGO);
 t_digest = getFSString(fs,F_T_DIGEST);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSString(ps,4,name);
 setPSString(ps,5,type);
 setPSYesNo(ps,6,extstore);
 setPSString(ps,7,mimetype);
 setPSString(ps,8,descr);
 setPSLong(ps,9,b_size);
 setPSLong(ps,10,b_chcnt);
 setPSString(ps,11,b_algo);
 setPSString(ps,12,b_digest);
 setPSLong(ps,13,t_size);
 setPSLong(ps,14,t_chcnt);
 setPSString(ps,15,t_algo);
 setPSString(ps,16,t_digest);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = super.toFieldSet();
 fs.add(new ZField(F_NAME,name,FC_NAME,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_TYPE,type,FC_TYPE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_EXTSTORE,extstore,FC_EXTSTORE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_MIMETYPE,mimetype,FC_MIMETYPE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_DESCR,descr,FC_DESCR,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_B_SIZE,b_size,FC_B_SIZE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_B_CHCNT,b_chcnt,FC_B_CHCNT,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_B_ALGO,b_algo,FC_B_ALGO,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_B_DIGEST,b_digest,FC_B_DIGEST,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_T_SIZE,t_size,FC_T_SIZE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_T_CHCNT,t_chcnt,FC_T_CHCNT,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_T_ALGO,t_algo,FC_T_ALGO,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_T_DIGEST,t_digest,FC_T_DIGEST,ZField.T_UNKNOWN,0));
 return(fs);
} //

public String getSubsysCode(){return(SUBSYS_CODE);}
public String getTabCode(){return(TAB_CODE);}
public String getTabName(){return(TAB_NAME);}
public String getParentTabCode(){return(null);}
public int getNumOfFields() {return(13);}

public DORecord createFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
 {return(new FFile(rs));}
public DORecord createFromFS(ZFieldSet fs)
 throws ZException
 {return(new FFile(fs));};
public DORecord createInstance(){return(new FFile());}

// constructors
public FFile() { }
public FFile(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public FFile(java.sql.ResultSet rs)
 throws java.sql.SQLException { this.getFromRS(rs); }

} //FFile
