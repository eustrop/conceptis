// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2011
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
// WARNING! this code produced by automatic codegeneration tool
//          located at src/sql/PGSQL/codegen/tools/codegen.awk
//          In most cases you should'n change this file, but the above one.
//
// $Id$
//

package ru.mave.ConcepTIS.dao.FS;

import ru.mave.ConcepTIS.dao.*;
import java.math.BigDecimal;

/** FS.FDir table.
 *
 */
public class FDir extends DORecord
{
public Long	f_id;
public String	fname;
public String	mimetype;
public String	descr;

public String getCaption(){return(null);}

// Strings

public final static String TAB_CODE = "FD";
public final static String TAB_NAME = "FDir";
public final static String SUBSYS_CODE = "FS";

public final static String F_F_ID = "f_id";
public final static String F_FNAME = "fname";
public final static String F_MIMETYPE = "mimetype";
public final static String F_DESCR = "descr";

public final static String FC_F_ID = "FS.FD01";
public final static String FCS_F_ID = "01";
public final static String FC_FNAME = "FS.FD02";
public final static String FCS_FNAME = "02";
public final static String FC_MIMETYPE = "FS.FD03";
public final static String FCS_MIMETYPE = "03";
public final static String FC_DESCR = "FS.FD04";
public final static String FCS_DESCR = "04";

// children management

public DORecord[] createChildrenFactory()
{
 return(new DORecord[0]);
}

public String[] getChildTabCodes()
{
 return(new String[0]);
}

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 super.getFromRS(rs);
 f_id = getRSLong(rs,8);
 fname = getRSString(rs,9);
 mimetype = getRSString(rs,10);
 descr = getRSString(rs,11);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 super.getFromFS(fs);
 f_id = getFSLong(fs,F_F_ID);
 fname = getFSString(fs,F_FNAME);
 mimetype = getFSString(fs,F_MIMETYPE);
 descr = getFSString(fs,F_DESCR);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSLong(ps,4,f_id);
 setPSString(ps,5,fname);
 setPSString(ps,6,mimetype);
 setPSString(ps,7,descr);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = super.toFieldSet();
 fs.add(new ZField(F_F_ID,f_id,FC_F_ID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_FNAME,fname,FC_FNAME,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_MIMETYPE,mimetype,FC_MIMETYPE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_DESCR,descr,FC_DESCR,ZField.T_UNKNOWN,0));
 return(fs);
} //

public String getSubsysCode(){return(SUBSYS_CODE);}
public String getTabCode(){return(TAB_CODE);}
public String getTabName(){return(TAB_NAME);}
public String getParentTabCode(){return(FFile.TAB_CODE);}
public int getNumOfFields() {return(4);}

public DORecord createFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
 {return(new FDir(rs));}
public DORecord createFromFS(ZFieldSet fs)
 throws ZException
 {return(new FDir(fs));};
public DORecord createInstance(){return(new FDir());}

// constructors
public FDir() { }
public FDir(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public FDir(java.sql.ResultSet rs)
 throws java.sql.SQLException { this.getFromRS(rs); }

} //FDir
