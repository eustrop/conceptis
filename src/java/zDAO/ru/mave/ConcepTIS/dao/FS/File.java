// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2011
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
// WARNING! this code produced by automatic codegeneration tool
//          located at src/sql/PGSQL/codegen/tools/codegen_ot.awk
//          In most cases you should'n change this file, but the above one.
//
// $Id$
//

package ru.mave.ConcepTIS.dao.FS;

import ru.mave.ConcepTIS.dao.*;

/** FS.File data object.
 *
 */
public class File extends DOTProcessor
{

public final static String DOBJECT_CODE = "FS.F";
public final static String DOBJECT_CODE_SHORT = "F";
public final static String DOBJECT_NAME = "File";
public final static String SUBSYS_CODE = "FS";

public String getDOTypeCodeShort(){return(DOBJECT_CODE_SHORT);}
public String getDOTypeCode(){return(DOBJECT_CODE);}
public String getDOTypeName(){return(DOBJECT_NAME);}
public String getDOTypeSubsys(){return(SUBSYS_CODE);}

private static DOTProcessor dotp;

/** get common instance of this class. */
public static synchronized DOTProcessor getDOTProcessor()
{
if(dotp == null) dotp = new File();
return(dotp);
}

/** dobject with this DOTProcessor set. */
public static DObject newDObject()
{ return(new DObject(getDOTProcessor()) ); }

public DORecord[] createMembersFactory()
{
  DORecord[] dor = new DORecord[4];
  dor[0] = new FFile();
  dor[1] = new FDir();
  dor[2] = new FBlob();
  dor[3] = new FText();
  return(dor);
}

public String makeCaption(DObject o)
{ return(DObject.make_headrecord_caption(o,FFile.TAB_CODE));}

// constructors
private File(){}
} //File
