// ConcepTIS project
// (c) Alex V Eustrop 2009-2010
// see LICENSE at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao;

import java.sql.Types;
import java.math.BigDecimal;

/** any database record. comparable by getKey()
 *
 * @see RList
 */
public abstract class Record
implements Comparable
{

public static final String SZ_EMPTY="";

/** numeric id of record suitable as unique index
 * in some most useful subset of such records; null if not applicable.
 */
public abstract Long getID();

/** Textual key of record suitable as unique index
 * in some most useful subset of such records; null if not applicable.
 */
public abstract String getKey();

/** Textual informative caption for visual user iterface purpose.
 * it is not recomended to return null from this method (but not prohibited).
 */
public abstract String getCaption();

/** getCaption() on default (but this may be changed later).
 */
public String toString(){return(getCaption());}

/** (semi-abstract) convert Record to ZJson
 */
public ZJson toZJson(){ZJson j = new ZJson(); j.addItem("key",getKey()); j.addItem("id",getID()); j.addItem("caption",getCaption()); return(j);}

/** Instances of Record class are comparable by the value of getKey(), getID().
* null is greater than any non null and equals to itself (useful to place nulls
* at the end of list while sorting);
* getID() used only when both getKey() values are equal (both are null
* for instance)
* throws ClassCastException if non Record passed
*/
public int compareTo(Object o)
{
Record r = (Record)o; // throws ClassCastException if non Record passed
if(r==null) return(1);
if(r==this) return(0);
int cmp_status = cmp_keys(getKey(), r.getKey());
if(cmp_status == 0) cmp_status = cmp_keys(getID(), r.getID());
return(cmp_status);
} // compareTo

private int cmp_keys(Comparable my_key, Comparable its_key)
{
if(my_key == null) { if( its_key != null) return(1); else return(0); }
if(its_key == null) return(-1);
return(my_key.compareTo(its_key));
}

 //
 // static data manipulation methods
 //

 /** trim white spaces from sz but preserve it's null value if so.
  * helpful for CHAR SQL DATATYPE.
  */
 public static String trimString(String sz)
 {if(sz== null) return(null); return(sz.trim()); }

 /** (THIS IS JUST A trimString() YET!)
  * rtrim (right trim) white spaces from sz but preserve it's null value if so.
  * helpful for CHAR SQL DATATYPE.
  * @see #trimString
  */
 public static String rtrimString(String sz)
 {if(sz== null) return(null); return(sz.trim()); }

 /* hex to long and long 2 hex conversion */
 public static Long HEX2LongWithNFE(String v) throws NumberFormatException {if(v==null)return(null);Long n=null;n=Long.parseLong(v.trim(),16); return(n);}
 public static Long HEX2Long(String v){if(v==null)return(null);Long n=null;try{n=Long.parseLong(v.trim(),16);}catch(NumberFormatException nfe){} return(n);}
 public static long HEX2long(String v){Long l=HEX2Long(v); if(l==null)return(0);return(l.longValue());}
 public static String Long2HEX(Long v){if(v==null)return(null); return(long2HEX(v.longValue())); }
 public static String long2HEX(long v){return(String.format("%X",v));}


 /** get field value as String object or null if SQL NULL happens.
 * @param column column's index
 */
 public static String getRSString(java.sql.ResultSet rs, int column)
  throws java.sql.SQLException
 { String v = rs.getString(column); if(rs.wasNull()) v=null; return(v); }

 public static String getRSString(java.sql.ResultSet rs, String column)
  throws java.sql.SQLException
 { String v = rs.getString(column); if(rs.wasNull()) v=null; return(v); }

 /** put String field value into column PreparedStatement parameter.
 * @param column column's index
 */
 public static void setPSString(java.sql.PreparedStatement ps, int column, String v)
  throws java.sql.SQLException
 {if(v==null){ps.setNull(column,Types.VARCHAR);}else{ps.setString(column, v);}}

 /** get field value as BigDecimal object or null if SQL NULL happens.
 * @param column column's index
 */
 public static BigDecimal getRSBigDecimal(java.sql.ResultSet rs, int column)
  throws java.sql.SQLException
 { BigDecimal v = rs.getBigDecimal(column); if(rs.wasNull()) v=null; return(v); }

 public static BigDecimal getRSBigDecimal(java.sql.ResultSet rs, String column)
  throws java.sql.SQLException
 { BigDecimal v = rs.getBigDecimal(column); if(rs.wasNull()) v=null; return(v); }

 /** put BigDecimal field value into column PreparedStatement parameter.
 * @param column column's index
 */
 public static void setPSBigDecimal(java.sql.PreparedStatement ps, int column, BigDecimal v)
  throws java.sql.SQLException
 {if(v==null){ps.setNull(column,Types.DECIMAL);}else{ps.setBigDecimal(column, v);}}

 /** get field value as Long object or null if SQL NULL happens.
 * @param column column's index
 */
 public static Long getRSLong(java.sql.ResultSet rs, int column)
  throws java.sql.SQLException
 { Long v = new Long(rs.getLong(column)); if(rs.wasNull()) v=null; return(v); }

 /** get field value as Long object or null if SQL NULL happens.
 * @param column name of column
 */
 public static Long getRSLong(java.sql.ResultSet rs, String column)
  throws java.sql.SQLException
 { Long v = new Long(rs.getLong(column)); if(rs.wasNull()) v=null; return(v); }

 /** put Long field value into column PreparedStatement parameter.
 * @param column column's index
 */
 public static void setPSLong(java.sql.PreparedStatement ps, int column, Long v)
  throws java.sql.SQLException
 {if(v==null){ps.setNull(column,Types.BIGINT);}else{ps.setLong(column, v.longValue());}}

 /** get field value as Integer object or null if SQL NULL happens.
 * @param column column's index
 */
 public static Integer getRSInt(java.sql.ResultSet rs, int column)
  throws java.sql.SQLException
 { Integer v = new Integer(rs.getInt(column)); if(rs.wasNull()) v=null; return(v); }

 /** get field value as Integer object or null if SQL NULL happens.
 * @param column name of column
 */
 public static Integer getRSInt(java.sql.ResultSet rs, String column)
  throws java.sql.SQLException
 { Integer v = new Integer(rs.getInt(column)); if(rs.wasNull()) v=null; return(v); }

 /** put Integer field value into column PreparedStatement parameter.
 * @param column column's index
 */
 public static void setPSInt(java.sql.PreparedStatement ps, int column, Integer v)
  throws java.sql.SQLException
 {if(v==null){ps.setNull(column,Types.INTEGER);}else{ps.setInt(column, v.intValue());}}

 //
 // Int as Integer
 public static Integer getRSInteger(java.sql.ResultSet rs, int column)
  throws java.sql.SQLException { return(getRSInt(rs,column)); }
 public static Integer getRSInteger(java.sql.ResultSet rs, String column)
  throws java.sql.SQLException { return(getRSInt(rs,column)); }
 public static void setPSInteger(java.sql.PreparedStatement ps, int column, Integer v)
  throws java.sql.SQLException { setPSInt(ps,column,v); }
 public static Integer getFSInteger(ZFieldSet fs, String column)
  throws ZException { return(getFSInt(fs,column)); }

 /** get field value as Short object or null if SQL NULL happens.
 * @param column column's index
 */
 public static Short getRSShort(java.sql.ResultSet rs, int column)
  throws java.sql.SQLException
 { Short v = new Short(rs.getShort(column)); if(rs.wasNull()) v=null; return(v); }

 /** get field value as Short object or null if SQL NULL happens.
 * @param column name of column
 */
 public static Short getRSShort(java.sql.ResultSet rs, String column)
  throws java.sql.SQLException
 { Short v = new Short(rs.getShort(column)); if(rs.wasNull()) v=null; return(v); }

 /** put Short field value into column PreparedStatement parameter.
 * @param column column's index
 */
 public static void setPSShort(java.sql.PreparedStatement ps, int column, Short v)
  throws java.sql.SQLException
 {if(v==null){ps.setNull(column,Types.SMALLINT);}else{ps.setShort(column, v.shortValue());}}

 /** put DateTime field value into column PreparedStatement parameter.
 * @param column column's index
 */
 public static void setPSDateTime(java.sql.PreparedStatement ps, int column, DateTime v)
  throws java.sql.SQLException
 {if(v==null){ps.setNull(column,Types.DATE);}else{ps.setTimestamp(column, v);}}

 /** put DateOnly field value into column PreparedStatement parameter.
 * @param column column's index
 */
 public static void setPSDateOnly(java.sql.PreparedStatement ps, int column, DateOnly v)
  throws java.sql.SQLException
 {if(v==null){ps.setNull(column,Types.DATE);}else{ps.setDate(column, v);}}

 /** get field value as Boolean object or null if SQL NULL happens.
 * @param column column's index
 */
 public static Boolean getRSBoolean(java.sql.ResultSet rs, int column)
  throws java.sql.SQLException
 { Boolean v = new Boolean(rs.getBoolean(column)); if(rs.wasNull()) v=null; return(v); }

 /** get field value as Boolean object or null if SQL NULL happens.
 * @param column name of column
 */
 public static Boolean getRSBoolean(java.sql.ResultSet rs, String column)
  throws java.sql.SQLException
 { Boolean v = new Boolean(rs.getBoolean(column)); if(rs.wasNull()) v=null; return(v); }

 /** get field value as YesNo object or null if SQL NULL happens.
 * @see YesNo
 * @param column column's index
 */
 public static YesNo getRSYesNo(java.sql.ResultSet rs, int column)
  throws java.sql.SQLException
 { YesNo v = new YesNo(rs.getString(column)); if(rs.wasNull()) v=null; return(v); }

 /** get field value as YesNo object or null if SQL NULL happens.
 * @see YesNo
 * @param column name of column
 */
 public static YesNo getRSYesNo(java.sql.ResultSet rs, String column)
  throws java.sql.SQLException
 { YesNo v = new YesNo(rs.getString(column)); if(rs.wasNull()) v=null; return(v); }

 /** put YesNo code value into column PreparedStatement parameter.
 * @param column column's index
 */
 public static void setPSYesNo(java.sql.PreparedStatement ps, int column, YesNo v)
  throws java.sql.SQLException
 {if(v==null){ps.setNull(column,Types.VARCHAR);}else{ps.setString(column, v.getCode());}}

 /** get field value as Float object or null if SQL NULL happens.
 * @param column column's index
 */
 public static Float getRSFloat(java.sql.ResultSet rs, int column)
  throws java.sql.SQLException
 { Float v = new Float(rs.getFloat(column)); if(rs.wasNull()) v=null; return(v); }

 /** get field value as Float object or null if SQL NULL happens.
 * @param column name of column
 */
 public static Float getRSFloat(java.sql.ResultSet rs, String column)
  throws java.sql.SQLException
 { Float v = new Float(rs.getFloat(column)); if(rs.wasNull()) v=null; return(v); }

 /** get field value as Double object or null if SQL NULL happens.
 * @param column column's index
 */
 public static Double getRSDouble(java.sql.ResultSet rs, int column)
  throws java.sql.SQLException
 { Double v = new Double(rs.getDouble(column)); if(rs.wasNull()) v=null; return(v); }

 /** get field value as Double object or null if SQL NULL happens.
 * @param column name of column
 */
 public static Double getRSDouble(java.sql.ResultSet rs, String column)
  throws java.sql.SQLException
 { Double v = new Double(rs.getDouble(column)); if(rs.wasNull()) v=null; return(v); }

 /** put Double field value into column PreparedStatement parameter.
 * @param column column's index
 */
 public static void setPSDouble(java.sql.PreparedStatement ps, int column, Double v)
  throws java.sql.SQLException
 {if(v==null){ps.setNull(column,Types.DOUBLE);}else{ps.setDouble(column, v.doubleValue());}}

 /** get field value as DateTime object or null if SQL NULL happens. */
 public static DateTime getRSDateTime(java.sql.ResultSet rs, int column)
  throws java.sql.SQLException
 {
 DateTime v = new DateTime(rs.getTimestamp(column));
 if(rs.wasNull()) v=null; return(v);
 }

 public static DateTime getRSDateTime(java.sql.ResultSet rs, String column)
  throws java.sql.SQLException
 { DateTime v = new DateTime(rs.getTimestamp(column)); if(rs.wasNull()) v=null; return(v); }

 /** get field value as DateOnly object or null if SQL NULL happens. */
 public static DateOnly getRSDateOnly(java.sql.ResultSet rs, int column)
  throws java.sql.SQLException
 {
 DateOnly v = new DateOnly(rs.getDate(column));
 if(rs.wasNull()) v=null; return(v);
 }

 public static DateOnly getRSDateOnly(java.sql.ResultSet rs, String column)
  throws java.sql.SQLException
 { DateOnly v = new DateOnly(rs.getDate(column)); if(rs.wasNull()) v=null; return(v); }

 //
 // ZFieldSet data manipulation
 //

 /** get field value as String object from ZFieldSet.
 * @param column name of field
 */
 public static String getFSString(ZFieldSet fs, String column)
  throws ZException
 {
  String v = fs.getValue(column); if(v==null) return(null);
  // if(v.equals(SZ_NULL_AS_SQLNULL)) return(null);
  // if(SZ_NULL_AS_STRING.equals(v)) v=SZ_NULL_AS_SQLNULL;;
  return(v);
 }

 /** get field value as BigDecimal object from ZFieldSet.
 * @param column name of field
 */
 public static BigDecimal getFSBigDecimal(ZFieldSet fs, String column)
  throws ZException
 {
  String s = fs.getValue(column); if(s==null) return(null);
  s = s.trim(); // remove spaces
  if(SZ_EMPTY.equals(s)) return(null);
  BigDecimal v; try{ v = new BigDecimal(s); }
  catch(NumberFormatException e){throw(new ZException(e));}
  return(v);
 }

 /** get field value as Double object from ZFieldSet.
 * @param column name of field
 */
 public static Double getFSDouble(ZFieldSet fs, String column)
  throws ZException
 {
  String s = fs.getValue(column); if(s==null) return(null);
  s = s.trim(); // remove spaces
  if(SZ_EMPTY.equals(s)) return(null);
  Double v; try{ v = new Double(s); }
  catch(NumberFormatException e){throw(new ZException(e));}
  return(v);
 }

 /** get field value as Long object from ZFieldSet.
 * @param column name of field
 */
 public static Long getFSHEXLong(ZFieldSet fs, String column)
  throws ZException
 {
  String s = fs.getValue(column); if(s==null) return(null);
  s = s.trim(); // remove spaces
  if(SZ_EMPTY.equals(s)) return(null);
  Long v; try{ v = HEX2LongWithNFE(s); }
  catch(NumberFormatException e){throw(new ZException(e));}
  return(v);
 }

 /** get field value as Long object from ZFieldSet.
 * @param column name of field
 */
 public static Long getFSLong(ZFieldSet fs, String column)
  throws ZException
 {
  String s = fs.getValue(column); if(s==null) return(null);
  s = s.trim(); // remove spaces
  if(SZ_EMPTY.equals(s)) return(null);
  Long v; try{ v = new Long(s); }
  catch(NumberFormatException e){throw(new ZException(e));}
  return(v);
 }

 /** get field value as Int object from ZFieldSet.
 * @param column name of field
 */
 public static Integer getFSInt(ZFieldSet fs, String column)
  throws ZException
 {
  String s = fs.getValue(column); if(s==null) return(null);
  s = s.trim(); // remove spaces
  if(SZ_EMPTY.equals(s)) return(null);
  Integer v; try{ v = new Integer(s); }
  catch(NumberFormatException e){throw(new ZException(e));}
  return(v);
 }

 /** get field value as Short object from ZFieldSet.
 * @param column name of field
 */
 public static Short getFSShort(ZFieldSet fs, String column)
  throws ZException
 {
  String s = fs.getValue(column); if(s==null) return(null);
  s = s.trim(); // remove spaces
  if(SZ_EMPTY.equals(s)) return(null);
  Short v; try{ v = new Short(s); }
  catch(NumberFormatException e){throw(new ZException(e));}
  return(v);
 }

 /** get field value as YesNo object from ZFieldSet.
 * @param column name of field
 */
 public static YesNo getFSYesNo(ZFieldSet fs, String column)
  throws ZException
 {
  String s = fs.getValue(column); if(s==null) return(null);
  if(SZ_EMPTY.equals(s)) return(null);
  YesNo v; v = new YesNo(s);
  return(v);
 }

 /** get field value as DateTime object from ZFieldSet.
 * @param column name of field
 */
 public static DateTime getFSDateTime(ZFieldSet fs, String column)
  throws ZException
 {
  String s = fs.getValue(column); if(s==null) return(null);
  s = s.trim(); // remove spaces
  if(SZ_EMPTY.equals(s)) return(null);
  DateTime v; try{ v = new DateTime(s); }
  catch(IllegalArgumentException e){throw(new ZException(e));}
  return(v);
 }

 /** get field value as DateOnly object from ZFieldSet.
 * @param column name of field
 */
 public static DateOnly getFSDateOnly(ZFieldSet fs, String column)
  throws ZException
 {
  String s = fs.getValue(column); if(s==null) return(null);
  s = s.trim(); // remove spaces
  if(SZ_EMPTY.equals(s)) return(null);
  DateOnly v; try{ v = new DateOnly(s); }
  catch(IllegalArgumentException e){throw(new ZException(e));}
  return(v);
 }

} //Record
