// ConcepTIS project
// (c) Alex V Eustrop 2009-2010
// see LICENSE at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao.TIS;

import ru.mave.ConcepTIS.dao.*;

/** .
 *
 */
public class ZName extends Record
{
public Long	ZOID;
public String	ZTYPE;
public Long	ZSID;
public Short	ZLVL;
public String	ZNAME;

public Long getID(){return(ZOID);}
public String getKey(){return(ZNAME);}
public String getCaption(){return(getKey());}

// Strings

public final static String F_ZOID = "ZOID";
public final static String F_ZTYPE = "ZTYPE";
public final static String F_ZSID = "ZSID";
public final static String F_ZLVL = "ZLVL";
public final static String F_ZNAME = "ZNAME";

public final static String FC_ZOID = "ZN01";
public final static String FC_ZTYPE = "ZN02";
public final static String FC_ZSID = "ZN03";
public final static String FC_ZLVL = "ZN04";
public final static String FC_ZNAME = "ZN05";

// SQL requests
public final static String TISQL_LIST =
	"select * from TIS.V_ZName";
public final static String TISQL_LOAD =
	"select * from TIS.V_ZName where ? = ZOID";

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 ZOID = getRSLong(rs,1);
 ZTYPE = rtrimString(getRSString(rs,2));
 ZSID = getRSLong(rs,3);
 ZLVL = getRSShort(rs,4);
 ZNAME = getRSString(rs,5);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 ZOID = getFSLong(fs,F_ZOID);
 ZTYPE = getFSString(fs,F_ZTYPE);
 ZSID = getFSLong(fs,F_ZSID);
 ZLVL = getFSShort(fs,F_ZLVL);
 ZNAME = getFSString(fs,F_ZNAME);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSLong(ps,1,ZOID);
 setPSString(ps,2,ZTYPE);
 setPSLong(ps,3,ZSID);
 setPSShort(ps,4,ZLVL);
 setPSString(ps,5,ZNAME);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = new ZFieldSet();
 fs.add(new ZField(F_ZOID,ZOID,FC_ZOID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_ZTYPE,ZTYPE,FC_ZTYPE,ZField.T_UNKNOWN,1));
 fs.add(new ZField(F_ZSID,ZSID,FC_ZSID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_ZLVL,ZLVL,FC_ZLVL,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_ZNAME,ZNAME,FC_ZNAME,ZField.T_UNKNOWN,32));
 return(fs);
} //

// constructors
public ZName() { }
public ZName(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public ZName(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 this.getFromRS(rs);
} // ZName(java.sql.ResultSet rs)

} //ZName
