// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao.TIS;

import ru.mave.ConcepTIS.dao.*;

/** .
 *
 */
public class QSeq extends Record
{
public Long	QOID;
public Long	QVER;
public Long	QTOV;
public Long	QSID;
public Short	QLVL;
public Long	qstart;
public Long	qend;
public String	QNAME;
public String	descr;
public Long	last_qseq; //virtual field

public Long getID(){return(null);}
public String getKey(){return(null);}
public String getCaption(){return(getKey());}

// Strings

public final static String F_QOID = "QOID";
public final static String F_QVER = "QVER";
public final static String F_QTOV = "QTOV";
public final static String F_QSID = "QSID";
public final static String F_QLVL = "QLVL";
public final static String F_QSTART = "qstart";
public final static String F_QEND = "qend";
public final static String F_QNAME = "QNAME";
public final static String F_DESCR = "descr";
public final static String F_LAST_QSEQ = "last_qseq"; //virtual field

public final static String FC_QOID = "QS01";
public final static String FC_QVER = "QS02";
public final static String FC_QTOV = "QS03";
public final static String FC_QSID = "QS04";
public final static String FC_QLVL = "QS05";
public final static String FC_QSTART = "QS06";
public final static String FC_QEND = "QS07";
public final static String FC_QNAME = "QS08";
public final static String FC_DESCR = "QS09";
public final static String FC_LAST_QSEQ = "QS90"; //virtual field

// SQL requests
public final static String TISQL_LIST =
	"select *,TIS.last_qseq(QOID) last_qseq from TIS.V_QSeq";
public final static String TISQL_LIST_BY_QNAME =
	"select *,TIS.last_qseq(QOID) last_qseq from TIS.V_QSeq where QNAME = ?";
public final static String TISQL_LIST_BY_QSID =
	"select *,TIS.last_qseq(QOID) last_qseq from TIS.V_QSeq where QSID = ?";
public final static String TISQL_LOAD =
	"select *,TIS.last_qseq(QOID) last_qseq from TIS.V_QSeq where QOID = ?";
public final static String TISQL_CREATE =
	"select * from TIS.create_QSeq(?,?,?,?,?,?,?,?,?)";
public final static String TISQL_UPDATE =
	"select * from TIS.update_QSeq(?,?,?,?,?,?,?,?,?)";
public final static String TISQL_DELETE =
	"select * from TIS.delete_QSeq(?)";

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 QOID = getRSLong(rs,1);
 QVER = getRSLong(rs,2);
 QTOV = getRSLong(rs,3);
 QSID = getRSLong(rs,4);
 QLVL = getRSShort(rs,5);
 qstart = getRSLong(rs,6);
 qend = getRSLong(rs,7);
 QNAME = getRSString(rs,8);
 descr = getRSString(rs,9);
 // virtual fields
 last_qseq = getRSLong(rs,10);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 QOID = getFSLong(fs,F_QOID);
 QVER = getFSLong(fs,F_QVER);
 QTOV = getFSLong(fs,F_QTOV);
 QSID = getFSLong(fs,F_QSID);
 QLVL = getFSShort(fs,F_QLVL);
 qstart = getFSHEXLong(fs,F_QSTART);
 qend = getFSHEXLong(fs,F_QEND);
 QNAME = getFSString(fs,F_QNAME);
 descr = getFSString(fs,F_DESCR);
 // virtual fields
 last_qseq = getFSHEXLong(fs,F_LAST_QSEQ);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSLong(ps,1,QOID);
 setPSLong(ps,2,QVER);
 setPSLong(ps,3,QTOV);
 setPSLong(ps,4,QSID);
 setPSShort(ps,5,QLVL);
 setPSLong(ps,6,qstart);
 setPSLong(ps,7,qend);
 setPSString(ps,8,QNAME);
 setPSString(ps,9,descr);
 // virual fields
 // last_qseq - not set
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = new ZFieldSet();
 fs.add(new ZField(F_QOID,QOID,FC_QOID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_QVER,QVER,FC_QVER,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_QTOV,QTOV,FC_QTOV,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_QSID,QSID,FC_QSID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_QLVL,QLVL,FC_QLVL,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_QSTART,Long2HEX(qstart),FC_QSTART,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_QEND,Long2HEX(qend),FC_QEND,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_QNAME,QNAME,FC_QNAME,ZField.T_UNKNOWN,32));
 fs.add(new ZField(F_DESCR,descr,FC_DESCR,ZField.T_UNKNOWN,127));
 // virtual field:
 fs.add(new ZField(F_LAST_QSEQ,Long2HEX(last_qseq),FC_LAST_QSEQ,ZField.T_UNKNOWN,0));
 return(fs);
} //

// constructors
public QSeq() { }
public QSeq(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public QSeq(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 this.getFromRS(rs);
} // QSeq(java.sql.ResultSet rs)

} //QSeq
