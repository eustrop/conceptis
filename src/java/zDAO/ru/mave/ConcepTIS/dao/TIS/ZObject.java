// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao.TIS;

import ru.mave.ConcepTIS.dao.*;


/** TIS.ZObject data class.
 *
 */
public class ZObject extends Record
{
public Long	ZOID;
public Long	ZVER;
public String	ZTYPE;
public Long	ZSID;
public Short	ZLVL;
public Long	ZUID;
public String	ZSTA;
public DateTime	ZDATE;
public DateTime	ZDATO;
public Long	QRSQ;

public Long getID(){return(ZVER);}
public String getKey(){return(null);}
public String getCaption(){return("ZOID:" + ZOID);}

// Strings

public final static String F_ZOID = "ZOID";
public final static String F_ZVER = "ZVER";
public final static String F_ZTYPE = "ZTYPE";
public final static String F_ZSID = "ZSID";
public final static String F_ZLVL = "ZLVL";
public final static String F_ZUID = "ZUID";
public final static String F_ZSTA = "ZSTA";
public final static String F_ZDATE = "ZDATE";
public final static String F_ZDATO = "ZDATO";
public final static String F_QRSQ = "QRSQ";

public final static String FC_ZOID = "ZOID";
public final static String FC_ZVER = "ZVER";
public final static String FC_ZTYPE = "ZTYPE";
public final static String FC_ZSID = "ZSID";
public final static String FC_ZLVL = "ZLVL";
public final static String FC_ZUID = "ZUID";
public final static String FC_ZSTA = "ZSTA";
public final static String FC_ZDATE = "ZDATE";
public final static String FC_ZDATO = "ZDATO";
public final static String FC_QRSQ = "QRSQ";

// SQL requests
public final static String TISQL_LIST =
	"select * from TIS.V_ZObject";
public final static String TISQL_LOAD =
	"select * from TIS.V_ZObject where ? = null";
public final static String TISQL_CREATE =
	"select * from TIS.create_ZObject(?,?,?,?,?,?,?,?,?)";
public final static String TISQL_UPDATE =
	"select * from TIS.update_ZObject(?,?,?,?,?,?,?,?,?)";
public final static String TISQL_DELETE =
	"select * from TIS.delete_ZObject(?)";

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 ZOID = getRSLong(rs,1);
 ZVER = getRSLong(rs,2);
 ZTYPE = rtrimString(getRSString(rs,3));
 ZSID = getRSLong(rs,4);
 ZLVL = getRSShort(rs,5);
 ZUID = getRSLong(rs,6);
 ZSTA = rtrimString(getRSString(rs,7));
 ZDATE = getRSDateTime(rs,8);
 ZDATO = getRSDateTime(rs,9);
 QRSQ = getRSLong(rs,10);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 ZOID = getFSLong(fs,F_ZOID);
 ZVER = getFSLong(fs,F_ZVER);
 ZTYPE = getFSString(fs,F_ZTYPE);
 ZSID = getFSLong(fs,F_ZSID);
 ZLVL = getFSShort(fs,F_ZLVL);
 ZUID = getFSLong(fs,F_ZUID);
 ZSTA = getFSString(fs,F_ZSTA);
 ZDATE = getFSDateTime(fs,F_ZDATE);
 ZDATO = getFSDateTime(fs,F_ZDATO);
 QRSQ = getFSLong(fs,F_QRSQ);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSLong(ps,1,ZOID);
 setPSLong(ps,2,ZVER);
 setPSString(ps,3,ZTYPE);
 setPSLong(ps,4,ZSID);
 setPSShort(ps,5,ZLVL);
 setPSLong(ps,6,ZUID);
 setPSString(ps,7,ZSTA);
 setPSDateTime(ps,8,ZDATE);
 setPSDateTime(ps,9,ZDATO);
 setPSLong(ps,10,QRSQ);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = new ZFieldSet();
 fs.add(new ZField(F_ZOID,ZOID,FC_ZOID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_ZVER,ZVER,FC_ZVER,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_ZTYPE,ZTYPE,FC_ZTYPE,ZField.T_UNKNOWN,1));
 fs.add(new ZField(F_ZSID,ZSID,FC_ZSID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_ZLVL,ZLVL,FC_ZLVL,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_ZUID,ZUID,FC_ZUID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_ZSTA,ZSTA,FC_ZSTA,ZField.T_UNKNOWN,1));
 fs.add(new ZField(F_ZDATE,ZDATE,FC_ZDATE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_ZDATO,ZDATO,FC_ZDATO,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_QRSQ,QRSQ,FC_QRSQ,ZField.T_UNKNOWN,0));
 return(fs);
} //

// constructors
public ZObject() { }
public ZObject(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public ZObject(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 this.getFromRS(rs);
} // ZObject(java.sql.ResultSet rs)

} //ZObject
