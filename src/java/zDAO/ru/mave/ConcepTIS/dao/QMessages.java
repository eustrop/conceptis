// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao;

import java.util.*;

/** (replacment of ZMessages class)
* central storage of all textual messages used by this package.
* Any displayable text as well as complex SQL queries must be
* obtained over this class object only.
* It's useful for future localization, porting and spelling checks.
*/
public class QMessages extends ZMessages
{
// constructors
public QMessages(){ super(LANG_DEFAULT); }
public QMessages(String lang){ super(lang); }
} //QMessages
