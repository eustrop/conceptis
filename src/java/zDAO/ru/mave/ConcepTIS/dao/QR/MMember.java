// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2011
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
// WARNING! this code produced by automatic codegeneration tool
//          located at src/sql/PGSQL/codegen/tools/codegen.awk
//          In most cases you should'n change this file, but the above one.
//
// $Id$
//

package ru.mave.ConcepTIS.dao.QR;

import ru.mave.ConcepTIS.dao.*;
import java.math.BigDecimal;

/** QR.MMember table.
 *
 */
public class MMember extends DORecord
{
public String	code;
public Long	org_id;
public Long	scope_id;
public String	status;
public String	lei_type;
public String	lei;
public String	let;
public String	name;
public String	addr;
public String	site;
public String	phone;
public String	email;
public String	descr;
public String	owiki;

public String getCaption(){return(code);}

// Strings

public final static String TAB_CODE = "MM";
public final static String TAB_NAME = "MMember";
public final static String SUBSYS_CODE = "QR";

public final static String F_CODE = "code";
public final static String F_ORG_ID = "org_id";
public final static String F_SCOPE_ID = "scope_id";
public final static String F_STATUS = "status";
public final static String F_LEI_TYPE = "lei_type";
public final static String F_LEI = "lei";
public final static String F_LET = "let";
public final static String F_NAME = "name";
public final static String F_ADDR = "addr";
public final static String F_SITE = "site";
public final static String F_PHONE = "phone";
public final static String F_EMAIL = "email";
public final static String F_DESCR = "descr";
public final static String F_OWIKI = "owiki";

public final static String FC_CODE = "QR.MM01";
public final static String FCS_CODE = "01";
public final static String FC_ORG_ID = "QR.MM02";
public final static String FCS_ORG_ID = "02";
public final static String FC_SCOPE_ID = "QR.MM03";
public final static String FCS_SCOPE_ID = "03";
public final static String FC_STATUS = "QR.MM04";
public final static String FCS_STATUS = "04";
public final static String FC_LEI_TYPE = "QR.MM05";
public final static String FCS_LEI_TYPE = "05";
public final static String FC_LEI = "QR.MM06";
public final static String FCS_LEI = "06";
public final static String FC_LET = "QR.MM07";
public final static String FCS_LET = "07";
public final static String FC_NAME = "QR.MM08";
public final static String FCS_NAME = "08";
public final static String FC_ADDR = "QR.MM09";
public final static String FCS_ADDR = "09";
public final static String FC_SITE = "QR.MM10";
public final static String FCS_SITE = "10";
public final static String FC_PHONE = "QR.MM11";
public final static String FCS_PHONE = "11";
public final static String FC_EMAIL = "QR.MM12";
public final static String FCS_EMAIL = "12";
public final static String FC_DESCR = "QR.MM13";
public final static String FCS_DESCR = "13";
public final static String FC_OWIKI = "QR.MM14";
public final static String FCS_OWIKI = "14";

// children management

public DORecord[] createChildrenFactory()
{
 DORecord dor[]=new DORecord[1];
 dor[0]=new MRange();
 return(dor);
}

public String[] getChildTabCodes()
{
 String c[]=new String[1];
 c[0]=MRange.TAB_CODE;
 return(c);
}

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 super.getFromRS(rs);
 code = getRSString(rs,8);
 org_id = getRSLong(rs,9);
 scope_id = getRSLong(rs,10);
 status = rtrimString(getRSString(rs,11));
 lei_type = rtrimString(getRSString(rs,12));
 lei = rtrimString(getRSString(rs,13));
 let = rtrimString(getRSString(rs,14));
 name = rtrimString(getRSString(rs,15));
 addr = rtrimString(getRSString(rs,16));
 site = getRSString(rs,17);
 phone = getRSString(rs,18);
 email = rtrimString(getRSString(rs,19));
 descr = getRSString(rs,20);
 owiki = getRSString(rs,21);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 super.getFromFS(fs);
 code = getFSString(fs,F_CODE);
 org_id = getFSLong(fs,F_ORG_ID);
 scope_id = getFSLong(fs,F_SCOPE_ID);
 status = getFSString(fs,F_STATUS);
 lei_type = getFSString(fs,F_LEI_TYPE);
 lei = getFSString(fs,F_LEI);
 let = getFSString(fs,F_LET);
 name = getFSString(fs,F_NAME);
 addr = getFSString(fs,F_ADDR);
 site = getFSString(fs,F_SITE);
 phone = getFSString(fs,F_PHONE);
 email = getFSString(fs,F_EMAIL);
 descr = getFSString(fs,F_DESCR);
 owiki = getFSString(fs,F_OWIKI);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSString(ps,4,code);
 setPSLong(ps,5,org_id);
 setPSLong(ps,6,scope_id);
 setPSString(ps,7,status);
 setPSString(ps,8,lei_type);
 setPSString(ps,9,lei);
 setPSString(ps,10,let);
 setPSString(ps,11,name);
 setPSString(ps,12,addr);
 setPSString(ps,13,site);
 setPSString(ps,14,phone);
 setPSString(ps,15,email);
 setPSString(ps,16,descr);
 setPSString(ps,17,owiki);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = super.toFieldSet();
 fs.add(new ZField(F_CODE,code,FC_CODE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_ORG_ID,org_id,FC_ORG_ID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SCOPE_ID,scope_id,FC_SCOPE_ID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_STATUS,status,FC_STATUS,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_LEI_TYPE,lei_type,FC_LEI_TYPE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_LEI,lei,FC_LEI,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_LET,let,FC_LET,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_NAME,name,FC_NAME,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_ADDR,addr,FC_ADDR,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SITE,site,FC_SITE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_PHONE,phone,FC_PHONE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_EMAIL,email,FC_EMAIL,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_DESCR,descr,FC_DESCR,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_OWIKI,owiki,FC_OWIKI,ZField.T_UNKNOWN,0));
 return(fs);
} //

public String getSubsysCode(){return(SUBSYS_CODE);}
public String getTabCode(){return(TAB_CODE);}
public String getTabName(){return(TAB_NAME);}
public String getParentTabCode(){return(null);}
public int getNumOfFields() {return(14);}

public DORecord createFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
 {return(new MMember(rs));}
public DORecord createFromFS(ZFieldSet fs)
 throws ZException
 {return(new MMember(fs));};
public DORecord createInstance(){return(new MMember());}

// constructors
public MMember() { }
public MMember(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public MMember(java.sql.ResultSet rs)
 throws java.sql.SQLException { this.getFromRS(rs); }

} //MMember
