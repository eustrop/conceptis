// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2011
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
// WARNING! this code produced by automatic codegeneration tool
//          located at src/sql/PGSQL/codegen/tools/codegen.awk
//          In most cases you should'n change this file, but the above one.
//
// $Id$
//

package ru.mave.ConcepTIS.dao.QR;

import ru.mave.ConcepTIS.dao.*;
import java.math.BigDecimal;

/** QR.CCard table.
 *
 */
public class CCard extends DORecord
{
public Long	QR;
public YesNo	autoexport;
public Long	pubqr_id;
public DateOnly	cardate;
public Long	cnum_id;
public String	cnum;
public DateOnly	cdate;
public BigDecimal	cmoney;
public String	cmoney_cur;
public String	cmoney_vat;
public String	cmoney_desc;
public Long	supplier_id;
public String	supplier;
public Long	client_id;
public String	client;
public String	claddr;
public String	prodtype;
public String	prodmodel;
public String	pmrevision;
public String	sn;
public DateOnly	prodate;
public String	GTD;
public DateOnly	saledate;
public DateOnly	sendate;
public DateOnly	wstart;
public DateOnly	wend;
public Double	gis_long;
public Double	gis_lat;
public Double	gis_alt;
public String	comment;
public String	csvcard;

public String getCaption(){return(null);}

// Strings

public final static String TAB_CODE = "CC";
public final static String TAB_NAME = "CCard";
public final static String SUBSYS_CODE = "QR";

public final static String F_QR = "QR";
public final static String F_AUTOEXPORT = "autoexport";
public final static String F_PUBQR_ID = "pubqr_id";
public final static String F_CARDATE = "cardate";
public final static String F_CNUM_ID = "cnum_id";
public final static String F_CNUM = "cnum";
public final static String F_CDATE = "cdate";
public final static String F_CMONEY = "cmoney";
public final static String F_CMONEY_CUR = "cmoney_cur";
public final static String F_CMONEY_VAT = "cmoney_vat";
public final static String F_CMONEY_DESC = "cmoney_desc";
public final static String F_SUPPLIER_ID = "supplier_id";
public final static String F_SUPPLIER = "supplier";
public final static String F_CLIENT_ID = "client_id";
public final static String F_CLIENT = "client";
public final static String F_CLADDR = "claddr";
public final static String F_PRODTYPE = "prodtype";
public final static String F_PRODMODEL = "prodmodel";
public final static String F_PMREVISION = "pmrevision";
public final static String F_SN = "sn";
public final static String F_PRODATE = "prodate";
public final static String F_GTD = "GTD";
public final static String F_SALEDATE = "saledate";
public final static String F_SENDATE = "sendate";
public final static String F_WSTART = "wstart";
public final static String F_WEND = "wend";
public final static String F_GIS_LONG = "gis_long";
public final static String F_GIS_LAT = "gis_lat";
public final static String F_GIS_ALT = "gis_alt";
public final static String F_COMMENT = "comment";
public final static String F_CSVCARD = "csvcard";

public final static String FC_QR = "QR.CC01";
public final static String FCS_QR = "01";
public final static String FC_AUTOEXPORT = "QR.CC02";
public final static String FCS_AUTOEXPORT = "02";
public final static String FC_PUBQR_ID = "QR.CC03";
public final static String FCS_PUBQR_ID = "03";
public final static String FC_CARDATE = "QR.CC04";
public final static String FCS_CARDATE = "04";
public final static String FC_CNUM_ID = "QR.CC05";
public final static String FCS_CNUM_ID = "05";
public final static String FC_CNUM = "QR.CC06";
public final static String FCS_CNUM = "06";
public final static String FC_CDATE = "QR.CC07";
public final static String FCS_CDATE = "07";
public final static String FC_CMONEY = "QR.CC08";
public final static String FCS_CMONEY = "08";
public final static String FC_CMONEY_CUR = "QR.CC09";
public final static String FCS_CMONEY_CUR = "09";
public final static String FC_CMONEY_VAT = "QR.CC10";
public final static String FCS_CMONEY_VAT = "10";
public final static String FC_CMONEY_DESC = "QR.CC11";
public final static String FCS_CMONEY_DESC = "11";
public final static String FC_SUPPLIER_ID = "QR.CC12";
public final static String FCS_SUPPLIER_ID = "12";
public final static String FC_SUPPLIER = "QR.CC13";
public final static String FCS_SUPPLIER = "13";
public final static String FC_CLIENT_ID = "QR.CC14";
public final static String FCS_CLIENT_ID = "14";
public final static String FC_CLIENT = "QR.CC15";
public final static String FCS_CLIENT = "15";
public final static String FC_CLADDR = "QR.CC16";
public final static String FCS_CLADDR = "16";
public final static String FC_PRODTYPE = "QR.CC17";
public final static String FCS_PRODTYPE = "17";
public final static String FC_PRODMODEL = "QR.CC18";
public final static String FCS_PRODMODEL = "18";
public final static String FC_PMREVISION = "QR.CC19";
public final static String FCS_PMREVISION = "19";
public final static String FC_SN = "QR.CC20";
public final static String FCS_SN = "20";
public final static String FC_PRODATE = "QR.CC21";
public final static String FCS_PRODATE = "21";
public final static String FC_GTD = "QR.CC22";
public final static String FCS_GTD = "22";
public final static String FC_SALEDATE = "QR.CC23";
public final static String FCS_SALEDATE = "23";
public final static String FC_SENDATE = "QR.CC24";
public final static String FCS_SENDATE = "24";
public final static String FC_WSTART = "QR.CC25";
public final static String FCS_WSTART = "25";
public final static String FC_WEND = "QR.CC26";
public final static String FCS_WEND = "26";
public final static String FC_GIS_LONG = "QR.CC27";
public final static String FCS_GIS_LONG = "27";
public final static String FC_GIS_LAT = "QR.CC28";
public final static String FCS_GIS_LAT = "28";
public final static String FC_GIS_ALT = "QR.CC29";
public final static String FCS_GIS_ALT = "29";
public final static String FC_COMMENT = "QR.CC30";
public final static String FCS_COMMENT = "30";
public final static String FC_CSVCARD = "QR.CC31";
public final static String FCS_CSVCARD = "31";

// children management

public DORecord[] createChildrenFactory()
{
 return(new DORecord[0]);
}

public String[] getChildTabCodes()
{
 return(new String[0]);
}

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 super.getFromRS(rs);
 QR = getRSLong(rs,8);
 autoexport = getRSYesNo(rs,9);
 pubqr_id = getRSLong(rs,10);
 cardate = getRSDateOnly(rs,11);
 cnum_id = getRSLong(rs,12);
 cnum = getRSString(rs,13);
 cdate = getRSDateOnly(rs,14);
 cmoney = getRSBigDecimal(rs,15);
 cmoney_cur = getRSString(rs,16);
 cmoney_vat = getRSString(rs,17);
 cmoney_desc = getRSString(rs,18);
 supplier_id = getRSLong(rs,19);
 supplier = getRSString(rs,20);
 client_id = getRSLong(rs,21);
 client = getRSString(rs,22);
 claddr = getRSString(rs,23);
 prodtype = getRSString(rs,24);
 prodmodel = getRSString(rs,25);
 pmrevision = getRSString(rs,26);
 sn = getRSString(rs,27);
 prodate = getRSDateOnly(rs,28);
 GTD = getRSString(rs,29);
 saledate = getRSDateOnly(rs,30);
 sendate = getRSDateOnly(rs,31);
 wstart = getRSDateOnly(rs,32);
 wend = getRSDateOnly(rs,33);
 gis_long = getRSDouble(rs,34);
 gis_lat = getRSDouble(rs,35);
 gis_alt = getRSDouble(rs,36);
 comment = getRSString(rs,37);
 csvcard = getRSString(rs,38);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 super.getFromFS(fs);
 QR = getFSHEXLong(fs,F_QR);
 autoexport = getFSYesNo(fs,F_AUTOEXPORT);
 pubqr_id = getFSLong(fs,F_PUBQR_ID);
 cardate = getFSDateOnly(fs,F_CARDATE);
 cnum_id = getFSLong(fs,F_CNUM_ID);
 cnum = getFSString(fs,F_CNUM);
 cdate = getFSDateOnly(fs,F_CDATE);
 cmoney = getFSBigDecimal(fs,F_CMONEY);
 cmoney_cur = getFSString(fs,F_CMONEY_CUR);
 cmoney_vat = getFSString(fs,F_CMONEY_VAT);
 cmoney_desc = getFSString(fs,F_CMONEY_DESC);
 supplier_id = getFSLong(fs,F_SUPPLIER_ID);
 supplier = getFSString(fs,F_SUPPLIER);
 client_id = getFSLong(fs,F_CLIENT_ID);
 client = getFSString(fs,F_CLIENT);
 claddr = getFSString(fs,F_CLADDR);
 prodtype = getFSString(fs,F_PRODTYPE);
 prodmodel = getFSString(fs,F_PRODMODEL);
 pmrevision = getFSString(fs,F_PMREVISION);
 sn = getFSString(fs,F_SN);
 prodate = getFSDateOnly(fs,F_PRODATE);
 GTD = getFSString(fs,F_GTD);
 saledate = getFSDateOnly(fs,F_SALEDATE);
 sendate = getFSDateOnly(fs,F_SENDATE);
 wstart = getFSDateOnly(fs,F_WSTART);
 wend = getFSDateOnly(fs,F_WEND);
 gis_long = getFSDouble(fs,F_GIS_LONG);
 gis_lat = getFSDouble(fs,F_GIS_LAT);
 gis_alt = getFSDouble(fs,F_GIS_ALT);
 comment = getFSString(fs,F_COMMENT);
 csvcard = getFSString(fs,F_CSVCARD);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSLong(ps,4,QR);
 setPSYesNo(ps,5,autoexport);
 setPSLong(ps,6,pubqr_id);
 setPSDateOnly(ps,7,cardate);
 setPSLong(ps,8,cnum_id);
 setPSString(ps,9,cnum);
 setPSDateOnly(ps,10,cdate);
 setPSBigDecimal(ps,11,cmoney);
 setPSString(ps,12,cmoney_cur);
 setPSString(ps,13,cmoney_vat);
 setPSString(ps,14,cmoney_desc);
 setPSLong(ps,15,supplier_id);
 setPSString(ps,16,supplier);
 setPSLong(ps,17,client_id);
 setPSString(ps,18,client);
 setPSString(ps,19,claddr);
 setPSString(ps,20,prodtype);
 setPSString(ps,21,prodmodel);
 setPSString(ps,22,pmrevision);
 setPSString(ps,23,sn);
 setPSDateOnly(ps,24,prodate);
 setPSString(ps,25,GTD);
 setPSDateOnly(ps,26,saledate);
 setPSDateOnly(ps,27,sendate);
 setPSDateOnly(ps,28,wstart);
 setPSDateOnly(ps,29,wend);
 setPSDouble(ps,30,gis_long);
 setPSDouble(ps,31,gis_lat);
 setPSDouble(ps,32,gis_alt);
 setPSString(ps,33,comment);
 setPSString(ps,34,csvcard);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = super.toFieldSet();
 fs.add(new ZField(F_QR,Long2HEX(QR),FC_QR,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_AUTOEXPORT,autoexport,FC_AUTOEXPORT,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_PUBQR_ID,pubqr_id,FC_PUBQR_ID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_CARDATE,cardate,FC_CARDATE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_CNUM_ID,cnum_id,FC_CNUM_ID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_CNUM,cnum,FC_CNUM,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_CDATE,cdate,FC_CDATE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_CMONEY,cmoney,FC_CMONEY,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_CMONEY_CUR,cmoney_cur,FC_CMONEY_CUR,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_CMONEY_VAT,cmoney_vat,FC_CMONEY_VAT,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_CMONEY_DESC,cmoney_desc,FC_CMONEY_DESC,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SUPPLIER_ID,supplier_id,FC_SUPPLIER_ID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SUPPLIER,supplier,FC_SUPPLIER,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_CLIENT_ID,client_id,FC_CLIENT_ID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_CLIENT,client,FC_CLIENT,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_CLADDR,claddr,FC_CLADDR,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_PRODTYPE,prodtype,FC_PRODTYPE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_PRODMODEL,prodmodel,FC_PRODMODEL,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_PMREVISION,pmrevision,FC_PMREVISION,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SN,sn,FC_SN,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_PRODATE,prodate,FC_PRODATE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_GTD,GTD,FC_GTD,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SALEDATE,saledate,FC_SALEDATE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SENDATE,sendate,FC_SENDATE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_WSTART,wstart,FC_WSTART,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_WEND,wend,FC_WEND,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_GIS_LONG,gis_long,FC_GIS_LONG,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_GIS_LAT,gis_lat,FC_GIS_LAT,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_GIS_ALT,gis_alt,FC_GIS_ALT,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_COMMENT,comment,FC_COMMENT,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_CSVCARD,csvcard,FC_CSVCARD,ZField.T_UNKNOWN,0));
 return(fs);
} //

public String getSubsysCode(){return(SUBSYS_CODE);}
public String getTabCode(){return(TAB_CODE);}
public String getTabName(){return(TAB_NAME);}
public String getParentTabCode(){return(null);}
public int getNumOfFields() {return(31);}

public DORecord createFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
 {return(new CCard(rs));}
public DORecord createFromFS(ZFieldSet fs)
 throws ZException
 {return(new CCard(fs));};
public DORecord createInstance(){return(new CCard());}

// constructors
public CCard() { }
public CCard(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public CCard(java.sql.ResultSet rs)
 throws java.sql.SQLException { this.getFromRS(rs); }

} //CCard
