// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2011
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
// WARNING! this code produced by automatic codegeneration tool
//          located at src/sql/PGSQL/codegen/tools/codegen_ot.awk
//          In most cases you should'n change this file, but the above one.
//
// $Id$
//

package ru.mave.ConcepTIS.dao.QR;

import ru.mave.ConcepTIS.dao.*;

/** QR.Member data object.
 *
 */
public class Member extends DOTProcessor
{

public final static String DOBJECT_CODE = "QR.M";
public final static String DOBJECT_CODE_SHORT = "M";
public final static String DOBJECT_NAME = "Member";
public final static String SUBSYS_CODE = "QR";

public String getDOTypeCodeShort(){return(DOBJECT_CODE_SHORT);}
public String getDOTypeCode(){return(DOBJECT_CODE);}
public String getDOTypeName(){return(DOBJECT_NAME);}
public String getDOTypeSubsys(){return(SUBSYS_CODE);}

private static DOTProcessor dotp;

/** get common instance of this class. */
public static synchronized DOTProcessor getDOTProcessor()
{
if(dotp == null) dotp = new Member();
return(dotp);
}

/** dobject with this DOTProcessor set. */
public static DObject newDObject()
{ return(new DObject(getDOTProcessor()) ); }

public DORecord[] createMembersFactory()
{
  DORecord[] dor = new DORecord[2];
  dor[0] = new MMember();
  dor[1] = new MRange();
  return(dor);
}

public String makeCaption(DObject o)
{ return(DObject.make_headrecord_caption(o,MMember.TAB_CODE));}

// constructors
private Member(){}
} //Member
