// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2011
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
// WARNING! this code produced by automatic codegeneration tool
//          located at src/sql/PGSQL/codegen/tools/codegen.awk
//          In most cases you should'n change this file, but the above one.
//
// $Id$
//

package ru.mave.ConcepTIS.dao.QR;

import ru.mave.ConcepTIS.dao.*;
import java.math.BigDecimal;

/** QR.MRange table.
 *
 */
public class MRange extends DORecord
{
public Long	rstart;
public Short	rbitl;
public String	rtype;
public String	status;
public String	action;
public String	redirect;
public DateTime	alloc;
public Long	member_id;
public Long	doc_id;
public String	descr;
public String	owiki;

public String getCaption(){return(null);}

// Strings

public final static String TAB_CODE = "MR";
public final static String TAB_NAME = "MRange";
public final static String SUBSYS_CODE = "QR";

public final static String F_RSTART = "rstart";
public final static String F_RBITL = "rbitl";
public final static String F_RTYPE = "rtype";
public final static String F_STATUS = "status";
public final static String F_ACTION = "action";
public final static String F_REDIRECT = "redirect";
public final static String F_ALLOC = "alloc";
public final static String F_MEMBER_ID = "member_id";
public final static String F_DOC_ID = "doc_id";
public final static String F_DESCR = "descr";
public final static String F_OWIKI = "owiki";

public final static String FC_RSTART = "QR.MR01";
public final static String FCS_RSTART = "01";
public final static String FC_RBITL = "QR.MR02";
public final static String FCS_RBITL = "02";
public final static String FC_RTYPE = "QR.MR03";
public final static String FCS_RTYPE = "03";
public final static String FC_STATUS = "QR.MR04";
public final static String FCS_STATUS = "04";
public final static String FC_ACTION = "QR.MR05";
public final static String FCS_ACTION = "05";
public final static String FC_REDIRECT = "QR.MR06";
public final static String FCS_REDIRECT = "06";
public final static String FC_ALLOC = "QR.MR07";
public final static String FCS_ALLOC = "07";
public final static String FC_MEMBER_ID = "QR.MR08";
public final static String FCS_MEMBER_ID = "08";
public final static String FC_DOC_ID = "QR.MR09";
public final static String FCS_DOC_ID = "09";
public final static String FC_DESCR = "QR.MR10";
public final static String FCS_DESCR = "10";
public final static String FC_OWIKI = "QR.MR11";
public final static String FCS_OWIKI = "11";

// children management

public DORecord[] createChildrenFactory()
{
 return(new DORecord[0]);
}

public String[] getChildTabCodes()
{
 return(new String[0]);
}

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 super.getFromRS(rs);
 rstart = getRSLong(rs,8);
 rbitl = getRSShort(rs,9);
 rtype = getRSString(rs,10);
 status = rtrimString(getRSString(rs,11));
 action = getRSString(rs,12);
 redirect = getRSString(rs,13);
 alloc = getRSDateTime(rs,14);
 member_id = getRSLong(rs,15);
 doc_id = getRSLong(rs,16);
 descr = getRSString(rs,17);
 owiki = getRSString(rs,18);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 super.getFromFS(fs);
 //rstart = getFSLong(fs,F_RSTART);
 rstart = getFSHEXLong(fs,F_RSTART);
 rbitl = getFSShort(fs,F_RBITL);
 rtype = getFSString(fs,F_RTYPE);
 status = getFSString(fs,F_STATUS);
 action = getFSString(fs,F_ACTION);
 redirect = getFSString(fs,F_REDIRECT);
 alloc = getFSDateTime(fs,F_ALLOC);
 member_id = getFSLong(fs,F_MEMBER_ID);
 doc_id = getFSLong(fs,F_DOC_ID);
 descr = getFSString(fs,F_DESCR);
 owiki = getFSString(fs,F_OWIKI);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSLong(ps,4,rstart);
 setPSShort(ps,5,rbitl);
 setPSString(ps,6,rtype);
 setPSString(ps,7,status);
 setPSString(ps,8,action);
 setPSString(ps,9,redirect);
 setPSDateTime(ps,10,alloc);
 setPSLong(ps,11,member_id);
 setPSLong(ps,12,doc_id);
 setPSString(ps,13,descr);
 setPSString(ps,14,owiki);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = super.toFieldSet();
 fs.add(new ZField(F_RSTART,Long2HEX(rstart),FC_RSTART,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_RBITL,rbitl,FC_RBITL,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_RTYPE,rtype,FC_RTYPE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_STATUS,status,FC_STATUS,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_ACTION,action,FC_ACTION,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_REDIRECT,redirect,FC_REDIRECT,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_ALLOC,alloc,FC_ALLOC,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_MEMBER_ID,member_id,FC_MEMBER_ID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_DOC_ID,doc_id,FC_DOC_ID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_DESCR,descr,FC_DESCR,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_OWIKI,owiki,FC_OWIKI,ZField.T_UNKNOWN,0));
 return(fs);
} //

public String getSubsysCode(){return(SUBSYS_CODE);}
public String getTabCode(){return(TAB_CODE);}
public String getTabName(){return(TAB_NAME);}
public String getParentTabCode(){return(MMember.TAB_CODE);}
public int getNumOfFields() {return(11);}

public DORecord createFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
 {return(new MRange(rs));}
public DORecord createFromFS(ZFieldSet fs)
 throws ZException
 {return(new MRange(fs));};
public DORecord createInstance(){return(new MRange());}

// constructors
public MRange() { }
public MRange(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public MRange(java.sql.ResultSet rs)
 throws java.sql.SQLException { this.getFromRS(rs); }

} //MRange
