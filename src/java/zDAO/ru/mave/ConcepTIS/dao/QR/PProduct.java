// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2011
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
// WARNING! this code produced by automatic codegeneration tool
//          located at src/sql/PGSQL/codegen/tools/codegen.awk
//          In most cases you should'n change this file, but the above one.
//
// $Id$
//

package ru.mave.ConcepTIS.dao.QR;

import ru.mave.ConcepTIS.dao.*;
import java.math.BigDecimal;

/** QR.PProduct table.
 *
 */
public class PProduct extends DORecord
{
public String	prodtype;
public String	prodmodel;
public String	pmrevision;
public String	prodpart;
public Long	dir_id;
public String	lang;
public String	title;
public String	owiki;

public String getCaption(){return(null);}

// Strings

public final static String TAB_CODE = "PP";
public final static String TAB_NAME = "PProduct";
public final static String SUBSYS_CODE = "QR";

public final static String F_PRODTYPE = "prodtype";
public final static String F_PRODMODEL = "prodmodel";
public final static String F_PMREVISION = "pmrevision";
public final static String F_PRODPART = "prodpart";
public final static String F_DIR_ID = "dir_id";
public final static String F_LANG = "lang";
public final static String F_TITLE = "title";
public final static String F_OWIKI = "owiki";

public final static String FC_PRODTYPE = "QR.PP01";
public final static String FCS_PRODTYPE = "01";
public final static String FC_PRODMODEL = "QR.PP02";
public final static String FCS_PRODMODEL = "02";
public final static String FC_PMREVISION = "QR.PP03";
public final static String FCS_PMREVISION = "03";
public final static String FC_PRODPART = "QR.PP04";
public final static String FCS_PRODPART = "04";
public final static String FC_DIR_ID = "QR.PP05";
public final static String FCS_DIR_ID = "05";
public final static String FC_LANG = "QR.PP06";
public final static String FCS_LANG = "06";
public final static String FC_TITLE = "QR.PP07";
public final static String FCS_TITLE = "07";
public final static String FC_OWIKI = "QR.PP08";
public final static String FCS_OWIKI = "08";

// children management

public DORecord[] createChildrenFactory()
{
 return(new DORecord[0]);
}

public String[] getChildTabCodes()
{
 return(new String[0]);
}

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 super.getFromRS(rs);
 prodtype = getRSString(rs,8);
 prodmodel = getRSString(rs,9);
 pmrevision = getRSString(rs,10);
 prodpart = getRSString(rs,11);
 dir_id = getRSLong(rs,12);
 lang = getRSString(rs,13);
 title = getRSString(rs,14);
 owiki = getRSString(rs,15);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 super.getFromFS(fs);
 prodtype = getFSString(fs,F_PRODTYPE);
 prodmodel = getFSString(fs,F_PRODMODEL);
 pmrevision = getFSString(fs,F_PMREVISION);
 prodpart = getFSString(fs,F_PRODPART);
 dir_id = getFSLong(fs,F_DIR_ID);
 lang = getFSString(fs,F_LANG);
 title = getFSString(fs,F_TITLE);
 owiki = getFSString(fs,F_OWIKI);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSString(ps,4,prodtype);
 setPSString(ps,5,prodmodel);
 setPSString(ps,6,pmrevision);
 setPSString(ps,7,prodpart);
 setPSLong(ps,8,dir_id);
 setPSString(ps,9,lang);
 setPSString(ps,10,title);
 setPSString(ps,11,owiki);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = super.toFieldSet();
 fs.add(new ZField(F_PRODTYPE,prodtype,FC_PRODTYPE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_PRODMODEL,prodmodel,FC_PRODMODEL,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_PMREVISION,pmrevision,FC_PMREVISION,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_PRODPART,prodpart,FC_PRODPART,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_DIR_ID,dir_id,FC_DIR_ID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_LANG,lang,FC_LANG,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_TITLE,title,FC_TITLE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_OWIKI,owiki,FC_OWIKI,ZField.T_UNKNOWN,0));
 return(fs);
} //

public String getSubsysCode(){return(SUBSYS_CODE);}
public String getTabCode(){return(TAB_CODE);}
public String getTabName(){return(TAB_NAME);}
public String getParentTabCode(){return(null);}
public int getNumOfFields() {return(8);}

public DORecord createFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
 {return(new PProduct(rs));}
public DORecord createFromFS(ZFieldSet fs)
 throws ZException
 {return(new PProduct(fs));};
public DORecord createInstance(){return(new PProduct());}

// constructors
public PProduct() { }
public PProduct(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public PProduct(java.sql.ResultSet rs)
 throws java.sql.SQLException { this.getFromRS(rs); }

} //PProduct
