// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2011
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
// WARNING! this code produced by automatic codegeneration tool
//          located at src/sql/PGSQL/codegen/tools/codegen.awk
//          In most cases you should'n change this file, but the above one.
//
// $Id$
//

package ru.mave.ConcepTIS.dao.QR;

import ru.mave.ConcepTIS.dao.*;
import java.math.BigDecimal;

/** QR.QRecord table.
 *
 */
public class QRecord extends DORecord
{
public Long	QR;
public String	action;
public String	redirect;
public Long	obj_id;
public String	cnum;
public DateOnly	cdate;
public BigDecimal	price_gpl;
public String	price_vat;
public Long	support_id;
public String	prodtype;
public String	prodmodel;
public String	pmrevision;
public String	sn;
public DateOnly	prodate;
public String	GTD;
public DateOnly	saledate;
public DateOnly	sendate;
public DateOnly	wstart;
public DateOnly	wend;
public Double	gis_long;
public Double	gis_lat;
public Double	gis_alt;
public String	comment;
public String	csvcard;
public String	owiki;

public String getCaption(){return(null);}

// Strings

public final static String TAB_CODE = "QR";
public final static String TAB_NAME = "QRecord";
public final static String SUBSYS_CODE = "QR";

public final static String F_QR = "QR";
public final static String F_ACTION = "action";
public final static String F_REDIRECT = "redirect";
public final static String F_OBJ_ID = "obj_id";
public final static String F_CNUM = "cnum";
public final static String F_CDATE = "cdate";
public final static String F_PRICE_GPL = "price_gpl";
public final static String F_PRICE_VAT = "price_vat";
public final static String F_SUPPORT_ID = "support_id";
public final static String F_PRODTYPE = "prodtype";
public final static String F_PRODMODEL = "prodmodel";
public final static String F_PMREVISION = "pmrevision";
public final static String F_SN = "sn";
public final static String F_PRODATE = "prodate";
public final static String F_GTD = "GTD";
public final static String F_SALEDATE = "saledate";
public final static String F_SENDATE = "sendate";
public final static String F_WSTART = "wstart";
public final static String F_WEND = "wend";
public final static String F_GIS_LONG = "gis_long";
public final static String F_GIS_LAT = "gis_lat";
public final static String F_GIS_ALT = "gis_alt";
public final static String F_COMMENT = "comment";
public final static String F_CSVCARD = "csvcard";
public final static String F_OWIKI = "owiki";

public final static String FC_QR = "QR.QR01";
public final static String FCS_QR = "01";
public final static String FC_ACTION = "QR.QR02";
public final static String FCS_ACTION = "02";
public final static String FC_REDIRECT = "QR.QR03";
public final static String FCS_REDIRECT = "03";
public final static String FC_OBJ_ID = "QR.QR04";
public final static String FCS_OBJ_ID = "04";
public final static String FC_CNUM = "QR.QR06";
public final static String FCS_CNUM = "06";
public final static String FC_CDATE = "QR.QR07";
public final static String FCS_CDATE = "07";
public final static String FC_PRICE_GPL = "QR.QR08";
public final static String FCS_PRICE_GPL = "08";
public final static String FC_PRICE_VAT = "QR.QR09";
public final static String FCS_PRICE_VAT = "09";
public final static String FC_SUPPORT_ID = "QR.QR10";
public final static String FCS_SUPPORT_ID = "10";
public final static String FC_PRODTYPE = "QR.QR17";
public final static String FCS_PRODTYPE = "17";
public final static String FC_PRODMODEL = "QR.QR18";
public final static String FCS_PRODMODEL = "18";
public final static String FC_PMREVISION = "QR.QR19";
public final static String FCS_PMREVISION = "19";
public final static String FC_SN = "QR.QR20";
public final static String FCS_SN = "20";
public final static String FC_PRODATE = "QR.QR21";
public final static String FCS_PRODATE = "21";
public final static String FC_GTD = "QR.QR22";
public final static String FCS_GTD = "22";
public final static String FC_SALEDATE = "QR.QR23";
public final static String FCS_SALEDATE = "23";
public final static String FC_SENDATE = "QR.QR24";
public final static String FCS_SENDATE = "24";
public final static String FC_WSTART = "QR.QR25";
public final static String FCS_WSTART = "25";
public final static String FC_WEND = "QR.QR26";
public final static String FCS_WEND = "26";
public final static String FC_GIS_LONG = "QR.QR27";
public final static String FCS_GIS_LONG = "27";
public final static String FC_GIS_LAT = "QR.QR28";
public final static String FCS_GIS_LAT = "28";
public final static String FC_GIS_ALT = "QR.QR29";
public final static String FCS_GIS_ALT = "29";
public final static String FC_COMMENT = "QR.QR30";
public final static String FCS_COMMENT = "30";
public final static String FC_CSVCARD = "QR.QR31";
public final static String FCS_CSVCARD = "31";
public final static String FC_OWIKI = "QR.QR32";
public final static String FCS_OWIKI = "32";

// children management

public DORecord[] createChildrenFactory()
{
 return(new DORecord[0]);
}

public String[] getChildTabCodes()
{
 return(new String[0]);
}

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 super.getFromRS(rs);
 QR = getRSLong(rs,8);
 action = getRSString(rs,9);
 redirect = getRSString(rs,10);
 obj_id = getRSLong(rs,11);
 cnum = getRSString(rs,12);
 cdate = getRSDateOnly(rs,13);
 price_gpl = getRSBigDecimal(rs,14);
 price_vat = getRSString(rs,15);
 support_id = getRSLong(rs,16);
 prodtype = getRSString(rs,17);
 prodmodel = getRSString(rs,18);
 pmrevision = getRSString(rs,19);
 sn = getRSString(rs,20);
 prodate = getRSDateOnly(rs,21);
 GTD = getRSString(rs,22);
 saledate = getRSDateOnly(rs,23);
 sendate = getRSDateOnly(rs,24);
 wstart = getRSDateOnly(rs,25);
 wend = getRSDateOnly(rs,26);
 gis_long = getRSDouble(rs,27);
 gis_lat = getRSDouble(rs,28);
 gis_alt = getRSDouble(rs,29);
 comment = getRSString(rs,30);
 csvcard = getRSString(rs,31);
 owiki = getRSString(rs,32);
} //

public void getFromFS(ZFieldSet fs)
 throws ZException
{
 super.getFromFS(fs);
 QR = getFSHEXLong(fs,F_QR);
 action = getFSString(fs,F_ACTION);
 redirect = getFSString(fs,F_REDIRECT);
 obj_id = getFSLong(fs,F_OBJ_ID);
 cnum = getFSString(fs,F_CNUM);
 cdate = getFSDateOnly(fs,F_CDATE);
 price_gpl = getFSBigDecimal(fs,F_PRICE_GPL);
 price_vat = getFSString(fs,F_PRICE_VAT);
 support_id = getFSLong(fs,F_SUPPORT_ID);
 prodtype = getFSString(fs,F_PRODTYPE);
 prodmodel = getFSString(fs,F_PRODMODEL);
 pmrevision = getFSString(fs,F_PMREVISION);
 sn = getFSString(fs,F_SN);
 prodate = getFSDateOnly(fs,F_PRODATE);
 GTD = getFSString(fs,F_GTD);
 saledate = getFSDateOnly(fs,F_SALEDATE);
 sendate = getFSDateOnly(fs,F_SENDATE);
 wstart = getFSDateOnly(fs,F_WSTART);
 wend = getFSDateOnly(fs,F_WEND);
 gis_long = getFSDouble(fs,F_GIS_LONG);
 gis_lat = getFSDouble(fs,F_GIS_LAT);
 gis_alt = getFSDouble(fs,F_GIS_ALT);
 comment = getFSString(fs,F_COMMENT);
 csvcard = getFSString(fs,F_CSVCARD);
 owiki = getFSString(fs,F_OWIKI);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 setPSLong(ps,4,QR);
 setPSString(ps,5,action);
 setPSString(ps,6,redirect);
 setPSLong(ps,7,obj_id);
 setPSString(ps,8,cnum);
 setPSDateOnly(ps,9,cdate);
 setPSBigDecimal(ps,10,price_gpl);
 setPSString(ps,11,price_vat);
 setPSLong(ps,12,support_id);
 setPSString(ps,13,prodtype);
 setPSString(ps,14,prodmodel);
 setPSString(ps,15,pmrevision);
 setPSString(ps,16,sn);
 setPSDateOnly(ps,17,prodate);
 setPSString(ps,18,GTD);
 setPSDateOnly(ps,19,saledate);
 setPSDateOnly(ps,20,sendate);
 setPSDateOnly(ps,21,wstart);
 setPSDateOnly(ps,22,wend);
 setPSDouble(ps,23,gis_long);
 setPSDouble(ps,24,gis_lat);
 setPSDouble(ps,25,gis_alt);
 setPSString(ps,26,comment);
 setPSString(ps,27,csvcard);
 setPSString(ps,28,owiki);
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = super.toFieldSet();
 fs.add(new ZField(F_QR,Long2HEX(QR),FC_QR,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_ACTION,action,FC_ACTION,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_REDIRECT,redirect,FC_REDIRECT,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_OBJ_ID,obj_id,FC_OBJ_ID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_CNUM,cnum,FC_CNUM,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_CDATE,cdate,FC_CDATE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_PRICE_GPL,price_gpl,FC_PRICE_GPL,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_PRICE_VAT,price_vat,FC_PRICE_VAT,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SUPPORT_ID,support_id,FC_SUPPORT_ID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_PRODTYPE,prodtype,FC_PRODTYPE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_PRODMODEL,prodmodel,FC_PRODMODEL,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_PMREVISION,pmrevision,FC_PMREVISION,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SN,sn,FC_SN,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_PRODATE,prodate,FC_PRODATE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_GTD,GTD,FC_GTD,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SALEDATE,saledate,FC_SALEDATE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_SENDATE,sendate,FC_SENDATE,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_WSTART,wstart,FC_WSTART,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_WEND,wend,FC_WEND,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_GIS_LONG,gis_long,FC_GIS_LONG,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_GIS_LAT,gis_lat,FC_GIS_LAT,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_GIS_ALT,gis_alt,FC_GIS_ALT,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_COMMENT,comment,FC_COMMENT,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_CSVCARD,csvcard,FC_CSVCARD,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_OWIKI,owiki,FC_OWIKI,ZField.T_UNKNOWN,0));
 return(fs);
} //

public String getSubsysCode(){return(SUBSYS_CODE);}
public String getTabCode(){return(TAB_CODE);}
public String getTabName(){return(TAB_NAME);}
public String getParentTabCode(){return(null);}
public int getNumOfFields() {return(25);}

public DORecord createFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
 {return(new QRecord(rs));}
public DORecord createFromFS(ZFieldSet fs)
 throws ZException
 {return(new QRecord(fs));};
public DORecord createInstance(){return(new QRecord());}

// constructors
public QRecord() { }
public QRecord(ZFieldSet fs) throws ZException { this.getFromFS(fs); }
public QRecord(java.sql.ResultSet rs)
 throws java.sql.SQLException { this.getFromRS(rs); }

} //QRecord
