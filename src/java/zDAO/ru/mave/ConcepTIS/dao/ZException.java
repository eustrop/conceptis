// ConcepTIS project
// (c) Alex V Eustrop 2009-2010
// see LICENSE at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao;

/** exception which this package able to throw.
 */
public class ZException extends Exception
{

private ZExecStatus es;

/** @see ZExecStatus */
public Long getZID()
 {if(es==null)return(null);return(es.ZID);}
/** @see ZExecStatus */
public Long getZVER()
 {if(es==null)return(null);return(es.ZVER);}

/** @see ZExecStatus */
public Number getErrnum()
 {if(es==null)return(null);return(es.errnum);}
/** @see ZExecStatus */
public String getErrcode()
 {if(es==null)return(null);return(es.errcode);}
/** @see ZExecStatus */
public String getErrdesc()
 {if(es==null)return(null);return(es.errdesc);}

// constructors
ZException(){}
ZException(ZExecStatus es){super(es.errdesc);this.es=es;}
public ZException(String s){super(s);}
ZException(ZMessages zm, int msg_id)
 {this(zm.getMsg(msg_id));}	
ZException(Exception e){super(e.getMessage());}

} //ZException
