// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao;

import java.util.*;

/** central storage of all textual messages used by this package.
* Any displayable text as well as complex SQL queries must be
* obtained over this class object only.
* It's useful for future localization, porting and spelling checks.
*/
public class ZMessages
{
private static Hashtable htLangSet = new Hashtable();
private static boolean is_initialized = false;
private String user_lang = null;

public final static int MSG_ARRAY_STARTED = 0;
public final static int SUCCESS = 0;
public final static int WARNING = 1;
public final static int YES = 2;
public final static int NO = 3;
public final static int ERROR = 4;
// errors
public final static int ERR_NOT_IMPLEMENTED = ERROR + 1;
public final static int ERR_CALL_NOT_IMPLEMENTED = ERROR + 2;
public final static int ERR_INVALID_USERNAME = ERROR + 3 ;
public final static int ERR_WHILE_JDBC_CONNECTION = ERROR + 4;
public final static int ERR_WHILE_JDBC_SQL = ERROR + 5;
public final static int ERR_INVALID_VERSION = ERROR + 6;
public final static int ERR_INVALID_CODE_X_REQUIRED_Y = ERROR + 7;
//
public final static int MSG_ARRAY_SIZE = 10;

public final static String LANG_EN = "EN";
public final static String LANG_RU = "RU";
public final static String LANG_DEFAULT = LANG_EN;

// TISC subsystem (SIC!)
// use ru.mave.ConcepTIS.dao.SUBSYS.ObjectClass.DOBJECT_CODE
// instead of adding all codes here (note since 2023-04-51 Eustrop)
// use ru.mave.ConcepTIS.dao.TISC.Area.DOBJECT_CODE
// instead of DOBJ_TISC_AREA  for instance
public final static String DOBJ_TISC_AREA = "TISC.A";
public final static String DOBJ_TISC_DOCUMENT = "TISC.D";
public final static String DOBJ_TISC_CONTAINER = "TISC.C";
// FS subsystem
// use ru.mave.ConcepTIS.dao.FS.File.DOBJECT_CODE
//public final static String DOBJ_FS_FILE = "FS.F";

private final static String SZ_EMPTY = "";

public final static String[] msgEN = {
 // general words
 "Success", "Warning", "Yes", "No", "Error",
 // Errors
 "Not implemented",
 "Call not implemented",
 "Invalid username (%1)",
 "Error while JDBC connection(login=\"%1\")",
 "Error while JDBC processing SQL query: \"%1\"",
 "Invalid version",
 "Invalid code %1 (required %2)",
 null // end of messages array
	};
public final static String[] msgDefault = msgEN;
public static String msgRU[] = new String[MSG_ARRAY_SIZE];

//
// SQL queries section (library of queries for future porting)
//

// SQL_
public final static int SQL_PING = 0;
public final static int SQL_PING_TIS = 1;
public final static int SQL_LIST_DICTIONARIES = 2;
public final static int SQL_LOAD_DICTIONARY = 3;
public final static int SQL_LOAD_DICTIONARY_MSG4LANG = 4; // only existing loclzd msg
public final static int SQL_LOAD_DICTIONARY4LANG = 5; // all codes with loclzd or deflt msgs
// TISQL_
public final static int TISQL_ = 5;
public final static int TISQL_WHERE = TISQL_ + 1;
public final static int TISQL_LIKE = TISQL_ + 2;
public final static int TISQL_AND = TISQL_ + 3;
public final static int TISQL_OR = TISQL_ + 4;
public final static int TISQL_NOT = TISQL_ + 5;
public final static int TISQL_IN = TISQL_ + 6;
public final static int TISQL_EXISTS = TISQL_ + 7;
// TISQLCOND_
public final static int TISQLCOND_ = TISQL_ + 7;
public final static int TISQL_LIMIT = TISQLCOND_ + 1;
public final static int TISQL_OFFSET = TISQLCOND_ + 2;
public final static int TISQLCOND_AT_ZSID = TISQLCOND_ + 3;
public final static int TISQLCOND_OF_ZTYPE = TISQLCOND_ + 4;
public final static int TISQLCOND_AT_ZO_ZSID = TISQLCOND_ + 5;
public final static int TISQLCOND_OF_ZO_ZTYPE = TISQLCOND_ + 6;
// TISQL_ORDER
public final static int TISQLORDER_ = TISQLCOND_ + 6;
public final static int TISQLORDER_ZOID = TISQLORDER_ + 1;
public final static int TISQLORDER_ZOID_ZVER = TISQLORDER_ + 2;
// TISQL_SAM_
public final static int TISQL_SAM_ = TISQLORDER_ + 2;
public final static int TISQL_LISTSAMGROUPS4USER = TISQL_SAM_ + 1;
public final static int TISQL_LISTSAMUSERCAPABILITIES4USER = TISQL_SAM_ + 2;
public final static int TISQL_LISTSAMUSER4GROUP = TISQL_SAM_ + 3;
public final static int TISQL_LISTSAMACLSCOPE4GROUP = TISQL_SAM_ + 4;
public final static int TISQL_LISTSAMSCOPEPOLICY = TISQL_SAM_ + 5;
public final static int TISQL_GRANT_SAMUSERGROUP = TISQL_SAM_ + 6;
public final static int TISQL_REVOKE_SAMUSERGROUP = TISQL_SAM_ + 7;
public final static int TISQL_GRANT_SAMUSERCAPABILITY = TISQL_SAM_ + 8;
public final static int TISQL_REVOKE_SAMUSERCAPABILITY = TISQL_SAM_ + 9;
public final static int TISQL_SET_SAMSCOPE_CHPTS = TISQL_SAM_ + 10;
public final static int TISQL_SET_SAMSCOPEPOLICY = TISQL_SAM_ + 11;
public final static int TISQL_DELETE_SAMSCOPEPOLICY = TISQL_SAM_ + 12;
public final static int TISQL_SET_SAMACLSCOPE = TISQL_SAM_ + 13;
public final static int TISQL_DELETE_SAMACLSCOPE = TISQL_SAM_ + 14;
public final static int TISQL_SET_SAMUSERPASSWORD = TISQL_SAM_ + 15;
public final static int TISQL_LIST_SAMUSERPASSWORD = TISQL_SAM_ + 16;
// TISQL_TIS_
public final static int TISQL_TIS_ = TISQL_SAM_ + 16;
public final static int TISQL_COMMIT_OBJECT = TISQL_TIS_ + 1;
public final static int TISQL_CREATE_OBJECT = TISQL_TIS_ + 2;
public final static int TISQL_DELETE_OBJECT = TISQL_TIS_ + 3;
public final static int TISQL_LOCK_OBJECT = TISQL_TIS_ + 4;
public final static int TISQL_MOVE_OBJECT = TISQL_TIS_ + 5;
public final static int TISQL_OPEN_OBJECT = TISQL_TIS_ + 6;
public final static int TISQL_ROLLBACK_OBJECT = TISQL_TIS_ + 7;
public final static int TISQL_SET_ZNAME = TISQL_TIS_ + 8;
public final static int TISQL_SET_SLEVEL = TISQL_TIS_ + 9;
public final static int TISQL_SET_VDATE = TISQL_TIS_ + 10;
public final static int TISQL_GET_VDATE = TISQL_TIS_ + 11;
public final static int TISQL_UNLOCK_OBJECT = TISQL_TIS_ + 12;
// TISQL_LIST
public final static int TISQL_LIST_ = TISQL_TIS_ + 12;
public final static int TISQL_LIST_ACTIVE_OBJECTS = TISQL_LIST_ + 1;
public final static int TISQL_LIST_LOCKED_OBJECTS = TISQL_LIST_ + 2;
public final static int TISQL_LIST_MY_INTERMEDIATE_OBJECTS = TISQL_LIST_ + 3;
public final static int TISQL_LIST_ACTIVE_OBJECTS_WITH_ZNAME = TISQL_LIST_ + 4;
// TISQL_LOAD
public final static int TISQL_LOAD_ = TISQL_LIST_ + 4;
public final static int TISQL_LOAD_ZO_HVERSIONS = TISQL_LOAD_ + 1;
public final static int TISQL_LOAD_ZO_NVERSION = TISQL_LOAD_ + 2;
public final static int TISQL_LOAD_ZO_LVERSION = TISQL_LOAD_ + 3;
public final static int TISQL_LOAD_ZO_IVERSION = TISQL_LOAD_ + 4;
public final static int TISQL_LOAD_ZNAME = TISQL_LOAD_ + 5;
/** all ZNAMEs for set of ZOIDs taken from some idfield of
 *  specified dobject.
 *  tag-based params (%1,%2,%3):SUBSYSTEM,TABLE,IDFIELD;
 *  SQL params (1):ZOID */
public final static int TISQL_LOAD_ZNAMES4IDFIELD = TISQL_LOAD_ + 6;
/** all rows from some table for set of ZOIDs taken from some idfield of
 *  specified dobject.
 *  tag-based params (%1,%2,%3,%4,%5):RSSUBSYS,RSTABLE,
 *  IDSUBSYS,IDTAB,IDFIELD;
 *  SQL params (1):ZOID */
public final static int TISQL_LOAD_RS4IDFIELD = TISQL_LOAD_ + 7;
/** all rows from some table for set of ZOIDs taken from some idfield of
 *  specified dobject. RSSUBSYS.RSTAB aliased to T, so all fields in
 *  RSFIELDS must be prefixed by T (T.ZOID,T.ZRID,T.name and so on).
 *  tag-based params (%1,%2,%3,%4,%5,%6):RSSUBSYS,RSTAB,RSFIELDS,
 *  IDSUBSYS,IDTAB,IDFIELD;
 *  SQL params (1):ZOID */
public final static int TISQL_LOAD_RSFIELDS4IDFIELD = TISQL_LOAD_ + 8;
// TISQL_FIND
public final static int TISQL_FIND_ = TISQL_LOAD_ + 8;
public final static int TISQL_FIND_BY_ZNAME = TISQL_FIND_ + 1;
// TISQL_QSEQ
public final static int TISQL_QSEQ_ = TISQL_FIND_ + 1;
public final static int TISQL_QSEQ_CREATE = TISQL_QSEQ_ + 1;
public final static int TISQL_QSEQ_UPDATE = TISQL_QSEQ_ + 2;
public final static int TISQL_QSEQ_NEXT_BY_QOID = TISQL_QSEQ_ + 3;
public final static int TISQL_QSEQ_NEXT_BY_QSID_QNAME = TISQL_QSEQ_ + 4;

public final static String[] SQLQueries = {
// SQL_
 "select user",
 "select sam.get_user()",
 "select dic, count(*) from dic.V_ZDic group by dic order by dic",
 "select dic,code,value,descr from dic.V_ZDic where dic = ?",
 "select dic,code,value,descr from dic.V_ZCodeMsg where dic = ? and lang = ?",
 "select ZD.dic, ZD.code, coalesce(ZC.value,ZD.value) as \"value\", " + 
  "coalesce(ZC.descr,ZD.descr) as \"descr\" from dic.V_ZDic ZD " +
  "LEFT OUTER JOIN  dic.V_ZCodeMsg ZC ON (ZD.dic = ZC.dic and " + 
  "ZD.code = ZC.code and ZC.lang = ?) where ZD.dic = ?",
// TISQL_
 " where ", " like ", " and ", " or ", " not ", " in ", " exists ",
// TISQLCOND_
 " LIMIT ?",
 " OFFSET ?",
 "(ZSID = ?)",
 "(ZTYPE = ?)",
 "(ZO.ZSID = ?)",
 "(ZO.ZTYPE = ?)",
// TISQL_ORDER_
 " order by ZOID",
 " order by ZOID,ZVER",
// TISQLSAM_
 "select * from SAM.V_Group where id in (select gid from SAM.V_UserGroup where uid = ?)",
 "select * from SAM.V_UserCapability where uid = ? order by capcode,sid",
 "select * from SAM.V_User where id in (select uid from SAM.V_UserGroup where gid = ?)",
 "select * from SAM.V_ACLScope where gid = ? order by sid,obj_type",
 "select * from SAM.V_ScopePolicy where sid = ? order by obj_type",
 "select * from SAM.grant_UserGroup(?,?)",
 "select * from SAM.revoke_UserGroup(?,?)",
 "select * from SAM.grant_UserCapability(?,?,?)",
 "select * from SAM.revoke_UserCapability(?,?,?)",
 "select * from SAM.set_Scope_chpts(?,?)",
 "select * from SAM.set_ScopePolicy(?,?,?,?,?)",
 "select * from SAM.delete_ScopePolicy(?,?)",
 "select * from SAM.set_ACLScope(?,?,?,?,?,?,?,?,?)",
 "select * from SAM.delete_ACLScope(?,?,?)",
 "select * from SAM.set_UserPasswordMD5(?,?)",
 "select * from SAM.V_UserPassword where uid = ?",
// TISQL_TIS_
 "select * from tis.commit_object(?,?,?)",
 "select * from tis.create_object(?,?,?)",
 "select * from tis.delete_object(?,?,?)",
 "select * from tis.lock_object(?,?,?)",
 "select * from tis.move_object(?,?,?,?)",
 "select * from tis.open_object(?,?,?)",
 "select * from tis.rollback_object(?,?,?,?)",
 "select * from tis.set_ZNAME(?,?,?)",
 "select * from tis.set_slevel(?,?,?,?)",
 "select * from tis.set_vdate(?,?)",
 "select * from tis.V_ZDateView where uid = SAM.get_user()",
 "select * from tis.unlock_object(?,?,?,?)",
// TISQL_LIST_
 "select * from TIS.V_ZObject ZO",
 "select * from TIS.VL_ZObject ZO",
 "select * from TIS.VI_ZObject ZO",
 "select ZO.*,ZN.ZNAME from TIS.V_ZObject ZO LEFT OUTER JOIN TIS.V_ZNAME ZN ON ZO.ZOID = ZN.ZOID",
 // TISQL_LOAD_
 "select * from TIS.VH_ZObject where (ZOID = ?) order by ZVER desc",
 "select * from TIS.V_ZObject where (ZOID = ?)",
 "select * from TIS.VL_ZObject where (ZOID = ?)",
 "select * from TIS.VI_ZObject where (ZOID = ?)",
 "select * from TIS.V_ZNAME where (ZOID = ?)",
 "select ZN.* from TIS.V_ZNAME ZN, %1.V_%2 T where (ZN.ZOID = T.%3 and T.ZOID = ?)",
 "select T.* from %1.V_%2 T, %3.V_%4 T2 where (T.ZOID = T2.%5 and T2.ZOID = ?)",
 "select %3 from %1.V_%2 T, %4.V_%5 T2 where (T.ZOID = T2.%6 and T2.ZOID = ?)",
// TISQL_FIND_
 "select * from TIS.V_ZNAME where (ZNAME = ?)",
// TISQL_QSEQ_
 "select * from tis.create_qseq(?,?,?,?,?,?)", // create_qseq(v_QSID  bigint, v_QLVL smallint, v_qstart bigint, v_qend  bigint, v_qname varchar(32), v_descr varchar(127))
 "select * from tis.update_qseq(?,?,?,?,?,?)", // not implemented yet (2024-04-22) : update_qseq(QOID bigint, QRSQ bigint,v_qstart,v_qend,v_qname,v_descr);
 "select * from tis.next_qseq(?)", // tis.next_qseq(v_QSID  bigint )
 "select * from tis.next_qseq(?,?)", // tis.next_qseq(v_QOID  bigint, v_qname varchar(32) )
 null
}; //SQLQueries
public final static String[] STD_HEADER_FIELDS =
{ "ZOID", "ZRID", "ZVER", "ZTOV", "ZSID", "ZLVL", "ZPID" };
public final static String[] UPPERCASE_FIELDS = { "ZNAME" };

public static boolean isSTD_HEADER_FIELD(String fname)
{
 if(fname == null) return(false);
 if(!fname.startsWith("Z")) return(false);
 for(int i=0; i<STD_HEADER_FIELDS.length;i++)
 {if(fname.equals(STD_HEADER_FIELDS[i])) return(true); }
 return(false);
}

/** ajust case of field name (ZOID,ZRID,...,ZNAME must be uppercase). */
public static String adjustFieldName(String fname)
{
if(fname==null)return(fname);
String fname_up = fname.toUpperCase();
if(isSTD_HEADER_FIELD(fname_up)) return(fname_up.intern());
 for(int i=0; i<UPPERCASE_FIELDS.length;i++)
 {if(fname_up.equals(UPPERCASE_FIELDS[i])) return(fname_up.intern()); }
return(fname.intern());
}


/** get specific query from the library of queries.
 * Could be useful to porting to another DBMS.
 */
public String getSQL(int query_id)
{
return(SQLQueries[query_id]);
} // getSQL()

public String getSQL(int query_id, String[] tagparams)
{
return(translate_tokens(getSQL(query_id),PARAM_TOKENS,tagparams,tagparams.length));
} // getSQL()

/** set language code for future message gaining.
* @see #getLang
* @see #LANG_RU
* @see #LANG_EN
*/
public void setLang(String lang){user_lang=lang;}
public String getLang(){ return(user_lang); }


/** get message for current language (plain text).
* @see #setLang
*/
public String getMsg(int msg_id)
{
 return(getMsg4Lang(user_lang,msg_id));
} // getMsg

public String getMsg(int msg_id,String arg1)
{ return( getMsg(msg_id,new String[]{arg1}) ); }

public String getMsg(int msg_id,String arg1, String arg2)
{ return( getMsg(msg_id,new String[]{arg1, arg2}) ); }

public String getMsg(int msg_id,String arg1, String arg2, String arg3)
{ return( getMsg(msg_id,new String[]{arg1, arg2, arg3}) ); }

private final static String[] PARAM_TOKENS =
 { "%1", "%2", "%3", "%4", "%5", "%6", "%7", "%8", "%9" };
// this could be useful later, but args[] must be preceded by "%"
// somwhere internally in the getMsgText(int msg_id,String[] args)
// { "%%","%1", "%2", "%3", "%4", "%5", "%6", "%7", "%8", "%9" };
/** get message with positional parameters replased by passed args.
* upto nine positional parameters supported (%1, %2, ..., %9)
*/
public String getMsg(int msg_id,String[] args)
{
 return(translate_tokens(getMsg4Lang(user_lang,msg_id),PARAM_TOKENS,args,args.length));
} // getMsg(int msg_id,String[] args)

public static String getMsg4Lang(String lang,int msg_id)
{
 String msg=null;
 // once at the future:
 String[] msgSet = (String[])htLangSet.get(lang);
 if(msgSet != null) msg=msgSet[msg_id]; else
 if(LANG_EN.equals(lang)) msg=msgEN[msg_id];
 else if(LANG_RU.equals(lang)) msg=msgRU[msg_id];
 if(msg==null) msg=msgDefault[msg_id];
 return(msg);
} // getMsg4Lang

//
// static conversion helpful functions
// obj2text(), obj2html(), obj2value() - useful functions
// translate_tokens() - background work for them
//

 /** convert object to text even if object is null.
 */
 public static String obj2text(Object o)
 {
 if(o == null) return("null"); return(o.toString());
 }

 /** convert object to text but preserve null value if so.
 * @see obj2text
 */
 public static String obj2string(Object o)
 {
 if(o == null) return(null); return(obj2text(o));
 }
 public final static String[] JSON_VALUE_CHARACTERS = { "\n","\r","\"","\\" };
 public final static String[] JSON_VALUE_CHARACTERS_SUBST = {"\\n","\\r","\\","\\\\"};

 public static String text2json(String text)
 {
 return("\"" + translate_tokens(text,JSON_VALUE_CHARACTERS,JSON_VALUE_CHARACTERS_SUBST) + "\"");
 }


 /** replace all sz's occurrences of 'from[x]' onto 'to[x]' and return the result.
  * Each occurence processed once and result depend on token's order at 'from'. 
  * For instance: translate_tokens("hello",new String[]{"he","hel","hl"}, new String[]{"eh","leh","lh"})
  * give "ehllo", not "lehlo" or "elhlo" (in fact "hel" to "leh" translation never be done).
  * @param sz - string for translation
  * @param from - array of tokens to search
  * @param to - array of tokens to translate to
  * @param len - the number of tokens at "from" to look. use -1 to look for all
  *   actually len = min(len,from.length) if len >=0 and len = from.length otherwise
  *	
  */
 public static String translate_tokens(String sz, String[] from, String[] to, int len)
 {
  if(sz == null) return(sz);
  StringBuffer sb = new StringBuffer(sz.length() + 256);
  int p=0;
  if(len<0) len=from.length;
  //if(len>to.length) len=to.length; // let's
  while(p<sz.length())
  {
  int i=0;
  while(i<len) // search for token
  {
   if(sz.startsWith(from[i],p)) { sb.append(to[i]); p=--p +from[i].length(); break; }
   i++;
  }
  if(i>=len) sb.append(sz.charAt(p)); // not found
  p++;
  }
  return(sb.toString());
 } // translate_tokens

 public static String translate_tokens(String sz, String[] from, String[] to)
 {return(translate_tokens(sz,from,to,-1));}

/** this method could be extended to support loadable language-specific
 *  arrays of strings in the future.
 */
private static synchronized void load_localized_messages()
{
 if(!is_initialized){
 // String[] msgRU=new String[msgDefault.length];
 // msgRU[HELLO_WORLD]="Privet Mir!";
 // htLangSet.put(LANG_RU,msgRU);
 is_initialized = true; // to run this code only once
 }
}

// constructors
public ZMessages(){ this(LANG_DEFAULT); }
public ZMessages(String lang){ load_localized_messages(); setLang(lang);}
} //ZMessages
