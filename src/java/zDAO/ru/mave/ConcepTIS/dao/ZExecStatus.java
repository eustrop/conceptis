// ConcepTIS project
// (c) Alex V Eustrop 2009-2010
// see LICENSE at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao;

/** storage for ConcepTIS DB SAM.execstatus composit SQL TYPE.
 *
 */
public class ZExecStatus extends Record
{
public Long ZID; // bigint, -- id of processed object (id,ZOID or ZRID)
public Long ZVER; // bigin,-- version of processed object (ZVER) if applicable
public Integer errnum; // int, -- error number. 0 or less for success (see dic.ErrorMsg)
public String errcode; // char(16), -- symbolic code for errnum
public String errdesc; // varchar(255), -- description of error
// -- parameters included to errdesc (for debug purpose):
public String p1; // varchar(255),
public String p2; // varchar(255),
public String p3; // varchar(255)

public Long getID(){return(ZID);}
public String getKey(){return(errcode);}
public String getCaption(){return(errcode+":"+errdesc);}

// Strings

public final static String F_ZID = "ZID";
public final static String F_ZVER = "ZVER";
public final static String F_ERRNUM = "errnum";
public final static String F_ERRCODE = "errcode";
public final static String F_ERRDESC = "errdesc";

public final static String F_P1 = "p1";
public final static String F_P2 = "p2";
public final static String F_P3 = "p3";

// SQL requests

// record processing methods

public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 ZID = getRSLong(rs,1);
 ZVER = getRSLong(rs,2);
 errnum = getRSInt(rs,3);
 errcode = getRSString(rs,4);
 errdesc = getRSString(rs,5);
 p1 = getRSString(rs,6);
 p2 = getRSString(rs,7);
 p3 = getRSString(rs,8);
} //

public void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException
{
 throw(new java.sql.SQLException("not implemented"));
} //

// constructors
public ZExecStatus() { }
public ZExecStatus(java.sql.ResultSet rs)
 throws java.sql.SQLException
{ rs.next(); this.getFromRS(rs); }

} //ZExecStatus
