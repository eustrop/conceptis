// ConcepTIS project
// (c) Alex V Eustrop 2009-2010
// see LICENSE at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao;

/** Storage for TIS Yes/No/Unknown code value similarly to Boolean class.
 * Object of this class can hold any String code (getCode()/setCode(),
 * but only "Y" code represent the true value (getBoolean()).
 *
 * @see #isUnknown
 */
public class YesNo 
{
public static final String YES = "Y"; 
public static final String NO = "N";
public static final String UNKNOWN = "?";

private String code_value=null;

public String getCode()
{
 if(code_value!=null) return(code_value);
 return(UNKNOWN);
}

public void setCode(String code) {code_value=code;}

/** true if getCode() not in {"Y","N"}. */
public boolean isUnknown()
{
if(getBoolean()) return(false);
if(NO.equals(code_value)) return(false);
return(true);
}

public boolean getBoolean()
{
 if(YES.equals(code_value)) return(true);
 return(false);
}

public void setBoolean(boolean value)
{
 if(value) code_value = YES;
 else code_value = NO;
}

public boolean equals(Object obj)
{
 if(obj == null) return(false);
 try{
  if(((YesNo)obj).getCode().equals(getCode())) return(true);
 }catch(ClassCastException e){}
 return(false);
} // equals(Object obj)

public String toString(){return(getCode());}

public int hashCode()
{
 if(code_value == null) return(0);
 return(code_value.hashCode());
}

// constructors
YesNo(){this(false);}
YesNo(boolean value){setBoolean(value);}
YesNo(String code) {setCode(code);}

} //YesNo
