// ConcepTIS project
// (c) Alex V Eustrop 2009-2010
// see LICENSE at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao;
import java.util.*;

/** Data Object record based on STD_HEADER.
 *
 * @see DObject
 */
public abstract class DORecord extends Record
{

public Long	ZOID;
/** @see setZRID() */
public Long	ZRID;
public Long	ZVER;
public Long	ZTOV;
public Long	ZSID;
public Short	ZLVL;
public Long	ZPID;

/** use this method to redistribute newly assigned ZRID
 * to all children ZPID fields.
 * @see organizeChildren()
 */
public void setZRID(Long newZRID)
{
ZRID = newZRID;
organizeChildren();
}

/** should implements ZOID/ZPID redistribution on children records and so on. */
public void organizeChildren()
{ // for all children
 int cnt_chld = countChildren();
 for(int i=0;i<cnt_chld;i++)
 {
  DORecord r = getChild(i);
  r.ZPID = ZRID;
  r.ZOID = ZOID;
  if(isDeleted()) {
   if( !r.isDeleted() ) r.deleteIndirect();
   if( !isMarkedForDeletion() && r.isMarkedForDeletion() )
	{ r.deleteIndirect(); r.commitDeletion(); }
  } //if
 } //for
} //organizeChildren

private final static byte DEL_STATUS_NONE = 0;
private final static byte DEL_STATUS_MARKED_DIRECT = 1;
private final static byte DEL_STATUS_MARKED_INDIRECT = 2;
private final static byte DEL_STATUS_DELETED = 3;
private final static byte DEL_STATUS_DELETED_DIRECT = 3;
private final static byte DEL_STATUS_DELETED_INDIRECT = 4;

private byte deletion_status = DEL_STATUS_NONE;

private RList[] rl_children;

/** array of newly created array of DORecords, one instance
 * for each type allowed as child for this type of record.
 */
public abstract DORecord[] createChildrenFactory();

/** array of codes of all record types allowed as children. */
public abstract String[] getChildTabCodes();

/** number of children records (all types). */
public int countChildren()
{
 int count=0;
 if(rl_children==null) return(0);
 for(int i=0;i<rl_children.length;i++)
 { if(rl_children[i]!=null) count=count+rl_children[i].size(); }
 return(count);
} //

/** Collection of children records of specified type. */
public RList getTableChildren(String tab_code)
{
 String[] codes=getChildTabCodes();
 if(codes.length == 0) return(null);
 if(rl_children==null) rl_children = new RList[codes.length];
 for(int i=0;i<codes.length;i++)
 {
  if(!codes[i].equals(tab_code))continue;
  if(rl_children[i]==null) rl_children[i]= new RList();
  return(rl_children[i]);
 }
 return(null);
} //getTableChildren(String tab_code)

public DORecord getChild(int i)
{return(find_child_by_index(i,false));}


private DORecord find_child_by_index(int i, boolean delete)
{
 int ii=i;
 if(rl_children!=null && i >= 0)
 {
  String[] codes=getChildTabCodes();
  for(int k=0;k<codes.length;k++)
  {
   if(rl_children[k]==null) continue;
   if(rl_children[k].size()>ii){
    DORecord r = (DORecord)rl_children[k].get(ii);
    if(delete) rl_children[k].remove(ii);
    return(r);
   }
   ii=ii-rl_children[k].size();
  }
 }
throw(new  IndexOutOfBoundsException("getChild(" + i + ")") );
} //getChild(int i)

public DORecord getChild(String tab_code,int i)
{
 RList rl = getTableChildren(tab_code);
 if(rl != null) return((DORecord)rl.get(i));
 throw(new  IndexOutOfBoundsException(
    "getChild(tab_code=\"" + tab_code + "\","+ i + ")") );
}

/** throws ZExcetion if code not equals getTabCode(). */
private void test_tab_code(String code) throws ZException
{
 String this_code = getTabCode();
 if(this_code.equals(code)) return;
 ZMessages zm = new ZMessages();
  throw(new ZException(
    zm.getMsg(zm.ERR_INVALID_CODE_X_REQUIRED_Y,code,this_code)));
}

private void put_children_to_fs(ZFieldSet fs)
{
 int num_children = countChildren();
 for(int i=0;i<num_children;i++)
 { fs.add(getChild(i).toFieldSet() ); }
}

private void get_children_from_fs(ZFieldSet fs)
throws ZException
{
 int num_fs = fs.countFS();
 int i;
 DORecord[] chf = createChildrenFactory();
 String[] chf_codes = getChildTabCodes();
 Hashtable hchf = new Hashtable();
 for(i=0;i<chf.length;i++){hchf.put(chf_codes[i],chf[i]);}
 for(i=0;i<num_fs;i++)
 {
  ZFieldSet child_fs=fs.getFS(i);
  DORecord fac=(DORecord)hchf.get(child_fs.getCode());
  if(fac==null) continue;
  addChild(fac.createFromFS(child_fs));
 }
}


public DORecord getChild(String tab_code,Long ZRID)
{
 RList rl = getTableChildren(tab_code);
 if(rl != null) return((DORecord)rl.getByID(ZRID));
 throw(new  IndexOutOfBoundsException(
    "getChild(tab_code=\"" + tab_code + "\",ZRID="+ ZRID + ")") );
}

public void addChild(DORecord r)
{
 RList rl = getTableChildren(r.getTabCode());
 if(rl == null) throw(new  IllegalArgumentException());
 rl.add(r);
 r.ZPID = ZRID;
}

public void removeChild(DORecord r)
{
 RList rl = getTableChildren(r.getTabCode());
 if(rl == null) throw(new  IllegalArgumentException());
 rl.remove(r);
}

public void removeChild(int i) {find_child_by_index(i,true);}

public void removeChild(String tab_code,int i)
{
 RList rl = getTableChildren(tab_code);
 if(rl != null){ rl.remove(i); return; }
 throw(new  IndexOutOfBoundsException(
    "removeChild(tab_code=\"" + tab_code + "\","+ i + ")") );
}

public void clearChildren(){rl_children=null;};

public Enumeration enumChildTabCodes(){
 String[] codes=getChildTabCodes();
 Vector v = new Vector(codes.length);
 for(int i=0;i<codes.length;i++){v.add(codes[i]);}
 return( v.elements() );
}

public boolean isDeleted(){return(deletion_status > 0);}
public boolean isDeletedIndirect()
{
 return( deletion_status==DEL_STATUS_MARKED_INDIRECT ||
    deletion_status==DEL_STATUS_DELETED_INDIRECT );
}

/** true if no deletion done in the database. */
public boolean isMarkedForDeletion()
{ return(deletion_status > 0 && deletion_status < DEL_STATUS_DELETED); }

/** true if deletion reflected to the DB. */
public boolean isCommittedDeletion()
{ return(deletion_status >= DEL_STATUS_DELETED); }

/** mark this record for deletion. */
public void delete() {deletion_status=DEL_STATUS_MARKED_DIRECT; }
/** mark this record for indirect deletion. */
public void deleteIndirect() {deletion_status=DEL_STATUS_MARKED_INDIRECT; }
/** commit deletion. */
public void commitDeletion()
{
 if(isDeleted() && isMarkedForDeletion() )
  deletion_status = (byte)(deletion_status + DEL_STATUS_DELETED - 1);
 else return;
 organizeChildren();
 // int cnt_chld = countChildren(); for(int i=0;i<cnt_chld;i++)
 //{ DORecord r = getChild(i); r.deleteIndirect(); r.commitDeletion(); }
} //commitDeletion()

// constants
public final static long ZC_ZTOV_UPDATED = -1;
public final static long ZC_ZTOV_DELETED = -2;
public final static long ZC_ZTOV_NOCHANGES = -3;
public final static long ZC_ZTOV_RDELETED = -4;

// Strings

public final static String F_ZOID = "ZOID";
public final static String F_ZRID = "ZRID";
public final static String F_ZVER = "ZVER";
public final static String F_ZTOV = "ZTOV";
public final static String F_ZSID = "ZSID";
public final static String F_ZLVL = "ZLVL";
public final static String F_ZPID = "ZPID";

public final static String FC_ZOID = "ZOID";
public final static String FC_ZRID = "ZRID";
public final static String FC_ZVER = "ZVER";
public final static String FC_ZTOV = "ZTOV";
public final static String FC_ZSID = "ZSID";
public final static String FC_ZLVL = "ZLVL";
public final static String FC_ZPID = "ZPID";

public Long getID(){return(ZRID);}
public String getKey(){return(null);}

// record processing methods

/** import all fields values from passed record
 * (object values aren't reused but cloned, children records aren't imported).*/
public void importData(DORecord dor)
{
try {
   getFromFS(dor.toFieldSet());
   }catch(ZException ze){throw(new RuntimeException(ze.toString())); }
}

/** (semi-abstract) import data from current row of SQL ResultSet. */
public void getFromRS(java.sql.ResultSet rs)
 throws java.sql.SQLException
{
 ZOID = getRSLong(rs,1);
 ZRID = getRSLong(rs,2);
 ZVER = getRSLong(rs,3);
 ZTOV = getRSLong(rs,4);
 ZSID = getRSLong(rs,5);
 ZLVL = getRSShort(rs,6);
 ZPID = getRSLong(rs,7);
} //

/** (semi-abstract) import data from passed FieldSet. */
public void getFromFS(ZFieldSet fs)
 throws ZException
{
 test_tab_code(fs.getCode()); //throws ZException
 ZOID = getFSLong(fs,F_ZOID);
 ZRID = getFSLong(fs,F_ZRID);
 ZVER = getFSLong(fs,F_ZVER);
 ZTOV = getFSLong(fs,F_ZTOV);
 ZSID = getFSLong(fs,F_ZSID);
 ZLVL = getFSShort(fs,F_ZLVL);
 ZPID = getFSLong(fs,F_ZPID);
 get_children_from_fs(fs);
 setZRID(ZRID);
 if(fs.getMarkedForDeletion()) delete();
} //

public ZFieldSet toFieldSet()
{
 ZFieldSet fs = new ZFieldSet();
 fs.setCode(getTabCode());
 fs.setMarkedForDeletion(isMarkedForDeletion());
 fs.add(new ZField(F_ZOID,ZOID,FC_ZOID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_ZRID,ZRID,FC_ZRID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_ZVER,ZVER,FC_ZVER,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_ZTOV,ZTOV,FC_ZTOV,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_ZSID,ZSID,FC_ZSID,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_ZLVL,ZLVL,FC_ZLVL,ZField.T_UNKNOWN,0));
 fs.add(new ZField(F_ZPID,ZPID,FC_ZPID,ZField.T_UNKNOWN,0));
 put_children_to_fs(fs);
 return(fs);
} //

public abstract void putToPS(java.sql.PreparedStatement ps)
 throws java.sql.SQLException;

public abstract String getSubsysCode();
public abstract String getTabCode();
public abstract String getParentTabCode();
public abstract String getTabName();
public abstract int getNumOfFields();

public String getParamQuestMarks()
{
 int i=getNumOfFields();
 StringBuffer sb = new StringBuffer(i*2);
 while(i-- > 0){sb.append('?'); if(i>0) sb.append(','); }
 return(sb.toString());
}

// SQL requests
public final static String TISQL_LIST = "select * from %S.V_%T";
public final static String TISQL_LOAD = "select * from %S.V_%T where ZOID = ?";
public final static String TISQL_LOAD_INTERMEDIATE =
	"select * from %S.VI_%T where ZOID = ?";
public final static String TISQL_LOAD_HISTORY =
	"select * from %S.VH_%T where ZOID = ? and ZVER = ?";
public final static String TISQL_CREATE ="select * from %S.create_%T(?,?,?,%P)";
public final static String TISQL_UPDATE ="select * from %S.update_%T(?,?,?,%P)";
public final static String TISQL_DELETE = "select * from %S.delete_%T(?,?,?)";
public final static String[] TISQL_DOR_TOKENS = {"%S","%T","%P"};


public String getTISQL_LOAD()
 {return(ZMessages.translate_tokens(TISQL_LOAD,TISQL_DOR_TOKENS,
  new String[]{getSubsysCode(),getTabName()},2));}
public String getTISQL_LOAD_INTERMEDIATE()
 {return(ZMessages.translate_tokens(TISQL_LOAD_INTERMEDIATE,TISQL_DOR_TOKENS,
  new String[]{getSubsysCode(),getTabName()},2));}
public String getTISQL_CREATE()
 {return(ZMessages.translate_tokens(TISQL_CREATE,TISQL_DOR_TOKENS,
  new String[]{getSubsysCode(),getTabName(),getParamQuestMarks()} ));}
public String getTISQL_UPDATE()
 {return(ZMessages.translate_tokens(TISQL_UPDATE,TISQL_DOR_TOKENS,
  new String[]{getSubsysCode(),getTabName(),getParamQuestMarks()} ));}
public String getTISQL_DELETE()
 {return(ZMessages.translate_tokens(TISQL_DELETE,TISQL_DOR_TOKENS,
  new String[]{getSubsysCode(),getTabName()},2));}

/** create new record object from passed recordset data. */
public abstract DORecord createFromRS(java.sql.ResultSet rs)
  throws java.sql.SQLException ;
/** create new record object from  passed fieldset data. */
public abstract DORecord createFromFS(ZFieldSet fs)
  throws ZException ;
/** create new record. */
public abstract DORecord createInstance();

} //DORecord
