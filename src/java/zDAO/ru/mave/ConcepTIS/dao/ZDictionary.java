// ConcepTIS project
// (c) Alex V Eustrop 2009-2010
// see LICENSE at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao;

/** Read only dictionary - collection of DCode
 * records indexed by getCode().
 *
 */
public class ZDictionary
{
private RList rlist = new RList();
private ZDictionary default_lang = null;

public int size(){return(rlist.size());}
protected void add(DCode dc) { rlist.add(dc); }

public ZDictionary getDefault(){return(default_lang);}

public DCode get(int i){ return( (DCode)rlist.get(i) ); }
public DCode getByCode(String code)
{
 DCode dc = (DCode)rlist.getByKey(code);
 if(dc == null && default_lang != null)
    dc = default_lang.getByCode(code);
 return(dc);
} // getByCode(String code)

public String getValue(String code)
{
 DCode dc = getByCode(code);
 if(dc == null) return(null);
 return(dc.getValue());
}

public String getCaption(String code)
{
 DCode dc = getByCode(code);
 if(dc == null) return( (new DCode(code,null,null)).getCaption() );
 return(dc.getCaption());
}

// Constructors
public ZDictionary() { }
public ZDictionary(ZDictionary default_lang)
 { this.default_lang = default_lang; }

} //Dictionary
