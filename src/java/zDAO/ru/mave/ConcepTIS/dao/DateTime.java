// ConcepTIS project
// (c) Alex V Eustrop 2009-2010
// see LICENSE at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.dao;

/** Storage for Date, Time or Timestamp data.
 */
public class DateTime extends java.sql.Timestamp
{
// constructors
public DateTime(){this(0);}
public DateTime(long time){super(time);}
public DateTime(java.util.Date time) {super(time.getTime());}
public DateTime(java.sql.Timestamp time)
{super(0); if(time!=null){ setTime(time.getTime());
  super.setNanos(time.getNanos());} }
public DateTime(String time)
 {this(java.sql.Timestamp.valueOf(time));}

} //DateTime
