package org.eustrosoft.qtis.ftpub.util;

import org.eustrosoft.qtis.ftpub.FileDetails;
import org.eustrosoft.qtis.ftpub.FtpDao;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.Arrays;

import static org.eustrosoft.qtis.ftpub.util.StringUtils.getStrValueOrEmpty;

public final class HttpUtil {

    public static final String[] AVAILABLE_CONTENT_TYPES = new String[]{
            "application/octet-stream", "application/json",
            "application/pdf", "application/xml", "text/plain",
            "image/gif", "image/jpeg", "image/jpg", "image/png",
            "image/svg+xml","image/svg", "video/mp4", "video/mpeg"
    };

    public static void printBackLine(HttpServletRequest request, PrintWriter writer, String path) {
        String[] pathParts = FtpDao.getPathParts(path);
        if (pathParts.length >= 1) {
            StringBuilder backPath = new StringBuilder();
            backPath.append(FtpDao.SEPARATOR);
            for (int i = 0; i < pathParts.length - 1; i++) {
                backPath.append(pathParts[i]);
                backPath.append(FtpDao.SEPARATOR);
            }
            writer.println("<td>");
            writer.println(String.format(
                        "<a href=\"%s\"> %s </a>",
                        getFormedFilePath(request, backPath.toString(), ""),
                        ".."
                    )
            );
            writer.println("</td>");
        }
    }

    public static void printLinkForPath(HttpServletRequest request, PrintWriter writer,
                                        String basePath, FileDetails fileDetails) {
        String path = getFormedFilePath(request, basePath, fileDetails.getFileName());
        writer.println("<tr>");
        writer.println("<td style=\"width: 60%\">");
        writer.println(String.format("<a href=\"%s\"> %s </a>", path, fileDetails.getFileName()));
        writer.println("</td>");
        writer.println("<td style=\"width: 20%\">");
        writer.println(StringUtils.getStrValueOrEmpty(fileDetails.getDescription()));
        writer.println("</td>");
        writer.println("<td style=\"width: 10%\">");
        writer.println(StringUtils.getStrValueOrEmpty(fileDetails.getSecurityLevel()));
        writer.println("</td>");
        writer.println("<td style=\"width: 10%\">");
        writer.println(StringUtils.getStrValueOrEmpty(fileDetails.getFileLength()));
        writer.println("</td>");
        writer.println("</tr>");
    }

    private static String getFormedFilePath(HttpServletRequest request, String basePath, String fileName) {
        String bp = basePath.endsWith(FtpDao.SEPARATOR) ? basePath : String.format("%s%s", basePath, FtpDao.SEPARATOR);
        String servlet_path = request.getServletPath();
        if(servlet_path == null) servlet_path = "";
        if(servlet_path == "") servlet_path = request.getContextPath();
        else servlet_path = request.getContextPath() + servlet_path ;
        return String.format("%s%s%s", servlet_path , bp, fileName);
        //return String.format("%s%s%s", request.getContextPath() , bp, fileName);
    }

    public static void setHeadersForFileDownload(HttpServletResponse response, FileDetails fileDetails) {
        response.reset(); //SIC! to thing about (some header has been set before)
        String mime_type = fileDetails.getMimeType();
        String file_name = fileDetails.getFileName();
	if(isEmptyMimeType(mime_type) && file_name != null)
	{
	    if(file_name.endsWith(".pdf")) mime_type = "application/pdf";
	    if(file_name.endsWith(".PDF")) mime_type = "application/pdf";
	    if(file_name.endsWith(".jpg")) mime_type = "image/jpeg";
	    if(file_name.endsWith(".JPG")) mime_type = "image/jpeg";
	    if(file_name.endsWith(".jpeg")) mime_type = "image/jpeg";
	    if(file_name.endsWith(".JPEG")) mime_type = "image/jpeg";
	    if(file_name.endsWith(".png")) mime_type = "image/png";
	    if(file_name.endsWith(".PNG")) mime_type = "image/png";
	    if(file_name.endsWith(".svg")) mime_type = "image/svg+xml";
	    if(file_name.endsWith(".SVG")) mime_type = "image/svg+xml";
	    if(file_name.endsWith(".txt")) mime_type = "text/plain";
	    if(file_name.endsWith(".TXT")) mime_type = "text/plain";
	}
        setFileContentType(response, mime_type);
        //response.setCharacterEncoding(fileDetails.getEncoding());
        response.setHeader(
                "Content-Disposition",
                String.format(
                        //"attachment; filename=\"%s\"",
                        "inline; filename=\"%s\"",
                        fileDetails.getFileName()
                )
        );
        response.setContentLengthLong(fileDetails.getFileLength());
        response.setBufferSize(1024);
        response.setHeader("Accept-Ranges", "bytes");
    }

    public static String getServletPath(HttpServletRequest request) {
        String servletPath = request.getPathInfo();
        return servletPath == null || servletPath.isEmpty() ? FtpDao.SEPARATOR : request.getPathInfo();
    }

    public static void checkPathInjection(String... params) throws Exception {
        for (String param : params) {
            if (param == null || param.isEmpty()) {
                throw new Exception("Param was null or empty.");
            }
            if (param.contains("..")) {
                throw new Exception("Path Injection Detected.");
            }
        }
    }

    public static void setFileContentType(HttpServletResponse httpResponse, String mimeType) {
        if (mimeType != null && !mimeType.isEmpty()) {
            if (isAvailableContentType(mimeType)) {
                httpResponse.setContentType(mimeType);
                return;
            }
        }
        httpResponse.setContentType("application/octet-stream");
    }
    public static boolean isEmptyMimeType(String mime_type)
    {
        if(mime_type == null) return true;
        if(mime_type.isEmpty()) return true;
        return(false);
    }

    protected static boolean isAvailableContentType(String contentType) {
        return Arrays.asList(AVAILABLE_CONTENT_TYPES).contains(contentType);
    }

    private HttpUtil() { }
}
