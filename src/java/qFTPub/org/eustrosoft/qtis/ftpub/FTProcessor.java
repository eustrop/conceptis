package org.eustrosoft.qtis.ftpub;

import org.eustrosoft.qtis.ftpub.util.WebParams;
import org.eustrosoft.qdbp.QDBPSession;
import org.eustrosoft.qdbp.QDBPool;
import org.eustrosoft.qtis.ftpub.util.StringUtils;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.eustrosoft.qtis.ftpub.util.StringUtils.getStrValueOrEmpty;
import static org.eustrosoft.qtis.ftpub.util.HttpUtil.*;

public class FTProcessor {
    private HttpServletRequest request;
    private HttpServletResponse response;
    private QDBPSession session;
    public void setQDBPSession(QDBPSession s){session=s;}
    public static final String VIRTUAL_PATH = "/s";
    public static final String VERSION = "0.1.5.3";
    public static String getVersion(){return(VERSION);}


    //@Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        //QDBPSession session=null;
        this.request = req;
        this.response = resp;
        request.setCharacterEncoding("UTF-8");

        // Login
        if(session == null) {
            response.sendError(503,"No session with DB, call administrator");
            return;
        }

        // Processing path
        try {
            synchronized (session) {
                String contextPath = getServletPath(req);
                printContentByPath(session, contextPath);
            }
        } catch (Exception e) {
            response.sendError(500,"Exception while processing path, be sure this is correct path or call administrator.\n"
                                   + e.toString());
            //e.printStackTrace();
        }
    }

    private void printContentByPath(QDBPSession session,String path)
            throws Exception {
        checkPathInjection(path);
	if(!path.startsWith(VIRTUAL_PATH))
        {
         PrintWriter writer = response.getWriter();
	 writer.println("<html>");
         Long millis = System.currentTimeMillis();
         writer.println("<meta charset=\"UTF-8\">");
         writer.println("<h1>Index of " + path + "</h1>");
         writer.println("<hr>");
	 writer.println("<tt>");
         writer.println("<a href='/'>..</a>");
	 writer.println(" - home of this site");
	 writer.println("</tt>");
         writer.println("<br>");
	 writer.println("<tt>");
	 writer.print("<a href='");
	 writer.print(request.getContextPath());
	 writer.print(request.getServletPath());
	 writer.print(VIRTUAL_PATH);
	 writer.print("/'>");
	 writer.print(VIRTUAL_PATH);
	 writer.print("/");
	 writer.println("</a>");
	 writer.println(" - virtual directory. mount point for list of data scopes");
	 writer.println("</tt>");
         writer.println("<hr>");
         long timing = System.currentTimeMillis() - millis;
         writer.println("<p> Timing: " + timing + " ms. </p>");
         writer.println("</html>");
	 return;
        }

        Connection connection = session.getSQLConnection();
        if (connection == null) {
            throw new SQLException("No SQL connection, call the administrator");
        }

        String fullPath = null;
        try {
	    fullPath = FtpDao.getFullPath(connection, path.substring(VIRTUAL_PATH.length()));
        }
	catch (Exception e)
	{
	    response.sendError(404,"File not found");
	    return;
	}
        if (FtpDao.isFile(connection, fullPath)) {
            downloadFile(response, fullPath, connection);
        } else {
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Charset", "UTF-8");
            printFiles(path, connection, fullPath);
        }
    }

    private void printFiles(String path, Connection connection, String fullPath) throws Exception {
        PrintWriter writer = response.getWriter();
        writer.println("<html>");
        Long millis = System.currentTimeMillis();
        writer.println("<meta charset=\"UTF-8\">");
        writer.println("<h1>Index of " + path + "</h1>");
        writer.println("<hr>");
        PreparedStatement preparedStatement = FtpDao.getViewStatementForPath(connection, fullPath);
        if (preparedStatement != null) {
            ResultSet resultSet = preparedStatement.executeQuery();

            writer.println("<table style=\"border: none; width: 80vw\">");
            printBackLine(request, writer, path);
            while (resultSet.next()) {
                try {
                    String name = StringUtils.getStrValueOrEmpty(resultSet, Constants.NAME);
                    String fname = StringUtils.getStrValueOrEmpty(resultSet, Constants.F_NAME);
                    String finalName = fname.isEmpty() ? name : fname;
                    CMSType type = CMSType.getType(resultSet);
                    String descr = StringUtils.getStrValueOrEmpty(resultSet, Constants.DESCRIPTION);
                    String zlvl = StringUtils.getStrValueOrEmpty(resultSet, Constants.ZLVL);
                    String fileId = StringUtils.getStrValueOrEmpty(resultSet, "f_id");
                    Long fileLength = 0L;
                    if (fileId != null && !fileId.isEmpty()) {
                        fileLength = FtpDao.getFileLength(connection, Long.parseLong(fileId));
                    }
                    if (type.equals(CMSType.DIRECTORY)) {
                        finalName = finalName + FtpDao.SEPARATOR;
                    }
                    printLinkForPath(
                            request, writer, path,
                            new FileDetails(
                                    null, finalName, descr, zlvl, type, fileLength, null
                            )
                    );
                } catch (Exception ex) {
                    //ex.printStackTrace();
                }
            }
            writer.println("</table>");
            preparedStatement.close();
            resultSet.close();
        }
        writer.println("<hr>");
        long timing = System.currentTimeMillis() - millis;
        writer.println("<p> Timing: " + timing + " ms. </p>");
        writer.println("</html>");
    }

    private void downloadFile(HttpServletResponse response, String path, Connection connection) throws IOException {
        OutputStream outputStream = response.getOutputStream();
        try {
            setHeadersForFileDownload(response, new FtpDao().getFileDetails(connection, path));
            response.setHeader("X-FTProcessor-version", getVersion());
            new FtpDao().printToOutput(connection, outputStream, path);
        } catch (Exception ex) {
            String unexpectedError = "Unexpected Error Occurred. Call administrator.";
            outputStream.write(unexpectedError.getBytes());
        } finally {
            outputStream.flush();
            outputStream.close();
        }
    }
}
