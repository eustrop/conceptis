package org.eustrosoft.qtis.ftpriv;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import org.eustrosoft.qtis.ftpub.util.WebParams;
import org.eustrosoft.qdbp.QDBPSession;
import org.eustrosoft.qdbp.QDBPool;
import java.sql.SQLException;
import org.eustrosoft.qtis.ftpub.FTProcessor;
import org.eustrosoft.qtis.SessionCookie.*;

public class FTPServlet extends HttpServlet {

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        QDBPSession session = null;
        //try {
            session = getQDBPSession(req,resp);
        //} catch (SQLException exception) {
        if(session == null) {
            resp.getWriter().println("Exception while logon, call administrator");
            //exception.printStackTrace(); //SIC! 
            return;
        }
        // process request
        FTProcessor ftp = new FTProcessor();
        ftp.setQDBPSession(session);
        resp.setHeader("X-FTPServlet-version", "0.1.5-priv");
        resp.setHeader("X-FTProcessor-version", ftp.getVersion());
        ftp.doGet(req,resp);
        ftp.setQDBPSession(null);
    }
    private QDBPSession logon(HttpServletRequest request, HttpServletResponse response) throws SQLException {
        QDBPool pool = QDBPool.get(WebParams.getString(request, WebParams.DB_POOL_NAME));
        if (pool == null) {
            pool = new QDBPool(
                    WebParams.getString(request, WebParams.DB_POOL_NAME),
                    WebParams.getString(request, WebParams.DB_POOL_URL),
                    WebParams.getString(request, WebParams.DB_JDBC_CLASS)
            );
            QDBPool.add(pool);
        }
        return(pool.logon());
    }
// this code imported from org/eustrosoft/qtis/webapps/WARequestHandler.java 2023-12-22 18:55

 //public void setQDBPSession(QDBPSession s){this.dbps = s;}
 public QDBPSession getQDBPSession(HttpServletRequest request, HttpServletResponse response)
 {
  // BEGIN PATCHES
  QDBPSession dbps = null;
  QTISSessionCookie session_cookie = null;
  String QDBPOOL_NAME = WebParams.getString(request, WebParams.DB_POOL_NAME);
  String QDBPOOL_URL = WebParams.getString(request, WebParams.DB_POOL_URL);
  String QDBPOOL_JDBC_CLASS = WebParams.getString(request, WebParams.DB_JDBC_CLASS);
  // END PATCHES
   if(dbps != null) return(dbps);
   // 1. get or create DB pool
   QDBPool dbp = QDBPool.get(QDBPOOL_NAME);
   //if(dbps != null) dbps.free(); //SIC! don't implemented yet
   dbps = null;
   if(dbp == null){dbp = new QDBPool(QDBPOOL_NAME,QDBPOOL_URL,QDBPOOL_JDBC_CLASS); QDBPool.add(dbp);}
   // 2. get session
   // 2.1. get session cookie and its value if so
   session_cookie = new QTISSessionCookie(request, response);
   // 2.2. get session by cookie
   dbps = dbp.logon(session_cookie.value());
   // debug: if(dbps == null) throw new Exception(session_cookie.value() + QDBPOOL_NAME);
   // 2.3. renew session if ready (reserved for future use)
   if(dbps != null && dbps.isSessionRenewReady()) {dbps.renewSession(); session_cookie.set(dbps.getSessionSecretCookie(),dbps.getSessionCookieMaxAge()); }
   if(dbps == null) dbps = dbp.createSession(); // temporary session stub, not stored in the pool
   // END get session
   // 3. process request
   // register JDBC driver
   //register_jdbc_driver("org.postgresql.Driver");
   // open JDBC connection
   //dbc=DriverManager.getConnection(getJDBCURL(),request.getRemoteUser(),"");
   //dbps.lock();
   return(dbps);
 }
}
