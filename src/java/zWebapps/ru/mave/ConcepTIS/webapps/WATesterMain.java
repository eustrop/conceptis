package ru.mave.ConcepTIS.webapps;

/** this class is for testing und debugging purpose only. */
public class WATesterMain
{
 /** this method is for testing und debugging purpose only. */
 public static void main(String[] varg)
 {
  test_WAMessages();
 } // main()
 private static void test_WAMessages()
 {
  WAMessages wam = new WAMessages();
  System.out.println(wam.getMsg(wam.HELLO_WORLD));
  //wam.HELLO_WORLD=2;
  System.out.println(wam.getLang());
  wam.setLang(wam.LANG_RU);
  System.out.println(wam.getMsg(wam.HELLO_WORLD));
  System.out.println(wam.getMsg(wam.YES));
  System.out.println(wam.HELLO_WORLD);
  System.out.println(wam.getLang());
  // parameters substitution testing
  System.out.println(wam.getMsg(wam.DEBUG_9PARAMS,
   new String[]{"1","dva","three","quattour","pente","sechs",
   "sept","otto","<b>nueve<b>"}));
  System.out.println(wam.getMsgHtml(wam.DEBUG_9PARAMS,
   new String[]{"1","dva","three","quattour","pente","sechs",
   "sept","otto","<b>nueve<b>"}));
  System.out.println(wam.getMsg(wam.DEBUG_9PARAMS,"1","dva","three"));
  // html unsafe message testing
  System.out.println(wam.getMsgText(wam.DEBUG_HTMLUNSAFE_EXAMPLE));
  System.out.println(wam.getMsg(wam.DEBUG_HTMLUNSAFE_EXAMPLE));
  System.out.println(wam.getMsgText(wam.DEBUG_HTMLUNSAFE_EXAMPLE,
   new String[]{"<b>Yes</b>","<a href=/>No</a>"}));
  System.out.println(wam.getMsg(wam.DEBUG_HTMLUNSAFE_EXAMPLE,
   "<b>Yes</b>","<a href=/>No</a>"));
  System.out.println(wam.getMsgHtml(wam.DEBUG_HTMLUNSAFE_EXAMPLE,
	"<b>Yes</b>","<a href=/>No</a>"));
  } // test_WAMessages()
} //WATesterMain
