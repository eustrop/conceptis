// ConcepTIS project
// (c) Alex V Eustrop 2009-2010
// see LICENSE at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.webapps;

import java.util.*;
import java.sql.*;
import ru.mave.ConcepTIS.dao.*;
import ru.mave.ConcepTIS.dao.TIS.*;
import ru.mave.ConcepTIS.dao.SAM.*;

public class WARHTISObject extends WARequestHandler{
	

 public static final String SUBSYS_THIS = WAMessages.SUBSYS_TIS;

 // interfaces/abstracts implementation

 public void processREST() { was.printRESTNotImplemented(); }

 public void process(){
 
  String req = request.getParameter(wam.PARAM_REQUEST);
  was.beginHMenu();
 was.printHMenuSeparator();
  //was.printHMenuCaption(wam.getMnuDesc(wam.MNU_SAM_MAIN)+":");
  was.printHMenuItem(wam.MNU_LIST);
  was.printHMenuItem(wam.MNU_CREATE);
  was.printHMenuItem(wam.MNU_FIND);
  was.printHMenuItem(wam.MNU_TIS_MENU);
  was.closeHMenu();
  was.printSeparator();
  try{
	  db_logon(); 
	  } catch(Exception e){
		  printerrmsg(e); 
		  return; 
		  }
  try{
	  
   if(wam.REQUEST_VIEW.equals(req))
	   do_view(); 
   else if(wam.REQUEST_CREATE.equals(req))
	   do_create();
   else if(wam.REQUEST_OPEN.equals(req))
	   do_open();
   else if(wam.REQUEST_COMMIT.equals(req))
	   do_commit();
   else if(wam.REQUEST_ROLLBACK.equals(req))
	   do_rollback();
   else if(wam.REQUEST_LOCK.equals(req))
	   do_lock();
   else if(wam.REQUEST_UNLOCK.equals(req))
	   do_unlock();
   else if(wam.REQUEST_DELETE.equals(req) )
		   do_delete();
   else if(wam.REQUEST_FIND.equals(req))
	   do_find();
   else if(wam.REQUEST_MOVE.equals(req))
	   do_move();
   else if(wam.REQUEST_SETSLEVEL.equals(req))
	   do_setslevel();
   else if(wam.REQUEST_SETVDATE.equals(req))
	   do_setvdate();
   else if(wam.REQUEST_MENU.equals(req))
	   do_menu();
   else if(wam.REQUEST_BTNMENU.equals(req))
	   process_btnmenu();
   else if(req == null || wam.REQUEST_LIST.equals(req)) 
	   do_list();
   else
	   throw(new WAException("test"));//throw(new WAException(wam,wam.ERROR_NOT_IMPLEMENTED));
  }catch(Exception e){
	  printerrmsg(e);
	  return;
	  }
  finally{ 
	  db_logoff();
	  } 
 } // process
 
/** list pf available actions. */
public void do_menu(){

// TIS generic actions
 was.beginTMenu();
 was.printTMenuCurrent(wam.MNU_TIS_MAIN);
 was.beginTMenu();
 was.printTMenuItem(wam.MNU_LIST);
 was.printTMenuItem(wam.MNU_FIND);
 was.printTMenuItem(wam.MNU_VIEW);
 was.printTMenuSeparator();
 was.printTMenuItem(wam.MNU_CREATE);
 was.printTMenuItem(wam.MNU_OPEN);
 was.printTMenuItem(wam.MNU_COMMIT);
 was.printTMenuItem(wam.MNU_ROLLBACK);
 //was.printTMenuItem(wam.MNU_EDIT);
 //was.printTMenuItem(wam.MNU_SAVE);
 was.printTMenuItem(wam.MNU_CMP);
 was.printTMenuSeparator();
 was.printTMenuItem(wam.MNU_LOCK);
 was.printTMenuItem(wam.MNU_UNLOCK);
 was.printTMenuSeparator();
 was.printTMenuItem(wam.MNU_DELETE);
 was.printTMenuSeparator();
 was.printTMenuItem(wam.MNU_MOVE);
 was.printTMenuItem(wam.MNU_SETSLEVEL);
 was.printTMenuSeparator();
 was.printTMenuItem(wam.MNU_SETVDATE);
 was.printTMenuSeparator();
 was.printTMenuItemCustomPSQL(wam.getMsgText(wam.CAPTION_GET_ZOBJECT_SEQUENCE_VALUE),wam.getSQL(wam.SQL_GET_ZOBJECT_SEQUENCE_VALUE));
 was.printTMenuItemCustomPSQL(wam.getMsgText(wam.CAPTION_SET_ZOBJECT_SEQUENCE_VALUE),wam.getSQL(wam.SQL_SET_ZOBJECT_SEQUENCE_VALUE));
 was.closeTMenu();
 was.closeTMenu();
} // do_menu()

 /** process request passed per button clicking. */
 private void process_btnmenu()  throws java.sql.SQLException, ZException, WAException{

 
  String key = request.getParameter(wam.PARAM_ZID);
  Long ZID = null;
  
  try{
	  ZID = Long.valueOf(key);
	  } catch(Exception e){}
  
  String ZTYPE = request.getParameter(wam.PARAM_TARGET);
  if(request.getParameter(wam.PARAM_BTN_CANCEL) != null) {
	  do_list();
  }else if(request.getParameter(wam.PARAM_BTN_DELETE) != null) {
	  do_delete(); 
	  
 } else if(request.getParameter(wam.PARAM_BTN_VIEW) != null) {
  was.sendRedirect(response,wam.SUBSYS_TISC,wam.REQUEST_VIEW,ZTYPE,ZID);
  
 } else if(request.getParameter(wam.PARAM_BTN_EDIT) != null){
   was.sendRedirect(response,wam.SUBSYS_TISC,wam.REQUEST_EDIT,ZTYPE,ZID);
   
    //{do_edit(); }
 } else throw(new WAException(wam,wam.ERROR_NOT_IMPLEMENTED));
 } // process_btnmenu()

 /** list target objects. */
 private void do_list() throws java.sql.SQLException, ZException{
  
 
  ZFieldSet fs = was.extractFieldSet(request), etalon_fs;
  int rows_per_page=was.getRowsPerPage(fs);
  long row_offset=was.getRowOffset(fs);
  // correct data and display it again
  ZObject ZO=new ZObject();
  etalon_fs = ZO.toFieldSet();
  //etalon_fs.add(new ZField(wam.PARAM_ROWS_PER_PAGE,Integer.toString(rows_per_page)));
  fs.importMissedData(etalon_fs);
  try { ZO = new ZObject(fs); }
   catch(Exception e){ printerrmsg(e);}
  if(request.getParameter(wam.PARAM_BTN_SHOW) != null) row_offset = 0;
  if(request.getParameter(wam.PARAM_BTN_NEXT) != null)
	row_offset = row_offset+rows_per_page;
  fs.setValue(wam.PARAM_ROW_OFFSET, new Long(row_offset));
  if(request.getParameter(wam.PARAM_BTN_CANCEL) != null) fs = etalon_fs;
  fs.replaceNullValue(ZObject.F_ZSTA,new Integer(WAMessages.ITEM_STATUS_ACTIVE));
  print_form_list(fs);
  etalon_fs = fs;
  // cancel request
  if( !( (request.getParameter(wam.PARAM_BTN_SHOW) != null) ||
	 (request.getParameter(wam.PARAM_BTN_NEXT) != null) ||
	 (request.getParameter(wam.PARAM_BTN_REFRESH) != null) )) return;
  RList rl = null;
  String szStatus = fs.getValue(ZO.F_ZSTA); int status = WAMessages.ITEM_STATUS_ACTIVE;
  
  try {
	  status = Integer.parseInt(szStatus);
	  } catch(NumberFormatException ne){}
  
  if(WAMessages.ITEM_STATUS_INTERMEDIATE == status)
   rl = getZSystem().listDObjectsIntermediate(ZO.ZSID,ZO.ZTYPE,rows_per_page,row_offset);
  else if (WAMessages.ITEM_STATUS_LOCKED == status)
   rl = getZSystem().listDObjectsLocked(ZO.ZSID,ZO.ZTYPE,rows_per_page,row_offset);
  else if (WAMessages.ITEM_STATUS_ACTIVE == status)
   rl = getZSystem().listDObjects(ZO.ZSID,ZO.ZTYPE,rows_per_page,row_offset);
  else return;
  // display list of objects
   int did=was.beginDocument(); 
   try{
   was.beginTable();
   print_TabHeader_ZObject();
   for(int i=0;i<rl.size();i++){
     print_TabRow_ZObject(((DObject)rl.get(i)).getVersion());
     }
   was.closeTable();
   //was.println(wam.getMsgText(wam.TOTAL_ROWS,Integer.toString(rl.size())));
   was.printTotalRowsPager(request,etalon_fs,rl.size());
   }finally{was.closeDocument(did);
   }
 } // do_list()

 private final int ZO_MODE_FULL = 0xFFFFFFFF;
 private final int ZO_MODE_ALL_EXTENDED = 0xFFFF0000;
 private final int ZO_MODE_VERSION = ZO_MODE_FULL - (ZO_MODE_ZOID + ZO_MODE_ZTYPE);
 private final int ZO_MODE_OBJECT = ~(ZO_MODE_ZDATO + ZO_MODE_ZDATE);
 //
 private final static int ZO_MODE_ZOID = 1;
 private final static int ZO_MODE_ZVER = 2;
 private final static int ZO_MODE_ZTYPE = 4;
 private final static int ZO_MODE_ZSID = 8;
 private final static int ZO_MODE_ZLVL = 16;
 private final static int ZO_MODE_ZUID = 32;
 private final static int ZO_MODE_ZSTA = 64;
 private final static int ZO_MODE_ZDATE = 128;
 private final static int ZO_MODE_ZDATO = 256;
 // extended flags
 private final static int ZO_MODE_FORCE = 1<<15;
 private final static int ZO_MODE_STATUS = 1<<16;
 private final static int ZO_MODE_ROWSPAGER = 2<<17;

 private boolean has_flag(int mode, int flag)
  {if((mode & flag) != 0) return(true); return(false); }


 private void print_TabRow_ZObject(ZObject r)
 {print_TabRow_ZObject(r,ZO_MODE_FULL);}

 private void print_TabRow_ZObject(ZObject r, int mode)
 {
  ZFieldSet fs = r.toFieldSet();
    was.beginTableRow();
     if(has_flag(mode,ZO_MODE_ZOID)) was.printCellZOID(SUBSYS_THIS,null,r.ZOID);
     //was.printCellField(fs.get(ZObject.F_ZOID));
     was.printCellField(fs.get(ZObject.F_ZVER));
     if(has_flag(mode,ZO_MODE_ZTYPE)) was.printCellField(fs.get(ZObject.F_ZTYPE));
     if(has_flag(mode,ZO_MODE_ZSID)) was.printCellZSID(r.ZSID);
     was.printCellField(fs.get(ZObject.F_ZLVL));
     if(has_flag(mode,ZO_MODE_ZUID)) was.printCellZUID(r.ZUID);
     was.printCellField(fs.get(ZObject.F_ZSTA));
     was.printCellField(fs.get(ZObject.F_ZDATE));
     was.printCellField(fs.get(ZObject.F_ZDATO));
    was.closeTableRow();
 } // print_TabRow_ZObject

 private void print_TabHeader_ZObject(){
 print_TabHeader_ZObject(ZO_MODE_FULL);
 }

 private void print_TabHeader_ZObject(int mode){
 
    was.beginTableRow();
     ZFieldSet fs= (new ZObject()).toFieldSet();
    if(has_flag(mode,ZO_MODE_ZOID)) was.printHCellCaption(fs.get(ZObject.F_ZOID));
     was.printHCellCaption(fs.get(ZObject.F_ZVER));
    if(has_flag(mode,ZO_MODE_ZTYPE)) was.printHCellCaption(fs.get(ZObject.F_ZTYPE));
     was.printHCellCaption(fs.get(ZObject.F_ZSID));
     was.printHCellCaption(fs.get(ZObject.F_ZLVL));
     was.printHCellCaption(fs.get(ZObject.F_ZUID));
     was.printHCellCaption(fs.get(ZObject.F_ZSTA));
     was.printHCellCaption(fs.get(ZObject.F_ZDATE));
     was.printHCellCaption(fs.get(ZObject.F_ZDATO));
    was.closeTableRow();
 } //print_TabHeader_ZObject

private final static int[] ARRAYDIC_OBJECT_STATUS = {
 WAMessages.ITEM_STATUS_ACTIVE,
 WAMessages.ITEM_STATUS_INTERMEDIATE,
 WAMessages.ITEM_STATUS_LOCKED 
 };

 private void print_form_list(ZFieldSet fs) {

  ZObject r = new ZObject(); // short alias for ZObject class
  RList rl_scopes=null; // list of SAMScopes
  ZDictionary dicZTYPE = getZSystem().getDictionary(wam.DIC_TIS_OBJECT_TYPE);
  try{ rl_scopes = getZSystem().listSAMScopes(); }
    catch(Exception e) { printerrmsg(e); rl_scopes = null; }
  int did=was.beginDocument();
  try{
   // header
   was.beginFormGet(SUBSYS_THIS,wam.REQUEST_LIST,null);
   was.beginRecord();
   was.beginTable();
    was.printTabFieldInputList(fs.get(r.F_ZSID),rl_scopes,wam.ITEM_ANY_SCOPE);
    was.printTabFieldInputDic(fs.get(r.F_ZTYPE),dicZTYPE,wam.ITEM_ANY_OBJECT_TYPE);
    was.printTabFieldInputArrayDic(wam.getMsgText(wam.CAPTION_OBJECT_STATUS),
	r.F_ZSTA,fs.getValue(r.F_ZSTA),ARRAYDIC_OBJECT_STATUS);
    was.printTabFieldRowsPager(fs);
   was.closeTable();
   was.printButtonMenuBarList();
   was.closeRecord();
   was.closeForm();
  }finally{was.closeDocument(did);}
 } // print_form_list()

 private void print_form_ZO_Exec(ZFieldSet fs, int mnu_id)
 {
  RList rlScopes=null,rlUsers=null; 
  ZObject r = new ZObject(); // short alias for ZObject class
  ZDictionary dicZTYPE = getZSystem().getDictionary(wam.DIC_TIS_OBJECT_TYPE);
  ZDictionary dicSLEVEL = getZSystem().getDictionary(wam.DIC_SLEVEL);
  ZDictionary dicZSTA = getZSystem().getDictionary(wam.DIC_ZSTA);
  ZDictionary dicYESNO = getZSystem().getDictionary(wam.DIC_YESNO);
  String szZID = request.getParameter(wam.PARAM_ZID);
  Long ZID = null; try{ ZID = new Long(szZID); } catch (NumberFormatException ne){}
  String szZVER = request.getParameter(wam.PARAM_ZVER);
  String szZTYPE = request.getParameter(wam.PARAM_TARGET);
  String btn_caption = wam.getMnuCaption(mnu_id);
  String mnu_desc = wam.getMnuDesc(mnu_id);
  String req = wam.getMnuRequest(mnu_id);
  try{
	  rlScopes=getZSystem().listSAMScopes();
	  }catch(Exception e){
		  printerrmsg(e);
		  }
  try{
	  rlUsers=getZSystem().listSAMUsers();
	  }catch(Exception e){
		  printerrmsg(e);
		  }
  int mode=ZO_MODE_FULL;
  if(mnu_id == wam.MNU_CREATE) mode = ZO_MODE_ZTYPE+ZO_MODE_ZSID+ZO_MODE_ZLVL;
  else if(mnu_id == wam.MNU_OPEN || mnu_id == wam.MNU_LOCK ||
	  mnu_id == wam.MNU_DELETE || mnu_id == wam.MNU_COMMIT )
	  mode = ZO_MODE_ZTYPE+ZO_MODE_ZOID+ZO_MODE_ZVER;
  else if(mnu_id == wam.MNU_ROLLBACK || mnu_id == wam.MNU_UNLOCK)
	  mode = ZO_MODE_ZTYPE+ZO_MODE_ZOID+ZO_MODE_ZVER+ZO_MODE_FORCE;
  else if(mnu_id == wam.MNU_MOVE)
	  mode = ZO_MODE_ZTYPE+ZO_MODE_ZOID+ZO_MODE_ZVER+ZO_MODE_ZSID;
  else if(mnu_id == wam.MNU_SETSLEVEL)
	  mode = ZO_MODE_ZTYPE+ZO_MODE_ZOID+ZO_MODE_ZVER+ZO_MODE_ZLVL;
  else if(mnu_id == wam.MNU_SETVDATE)
	  mode = ZO_MODE_ZDATO + ZO_MODE_FORCE;
  int did=was.beginDocument();
  try{
   // header
   was.printTitle(mnu_desc);
   was.beginFormGet(SUBSYS_THIS,req,szZTYPE,ZID);
   was.beginRecord();
   was.beginTable();
    if(has_flag(mode,ZO_MODE_ZTYPE)) was.printTabFieldInputDic(fs.get(r.F_ZTYPE),dicZTYPE);
    if(has_flag(mode,ZO_MODE_ZOID)) was.printTabFieldInput(fs.get(r.F_ZOID));
    if(has_flag(mode,ZO_MODE_ZVER)) was.printTabFieldInput(fs.get(r.F_ZVER));
    if(has_flag(mode,ZO_MODE_ZSID)) was.printTabFieldInputList(fs.get(r.F_ZSID),rlScopes);
    if(has_flag(mode,ZO_MODE_ZLVL)) was.printTabFieldInputDic(fs.get(r.F_ZLVL),dicSLEVEL);
    if(has_flag(mode,ZO_MODE_ZUID)) was.printTabFieldInputList(fs.get(r.F_ZUID),rlUsers);
    if(has_flag(mode,ZO_MODE_ZSTA)) was.printTabFieldInputDic(fs.get(r.F_ZSTA),dicZSTA);
    if(has_flag(mode,ZO_MODE_ZDATE)) was.printTabFieldInput(fs.get(r.F_ZDATE));
    if(has_flag(mode,ZO_MODE_ZDATO)) was.printTabFieldInput(fs.get(r.F_ZDATO));
    if(has_flag(mode,ZO_MODE_FORCE)) was.printTabFieldInputDic(fs.get(wam.PARAM_FORCE),dicYESNO);
   was.closeTable();
   was.printButtonMenuBarExec(btn_caption);
   was.closeRecord();
   was.closeForm();
  }finally{was.closeDocument(did);}
 } //print_form_ZO

 /** find target objects. */
 private void do_find() throws WAException {
	 
// throw(new WAException(wam,wam.ERROR_NOT_IMPLEMENTED)); 
	 throw(new WAException("test")); 
 } // do_find()

 /** view target object. */
 private void do_view() throws java.sql.SQLException, ZException, WAException{
 
 
  String key = request.getParameter(wam.PARAM_ZID);
  Long ZOID = wam.string2Long(key);
  DObject o = new DObject(ZOID);
  ZFieldSet fs;
  getZSystem().loadDObjectMetadataFull(o);
  if(!o.isExists()) throw(new WAException(wam,wam.ERROR_NOT_FOUND));
  RList rl = o.getHVersions();
  RList rl_scopes=null; // list of SAMScopes
  RList rl_users=null; // list of SAMGroups
  try{ rl_scopes = getZSystem().listSAMScopes(); } catch(Exception e) { printerrmsg(e);}
  try{ rl_users = getZSystem().listSAMUsers(); } catch(Exception e) { printerrmsg(e);}
  ZDictionary dicSLEVEL = getZSystem().getDictionary(wam.DIC_SLEVEL);
  int did=was.beginDocument(); try{
   // header
   fs=o.getVersion().toFieldSet();
   was.printMenuBarViewDObjectTIS(o.getZTYPE(), o.getZOID());
   was.beginTable();
   was.printTabField(ZName.F_ZNAME,o.getZNAME());
   //was.printTabField(fs,ZObject.F_ZOID);
   was.printTabFieldZOID(fs,ZObject.F_ZOID,SUBSYS_THIS);
   was.printTabFieldDic(fs,ZObject.F_ZTYPE,getDic(wam.DIC_TIS_OBJECT_TYPE));
   was.closeTable();
   // menu
   String target = o.getZTYPE();
   Long id = o.getZOID();
   Long ver = o.getZVER();
   //was.printHMenuAction(wam.MNU_CREATE,SUBSYS_THIS,o.getZTYPE(),o.getZOID(),o.getZVER());
   //was.printHMenuAction(wam.MNU_EDIT,SUBSYS_THIS,target,id,ver);
   //was.printHMenuAction(wam.MNU_VIEW,SUBSYS_THIS,target,id,ver);
   was.beginHMenu();
   was.printHMenuAction(wam.MNU_OPEN,SUBSYS_THIS,target,id,ver);
   was.printHMenuAction(wam.MNU_LOCK,SUBSYS_THIS,target,id,ver);
   was.printHMenuAction(wam.MNU_UNLOCK,SUBSYS_THIS,target,id,ver);
   was.printHMenuSeparator();
   //was.closeHMenu(); was.beginHMenu();
   was.printHMenuAction(wam.MNU_COMMIT,SUBSYS_THIS,target,id,ver);
   was.printHMenuAction(wam.MNU_ROLLBACK,SUBSYS_THIS,target,id,ver);
   //was.printHMenuAction(wam.MNU_CMP,SUBSYS_THIS,target,id,ver);
   was.printHMenuSeparator();
   was.printHMenuAction(wam.MNU_DELETE,SUBSYS_THIS,target,id,ver);
   was.printHMenuAction(wam.MNU_MOVE,SUBSYS_THIS,target,id,ver);
   was.printHMenuAction(wam.MNU_SETSLEVEL,SUBSYS_THIS,target,id,ver);
   was.closeHMenu();
   // current version
   was.beginTable();
   was.printTabFieldSeparator(wam.getMsgText(wam.CAPTION_CURRENT_VERSION));
   was.printTabField(fs,ZObject.F_ZVER);
   was.printTabFieldDic(fs,ZObject.F_ZLVL,getDic(wam.DIC_SLEVEL));
   was.printTabFieldDic(fs,ZObject.F_ZSTA,getDic(wam.DIC_ZSTA));
   was.printTabFieldList(fs,ZObject.F_ZUID,rl_users,wam.SUBSYS_SAM, wam.TARGET_SAMUSER);
   was.printTabFieldList(fs,ZObject.F_ZSID,rl_scopes,wam.SUBSYS_SAM, wam.TARGET_SAMSCOPE);
   was.printTabField(fs,ZObject.F_ZDATE);
   was.printTabField(fs,ZObject.F_ZDATO);
   was.closeTable();
   // list of versions
   was.beginTable();
   print_TabHeader_ZObject(ZO_MODE_VERSION);
   if(o.getLVersion() !=null) print_TabRow_ZObject(o.getLVersion(),ZO_MODE_VERSION);
   else if(o.getIVersion() !=null) print_TabRow_ZObject(o.getIVersion(),ZO_MODE_VERSION);
   for(int i=0;i<rl.size();i++)
   {
    print_TabRow_ZObject((ZObject)rl.get(i),ZO_MODE_VERSION);
   }
   was.closeTable();
   was.println(wam.getMsgText(wam.TOTAL_ROWS,Integer.toString(rl.size())));
  }finally{was.closeDocument(did);}
 } // do_view()

 private void do_mnu_request(int mnu_id) throws WAException, ZException
 {
  ZFieldSet fs = was.extractFieldSet(request), etalon_fs;
  String szZID = request.getParameter(wam.PARAM_ZID);
  Long ZID = null; try{ ZID = new Long(szZID); } catch (NumberFormatException ne){}
  String szZVER = request.getParameter(wam.PARAM_ZVER);
  String szZTYPE = request.getParameter(wam.PARAM_TARGET);
  YesNo FORCE = null;
  // correct data and display it again
  ZObject ZO=new ZObject();
  try{
   if(mnu_id == wam.MNU_SETSLEVEL) {}
   if(mnu_id == wam.MNU_SETVDATE) {ZO.ZDATO = getZSystem().getVDate();}
  }
  catch(Exception e){ printerrmsg(e);}
  etalon_fs = ZO.toFieldSet();
  etalon_fs.add(new ZField(wam.PARAM_FORCE,wam.SZ_YESNO_N));
  fs.importMissedData(etalon_fs);
  //
  if(request.getParameter(wam.PARAM_BTN_CANCEL) != null)
  {
   if(mnu_id == wam.MNU_CREATE ){
    was.sendRedirect(response,SUBSYS_THIS,wam.REQUEST_LIST,szZTYPE,ZID);
    return; }
   if(ZID!=null) {
    was.sendRedirect(response,SUBSYS_THIS,wam.REQUEST_VIEW,szZTYPE,ZID);
    return;}
   fs = etalon_fs;
  }
  try {
   ZO.getFromFS(fs); FORCE=Record.getFSYesNo(fs,wam.PARAM_FORCE);
   if(szZID!=null) fs.replaceNullValue(ZO.F_ZOID,szZID);
   if(szZVER!=null) fs.replaceNullValue(ZO.F_ZVER,szZVER);
   if(szZTYPE!=null) fs.replaceNullValue(ZO.F_ZTYPE,szZTYPE);
  } catch(Exception e){ printerrmsg(e); return;}
  finally{print_form_ZO_Exec(fs,mnu_id);}
  // cancel request
  if( ! (request.getParameter(wam.PARAM_BTN_EXEC) != null) ) return;
  // execute request
  int did=was.beginDocument(); try{
  ZExecStatus es = null;
  if(mnu_id == wam.MNU_OPEN)
   es = getZSystem().open_object(ZO.ZTYPE,ZO.ZOID,ZO.ZVER);
  else if(mnu_id == wam.MNU_CREATE)
   es = getZSystem().create_object(ZO.ZTYPE,ZO.ZSID,ZO.ZLVL);
  else if(mnu_id == wam.MNU_LOCK)
   es = getZSystem().lock_object(ZO.ZTYPE,ZO.ZOID,ZO.ZVER);
  else if(mnu_id == wam.MNU_COMMIT)
   es = getZSystem().commit_object(ZO.ZTYPE,ZO.ZOID,ZO.ZVER);
  else if(mnu_id == wam.MNU_DELETE)
   es = getZSystem().delete_object(ZO.ZTYPE,ZO.ZOID,ZO.ZVER);
  else if(mnu_id == wam.MNU_UNLOCK)
   es = getZSystem().unlock_object(ZO.ZTYPE,ZO.ZOID,ZO.ZVER,FORCE);
  else if(mnu_id == wam.MNU_ROLLBACK)
   es = getZSystem().rollback_object(ZO.ZTYPE,ZO.ZOID,ZO.ZVER,FORCE);
  else if(mnu_id == wam.MNU_MOVE)
   es = getZSystem().move_object(ZO.ZTYPE,ZO.ZOID,ZO.ZVER,ZO.ZSID);
  else if(mnu_id == wam.MNU_SETSLEVEL)
   es = getZSystem().set_slevel(ZO.ZTYPE,ZO.ZOID,ZO.ZVER,ZO.ZLVL);
  else if(mnu_id == wam.MNU_SETVDATE)
   es = getZSystem().set_vdate(ZO.ZDATO,FORCE);
  else throw(new WAException(wam,wam.ERROR_NOT_IMPLEMENTED));
  //
  getZSystem().checkExecStatus(es); // throws ZException
  was.printExecStatus(es);
  if(mnu_id == wam.MNU_CREATE) ZO.ZOID = es.ZID;
  ZO.ZVER = es.ZVER;
  } catch(java.sql.SQLException sqle){throw new WAException(sqle.getMessage()); }
  finally{was.closeDocument(did);}
 } // do_mnu_request

 /** create target object. */
 private void do_create() throws WAException,ZException
 { do_mnu_request(wam.MNU_CREATE); } // do_create()

 /** open target object. */
 private void do_open() throws WAException,ZException
 { do_mnu_request(wam.MNU_OPEN); } // do_open()

 /** commit target object. */
 private void do_commit() throws WAException,ZException
 { do_mnu_request(wam.MNU_COMMIT); } // do_commit()

 /** rollback target object. */
 private void do_rollback() throws WAException,ZException
 { do_mnu_request(wam.MNU_ROLLBACK); } // do_rollback()

 /** lock target object. */
 private void do_lock() throws WAException,ZException
 { do_mnu_request(wam.MNU_LOCK); } // do_lock()

 /** unlock target object. */
 private void do_unlock() throws WAException,ZException
 { do_mnu_request(wam.MNU_UNLOCK); } // do_unlock()

 /** delete target object. */
 private void do_delete() throws WAException,ZException
 { do_mnu_request(wam.MNU_DELETE); } // do_delete()

 /** move target object. */
 private void do_move() throws WAException,ZException
 { do_mnu_request(wam.MNU_MOVE); } // do_move()

 /** set slevel of target object. */
 private void do_setslevel() throws WAException,ZException
 { do_mnu_request(wam.MNU_SETSLEVEL); } // do_slevel()

 /** set vdate of target object. */
 private void do_setvdate() throws WAException,ZException
 { do_mnu_request(wam.MNU_SETVDATE);} // do_setvdate()

 // constructors
 public WARHTISObject(
	javax.servlet.http.HttpServletRequest request,
	javax.servlet.http.HttpServletResponse response,
	javax.servlet.jsp.JspWriter out)
 { super(request,response,out); }
 public WARHTISObject(WARequestHandler warh) { super(warh); }

} //WARHTISObject
