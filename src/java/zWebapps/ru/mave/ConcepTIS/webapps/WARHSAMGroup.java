// ConcepTIS project
// (c) Alex V Eustrop 2009-2010
// see LICENSE at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.webapps;

import java.util.*;
import java.sql.*;
import ru.mave.ConcepTIS.dao.*;
import ru.mave.ConcepTIS.dao.SAM.*;

public class WARHSAMGroup
	extends WARequestHandler
{
 public static final String SUBSYS_THIS = WAMessages.SUBSYS_SAM;
 public static final String TARGET_THIS = WAMessages.TARGET_SAMGROUP;

 public void processREST() { was.printRESTNotImplemented(); }

 // interfaces/abstracts implementation
 public void process()
 {
  String req = request.getParameter(wam.PARAM_REQUEST);
  was.beginHMenu();
  was.printHMenuSeparator();
  //was.printHMenuCaption(wam.getMnuDesc(wam.MNU_SAM_MAIN)+":");
  was.printHMenuItem(wam.MNU_SAM_GROUP_LIST);
  was.printHMenuItem(wam.MNU_SAM_GROUP_CREATE);
  was.printHMenuItem(wam.MNU_SAM_GROUP_FIND);
  was.closeHMenu();
  was.printSeparator();
  try{ db_logon(); } catch(Exception e){printerrmsg(e); return; }
  try{
   if(wam.REQUEST_VIEW.equals(req))  do_view(); 
   else if(wam.REQUEST_CREATE.equals(req)) do_create();
   else if(wam.REQUEST_EDIT.equals(req)) do_edit();
   else if(wam.REQUEST_DELETE.equals(req)) do_delete();
   else if(wam.REQUEST_SAVE.equals(req)) do_save();
   else if(wam.REQUEST_FIND.equals(req)) do_find();
   else if(wam.REQUEST_MNGACL.equals(req)) do_mngacl();
   else if(wam.REQUEST_BTNMENU.equals(req)) process_btnmenu();
   else if(req == null || wam.REQUEST_LIST.equals(req)) do_list();
   else throw(new WAException(wam,wam.ERROR_NOT_IMPLEMENTED));
  }catch(Exception e){printerrmsg(e); return; }
  finally{ db_logoff(); } 
 } // process

 /** process request passed per button clicking. */
 private void process_btnmenu()
  throws java.sql.SQLException, ZException, WAException
 {
  if(request.getParameter(wam.PARAM_BTN_CANCEL) != null) {do_list();}
  else if(request.getParameter(wam.PARAM_BTN_EDIT) != null) {do_edit(); }
  else if(request.getParameter(wam.PARAM_BTN_DELETE) != null) {do_delete(); }
  //else if(request.getParameter(wam.PARAM_BTN_VIEW) != null) {do_view(); }
  else throw(new WAException(wam,wam.ERROR_NOT_IMPLEMENTED));
 } // process_btnmenu()

 /** delete SAMGroups. */
 private void do_delete()
  throws java.sql.SQLException, ZException, WAException
 {
  Long id = new Long(request.getParameter(wam.PARAM_ZID));
  if(request.getParameter(wam.PARAM_BTN_NO) != null) {do_view();return;}
  else if(request.getParameter(wam.PARAM_BTN_YES) != null) {
   getZSystem().deleteSAMGroup(id);
   was.printMsg(wam.INFO_SUCCESS_DELETED);
   return;
  }
  // else
  Group r=null; try { r = getZSystem().loadSAMGroup(id);}
  catch(Exception e) { printerrmsg(e);}
  was.printQuestDelete(wam.OBJ_SAMGROUP,r,SUBSYS_THIS,TARGET_THIS,id);
 } //do_delete()

 private void do_mngacl()
  throws java.sql.SQLException, ZException, WAException
 {
  ACLScope r; // passed object or missed data donor
  boolean is_delete=false;
  // get object data from request's parameters
  ZFieldSet fs = was.extractFieldSet(request);
  is_delete=(request.getParameter(wam.PARAM_BTN_DELETE)!=null);
  try {
   r = new ACLScope(fs);
   if(is_delete){ getZSystem().deleteSAMACLScope(r.gid, r.sid,r.obj_type); }
   else{ getZSystem().setSAMACLScope(r); }
  } catch(Exception e){printerrmsg(e);}
  do_view();
 } //do_mngacl

 /** show list of all SAMGroups. */
 private void do_list()
  throws java.sql.SQLException, ZException
 {
   RList rl = getZSystem().listSAMGroups();
   ZFieldSet fs;
   // WARHPSQL warh = new WARHPSQL(this); warh.print_sql_rs(rs);
   int did=was.beginDocument(); try{
   was.beginTable();
    was.beginTableRow();
     fs= (new Group()).toFieldSet();
     was.printHCellValue(wam.getMsgText(wam.CAPTION_SAMGROUP));
     //was.printHCellCaption(fs.get(Group.F_NAME));
     was.printHCellCaption(fs.get(Group.F_DESCR));
    was.closeTableRow();
   for(int i=0;i<rl.size();i++)
   {
    Group r = (Group)rl.get(i);
    fs = r.toFieldSet();
    was.beginTableRow();
    was.printCellHRefView(r.name,SUBSYS_THIS,TARGET_THIS,r.id);
    was.printCellValue(wam.obj2string(r.descr));
    was.closeTableRow();
   }
   was.closeTable();
   }finally{was.closeDocument(did);}
 } // do_list()

 /** show SAMGroup and all it's properties. */
 private void do_view()
  throws java.sql.SQLException, ZException, WAException
 {
  String key = request.getParameter(wam.PARAM_ZID);
  Long id = Long.valueOf(key);
  Group r = getZSystem().loadSAMGroup(id);
  ACLScope XAS;
  ZFieldSet fs;
  if( r == null ) throw(new WAException(wam,wam.ERROR_NOT_FOUND));
  RList rl;
  RList rl_scopes=null; // list of SAMScopes
  try{ rl_scopes = getZSystem().listSAMScopes(); } catch(Exception e) { printerrmsg(e);}
  int did=was.beginDocument();
  try{
   // header
   was.printButtonMenuBarView(SUBSYS_THIS, wam.REQUEST_BTNMENU, TARGET_THIS, r.id);
   was.beginTable();
    fs = r.toFieldSet();
    was.printTabField(fs,r.F_NAME);
    was.printTabField(fs,r.F_ID);
    was.printTabFieldRefView(fs,r.F_SID,rl_scopes,SUBSYS_THIS,wam.TARGET_SAMSCOPE);
    was.printTabField(fs,r.F_DESCR);
   was.closeTable();
   // ACL
   rl = getZSystem().listSAMACLScope4Group(id);
   ZDictionary dicACLA = getZSystem().getDictionary(wam.DIC_ACLA);
   //was.print(wam.getMsgText(wam.CAPTION_GROUPSCAPABILITIES));
   was.beginTable();
    was.beginTableRow();
     was.printHCellValue(wam.getMsgText(wam.CAPTION_SCOPE));
     was.printHCellValue(wam.getMsgText(wam.CAPTION_OBJECT_TYPE));
     was.printHCellValue(wam.getMsgText(wam.CAPTION_ACL_WCDRLH));
    was.closeTableRow();
   for(int i=0;i<rl.size();i++)
    {
     XAS = (ACLScope)rl.get(i);
     fs = XAS.toFieldSet();
     was.beginTableRow();
     was.printCellFieldRefView(fs.get(ACLScope.F_SID),rl_scopes,SUBSYS_THIS,wam.TARGET_SAMSCOPE);
     was.printCellFieldDic(fs.get(ACLScope.F_OBJ_TYPE), getDic(wam.DIC_TIS_OBJECT_TYPE));
     String szWCDRLH = ""+XAS.writea+XAS.createa+XAS.deletea+XAS.reada+XAS.locka+XAS.historya;
     was.printCellValue(szWCDRLH);
     was.closeTableRow();
    }
   was.closeTable();
   XAS = new ACLScope(); XAS.gid=r.id; fs=XAS.toFieldSet();
   was.beginFormPost(SUBSYS_THIS,wam.REQUEST_MNGACL,TARGET_THIS,id);
    was.beginRecord();
    was.printFieldInputHidden( fs.get(ACLScope.F_GID) );
    was.beginTable();
     was.printTabFieldInputList(
      new ZField(ACLScope.F_SID,new Long(0),null,0,0,
       wam.getMsgText(wam.CAPTION_SCOPE)), rl_scopes);
     was.printTabFieldInputDic( fs.get(ACLScope.F_OBJ_TYPE), getDic(wam.DIC_TIS_OBJECT_TYPE));
     was.printTabFieldInputDic( fs.get(ACLScope.F_WRITEA), getDic(wam.DIC_ACLA));
     was.printTabFieldInputDic( fs.get(ACLScope.F_CREATEA), getDic(wam.DIC_ACLA));
     was.printTabFieldInputDic( fs.get(ACLScope.F_DELETEA), getDic(wam.DIC_ACLA));
     was.printTabFieldInputDic( fs.get(ACLScope.F_READA), getDic(wam.DIC_ACLA));
     was.printTabFieldInputDic( fs.get(ACLScope.F_LOCKA), getDic(wam.DIC_ACLA));
     was.printTabFieldInputDic( fs.get(ACLScope.F_HISTORYA), getDic(wam.DIC_ACLA));
    was.closeTable();
    was.closeRecord();
    was.printButtonSet();
    was.printButtonDelete();
   was.closeForm();
   // Users
   rl = getZSystem().listSAMUsers4Group(id);
   fs = (new User()).toFieldSet();
   was.beginTable();
    was.beginTableRow();
     was.printHCellCaption(fs.get(User.F_LOGIN));
     was.printHCellCaption(fs.get(User.F_FULL_NAME));
    was.closeTableRow();
    for(int i=0;i<rl.size();i++)
    {
     User r2 = (User)rl.get(i);
     fs = r2.toFieldSet();
     was.beginTableRow();
     was.printCellHRefView(r2.login,SUBSYS_THIS,wam.TARGET_SAMUSER,r2.id);
     //was.printCellFieldRefView(fs.get(Group.F_SID),rl_scopes,SUBSYS_THIS,wam.TARGET_SAMSCOPE);
     was.printCellValue(wam.obj2string(r2.full_name));
     was.closeTableRow();
    }
   was.closeTable();
  }finally{was.closeDocument(did);}
 } // do_view()

 private void do_save()
	throws java.sql.SQLException,ZException,WAException
 {
  Group r; // passed object or missed data donor
  // get object data from requests's parameters
  ZFieldSet fs = was.extractFieldSet(request);
  // cancel request
  if(request.getParameter(wam.PARAM_BTN_CANCEL) != null)
   {if(request.getParameter(wam.PARAM_ZID) != null) do_view();
    else do_list(); return; }
  // save object
  if(request.getParameter(wam.PARAM_BTN_SAVE) != null)
  try {
   r = new Group(fs);
   getZSystem().saveSAMGroup(r);
   was.printMsg(wam.INFO_SUCCESS_SAVED);
   was.print(" (");
   was.printHRefView(r.getCaption(),SUBSYS_THIS,TARGET_THIS,r.id);
   was.print(")");
   return;
  } catch(Exception e){printerrmsg(e);}
  // correct data and display it again
  r = new Group(); // missed data donor
  fs.importMissedData(r.toFieldSet());
  int did=was.beginDocument();
  try{ print_form_edit(fs); }finally{was.closeDocument(did);}
 } //do_save()

 private void do_edit()
	throws java.sql.SQLException,ZException,WAException
 {
  String key = request.getParameter(wam.PARAM_ZID);
  Long id = Long.valueOf(key);
  Group r = getZSystem().loadSAMGroup(id);
  if( r == null ) throw(new WAException(wam,wam.ERROR_NOT_FOUND));
  int did=was.beginDocument();
  try{ print_form_edit(r.toFieldSet()); }finally{was.closeDocument(did);}
 } // do_create()

 private void do_create()
 {
  int did=was.beginDocument();
  Group r = new Group();
  try{ print_form_edit(r.toFieldSet()); }
  finally{was.closeDocument(did);}
 } // do_create()

 private void print_form_edit(ZFieldSet fs)
 {
  Group r = new Group(); // short alias for Group class
  RList rl_scopes=null; // list of SAMScopes
  try{ rl_scopes = getZSystem().listSAMScopes(); }
    catch(Exception e) { printerrmsg(e); rl_scopes = null; }
  int did=was.beginDocument();
  try{
   // header
   was.beginFormPost(SUBSYS_THIS,wam.REQUEST_SAVE,TARGET_THIS);
   // put ZID into request form
   ZField f_id = fs.get(r.F_ID);
   if(f_id != null){
     f_id = new ZField(f_id); f_id.setName(wam.PARAM_ZID);
     if(f_id.getValue() != null) was.printFieldInputHidden(f_id);
   }
   was.printButtonMenuBarEdit();
   was.beginRecord();
   was.printFieldInputHidden(fs.get(r.F_ID));
   was.beginTable();
    was.printTabFieldInput(fs.get(r.F_NAME));
    was.printTabFieldInputList(fs.get(r.F_SID),rl_scopes);
    was.printTabFieldInput(fs.get(r.F_DESCR));
   was.closeTable();
   was.printButtonMenuBarEdit();
    was.closeRecord();
   was.closeForm();
  }finally{was.closeDocument(did);}
 } // print_form_edit()


 /** search for target objects. */
 private void do_find()
  throws WAException
 {
 throw(new WAException(wam,wam.ERROR_NOT_IMPLEMENTED));
 } // do_find()

 private void print_form_find()
 {
 } // print_form_find()

 // constructors
 public WARHSAMGroup(
	javax.servlet.http.HttpServletRequest request,
	javax.servlet.http.HttpServletResponse response,
	javax.servlet.jsp.JspWriter out)
 { super(request,response,out); }
 public WARHSAMGroup(WARequestHandler warh) { super(warh); }

} //WARHSAMGroup
