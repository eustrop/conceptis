// ConcepTIS project
// (c) Alex V Eustrop 2009-2010
// see LICENSE at the project's root directory
//
// $Id$
//
// History:
//  2009/11/28 started from psql.jsp
//

package ru.mave.ConcepTIS.webapps;

import ru.mave.ConcepTIS.dao.*;
import java.util.*;
import java.sql.*;

/** top level Web Application abstract class. This is storage of
 * common methods and properties.
 * @see WARHMain
 */
public abstract class WARequestHandler{


//
// Global parameters
//
//private final static String CLASS_VERSION = "$Id$";
private String DBSERVER_URL = "jdbc:postgresql:conceptisdb";

public final static String SZ_EMPTY = "";
/** no more rows per page should be shown (useful for huge lists). */
public final static int MAX_ROWS_PER_PAGE = 10;

//
// JSP emulation parameters
// (must be initialized at constructor)
//

protected javax.servlet.jsp.JspWriter out;
protected javax.servlet.http.HttpServletRequest request;
protected javax.servlet.http.HttpServletResponse response;

// JSP/Servlet body preservation objects (only one can be used!)
protected java.io.BufferedReader bodyReader = null;
protected javax.servlet.ServletInputStream bodyInputStream = null;
java.io.IOException bodyPreservationException = null;

/** the same as the similar javax.servlet.ServletRequest method.
* Howerver returns null if no body preserved with preserveReader(). */
public java.io.BufferedReader getReader(){return(bodyReader);}

/** the same as the similar javax.servlet.ServletRequest method.
* Howerver returns null if no body preserved with preserveInputStream(). */
public javax.servlet.ServletInputStream getInputStream(){return(bodyInputStream);}

public boolean isBodyPreserved(){return(bodyReader != null || bodyInputStream != null);}

/** preserved request's body for future using over getReader() call. */
public void preserveReader()
 {
  if(request!=null && !isBodyPreserved() )
  try{bodyReader=request.getReader();}
  catch(java.io.IOException ioe){ bodyPreservationException = ioe;}
 }
/** preserved request's body for future using over getInputStream() call. */
public void preserveInputStream()
 {
  if(request!=null && !isBodyPreserved())
  try{bodyInputStream=request.getInputStream();}
  catch(java.io.IOException ioe){ bodyPreservationException = ioe;}
 }

protected WASkin was;
protected WAMessages wam;

 //
 // some useful static functions.
 //

 /** @see WAMessages#obj2html */
 public static String obj2html(Object o) { return(WAMessages.obj2html(o)); }

 //
 // DB interaction & result printing methods
 //

 /** set JDBC url. */
 public void setJDBCURL(String url) { DBSERVER_URL=url; }
 public String getJDBCURL() { return(DBSERVER_URL); }

 /** set nutshell CGI url. */
 public void setCGI(String url) { was.setCGI(url); }
 public String getCGI() { return(was.getCGI()); }

  /** create driver class by its classname (jdbc_driver parameter)
   * and register it via DriverManager.registerDriver().
   * @param jdbc_driver - "org.postgresql.Driver" for postgres,
   * "oracle.jdbc.driver.OracleDriver" for oracle, etc.
   */ 
 public static void register_jdbc_driver(String jdbc_driver)throws Exception{
	
  
   java.sql.Driver d;
   Class dc;
   // get "Class" object for driver's class
   try{
   
    dc = Class.forName(jdbc_driver);
    d = (java.sql.Driver)dc.newInstance();
   
  } catch(ClassNotFoundException e) {
     throw new Exception("register_jdbc_driver:" + "unable to get Class for " + jdbc_driver + ":" + e); 
    
   }catch(Exception e) {
     throw new Exception("register_jdbc_driver: unable to get driver " + jdbc_driver + " : " + e); }
     
   // register driver
   try {
	   DriverManager.registerDriver(d); 
	 
    } catch(SQLException e) { throw new Exception("register_jdbc_driver: unable to register driver "+jdbc_driver+" : "+e);}
   
  } // register_jdbc_driver()


 private ZSystem zsys;
 public ZSystem getZSystem(){return(zsys);}
 public void setZSystem(ZSystem zsys){this.zsys=zsys;}

 public ZDictionary getDic(String dic_name)
 {
  if(getZSystem() == null) return(new ZDictionary());
  return(getZSystem().getDictionary(dic_name));
 }

 public void db_logon() throws Exception {

   if(getZSystem() != null) return; // already logged on
   java.sql.Connection dbc;
   // register JDBC driver
   register_jdbc_driver("org.postgresql.Driver");
   // open JDBC connection
   dbc=DriverManager.getConnection(getJDBCURL(),request.getRemoteUser(),"");
   setZSystem(new ZSystem(dbc));
   was.setFieldDic(getDic(WAMessages.DIC_TIS_FIELD));
 } // db_logon()

 public void db_logoff()
 {
  if(zsys == null) return;
  java.sql.Connection dbc = zsys.getDBC();
  if(dbc == null) return;
  try{ dbc.close(); }
  catch(SQLException e){} // ignore
  zsys = null;
 } // db_logoff()


 //
 // Message displaying tools
 //

 /** display informationl message. TISExmlDB.java legacy where have been wrapper to System.out.print */
 public void printmsg(String msg) {was.printMsg(msg);}

 /** dispaly error message. TISExmlDB.java legacy where have been just a wrapper to System.err.print */
 public void printerrmsg(String msg) {was.printErrMsg(msg);}
 public void printerrmsg(String msg, Exception e) {was.printErrMsg(msg,e);}
 public void printerrmsg(Exception e) {was.printErrMsg(e);}


 //
 // http/html hints&tools
 //

 /** some hints for old and buggy browsers like NN4.x */
 public void cache_expire_hints()
   throws java.io.UnsupportedEncodingException
 {
 
  long enter_time = System.currentTimeMillis();
  long expire_time = enter_time + 24*60*60*1000;
  response.setHeader("Cache-Control","No-cache");
  response.setHeader("Pragma","no-cache");
  response.setDateHeader("Expires",expire_time);
  request.setCharacterEncoding("UTF-8");
  String szSQLRequest=SZ_EMPTY;
 }

 // abstract methods
/** process request and produce answer as UI page (html) */
 public abstract void process();
/** process request and produce answer as JSON  */
 public abstract void processREST();

 // constructors

 public WARequestHandler(
	javax.servlet.http.HttpServletRequest request,
	javax.servlet.http.HttpServletResponse response,
	javax.servlet.jsp.JspWriter out)
 {
  this.request = request;
  this.response = response;
  this.out = out;
  wam = new WAMessages(); was = new WASkin(out,wam);
 } // WARequestHandler(HttpServletRequest,JspWriter)

 public WARequestHandler(WARequestHandler warh)
 {
  this.request = warh.request;
  this.response = warh.response;
  this.out = warh.out;
  this.wam = warh.wam;
  this.was = warh.was;
  this.setJDBCURL(warh.getJDBCURL());
 } // WARequestHandler(WARequestHandler warh)
 
} // WARequestHandler.java
