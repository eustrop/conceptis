package ru.mave.ConcepTIS.webapps.json;


import java.util.LinkedHashMap;
import java.util.Map;

public class JSONobj {

    private Map<String, Object> map;


    public JSONobj() {
        map = new LinkedHashMap<>();
    }
    public JSONobj(Map<String, Object> map){
        this.map = map;
    }

    public void put(String key, Object value) throws JSONException {
        testValidity(value);
        map.put(key, value);
    }

    public static void testValidity(Object o) throws JSONException {
        if (o != null) {
            if (o instanceof Double) {
                if (((Double) o).isInfinite() || ((Double) o).isNaN()) {
                    throw new JSONException(
                            "JSON does not allow non-finite numbers.");
                }
            } else if (o instanceof Float) {
                if (((Float) o).isInfinite() || ((Float) o).isNaN()) {
                    throw new JSONException(
                            "JSON does not allow non-finite numbers.");
                }
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        boolean comma = false;
        for (Map.Entry<String, Object> pair : map.entrySet()) {
            if (comma) {
                sb.append(",");
            }
            sb.append(quote(pair.getKey()));
            sb.append(":");
            Object value = pair.getValue();
            if (value instanceof JSONobj) sb.append( value.toString());
            else if (value instanceof Number) sb.append( value.toString());
            else sb.append(quote((String) value));
            comma = true;
        }
        sb.append("}");
        return sb.toString();
    }

    private String quote(String str) {
        if (str == null || str.isEmpty()) {
            return "\"\"";
        }
        char b;
        char c = 0;
        StringBuilder sb = new StringBuilder();
        sb.append("\"");
        for (int i = 0; i < str.length(); i++) {
            b = c;
            c = str.charAt(i);
            switch (c) {
                case '\\':
                case '"':
                    sb.append('\\');
                    sb.append(c);
                    break;
                case '/':
                    if (b == '<') {
                        sb.append('\\');
                    }
                    sb.append(c);
                    break;
                case '\b':
                    sb.append("\\b");
                    break;
                case '\t':
                    sb.append("\\t");
                    break;
                case '\n':
                    sb.append("\\n");
                    break;
                case '\f':
                    sb.append("\\f");
                    break;
                case '\r':
                    sb.append("\\r");
                    break;
                default:sb.append(c);
                    break;
            }
        }
        sb.append("\"");
        return sb.toString();
    }
}
