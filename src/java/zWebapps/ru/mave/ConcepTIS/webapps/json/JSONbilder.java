package ru.mave.ConcepTIS.webapps.json;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;


public class JSONbilder {

    private ResultSet rs;
    private ResultSetMetaData rsmd;

    public JSONbilder(ResultSet rs) {
        this.rs = rs;
        try {
            rsmd = rs.getMetaData();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public JSONobj processResultSet() throws JSONException {
        JSONobj jsonArray = new JSONobj();
        int counter = 0;
        try {
            int columCount = rsmd.getColumnCount();
            ArrayList<String> columName = new ArrayList<>();
            JSONobj jsonObjMetaData = new JSONobj();
            for (int i = 1; i <= columCount; i++) {
                String name = rsmd.getColumnName(i);
                String type = rsmd.getColumnTypeName(i);
                columName.add(name);
                jsonObjMetaData.put(name,type);
            }
            jsonArray.put(String.valueOf(counter),jsonObjMetaData);
            counter++;
            while (rs.next()) {
                JSONobj jsonObj = new JSONobj();
                for (int i = 1; i <= columCount; i++) {
                    jsonObj.put(columName.get(i - 1), rs.getObject(i));
                }
                jsonArray.put(String.valueOf(counter), jsonObj);
                counter++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }
}
