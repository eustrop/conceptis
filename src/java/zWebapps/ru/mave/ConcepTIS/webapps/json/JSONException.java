package ru.mave.ConcepTIS.webapps.json;

public class JSONException extends Exception {

    public JSONException(String message) {
        super(message);
    }
}

