// ConcepTIS project
// (c) Alex V Eustrop 2009-2010
// see LICENSE at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.webapps;

import java.util.*;
import java.sql.*;
import ru.mave.ConcepTIS.dao.*;
import ru.mave.ConcepTIS.dao.SAM.*;

public class WARHSAMUser
	extends WARequestHandler
{
 public static final String SUBSYS_THIS = WAMessages.SUBSYS_SAM;
 public static final String TARGET_THIS = WAMessages.TARGET_SAMUSER;

 // interfaces/abstracts implementation

 public void processREST() { was.printRESTNotImplemented(); }

 public void process()
 {
  String req = request.getParameter(wam.PARAM_REQUEST);
  was.beginHMenu();
  was.printHMenuSeparator();
  //was.printHMenuCaption(wam.getMnuDesc(wam.MNU_SAM_MAIN)+":");
  was.printHMenuItem(wam.MNU_SAM_USER_LIST);
  was.printHMenuItem(wam.MNU_SAM_USER_CREATE);
  was.printHMenuItem(wam.MNU_SAM_USER_FIND);
  was.closeHMenu();
  was.printSeparator();
  try{ db_logon(); } catch(Exception e){printerrmsg(e); return; }
  try{
   if(wam.REQUEST_VIEW.equals(req))  do_view(); 
   else if(wam.REQUEST_CREATE.equals(req)) do_create();
   else if(wam.REQUEST_EDIT.equals(req)) do_edit();
   else if(wam.REQUEST_DELETE.equals(req)) do_delete();
   else if(wam.REQUEST_SAVE.equals(req)) do_save();
   else if(wam.REQUEST_FIND.equals(req)) do_find();
   else if(wam.REQUEST_MNGGROUP.equals(req)) do_mnggroup();
   else if(wam.REQUEST_MNGCAPABILITY.equals(req)) do_mngcapability();
   else if(wam.REQUEST_BTNMENU.equals(req)) process_btnmenu();
   else if(req == null || wam.REQUEST_LIST.equals(req)) do_list();
   else throw(new WAException(wam,wam.ERROR_NOT_IMPLEMENTED));
  }catch(Exception e){printerrmsg(e); return; }
  finally{ db_logoff(); } 
 } // process

 /** process request passed per button clicking. */
 private void process_btnmenu()
  throws java.sql.SQLException, ZException, WAException
 {
  if(request.getParameter(wam.PARAM_BTN_CANCEL) != null) {do_list();}
  else if(request.getParameter(wam.PARAM_BTN_EDIT) != null) {do_edit(); }
  else if(request.getParameter(wam.PARAM_BTN_DELETE) != null) {do_delete(); }
  //else if(request.getParameter(wam.PARAM_BTN_VIEW) != null) {do_view(); }
  else throw(new WAException(wam,wam.ERROR_NOT_IMPLEMENTED));
 } // process_btnmenu()

 /** delete SAMUsers. */
 private void do_delete()
  throws java.sql.SQLException, ZException, WAException
 {
  Long id = new Long(request.getParameter(wam.PARAM_ZID));
  if(request.getParameter(wam.PARAM_BTN_NO) != null) {do_view();return;}
  else if(request.getParameter(wam.PARAM_BTN_YES) != null) {
   getZSystem().deleteSAMUser(id);
   was.printMsg(wam.INFO_SUCCESS_DELETED);
   return;
  }
  // else
  User r=null; try { r = getZSystem().loadSAMUser(id);}
  catch(Exception e) { printerrmsg(e);}
  was.printQuestDelete(wam.OBJ_SAMUSER,r,SUBSYS_THIS,TARGET_THIS,id);
  // throw(new WAException(wam,wam.ERROR_NOT_IMPLEMENTED));
 } //do_delete()

 /** grant/revoke SAMUserGroup. */
 private void do_mnggroup()
  throws java.sql.SQLException, ZException, WAException
 {
  Long uid = new Long(request.getParameter(wam.PARAM_ZID));
  Long gid = new Long(request.getParameter(Group.F_ID));
  try{
  if(request.getParameter(wam.PARAM_BTN_GRANT) != null)
   getZSystem().grantSAMUserGroup(uid,gid);
  else if(request.getParameter(wam.PARAM_BTN_REVOKE) != null) 
   getZSystem().revokeSAMUserGroup(uid,gid);
  } catch(Exception e) { printerrmsg(e);}
  do_view();
 } //do_mnggroup()

 /** grant/revoke SAMUserCapability. */
 private void do_mngcapability()
  throws java.sql.SQLException, ZException, WAException
 {
  Long uid = new Long(request.getParameter(wam.PARAM_ZID));
  Long sid = new Long(request.getParameter(UserCapability.F_SID));
  String capcode = request.getParameter(UserCapability.F_CAPCODE);
  try{
  if(request.getParameter(wam.PARAM_BTN_GRANT) != null)
   getZSystem().grantSAMUserCapability(uid,sid,capcode);
  else if(request.getParameter(wam.PARAM_BTN_REVOKE) != null) 
   getZSystem().revokeSAMUserCapability(uid,sid,capcode);
  } catch(Exception e) { printerrmsg(e);}
  do_view();
  //throw(new WAException(wam,wam.ERROR_NOT_IMPLEMENTED));
 } //do_mngcapability()

 /** show list of all SAMUsers. */
 private void do_list()
  throws java.sql.SQLException, ZException
 {
   RList rl = getZSystem().listSAMUsers();
   ZFieldSet fs;
   // WARHPSQL warh = new WARHPSQL(this); warh.print_sql_rs(rs);
   int did=was.beginDocument(); try{
   was.beginTable();
    was.beginTableRow();
     fs= (new User()).toFieldSet();
     was.printHCellCaption(fs.get(User.F_LOGIN));
     //was.printHCellValue(wam.getMsgText(wam.CAPTION_SCOPE));
     was.printHCellCaption(fs.get(User.F_LOCKED));
     //was.printHCellValue(wam.getMsgText(wam.CAPTION_SLEVEL));
     //was.printHCellValue(wam.getMsgText(wam.CAPTION_LANG));
     //was.printHCellValue(wam.getMsgText(wam.CAPTION_DB_USER));
     was.printHCellCaption(fs.get(User.F_FULL_NAME));
    was.closeTableRow();
   for(int i=0;i<rl.size();i++)
   {
    User r = (User)rl.get(i);
    fs = r.toFieldSet();
    was.beginTableRow();
    was.printCellHRef(r.login,SUBSYS_THIS,wam.REQUEST_VIEW,
	new String[]{wam.PARAM_ZID,wam.PARAM_TARGET},
	new String[]{wam.obj2string(r.id),TARGET_THIS});
    //was.printCellValue(wam.obj2string(r.sid));
    was.printCellValue(wam.obj2string(r.locked));
    //was.printCellValue(wam.obj2string(r.slevel));
    //was.printCellValue(wam.obj2string(r.lang));
    //was.printCellValue(wam.obj2string(r.db_user));
    was.printCellValue(wam.obj2string(r.full_name));
    was.closeTableRow();
   }
   was.closeTable();
   }finally{was.closeDocument(did);}
 } // do_list()

 /** show SAMUser and all it's properties. */
 private void do_view()
  throws java.sql.SQLException, ZException, WAException
 {
  String key = request.getParameter(wam.PARAM_ZID);
  Long id = Long.valueOf(key);
  User r = getZSystem().loadSAMUser(id);
  ZFieldSet fs;
  if( r == null ) throw(new WAException(wam,wam.ERROR_NOT_FOUND));
  RList rl;
  RList rl_scopes=null; // list of SAMScopes
  RList rl_groups=null; // list of SAMGroups
  try{ rl_scopes = getZSystem().listSAMScopes(); } catch(Exception e) { printerrmsg(e);}
  try{ rl_groups = getZSystem().listSAMGroups(); } catch(Exception e) { printerrmsg(e);}
  ZDictionary dicCap = getZSystem().getDictionary(wam.DIC_CAPABILITY);
  int did=was.beginDocument();
  try{
   // header
   was.printButtonMenuBarView(SUBSYS_THIS, wam.REQUEST_BTNMENU, TARGET_THIS, r.id);
   was.beginTable();
    fs = r.toFieldSet();
    /*
    was.printTabFieldHRef(wam.getMsgText(wam.CAPTION_LOGIN),
	r.login,SUBSYS_THIS,wam.REQUEST_VIEW,
	new String[]{wam.PARAM_ZID,wam.PARAM_TARGET},
	new String[]{wam.obj2string(r.id),TARGET_THIS});
    */
    was.printTabField(fs,r.F_LOGIN);
    was.printTabField(fs,r.F_ID);
    was.printTabFieldRefView(fs,r.F_SID,rl_scopes,SUBSYS_THIS,wam.TARGET_SAMSCOPE);
    was.printTabFieldDic(fs,r.F_LOCKED,getDic(wam.DIC_YESNO));
    //was.printTabField(fs,r.F_SLEVEL);
    was.printTabFieldDic(fs,r.F_SLEVEL,getDic(wam.DIC_SLEVEL));
    was.printTabFieldDic(fs,r.F_LANG,getDic(wam.DIC_LANG));
    was.printTabField(fs,r.F_DB_USER);
    was.printTabField(fs,r.F_FULL_NAME);
   was.closeTable();
   // Groups
   rl = getZSystem().listSAMGroups4User(id);
   fs = (new Group()).toFieldSet();
   was.beginTable();
    was.beginTableRow();
     was.printHCellValue(wam.getMsgText(wam.CAPTION_SAMGROUP));
     //was.printHCellCaption(fs.get(Group.F_SID));
     // wam.getMsgText(wam.CAPTION_SCOPE));
     was.printHCellValue(wam.getMsgText(wam.CAPTION_DESCR));
    was.closeTableRow();
    for(int i=0;i<rl.size();i++)
    {
     Group r2 = (Group)rl.get(i);
     fs = r2.toFieldSet();
     was.beginTableRow();
     was.printCellHRefView(r2.name,SUBSYS_THIS,wam.TARGET_SAMGROUP,r2.id);
     //was.printCellFieldRefView(fs.get(Group.F_SID),rl_scopes,SUBSYS_THIS,wam.TARGET_SAMSCOPE);
     was.printCellValue(wam.obj2string(r2.descr));
     was.closeTableRow();
    }
   was.closeTable();
   was.beginFormGet(SUBSYS_THIS,wam.REQUEST_MNGGROUP,TARGET_THIS,id);
    was.beginTable();
     was.printTabFieldInputList(
      new ZField(Group.F_ID,null,null,0,0,
       wam.getMsgText(wam.CAPTION_SAMGROUP)), rl_groups);
    was.closeTable();
    was.printButtonGrant();
    was.printButtonRevoke();
   was.closeForm();
   // Capabilities
   rl = getZSystem().listSAMUserCapabilities(id);
   //was.print(wam.getMsgText(wam.CAPTION_USERSCAPABILITIES));
   was.beginTable();
    was.beginTableRow();
     was.printHCellValue(wam.getMsgText(wam.CAPTION_CAPABILITY));
     was.printHCellValue(wam.getMsgText(wam.CAPTION_SCOPE));
     was.printHCellValue(wam.getMsgText(wam.CAPTION_DESCR));
    was.closeTableRow();
   for(int i=0;i<rl.size();i++)
    {
     UserCapability r2 = (UserCapability)rl.get(i);
     fs = r2.toFieldSet();
     was.beginTableRow();
     was.printCellValue(wam.obj2string(r2.capcode));
     if((new Long(0)).equals(r2.sid) ) was.printCellValue(SZ_EMPTY);
     //  else was.printCellValue(wam.obj2string(r2.sid));
     else was.printCellFieldRefView(fs.get(UserCapability.F_SID),rl_scopes,SUBSYS_THIS,wam.TARGET_SAMSCOPE);
     was.printCellValue(dicCap.getValue(r2.capcode));
     was.closeTableRow();
    }
   was.closeTable();
   was.beginFormGet(SUBSYS_THIS,wam.REQUEST_MNGCAPABILITY,TARGET_THIS,id);
    was.beginTable();
     was.printTabFieldInputDic(
      new ZField(UserCapability.F_CAPCODE,null,null,0,0,
       wam.getMsgText(wam.CAPTION_CAPABILITY)), dicCap);
     was.printTabFieldInputList(
      new ZField(UserCapability.F_SID,new Long(0),null,0,0,
       wam.getMsgText(wam.CAPTION_SCOPE)), rl_scopes);
    was.closeTable();
    was.printButtonGrant();
    was.printButtonRevoke();
   was.closeForm();
  }finally{was.closeDocument(did);}
 } // do_view()

 private void do_save()
	throws java.sql.SQLException,ZException,WAException
 {
  User r; // passed object or missed data donor
  // get object data from requests's parameters
  ZFieldSet fs = was.extractFieldSet(request);
  // cancel request
  if(request.getParameter(wam.PARAM_BTN_CANCEL) != null)
   {if(request.getParameter(wam.PARAM_ZID) != null) do_view();
    else do_list(); return; }
  // save object
  if(request.getParameter(wam.PARAM_BTN_SAVE) != null)
  try {
   r = new User(fs);
   getZSystem().saveSAMUser(r);
   was.printMsg(wam.INFO_SUCCESS_SAVED);
   was.print(" (");
   was.printHRefView(r.getCaption(),SUBSYS_THIS,TARGET_THIS,r.id);
   was.print(")");
   return;
  } catch(Exception e){printerrmsg(e);}
  // correct data and display it again
  r = new User(); // missed data donor
  fs.importMissedData(r.toFieldSet());
  int did=was.beginDocument();
  try{ print_form_edit(fs); }finally{was.closeDocument(did);}
 } //do_save()

 private void do_edit()
	throws java.sql.SQLException,ZException,WAException
 {
  String key = request.getParameter(wam.PARAM_ZID);
  Long id = Long.valueOf(key);
  User r = getZSystem().loadSAMUser(id);
  if( r == null ) throw(new WAException(wam,wam.ERROR_NOT_FOUND));
  int did=was.beginDocument();
  try{ print_form_edit(r.toFieldSet()); }finally{was.closeDocument(did);}
 } // do_edit()

 private void do_create()
 {
  int did=was.beginDocument();
  User r = new User();
  try{ print_form_edit(r.toFieldSet()); }
  finally{was.closeDocument(did);}
 } // do_create()

 private void print_form_edit(ZFieldSet fs)
 {
  User r = new User(); // short alias for User class
  RList rl_scopes=null; // list of SAMScopes
  try{ rl_scopes = getZSystem().listSAMScopes(); }
    catch(Exception e) { printerrmsg(e); rl_scopes = null; }
  int did=was.beginDocument();
  try{
   // header
   was.beginFormPost(SUBSYS_THIS,wam.REQUEST_SAVE,TARGET_THIS);
   // put ZID into request form
   ZField f_id = fs.get(r.F_ID);
   if(f_id != null){
     f_id = new ZField(f_id); f_id.setName(wam.PARAM_ZID);
     if(f_id.getValue() != null) was.printFieldInputHidden(f_id);
   }
   was.printButtonMenuBarEdit();
   was.beginRecord();
   was.printFieldInputHidden(fs.get(r.F_ID));
   was.beginTable();
    was.printTabFieldInput(fs.get(r.F_LOGIN));
    was.printTabFieldInputList(fs.get(r.F_SID),rl_scopes);
    was.printTabFieldInputDic(fs.get(r.F_LOCKED),getDic(wam.DIC_YESNO));
    //was.printTabFieldInput(fs.get(r.F_SLEVEL));
    was.printTabFieldInputDic(fs.get(r.F_SLEVEL),getDic(wam.DIC_SLEVEL));
    was.printTabFieldInputDic(fs.get(r.F_LANG),getDic(wam.DIC_LANG));
    was.printTabFieldInput(fs.get(r.F_DB_USER));
    was.printTabFieldInput(fs.get(r.F_FULL_NAME));
   was.closeTable();
   was.printButtonMenuBarEdit();
    was.closeRecord();
   was.closeForm();
  }finally{was.closeDocument(did);}
 } // print_form_edit()


 /** search for target objects. */
 private void do_find()
  throws WAException
 {
 throw(new WAException(wam,wam.ERROR_NOT_IMPLEMENTED));
 } // do_find()

 private void print_form_find()
 {
 } // print_form_find()

 // constructors
 public WARHSAMUser(
	javax.servlet.http.HttpServletRequest request,
	javax.servlet.http.HttpServletResponse response,
	javax.servlet.jsp.JspWriter out)
 { super(request,response,out); }
 public WARHSAMUser(WARequestHandler warh) { super(warh); }

} //WARHSAMUser
