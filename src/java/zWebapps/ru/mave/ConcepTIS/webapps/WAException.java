// ConcepTIS project
// (c) Alex V Eustrop 2009-2010
// see LICENSE at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.webapps;

public class WAException extends Exception
{
WAException(String msg){super(msg);}
WAException(WAMessages wam, int msg_id)
 {super(wam.getMsgText(msg_id));}
WAException(WAMessages wam, int msg_id, String param1)
 {super(wam.getMsgText(msg_id,param1));}
WAException(WAMessages wam, int msg_id, String param1, String param2)
 {super(wam.getMsgText(msg_id,param1,param2));}
WAException(WAMessages wam, int msg_id, String param1, String param2, String param3)
 {super(wam.getMsgText(msg_id,param1,param2,param3));}
} // public class WAException
