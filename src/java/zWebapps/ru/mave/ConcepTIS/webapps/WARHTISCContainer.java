// ConcepTIS project
// (c) Alex V Eustrop 2009-2010
// see LICENSE at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.webapps;

import java.util.*;
import java.sql.*;
import ru.mave.ConcepTIS.dao.*;
import ru.mave.ConcepTIS.dao.TIS.*;
import ru.mave.ConcepTIS.dao.TISC.*;
import ru.mave.ConcepTIS.dao.SAM.*;

public class WARHTISCContainer extends WARHTypicalObject
{
 public static final String SUBSYS_THIS = WAMessages.SUBSYS_TISC;
 public static final String TARGET_THIS = WAMessages.TARGET_CONTAINER;

 public String getThisDocSubsys(){return(SUBSYS_THIS); }
 public String getThisDocTarget(){return(TARGET_THIS); }
 public int getThisDocTypeCaptionID() { return(wam.OBJ_TISC_CONTAINER); }
 public DObject newThisDocDObject() { return(Container.newDObject()); }

 void printThisDocGeneralHMenu()
 {
  was.printHMenuItem(wam.MNU_TISC_CONTAINER_LIST);
  was.printHMenuItem(wam.MNU_TISC_CONTAINER_CREATE);
  was.printHMenuItem(wam.MNU_TISC_CONTAINER_FIND);
 }

 RList loadThisDocList(Long ZSID, int rows_per_page, long row_offset)
   throws java.sql.SQLException,ZException
 { return( getZSystem().listTISCContainer(ZSID,rows_per_page,row_offset) ); }

 DObject loadThisDocDObject(Long ZOID)
   throws java.sql.SQLException,ZException
 { return(getZSystem().loadTISCContainer(ZOID)); }

 DObject extractThisDocDObject(ZFieldSet fs)
   throws java.sql.SQLException,ZException
 {DObject o = Container.newDObject(); o.getFromFS(fs);return(o);}

 void printThisDocView(DObject o)
 {
  RList rl = o.getTable(CContainer.TAB_CODE);
  RList rl_scopes=null; // list of SAMScopes
  try{ rl_scopes = getZSystem().listSAMScopes(); } catch(Exception e) { printerrmsg(e);}
  load_DocReferences(o);
  ZDictionary dicSLEVEL = getZSystem().getDictionary(wam.DIC_SLEVEL);
   if(rl.size() < 1) { // for empty objects
    was.printMsg(wam.INFO_EMPTY_DOBJECT_VIEWED);
    was.beginTable();
    was.printTabField(ZName.F_ZNAME,o.getZNAME());
    was.printTabField(ZObject.F_ZOID,wam.obj2string(o.getZOID()));
    was.printTabFieldDic(new ZField(ZObject.F_ZTYPE, o.getZTYPE()),
	getDic(wam.DIC_TIS_OBJECT_TYPE));
    was.closeTable();
   }
   for(int i=0;i<rl.size();i++)
   {
   // header
   DORecord dor =((DORecord)(rl.get(i)));
   ZFieldSet fs = dor.toFieldSet();
   was.beginTable();
   was.printTabField(fs,CContainer.F_NUM);
   //was.printTabField(fs,CContainer.F_SCOPE_ID);
   was.printTabFieldDic(fs,CContainer.F_TYPE,getDic(CContainer.FC_TYPE));
   was.printTabField(fs,CContainer.F_DESCR);
   //was.printTabFieldDic(fs,ZObject.F_ZTYPE,getDic(wam.DIC_TIS_OBJECT_TYPE));
   was.closeTable(); //header
   was.beginIndent();
   RList rlCE=dor.getTableChildren(CEntry.TAB_CODE);
    for(int iCE=0;iCE<rlCE.size();iCE++)
    {
    DORecord CE = (DORecord)rlCE.get(iCE);
    fs= CE.toFieldSet();
    was.beginTable();
    was.printTabFieldZOID2ZNAME(fs,CEntry.F_OBJ_ID,getZNames4Field(CEntry.FC_OBJ_ID),wam.SUBSYS_TISC);
    was.closeTable(); //header
    }
   was.closeIndent();
   }
 } // printDocumentView

 void printThisDocEditForm(ZFieldSet fs)
 {
  int i;
  CContainer CC = null;
  CEntry CE = null;
  RList rl_scopes = getSAMScopes(); // exceptions are ignored!
  for(i=0;i<fs.countFS();i++)
  {
   ZFieldSet fsCC = fs.getFS(i);
   if(!CContainer.TAB_CODE.equals(fsCC.getCode())) continue;
   was.beginRecord(); //CC
   was.printSTDFieldsInputDORecord(fsCC);
   was.beginTable();
    was.printTabRecordDeletionStatus(fsCC);
    was.printTabFieldInput(fsCC.get(CC.F_NUM));
    was.printTabFieldInputDic(fsCC.get(CC.F_TYPE),getDic(CC.FC_TYPE));
    was.printTabFieldInput(fsCC.get(CC.F_DESCR));
   was.closeTable();
   int i2;
   was.beginIndent();
   for(i2=0;i2<fsCC.countFS();i2++)
   {
    ZFieldSet fsCE = fsCC.getFS(i2);
    if(!CEntry.TAB_CODE.equals(fsCE.getCode())) continue;
    was.beginRecord(); //CE
    was.printSTDFieldsInputDORecord(fsCE);
    was.beginTable();
     was.printTabRecordDeletionStatus(fsCE);
     was.printTabFieldInput(fsCE.get(CE.F_OBJ_ID));
    was.closeTable();
    was.closeRecord(); //CE
   }
   was.printButtonAddRecord(CEntry.TAB_CODE);was.println();
   was.closeIndent();
   was.closeRecord(); //CC
  }
  if(i==0)was.println();
  was.printButtonAddRecord(CContainer.TAB_CODE);was.println();
  //was.print(wam.getMsgText(wam.ADD_RECORD));
  //was.printButtonAddRecord(CContainer.TAB_CODE,CContainer.TAB_CODE);was.println();
  was.printButtonMenuBarEdit();
  was.closeRecord(); //DObject
  was.closeForm();
 } //printThisDocEditForm

 // constructors
 public WARHTISCContainer(
	javax.servlet.http.HttpServletRequest request,
	javax.servlet.http.HttpServletResponse response,
	javax.servlet.jsp.JspWriter out)
 { super(request,response,out); }
 public WARHTISCContainer(WARequestHandler warh) { super(warh); }

} //WARHTISCContainer
