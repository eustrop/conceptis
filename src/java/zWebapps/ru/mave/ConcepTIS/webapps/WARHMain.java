// ConcepTIS project
// (c) Alex V Eustrop 2009-2010
// see LICENSE at the project's root directory
//
// $Id$
//

package ru.mave.ConcepTIS.webapps;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspWriter;


public class WARHMain extends WARequestHandler{
    private boolean showTopMenu = false; // show topMenu() at each page
    public boolean setTopMenu(boolean v) { boolean ov = v; showTopMenu = v; return(ov); }

    // constructors
    public WARHMain(HttpServletRequest request,HttpServletResponse response,JspWriter out){
        super(request,response,out);
    } // WARHMain(HttpServletRequest,JspWriter)

    public WARHMain(WARequestHandler warh) {
        super(warh);
    } // WARHMain(WARequestHandler warh)


    /** find, create and return specific request handler using request's parameters (subsys, request, target)
     * 
     */
    public WARequestHandler findRequestHandler()
    {
     String pSubsys = request.getParameter(wam.PARAM_SUBSYS);
     String pRequest = request.getParameter(wam.PARAM_REQUEST);
     String pTarget = request.getParameter(wam.PARAM_TARGET);
     String pZID = request.getParameter(wam.PARAM_ZID);
     Long ZID = wam.string2Long(pZID);
     WARequestHandler warh = null;
     if(wam.SUBSYS_PSQL.equals(pSubsys)) warh = new WARHPSQL(this);
     else if(wam.SUBSYS_DIC.equals(pSubsys)) warh = new WARHDictionary(this);
     else if(wam.SUBSYS_TIS.equals(pSubsys)) { warh = new WARHTISObject(this); }
     else if(wam.SUBSYS_SAM.equals(pSubsys)){
            if(wam.TARGET_SAMUSER.equals(pTarget)) warh = new WARHSAMUser(this);
            else if(wam.TARGET_SAMGROUP.equals(pTarget)) warh = new WARHSAMGroup(this);
            else if(wam.TARGET_SAMSCOPE.equals(pTarget)) warh = new WARHSAMScope(this);
     }else if(wam.SUBSYS_TISC.equals(pSubsys)){
            if(wam.TARGET_AREA.equals(pTarget)) warh = new WARHTISCArea(this);
            else if(wam.TARGET_CONTAINER.equals(pTarget)) warh=new WARHTISCContainer(this);
            else if(wam.TARGET_DOCUMENT.equals(pTarget)) warh=new WARHTISCDocument(this);
        }
      return(warh);
    } // findRequestHandler()


    public void process(){

        String pSubsys = request.getParameter(wam.PARAM_SUBSYS);
        String pRequest = request.getParameter(wam.PARAM_REQUEST);
        String pTarget = request.getParameter(wam.PARAM_TARGET);
        String pZID = request.getParameter(wam.PARAM_ZID);
        String flagJS = request.getParameter("flagJS");
        Long ZID = wam.string2Long(pZID);
        WARequestHandler warh = null;
        // if (flagJS !=null && !flagJS.equals("flag")) { // I don't understand this, possible example of bad code for discuss (Eustrop 2019/08/08)
	if(showTopMenu) topMenu();
        if(wam.SUBSYS_PSQL.equals(pSubsys)) warh = new WARHPSQL(this);
        else if(wam.SUBSYS_DIC.equals(pSubsys)) warh = new WARHDictionary(this);
        else if(wam.SUBSYS_TIS.equals(pSubsys)) { warh = new WARHTISObject(this); }
        else if(wam.SUBSYS_SAM.equals(pSubsys)){

            if(wam.TARGET_SAMUSER.equals(pTarget)) warh = new WARHSAMUser(this);
            else if(wam.TARGET_SAMGROUP.equals(pTarget)) warh = new WARHSAMGroup(this);
            else if(wam.TARGET_SAMSCOPE.equals(pTarget)) warh = new WARHSAMScope(this);
            else {
                menuSAM();
                return;
            }

        }else if(wam.SUBSYS_TISC.equals(pSubsys)){

            if(pTarget == null && ZID != null) { // resolve dobject's type code 

                try{
                    db_logon();
                    pTarget = getZSystem().resolveZTYPE(ZID);
                    if(pTarget == null) pSubsys = wam.SUBSYS_TIS;

                    was.sendRedirect(response,pSubsys,pRequest,pTarget,ZID);

                }catch(Exception e){ printerrmsg(e); }
                finally{ db_logoff(); }
                return;

            } if(wam.TARGET_AREA.equals(pTarget)) warh = new WARHTISCArea(this);
            else if(wam.TARGET_CONTAINER.equals(pTarget)) warh=new WARHTISCContainer(this);
            else if(wam.TARGET_DOCUMENT.equals(pTarget)) warh=new WARHTISCDocument(this);
            else {
                menuTISC();
                return;
            }
        }

        if(warh != null) warh.process();
        else{
            if(wam.SUBSYS_MAIN.equals(pSubsys) || !request.getParameterNames().hasMoreElements() ) mainMenu();
            else was.printErrMsg(wam.getMsgText(wam.ERROR_CALL_NOT_IMPLEMENTED));
        }
    } // process()

    public void processREST()
    {
        String pSubsys = request.getParameter(wam.PARAM_SUBSYS);
        String pRequest = request.getParameter(wam.PARAM_REQUEST);
        String pTarget = request.getParameter(wam.PARAM_TARGET);
        String pZID = request.getParameter(wam.PARAM_ZID);
        String flagJS = request.getParameter("flagJS");
        Long ZID = wam.string2Long(pZID);
        WARequestHandler warh = null;
	warh = findRequestHandler();
	if(warh == null) was.printRESTInvalidRequest();
	else warh.processREST();
    } // processREST()

    public void topMenu(){
	 was.beginHMenu();
	 was.printHMenuItem(wam.MNU_MAIN);
	 was.printHMenuItem(wam.MNU_SAM_MAIN);
	 was.printHMenuItem(wam.MNU_TISC_MAIN);
	 was.printHMenuItem(wam.MNU_DIC_MAIN);
	 was.printHMenuItem(wam.MNU_PSQL_MAIN);
			//was.printHMenuItem("<TISC>",wam.SUBSYS_TISC,null);
			//was.printHMenuItem("<SQL ad hoc>",wam.SUBSYS_PSQL,null);
	 was.closeHMenu();
   } //topMenu()

    public void mainMenu(){

        was.beginTMenu();
        was.printTMenuCurrent(wam.MNU_MAIN);
        was.beginTMenu();
        //was.printTMenuItem(wam.MNU_MAIN);
        was.printTMenuItem(wam.MNU_TISC_MAIN);
        was.printTMenuItem(wam.MNU_SAM_MAIN);
        was.printTMenuSeparator();
        was.printTMenuItem(wam.MNU_TIS_MAIN);
        was.printTMenuItem(wam.MNU_DIC_MAIN);
        was.printTMenuSeparator();
        was.printTMenuItem(wam.MNU_PSQL_MAIN);
        was.closeTMenu();
        was.printTMenuItem(wam.MNU_HELP_MAIN);
        was.closeTMenu();
    } // mainMenu()

    public void menuSAM(){

        was.beginTMenu();
        was.printTMenuCurrent(wam.MNU_SAM_MAIN);
        was.beginTMenu();
        was.printTMenuItem(wam.MNU_SAM_USER_LIST);
        was.printTMenuItem(wam.MNU_SAM_USER_CREATE);
        was.printTMenuItem(wam.MNU_SAM_USER_FIND);
        was.printTMenuSeparator();
        was.printTMenuItem(wam.MNU_SAM_GROUP_LIST);
        was.printTMenuItem(wam.MNU_SAM_GROUP_CREATE);
        was.printTMenuItem(wam.MNU_SAM_GROUP_FIND);
        was.printTMenuSeparator();
        was.printTMenuItem(wam.MNU_SAM_SCOPE_LIST);
        was.printTMenuItem(wam.MNU_SAM_SCOPE_CREATE);
        was.printTMenuItem(wam.MNU_SAM_SCOPE_FIND);
        was.printTMenuSeparator();
        was.printTMenuItemCustomPSQL(wam.getMsgText(wam.CAPTION_CREATE_DBOWNER_USER_ONCE),wam.getSQL(wam.SQL_CREATE_DBOWNER_USER_ONCE));
        was.closeTMenu();
        was.closeTMenu();
    } //menuSAM

    public void menuTISC(){

        was.beginTMenu();
        was.printTMenuCurrent(wam.MNU_TISC_MAIN);
        was.beginTMenu();
        was.printTMenuItem(wam.MNU_TISC_AREA_LIST);
        was.printTMenuItem(wam.MNU_TISC_AREA_CREATE);
        was.printTMenuItem(wam.MNU_TISC_AREA_FIND);
        was.printTMenuSeparator();
        was.printTMenuItem(wam.MNU_TISC_CONTAINER_LIST);
        was.printTMenuItem(wam.MNU_TISC_CONTAINER_CREATE);
        was.printTMenuItem(wam.MNU_TISC_CONTAINER_FIND);
        was.printTMenuSeparator();
        was.printTMenuItem(wam.MNU_TISC_DOCUMENT_LIST);
        was.printTMenuItem(wam.MNU_TISC_DOCUMENT_CREATE);
        was.printTMenuItem(wam.MNU_TISC_DOCUMENT_FIND);
        was.closeTMenu();
        was.closeTMenu();
    } //menuTISC


} //WARHMain
