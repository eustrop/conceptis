// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

//package ru.mave.ConcepTIS.webapps;
package org.eustrosoft.qtis.webapps;

public class WAException extends Exception
{
public WAException(String msg){super(msg);}
public WAException(WAMessages wam, int msg_id)
 {super(wam.getMsgText(msg_id));}
public WAException(WAMessages wam, int msg_id, String param1)
 {super(wam.getMsgText(msg_id,param1));}
public WAException(WAMessages wam, int msg_id, String param1, String param2)
 {super(wam.getMsgText(msg_id,param1,param2));}
public WAException(WAMessages wam, int msg_id, String param1, String param2, String param3)
 {super(wam.getMsgText(msg_id,param1,param2,param3));}
} // public class public WAException
