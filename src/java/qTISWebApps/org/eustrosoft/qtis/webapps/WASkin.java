// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

//package ru.mave.ConcepTIS.webapps;
package org.eustrosoft.qtis.webapps;

import java.util.*;
import ru.mave.ConcepTIS.dao.*;
import ru.mave.ConcepTIS.dao.TIS.*;

/** Markup-language-neutral tool for building hierarchical, table based visual
 * documents and input forms (HTML at present and not-only-html at future).
 * This class purposed to encapsulate all html (http, xml, rtf and so on)
 * specific into functional nutshells which could be easily replased by
 * any alternate solution. Instance of this class used by WARequestHandler
 * and it's derivations to construct html documents.
 *
 * @see WARequestHandler#setWASkin
 */
public class WASkin
{

//
// Global parameters
//
private final static String CLASS_VERSION = "$Id$";
private String CGI_NAME = "index.jsp";
private final String DBSERVER_URL = "jdbc:postgresql:conceptisdb";

private java.io.PrintWriter out;
private WAMessages wam;
private ZDictionary dicFieldDic;

private Stack TagStack = new Stack();
private int TagStackCount = 0;

/** this string used to prefix any "in-record" input fields. */
private final String RECORD_PREFIX = "__REC";
private final String RECORD_CODE = "_";
private final String RECORD_ADDNEW = "__ADDNEW";
private final String RECORD_DELETE = "__DEL";
/** current prefix for "in-record" input fields. */
private String CurRecPrefix = null;
/** stack of Integer objects one for each nested record.
 * This Integer object used for generation unique prefix-id for
 * each new nested record at the same hierarchic level.
 */
private Stack RecordStack = new Stack();
/** helpful cache of CurRecPrefix of all parental records. */
private Stack RecordPrefixStack = new Stack();
/** hierarchic level of current record number of objects e for each nested record. */
private int RecordsStackCount = 0;

private boolean is_error=false;
private Exception last_exception = null;
private boolean is_debug=true;

/** set debug mode. if v=true -> display all printDebug() messages, else - ignore them. */
public void setDebug(boolean v){is_debug=v;}
/** display debug messages if setDebug(true). */
public void printDebug(String msg){if(!is_debug)return;printDivDebug(msg);} 

/** main raw text writing method (all writes comes through). */
public void w(String s)
{
 is_error=false;
 try{out.print(s);}
 catch(Exception e)
  {is_error=true;last_exception=e;}
} // w(String s)
public void wln(String s){w(s);w("\n");}
public void wln(){w("\n");}

public void sendHTTPRedirect(
	javax.servlet.http.HttpServletResponse response, String szURL)
{
 try{
  //if(true) throw(new java.io.IOException("test exception"));
  response.sendRedirect(szURL);
 }
 catch(java.io.IOException e) {
  printErrMsg(e);
  wln(wam.getMsg(wam.ERROR_WHILE_HTTPREDIRECT));
  wln(mkHRef4URL(szURL,szURL));
 }
} // sendHTTPRedirect

// error control methods
public boolean checkError(){return(is_error);}
public Exception getLastException(){return(last_exception);}

/** obj2html */
private static String t2h(String s){return(WAMessages.obj2html(s));}
/** obj2value */
private static String o2v(Object o){return(WAMessages.obj2value(o));}
/** obj2urlvalue (WARNING: must be rewritten) */
private static String o2uv(Object o){return(WAMessages.obj2value(o));}
/** obj2text */
private static String o2t(Object o){return(WAMessages.obj2text(o));}
/** obj2string */
private static String o2s(Object o){return(WAMessages.obj2string(o));}

// TagStack routines

private static final String TAG_DOCUMENT = "Document";
private static final String TAG_RECORD = "Record";
private static final String TAG_TABLE = "table";
private static final String TAG_TBODY = "tbody";
private static final String TAG_TR = "tr";
private static final String TAG_TH = "th";
private static final String TAG_TD = "td";
private static final String TAG_UL = "UL";

public int openTag(String tag){TagStack.push(tag);return(++TagStackCount);}
public int closeTag(String tag)
{
  String t = null;
  while(TagStackCount > 0)
  {
   t = (String)TagStack.pop();
   if(TAG_DOCUMENT.equals(t)) wln("<!-- /Document -->");
    else { w("</"); w(t); wln(">"); }
   TagStackCount--;
   if(tag.equals(t)) break;
  }
  if(!tag.equals(t)) { wln("<!-- missing open tag " + tag + " -->"); }
  return(TagStackCount);
} // int closeTag(String tag)
public int closeTag(int tag)
{
 if(tag > TagStackCount) return(TagStackCount);
 while(TagStackCount >= tag)
 {
  String t = (String)TagStack.pop();
  if(TAG_DOCUMENT.equals(t)) wln("<!-- /Document -->");
   else { w("</"); w(t); wln(">"); }
  TagStackCount--;
 } //while
 return(TagStackCount);
} // closeTag(int tag)

//
// properties get/set
//

public void setOut(java.io.PrintWriter out){this.out=out;}
public java.io.PrintWriter getOut(){return(out);}

public String getCGI() { return(CGI_NAME); }
public String setCGI(String CGI) {String s=CGI_NAME; CGI_NAME=CGI; return(s);}

public ZDictionary getFieldDic() { return(dicFieldDic); }
public void setFieldDic(ZDictionary dic) {dicFieldDic=dic;}


//
// document/table/tree base primitives
//

public int beginDocument(){wln("<!-- Document -->");return(openTag(TAG_DOCUMENT));}
public int closeDocument(){return(closeTag(TAG_DOCUMENT));}
public int closeDocument(int tag_id){return(closeTag(tag_id));}


/** all input fields names (except 'submit' subtype of 'input') will
 * be prepended by special substring with fixed prefix and dynamic
 * encoding of current hierachic level.
 */
public int beginRecord()
{
 Integer rec_this = new Integer(0);
 RecordsStackCount++;
 if(RecordsStackCount > 1){
  // increment parent's counter of children
  Integer rec_parent = (Integer)RecordStack.pop();
  rec_parent = new Integer(rec_parent.intValue()+1);
  RecordStack.push(rec_parent);
  // preserve curent prefix of names and make the new one
  RecordPrefixStack.push(CurRecPrefix);
  CurRecPrefix = CurRecPrefix + rec_parent.toString() + ".";
 }
 else{ CurRecPrefix = RECORD_PREFIX + "."; }
 RecordStack.push(rec_this);
 w("<!-- Record (" + CurRecPrefix + ") -->");
 return(RecordsStackCount);
}

public int closeRecord(){
 Integer rec_this = (Integer)RecordStack.pop();
 RecordsStackCount--;
 w("<!-- /Record (" + CurRecPrefix + ") -->");
 if(RecordsStackCount>0)
  CurRecPrefix = (String)RecordPrefixStack.pop();
 else 
  CurRecPrefix = null;
 return(RecordsStackCount);
} // closeRecord()

/** helpful for ecoding URL parameters per temporary WASkin.
 * @see #mkURLParams
 */
public String getCurRecPrefix() { return(CurRecPrefix); }

public int closeRecord(int rec_id){
while(rec_id<=RecordsStackCount){closeRecord();}
return(RecordsStackCount);
}

private String mkFName(String name){
if(RecordsStackCount > 0 ) return(CurRecPrefix+o2v(name));
return(o2v(name));
}

public void beginIndent()
{
 wln("<!-- Indent -->");
  wln("<table class='TISCIndent' width=98%><tbody>"); // if width=100% -> visual bug on Firefox&FreeBSD
  int t=openTag(TAG_TABLE);openTag(TAG_TBODY);
 //beginTable();
 beginTableRow();
 wln("<td width='3%'>&nbsp;</td><td width='97%'>");
}//openIndent

public void closeIndent()
{
 wln("</td>");
 closeTableRow();
 closeTable();
 wln("<!-- /Indent -->");
} //closeIndent()

public int beginTable()
 {
  wln("<table><tbody>");
  int t=openTag(TAG_TABLE);openTag(TAG_TBODY);
  return(t);
 }
public int beginTable(String raw_options)
 {
  w("<table ");
  w(raw_options);
  wln("><tbody>");
  int t=openTag(TAG_TABLE);openTag(TAG_TBODY);
  return(t);
 }
public int closeTable(){return(closeTag(TAG_TABLE));}
public int closeTable(int tag_id){return(closeTag(tag_id));}
public int beginTableRow(){w("<tr>");return(openTag(TAG_TR));}
public int closeTableRow(){return(closeTag(TAG_TR));}
public void beginTableCell(){w("<td>");}
public void closeTableCell(){wln("</td>");}
public void beginTableHCell(){w("<th bgcolor='#CCCCCC'>");}
public void closeTableHCell(){wln("</th>");}

public int beginTree(){wln("<ul>");return(openTag(TAG_UL));}
public int closeTree(){return(closeTag(TAG_UL));}
public int closeTree(int tag_id){return(closeTag(tag_id));}

public void beginNode(){w("<li>");}
public void closeNode(){wln("</li>");}
public void printNode(String s){beginNode();w(t2h(s)); closeNode();}

public void print(String s){w(t2h(s));}
public void println(String s){w(t2h(s));wln("<br>");}
public void println(){wln("<br>");}

public void printSeparator(){wln("<hr>");}
public void printParagraph(String s){w("<p>"); w(t2h(s)); w("</p>");}
public void printDiv(String s){w("<div>"); w(t2h(s)); w("</div>");}
public void printDivDebug(String s){w("<div class='TISCUIDebug' title='this is debug message!'><pre>"); w(t2h(s)); w("</pre></div>");}

public void printTitle(String s){w("<h2>"); w(t2h(s)); w("</h2>");}
public void printSubtitle(String s){printSubtitle1(s);}
public void printSubtitle1(String s){w("<h3>"); w(t2h(s)); w("</h3>");}
public void printSubtitle2(String s){w("<h4>"); w(t2h(s)); w("</h4>");}

/** for internal WASkin use. both fname and fvalue are raw html code. */
public void printTabFieldRaw(String fcaption, String fvalue)
{
 beginTableRow();
  beginTableCell();
   w("<b>"); w(fcaption); w("</b>");
  closeTableCell();
  beginTableCell();
   w(fvalue);
  closeTableCell();
 closeTableRow();
} // printTabField(String fname, String fvalue)
public void printTabFieldRaw(String fcaption, String fvalue,String descr)
{
 beginTableRow();
  beginTableCell();
   w("<b>"); w(fcaption); w("</b>");
  closeTableCell();
  beginTableCell();
   w(fvalue);
  closeTableCell();
  beginTableCell();
   w(descr);
  closeTableCell();
 closeTableRow();
} // printTabField(String fname, String fvalue)
private void printTabFieldHeaderRaw(String fcaption, String fvalue)
{
 beginTableRow();
  beginTableHCell();
   w("<b>"); w(fcaption); w("</b>");
  closeTableHCell();
  beginTableHCell();
   w("<b>"); w(fvalue); w("</b>");
  closeTableHCell();
 closeTableRow();
} // printTabField(String fname, String fvalue)
private void printTabFieldHeaderRaw(String fcaption, String fvalue,String descr)
{
 beginTableRow();
  beginTableHCell();
   w("<b>"); w(fcaption); w("</b>");
  closeTableHCell();
  beginTableCell();
   w("<b>"); w(fvalue); w("</b>");
  closeTableHCell();
  beginTableHCell();
   w("<b>"); w(descr); w("</b>");
  closeTableHCell();
 closeTableRow();
} // printTabFieldHeaderRaw(String fname, String fvalue)
private void printTabFieldTextareaRaw(String fcaption, String fvalue)
{
 beginTableRow();
  w("<td colspan='3'>"); //beginTableCell();
   w("<b>"); w(fcaption); w("</b><br>");
   w("<hr noshade align='left' width='80%'>");
   w(fvalue);
   w("<hr nohade>");
  closeTableCell();
 closeTableRow();
} // printTabField(String fname, String fvalue)

public void printTabFieldSeparator() {printTabFieldSeparator(null);}
public void printTabFieldSeparator(String caption)
{
 if(caption==null) caption="nbps;";
 else caption=t2h(caption);
 beginTableRow();
   w("<td colspan='2' bgcolor='#CCCCCC'><b>"); w(caption); w("</b></td>");
 closeTableRow();
} //printTabFieldSeparator(String caption)

public void printTabFieldInputPassword(String fcaption, String fname,  String fvalue, int size, int maxlength, String descr)
{
 printTabFieldRaw(t2h(fcaption),mkFieldInputPassword(fname,fvalue,size,maxlength),t2h(descr));
} // printTabField(String fname, String fvalue)

public void printTabFieldInput(String fcaption, String fname,  String fvalue, int size, int maxlength, String descr)
{
 printTabFieldRaw(t2h(fcaption),mkFieldInputText(fname,fvalue,size,maxlength),t2h(descr));
} // printTabField(String fname, String fvalue)

public void printTabFieldInput(String fcaption, String fname,  String fvalue, int size, int maxlength)
{
 printTabFieldRaw(t2h(fcaption),mkFieldInputText(fname,fvalue,size,maxlength));
} // printTabField(String fname, String fvalue)

public void printTabFieldInput(String fcaption, String fname,  String fvalue, int size)
{printTabFieldInput(fcaption,fname,o2s(fvalue),size,-1);}

public void printTabFieldInput(String fcaption, String fname, String fvalue)
{ printTabFieldInput(fcaption,fname,o2s(fvalue),0); }

public void printTabFieldInput(String fcaption, String fname, Long fvalue)
{ printTabFieldInput(fcaption,fname,o2s(fvalue),22); }

public void printTabFieldInput(String fcaption, String fname, Short fvalue)
{ printTabFieldInput(fcaption,fname,o2s(fvalue),6); }

public void printTabFieldInput(String fcaption, String fname, YesNo fvalue)
{ printTabFieldInput(fcaption,fname,o2s(fvalue),1); }

public void printTabFieldInputArrayDic(String fcaption, String fname, Object fvalue,
	Object[] dic_values)
{ printTabFieldInputArrayDic(fcaption,fname,fvalue,dic_values,dic_values); }

public void printTabFieldInputArrayDic(String fcaption, String fname, Object fvalue,
	Object[] dic_values, Object[] dic_captions)
{
if(dic_values == null) {printTabFieldInput(fcaption,fname,o2s(fvalue)); return; }
if(dic_captions == null) dic_captions = dic_values;
printTabFieldRaw(t2h(fcaption),mkFieldInputArrayDic(fname,fvalue,dic_values,dic_captions));
} //printTabFieldInputArrayDic

/** dic_listitems is array of msg indexes for getMsgText(). */
public void printTabFieldInputArrayDic(String fcaption, String fname, Object fvalue,
	int[] dic_listitems)
{
 String[] dic_values=new String[dic_listitems.length];
 String[] dic_captions=new String[dic_listitems.length];
 for(int i=0; i<dic_listitems.length; i++){
  dic_values[i]=new Integer(dic_listitems[i]).toString();
  dic_captions[i]=new String(wam.getMsgText(dic_listitems[i]));
  }
 printTabFieldInputArrayDic(fcaption,fname,fvalue,dic_values,dic_captions); 
}

public void printTabFieldInput(ZField f)
{
if(f == null) {printTabField((String)null,(String)null); return; }
int len=f.getLength();
int size=len; if(size==0) size=30;
printTabFieldInput(mkFCaption(f),f.getName(),f.getValue(),size,len,mkFDescr(f));
}

public void printTabFieldInputPassword(ZField f)
{
if(f == null) {printTabField((String)null,(String)null); return; }
int len=f.getLength();
int size=len; if(size==0) size=30;
printTabFieldInputPassword(mkFCaption(f),f.getName(),f.getValue(),size,len,mkFDescr(f));
}

public void printTabFieldInputTextarea(ZField f)
{
if(f == null) {printTabField((String)null,(String)null); return; }
int len=f.getLength();
String caption = mkFDescr(f);
//if not mkFCaption(f)
printTabFieldInputTextarea(caption,f.getName(),f.getValue());
}
public void printTabFieldInputOWiki(ZField f) //!SIC for future implementation
{
if(f == null) {printTabField((String)null,(String)null); return; }
int len=f.getLength();
String caption = mkFDescr(f);
//if not mkFCaption(f)
printTabFieldInputTextarea(caption,f.getName(),f.getValue(),10,132);
}

public void printTabFieldInputDic(ZField f, ZDictionary dic)
{printTabFieldInputDic(f,dic,0);}

public void printTabFieldInputDic(ZField f, ZDictionary dic, int null_msg)
{
if(f == null) {printTabField((String)null,(String)null); return; }
if(dic == null) {printTabFieldInput(f); return; }
printTabFieldRaw(t2h(mkFCaption(f)),mkFieldInputDic(f.getName(),f.getValue(),dic, null_msg),mkFDescr(f));
}

public void printTabFieldInputList(ZField f, RList list)
{printTabFieldInputList(f,list,0);}

/** show field input as select from list.
 * @param null_msg WAMessage.getMsg() caption id for null value
 *    (zero for no null value possible unless f.getValue() is null already)
 */
public void printTabFieldInputList(ZField f, RList list, int null_msg)
{
if(f == null) {printTabField((String)null,(String)null); return; }
if(list == null) {printTabFieldInput(f); return; }
Long id=null; try{id=Long.valueOf(f.getValue());} catch(NumberFormatException e){}
printTabFieldRaw(t2h(mkFCaption(f)),mkFieldInputList(f.getName(),id,list, null_msg),mkFDescr(f));
}

public void printFieldInputHidden(String fname, Object fvalue)
{ w(mkFieldInputHidden(fname,o2s(fvalue))); }

public void printFieldInputHidden(ZField f)
{if(f!=null) w(mkFieldInputHidden(f.getName(),f.getValue())); }

public void printSTDFieldsInputDObject(ZFieldSet fs, ZSystem zsys)
{
 ZDictionary dicSLEVEL=null;
 RList rlScopes=null;
 dicSLEVEL = zsys.getDictionary(wam.DIC_SLEVEL);
 try{rlScopes=zsys.listSAMScopes();}catch(Exception e){/*printerrmsg(e);*/}
 printSTDFieldsInputDObject(fs,rlScopes,dicSLEVEL);
}

public void printSTDFieldsInputDObject(ZFieldSet fs)
{printSTDFieldsInputDObject(fs,null,null);}

public void printSTDFieldsInputDObject(ZFieldSet fs,
 RList rlScopes, ZDictionary dicSLEVEL)
{
printFieldInputHidden(RECORD_CODE,fs.getCode());
Long ZOID = wam.string2Long(fs.getValue(ZObject.F_ZOID));
if( ZOID != null) { // existing DObject
 printFieldInputHidden(fs.get(ZObject.F_ZOID));
 printFieldInputHidden(fs.get(ZObject.F_ZVER));
 printFieldInputHidden(fs.get(ZObject.F_ZTYPE));
 printFieldInputHidden(fs.get(ZObject.F_ZSID));
 printFieldInputHidden(fs.get(ZObject.F_ZLVL));
 printFieldInputHidden(fs.get(ZObject.F_ZUID));
 printFieldInputHidden(fs.get(ZObject.F_ZSTA));
 printFieldInputHidden(fs.get(ZObject.F_ZDATE));
 printFieldInputHidden(fs.get(ZObject.F_ZDATO));
}
else { // new DObject
 beginTable();
 printFieldInputHidden(fs.get(ZObject.F_ZTYPE));
 printTabFieldInputList(fs.get(ZObject.F_ZSID),rlScopes);
 printTabFieldInputDic(fs.get(ZObject.F_ZLVL),dicSLEVEL);
 closeTable();
}
} //printSTDFieldsInputDObject


public void printSTDFieldsInputDORecord(ZFieldSet fs)
{
printFieldInputHidden(RECORD_CODE,fs.getCode());
printFieldInputHidden(fs.get(DORecord.F_ZOID));
printFieldInputHidden(fs.get(DORecord.F_ZRID));
printFieldInputHidden(fs.get(DORecord.F_ZVER));
printFieldInputHidden(fs.get(DORecord.F_ZTOV));
printFieldInputHidden(fs.get(DORecord.F_ZSID));
printFieldInputHidden(fs.get(DORecord.F_ZLVL));
printFieldInputHidden(fs.get(DORecord.F_ZPID));
}

public void printRecordDeletionStatus(ZFieldSet fs)
{
 w(mkFieldInputCheckbox(RECORD_DELETE,fs.getMarkedForDeletion()));
 w(wam.getMsgText(wam.DELETE));
}

public void printTabRecordDeletionStatus(ZFieldSet fs)
{
 printTabFieldRaw(
 mkFieldInputCheckbox(RECORD_DELETE,fs.getMarkedForDeletion()),
 t2h(wam.getMsgText(wam.DELETE_RECORD))
 );
}

public void printTabField(String fcaption, String fvalue)
{
 printTabFieldRaw(t2h(fcaption),t2h(fvalue));
} // printTabField(String fname, String fvalue)
public void printTabField(String fcaption, String fvalue,String descr)
{
 printTabFieldRaw(t2h(fcaption),t2h(fvalue),t2h(descr));
} // printTabField(String fname, String fvalue,String descr)

public void printTabFieldHeader(String fcaption, String fvalue)
{
 printTabFieldHeaderRaw(t2h(fcaption),t2h(fvalue));
} // printTabFieldHeader(String fname, String fvalue)
public void printTabFieldHeader(String fcaption, String fvalue,String descr)
{
 printTabFieldHeaderRaw(t2h(fcaption),t2h(fvalue),t2h(descr));
} // printTabFieldHeader(String fname, String fvalue,String descr)

public void printTabFieldTextarea(String fcaption, String fvalue)
{
 printTabFieldTextareaRaw(t2h(fcaption),t2h(fvalue));
} // printTabField(String fname, String fvalue)

public void printTabField(ZField f)
{
 String fcaption=null, fvalue=null;
 if(f != null){
  fcaption = mkFCaption(f); fvalue = f.getValue();
 }
 printTabField(fcaption,fvalue);
} // printTabField(ZField f)
public void printTabFieldQR(ZField f)
{
 String fcaption=null, fvalue=null;
 if(f != null){
  fcaption = mkFCaption(f); fvalue = f.getValue();
  fvalue="<a target='qr_" + fvalue + "' href='/qr/?q=" + fvalue + "'>" +fvalue+"</a>";
 }
/*
 try{
  Long qr = Long.valueOf(fvalue);
  fvalue = Long.toHexString(qr.longValue()).toUpperCase();
 }
 catch(NumberFormatException nfe){}
*/
 //printTabField(fcaption,fvalue);
 printTabFieldRaw(t2h(fcaption),fvalue);
} // printTabField(ZField f)

public void printTabFieldTextarea(ZField f)
{
 String fcaption=null, fvalue=null;
 if(f != null){
  fcaption = mkFCaption(f); fvalue = f.getValue();
 }
 printTabFieldTextarea(fcaption,fvalue);
} // printTabField(ZField f)

public void printTabFieldOWiki(ZField f)
{
 String fcaption=null, fvalue=null;
 if(f != null){
  fcaption = mkFCaption(f); fvalue = f.getValue();
 }
 printTabFieldTextarea(fcaption,fvalue);
} // printTabField(ZField f)

public void printTabFieldDic(ZField f,ZDictionary dic)
{
 String fcaption=null, fvalue=null;
 if(f != null){
  fcaption = mkFCaption(f); fvalue = mkFCodeValue(f,dic);
 } //f!=null
 printTabField(fcaption,fvalue);
} // printTabFieldDic(ZField f,ZDictionary dic)

public void printTabFieldList(ZField f,RList dic, String subsys, String target)
{
 String fcaption=null, fvalue=null;
 if(f != null){
  fcaption = mkFCaption(f); fvalue = mkHRefView(f,dic,subsys,target);
 } //f!=null
 printTabFieldRaw(fcaption,fvalue);
} // printTabFieldList(ZField f,RList dic, String subsys, String target)


public void printTabFieldRefView(String fcaption, String fvalue,
	String subsys, String target, Long id)
{
 printTabFieldRaw(t2h(fcaption),mkHRefView(fvalue,subsys,target,id));
}

public void printTabFieldRefView(ZField f,RList dic,String subsys, String target)
{
 String fcaption=null, fvalue=t2h(null);
 if(f != null){
  fcaption = mkFCaption(f); fvalue = mkHRefView(f,dic,subsys,target);
 } //f!=null
 printTabFieldRaw(t2h(fcaption),fvalue);
} // printTabField(ZField f)

public void printTabField(ZFieldSet fs, String fname)
{
 printTabField(fs.get(fname));
} // printTabField(ZFieldSet fs, String fname)
public void printTabFieldQR(ZFieldSet fs, String fname)
{
 printTabFieldQR(fs.get(fname));
} // printTabField(ZFieldSet fs, String fname)
public void printTabFieldTextarea(ZFieldSet fs, String fname)
{
 printTabFieldTextarea(fs.get(fname));
} // printTabField(ZFieldSet fs, String fname)

public void printTabFieldOWiki(ZFieldSet fs, String fname)
{
 printTabFieldOWiki(fs.get(fname));
} // printTabField(ZFieldSet fs, String fname)

public void printTabFieldDic(ZFieldSet fs, String fname,ZDictionary dic)
{
 printTabFieldDic(fs.get(fname),dic);
} // printTabFieldDic(ZFieldSet fs, String fname,ZDictionary dic)

public void printTabFieldList(ZFieldSet fs, String fname,RList dic, String subsys, String target)
{
 printTabFieldList(fs.get(fname),dic,subsys,target);
} // printTabFieldDic(ZFieldSet fs, String fname,ZDictionary dic)

public void printTabFieldRefView(ZFieldSet fs, String fname,
	RList dic,String subsys, String target)
{ printTabFieldRefView(fs.get(fname),dic,subsys,target); }

public void printTabFieldZOID(ZFieldSet fs, String fname)
{printTabFieldZOID(fs,fname,WAMessages.SUBSYS_TIS,null);}

public void printTabFieldZOID(ZFieldSet fs, String fname, String subsys)
{printTabFieldZOID(fs,fname,subsys,null);}

public void printTabFieldZOID(ZFieldSet fs, String fname,
	String subsys, String target)
{
 if(subsys== null) subsys = wam.SUBSYS_TIS;
 printTabFieldRefView(fs.get(fname),null,subsys,target); 
}

public void printTabFieldZOID(String caption, Long ZOID)
{printTabFieldZOID(caption,ZOID,null,null);}

public void printTabFieldZOID(String caption, Long ZOID,
	String subsys, String target)
{
 if(subsys== null) subsys = wam.SUBSYS_TIS;
 printTabFieldRefView(new ZField(caption,o2s(ZOID)),
 null,subsys,target); 
}

// SIC! rewrite it to real description
public void printTabFieldZOID2Descr(ZFieldSet fs, String fname, RList dicZN, String subsys)
{ printTabFieldZOID2ZNAME(fs.get(fname),dicZN, subsys); }
public void printTabFieldZOID2ZNAME(ZFieldSet fs, String fname, RList dicZN, String subsys)
{ printTabFieldZOID2ZNAME(fs.get(fname),dicZN, subsys); }

public void printTabFieldZOID2ZNAME(ZField f,RList dicZN, String subsys)
{
 String fcaption=null, fvalue=null;
 if(f != null ){
  fcaption = mkFCaption(f); fvalue = mkHRefView(f,dicZN,subsys,null);
  if (dicZN != null){
  ZName ZN = (ZName)dicZN.getByID(wam.string2Long(f.getValue()));
  if(ZN == null) ZN = new ZName();
  fcaption = mkFCaption(f); fvalue = mkHRefView(f,dicZN,subsys,ZN.ZTYPE);
  } //dicZN!= null
 } //f!=null
 printTabFieldRaw(fcaption,fvalue);
} // printTabFieldZOID2ZNAME(ZField f,RList dicZN, String subsys)

public void printTabFieldHRef(String fname, String fvalue,
	String subsys, String request,
	String[] params, String[] values)
{
 beginTableRow();
  beginTableCell();
   w("<b>"); print(fname); w("</b>");
  closeTableCell();
  printCellHRef(fvalue,subsys,request,params,values);
 closeTableRow();
} // printTabField(String fname, String fvalue)

public void printCellValueRaw(String value)
{
 beginTableCell(); w(value); closeTableCell();
} // printCellValue()

public void printCellValue(String value)
{ printCellValueRaw(t2h(value)); }
public void printCellValueCustomPSQL(String caption,String SQL)
{
  printCellValueRaw(mkHRef(caption,wam.SUBSYS_PSQL,null,new String[]{wam.PARAM_SQLREQUEST},new String[]{SQL}));
}

public void printCellField(ZField f)
{ printCellValue(mkFValue(f)); }

public void printCellFieldDic(ZField f, ZDictionary dic)
{ printCellValue(mkFCodeValue(f,dic)); }

public void printCellFieldRefView(ZField f, RList dic, String subsys, String target)
{
 String fvalue=t2h(null);
 if(f != null){
  fvalue = mkHRefView(f,dic,subsys,target);
 } //f!=null
 printCellValueRaw(fvalue);
}

public void printCellZOID(Long ZOID) {printCellZOID(null,null,ZOID);}

public void printCellZOID(String subsys, String ZTYPE, Long ZOID) {
 if(subsys== null) subsys = wam.SUBSYS_TIS;
 printCellValueRaw(mkHRefView(o2s(ZOID),subsys,ZTYPE,ZOID));
}

public void printCellZUID(Long ZID) {
 printCellValueRaw(mkHRefView(o2s(ZID),wam.SUBSYS_SAM,wam.TARGET_SAMUSER,ZID));
}

public void printCellZSID(Long ZID) {
 printCellValueRaw(mkHRefView(o2s(ZID),wam.SUBSYS_SAM,wam.TARGET_SAMSCOPE,ZID));
}

public void printCellHRefView(String caption, String subsys, String target, Long id)
{
printCellValueRaw(mkHRefView(caption,subsys,target,id));
}

public void printCellHRef(String caption,
	String subsys, String request,
	String[] params, String[] values)
{
 beginTableCell();
  w(mkHRef(caption,subsys,request,params,values));
 closeTableCell();
} // printCellHRef()

public void printHRef(String caption,
	String subsys, String request,
	String[] params, String[] values)
{
  w(mkHRef(caption,subsys,request,params,values));
} // printHRef()

public void printHCellValueRaw(String value)
{
 beginTableHCell(); w(value); closeTableHCell();
} // printCellValue()

public void printHCellValue(String value)
{ printHCellValueRaw(t2h(value)); }

public void printHCellCaption(ZField f)
{ printHCellValueRaw(t2h(mkFCaption(f))); }

 // Menu creation primitives

public void beginHMenu(){wln("<div class='ctHMenu'><!-- HMenu -->");}
public void closeHMenu(){wln("<br></div><!-- /HMenu -->");}
public void printHMenuSeparator(){printHMenuSeparator(null);}
public void printHMenuSeparator(String caption)
 {if(caption==null)w("&nbsp;&nbsp;");
  else { print(caption); w("&nbsp;");} }
public void printHMenuCaption(String caption)
 {w("<b>");print(caption);w("</b>");}
public void printHMenuItem(int menu_id)
{ w(mkMenuItemHRef(menu_id)); w("&nbsp;"); }
public void printHMenuAction(int menu_id, String subsys, String target, Long id, Long ZVER)
{ w(mkMenuActionHRef(menu_id,subsys,target,id,ZVER)); w("&nbsp;"); }

public void beginTMenu(){beginTree();}
public void closeTMenu(){closeTree();}
public void printTMenuSeparator(){printTMenuSeparator(null);}
public void printTMenuSeparator(String caption)
{
 beginNode();
 if(caption == null) w("<hr width='10%' align='left'>");
 else w(t2h(caption));
 closeNode();
}
public void printTMenuItem(int menu_id)
{ beginNode();  w(mkMenuItemDescHRef(menu_id)); closeNode(); }
public void printTMenuItemCustomPSQL(String caption,String SQL)
{
  beginNode();
  w(mkHRef(caption,wam.SUBSYS_PSQL,null,new String[]{wam.PARAM_SQLREQUEST},new String[]{SQL}));
  closeNode();
}
public void printTMenuCurrent(int menu_id)
{ beginNode(); w("<b>");  w(wam.getMnuDesc(menu_id)); w("</b>"); closeNode(); }

 //
 // Make data/values/refernces/menus representation 
 //

 // visual data formatting

public String mkFValue(ZField f)
{
 if(f==null) return(null); return(f.getValue());
}  

/** Construct Caption for some external use (without html formatting). */
public String getFCaption(ZField f) { return(mkFCaption(f)); }
public String getFCaption(ZFieldSet fs, String name)
{ return(getFCaption(fs.get(name))); }

private String mkFCaption(ZField f)
{
 if(f==null) return(null);
 String caption=f.getFCaption();
 if(caption == null) {
  caption = f.getName();
  if(getFieldDic() != null){
   DCode dc = getFieldDic().getByCode(f.getCode());
   if(dc != null){ caption = dc.getValue(); }
 } }
 return(caption);
} // mkFCaption(ZField f)
private String mkFDescr(ZField f)
{
 if(f==null) return(null);
 String caption= null; //f.getFDescr(); //SIC! not implemented yet
 if(caption == null) {
  caption = f.getName();
  if(getFieldDic() != null){
   DCode dc = getFieldDic().getByCode(f.getCode());
   if(dc != null){ caption = dc.getDescription(); }
 } }
 return(caption);
} // mkFCaption(ZField f)

/** Construct CodeValue for some external use (without html formatting). */
public String getFCodeValue(ZField f,ZDictionary dic)
  { return(mkFCodeValue(f,dic)); }
public String getFCodeValue(ZFieldSet fs, String name,ZDictionary dic)
  { return(getFCodeValue(fs.get(name),dic)); }

public String mkFCodeValue(ZField f, ZDictionary dic)
{
 if(f==null) return(null);
 String value = f.getValue();
 if(dic!=null){
  DCode dc = dic.getByCode(value);
  if(dc != null) {
   value = dc.getCaption();
   if(value == null) value = f.getValue();
  }
 }
 return(value);
} // mkFCaption(ZField f)

 // input fields construction

public String mkFieldInputHidden(String fname, String fvalue)
{
 StringBuffer sb = new StringBuffer();
 sb.append("<input type='hidden'");
 sb.append(" name='"); sb.append(mkFName(fname)); sb.append("'");
 sb.append(" value='"); sb.append(encodeFieldValue(fvalue)); sb.append("'");
 sb.append(">");
 return(sb.toString());
} // mkFieldInputHidden(String fname, String fvalue)

public String mkFieldInputCheckbox(String fname, boolean fvalue)
{
 StringBuffer sb = new StringBuffer();
 sb.append("<input type='checkbox'");
 sb.append(" name='"); sb.append(mkFName(fname)); sb.append("'");
 sb.append(" value='Y'");
 if(fvalue) sb.append(" CHECKED ");
 sb.append(">");
 return(sb.toString());
} // mkFieldInputHidden(String fname, String fvalue)

public String mkFieldInputPassword(String fname, String fvalue, int size, int maxlength)
{ // SIC! it's copy-paste of mkFieldInputText. need to think about join them
 StringBuffer sb = new StringBuffer();
 if(size<=0 || size>72) size=72;
 if(maxlength<size && maxlength > 0) size=maxlength;
 if(maxlength<4 && maxlength>0) maxlength=4; // this is for SQL NULL encoding as null 
 if(maxlength<6 && maxlength>=4) maxlength=6; // ... for encoding null as "null"
 sb.append("<input type='password'");
 sb.append(" size='" + size + "'");
 if(maxlength > 0) sb.append(" maxmaxlength='" + maxlength + "'");
 sb.append(" name='"); sb.append(mkFName(fname)); sb.append("'");
 sb.append(" value='"); sb.append(encodeFieldValue(fvalue)); sb.append("'");
 sb.append(">");
 return(sb.toString());
} // mkFieldInputPassword

public String mkFieldInputText(String fname, String fvalue, int size, int maxlength)
{
 StringBuffer sb = new StringBuffer();
 if(size<=0 || size>72) size=72;
 if(maxlength<size && maxlength > 0) size=maxlength;
 if(maxlength<4 && maxlength>0) maxlength=4; // this is for SQL NULL encoding as null 
 if(maxlength<6 && maxlength>=4) maxlength=6; // ... for encoding null as "null"
 sb.append("<input type='text'");
 sb.append(" size='" + size + "'");
 if(maxlength > 0) sb.append(" maxmaxlength='" + maxlength + "'");
 sb.append(" name='"); sb.append(mkFName(fname)); sb.append("'");
 sb.append(" value='"); sb.append(encodeFieldValue(fvalue)); sb.append("'");
 sb.append(">");
 return(sb.toString());
} // mkFieldInputText(String fname, String fvalue, int size, int maxlength)

public String mkFieldInputArrayDic(String fname, Object fvalue, Object[] dic_values, Object[] dic_captions)
{
 StringBuffer sb = new StringBuffer();
 sb.append("<select name='"); sb.append(mkFName(fname)); sb.append("'>\n");
 int i;
 boolean value_found=false;
 for(i=0;i<dic_values.length;i++)
 {
  Object dc = dic_values[i];
  sb.append("<option value='");
  sb.append(encodeFieldValue(dc));
  sb.append("'");
  if(fvalue != null) if(fvalue.equals(dc)) {
   value_found=true; sb.append(" selected");
  }
  sb.append(">");
  sb.append(t2h(o2s(dic_captions[i])));
  sb.append("</option>\n");
 }
 if(!value_found){
  sb.append("<option value='");
  sb.append(encodeFieldValue(fvalue));
  sb.append("' selected>");
  sb.append(t2h(o2s(fvalue)));
  sb.append("</option>\n");
 }
 sb.append("</select>");
 return(sb.toString());
} // mkFieldInputDic(String fname, String fvalue, ZDictionary dic, int null_msg)

public String mkFieldInputDic(String fname, String fvalue, ZDictionary dic, int null_msg)
{
 StringBuffer sb = new StringBuffer();
 sb.append("<select name='"); sb.append(mkFName(fname)); sb.append("'>\n");
 int i;
 boolean value_found=false;
 if(null_msg != 0){
  sb.append("<option value='");
  sb.append(encodeFieldValue(null));
  if(fvalue==null) { sb.append("' selected>"); value_found=true; }
  else sb.append("'>");
  sb.append(o2t(wam.getMsg(null_msg)));
  sb.append("</option>\n");
 }
 for(i=0;i<dic.size();i++)
 {
  DCode dc = dic.get(i);
  sb.append("<option value='");
  sb.append(encodeFieldValue(dc.getCode()));
  sb.append("'");
  if(fvalue != null) if(fvalue.equals(dc.getCode())) {
   value_found=true; sb.append(" selected");
  }
  sb.append(">");
  sb.append(t2h(dc.getCaption()));
  sb.append("</option>\n");
 }
 if(!value_found){
  sb.append("<option value='");
  sb.append(encodeFieldValue(fvalue));
  sb.append("' selected>");
  sb.append(t2h(fvalue));
  sb.append("</option>\n");
 }
 sb.append("</select>");
 return(sb.toString());
} // mkFieldInputDic(String fname, String fvalue, ZDictionary dic, int null_msg)

public String mkFieldInputList(String fname, Long fvalue, RList dic, int null_msg)
{
 StringBuffer sb = new StringBuffer();
 sb.append("<select name='"); sb.append(mkFName(fname)); sb.append("'>\n");
 int i;
 boolean value_found=false;
 if(null_msg != 0){
  sb.append("<option value='");
  sb.append(encodeFieldValue(null));
  if(fvalue==null) { sb.append("' selected>"); value_found=true; }
  else sb.append("'>");
  sb.append(o2t(wam.getMsg(null_msg)));
  sb.append("</option>\n");
 }
 for(i=0;i<dic.size();i++)
 {
  Record dc = dic.get(i);
  Long dc_id = dc.getID();
  sb.append("<option value='");
  sb.append(encodeFieldValue(dc_id));
  sb.append("'");
  if(fvalue != null) if(fvalue.equals(dc_id)) {
   value_found=true; sb.append(" selected");
  }
  sb.append(">");
  sb.append(t2h(dc.getCaption()));
  sb.append("</option>\n");
 }
 if(!value_found){
  sb.append("<option value='");
  sb.append(encodeFieldValue(fvalue));
  sb.append("' selected><i>");
  if(fvalue!=null) { sb.append("ZID:"); sb.append(t2h(o2s(fvalue))); }
  else sb.append(t2h(o2s(fvalue)));
  sb.append("</i></option>\n");
 }
 sb.append("</select>");
 return(sb.toString());
} // mkFieldInputList(String fname, String fvalue, RList dic)

 // menu construction

public String mkMenuItemHRef(int menu_id)
{
return(mkMenuActionHRef(menu_id,null,null,null,null));
}

public String mkMenuActionHRef(int menu_id, String subsys, String target, Long id, Long ZVER)
{
 String mnu = wam.getMnuCaption(menu_id);
 String desc = wam.getMnuDesc(menu_id);
 if(subsys == null) subsys = wam.getMnuSubsys(menu_id);
 String request = wam.getMnuRequest(menu_id);
 if(target == null) target = wam.getMnuTarget(menu_id);
 StringBuffer sb = new StringBuffer();
 sb.append("<a href='");
 sb.append(mkURLRequest(subsys, request, target));
 if(id!=null) {sb.append("&"); sb.append(mkURLParameter(wam.PARAM_ZID, o2t(id)));}
 if(ZVER!=null) {sb.append("&"); sb.append(mkURLParameter(wam.PARAM_ZVER, o2t(ZVER)));}
 sb.append("' title='");
 sb.append(o2v(desc));
 sb.append("'>");
 sb.append(t2h("<" + mnu + ">"));
 sb.append("</a>");
 return(sb.toString());
}

public String mkMenuItemDescHRef(int menu_id)
{
 String mnu = wam.getMnuCaption(menu_id);
 String desc = wam.getMnuDesc(menu_id);
 String subsys = wam.getMnuSubsys(menu_id);
 String request = wam.getMnuRequest(menu_id);
 String target = wam.getMnuTarget(menu_id);
 StringBuffer sb = new StringBuffer();
 sb.append("<a href='");
 sb.append(mkURLRequest(subsys, request, target));
 sb.append("'>");
 // sb.append(t2h("<" + mnu + "> - "));
 sb.append(t2h(desc));
 sb.append("</a>");
 return(sb.toString());
}
 // HRef construction

public String mkHRef4URL(String caption, String URL)
{
 StringBuffer sb = new StringBuffer();
 sb.append("<a href='");
 sb.append(URL);
 sb.append("'>");
 sb.append(t2h(caption));
 sb.append("</a>");
 return(sb.toString());
}

/** make html hyperlink to specified subsystem's request with passed params/values.
 * caption will be converted to html.
 */
public String mkHRef(String caption,String subsys, String request, String[] params, String[] values)
{return(mkHRefRaw(t2h(caption),subsys,request,params,values));}

/** the same as mkHRef(), but passed caption is html code. */
public String mkHRefRaw(String caption,String subsys, String request, String[] params, String[] values)
{
 StringBuffer sb = new StringBuffer();
 sb.append("<a href='");
 sb.append(mkURLRequest(subsys, request));
 if(params != null){sb.append("&"); sb.append(mkURLParams(params, values));}
 sb.append("'>");
 if(caption==null)caption=t2h(caption);
 sb.append(caption);
 sb.append("</a>");
 return(sb.toString());
}

public String mkHRefTargetZID(String caption,String subsys, String request,
		String target, Long ZID, String[] params, String[] values)
{
 String[] params2,values2;
 StringBuffer sb = new StringBuffer();
 int num_of_params=2,i,k;
 if(params!=null) num_of_params=num_of_params+params.length;
 params2 = new String[num_of_params];
 values2 = new String[num_of_params];
 params2[0]=wam.PARAM_TARGET; values2[0]=target;
 params2[1]=wam.PARAM_ZID; values2[1]=o2t(ZID);
 for(i=2;i<num_of_params;i++)
 {k=i-2;params2[i]=params[k];values2[i]=values[k];}
 if(caption==null){
 sb.append("<i><u>");
 if(target!=null) sb.append(t2h(target));
 else sb.append("?");
 sb.append("</u></i>");
 caption="<i>:" + t2h(o2t(ZID)) + "</i>";
 }
 else{caption=t2h(caption);}
 if(caption.length()==0) caption="&nbsp;";
 sb.append(mkHRefRaw(caption,subsys,request,params2,values2));
 return(sb.toString());
} // mkHRefTargetZID

public String mkHRefView(ZField f, RList dic, String subsys, String target)
{
 if(f == null) return(t2h(null)); String caption = f.getValue();
 Long id=null; try{id=Long.valueOf(caption);} catch(NumberFormatException e){}
 if(id==null) return(t2h(null));
 if(dic != null){
  Record r = dic.getByID(id);
  if(r != null) caption = (r.getCaption()!=null)?r.getCaption():caption;
 }
 return(mkHRefView(caption,subsys,target,id));
} // mkHRefView(ZField f, RList dic, String subsys, String target)

public String mkHRefView(String caption,String subsys, String target, Long id)
{
 return(mkHRefTargetZID(caption,subsys,wam.REQUEST_VIEW,target,id,null,null));
// return(mkHRef(caption,subsys,wam.REQUEST_VIEW,
//		new String[]{wam.PARAM_TARGET,wam.PARAM_ZID},
//		new String[]{target,o2t(id)}));
} // mkHRefView(String caption,String subsys, String target, Long id)

public String mkHRefEdit(String caption,String subsys, String target, Long id)
{
 return(mkHRefTargetZID(caption,subsys,wam.REQUEST_EDIT,target,id,null,null));
} // mkHRefEdit(String caption,String subsys, String target, Long id)

public String mkHRefEdit(String caption,String subsys, String target, Long ZID, Long ZVER)
{
 return(mkHRefTargetZID(caption,subsys,wam.REQUEST_EDIT,target,ZID,
		new String[]{wam.PARAM_ZVER}, new String[]{o2t(ZVER)}));
} // mkHRefEdit(String caption,String subsys, String target, Long ZID, Long ZVER)

public String mkHRefEditIntermediate(String caption,String subsys, String target, Long ZID, Long ZVER)
{
 return(mkHRefTargetZID(caption,subsys,wam.REQUEST_EDIT,target,ZID,
	new String[]{wam.PARAM_ZVER,wam.PARAM_ZSTA}, new String[]{o2t(ZVER),wam.SZ_ZSTA_I}));
} // mkHRefEditIntermediate(String caption,String subsys, String target, Long ZID, Long ZVER)

 // URL construction

public static String mkURL(String CGI,String[] params, String values[])
{
 if(params == null) return(CGI);
 if(params.length == 0 ) return(CGI);
 StringBuffer sb = new StringBuffer();
 sb.append(CGI);
 sb.append("?");
 sb.append(mkURLParams(params,values));
 return(sb.toString());
} // mkURL(String CGI,String[] params, String values[])

public static String mkURLParameter(String param, String value)
{ return(mkURLParams(new String[]{param}, new String[]{value})); }

public static String mkURLParams(String[] params, String values[])
{
 StringBuffer sb = new StringBuffer();
 int i = 0 ;
 while(i<params.length)
 {
 if(values[i] != null) {
  if(i>0) if(values[i-1] != null) sb.append("&");
  sb.append(o2uv(params[i]));
  sb.append("=");
  sb.append(o2uv(values[i]));
  }
 i++;
 }
 return(sb.toString());
} // mkURLParams(String[] params, String values[])

public static String mkURLParams(ZFieldSet fs)
{
 StringBuffer sb = new StringBuffer();
 WASkin form = new WASkin();
 int i=0;
 form.beginRecord();
 while(i<fs.size())
 {
  ZField f=fs.get(i);
  if(f.getValue() != null) {
  if(sb.length()>0) sb.append("&");
  sb.append(o2uv(form.getCurRecPrefix() + o2v(f.getName()) ));
  sb.append("=");
  sb.append(o2uv(f.getValue()));
  }
 i++;
 }
 form.closeRecord();
 return(sb.toString());
} // mkURLParams(ZFieldSet fs)

public String mkURL(String[] params, String values[])
{
  return(mkURL(getCGI(),params, values));
}

public String mkURLRequest(String subsys, String request)
{
 return( mkURL(new String[]{wam.PARAM_SUBSYS,wam.PARAM_REQUEST},
	new String[]{subsys,request}) );
}

public String mkURLRequest(String subsys, String request, String target)
{
 return( mkURL(new String[]{wam.PARAM_SUBSYS,wam.PARAM_REQUEST,wam.PARAM_TARGET},
	new String[]{subsys,request,target}) );
}
public String mkURLRequest(javax.servlet.http.HttpServletRequest request)
{
 return( mkURL(
  new String[]{wam.PARAM_SUBSYS,wam.PARAM_REQUEST,wam.PARAM_TARGET},
  new String[]{
    request.getParameter(wam.PARAM_SUBSYS),
    request.getParameter(wam.PARAM_REQUEST),
    request.getParameter(wam.PARAM_TARGET)
  }) );
} // mkURLRequest(javax.servlet.http.HttpServletRequest request)

 //
 // Error & Information messages
 //

 /** display informationl message. TISExmlDB.java legacy where have been wrapper to System.out.print */
public void printMsg(String msg)
{wln();w("<b>");print(msg);w("</b>");println();}
public void printMsg(int msg_id) {printMsg(wam.getMsgText(msg_id));}
public void printMsg(int msg_id,String arg1) {printMsg(wam.getMsgText(msg_id,arg1));}
public void printMsg(int msg_id,String arg1,String arg2) {printMsg(wam.getMsgText(msg_id,arg1,arg2));}
public void printMsg(int msg_id,String arg1,String arg2,String arg3)
	{printMsg(wam.getMsgText(msg_id,arg1,arg2,arg3));}
/** warns user about locked state of dobject requested for edition. */
public void printWarnMsgEdit(DObject o) {
  if(o.isIntermediate())
  {
   if(o.isLocked()) printMsg(wam.WARN_OBJECT_LOCKED_BY_SOMEONE);
   else if(o.isMyIntermediate()) { printMsg(wam.WARN_OBJECT_OPENED_BY_YOU);
      println(wam.getMsgText(wam.WARN_SAVING_CHANGES_WILL_FAIL));
      w(wam.getMsgHtml(wam.WARN_FOLLOW_X_2EDIT_OPENED,
       mkHRefEditIntermediate(wam.getMsgText(wam.CAPTION_EDIT_INTERMEDIATE),
	 wam.getQTYPESubsys(o.getZTYPE()),o.getZTYPE(),o.getZOID(),o.getIVersion().ZVER) ));
      println(); return; }
   else printMsg(wam.WARN_OBJECT_OPENED_BY_SOMEONE);
   println(wam.getMsgText(wam.WARN_SAVING_CHANGES_WILL_FAIL));
  }
} //printWarnMsgEdit

public void printWarnMsgEditIntermediate(DObject o) {
if(!o.isMyIntermediate()) printWarnMsgEdit(o);
} //printWarnMsgEditIntermediate

/** warns user about locked state of dobject requested for edition. */
public void printWarnMsgEdit(ZFieldSet fs) {
 String ZSTA=null; try{ ZSTA = Record.getFSString(fs,ZObject.FC_ZSTA); }
 catch(ZException ze){printErrMsg(ze);}
 if(ZSTA == null) return;
 if(!ZSTA.equals(wam.SZ_ZSTA_I)) return;
 println(wam.getMsg(wam.WARN_OBJECT_IS_OPENED4EDITION_AND_LOCKED));
 println(wam.getMsg(wam.WARN_YOU_MUST_COMMIT_OBJECT_MANUALLY));
} //

/** warns user that "no requested object exists for some action". */
public void printWarnMsgObjectNotExists(String target, Long ZOID,int doc_type_caption_id)
{
 if(doc_type_caption_id == 0) doc_type_caption_id = wam.OBJ_UNKNOWN;
 String obj_type_name = wam.getMsgText(doc_type_caption_id);
 printMsg(wam.WARN_REQUESTED_O_NOT_EXISTS,obj_type_name);
 printWarnFolowXToExamineObjects(null,target,ZOID);
} //printWarnMsgObjectNotExists

/** show hyperlinks to explore target+ZOID and o.getZOID() with TIS subsystem. */
public void printWarnFolowXToExamineObjects(DObject o, String target, Long ZOID)
{
 String caption = null; //target + ":"+ o2s(ZOID);
 caption = mkHRefView(caption,wam.SUBSYS_TIS,target,ZOID);
 String caption2 = null;
 if(o!=null) if(o.getZOID() != null) {
  caption2 = o.getCaption();
  //if(caption2 == null) caption2 = o.getZTYPE() + ":"+ o2s(o.getZOID());
  caption2 = mkHRefView(caption2,wam.SUBSYS_TIS,o.getZTYPE(),o.getZOID());
 }
 if(caption2 != null) caption = caption +"," + caption2;
 String msg = wam.getMsgHtml(wam.WARN_FOLLOW_X_TO_EXAMINE_OBJECT, caption);
  w("<b>"); w(msg); wln("</b><br>");
}

/** found dobject's ZOID differ from requested one. */
public void printWarnMsgNoTargetZOID(DObject o,String target, Long ZOID,int doc_type_caption_id)
{
 if(doc_type_caption_id == 0) doc_type_caption_id = wam.OBJ_UNKNOWN;
 String o_t_name = wam.getMsgText(doc_type_caption_id);
 printMsg(wam.WARN_REQUESTED_O_X_HAS_ANOTHER_ZOID_Y,o_t_name,o2s(ZOID),o2s(o.getZOID()));
 printWarnFolowXToExamineObjects(o,target,ZOID);
}

/** found dobject's ZTYPE differ from requested one. */
public void printWarnMsgNoTargetZTYPE(DObject o,String target, Long ZOID,int doc_type_caption_id)
{
 if(doc_type_caption_id == 0) doc_type_caption_id = wam.OBJ_UNKNOWN;
 String o_t_name = wam.getMsgText(doc_type_caption_id);
 printMsg(wam.WARN_REQUESTED_O_X_HAS_ANOTHER_ZTYPE_Y,o_t_name,target,o.getZTYPE());
 printWarnFolowXToExamineObjects(o,target,ZOID);
}

 /** dispaly error message. TISExmlDB.java legacy where have been just a wrapper to System.err.print */
public void printErrMsg(String msg)
  {println();w("<b>");w(wam.getMsg(wam.ERROR));w(": ");print(msg);w("</b><br>");}
public void printErrMsg(String msg, Exception e)
  {
    println();w("<b>");
    print(wam.getMsgText(wam.ERROR));w(": ");
    print(msg);w("</b><i><br>(");
    print(o2t(e));
    w(")</i><br>");
  } // printErrMsg(String msg, Exception e)

public void printErrMsg(Exception e) {
 try{ 
    ZException ze = (ZException)e;
    if(ze.getErrnum() == null)
      printErrMsg(e.getMessage());
    else
      printErrMsg( e.getMessage() +
       " (" + o2s(ze.getErrcode()) + ":"
       + o2s(ze.getErrnum()) +  ")" );
    return;
    }catch(ClassCastException cce){}
 try{ // is it from this application ?
  WAException wae = (WAException)e;
  printErrMsg(e.getMessage());
 }
 catch(ClassCastException cce){
  printErrMsg(e.getMessage(),e);
  java.io.StringWriter swr = new java.io.StringWriter();
  java.io.PrintWriter pwr=new java.io.PrintWriter(swr);
  e.printStackTrace(pwr); //SIC! move js code to WAMessages?
  w("<a href='#' onclick='{if(ex_stack.style.display==\"none\"){ex_stack.style.display=\"block\";}else{ex_stack.style.display=\"none\";}return(false);}'>...</a>");
  w("<div id='ex_stack' style='display:none' ><i>");
  w("<i>");
    print(o2t(swr));
  w(")</i></div>");
  }
 } // printErrMsg(Exception e)

public void printExecStatus(ZExecStatus es) {
beginTable();
 beginTableRow();
  printHCellValue(es.F_ZID);
  printHCellValue(es.F_ZVER);
  printHCellValue(es.F_ERRDESC);
 closeTableRow();
 beginTableRow();
  printCellValueRaw(mkHRefView(o2s(es.ZID),wam.SUBSYS_TIS,null,es.ZID));
  printCellValue(o2s(es.ZVER));
  printCellValue(es.errdesc + "(" + o2s(es.errcode)
	+ ": " + o2s(es.errnum) + ")");
 closeTableRow();
closeTable();

} //printExecStatus

 // Question messages
 /** ask user about object deletion.
  *
  * @param obj_type_msg_id - id of message string with object's type name
  * @param r - record with this object's header for getCaption() call
  *
  * @see #printButtonMenuBarYesNo
  */
public void printQuestDelete(int obj_type_msg_id,
	Record r,String subsys, String target, Long id)
 {
  String obj_type_name = wam.getMsgText(obj_type_msg_id);
  String caption = null;
  if(r != null) caption=r.getCaption();
  //if(caption==null) caption = target + ":" + o2t(id);
  caption = mkHRefView(caption,subsys,target,id);
  String msg = wam.getMsgHtml(wam.QUEST_DELETE_OBJECT_X_NAMED_Y,
	t2h(obj_type_name),caption);
  w("<b>"); w(msg); wln("</b><br>-------------------------<br>");
  printButtonMenuBarYesNo(subsys, wam.REQUEST_DELETE, target, id);
 } // printQuestDelete()

public void printQuestDeleteDObject(int obj_type_msg_id,
	DObject o,String subsys, String target, Long ZOID)
 {
  if(o!=null) if(o.getZTYPE()==null) o=null;
  printQuestDelete(obj_type_msg_id,o,subsys,target,ZOID);
 } // printQuestDelete()

 //
 // complex document construction primitives
 // fields, inputs, buttons and so on.
 //

public void beginFormPost(String subsys, String req)
 {beginFormPost(subsys,req,null);}

public void beginFormPost(String subsys, String req, String target)
 {beginFormPost(subsys,req,target,null);}
 
public void beginFormPost(String subsys, String req, String target,Long id)
{
 w("<form method='POST' action='");
 w(mkURLRequest(subsys, req, target));
 wln("'>");
 if(id!=null)printFieldInputHidden(wam.PARAM_ZID,id);
}

public void beginFormGet()
{
 w("<form method='GET' action='");
 w(mkURLRequest(null, null, null));
 wln("'>");
}

public void beginFormGet(String subsys, String req, String target)
 { beginFormGet(subsys,req,target,null); }

public void beginFormGet(String subsys, String req, String target, Long id)
{
 beginFormGet();
 if(subsys!=null)printFieldInputHidden(wam.PARAM_SUBSYS,subsys);
 if(req!=null)printFieldInputHidden(wam.PARAM_REQUEST,req);
 if(target!=null)printFieldInputHidden(wam.PARAM_TARGET,target);
 if(id!=null)printFieldInputHidden(wam.PARAM_ZID,id);
} // beginFormGet

public void closeForm() {wln("</form>"); }

public void sendRedirect(
	javax.servlet.http.HttpServletResponse response, 
	String subsys, String req, String target)
{ sendRedirect(response,subsys,req,target,null); }
public void sendRedirect(
	javax.servlet.http.HttpServletResponse response, 
	String subsys, String req, String target,Long id)
{
 StringBuffer sbURL = new StringBuffer();
 sbURL.append(mkURLRequest(subsys, req, target));
 if(id != null) { sbURL.append("&");
 sbURL.append(mkURLParameter(wam.PARAM_ZID,o2s(id))); }
 sendHTTPRedirect(response,sbURL.toString());
}

public void printInputTextarea(String name, int rows, int cols, String value)
{
 w("<textarea name='");
 w(mkFName(name));
 w("' rows='");
 w("" + rows);
 w("' cols='");
 w("" + cols);
 w("' >");
 w(encodeFieldValue(value));
 wln("</textarea>");
}

public void printTabFieldInputTextarea(String fcaption, String fname, String value)
{
 int rows = 4;
 int cols = 72;
 printTabFieldInputTextarea(fcaption,fname,value,rows,cols);
}
public void printTabFieldInputTextarea(String fcaption, String fname, String value, int rows, int cols)
{
 w("<tr><td colspan='3'><b>");
 w(t2h(fcaption));
 w("</b>");
 w("<div id='div_"); // for future js manipulation
 w(mkFName(fname));
 wln("'>");
 w("<textarea name='");
 w(mkFName(fname));
 w("' rows='");
 w("" + rows);
 w("' cols='");
 w("" + cols);
 w("' >");
 w(encodeFieldValue(value));
 wln("</textarea>");
 wln("</div>");
 w("</td></tr>");
}

/** print submit button action primitive.
* @param name  for input field identification
* @param caption_id WAMessage.getMsg(caption_id) for visual caption
* @see WAMessage#getMsg
*/
public void printButton(String name,int caption_id)
{ printButton(name,wam.getMsgText(caption_id)); }

public void printButtonWithAffix(String name,int caption_id, int affix_id)
{ printButton(name,wam.getMsgText(caption_id) + wam.getMsgText(affix_id)); }

/** print submit button action primitive.
* @param name  for input field identification
* @param caption text for visual caption
*/
public void printButton(String name, String caption) {
 w("<input type='submit' ");
 if(name != null) w("name='"); w(o2v(name)); w("' ");
 if(caption != null){ w("value='");
  w(o2v(caption)); w("' ");}
 wln(">");
}
/** true if submit command button with button_name was pressed. */
public boolean isButtonPressed(
 javax.servlet.http.HttpServletRequest request, String button_name)
{ return(request.getParameter(button_name) != null); }

public void printButtonAddRecord(String rec_code) {
 printButton(mkFName(RECORD_ADDNEW)+"="+o2v(rec_code),
  wam.getMsgText(wam.ADD_RECORD_X,rec_code) );
 }
public void printButtonAddRecord(String rec_code, String caption) {
 printButton(mkFName(RECORD_ADDNEW), o2v(caption) );
 }
public void printButtonEdit() {
 printButton(wam.PARAM_BTN_EDIT, wam.EDIT); }
public void printButtonEditBasic() {
 printButtonWithAffix(wam.PARAM_BTN_EDIT_BASIC, wam.EDIT, wam.AFFIX_BASIC); }
public void printButtonEditNormal() {
 printButtonWithAffix(wam.PARAM_BTN_EDIT, wam.EDIT, wam.AFFIX_NORMAL); }
public void printButtonDelete() {
 printButton(wam.PARAM_BTN_DELETE, wam.DELETE); }
public void printButtonExecute() {
 printButton(wam.PARAM_BTN_EXEC, wam.EXECUTE); }
public void printButtonExec(int caption_id) {
 printButton(wam.PARAM_BTN_EXEC, caption_id); }
public void printButtonExec(String caption) {
 printButton(wam.PARAM_BTN_EXEC, caption); }
public void printButtonRefresh() {
 printButton(wam.PARAM_BTN_REFRESH, wam.REFRESH); }
public void printButtonSave() {
 printButton(wam.PARAM_BTN_SAVE, wam.SAVE); }
public void printButtonCancel() {
 printButton(wam.PARAM_BTN_CANCEL, wam.CANCEL); }
public void printButtonCreate() {
 printButton(wam.PARAM_BTN_CREATE, wam.CREATE); }
public void printButtonYes() {
 printButton(wam.PARAM_BTN_YES, wam.YES); }
public void printButtonNo() {
 printButton(wam.PARAM_BTN_NO, wam.NO); }
public void printButtonGrant() {
 printButton(wam.PARAM_BTN_GRANT, wam.GRANT); }
public void printButtonRevoke() {
 printButton(wam.PARAM_BTN_REVOKE, wam.REVOKE); }
public void printButtonSet() {
 printButton(wam.PARAM_BTN_SET, wam.SET); }
public void printButtonShow() {
 printButton(wam.PARAM_BTN_SHOW, wam.SHOW); }
public void printButtonFind() {
 printButton(wam.PARAM_BTN_FIND, wam.FIND); }
public void printButtonNext() {
 printButton(wam.PARAM_BTN_NEXT, wam.NEXT); }
public void printButtonCompare() {
 printButton(wam.PARAM_BTN_COMPARE, wam.COMPARE); }
public void printButtonView() {
 printButton(wam.PARAM_BTN_VIEW, wam.VIEW); }
public void printButtonViewBasic() {
 printButtonWithAffix(wam.PARAM_BTN_VIEW_BASIC, wam.VIEW, wam.AFFIX_BASIC); }
public void printButtonViewNormal() {
 printButtonWithAffix(wam.PARAM_BTN_VIEW, wam.VIEW, wam.AFFIX_NORMAL); }
public void printButtonManage() {
 printButton(wam.PARAM_BTN_MANAGE, wam.MANAGE); }
public void printButtonMetadata() {
 printButton(wam.PARAM_BTN_METADATA, wam.METADATA); }

public void printButtonMenuBarList() {
 printButtonShow();
 w("&nbsp;&nbsp;&nbsp;");
 printButtonNext();
 w("&nbsp;&nbsp;&nbsp;");
 printButtonCancel();
 wln();
} // printButtonMenuBarList()

public void printButtonMenuBarCreate() {
 printButtonCreate();
 w("&nbsp;&nbsp;&nbsp;");
 printButtonCancel();
 wln();
} // printButtonMenuBarCreate()

public void printButtonMenuBarExec(int caption_id) {
 printButtonExec(caption_id); w("&nbsp;&nbsp;&nbsp;");
 printButtonCancel(); wln();
} // printButtonMenuBarExec()

public void printButtonMenuBarExec(String caption) {
 printButtonExec(caption); w("&nbsp;&nbsp;&nbsp;");
 printButtonCancel(); wln();
} // printButtonMenuBarExec()

public void printButtonMenuBarFind() {
 printButtonFind();
 w("&nbsp;&nbsp;&nbsp;");
 printButtonNext();
 w("&nbsp;&nbsp;&nbsp;");
 printButtonCancel();
 wln();
} // printButtonMenuBarFind()

public void printButtonMenuBarEdit() {
 printButtonSave();
 w("&nbsp;&nbsp;&nbsp;");
 printButtonRefresh();
 w("&nbsp;&nbsp;&nbsp;");
 printButtonEditBasic();
 w("&nbsp;&nbsp;&nbsp;");
 printButtonCancel();
 wln();
} // printButtonMenuBarEdit()

public void printButtonMenuBarEditBasic() {
 printButtonSave();
 w("&nbsp;&nbsp;&nbsp;");
 printButtonRefresh();
 w("&nbsp;&nbsp;&nbsp;");
 printButtonEditNormal();
 w("&nbsp;&nbsp;&nbsp;");
 printButtonCancel();
 wln();
} // printButtonMenuBarEdit()

public void printButtonMenuBarYesNo(String subsys, String req, String target, Long id) {
 beginFormGet(subsys,req,target,id);
 printButtonYes();
 w("&nbsp;&nbsp;&nbsp;");
 printButtonNo();
 closeForm();
} // printButtonMenuBarYesNo()

public void printButtonMenuBarView(String subsys, String req, String target, Long id) {
 beginFormGet(subsys,req,target,id);
 printButtonEdit();
 w("&nbsp;&nbsp;&nbsp;");
 printButtonDelete();
 closeForm();
} // printButtonMenuBarView()

public void printMenuBarViewDObjectTIS(String ZTYPE, Long ZOID) {
 beginFormGet(wam.SUBSYS_TIS,wam.REQUEST_BTNMENU,ZTYPE,ZOID);
 printButtonEdit();
 w("&nbsp;&nbsp;&nbsp;");
 printButtonView();
 w("&nbsp;&nbsp;&nbsp;");
 printButtonDelete();
 closeForm();
}

public void printMenuBarViewDObjectTISCBasic(String ZTYPE, Long ZOID) {
 beginFormGet(wam.getQTYPESubsys(ZTYPE),wam.REQUEST_BTNMENU,ZTYPE,ZOID);
 printButtonEdit();
 w("&nbsp;&nbsp;&nbsp;");
 printButtonMetadata();
 w("&nbsp;&nbsp;&nbsp;");
 printButtonViewNormal();
 w("&nbsp;&nbsp;&nbsp;");
 printButtonDelete();
 closeForm();
}

public void printMenuBarViewDObjectTISC(String ZTYPE, Long ZOID) {
 beginFormGet(wam.getQTYPESubsys(ZTYPE),wam.REQUEST_BTNMENU,ZTYPE,ZOID);
 printButtonEdit();
 w("&nbsp;&nbsp;&nbsp;");
 printButtonMetadata();
 w("&nbsp;&nbsp;&nbsp;");
 printButtonViewBasic();
 w("&nbsp;&nbsp;&nbsp;");
 printButtonDelete();
 closeForm();
}

public void printHRefRequest(String text, String subsys, String request)
{
 w("<a href='");
 w(mkURLRequest(subsys, request));
 w("'>");
 print(text);
 wln("</a>");
}

public void printHRefView(String caption, String subsys, String target, Long id)
{
 w(mkHRefView(caption,subsys, target, id));
}

// extract data from request's parameters

/** extract all records as ZFieldSet.
 * @see @beginRecord
 * @see @closeRecord
 */
public ZFieldSet extractFieldSet(javax.servlet.http.HttpServletRequest request)
{
 ZFieldSet fs = new ZFieldSet();
 Enumeration e = request.getParameterNames();
 while(e.hasMoreElements())
 {
  String name = (String)e.nextElement(); // full name (__REC.1.2...fname)
  String value = request.getParameter(name);
  String fname = null; // field/final name
  String fvalue = null;
  // split name into parts
  ZFieldSet fs2 = fs, fs3;
  StringTokenizer st = new StringTokenizer(name,".");
  if(!st.hasMoreTokens()) continue;
  if(!RECORD_PREFIX.equals(st.nextToken())) continue;
  while (st.hasMoreTokens())
  {
   String key=st.nextToken();
   if(!st.hasMoreTokens()) { fname = key; break; } // final name found
   fs3 = fs2.getFS(key); //find next fs by it's key
   if( fs3 == null){ fs3=new ZFieldSet(); fs3.setKey(key); fs2.add(fs3); }
   fs2=fs3;
  } // while (st.hasMoreTokens())
  fvalue = decodeFieldValue(value);
  if(RECORD_CODE.equals(fname)) fs2.setCode(fvalue);
  else if(fname.startsWith(RECORD_ADDNEW))
   {
    if( fname.length()>RECORD_ADDNEW.length() )
	fvalue=fname.substring(RECORD_ADDNEW.length()+1);
    fs3=new ZFieldSet();fs3.setCode(fvalue); fs2.add(fs3);
   }
  else if(RECORD_DELETE.equals(fname)){ fs2.setMarkedForDeletion(true); }
  else fs2.add(new ZField(fname,fvalue));
 } // while(e.hasMoreElements())
 fs.sortChildrenFSRecursively();
 return(fs);
} // extractFieldSet(request)


/** this string value (null) could be used to encode SQL NULL into String." */
public static final String SZ_NULL_AS_SQLNULL="null";
/** this string value ("null") must be used instead of SZ_NULL_AS_SQLNULL." */
public static final String SZ_NULL_AS_STRING="\"null\"";


/** encode value for using it in some kind of input field.
 * this encoding must take into consideration some special cases
 * like SQL NULL values and so on. Works together with decodeFieldValue.
 * null value will be encoded as string null and 4-characters string null
 * will be encoded as 6 characters string "null" (which can't be properly
 * encoded yet).
 *
 * @see #decodeFieldValue()
 */
public String encodeFieldValue(Object value)
{
if(value == null) return(SZ_NULL_AS_SQLNULL);
String v = o2t(value);
if(SZ_NULL_AS_SQLNULL.equals(v)) v = SZ_NULL_AS_STRING;
return(o2v(v));
}

/** decode value come from  some kind of input field.
 * this function must take into consideration some special cases
 * like SQL NULL values and so on. Works together with encodeFieldValue.
 *
 * @see #encodeFieldValue()
 */
public String decodeFieldValue(String value)
{
if(value == null) return(null);
if(SZ_NULL_AS_SQLNULL.equals(value)) return(null);
if(SZ_NULL_AS_STRING.equals(value)) return(SZ_NULL_AS_SQLNULL);
return(wam.translate_tokens(value,new String[]{"\r"},new String[]{""}));
}

// rowset pager subsystem (designed for view huge list of objects/records)

public static final int  MAX_ROWS_PER_PAGE = 5000;
public static final int MIN_ROWS_PER_PAGE = 1;
public static final int DEFAULT_ROWS_PER_PAGE = 50;

/** only these values are possible for avoiding of attacks. */
public static final String[] ROWS_PER_PAGE_OPTION = {"1","2","5","10","50","100","200","500","1000","5000"};

public void printTabFieldRowsPager(ZFieldSet fs)
{
 int rows_per_page = getRowsPerPage(fs);
 long row_offset = getRowOffset(fs);
 printTabFieldInputArrayDic(wam.getMsgText(wam.CAPTION_ROWS_PER_PAGE),
	wam.PARAM_ROWS_PER_PAGE,
	Long.toString(rows_per_page),ROWS_PER_PAGE_OPTION);
 printFieldInputHidden(wam.PARAM_ROW_OFFSET, new Long(row_offset));
} //printTabFieldPager

private final static int PAGES_PER_SEGMENT = 10;

/** print the panel of page selects at the buttom of some list. It's designed
  * for any rowset partal viewing (list/find requests mostly). 
  * subs/request/ZID/target parameters will be taken from request, and all fs
  * parameters will be appended to each resulting GET request.
  */ 
public void printTotalRowsPager(javax.servlet.http.HttpServletRequest request,
	ZFieldSet fs, int rows_shown)
{
 int rows_per_page = getRowsPerPage(fs);
 long row_offset = getRowOffset(fs);
 long segment,seg_size,seg_offset;
 ZFieldSet fs_rowsoffset = new ZFieldSet();
 String szURL=mkURLRequest(request);
 String szRowOffsetBak = fs.getValue(wam.PARAM_ROW_OFFSET); //preserve value
 fs.setValue(wam.PARAM_ROW_OFFSET,null); // delete parameter from fs
 szURL=szURL + "&" + mkURLParameter(wam.PARAM_BTN_REFRESH,"1")
       + "&" + mkURLParams(fs) + "&";
 int i;
 println(wam.getMsgText(wam.ROWS_SHOWN_FROM_TO,Integer.toString(rows_shown),
	Long.toString(row_offset+1),Long.toString(row_offset+rows_per_page)) );
 seg_size=(rows_per_page*PAGES_PER_SEGMENT);
 segment=row_offset/seg_size;
 seg_offset = segment*seg_size;
 if(segment!=0){
  fs_rowsoffset.setValue(wam.PARAM_ROW_OFFSET,
  new Long(seg_offset-rows_per_page));
  //new Long((segment-1)*PAGES_PER_SEGMENT*rows_per_page));
  w(mkHRef4URL("<<...",szURL + mkURLParams(fs_rowsoffset))); w("&nbsp;");
 }
 for(i=0;i<PAGES_PER_SEGMENT;i++)
 {
 long page_offset=seg_offset+i*rows_per_page;
 long page_end=page_offset+rows_per_page;
 long page_no = segment*PAGES_PER_SEGMENT + i + 1;
 if(row_offset>=page_offset && row_offset<page_end){
  w("<b>"); print(Long.toString(page_no));  w("</b>");
  if(rows_shown<rows_per_page) break;
  }
 else{
  fs_rowsoffset.setValue(wam.PARAM_ROW_OFFSET, new Long(page_offset));
  w(mkHRef4URL(Long.toString(page_no),szURL + mkURLParams(fs_rowsoffset)));
 }
 w("&nbsp;");
 }
 if(rows_shown>=rows_per_page) {
  fs_rowsoffset.setValue(wam.PARAM_ROW_OFFSET,
    new Long(seg_offset+rows_per_page*PAGES_PER_SEGMENT));
  w(mkHRef4URL("...>>",szURL + mkURLParams(fs_rowsoffset)));
 }
 fs.setValue(wam.PARAM_ROW_OFFSET,szRowOffsetBak); // restore preserved value
} //printTotalRowsPager

public int getRowsPerPage(ZFieldSet fs)
{
 ZField f = fs.getByName(wam.PARAM_ROWS_PER_PAGE);
 int i=0;
 try{ if(f != null) while(i<ROWS_PER_PAGE_OPTION.length)
      { if(ROWS_PER_PAGE_OPTION[i].equals(f.getValue()))
        return(Integer.parseInt(ROWS_PER_PAGE_OPTION[i])); i++; }
 }catch(NumberFormatException e){} //ignore and use default
 return(DEFAULT_ROWS_PER_PAGE);
} //getRowsPerPage

public long getRowOffset(ZFieldSet fs)
{
 long offset=0;
 ZField f = fs.getByName(wam.PARAM_ROW_OFFSET);
 if(f != null) try {offset=Long.parseLong(f.getValue());}
 catch(NumberFormatException e){} //ignore and use offset=0
 return(offset);
} //getRowOffset

//
// REST
//

public void printRESTInvalidRequest()
{
w("{\"error\":999, \"errormsg\":\"InvalidRequest\"}");
}
public void printRESTNotImplemented()
{
w("{\"error\":9999, \"errormsg\":\"NotImplemented\"}");
}
public void printRESTException(Exception e)
{
w("{\"error\":99999, \"errormsg\":\"Exception\"}");
}
public void printRESTNullObject()
{
w("{\"error\":99, \"errormsg\":\"NullObject\"}");
}
public void printRESTRawString(String s)
{
w(s);
}

//
// AnswerTypes : zFieldSet, DObject, RList, ResultSet, SetOfResultSets, Error, Exception,
// FS,DO,RL,RS,SRS
//
public void beginRESTAnswer(String answer_type)
{
wln("{");
wln(" ");
}
public void closeRESTAnswer()
{wln("}");}

// constructors

public WASkin() {}
public WASkin(java.io.PrintWriter out, WAMessages wam)
{
 setOut(out);
 this.wam=wam;
}

public WASkin(String CGI, java.io.PrintWriter out, WAMessages wam)
{
 setCGI(CGI);
 setOut(out);
 this.wam=wam;
}

} // WASkin
