// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//
// History:
//  2009/12/01 started from WARequestHandler.java,v 1.1
//

//package ru.mave.ConcepTIS.webapps;
package org.eustrosoft.qtis.webapps;

//import ru.mave.ConcepTIS.webapps.json.*;

import java.io.IOException;
import java.sql.*;


/**
 * Ad hoc SQL request submitting and processing tool. This
 * webapp is similar to PostgreSQL psql, ORCALE sqlplus and
 * SQLServer isql utilities but less functional and more specific
 * to ConcepTIS implemenation of TIS-SQL approach.
 *
 * @see WARHMain
 */
public class WARHPSQL extends WARequestHandler {

    //
// Global parameters
//
    private final static String CLASS_VERSION = "$Id$";
    private final static String DEFAULT_SQLREQUEST =
            "select current_database(), inet_server_addr(), inet_client_addr(), version();\n" +
            "select SAM.get_user(),SAM.get_user_login(),SAM.get_user_slevel(),user;\n" +
                    "select 'useful views could be looked at pg_views, tables at pg_tables and so on';\n" +
                    "select schemaname, viewname from pg_views where schemaname in" +
                    "('dic','sam','tis','tisc') order by schemaname";
    private final static String RESOLVE_ID =
            "select id as uid, cast(null as bigint)as sid, cast (null as bigint)" +
                    " as gid, login as name from sam.v_user where id = ?" +
                    " union all select null , id, null, name  from sam.v_scope where id = ?" +
                    " union all select null , null, id, name  from sam.v_group where id = ?";

//
// request processing
//
 public void processREST() { was.printRESTNotImplemented(); }

    public void process() {
        String szSQLRequest = request.getParameter(wam.PARAM_REQUEST);
        //String flagJS = request.getParameter("flagJS");
        if (wam.REQUEST_VIEW.equals(szSQLRequest)) processView();
        //else if (flagJS != null && flagJS.equals("flag")) processSeveralSQLRequestFormJS(); 
        else processSQLRequestForm();
    }

    private void processView() {
        String szZID = request.getParameter(wam.PARAM_ZID);
        long id = 0;
        try {
            id = Long.parseLong(szZID);
        } catch (NumberFormatException ne) {
            return;
        }
        java.sql.Connection dbc = null;
        was.printSeparator(); // <hr>
        was.printTitle(wam.getMsg(wam.CAPTION_ID_MB_ONE_OF_THOSE, Long.toString(id)));
        try {
            dbc = do_db_logon();
        } catch (Exception e) {
            printerrmsg(wam.getMsgText(wam.ERROR_WHILE_JDBC_CONNECTION,
                    request.getRemoteUser()), e);
        }
        try {
            java.sql.PreparedStatement ps = null;
            java.sql.ResultSet rs = null;
            ps = dbc.prepareStatement(RESOLVE_ID);
            ps.setLong(1, id);
            ps.setLong(2, id);
            ps.setLong(3, id);
            rs = ps.executeQuery();
            print_sql_rs(rs);
        } catch (Exception e) {
            printerrmsg(wam.getMsgText(wam.ERROR_WHILE_JDBC_SQL), e);
        } finally {
            do_db_logoff(dbc);
        }
    } //processView()

/*
    private void processSeveralSQLRequestFormJS(){
    	String szSQLRequest = request.getParameter(wam.PARAM_SQLREQUEST);
    	if (SZ_EMPTY.equals(szSQLRequest) || szSQLRequest == null) return;
        int req_processed = 0,counter = 0;
        JSONobj majorObject = new JSONobj();
        try (Connection dbc = do_db_logon();
        	 Statement st = dbc.createStatement()){
            st.execute(szSQLRequest);
            while (true) {
                int rows_updated = st.getUpdateCount();
                if (rows_updated != -1) {
                    st.getMoreResults();
                    req_processed++;
                    continue;
                }
                try (ResultSet rs = st.getResultSet()) {
                    if (rs != null) {
                        JSONbilder jsoNbilder = new JSONbilder(rs);
                        majorObject.put("select №"+String.valueOf(counter++),jsoNbilder.processResultSet());
                    }else break;
                    st.getMoreResults();
                    req_processed++;
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
            out.println(majorObject);	
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void processOneSQLRequestFormJS() {
    	
    	String szSQLRequest = request.getParameter(wam.PARAM_SQLREQUEST);
    	if (SZ_EMPTY.equals(szSQLRequest) || szSQLRequest == null) return;
    	try (Connection dbc = do_db_logon();
    		Statement st = dbc.createStatement();
			ResultSet rs = st.executeQuery(szSQLRequest)){
    		 JSONobj jsoNobj = null;
    	        try {
    	            JSONbilder jsoNbilder = new JSONbilder(rs);
    	            jsoNobj = jsoNbilder.processResultSet();
    	        } catch (JSONException e){
    	            e.printStackTrace();
    	        }
    	        
    	       out.println(jsoNobj);
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
*/
    
    private void processSQLRequestForm() {
        was.beginDocument();
        was.beginFormPost(wam.SUBSYS_PSQL, wam.REQUEST_EXEC);
        was.println("SQL request:");
        // get SQL request from the passed parameters 
        // and display it as <textarea>
        String szSQLRequest = request.getParameter(wam.PARAM_SQLREQUEST);
        if (SZ_EMPTY.equals(szSQLRequest) || szSQLRequest == null)
            szSQLRequest = DEFAULT_SQLREQUEST;
        was.printInputTextarea(wam.PARAM_SQLREQUEST, 10, 72, szSQLRequest);
        was.println();
        was.printButtonExecute();
        was.printButtonRefresh();
        was.closeForm();

        //
        // execute passed SQL request if button "exec" pressed
        //

        if ((!SZ_EMPTY.equals(szSQLRequest)) && szSQLRequest != null &&
                request.getParameter(wam.PARAM_BTN_EXEC) != null) {
            java.sql.Connection dbc = null;
            was.printSeparator(); // <hr>
            try {
                dbc = do_db_logon();
                if(!checkTISQLSAMCapability(dbc,TISQL_CAP_SVC_SQL_ADHOC))
                    throw (new Exception("Ad-hoc SQL not allowed for you, " + TISQL_CAP_SVC_SQL_ADHOC + " capability required; ask System administrator ") );
            } catch (Exception e) {
                printerrmsg(wam.getMsgText(wam.ERROR_WHILE_JDBC_CONNECTION,
                        request.getRemoteUser()), e);
                return;
            }
            try {
                exec_sql(dbc, szSQLRequest);
            } catch (Exception e) {
                printerrmsg(wam.getMsgText(wam.ERROR_WHILE_JDBC_SQL), e);
            } finally {
                do_db_logoff(dbc);
            }
        }
        was.closeDocument();
    } // processSQLRequestForm()

//
// DB interaction
//

    public java.sql.Connection do_db_logon() throws Exception {
/*
        java.sql.Connection dbc;
        // register JDBC driver
        register_jdbc_driver("org.postgresql.Driver");
        // open JDBC connection
        dbc = DriverManager.getConnection(getJDBCURL(), request.getRemoteUser(), "");
*/
	db_logon();
        return (getDBC());
    } // db_logon()

    public void do_db_logoff(java.sql.Connection dbc) {
	db_logoff();
/*
        if (dbc == null) return;
        try {
            dbc.close();
        } catch (SQLException e) {
        } // ignore
*/
    } // db_logoff()

   public static final String TISQL_SAM_CHECK_CAPABILITY ="select SAM.check_capability(?)";
   public static final String TISQL_CAP_SVC_SQL_ADHOC ="SVC_SQL_ADHOC";
   public static boolean checkTISQLSAMCapability(java.sql.Connection dbc, String cap)
             throws java.sql.SQLException
   {
    java.sql.PreparedStatement st = null;
    java.sql.ResultSet rs = null;
    boolean is_cap = false; // there are no capability by default if something going wrong
    try{
       st = dbc.prepareStatement(TISQL_SAM_CHECK_CAPABILITY);
       st.setString(1, cap);
       rs = st.executeQuery();
       rs.next();
       is_cap = rs.getBoolean(1);
    }
    //catch (SQLException e) {is_cap=false;}
    finally {
       try { if (rs != null) rs.close(); } catch (SQLException e) { }
       try { if (st != null) st.close(); } catch (SQLException e) { }
    }
    return(is_cap);
   } // checkTISQLCapability(java.sql.Connection dbc, String cap)

    /**
     * execute sz_sql and print it's ResultSet as html table.
     */
    public void exec_sql(java.sql.Connection dbc, String sz_sql)
            throws java.sql.SQLException, java.io.IOException {
        java.sql.Statement st = null;
        java.sql.ResultSet rs = null;
        int req_processed = 0;
        try {
            st = dbc.createStatement();
            // rs = st.executeQuery(sz_sql); print_sql_rs(rs);
            st.execute(sz_sql);
            // while(true){rs=st.getResultSet();print_sql_rs(rs); if(was.checkError()) break;} // infinite loop for future research
            while (true) {
                int rows_updated = st.getUpdateCount();
                if (rows_updated != -1) {
                    if (req_processed != 0) was.printSeparator();
                    was.println(wam.getMsgText(wam.TOTAL_ROWS_CHANGED,
                            Integer.toString(rows_updated)));
                    st.getMoreResults();
                    req_processed++;
                    continue;
                }
                rs = st.getResultSet();
                if (rs != null) {
                    if (req_processed != 0) was.printSeparator();
                    print_sql_rs(rs);
                } else break;
                st.getMoreResults();
                req_processed++;
            } // while(true)
        } catch (SQLException e) {
            printerrmsg(wam.getMsgText(wam.ERROR_WHILE_JDBC_SQL, sz_sql), e);
        } finally {
            try {
                if (rs != null) rs.close();
            } catch (SQLException e) {
            }
            try {
                if (st != null) st.close();
            } catch (SQLException e) {
            }
        }
        if (req_processed != 1) {
            was.println();
            was.println(wam.getMsgText(wam.TOTAL_SQL_REQUESTS_PROCESSED,
                    Integer.toString(req_processed)));
        }
    } //exec_sql()

    /**
     * print the whole of rs as html table/
     */
    public void print_sql_rs(java.sql.ResultSet rs)
            throws java.sql.SQLException, java.io.IOException {
        java.sql.ResultSetMetaData rsmd = rs.getMetaData();
        int column_count = rsmd.getColumnCount();
        int i = 0;
        int rowcount = 0;
        String col_subsys[], col_target[];
        col_subsys = new String[column_count + 1];
        col_target = new String[column_count + 1];
        int doc_id = was.beginDocument();
        try {
            // print column's titles
            was.beginTable();
            was.beginTableRow();
            for (i = 1; i <= column_count; i++) {
                String cn = rsmd.getColumnName(i);
                if (cn == null) cn = SZ_EMPTY;
                was.printHCellValue(rsmd.getColumnName(i) +
                        "\n(" + rsmd.getColumnTypeName(i) + ")");
                cn = cn.toUpperCase();
                if (cn.equals("ZSID") || cn.equals("QSID") || cn.equals("SCOPE_ID") || cn.equals("SID")) {
                    col_subsys[i] = wam.SUBSYS_SAM;
                    col_target[i] = wam.TARGET_SAMSCOPE;
                } else if (cn.equals("STARTID") || cn.equals("PRANGE") ) {
                    col_subsys[i] = wam.SUBSYS_SAM;
                    col_target[i] = wam.TARGET_SAMRANGE;
                } else if (cn.equals("ZUID") || cn.equals("QUID") || cn.equals("USER_ID") || cn.equals("UID")) {
                    col_subsys[i] = wam.SUBSYS_SAM;
                    col_target[i] = wam.TARGET_SAMUSER;
                } else if (cn.equals("GID") || cn.equals("GROUP_ID")) {
                    col_subsys[i] = wam.SUBSYS_SAM;
                    col_target[i] = wam.TARGET_SAMUSER;
                } else if (cn.equals("ZOID") || cn.equals("QOID") || cn.endsWith("_ID")) {
                    col_subsys[i] = wam.SUBSYS_TIS;
                    col_target[i] = wam.TARGET_SAMUSER;
                } else if (cn.equals("ID")) {
                    col_subsys[i] = wam.SUBSYS_PSQL;
                }
            } // for column's titles
            was.closeTableRow();
            while (rs.next()) {
                // print columns values
                was.beginTableRow();
                // Random r = new Random();
                for (i = 1; i <= column_count; i++) {
                    Object o = rs.getObject(i);
                    if (o == null) was.printCellValue(null);
                    else if (col_subsys[i] != null) {
                        try {
                            Long id = new Long(o.toString());
                            was.printCellHRefView(wam.obj2text(o), col_subsys[i], col_target[i], id);
                        } catch (Exception e) {
                            was.printCellValue(wam.obj2text(o));
                        }
                    } else was.printCellValue(wam.obj2text(o));
                    // rs.getObject(i).getClass().getName()
                } // for column's values
                // if(r.nextBoolean() && r.nextBoolean()) throw(new java.sql.SQLException());
                was.closeTableRow();
                rowcount++;
            } // while(rs.next())
            was.closeTable();
        } finally {
            was.closeDocument(doc_id);
        }
        was.println(wam.getMsgText(wam.TOTAL_ROWS, Integer.toString(rowcount)));
    } // print_sql_rs

    // constructors
    public WARHPSQL(
            javax.servlet.http.HttpServletRequest request,
            javax.servlet.http.HttpServletResponse response,
            java.io.PrintWriter out) {
        super(request, response, out);
    }

    public WARHPSQL(WARequestHandler warh) {
        super(warh);
    }

} //WARHPSQL
