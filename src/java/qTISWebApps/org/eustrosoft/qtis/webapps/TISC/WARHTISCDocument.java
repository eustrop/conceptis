// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2011
// (c) EustroSoft.org 2018-2019
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
//
//package ru.mave.ConcepTIS.webapps;
package org.eustrosoft.qtis.webapps.TISC;

import java.sql.*;
import ru.mave.ConcepTIS.dao.*;
import ru.mave.ConcepTIS.dao.SAM.*;
import ru.mave.ConcepTIS.dao.TIS.*;
import ru.mave.ConcepTIS.dao.TISC.*;
import org.eustrosoft.qtis.webapps.*;


public class WARHTISCDocument extends WARHTypicalObject{
 
 public static final String SUBSYS_THIS = WAMessages.SUBSYS_TISC;
 public static final String TARGET_THIS = WAMessages.TARGET_DOCUMENT;
 
 @Override public String getThisDocSubsys() { return SUBSYS_THIS; }
 @Override public String getThisDocTarget() { return TARGET_THIS; }
 @Override public int getThisDocTypeCaptionID() { return(WAMessages.OBJ_TISC_DOCUMENT); }
 @Override public DObject newThisDocDObject() { return(Document.newDObject()); }

 @Override public void printThisDocGeneralHMenu() 
 {
  was.printHMenuItem(WAMessages.MNU_TISC_DOCUMENT_LIST);
  was.printHMenuItem(WAMessages.MNU_TISC_DOCUMENT_CREATE);
  was.printHMenuItem(WAMessages.MNU_TISC_DOCUMENT_FIND);
 } //printThisDocGeneralHMenu() 
 
 @Override public RList loadThisDocList(Long ZSID, int rows_per_page, long row_offset)
 throws SQLException, ZException
 { return getZSystem().listTISCDocument(ZSID, rows_per_page, row_offset); }
 
 @Override public DObject loadThisDocDObject(Long ZOID)
  throws SQLException, ZException 
 { return getZSystem().loadTISCDocument(ZOID); }
 
 @Override public DObject extractThisDocDObject(ZFieldSet fs)
  throws SQLException, ZException 
 { DObject o = Document.newDObject(); o.getFromFS(fs); return(o); }

 @Override public void printThisDocView(DObject o)
 {
  RList rl = o.getTable(DDocument.TAB_CODE);
  RList rl_scopes=null; // list of SAMScopes
  DORecord dor = null;
  try{ rl_scopes = getZSystem().listSAMScopes(); } catch(Exception e) { printerrmsg(e); }
  load_DocReferences(o);
  ZDictionary dicSLEVEL = getZSystem().getDictionary(WAMessages.DIC_SLEVEL);
    if(rl.size() < 1) { // for empty objects
     was.printMsg(WAMessages.INFO_EMPTY_DOBJECT_VIEWED);
     was.beginTable();
     was.printTabField(ZName.F_ZNAME,o.getZNAME());
     was.printTabField(ZObject.F_ZOID,WAMessages.obj2string(o.getZOID()));
     was.printTabFieldDic(new ZField(ZObject.F_ZTYPE, o.getZTYPE()),
         getDic(WAMessages.DIC_TIS_OBJECT_TYPE));
     was.closeTable();
    }
    // header
    for(int i=0;i<rl.size();i++)
    {
     dor =((DORecord)(rl.get(i)));
     ZFieldSet fs=dor.toFieldSet();
     was.beginTable();
      was.printTabField(fs,DDocument.F_TITLE);
      was.printTabFieldList(fs,DDocument.F_TYPE,rl_scopes,WAMessages.SUBSYS_SAM, WAMessages.TARGET_SAMSCOPE);
      was.printTabField(fs,DDocument.F_SYMBOL);
      was.printTabField(fs,DDocument.F_AUTH);
      was.printTabField(fs,DDocument.F_ABSTR);
     was.closeTable();
     //DR
     was.beginIndent();
      RList rlDR=dor.getTableChildren(DRef.TAB_CODE);
      for(int iDR=0;iDR<rlDR.size();iDR++)
      {
      DORecord DR = (DORecord)rlDR.get(iDR);
      fs= DR.toFieldSet();
      was.beginTable();
      was.printTabFieldZOID2ZNAME(fs,DRef.F_DOC_ID,getZNames4Field(DRef.FC_DOC_ID),wam.SUBSYS_TISC);
      was.printTabFieldDic(fs,DRef.F_RELTYPE,getDic(DRef.FC_RELTYPE));
      was.closeTable(); //header
      }
     was.closeIndent();
     //DW
     was.beginIndent();
      RList rlDW=dor.getTableChildren(DRow.TAB_CODE);
      for(int iDW=0;iDW<rlDW.size();iDW++)
      {
      DORecord DW = (DORecord)rlDW.get(iDW);
      fs= DW.toFieldSet();
      was.beginTable();
      was.printTabField(fs,DRow.F_NUM);
      was.printTabField(fs,DRow.F_ITEM);
      was.closeTable(); //header
        //DP
        RList rlDP=DW.getTableChildren(DRProperty.TAB_CODE);
	if(rlDP.size()>0){
        was.beginIndent();
         for(int iDP=0;iDP<rlDP.size();iDP++)
         {
         DORecord DP = (DORecord)rlDP.get(iDP);
         fs= DP.toFieldSet();
         was.beginTable();
	  was.printTabFieldDic(fs,DRProperty.F_TYPE,getDic(DRProperty.FC_TYPE));
          was.printTabField(fs,DRProperty.F_NVALUE);
          was.printTabField(fs,DRProperty.F_TVALUE);
         was.closeTable(); //header
         }
        was.closeIndent();
        } //if(rlDP.size()>0){
      }
     was.closeIndent();
    } //header
 }//printThisDocView

 @Override public void printThisDocEditForm(ZFieldSet fs)
 {
  int i;
 //   DDocument AA = null;
 //   RList rl_scopes = getSAMScopes(); // exceptions are ignored!
  for(i=0;i<fs.countFS();i++){
   ZFieldSet fsDD = fs.getFS(i);
   if(!DDocument.TAB_CODE.equals(fsDD.getCode())) continue;
   was.beginRecord(); //dd
   was.printSTDFieldsInputDORecord(fsDD);
   //DDocument
   was.beginTable();
    was.printTabRecordDeletionStatus(fsDD);
    was.printTabFieldInput(fsDD.get(DDocument.F_SYMBOL));
    was.printTabFieldInput(fsDD.get(DDocument.F_TITLE));
    was.printTabFieldInputDic(fsDD.get(DDocument.F_TYPE),getDic(DDocument.FC_TYPE));
    was.printTabFieldInput(fsDD.get(DDocument.F_AUTH));
    was.printTabFieldInput(fsDD.get(DDocument.F_ABSTR));
   was.closeTable();
   // buttons to add child type of records 
   was.printButtonAddRecord(DRef.TAB_CODE);
   was.printButtonAddRecord(DRow.TAB_CODE);
   was.println();
   //DRef
   int i2;
   was.beginIndent();
   for(i2=0;i2<fsDD.countFS();i2++){
    ZFieldSet fsDR = fsDD.getFS(i2);
    if(!DRef.TAB_CODE.equals(fsDR.getCode())) continue;
    was.beginRecord(); //CE
    was.printSTDFieldsInputDORecord(fsDR);
    was.beginTable();
     was.printTabRecordDeletionStatus(fsDR);
     was.printTabFieldInput(fsDR.get(DRef.F_DOC_ID));
     //was.printTabFieldInput(fsDR.get(DRef.F_RELTYPE));
     was.printTabFieldInputDic(fsDR.get(DRef.F_RELTYPE),getDic(DRef.FC_RELTYPE));
    was.closeTable();
    was.closeRecord();
   }
   was.closeIndent();
   //DRow
   int i3;
   was.beginIndent();
   for(i3=0;i3<fsDD.countFS();i3++){
    ZFieldSet fsRW = fsDD.getFS(i3);
    if(!DRow.TAB_CODE.equals(fsRW.getCode())) continue;
    was.beginRecord(); //DW
    was.printSTDFieldsInputDORecord(fsRW);
    was.beginTable();
     was.printTabRecordDeletionStatus(fsRW);
     was.printTabFieldInput(fsRW.get(DRow.F_NUM));
     was.printTabFieldInput(fsRW.get(DRow.F_ITEM));
     was.printButtonAddRecord(DRProperty.TAB_CODE);
    was.closeTable();
 //   DProperty    
    was.beginIndent(); //DP
    int i4;
    for(i4=0;i4<fsRW.countFS();i4++)
    {
     ZFieldSet fsDP = fsRW.getFS(i4);
     if(!DRProperty.TAB_CODE.equals(fsDP.getCode())) continue;
     was.beginRecord(); //DP
     was.printSTDFieldsInputDORecord(fsDP);
     was.beginTable();
      was.printTabRecordDeletionStatus(fsDP);
      was.printTabFieldInputDic(fsDP.get(DRProperty.F_TYPE),getDic(DRProperty.FC_TYPE));
      was.printTabFieldInput(fsDP.get(DRProperty.F_NVALUE));
      was.printTabFieldInput(fsDP.get(DRProperty.F_TVALUE));
     was.closeTable();
     was.closeRecord(); //DP
    } //DP
    was.closeIndent(); //DP
    was.closeRecord(); //DW
   }
   was.closeIndent(); //DD
   was.closeRecord(); //DD
  }
  
  if(i==0) was.println();
  was.printButtonAddRecord(DDocument.TAB_CODE);
  was.println();
  was.printButtonMenuBarEdit();
  was.closeRecord(); //DObject
  was.closeForm();
 } //printThisDocEditForm()
 
 // constructors
 public WARHTISCDocument(
  javax.servlet.http.HttpServletRequest request,
  javax.servlet.http.HttpServletResponse response,
  java.io.PrintWriter out)
 { super(request, response, out); }

 public WARHTISCDocument(WARequestHandler warh) { super(warh); }

} //WARHTISCDocument
