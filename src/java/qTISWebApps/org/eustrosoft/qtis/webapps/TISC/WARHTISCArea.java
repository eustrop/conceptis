// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

//package ru.mave.ConcepTIS.webapps;
package org.eustrosoft.qtis.webapps.TISC;

import java.util.*;
import java.sql.*;
import ru.mave.ConcepTIS.dao.*;
import ru.mave.ConcepTIS.dao.TIS.*;
import ru.mave.ConcepTIS.dao.TISC.*;
import ru.mave.ConcepTIS.dao.SAM.*;
import org.eustrosoft.qtis.webapps.*;

public class WARHTISCArea extends WARHTypicalObject
{
 public static final String SUBSYS_THIS = WAMessages.SUBSYS_TISC;
 public static final String TARGET_THIS = WAMessages.TARGET_AREA;

 public String getThisDocSubsys(){return(SUBSYS_THIS); }
 public String getThisDocTarget(){return(TARGET_THIS); }
 public int getThisDocTypeCaptionID() { return(wam.OBJ_TISC_AREA); }
 public DObject newThisDocDObject() { return(Area.newDObject()); }

 public void printThisDocGeneralHMenu()
 {
  was.printHMenuItem(wam.MNU_TISC_AREA_LIST);
  was.printHMenuItem(wam.MNU_TISC_AREA_CREATE);
  was.printHMenuItem(wam.MNU_TISC_AREA_FIND);
 }

 public RList loadThisDocList(Long ZSID, int rows_per_page, long row_offset)
   throws java.sql.SQLException,ZException
 { return( getZSystem().listTISCArea(ZSID,rows_per_page,row_offset) ); }

 public DObject loadThisDocDObject(Long ZOID)
   throws java.sql.SQLException,ZException
 { return(getZSystem().loadTISCArea(ZOID)); }

 public DObject extractThisDocDObject(ZFieldSet fs)
   throws java.sql.SQLException,ZException
 {DObject o=Area.newDObject();o.getFromFS(fs);return(o);}

 public void printThisDocView(DObject o)
 {
  RList rl = o.getTable(AArea.TAB_CODE);
  RList rl_scopes=null; // list of SAMScopes
  try{ rl_scopes = getZSystem().listSAMScopes(); } catch(Exception e) { printerrmsg(e);}
  ZDictionary dicSLEVEL = getZSystem().getDictionary(wam.DIC_SLEVEL);
  load_DocReferences(o);
   if(rl.size() < 1) { // for empty objects
    was.printMsg(wam.INFO_EMPTY_DOBJECT_VIEWED);
    was.beginTable();
    was.printTabField(ZName.F_ZNAME,o.getZNAME());
    was.printTabField(ZObject.F_ZOID,wam.obj2string(o.getZOID()));
    was.printTabFieldDic(new ZField(ZObject.F_ZTYPE, o.getZTYPE()),
	getDic(wam.DIC_TIS_OBJECT_TYPE));
    was.closeTable();
   }
   // header
   for(int i=0;i<rl.size();i++)
   {
   ZFieldSet fs=((DORecord)(rl.get(i))).toFieldSet();
   was.beginTable();
   was.printTabField(fs,AArea.F_NAME);
   //was.printTabField(fs,AArea.F_SCOPE_ID);
   was.printTabFieldList(fs,AArea.F_SCOPE_ID,rl_scopes,wam.SUBSYS_SAM, wam.TARGET_SAMSCOPE);
   was.printTabField(fs,AArea.F_DESCR);
   //was.printTabFieldDic(fs,ZObject.F_ZTYPE,getDic(wam.DIC_TIS_OBJECT_TYPE));
   was.closeTable();
   } //header
 } // printDocumentView

 public void printThisDocEditForm(ZFieldSet fs)
 {
  int i;
  AArea AA = null;
  RList rl_scopes = getSAMScopes(); // exceptions are ignored!
  for(i=0;i<fs.countFS();i++)
  {
   ZFieldSet fsAA = fs.getFS(i);
   if(!AArea.TAB_CODE.equals(fsAA.getCode())) continue;
   was.beginRecord(); //AA
   was.printSTDFieldsInputDORecord(fsAA);
   //was.printFieldInputHidden(fsAA.get(r.F_ID));
   //was.printRecordDeletionStatus(fsAA);
   was.beginTable();
    was.printTabRecordDeletionStatus(fsAA);
    was.printTabFieldInput(fsAA.get(AA.F_NAME));
    was.printTabFieldInputList(fsAA.get(AA.F_SCOPE_ID),rl_scopes);
    was.printTabFieldInput(fsAA.get(AA.F_DESCR));
   was.closeTable();
   was.closeRecord(); //AA
  }
  if(i==0)was.println();
  was.printButtonAddRecord(AArea.TAB_CODE);was.println();
  was.print(wam.getMsgText(wam.ADD_RECORD));
  was.printButtonAddRecord(AArea.TAB_CODE,AArea.TAB_CODE);was.println();
  was.printButtonMenuBarEdit();
  was.closeRecord(); //DObject
  was.closeForm();
 } //printThisDocEditForm

 // constructors
 public WARHTISCArea(
	javax.servlet.http.HttpServletRequest request,
	javax.servlet.http.HttpServletResponse response,
	java.io.PrintWriter out)
 { super(request,response,out); }
 public WARHTISCArea(WARequestHandler warh) { super(warh); }

} //WARHTISCArea
