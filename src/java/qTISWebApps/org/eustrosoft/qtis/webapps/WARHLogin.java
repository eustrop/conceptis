// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

//package ru.mave.ConcepTIS.webapps;
package org.eustrosoft.qtis.webapps;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import org.eustrosoft.qdbp.*;
import org.eustrosoft.qtis.SessionCookie.*;


public class WARHLogin extends WARequestHandler{
    boolean showTopMenu = false;
    public boolean setTopMenu(boolean v) { boolean ov = v; showTopMenu = v; return(ov); }

    // constructors
    public WARHLogin(HttpServletRequest request,HttpServletResponse response,java.io.PrintWriter out){
        super(request,response,out);
    } // WARHLogin(HttpServletRequest,JspWriter)

    public WARHLogin(WARequestHandler warh) {
        super(warh);
    } // WARHLogin(WARequestHandler warh)


    /** find, create and return specific request handler using request's parameters (subsys, request, target)
     * 
     */
    public WARequestHandler findRequestHandler()
    {
      return(this);
    } // findRequestHandler()


    public void process(){
        WARequestHandler warh = null;
        login();
    } // process()

    public void processREST()
    {
    } // processREST()


    public void login() 
   {
    
 // 1. get or create DB pool
 QDBPool dbp = QDBPool.get(getQDBPoolName());
 QDBPSession dbps = null;
    try{
 if(dbp == null){dbp = new QDBPool(getQDBPoolName(),getQDBPoolURL(),getQDBPoolClass()); QDBPool.add(dbp);}
 // 2. get session
 // 2.1. get session cookie and its value if so
 QTISSessionCookie session_cookie = new QTISSessionCookie(request, response);
 // 2.2. get session by cookie
 dbps = dbp.logon(session_cookie.value());
 // 2.3. renew session if ready (reserved for future use)
 if(dbps != null && dbps.isSessionRenewReady()) {dbps.renewSession(); session_cookie.set(dbps.getSessionSecretCookie(),dbps.getSessionCookieMaxAge()); }
 if(dbps == null) dbps = dbp.createSession(); // temporary session stub, not stored in the pool
 // END get session

    //was.printErrMsg(wam.getMsgText(wam.ERROR_CALL_NOT_IMPLEMENTED));
    String btn_login=request.getParameter("btn_login");
    String btn_logout=request.getParameter("btn_logout");
    String btn_cancel=request.getParameter("btn_cancel");
    if(btn_cancel != null)
    {
     was.sendHTTPRedirect(response,getCGI());
    }
    if(btn_logout != null)
    {
     dbps.logout();
     session_cookie.delete();
     out.println("Logout!<br>");
    }
    if(btn_login != null)
    {
     String login=request.getParameter("login");
     String password=request.getParameter("password");
     try{
      if(dbps != null) dbps.logout();
      dbps = dbp.logon(login,password);
      session_cookie.set(dbps.getSessionSecretCookie(),dbps.getSessionCookieMaxAge());
      was.sendHTTPRedirect(response,getCGI());
      out.println("login Ok!<br>");
      }
      catch(Exception e){was.printErrMsg(e);}
    }
    } catch(Exception e){was.printErrMsg(e);}
    //out.println("session cookie should expare at (s):" + dbps.getSessionCookieExpire());
    if(!request.isSecure()){
        out.println("Logon over insecure connection depricated");
    }
    else {
     out.println("<center>");
     out.println("<H2>Login:</H2>");
     out.println("<form method=\"POST\" action=\""  + getCGI() + "?subsys=main&request=login\">");
     out.println("<table>");

     out.println("<tr><td>");
     out.println("Login:</td><td><input type=\"text\" name=\"login\" value=\"\"><br>");
     out.println("</td></tr>");
     out.println("<tr><td>");
     out.println("Password:</td><td><input type=\"password\" name=\"password\" value=\"\"></br>");
     out.println("</td></tr>");
     out.println("<tr><td>");
     out.println("<input type=\"submit\" name=\"btn_login\" value=\"login\">");
     out.println("<input type=\"submit\" name=\"btn_logout\" value=\"logout\">");
     out.println("</td><td>");
     out.println("<input type=\"submit\" name=\"btn_cancel\" value=\"Cancel\">");
     out.println("</td></tr>");
     out.println("</table>");
     out.println("</form>");
     out.println("</center>");
    }

   } // login()

} //WARHLogin
