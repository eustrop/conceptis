// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//
// History:
//  2009/11/28 started from psql.jsp
//

//package ru.mave.ConcepTIS.webapps;
package org.eustrosoft.qtis.webapps;

import ru.mave.ConcepTIS.dao.*;
import java.util.*;
import java.sql.*;
import org.eustrosoft.qdbp.*;
import org.eustrosoft.qtis.SessionCookie.*;

/** top level Web Application abstract class. This is storage of
 * common methods and properties.
 * @see WARHMain
 */
public abstract class WARequestHandler{


//
// Global parameters
//
//private final static String CLASS_VERSION = "$Id$";
private String DBSERVER_URL = "jdbc:postgresql:conceptisdb";

//DBPOOL configuration
private String QDBPOOL_NAME = null ; // = "QSQL"; // name of QDBPoll
private String QDBPOOL_URL = null ; // = "jdbc:postgresql://conceptis-pg-server:5432/conceptisdb"; // url of database
private String QDBPOOL_JDBC_CLASS = null ; // = "org.postgresql.Driver"; // java class to register
public final static String PARAM_QDBPOOL_NAME = "QDBPOOL_NAME"; // name of QDBPoll
public final static String PARAM_QDBPOOL_URL = "QDBPOOL_URL";
public final static String PARAM_QDBPOOL_JDBC_CLASS = "QDBPOOL_JDBC_CLASS";
// parameters below not used yet:
private final static String DBPOOL_USER = ""; // name of database user. if not set - logon with passed session user/password to DB
private final static String DBPOOL_USER_PASSWORD = ""; //
private final static String DBPOOL_TISQL_LOGON = ""; // "select sam.logon(?,?,?);" (user,password,session_key)
private final static String DBPOOL_TISQL_BINDSESSION = ""; // "select sam.bind_session(?,?);" (session_id,session_key)
private final static String DBPOOL_OPT_ALLOWTRUSTED = "NO"; // default NO
private final static String DBPOOL_OPT_MINPASSWORD = "8"; // minimun password length (default 8)

public void setQDBPool(String QDBPOOL_NAME, String QDBPOOL_URL, String QDBPOOL_JDBC_CLASS)
{
 this.QDBPOOL_NAME = QDBPOOL_NAME;
 this.QDBPOOL_URL = QDBPOOL_URL;
 this.QDBPOOL_JDBC_CLASS = QDBPOOL_JDBC_CLASS;
}
public String getQDBPoolName(){return(QDBPOOL_NAME);}
public String getQDBPoolURL(){return(QDBPOOL_URL);}
public String getQDBPoolClass(){return(QDBPOOL_JDBC_CLASS);}


public final static String SZ_EMPTY = "";
/** no more rows per page should be shown (useful for huge lists). */
public final static int MAX_ROWS_PER_PAGE = 10;

//
// JSP emulation parameters
// (must be initialized at constructor)
//

protected java.io.PrintWriter out;
protected javax.servlet.http.HttpServletRequest request;
protected javax.servlet.http.HttpServletResponse response;

// JSP/Servlet body preservation objects (only one can be used!)
protected java.io.BufferedReader bodyReader = null;
protected javax.servlet.ServletInputStream bodyInputStream = null;
java.io.IOException bodyPreservationException = null;

/** the same as the similar javax.servlet.ServletRequest method.
* Howerver returns null if no body preserved with preserveReader(). */
public java.io.BufferedReader getReader(){return(bodyReader);}

/** the same as the similar javax.servlet.ServletRequest method.
* Howerver returns null if no body preserved with preserveInputStream(). */
public javax.servlet.ServletInputStream getInputStream(){return(bodyInputStream);}

public boolean isBodyPreserved(){return(bodyReader != null || bodyInputStream != null);}

/** preserved request's body for future using over getReader() call. */
public void preserveReader()
 {
  if(request!=null && !isBodyPreserved() )
  try{bodyReader=request.getReader();}
  catch(java.io.IOException ioe){ bodyPreservationException = ioe;}
 }
/** preserved request's body for future using over getInputStream() call. */
public void preserveInputStream()
 {
  if(request!=null && !isBodyPreserved())
  try{bodyInputStream=request.getInputStream();}
  catch(java.io.IOException ioe){ bodyPreservationException = ioe;}
 }

protected WASkin was;
protected WAMessages wam;

 //
 // some useful static functions.
 //

 /** @see WAMessages#obj2html */
 public static String obj2html(Object o) { return(WAMessages.obj2html(o)); }

 //
 // DB interaction & result printing methods
 //

 /** set JDBC url. */
 public void setJDBCURL(String url) { DBSERVER_URL=url; }
 public String getJDBCURL() { return(DBSERVER_URL); }

 /** set nutshell CGI url. */
 public void setCGI(String url) { was.setCGI(url); }
 public String getCGI() { return(was.getCGI()); }

  /** create driver class by its classname (jdbc_driver parameter)
   * and register it via DriverManager.registerDriver().
   * @param jdbc_driver - "org.postgresql.Driver" for postgres,
   * "oracle.jdbc.driver.OracleDriver" for oracle, etc.
   */ 
 public static void register_jdbc_driver(String jdbc_driver)throws Exception{
	
  
   java.sql.Driver d;
   Class dc;
   // get "Class" object for driver's class
   try{
   
    dc = Class.forName(jdbc_driver);
    d = (java.sql.Driver)dc.newInstance();
   
  } catch(ClassNotFoundException e) {
     throw new Exception("register_jdbc_driver:" + "unable to get Class for " + jdbc_driver + ":" + e); 
    
   }catch(Exception e) {
     throw new Exception("register_jdbc_driver: unable to get driver " + jdbc_driver + " : " + e); }
     
   // register driver
   try {
	   DriverManager.registerDriver(d); 
	 
    } catch(SQLException e) { throw new Exception("register_jdbc_driver: unable to register driver "+jdbc_driver+" : "+e);}
   
  } // register_jdbc_driver()


 private ZSystem zsys;
 public ZSystem getZSystem(){return(zsys);}
 public java.sql.Connection getDBC(){if(zsys!=null)return(zsys.getDBC());return(null);}
 public void setZSystem(ZSystem zsys){this.zsys=zsys;}

 public ZDictionary getDic(String dic_name)
 {
  if(getZSystem() == null) return(new ZDictionary());
  return(getZSystem().getDictionary(dic_name));
 }

 protected QDBPSession dbps;
 protected QTISSessionCookie session_cookie;

 public void setQDBPSession(QDBPSession s){this.dbps = s;}
 public QDBPSession getQDBPSession()
 {
   if(dbps != null) return(dbps);
   // 1. get or create DB pool
   QDBPool dbp = QDBPool.get(QDBPOOL_NAME);
   //if(dbps != null) dbps.free(); //SIC! don't implemented yet
   dbps = null;
   if(dbp == null){dbp = new QDBPool(QDBPOOL_NAME,QDBPOOL_URL,QDBPOOL_JDBC_CLASS); QDBPool.add(dbp);}
   // 2. get session
   // 2.1. get session cookie and its value if so
   session_cookie = new QTISSessionCookie(request, response);
   // 2.2. get session by cookie
   dbps = dbp.logon(session_cookie.value());
   // debug: if(dbps == null) throw new Exception(session_cookie.value() + QDBPOOL_NAME);
   // 2.3. renew session if ready (reserved for future use)
   if(dbps != null && dbps.isSessionRenewReady()) {dbps.renewSession(); session_cookie.set(dbps.getSessionSecretCookie(),dbps.getSessionCookieMaxAge()); }
   if(dbps == null) dbps = dbp.createSession(); // temporary session stub, not stored in the pool
   // END get session
   // 3. process request
   // register JDBC driver
   //register_jdbc_driver("org.postgresql.Driver");
   // open JDBC connection
   //dbc=DriverManager.getConnection(getJDBCURL(),request.getRemoteUser(),"");
   //dbps.lock();
   return(dbps);
 }

 public void db_logon() throws Exception {
   if(getZSystem() != null) return; // already logged on
   java.sql.Connection dbc;
   if(dbps == null) dbps = getQDBPSession();
   QDBPConnection qcon =  dbps.getConnection();
   if(qcon == null) throw new WAException(wam,wam.ERROR_NO_ACTIVE_SESSION);
   dbc = qcon.get();
   setZSystem(new ZSystem(dbc));
   was.setFieldDic(getDic(WAMessages.DIC_TIS_FIELD));
 } // db_logon()

 public void db_logoff()
 {
  dbps=null;
  if(zsys == null) return;
  if(dbps == null) return;
/*
  java.sql.Connection dbc = zsys.getDBC();
  if(dbc == null) return;
  try{ dbc.close(); }
  catch(SQLException e){} // ignore
*/
  zsys = null; //SIC! to think about it
 } // db_logoff()


 //
 // Message displaying tools
 //

 /** display informationl message. TISExmlDB.java legacy where have been wrapper to System.out.print */
 public void printmsg(String msg) {was.printMsg(msg);}

 /** dispaly error message. TISExmlDB.java legacy where have been just a wrapper to System.err.print */
 public void printerrmsg(String msg) {was.printErrMsg(msg);}
 public void printerrmsg(String msg, Exception e) {was.printErrMsg(msg,e);}
 public void printerrmsg(Exception e) {was.printErrMsg(e);}


 //
 // http/html hints&tools
 //

 /** some hints for old and buggy browsers like NN4.x */
 public void cache_expire_hints()
   throws java.io.UnsupportedEncodingException
 {
  long enter_time = System.currentTimeMillis();
  long expire_time = enter_time + 24*60*60*1000;
  response.setHeader("Cache-Control","No-cache, no-store, must-revalidate");
  response.setHeader("Pragma","no-cache");
  response.setDateHeader("Expires",expire_time);
  request.setCharacterEncoding("UTF-8");
 }

 // abstract methods
/** process request and produce answer as UI page (html) */
 public abstract void process();
/** process request and produce answer as JSON  */
 public abstract void processREST();

 // constructors

 public WARequestHandler(
	javax.servlet.http.HttpServletRequest request,
	javax.servlet.http.HttpServletResponse response,
	java.io.PrintWriter out)
 {
  this.request = request;
  this.response = response;
  this.out = out;
  wam = new WAMessages(); was = new WASkin(out,wam);
 } // WARequestHandler(HttpServletRequest,JspWriter)

 public WARequestHandler(WARequestHandler warh)
 {
  this.request = warh.request;
  this.response = warh.response;
  this.out = warh.out;
  this.wam = warh.wam;
  this.was = warh.was;
  this.dbps = warh.dbps;
  this.session_cookie = warh.session_cookie;
  this.setJDBCURL(warh.getJDBCURL());
  this.setQDBPool(warh.getQDBPoolName(),warh.getQDBPoolURL(),warh.getQDBPoolClass());
 } // WARequestHandler(WARequestHandler warh)
 
} // WARequestHandler.java
