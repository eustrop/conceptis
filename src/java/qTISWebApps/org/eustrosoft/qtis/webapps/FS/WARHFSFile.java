// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2011
// (c) EustroSoft.org 2018-2019
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// History:
//	2023-04-47 21:51 started from WARH_T_I_SC_D_ocument
//package ru.mave.ConcepTIS.webapps;
package org.eustrosoft.qtis.webapps.FS;

import java.sql.*;
import ru.mave.ConcepTIS.dao.*;
import ru.mave.ConcepTIS.dao.SAM.*;
import ru.mave.ConcepTIS.dao.TIS.*;
import ru.mave.ConcepTIS.dao.FS.*;
import org.eustrosoft.qtis.webapps.*;


public class WARHFSFile extends WARHTypicalObject{
 
 public static final String SUBSYS_THIS = WAMessages.SUBSYS_FS;
 public static final String TARGET_THIS = WAMessages.TARGET_FILE;
 
 @Override public String getThisDocSubsys() { return SUBSYS_THIS; }
 @Override public String getThisDocTarget() { return TARGET_THIS; }
 @Override public int getThisDocTypeCaptionID() { return(WAMessages.OBJ_FS_FILE); }
 @Override public DObject newThisDocDObject() { return(File.newDObject()); }

 @Override public void printThisDocGeneralHMenu() 
 {
  was.printHMenuItem(WAMessages.MNU_FS_FILE_LIST);
  was.printHMenuItem(WAMessages.MNU_FS_FILE_CREATE);
  was.printHMenuItem(WAMessages.MNU_FS_FILE_FIND);
 } //printThisDocGeneralHMenu() 
 
 @Override public RList loadThisDocList(Long ZSID, int rows_per_page, long row_offset)
 throws SQLException, ZException
 { return getZSystem().listFSFile(ZSID, rows_per_page, row_offset); }
 
 @Override public DObject loadThisDocDObject(Long ZOID)
  throws SQLException, ZException 
 { return getZSystem().loadFSFile(ZOID); }
 
 @Override public DObject extractThisDocDObject(ZFieldSet fs)
  throws SQLException, ZException 
 { DObject o = File.newDObject(); o.getFromFS(fs); return(o); }

 @Override public void printThisDocView(DObject o)
 {
  RList rl = o.getTable(FFile.TAB_CODE);
  RList rl_scopes=null; // list of SAMScopes
  DORecord dor = null;
  try{ rl_scopes = getZSystem().listSAMScopes(); } catch(Exception e) { printerrmsg(e); }
  load_DocReferences(o);
  ZDictionary dicSLEVEL = getZSystem().getDictionary(WAMessages.DIC_SLEVEL);
    if(rl.size() < 1) { // for empty objects
     was.printMsg(WAMessages.INFO_EMPTY_DOBJECT_VIEWED);
     was.beginTable();
     was.printTabField(ZName.F_ZNAME,o.getZNAME());
     was.printTabField(ZObject.F_ZOID,WAMessages.obj2string(o.getZOID()));
     was.printTabFieldDic(new ZField(ZObject.F_ZTYPE, o.getZTYPE()),
         getDic(WAMessages.DIC_TIS_OBJECT_TYPE));
     was.closeTable();
    }
    // header
    for(int i=0;i<rl.size();i++)
    {
     dor =((DORecord)(rl.get(i)));
     ZFieldSet fs=dor.toFieldSet();
     was.beginTable();
      was.printTabField(fs,FFile.F_NAME);
      was.printTabFieldDic(fs,FFile.F_TYPE,getDic(wam.DIC_FILE_TYPE));
      was.printTabFieldDic(fs,FFile.F_EXTSTORE,getDic(wam.DIC_YESNO));
      was.printTabField(fs,FFile.F_MIMETYPE);
      was.printTabField(fs,FFile.F_DESCR);
      was.printTabField(fs,FFile.F_B_SIZE);
      was.printTabField(fs,FFile.F_B_CHCNT);
      was.printTabField(fs,FFile.F_B_ALGO);
      was.printTabField(fs,FFile.F_B_DIGEST);
      was.printTabField(fs,FFile.F_T_SIZE);
      was.printTabField(fs,FFile.F_T_CHCNT);
      was.printTabField(fs,FFile.F_T_ALGO);
      was.printTabField(fs,FFile.F_T_DIGEST);
     was.closeTable();
     //DW
     was.beginIndent();
      RList rlDW=dor.getTableChildren(FDir.TAB_CODE);
      for(int iDW=0;iDW<rlDW.size();iDW++)
      {
      DORecord DW = (DORecord)rlDW.get(iDW);
      fs= DW.toFieldSet();
      was.beginTable();
      //was.printTabFieldZOID2ZNAME(fs,FDir.FC_F_ID,getZNames4Field(FDir.FC_F_ID),wam.SUBSYS_FS);
      was.printTabField(fs,FDir.F_F_ID);
      was.printTabField(fs,FDir.F_FNAME);
      was.printTabField(fs,FDir.F_MIMETYPE);
      was.printTabField(fs,FDir.F_DESCR);
      was.closeTable(); //header
      }
     was.closeIndent();
     //DR
     was.beginIndent();
      RList rlDR=dor.getTableChildren(FBlob.TAB_CODE);
      for(int iDR=0;iDR<rlDR.size();iDR++)
      {
      DORecord DR = (DORecord)rlDR.get(iDR);
      fs= DR.toFieldSet();
      was.beginTable();
      was.printTabField(fs,FBlob.F_CHUNK);
      was.printTabField(fs,FBlob.F_NO);
      was.printTabField(fs,FBlob.F_SIZE);
      was.printTabField(fs,FBlob.F_CRC32);
      was.closeTable(); //header
      }
     was.closeIndent();
    } //header
 }//printThisDocView

 @Override public void printThisDocEditForm(ZFieldSet fs)
 {
  int i;
 //   FFile AA = null;
 //   RList rl_scopes = getSAMScopes(); // exceptions are ignored!
  for(i=0;i<fs.countFS();i++){
   ZFieldSet fsDD = fs.getFS(i);
   if(!FFile.TAB_CODE.equals(fsDD.getCode())) continue;
   was.beginRecord(); //dd
   was.printSTDFieldsInputDORecord(fsDD);
   //FFile
   was.beginTable();
    was.printTabRecordDeletionStatus(fsDD);
      was.printTabFieldInput(fsDD.get(FFile.F_NAME));
      was.printTabFieldInputDic(fsDD.get(FFile.F_TYPE),getDic(wam.DIC_FILE_TYPE));
      was.printTabFieldInputDic(fsDD.get(FFile.F_EXTSTORE),getDic(wam.DIC_YESNO));
      was.printTabFieldInput(fsDD.get(FFile.F_MIMETYPE));
      was.printTabFieldInput(fsDD.get(FFile.F_DESCR));
      was.printTabFieldInput(fsDD.get(FFile.F_B_SIZE));
      was.printTabFieldInput(fsDD.get(FFile.F_B_CHCNT));
      was.printTabFieldInput(fsDD.get(FFile.F_B_ALGO));
      was.printTabFieldInput(fsDD.get(FFile.F_B_DIGEST));
      was.printTabFieldInput(fsDD.get(FFile.F_T_SIZE));
      was.printTabFieldInput(fsDD.get(FFile.F_T_CHCNT));
      was.printTabFieldInput(fsDD.get(FFile.F_T_ALGO));
      was.printTabFieldInput(fsDD.get(FFile.F_T_DIGEST));
   was.closeTable();
   // buttons to add child type of records 
   was.printButtonAddRecord(FBlob.TAB_CODE);
   was.printButtonAddRecord(FDir.TAB_CODE);
   was.println();
   //FDir
   int i3;
   was.beginIndent();
   for(i3=0;i3<fsDD.countFS();i3++){
    ZFieldSet fsRW = fsDD.getFS(i3);
    if(!FDir.TAB_CODE.equals(fsRW.getCode())) continue;
    was.beginRecord(); //DW
    was.printSTDFieldsInputDORecord(fsRW);
    was.beginTable();
     was.printTabRecordDeletionStatus(fsRW);
      was.printTabField(fsRW.get(FDir.F_F_ID));
      was.printTabField(fsRW.get(FDir.F_FNAME));
      was.printTabField(fsRW.get(FDir.F_MIMETYPE));
      was.printTabField(fsRW.get(FDir.F_DESCR));
    was.closeTable();
    was.closeRecord(); //DW
   }
   was.closeIndent(); //DD
   //FBlob
   int i2;
   was.beginIndent();
   for(i2=0;i2<fsDD.countFS();i2++){
    ZFieldSet fsDR = fsDD.getFS(i2);
    if(!FBlob.TAB_CODE.equals(fsDR.getCode())) continue;
    was.beginRecord(); //CE
    was.printSTDFieldsInputDORecord(fsDR);
    was.beginTable();
     was.printTabRecordDeletionStatus(fsDR);
      was.printTabField(fsDR.get(FBlob.F_CHUNK));
      was.printTabField(fsDR.get(FBlob.F_NO));
      was.printTabField(fsDR.get(FBlob.F_SIZE));
      was.printTabField(fsDR.get(FBlob.F_CRC32));
    was.closeTable();
    was.closeRecord();
   }
   was.closeIndent();
   //
   was.closeRecord(); //DD
  }
  
  if(i==0) was.println();
  was.printButtonAddRecord(FFile.TAB_CODE);
  was.println();
  was.printButtonMenuBarEdit();
  was.closeRecord(); //DObject
  was.closeForm();
 } //printThisDocEditForm()
 
 // constructors
 public WARHFSFile(
  javax.servlet.http.HttpServletRequest request,
  javax.servlet.http.HttpServletResponse response,
  java.io.PrintWriter out)
 { super(request, response, out); }

 public WARHFSFile(WARequestHandler warh) { super(warh); }

} //WARHFSFile
