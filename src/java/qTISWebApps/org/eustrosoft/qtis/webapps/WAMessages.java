// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

//package ru.mave.ConcepTIS.webapps;
package org.eustrosoft.qtis.webapps;

import java.util.*;

/** central storage of all textual messages used by this package.
* Any displayable text must be obtained over this class object only.
* It's useful for future localization and spelling checks.
*/
public class WAMessages
{
private static Hashtable htLangSet = new Hashtable();
private static boolean is_initialized = false;
private String user_lang = null;

public final static int MSG_ARRAY_STARTED = 0;
public final static int HELLO_WORLD = 1;
public final static int YES = 2;
public final static int NO = 3;
public final static int OK = 4;
public final static int ERROR = 5;
public final static int WARNING = 6;
public final static int SUCCESS = 7;
public final static int INVALID_USERNAME = 8;
public final static int DEBUG_9PARAMS = 9; 
public final static int DEBUG_HTMLUNSAFE_EXAMPLE = 10; 
public final static int REFRESH = 11; 
public final static int EXECUTE = 12; 
public final static int DELETE = 13;
public final static int CANCEL = 14;
public final static int EDIT = 15;
public final static int VIEW = 16;
public final static int LOCK = 17;
public final static int UNLOCK = 18;
public final static int OPEN = 19;
public final static int COMMIT = 20;
public final static int ROLLBACK = 21;
public final static int SAVE = 22;
public final static int GRANT = 23;
public final static int REVOKE = 24;
public final static int SET = 25;
public final static int COMPARE = 26;
public final static int LIST = 27;
public final static int SHOW = 28;
public final static int NEXT = 29;
public final static int FIND = 30;
public final static int NULL = 31;
public final static int TOTAL_ROWS = 32;
public final static int TOTAL_ROWS_CHANGED = 33;
public final static int TOTAL_SQL_REQUESTS_PROCESSED = 34;
public final static int ROWS_SHOWN = 35;
public final static int ROWS_SHOWN_FROM_TO = 36;
public final static int CREATE = 37;
public final static int MANAGE = 38;
public final static int METADATA = 39;
public final static int ADD = 40;
public final static int ADD_RECORD = 41;
public final static int ADD_RECORD_X = 42;
public final static int DELETE_RECORD = 43;
public final static int AFFIX_BASIC = 44;
public final static int AFFIX_NORMAL = 45;
// objects
private final static int OBJ_ = 45; // 
public final static int OBJ_SAMUSER = OBJ_ + 1;
public final static int OBJ_SAMUSERGROUP = OBJ_ + 2;
public final static int OBJ_SAMUSERCAPABILITY = OBJ_ + 3;
public final static int OBJ_SAMGROUP = OBJ_ + 4;
public final static int OBJ_SAMSCOPE = OBJ_ + 5;
public final static int OBJ_SAMSCOPEPOLICY = OBJ_ + 6;
public final static int OBJ_SAMACLSCOPE = OBJ_ + 7;
public final static int OBJ_SAMACLOBJECT = OBJ_ + 8;
public final static int OBJ_SAMAUDITEVENT = OBJ_ + 9;
public final static int OBJ_SAMAUDITLOG = OBJ_ + 10;
public final static int OBJ_DICTIONARY = OBJ_ + 11;
public final static int OBJ_TISOBJECT = OBJ_ + 12;
public final static int OBJ_UNKNOWN = OBJ_ + 13;
public final static int OBJ_TISC_AREA = OBJ_ + 14;
public final static int OBJ_TISC_DOCUMENT = OBJ_ + 15;
public final static int OBJ_TISC_CONTAINER = OBJ_ + 16;
public final static int OBJ_FS_FILE = OBJ_ + 17;
public final static int OBJ_QR_MEMBER = OBJ_ + 18;
public final static int OBJ_QR_PRODUCT = OBJ_ + 19;
public final static int OBJ_QR_CARD = OBJ_ + 20;
public final static int OBJ_QR_QR = OBJ_ + 21;
// captions
private final static int CAPTION_ = OBJ_ + 21; // 
public final static int CAPTION_TOTAL_ITEMS = CAPTION_ + 1;
public final static int CAPTION_DICTIONARY = CAPTION_ + 2;
public final static int CAPTION_CODE = CAPTION_ + 3;
public final static int CAPTION_VALUE = CAPTION_ + 4;
public final static int CAPTION_DESCR = CAPTION_ + 5;
public final static int CAPTION_LANG = CAPTION_ + 6;
public final static int CAPTION_SCOPE = CAPTION_ + 7;
public final static int CAPTION_SLEVEL = CAPTION_ + 8;
public final static int CAPTION_LOCKED = CAPTION_ + 9;
public final static int CAPTION_LOGIN = CAPTION_ + 10;
public final static int CAPTION_DB_USER = CAPTION_ + 11;
public final static int CAPTION_FULL_NAME = CAPTION_ + 12;
public final static int CAPTION_NAME = CAPTION_ + 13;
public final static int CAPTION_SAMGROUP = CAPTION_ + 14;
public final static int CAPTION_USERSCAPABILITIES = CAPTION_ + 15;
public final static int CAPTION_CAPABILITY = CAPTION_ + 16;
public final static int CAPTION_ACL = CAPTION_ + 17;
public final static int CAPTION_ACL_WCDRLH = CAPTION_ + 18;
public final static int CAPTION_OBJECT_TYPE = CAPTION_ + 19;
public final static int CAPTION_ROWS_PER_PAGE = CAPTION_ + 20;
public final static int CAPTION_ROW_OFFSET = CAPTION_ + 21;
public final static int CAPTION_OBJECT_STATUS = CAPTION_ + 22;
public final static int CAPTION_CURRENT_VERSION = CAPTION_ + 23;
public final static int CAPTION_LOCKING_VERSION = CAPTION_ + 24;
public final static int CAPTION_LATEST_VERSION = CAPTION_ + 25;
public final static int CAPTION_ID_MB_ONE_OF_THOSE = CAPTION_ + 26;
public final static int CAPTION_EDIT_INTERMEDIATE = CAPTION_ + 27;
public final static int CAPTION_CREATE_DBOWNER_USER_ONCE = CAPTION_ + 28;
public final static int CAPTION_GET_ZOBJECT_SEQUENCE_VALUE = CAPTION_ + 29;
public final static int CAPTION_SET_ZOBJECT_SEQUENCE_VALUE = CAPTION_ + 30;
// listitem captions
private final static int ITEM_ = CAPTION_ + 30; // 
public final static int ITEM_ANY_SCOPE = ITEM_ + 1;
public final static int ITEM_ANY_OBJECT_TYPE = ITEM_ + 2;
public final static int ITEM_STATUS_ACTIVE = ITEM_ + 3;
public final static int ITEM_STATUS_INTERMEDIATE = ITEM_ + 4;
public final static int ITEM_STATUS_LOCKED = ITEM_ + 5;
public final static int ITEM_STATUS_DELETED = ITEM_ + 6;
public final static int ITEM_STATUS_TRANSIT = ITEM_ + 7;
// informational  messages
private final static int INFO_ = ITEM_ + 7; // 
public final static int INFO_SUCCESS_SAVED = INFO_ + 1; // 
public final static int INFO_SUCCESS_DELETED = INFO_ + 2; // 
public final static int INFO_SUCCESS_CREATED = INFO_ + 3; // 
public final static int INFO_EMPTY_DOBJECT_VIEWED = INFO_ + 4; // 
// questions
private final static int QUEST_ = INFO_ + 4; // 
public final static int QUEST_DELETE_OBJECT_X_WITH_Y_EQ_Z = QUEST_ + 1; // 
public final static int QUEST_DELETE_OBJECT_X_NAMED_Y = QUEST_ + 2; // 
// warnings
private final static int WARN_ = QUEST_ + 2 ;
public final static int WARN_OBJECT_LOCKED_BY_SOMEONE = WARN_ + 1 ;
public final static int WARN_OBJECT_OPENED_BY_SOMEONE = WARN_ + 2 ;
public final static int WARN_OBJECT_OPENED_BY_YOU = WARN_ + 3 ;
public final static int WARN_SAVING_CHANGES_WILL_FAIL = WARN_ + 4 ;
public final static int WARN_FOLLOW_X_2EDIT_OPENED = WARN_ + 5 ;
public final static int WARN_OBJECT_WAS_PREOPENED_BEFORE_EDITION = WARN_ + 6 ;
public final static int WARN_FOLLOW_ONE_OF_TO_FINISH = WARN_ + 7 ;
public final static int WARN_OBJECT_IS_OPENED4EDITION_AND_LOCKED = WARN_ + 8 ;
public final static int WARN_YOU_MUST_COMMIT_OBJECT_MANUALLY = WARN_ + 9 ;
public final static int WARN_NEXT_ACTION_IS_FOR_TESTING = WARN_ + 10 ;
public final static int WARN_REQUESTED_O_NOT_EXISTS = WARN_ + 11 ;
public final static int WARN_REQUESTED_O_X_HAS_ANOTHER_ZOID_Y = WARN_ + 12 ;
public final static int WARN_REQUESTED_O_X_HAS_ANOTHER_ZTYPE_Y = WARN_ + 13 ;
public final static int WARN_FOLLOW_X_TO_EXAMINE_OBJECT = WARN_ + 14 ;
// errors
private final static int ERROR_ = WARN_ + 14 ; // errors are here
public final static int ERROR_WHILE_JDBC_CONNECTION = ERROR_ + 1;
public final static int ERROR_WHILE_JDBC_SQL = ERROR_ + 2;
public final static int ERROR_CALL_NOT_IMPLEMENTED = ERROR_ + 3;
public final static int ERROR_NOT_IMPLEMENTED = ERROR_ + 4;
public final static int ERROR_NOT_FOUND = ERROR_ + 5;
public final static int ERROR_X_NOT_FOUND = ERROR_ + 6;
public final static int ERROR_X_WITH_ID_Y_NOT_FOUND = ERROR_ + 7;
public final static int ERROR_X_WITH_ZOID_Y_NOT_FOUND = ERROR_ + 8;
public final static int ERROR_X_WITH_CODE_Y_NOT_FOUND = ERROR_ + 9;
public final static int ERROR_WHILE_HTTPREDIRECT = ERROR_ + 10;
public final static int ERROR_NO_ACTIVE_SESSION = ERROR_ + 11;
// menus
private final static int MNU_ = ERROR_ + 11; // menus are here 
//
public final static int MNU_MAIN = MNU_ + 5*1;
public final static int MNU_SAM_MAIN = MNU_ + 5*2;
public final static int MNU_TIS_MAIN = MNU_ + 5*3;
public final static int MNU_TISC_MAIN = MNU_ + 5*4;
public final static int MNU_PSQL_MAIN = MNU_ + 5*5;
public final static int MNU_DIC_MAIN = MNU_ + 5*6;
public final static int MNU_HELP_MAIN = MNU_ + 5*7;
// TIS generic actions
public final static int MNU_CREATE = MNU_ + 5*8;
public final static int MNU_DELETE = MNU_ + 5*9;
public final static int MNU_EDIT = MNU_ + 5*10;
public final static int MNU_VIEW = MNU_ + 5*11;
public final static int MNU_LOCK = MNU_ + 5*12;
public final static int MNU_UNLOCK = MNU_ + 5*13;
public final static int MNU_OPEN = MNU_ + 5*14;
public final static int MNU_COMMIT = MNU_ + 5*15;
public final static int MNU_ROLLBACK = MNU_ + 5*16;
public final static int MNU_SAVE = MNU_ + 5*17;
public final static int MNU_CMP = MNU_ + 5*18;
public final static int MNU_MOVE = MNU_ + 5*19;
public final static int MNU_LIST = MNU_ + 5*20;
public final static int MNU_FIND = MNU_ + 5*21;
public final static int MNU_SETSLEVEL = MNU_ + 5*22;
public final static int MNU_SETVDATE = MNU_ + 5*23;
// TIS.QSEQ actions
public final static int MNU_QSEQ_CREATE = MNU_ + 5*24;
public final static int MNU_QSEQ_UPDATE = MNU_ + 5*25;
public final static int MNU_QSEQ_NEXT_BY_QOID = MNU_ + 5*26;
public final static int MNU_QSEQ_NEXT_BY_QSID_QNAME = MNU_ + 5*27;
//
public final static int MNU_TIS_MENU = MNU_ + 5*28;
// LOGIN actions
public final static int MNU_LOGIN_MAIN = MNU_ + 5*29;
// QR/FS 
public final static int MNU_QR_MAIN = MNU_ + 5*30;
public final static int MNU_FS_MAIN = MNU_ + 5*31;

// DIC menus
private final static int MNU_DIC_ = MNU_ + 5*31;
public final static int MNU_DIC_LIST = MNU_DIC_ + 5*1;
public final static int MNU_DIC_VIEW = MNU_DIC_ + 5*2;
// SAM menus
private final static int MNU_SAM_ = MNU_DIC_ + 5*2;
public final static int MNU_SAM_CREATE = MNU_SAM_ + 5*1;
public final static int MNU_SAM_DELETE = MNU_SAM_ + 5*2;
public final static int MNU_SAM_EDIT = MNU_SAM_ + 5*3;
public final static int MNU_SAM_VIEW = MNU_SAM_ + 5*4;
public final static int MNU_SAM_FIND = MNU_SAM_ + 5*5;
public final static int MNU_SAM_GRANTCAP = MNU_SAM_ + 5*6;
public final static int MNU_SAM_REVOKECAP = MNU_SAM_ + 5*7;
public final static int MNU_SAM_GRANTGROUP = MNU_SAM_ + 5*8;
public final static int MNU_SAM_REVOKEGROUP = MNU_SAM_ + 5*9;
public final static int MNU_SAM_SET_ACL = MNU_SAM_ + 5*10;
public final static int MNU_SAM_DELETE_ACL = MNU_SAM_ + 5*11;
public final static int MNU_SAM_SET_POLICY = MNU_SAM_ + 5*12;
public final static int MNU_SAM_DELETE_POLICY = MNU_SAM_ + 5*13;
// SAMUser
public final static int MNU_SAM_USER_LIST = MNU_SAM_ + 5*14;
public final static int MNU_SAM_USER_CREATE = MNU_SAM_ + 5*15;
public final static int MNU_SAM_USER_DELETE = MNU_SAM_ + 5*16;
public final static int MNU_SAM_USER_EDIT = MNU_SAM_ + 5*17;
public final static int MNU_SAM_USER_VIEW = MNU_SAM_ + 5*18;
public final static int MNU_SAM_USER_FIND = MNU_SAM_ + 5*19;
// SAMGroup
public final static int MNU_SAM_GROUP_LIST = MNU_SAM_ + 5*20;
public final static int MNU_SAM_GROUP_CREATE = MNU_SAM_ + 5*21;
public final static int MNU_SAM_GROUP_DELETE = MNU_SAM_ + 5*22;
public final static int MNU_SAM_GROUP_EDIT = MNU_SAM_ + 5*23;
public final static int MNU_SAM_GROUP_VIEW = MNU_SAM_ + 5*24;
public final static int MNU_SAM_GROUP_FIND = MNU_SAM_ + 5*25;
// SAMScope
public final static int MNU_SAM_SCOPE_LIST = MNU_SAM_ + 5*26;
public final static int MNU_SAM_SCOPE_CREATE = MNU_SAM_ + 5*27;
public final static int MNU_SAM_SCOPE_DELETE = MNU_SAM_ + 5*28;
public final static int MNU_SAM_SCOPE_EDIT = MNU_SAM_ + 5*29;
public final static int MNU_SAM_SCOPE_VIEW = MNU_SAM_ + 5*30;
public final static int MNU_SAM_SCOPE_FIND = MNU_SAM_ + 5*31;
// SAMRange
public final static int MNU_SAM_RANGE_LIST = MNU_SAM_ + 5*32;
public final static int MNU_SAM_RANGE_CREATE = MNU_SAM_ + 5*33;
public final static int MNU_SAM_RANGE_DELETE = MNU_SAM_ + 5*34;
public final static int MNU_SAM_RANGE_EDIT = MNU_SAM_ + 5*35;
public final static int MNU_SAM_RANGE_VIEW = MNU_SAM_ + 5*36;
public final static int MNU_SAM_RANGE_FIND = MNU_SAM_ + 5*37;

// TISC subsystem
public final static int MNU_TISC_ = MNU_SAM_ + 5*37;
//AREA
public final static int MNU_TISC_AREA = MNU_TISC_ + 0;
public final static int MNU_TISC_AREA_LIST = MNU_TISC_AREA + 5*1;
public final static int MNU_TISC_AREA_CREATE = MNU_TISC_AREA + 5*2;
public final static int MNU_TISC_AREA_FIND = MNU_TISC_AREA + 5*3;
//CONTAINER
public final static int MNU_TISC_CONTAINER = MNU_TISC_AREA + 5*3;
public final static int MNU_TISC_CONTAINER_LIST = MNU_TISC_CONTAINER + 5*1;
public final static int MNU_TISC_CONTAINER_CREATE = MNU_TISC_CONTAINER + 5*2;
public final static int MNU_TISC_CONTAINER_FIND = MNU_TISC_CONTAINER + 5*3;
//DOCUMENT
public final static int MNU_TISC_DOCUMENT = MNU_TISC_CONTAINER + 5*3;
public final static int MNU_TISC_DOCUMENT_LIST = MNU_TISC_DOCUMENT + 5*1;
public final static int MNU_TISC_DOCUMENT_CREATE = MNU_TISC_DOCUMENT + 5*2;
public final static int MNU_TISC_DOCUMENT_FIND = MNU_TISC_DOCUMENT + 5*3;
// FS Subsystem
public final static int MNU_FS_ = MNU_TISC_DOCUMENT_FIND;
// FS.FILE
public final static int MNU_FS_FILE = MNU_FS_ + 0;
public final static int MNU_FS_FILE_LIST = MNU_FS_FILE + 5*1;
public final static int MNU_FS_FILE_CREATE = MNU_FS_FILE + 5*2;
public final static int MNU_FS_FILE_FIND = MNU_FS_FILE + 5*3;
// QR Subsystem
public final static int MNU_QR_ = MNU_FS_FILE_FIND;
// QR.MEMBER
public final static int MNU_QR_MEMBER = MNU_QR_ + 0;
public final static int MNU_QR_MEMBER_LIST = MNU_QR_MEMBER + 5*1;
public final static int MNU_QR_MEMBER_CREATE = MNU_QR_MEMBER + 5*2;
public final static int MNU_QR_MEMBER_FIND = MNU_QR_MEMBER + 5*3;
// QR.PRODUCT
public final static int MNU_QR_PRODUCT = MNU_QR_ + 5*3*1;
public final static int MNU_QR_PRODUCT_LIST = MNU_QR_PRODUCT + 5*1;
public final static int MNU_QR_PRODUCT_CREATE = MNU_QR_PRODUCT + 5*2;
public final static int MNU_QR_PRODUCT_FIND = MNU_QR_PRODUCT + 5*3;
// QR.CARD
public final static int MNU_QR_CARD = MNU_QR_ + 5*3*2;
public final static int MNU_QR_CARD_LIST = MNU_QR_CARD + 5*1;
public final static int MNU_QR_CARD_CREATE = MNU_QR_CARD + 5*2;
public final static int MNU_QR_CARD_FIND = MNU_QR_CARD + 5*3;
// QR.QR
public final static int MNU_QR_QR = MNU_QR_ + 5*3*3;
public final static int MNU_QR_QR_LIST = MNU_QR_QR + 5*1;
public final static int MNU_QR_QR_CREATE = MNU_QR_QR + 5*2;
public final static int MNU_QR_QR_FIND = MNU_QR_QR + 5*3;


//
public final static int MSG_ARRAY_SIZE = 200;

public final static String LANG_EN = "EN";
public final static String LANG_RU = "RU";
public final static String LANG_DEFAULT = LANG_EN;

private final static String SZ_EMPTY = "";
private final static String SZ_NULL = "null";
private final static String SZ_UNKNOWN = "UNKNOWN";
public final static String SZ_YESNO_N = "N";
public final static String SZ_YESNO_Y = "Y";
public final static String SZ_YESNO_Q = "?";
public final static String SZ_ZSTA_I = "I";
public final static String SZ_ZSTA_N = "N";
public final static String SZ_ZSTA_C = "C";
public final static String SZ_ZSTA_L = "L";

public static final String DIC_CAPABILITY = "CAPABILITY";
public static final String DIC_TIS_FIELD = "TIS_FIELD";
public static final String DIC_TIS_OBJECT_TYPE = "TIS_OBJECT_TYPE";
public static final String DIC_LANG = "LANG";
public static final String DIC_SLEVEL = "SLEVEL";
public static final String DIC_YESNO = "YESNO";
public static final String DIC_ACLA = "ACLA";
public static final String DIC_EVENT_CLASS = "EVENT_CLASS";
public static final String DIC_ZSTA = "ZSTA";
public static final String DIC_FILE_TYPE = "FILE_TYPE";

public static final String SUBSYS_PSQL = "psql";
public static final String SUBSYS_NONE = "none";
public static final String SUBSYS_MAIN = "main";
public static final String SUBSYS_SAM = "SAM";
public static final String SUBSYS_TIS = "TIS";
public static final String SUBSYS_TISC = "TISC";
public static final String SUBSYS_DIC = "DIC";
public static final String SUBSYS_HELP = "help";
// QTIS subsystems
public static final String SUBSYS_COP = "COP"; //Contract/Organization/Person
public static final String SUBSYS_QR = "QR"; // QR-code accounting & description
public static final String SUBSYS_FS = "FS"; // FileSystem structures

public static final String REQUEST_LOGIN = "login";
public static final String REQUEST_LOGON = "logon";
public static final String REQUEST_LOGOFF = "logoff";

public static final String REQUEST_REFRESH = "refresh";
public static final String REQUEST_EXEC = "exec";
public static final String REQUEST_CREATE = "create";
public static final String REQUEST_DELETE = "delete";
public static final String REQUEST_EDIT = "edit";
public static final String REQUEST_EDIT_BASIC = "editbasic";
public static final String REQUEST_VIEW = "view";
public static final String REQUEST_VIEW_BASIC = "viewbasic";
public static final String REQUEST_LOCK = "lock";
public static final String REQUEST_UNLOCK = "unlock";
public static final String REQUEST_OPEN = "open";
public static final String REQUEST_COMMIT = "commit";
public static final String REQUEST_ROLLBACK = "rollback";
public static final String REQUEST_SAVE = "save";
public static final String REQUEST_SAVE_BASIC = "savebasic";
public static final String REQUEST_CMP = "cmp";
public static final String REQUEST_MOVE = "move";
public static final String REQUEST_LIST = "list";
public static final String REQUEST_FIND = "find";
public static final String REQUEST_GRANT = "GRANT";
public static final String REQUEST_REVOKE = "REVOKE";
public static final String REQUEST_SET = "SET";
public static final String REQUEST_SETSLEVEL = "setslevel";
public static final String REQUEST_SETVDATE = "setvdate";
public static final String REQUEST_QSEQ_CREATE = "qseqcreate";
public static final String REQUEST_QSEQ_UPDATE = "qsequpdate";
public static final String REQUEST_QSEQ_NEXT_BY_QOID = "qseqnext_quid";
public static final String REQUEST_QSEQ_NEXT_BY_QSID_QNAME = "qseqnext_qname";
public static final String REQUEST_MNGCAPABILITY = "mngcap";
public static final String REQUEST_MNGGROUP = "mnggroup";
public static final String REQUEST_MNGPASSWORD = "mngpassword";
public static final String REQUEST_MNGACL = "mngACL";
public static final String REQUEST_MNGPOLICY = "mngpolicy";
public static final String REQUEST_SETCHPTS = "setchpts";
public static final String REQUEST_BTNMENU = "btnmenu";
public static final String REQUEST_MENU = "menu";

public static final String TARGET_NONE = "none";
public static final String TARGET_SAMUSER = "XU";
public static final String TARGET_SAMGROUP = "XG";
public static final String TARGET_SAMSCOPE = "XS";
public static final String TARGET_SAMRANGE = "XR";
public static final String TARGET_SAMAUDITLOG = "XA";

public static final String TARGET_AREA = "TISC.A";
public static final String TARGET_DOCUMENT = "TISC.D";
public static final String TARGET_CONTAINER = "TISC.C";

public static final String TARGET_FILE = "FS.F";
public static final String TARGET_FS_FILE = "FS.F";

public static final String TARGET_QR_MEMBER = "QR.M";
public static final String TARGET_QR_PRODUCT = "QR.P";
public static final String TARGET_QR_CARD = "QR.C";
public static final String TARGET_QR_QR = "QR.Q";

public static final String PARAM_SUBSYS = "subsys";
public static final String PARAM_REQUEST = "request";
public static final String PARAM_TARGET = "target";
public static final String PARAM_SQLREQUEST = "SQLRequest";
public static final String PARAM_ZTYPE = "ZTYPE";
public static final String PARAM_ZOID = "ZOID";
public static final String PARAM_ZRID = "ZRID";
public static final String PARAM_ZID = "ZID";
public static final String PARAM_ZVER = "ZVER";
public static final String PARAM_ZSID = "ZSID";
public static final String PARAM_ZSTA = "ZSTA";
public static final String PARAM_FORCE = "FORCE";
public static final String PARAM_KEY = "key";
public static final String PARAM_LANG = "lang";
public static final String PARAM_ROWS_PER_PAGE = "rowsperpage";
public static final String PARAM_ROW_OFFSET = "rowoffset";
public static final String PARAM_DO_CREATE = "do_create"; //instead of update
public static final String PARAM_BTN_EXEC = "btn_exec";
public static final String PARAM_BTN_CREATE = "btn_create";
public static final String PARAM_BTN_REFRESH = "btn_refresh";
public static final String PARAM_BTN_SAVE = "btn_save";
public static final String PARAM_BTN_SAVE_BASIC = "btn_save_basic";
public static final String PARAM_BTN_CANCEL = "btn_cancel";
public static final String PARAM_BTN_DELETE = "btn_delete";
public static final String PARAM_BTN_EDIT = "btn_edit";
public static final String PARAM_BTN_EDIT_BASIC = "btn_edit_basic";
public static final String PARAM_BTN_YES = "btn_yes";
public static final String PARAM_BTN_NO = "btn_no";
public static final String PARAM_BTN_GRANT = "btn_grant";
public static final String PARAM_BTN_REVOKE = "btn_revoke";
public static final String PARAM_BTN_SET = "btn_set";
public static final String PARAM_BTN_LIST = "btn_list";
public static final String PARAM_BTN_SHOW = "btn_show";
public static final String PARAM_BTN_FIND = "btn_find";
public static final String PARAM_BTN_NEXT = "btn_next";
public static final String PARAM_BTN_COMPARE = "btn_cmp";
public static final String PARAM_BTN_VIEW = "btn_view";
public static final String PARAM_BTN_VIEW_BASIC = "btn_view_basic";
public static final String PARAM_BTN_MANAGE = "btn_mng";
public static final String PARAM_BTN_METADATA = "btn_metadata";
// QRService parameters
public static final String PARAM_Q = "q";
public static final String PARAM_P = "p";
public static final String PARAM_D = "d";


public final static String[] msgEN = {
 "", "Hello World!", "Yes", "No", "Ok", "Error", "Warning", "Success",
 "Invalid username (%1)",
 "DEBUG: %1, %2, %3, %4, %5, %6, %7, %8, %9",
 "This is <i>html unsafe</i> test message with &amp;,\nnew line, param1=%1, param2=%2, and so on.",
 "Refresh", "Execute", "Delete", "Cancel", "Edit", "View", "Lock", "Unlock", "Open", "Commit", "Rollback",
 "Save", "Grant", "Revoke", "Set", "Compare",
 "List","Show","Next","Find","null",
 "Total rows : %1",
 "Rows updated (deleted or inserted) : %1",
 "%1 SQL requests processed.",
 "Rows shown : %1",
 "Rows shown : %1 (%2-%3)",
 "Create", "Manage", "Metadata", "Add",
 "Add record", "Add record(%1)", "Delete record",
 "(basic)",
 "(normal)",
 // objects
 "User",
 "SAMUserGroup",
 "Capability",
 "Access group",
 "Scope",
 "SAMScopePolicy",
 "SAMACLScope",
 "SAMACLObject",
 "SAMAuditEvent",
 "Audit log",
 "Dictionary",
 "TISObject",
 "Unknown",
 "Area",
 "Document",
 "Container",
 "File",
/*
 "Member",
 "Product",
 "Card",
 "QR",
*/
 "Участник",
 "Описание продукта",
 "Карточка продажи",
 "QR-карточка",
 // caption
 "Total items",
 "Dictionary",
 "Code",
 "Value",
 "Description",
 "Language",
 "Scope",
 "Secrecy level",
 "Locked",
 "Login",
 "DB user name",
 "Full name",
 "Name",
 "Group",
 "User's capabilities",
 "Capability",
 "Access control list",
 "Access control list (WCDRLH)",
 "Object type",
 "Rows per page",
 "Row offset",
 "Object status",
 "Current version",
 "Locking version",
 "Latest version",
 "ID=%1 field value may be one of those:",
 "<edit intermediate version>",
 "Create user for DB owner (SQL, use it once)",
 "View last value of TIS.ZObject_seq (SQL, DB owner only)",
 "Set start value of TIS.ZObject_seq (SQL, DB owner only)",
 // listitem
 "Any scope",
 "Any object type",
 "Regular objects",
 "Intermediate objects (opened by you)",
 "Locked objects (by any user)",
 "Deleted objects",
 "Objects in transit state",
 // informational  messages
 "Object's data saved successfully",
 "Object successfully deleted",
 "Object successfully created",
 "This data object is empty or its content unavailable for you.",
 // questions
 "Are you sure that you want to delete object '%1' with %2=%3 ?",
 "Are you sure that you want to delete %1 '%2' ?",
 // warnings
 "Warning! This object is locked by someone.",
 "Warning! This object is opened by someone.",
 "Warning! This object is already opened by you.",
 "In most cases you will get fail while saving the changes made below.",
 "Follow %1 to restore intermediate state of your work from the DB.\n" + 
 "(that's useful after crash)",
 "Warning! this object was pre-opened before edition and should be committed manually.",
 "Follow one of the next actions to finish your work:",
 "Warning! this object is opened for edition and locked for any other users.",
 "You must commit or rollback your work manually and you shouldn't keep it locked more than needed.",
 "Warning! The next action form provided for testing and debugging purpose only",
 "Warning! no requested object exists for this action",
 "Warning! Data object '%1' found for this request has different ZOID=%3 than requested one (ZOID=%2)",
 "Warning! Data object '%1' found for this request has different TYPE (%3) than requested one (ZTYPE=%2)",
 "Follow %1 to examine actual state of requested data objects with administrative tool.",
 // errors
 "Error while JDBC connection(login=\"%1\")",
 "Error while JDBC processing SQL query: \"%1\"",
 "Call not implemented, try to use another one.",
 "Not implemented.",
 "Not found.",
 "%1 not found.",
 "%1 with id=%2 not found.",
 "%1 with ZOID=%2 not found.",
 "%1 with code=%2 not found.",
 "Error while sending HTTP redirect: ",
 "No active session with system",
 // menus: "Short caption", "Long caption", SUBSYS_, REQUEST_, TARGET_
 "Main", "Start page of application", SUBSYS_MAIN, null, null,
 "SAM", "SAM subsystem - users, groups, ACL management", SUBSYS_SAM, null, null,
 "TIS", "ConcepTIS generic object related actions a(create/delete, open/commit/rollback, lock/unlock etc.)", SUBSYS_TIS, null, null,
 "TISC", "TISC subsystem - generic ConcepTIS objects (Area,Document,Container)", SUBSYS_TISC, null, null,
 "PSQL ad hoc", "Ad hoc SQL request submitting tool", SUBSYS_PSQL, null, null,
 "Dictionary", "System's dictionaries", SUBSYS_DIC, null, null,
 "Help", "ConcepTIS documentation", SUBSYS_HELP, null, null,
 // TIS
 "CREATE", "Create data object", SUBSYS_TIS, REQUEST_CREATE, null,
 "DELETE", "Delete data object", SUBSYS_TIS, REQUEST_DELETE, null,
 "EDIT", "Edit data object", SUBSYS_TIS, REQUEST_EDIT, null,
 "VIEW", "View data object", SUBSYS_TIS, REQUEST_VIEW, null,
 "LOCK", "Lock data object", SUBSYS_TIS, REQUEST_LOCK, null,
 "UNLOCK", "Unlock data object", SUBSYS_TIS, REQUEST_UNLOCK, null,
 "OPEN", "Open data object", SUBSYS_TIS, REQUEST_OPEN, null,
 "COMMIT", "Commit data object", SUBSYS_TIS, REQUEST_COMMIT, null,
 "ROLLBACK", "Rollback data object", SUBSYS_TIS, REQUEST_ROLLBACK, null,
 "SAVE", "Save data object", SUBSYS_TIS, REQUEST_SAVE, null,
 "CMP", "Compare two versions of data object", SUBSYS_TIS, REQUEST_CMP, null,
 "MOVE", "Move object between scopes", SUBSYS_TIS, REQUEST_MOVE, null,
 "LIST", "List data objects of specified scope", SUBSYS_TIS, REQUEST_LIST, null,
 "FIND", "Find data object", SUBSYS_TIS, REQUEST_FIND, null,
 "SET SLEVEL", "Set data object secrecy level", SUBSYS_TIS, REQUEST_SETSLEVEL, null,
 "SET VDATE", "Set DB time section visible via VD_ views", SUBSYS_TIS, REQUEST_SETVDATE, null,
 "QSEQ CREATE", "Create new QSEQ object", SUBSYS_TIS, REQUEST_QSEQ_CREATE, null,
 "QSEQ UPDATE", "Update QSEQ object", SUBSYS_TIS, REQUEST_QSEQ_UPDATE, null,
 "QSEQ NEXT BY QOID", "Next value from QSEQ object (by QOID)", SUBSYS_TIS, REQUEST_QSEQ_NEXT_BY_QOID, null,
 "QSEQ NEXT BY QNAME", "Next value from QSEQ object (by QNAME)", SUBSYS_TIS, REQUEST_QSEQ_NEXT_BY_QSID_QNAME, null,
 "...", "List of possible TIS actions", SUBSYS_TIS, REQUEST_MENU, null,
 // Login menus
 "Login", "Login to system", SUBSYS_MAIN, REQUEST_LOGIN, null,
 // QR menus
 "QR", "Подсистема QR", SUBSYS_QR, null, null,
 // FS menus
 "FS", "Файловое хранилище", SUBSYS_FS, null, null,
 // DIC menus
 "List", "Show list of all dictionaties", SUBSYS_DIC, REQUEST_LIST, null,
 "View", "Show content of the dictionary", SUBSYS_DIC, REQUEST_VIEW, null,
 // SAM menus
 "Create", "Create SAM object ", SUBSYS_SAM, REQUEST_CREATE, null,
 "Delete", "Delete SAM object", SUBSYS_SAM, REQUEST_DELETE, null,
 "Edit", "Edit SAM object", SUBSYS_SAM, REQUEST_EDIT, null,
 "View", "View SAM object", SUBSYS_SAM, REQUEST_VIEW, null,
 "Find", "Find SAM object", SUBSYS_SAM, REQUEST_FIND, null,
 //
 "Grant capability", "Grant capability to user", SUBSYS_SAM, REQUEST_GRANT, null,
 "Revoke capability", "Revoke capability from user", SUBSYS_SAM, REQUEST_REVOKE, null,
 "Grant group", "Grant access group to user", SUBSYS_SAM, REQUEST_GRANT, null,
 "Revoke group", "Revoke access group from user", SUBSYS_SAM, REQUEST_REVOKE, null,
 "Set ACL", "Set scope ACL", SUBSYS_SAM, REQUEST_SET, null,
 "Delete ACL", "Delete scope ACL", SUBSYS_SAM, REQUEST_DELETE, null,
 "Set policy", "Set scope policy", SUBSYS_SAM, REQUEST_SET, null,
 "Delete policy", "Delete scope policy", SUBSYS_SAM, REQUEST_DELETE, null,
 // SAMUser
 "List Users", "List TIS Users", SUBSYS_SAM, REQUEST_LIST, TARGET_SAMUSER,
 "Create User", "Create new User", SUBSYS_SAM, REQUEST_CREATE, TARGET_SAMUSER,
 "Delete User", "Delete User", SUBSYS_SAM, REQUEST_DELETE, TARGET_SAMUSER,
 "Edit User", "Edit User", SUBSYS_SAM, REQUEST_EDIT, TARGET_SAMUSER,
 "View User", "View User", SUBSYS_SAM, REQUEST_VIEW, TARGET_SAMUSER,
 "Find User", "Find User", SUBSYS_SAM, REQUEST_FIND, TARGET_SAMUSER,
 // SAMGroup
 "List Groups", "List TIS Groups", SUBSYS_SAM, REQUEST_LIST, TARGET_SAMGROUP,
 "Create Group", "Create new Group", SUBSYS_SAM, REQUEST_CREATE, TARGET_SAMGROUP,
 "Delete Group", "Delete Group", SUBSYS_SAM, REQUEST_DELETE, TARGET_SAMGROUP,
 "Edit Group", "Edit Group", SUBSYS_SAM, REQUEST_EDIT, TARGET_SAMGROUP,
 "View Group", "View Group", SUBSYS_SAM, REQUEST_VIEW, TARGET_SAMGROUP,
 "Find Group", "Find Group", SUBSYS_SAM, REQUEST_FIND, TARGET_SAMGROUP,
 // SAMScope
 "List Scopes", "List TIS Scopes", SUBSYS_SAM, REQUEST_LIST, TARGET_SAMSCOPE,
 "Create Scope", "Create new Scope", SUBSYS_SAM, REQUEST_CREATE, TARGET_SAMSCOPE,
 "Delete Scope", "Delete Scope", SUBSYS_SAM, REQUEST_DELETE, TARGET_SAMSCOPE,
 "Edit Scope", "Edit Scope", SUBSYS_SAM, REQUEST_EDIT, TARGET_SAMSCOPE,
 "View Scope", "View Scope", SUBSYS_SAM, REQUEST_VIEW, TARGET_SAMSCOPE,
 "Find Scope", "Find Scope", SUBSYS_SAM, REQUEST_FIND, TARGET_SAMSCOPE,
 // SAMRange
 "List Ranges", "List TIS Ranges", SUBSYS_SAM, REQUEST_LIST, TARGET_SAMRANGE,
 "Create Range", "Create new Range", SUBSYS_SAM, REQUEST_CREATE, TARGET_SAMRANGE,
 "Delete Range", "Delete Range", SUBSYS_SAM, REQUEST_DELETE, TARGET_SAMRANGE,
 "Edit Range", "Edit Range", SUBSYS_SAM, REQUEST_EDIT, TARGET_SAMRANGE,
 "View Range", "View Range", SUBSYS_SAM, REQUEST_VIEW, TARGET_SAMRANGE,
 "Find Range", "Find Range", SUBSYS_SAM, REQUEST_FIND, TARGET_SAMRANGE,
 //TISC
 // TISC.Area
 "List Areas", "List Areas", SUBSYS_TISC, REQUEST_LIST, TARGET_AREA,
 "Create Area", "Create new Area", SUBSYS_TISC, REQUEST_CREATE, TARGET_AREA,
 "Find Area", "Find Area", SUBSYS_TISC, REQUEST_FIND, TARGET_AREA,
 // TISC.Container
 "List Containers", "List Containers", SUBSYS_TISC, REQUEST_LIST, TARGET_CONTAINER,
 "Create Container", "Create new Container", SUBSYS_TISC, REQUEST_CREATE, TARGET_CONTAINER,
 "Find Container", "Find Container", SUBSYS_TISC, REQUEST_FIND, TARGET_CONTAINER,
 // TISC.Document
 "List Documents", "List Documents", SUBSYS_TISC, REQUEST_LIST, TARGET_DOCUMENT,
 "Create Document", "Create new Document", SUBSYS_TISC, REQUEST_CREATE, TARGET_DOCUMENT,
 "Find Document", "Find Document", SUBSYS_TISC, REQUEST_FIND, TARGET_DOCUMENT,
 //FS
 // FS.File
 "List Files", "List Files", SUBSYS_FS, REQUEST_LIST, TARGET_FILE,
 "Create File", "Create new File", SUBSYS_FS, REQUEST_CREATE, TARGET_FILE,
 "Find File", "Find File", SUBSYS_FS, REQUEST_FIND, TARGET_FILE,
 //QR
/* translated to russian
 // QR.Member
 "List Members", "List Members", SUBSYS_QR, REQUEST_LIST, TARGET_QR_MEMBER,
 "Create Member", "Create new Member", SUBSYS_QR, REQUEST_CREATE, TARGET_QR_MEMBER,
 "Find Member", "Find Member", SUBSYS_QR, REQUEST_FIND, TARGET_QR_MEMBER,
 // QR.Product
 "List Products", "List Products", SUBSYS_QR, REQUEST_LIST, TARGET_QR_PRODUCT,
 "Create Product", "Create new Product", SUBSYS_QR, REQUEST_CREATE, TARGET_QR_PRODUCT,
 "Find Product", "Find Product", SUBSYS_QR, REQUEST_FIND, TARGET_QR_PRODUCT,
 // QR.Card
 "List Cards", "List Cards", SUBSYS_QR, REQUEST_LIST, TARGET_QR_CARD,
 "Create Card", "Create new Card", SUBSYS_QR, REQUEST_CREATE, TARGET_QR_CARD,
 "Find Card", "Find Card", SUBSYS_QR, REQUEST_FIND, TARGET_QR_CARD,
 // QR.QR
 "List QRs", "List QRs", SUBSYS_QR, REQUEST_LIST, TARGET_QR_QR,
 "Create QR", "Create new QR", SUBSYS_QR, REQUEST_CREATE, TARGET_QR_QR,
 "Find QR", "Find QR", SUBSYS_QR, REQUEST_FIND, TARGET_QR_QR,
*/
 // QR.Member
 "Список Участников", "Список организаций Участников (контрагента)", SUBSYS_QR, REQUEST_LIST, TARGET_QR_MEMBER,
 "Создать Участника", "Создать организацию-Участника (контрагента)", SUBSYS_QR, REQUEST_CREATE, TARGET_QR_MEMBER,
 "Найти Участника", "Найти организацию-Участника (контрагента)", SUBSYS_QR, REQUEST_FIND, TARGET_QR_MEMBER,
 // QR.Product
 "Список Продуктов", "Список типов Продуктов/Моделей", SUBSYS_QR, REQUEST_LIST, TARGET_QR_PRODUCT,
 "Создать Продукт", "Создать описание Продукт/Модель", SUBSYS_QR, REQUEST_CREATE, TARGET_QR_PRODUCT,
 "Найти Продукт", "Найти Продукт/Модель", SUBSYS_QR, REQUEST_FIND, TARGET_QR_PRODUCT,
 // QR.Card
 "Список Продаж", "Список Карточек продажи/поставки", SUBSYS_QR, REQUEST_LIST, TARGET_QR_CARD,
 "Создать Продажу", "Создать Карточку продажи/поставки", SUBSYS_QR, REQUEST_CREATE, TARGET_QR_CARD,
 "Найти Продажу", "Найти Карточку продажи/поставки", SUBSYS_QR, REQUEST_FIND, TARGET_QR_CARD,
 // QR.QR
 "Список QR", "Список Карточек QR (публичных)", SUBSYS_QR, REQUEST_LIST, TARGET_QR_QR,
 "Создать QR", "Создать Карточку QR (публичную)", SUBSYS_QR, REQUEST_CREATE, TARGET_QR_QR,
 "Найти QR", "Найти Карточку QR", SUBSYS_QR, REQUEST_FIND, TARGET_QR_QR,

 null // end of messages array
	};
public final static String[] msgDefault = msgEN;
public static String msgRU[] = new String[MSG_ARRAY_SIZE];

//
// SQL queries section (library of queries for future porting)
//

public final static int SQL_PING = 0;
public final static int SQL_PING_TIS = 1;
public final static int SQL_LIST_DICTIONARIES = 2;
public final static int SQL_LOAD_DICTIONARY = 3;
public final static int SQL_LOAD_DICTIONARY_MSG4LANG = 4; // only existing loclzd msg
public final static int SQL_LOAD_DICTIONARY4LANG = 5; // all codes with loclzd or deflt msgs
public final static int SQL_LIST_USERS = 6;
public final static int SQL_LOAD_USER = 7;
public final static int SQL_CREATE_DBOWNER_USER_ONCE = 8;
public final static int SQL_GET_ZOBJECT_SEQUENCE_VALUE = 9;
public final static int SQL_SET_ZOBJECT_SEQUENCE_VALUE = 10;

public final static String[] SQLQueries = {
 "select user",
 "select sam.get_user()",
 "select dic, count(*) from dic.V_ZDic group by dic order by dic",
 "select dic,code,value,descr from dic.V_ZDic where dic = ?",
 "select dic,code,value,descr from dic.V_ZCodeMsg where dic = ? and lang = ?",
 "select ZD.dic, ZD.code, coalesce(ZC.value,ZD.value) as \"value\", " + 
  "coalesce(ZC.descr,ZD.descr) as \"descr\" from dic.V_ZDic ZD " +
  "LEFT OUTER JOIN  dic.V_ZCodeMsg ZC ON (ZD.dic = ZC.dic and " + 
  "ZD.code = ZC.code and ZC.lang = ?) where ZD.dic = ?",
 "select * from SAM.V_User order by login",
 "select * from SAM.V_User where id = ?",
 "select SAM.create_DBOwnerUserOnce()",
 "select setval('TIS.ZObject_seq',nextval('TIS.ZObject_seq'),false)",
 "select setval('TIS.ZObject_seq','put_new_start_value_here',false)",
 null
}; //SQLQueries

/** get specific query from the library of queries.
 * Could be useful to porting to another DBMS.
 */
public String getSQL(int query_id)
{
return(SQLQueries[query_id]);
} // getSQL()

/** set language code for future message gaining.
* @see #getLang
* @see #LANG_RU
* @see #LANG_EN
*/
public void setLang(String lang){user_lang=lang;}
public String getLang(){ return(user_lang); }


/** get short menu caption message for current language (plain text).
* @see #setLang
* @see #getMsgText
*/
public String getMnuCaption(int menu_id)
{ return(getMsgText(menu_id - 4)); }
/** get long menu descriptive message (plain text). */
public String getMnuDesc(int menu_id)
{ return( getMsgText(menu_id - 3)); }
/** get menu subsystem code (plain text, language invariant). */
public String getMnuSubsys(int menu_id)
{ return( getMsgText(menu_id - 2)); }
/** get menu request code value (plain text, language invariant). */
public String getMnuRequest(int menu_id)
{ return( getMsgText(menu_id - 1)); }
/** get menu request target code value (plain text, language invariant). */
public String getMnuTarget(int menu_id)
{ return( getMsgText(menu_id)); }

/** get message for current language (html safe).
* @see #setLang
*/
public String getMsg(int msg_id)
{
 return(obj2html(getMsgText(msg_id)));
} // getMsg

public String getMsg(int msg_id,String arg1)
{ return( getMsg(msg_id,new String[]{arg1}) ); }

public String getMsg(int msg_id,String arg1, String arg2)
{ return( getMsg(msg_id,new String[]{arg1, arg2}) ); }

public String getMsg(int msg_id,String arg1, String arg2, String arg3)
{ return( getMsg(msg_id,new String[]{arg1, arg2, arg3}) ); }

/** get plain text message for current language (html unsafe).
* @see #setLang
* @see #getMsg
*/
public String getMsgText(int msg_id)
{
 return(getMsg4Lang(user_lang,msg_id));
} // getMsg

public String getMsgText(int msg_id,String arg1)
{ return( getMsgText(msg_id,new String[]{arg1}) ); }

public String getMsgText(int msg_id,String arg1, String arg2)
{ return( getMsgText(msg_id,new String[]{arg1, arg2}) ); }

public String getMsgText(int msg_id,String arg1, String arg2, String arg3)
{ return( getMsgText(msg_id,new String[]{arg1, arg2, arg3}) ); }

private final static String[] PARAM_TOKENS =
 { "%1", "%2", "%3", "%4", "%5", "%6", "%7", "%8", "%9" };
// this could be useful later, but args[] must be preceded by "%"
// somwhere internally in the getMsgText(int msg_id,String[] args)
// { "%%","%1", "%2", "%3", "%4", "%5", "%6", "%7", "%8", "%9" };
/** get message with positional parameters replased by passed args.
* upto nine positional parameters supported (%1, %2, ..., %9)
*/
public String getMsg(int msg_id,String[] args)
{
 return(obj2html(getMsgText(msg_id,args)));
} // getMsg(int msg_id,String[] args)

/** plain text (html unsafe) version of getMsg(int msg_id,String[] args).
 * @see #getMsg
 */
public String getMsgText(int msg_id,String[] args)
{
 return(translate_tokens(getMsgText(msg_id),PARAM_TOKENS,args,args.length));
} // getMsgText(int msg_id,String[] args)

/** get message as html-safe with html-capable args. 
* @see #getMsg(int msg_id,String[] args)
*/
public String getMsgHtml(int msg_id,String[] args)
{
 return(translate_tokens(obj2html(getMsgText(msg_id)),
	PARAM_TOKENS,args,args.length));
} // getMsgHtml(int msg_id,String[] args)

public String getMsgHtml(int msg_id,String arg1)
{ return( getMsgHtml(msg_id,new String[]{arg1}) ); }

public String getMsgHtml(int msg_id,String arg1, String arg2)
{ return( getMsgHtml(msg_id,new String[]{arg1, arg2}) ); }

public String getMsgHtml(int msg_id,String arg1, String arg2, String arg3)
{ return( getMsgHtml(msg_id,new String[]{arg1, arg2, arg3}) ); }

public static String getMsg4Lang(String lang,int msg_id)
{
 String msg=null;
 // once at the future:
 String[] msgSet = (String[])htLangSet.get(lang);
 if(msgSet != null) msg=msgSet[msg_id]; else
 if(LANG_EN.equals(lang)) msg=msgEN[msg_id];
 else if(LANG_RU.equals(lang)) msg=msgRU[msg_id];
 if(msg==null) msg=msgDefault[msg_id];
 return(msg);
} // getMsg4Lang

public static String getQTYPESubsys(String QTYPE)
{
if(QTYPE==null)return(null);
String[] parts = QTYPE.split("\\.");
return(parts[0]);
}

//
// static conversion helpful functions
// obj2text(), obj2html(), obj2value() - useful functions
// translate_tokens() - background work for them
//

 /** convert object to text even if object is null.
 */
 public static String obj2text(Object o)
 {
 if(o == null) return(SZ_NULL); return(o.toString());
 }

 /** convert object to text but preserve null value if so.
 * @see obj2text
 */
 public static String obj2string(Object o)
 {
 if(o == null) return(null); return(obj2text(o));
 }

 /** convert object to html text even if object is null.
 * @see #obj2text
 * @see #text2html
 */
 public static String obj2html(Object o)
 {
  if(o == null) return("<STRIKE><small>null</small></STRIKE>");
  else return(text2html(obj2text(o)));
 }

 /** convert szValue to Long object it's represents or into null value
  * if conversion failed.
 */
 public static Long string2Long(String szValue)
 {
 if(szValue == null) return(null);
 Long v=null; try{v=Long.valueOf(szValue);} catch(NumberFormatException e){}
 return(v);
 }

 //
 public final static String[] HTML_UNSAFE_CHARACTERS = {"<",">","&","\n"};
 public final static String[] HTML_UNSAFE_CHARACTERS_SUBST = {"&lt;","&gt;","&amp;","<br>\n"};
 public final static String[] VALUE_CHARACTERS = { "<",">","&","\"","'" };
 public final static String[] VALUE_CHARACTERS_SUBST = {"&lt;","&gt;","&amp;","&quot;","&#039;"};
 public final static String[] JSON_VALUE_CHARACTERS = { "\n","\r","\"","\\" };
 public final static String[] JSON_VALUE_CHARACTERS_SUBST = {"\\n","\\r","\\","\\\\"};

 /** convert plain textual data into html code with escaping unsafe symbols.
  * @param text - plain text
  * @return html escaped text
  */
 public static String text2html(String text)
 {
 return(translate_tokens(text,HTML_UNSAFE_CHARACTERS,HTML_UNSAFE_CHARACTERS_SUBST));
 } // text2html()

 /** convert plain textual data into html form value suitable for input or textarea fields.
  * @param text - plain text
  * @return escaped text
  */
 public static String text2value(String text)
 {
 return(translate_tokens(text,VALUE_CHARACTERS,VALUE_CHARACTERS_SUBST));
 } // text2value()

 public static String text2json(String text)
 {
 return("\"" + translate_tokens(text,JSON_VALUE_CHARACTERS,JSON_VALUE_CHARACTERS_SUBST) + "\"");
 }

 public static String obj2value(Object o)
 {
 return(text2value(obj2text(o)));
 }


 /** replace all sz's occurrences of 'from[x]' onto 'to[x]' and return the result.
  * Each occurence processed once and result depend on token's order at 'from'. 
  * For instance: translate_tokens("hello",new String[]{"he","hel","hl"}, new String[]{"eh","leh","lh"})
  * give "ehllo", not "lehlo" or "elhlo" (in fact "hel" to "leh" translation never be done).
  * @param sz - string for translation
  * @param from - array of tokens to search
  * @param to - array of tokens to translate to
  * @param len - the number of tokens at "from" to look. use -1 to look for all
  *   actually len = min(len,from.length) if len >=0 and len = from.length otherwise
  *	
  */
 public static String translate_tokens(String sz, String[] from, String[] to, int len)
 {
  if(sz == null) return(sz);
  StringBuffer sb = new StringBuffer(sz.length() + 256);
  int p=0;
  if(len<0) len=from.length;
  //if(len>to.length) len=to.length; // let's
  while(p<sz.length())
  {
  int i=0;
  while(i<len) // search for token
  {
   if(sz.startsWith(from[i],p)) { sb.append(to[i]); p=--p +from[i].length(); break; }
   i++;
  }
  if(i>=len) sb.append(sz.charAt(p)); // not found
  p++;
  }
  return(sb.toString());
 } // translate_tokens

 public static String translate_tokens(String sz, String[] from, String[] to)
 {return(translate_tokens(sz,from,to,-1));}

/** this method could be extended to support loadable language arrays at the future.
*/
private static synchronized void load_localized_messages()
{
 if(!is_initialized){
 // String[] msgRU=new String[msgDefault.length];
 // msgRU[HELLO_WORLD]="Privet Mir!";
 // htLangSet.put(LANG_RU,msgRU);
 is_initialized = true; // to run this code only once
 }
}

// constructors
public WAMessages(){ this(LANG_DEFAULT); }
public WAMessages(String lang){ load_localized_messages(); setLang(lang);}
} //WAMessages
