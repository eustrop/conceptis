// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

//package ru.mave.ConcepTIS.webapps;
package org.eustrosoft.qtis.webapps.DIC;

import java.util.*;
import java.sql.*;
import ru.mave.ConcepTIS.dao.*;
import org.eustrosoft.qtis.webapps.*;

public class WARHDictionary
	extends WARequestHandler
{
 // interfaces/abstracts implementation

 public void processREST() { was.printRESTNotImplemented(); }
 
 public void process()
 {
  String req = request.getParameter(wam.PARAM_REQUEST);
  if(req == null) req = wam.REQUEST_LIST;
  was.beginHMenu();
  was.printHMenuSeparator();
  was.printHMenuCaption(wam.getMnuDesc(wam.MNU_DIC_MAIN)+":");
  was.printHMenuItem(wam.MNU_DIC_LIST);
  was.closeHMenu();
  was.printSeparator();
  try{ db_logon(); } catch(Exception e){printerrmsg(e); return; }
  try{
   if(wam.REQUEST_VIEW.equals(req))  do_view_dictionary(); 
   else if(wam.REQUEST_LIST.equals(req)) do_list_dictionaries();
   else throw(new WAException(wam,wam.ERROR_NOT_IMPLEMENTED));
  }catch(Exception e){printerrmsg(e); return; }
  finally{ db_logoff(); } 
 } // process

 /** show list of all dictionaries. */
 private void do_list_dictionaries()
  throws java.sql.SQLException
 {
   ResultSet rs = getZSystem().execSQL(wam.getSQL(wam.SQL_LIST_DICTIONARIES));
   // WARHPSQL warh = new WARHPSQL(this); warh.print_sql_rs(rs);
   int did=was.beginDocument(); try{
   was.beginTable();
    was.beginTableRow();
     was.printHCellValue(wam.getMsgText(wam.CAPTION_DICTIONARY));
     was.printHCellValue(wam.getMsgText(wam.CAPTION_TOTAL_ITEMS));
    was.closeTableRow();
   while(rs.next())
   {
    was.beginTableRow();
    String dic = Record.getRSString(rs,1);
    Long count = Record.getRSLong(rs,2);
    was.printCellHRef(dic,wam.SUBSYS_DIC,wam.REQUEST_VIEW,
	new String[]{wam.PARAM_KEY},
	new String[]{dic});
    was.printCellValue(wam.obj2string(count));
    was.closeTableRow();
   }
   was.closeTable();
   }finally{was.closeDocument(did);getZSystem().closeRS(rs);}
 } // do_list_dictionaries()

 /** show dictionary content. */
 private void do_view_dictionary()
  throws java.sql.SQLException
 {
  String key = request.getParameter(wam.PARAM_KEY);
  //PreparedStatement ps = getZSystem().getDBC().prepareStatement(wam.getSQL(wam.SQL_LOAD_DICTIONARY));
  PreparedStatement ps = getZSystem().getPS4SQL(wam.getSQL(wam.SQL_LOAD_DICTIONARY));
  int did=was.beginDocument();
  try{
   ps.setString(1,key);
   ResultSet rs = ps.executeQuery();
   was.print(wam.getMsgText(wam.CAPTION_DICTIONARY));
   was.print(":");
   was.println(key);
   /*
   was.printCellHRef(dic,wam.SUBSYS_DIC,wam.REQUEST_VIEW,
	new String[]{wam.PARAM_KEY},
	new String[]{key});
   */
   was.beginTable();
    was.beginTableRow();
     // was.printHCellValue(wam.getMsgText(wam.CAPTION_DICTIONARY));
     was.printHCellValue(wam.getMsgText(wam.CAPTION_CODE));
     was.printHCellValue(wam.getMsgText(wam.CAPTION_VALUE));
     was.printHCellValue(wam.getMsgText(wam.CAPTION_DESCR));
    was.closeTableRow();
   while(rs.next())
   {
    was.beginTableRow();
    String dic = Record.getRSString(rs,1);
    String code = Record.getRSString(rs,2);
    String value = Record.getRSString(rs,3);
    String descr = Record.getRSString(rs,4);
    was.printCellValue(code);
    was.printCellValue(value);
    if( !"SQL".equals(key) )
    was.printCellValue(descr);
    else
    was.printCellValueCustomPSQL(descr,descr);
    was.closeTableRow();
   }
   was.closeTable();
  }finally{was.closeDocument(did);getZSystem().closePS(ps);}
 } // do_view_dictionary()
 /** search for code or dictionary. */
 private void do_search()
 {
 } // do_search()

 private void print_form_search()
 {
 } // print_form_search()

 // constructors
 public WARHDictionary(
	javax.servlet.http.HttpServletRequest request,
	javax.servlet.http.HttpServletResponse response,
	java.io.PrintWriter out)
 { super(request,response,out); }
 public WARHDictionary(WARequestHandler warh) { super(warh); }

} //WARHDictionary
