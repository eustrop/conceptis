// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

//package ru.mave.ConcepTIS.webapps;
package org.eustrosoft.qtis.webapps.QR;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
//import org.eustrosoft.qtis.webapps.DIC.*;
//import org.eustrosoft.qtis.webapps.SAM.*;
//import org.eustrosoft.qtis.webapps.TIS.*;
//import org.eustrosoft.qtis.webapps.FS.*;
import org.eustrosoft.qtis.webapps.*;
import org.eustrosoft.qdbp.*;
import org.eustrosoft.qtis.SessionCookie.*;


public class WARHQRSubsys extends WARequestHandler{
    private boolean showTopMenu = false; // show topMenu() at each page
    public boolean setTopMenu(boolean v) { boolean ov = v; showTopMenu = v; return(ov); }


    /** find, create and return specific request handler using request's parameters (subsys, request, target)
     * 
     */
    public WARequestHandler findRequestHandler()
    {
     String pSubsys = request.getParameter(wam.PARAM_SUBSYS);
     String pRequest = request.getParameter(wam.PARAM_REQUEST);
     String pTarget = request.getParameter(wam.PARAM_TARGET);
     String pZID = request.getParameter(wam.PARAM_ZID);
     Long ZID = wam.string2Long(pZID);
     WARequestHandler warh = null;
     if(wam.SUBSYS_QR.equals(pSubsys))
     {
       //warh = new WARHPSQL(this);
       if(wam.TARGET_QR_MEMBER.equals(pTarget)) warh = new WARHQRMember(this);
       else if(wam.TARGET_QR_PRODUCT.equals(pTarget)) warh=new WARHQRProduct(this);
       else if(wam.TARGET_QR_CARD.equals(pTarget)) warh=new WARHQRCard(this);
       else if(wam.TARGET_QR_QR.equals(pTarget)) warh=new WARHQRQR(this);
     }
     //else if(wam.SUBSYS_DIC.equals(pSubsys)) warh = new WARHDictionary(this);
     //else if(wam.SUBSYS_TIS.equals(pSubsys)) { warh = new WARHTISObject(this); }
     return(warh);
    } // findRequestHandler()


    public void process(){

        String pZID = request.getParameter(wam.PARAM_ZID);
        Long ZID = wam.string2Long(pZID);
        WARequestHandler warh = findRequestHandler();
	if(showTopMenu) topMenu();
        if(warh != null) warh.process();
	else{ menuQR(); }
    } // process()
    public void processREST(){} // SIC! mb for deletion

    public void topMenu(){
	 was.beginHMenu();
	 was.printHMenuItem(wam.MNU_MAIN);
	 was.printHMenuItem(wam.MNU_SAM_MAIN);
	 was.printHMenuItem(wam.MNU_TISC_MAIN);
	 was.printHMenuItem(wam.MNU_DIC_MAIN);
	 was.printHMenuItem(wam.MNU_PSQL_MAIN);
	 was.closeHMenu();
   } //topMenu()


    public void menuQR(){

        was.beginTMenu();
        //was.printTMenuCurrent(wam.MNU_QR_MAIN);
        was.beginTMenu();
        was.printTMenuItem(wam.MNU_QR_MEMBER_LIST);
        was.printTMenuItem(wam.MNU_QR_MEMBER_CREATE);
        was.printTMenuItem(wam.MNU_QR_MEMBER_FIND);
        was.printTMenuSeparator();
        was.printTMenuItem(wam.MNU_QR_PRODUCT_LIST);
        was.printTMenuItem(wam.MNU_QR_PRODUCT_CREATE);
        was.printTMenuItem(wam.MNU_QR_PRODUCT_FIND);
        was.printTMenuSeparator();
        was.printTMenuItem(wam.MNU_QR_CARD_LIST);
        was.printTMenuItem(wam.MNU_QR_CARD_CREATE);
        was.printTMenuItem(wam.MNU_QR_CARD_FIND);
        was.printTMenuSeparator();
        was.printTMenuItem(wam.MNU_QR_QR_LIST);
        was.printTMenuItem(wam.MNU_QR_QR_CREATE);
        was.printTMenuItem(wam.MNU_QR_QR_FIND);
        was.closeTMenu();
        was.closeTMenu();
    } //menuQR

    // constructors
    public WARHQRSubsys(HttpServletRequest request,HttpServletResponse response,java.io.PrintWriter out){
        super(request,response,out);
    } // WARHQRSubsys(HttpServletRequest,JspWriter)

    public WARHQRSubsys(WARequestHandler warh) { super(warh); }

} //WARHQRSubsys
