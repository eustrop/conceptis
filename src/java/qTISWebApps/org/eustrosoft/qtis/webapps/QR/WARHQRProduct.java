// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

//package ru.mave.ConcepTIS.webapps;
package org.eustrosoft.qtis.webapps.QR;

import java.util.*;
import java.sql.*;
import ru.mave.ConcepTIS.dao.*;
import ru.mave.ConcepTIS.dao.TIS.*;
import ru.mave.ConcepTIS.dao.QR.*;
import ru.mave.ConcepTIS.dao.SAM.*;
import org.eustrosoft.qtis.webapps.*;

public class WARHQRProduct extends WARHTypicalObject
{
 public static final String SUBSYS_THIS = WAMessages.SUBSYS_QR;
 public static final String TARGET_THIS = WAMessages.TARGET_QR_PRODUCT;

 public String getThisDocSubsys(){return(SUBSYS_THIS); }
 public String getThisDocTarget(){return(TARGET_THIS); }
 public int getThisDocTypeCaptionID() { return(wam.OBJ_QR_PRODUCT); }
 public DObject newThisDocDObject() { DObject o = Product.newDObject(); o.addRecord(new PProduct()); return(o); }

 public void printThisDocGeneralHMenu()
 {
  was.printHMenuItem(wam.MNU_QR_PRODUCT_LIST);
  was.printHMenuItem(wam.MNU_QR_PRODUCT_CREATE);
  was.printHMenuItem(wam.MNU_QR_PRODUCT_FIND);
 }

 public RList loadThisDocList(Long ZSID, int rows_per_page, long row_offset)
   throws java.sql.SQLException,ZException
 { return( getZSystem().listQRProduct(ZSID,rows_per_page,row_offset) ); }

 public DObject loadThisDocDObject(Long ZOID)
   throws java.sql.SQLException,ZException
 { return(getZSystem().loadQRProduct(ZOID)); }

 public DObject extractThisDocDObject(ZFieldSet fs)
   throws java.sql.SQLException,ZException
 {DObject o=Product.newDObject();o.getFromFS(fs);return(o);}

 public void printThisDocView(DObject o)
 {
  RList rl = o.getTable(PProduct.TAB_CODE);
  RList rl_scopes=null; // list of SAMScopes
  try{ rl_scopes = getZSystem().listSAMScopes(); } catch(Exception e) { printerrmsg(e);}
  ZDictionary dicSLEVEL = getZSystem().getDictionary(wam.DIC_SLEVEL);
  load_DocReferences(o);
   if(rl.size() < 1) { // for empty objects
    was.printMsg(wam.INFO_EMPTY_DOBJECT_VIEWED);
    was.beginTable();
    was.printTabField(ZName.F_ZNAME,o.getZNAME());
    was.printTabField(ZObject.F_ZOID,wam.obj2string(o.getZOID()));
    was.printTabFieldDic(new ZField(ZObject.F_ZTYPE, o.getZTYPE()),
	getDic(wam.DIC_TIS_OBJECT_TYPE));
    was.closeTable();
   }
   // header
   for(int i=0;i<rl.size();i++)
   {
   ZFieldSet fs=((DORecord)(rl.get(i))).toFieldSet();
   was.beginTable();
// view record QR.PProduct
was.printTabField(fs,PProduct.F_PRODTYPE);
was.printTabField(fs,PProduct.F_PRODMODEL);
was.printTabField(fs,PProduct.F_PMREVISION);
was.printTabField(fs,PProduct.F_PRODPART);
was.printTabFieldZOID2Descr(fs,PProduct.F_DIR_ID,getZNames4Field(PProduct.FC_DIR_ID),wam.SUBSYS_QR);
was.printTabFieldDic(fs,PProduct.F_LANG,getDic("LANG"));
was.printTabField(fs,PProduct.F_TITLE);
was.printTabFieldOWiki(fs,PProduct.F_OWIKI);

   was.closeTable();
   } //header
 } // printDocumentView

 public void printThisDocEditForm(ZFieldSet fs)
 {
  int i;
  PProduct PP = null;
  RList rl_scopes = getSAMScopes(); // exceptions are ignored!
  RList rl_DIR_ID = null; 
  //try{ rl_DIR_ID = getZSystem().listFSFile(null /*ZSID*/,4096,0); } catch(Exception e) { printerrmsg(e);}
  for(i=0;i<fs.countFS();i++)
  {
   ZFieldSet fsPP = fs.getFS(i);
   if(!PProduct.TAB_CODE.equals(fsPP.getCode())) continue;
   was.beginRecord(); //PP
   was.printSTDFieldsInputDORecord(fsPP);
   //was.printFieldInputHidden(fsPP.get(r.F_ID));
   //was.printRecordDeletionStatus(fsPP);
   was.beginTable();
    //was.printTabRecordDeletionStatus(fsPP);
// edit record QR.PProduct
was.printTabFieldInput(fsPP.get(PP.F_PRODTYPE));
was.printTabFieldInput(fsPP.get(PP.F_PRODMODEL));
was.printTabFieldInput(fsPP.get(PP.F_PMREVISION));
was.printTabFieldInput(fsPP.get(PP.F_PRODPART));
was.printTabFieldInputList(fsPP.get(PP.F_DIR_ID),rl_DIR_ID);
was.printTabFieldInputDic(fsPP.get(PP.F_LANG),getDic("LANG"));
was.printTabFieldInput(fsPP.get(PP.F_TITLE));
was.printTabFieldInputOWiki(fsPP.get(PP.F_OWIKI));

   was.closeTable();
   was.closeRecord(); //PP
  }
  if(i==0)was.println();
  //was.printButtonAddRecord(PProduct.TAB_CODE);was.println();
  //was.print(wam.getMsgText(wam.ADD_RECORD));
  //was.printButtonAddRecord(PProduct.TAB_CODE,PProduct.TAB_CODE);was.println();
  was.printButtonMenuBarEdit();
  was.closeRecord(); //DObject
  was.closeForm();
 } //printThisDocEditForm

 // constructors
 public WARHQRProduct(
	javax.servlet.http.HttpServletRequest request,
	javax.servlet.http.HttpServletResponse response,
	java.io.PrintWriter out)
 { super(request,response,out); }
 public WARHQRProduct(WARequestHandler warh) { super(warh); }

} //WARHQRProduct
