// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

//package ru.mave.ConcepTIS.webapps;
package org.eustrosoft.qtis.webapps.QR;

import java.util.*;
import java.sql.*;
import ru.mave.ConcepTIS.dao.*;
import ru.mave.ConcepTIS.dao.TIS.*;
import ru.mave.ConcepTIS.dao.QR.*;
import ru.mave.ConcepTIS.dao.SAM.*;
import org.eustrosoft.qtis.webapps.*;

public class WARHQRMember extends WARHTypicalObject
{
 public static final String SUBSYS_THIS = WAMessages.SUBSYS_QR;
 public static final String TARGET_THIS = Member.DOBJECT_CODE;

 public String getThisDocSubsys(){return(SUBSYS_THIS); }
 public String getThisDocTarget(){return(TARGET_THIS); }
 public int getThisDocTypeCaptionID() { return(wam.OBJ_QR_MEMBER); }
 public DObject newThisDocDObject() { DObject o = Member.newDObject(); o.addRecord(new MMember()); return(o); }

 public void printThisDocGeneralHMenu()
 {
  was.printHMenuItem(wam.MNU_QR_MEMBER_LIST);
  was.printHMenuItem(wam.MNU_QR_MEMBER_CREATE);
  was.printHMenuItem(wam.MNU_QR_MEMBER_FIND);
 }

 public RList loadThisDocList(Long ZSID, int rows_per_page, long row_offset)
   throws java.sql.SQLException,ZException
 { return( getZSystem().listQRMember(ZSID,rows_per_page,row_offset) ); }

 public DObject loadThisDocDObject(Long ZOID)
   throws java.sql.SQLException,ZException
 { return(getZSystem().loadQRMember(ZOID)); }

 public DObject extractThisDocDObject(ZFieldSet fs)
   throws java.sql.SQLException,ZException
 {DObject o = Member.newDObject(); o.getFromFS(fs);return(o);}

 public void printThisDocView(DObject o)
 {
  RList rl = o.getTable(MMember.TAB_CODE);
  RList rl_scopes=null; // list of SAMScopes
  try{ rl_scopes = getZSystem().listSAMScopes(); } catch(Exception e) { printerrmsg(e);}
  load_DocReferences(o);
  ZDictionary dicSLEVEL = getZSystem().getDictionary(wam.DIC_SLEVEL);
   if(rl.size() < 1) { // for empty objects
    was.printMsg(wam.INFO_EMPTY_DOBJECT_VIEWED);
    was.beginTable();
    was.printTabField(ZName.F_ZNAME,o.getZNAME());
    was.printTabField(ZObject.F_ZOID,wam.obj2string(o.getZOID()));
    was.printTabFieldDic(new ZField(ZObject.F_ZTYPE, o.getZTYPE()),
	getDic(wam.DIC_TIS_OBJECT_TYPE));
    was.closeTable();
   }
   for(int i=0;i<rl.size();i++)
   {
   // header
   DORecord dor =((DORecord)(rl.get(i)));
   ZFieldSet fs = dor.toFieldSet();
   was.beginTable();
   //was.printTabField(fs,MMember.F_NUM);
   //was.printTabField(fs,MMember.F_SCOPE_ID);
   //was.printTabFieldDic(fs,MMember.F_TYPE,getDic(MMember.FC_TYPE));
   //was.printTabField(fs,MMember.F_DESCR);
   //was.printTabFieldDic(fs,ZObject.F_ZTYPE,getDic(wam.DIC_TIS_OBJECT_TYPE));


// view form from ZFieldSet
// view record QR.QR.MMember
was.printTabField(fs,MMember.F_CODE);
//was.printTabField(fs,MMember.F_ORG_ID);
was.printTabFieldZOID2Descr(fs,MMember.F_ORG_ID,getZNames4Field(MMember.FC_ORG_ID),wam.SUBSYS_QR);
//was.printTabField(fs,MMember.F_SCOPE_ID);
//was.printTabFieldList(fs,MMember.F_SCOPE_ID,rl_scopes,wam.SUBSYS_SAM, wam.TARGET_SAMSCOPE);
was.printTabFieldList(fs,MMember.F_SCOPE_ID,getSAMScopes(),wam.SUBSYS_SAM,wam.TARGET_SAMSCOPE);
was.printTabFieldDic(fs,MMember.F_STATUS,getDic("QR_MEMBER_STATUS"));
was.printTabFieldDic(fs,MMember.F_LEI_TYPE,getDic("LEI_TYPE"));
was.printTabField(fs,MMember.F_LEI);
was.printTabFieldDic(fs,MMember.F_LET,getDic("LET_TYPE"));
was.printTabField(fs,MMember.F_NAME);
was.printTabField(fs,MMember.F_ADDR);
was.printTabField(fs,MMember.F_SITE);
was.printTabField(fs,MMember.F_PHONE);
was.printTabField(fs,MMember.F_EMAIL);
was.printTabFieldTextarea(fs,MMember.F_DESCR);
was.printTabFieldOWiki(fs,MMember.F_OWIKI);


   was.closeTable(); //header
   was.beginIndent();
   RList rlMR=dor.getTableChildren(MRange.TAB_CODE);
    for(int iMR=0;iMR<rlMR.size();iMR++)
    {
    DORecord MR = (DORecord)rlMR.get(iMR);
    fs= MR.toFieldSet();
    was.beginTable();
    //was.printTabFieldZOID2ZNAME(fs,MRange.F_OBJ_ID,getZNames4Field(MRange.FC_OBJ_ID),wam.SUBSYS_QR);

// view form from ZFieldSet
// view record QR.MRange
was.printTabField(fs,MRange.F_RSTART);
was.printTabField(fs,MRange.F_RBITL);
was.printTabFieldDic(fs,MRange.F_RTYPE,getDic("RANGE_TYPE"));
was.printTabFieldDic(fs,MRange.F_STATUS,getDic("QR_RANGE_STATUS"));
was.printTabFieldDic(fs,MRange.F_ACTION,getDic("QR_ACTION"));
was.printTabField(fs,MRange.F_REDIRECT);
was.printTabField(fs,MRange.F_ALLOC);
//was.printTabField(fs,MRange.F_MEMBER_ID);
//was.printTabField(fs,MRange.F_DOC_ID);
was.printTabFieldZOID2Descr(fs,MRange.F_MEMBER_ID,getZNames4Field(MRange.FC_MEMBER_ID),wam.SUBSYS_QR);
was.printTabFieldZOID2Descr(fs,MRange.F_DOC_ID,getZNames4Field(MRange.FC_DOC_ID),wam.SUBSYS_QR);
was.printTabFieldTextarea(fs,MRange.F_DESCR);
was.printTabFieldOWiki(fs,MRange.F_OWIKI);

    was.closeTable(); //header
    }
   was.closeIndent();
   }
 } // printDocumentView

 public void printThisDocEditForm(ZFieldSet fs)
 {
  int i;
  MMember MM = null;
  MRange MR = null;
  RList rl_scopes = getSAMScopes(); // exceptions are ignored!
  RList rl_MEMBER_ID = null; 
  RList rl_DOC_ID = null; 
  try{ rl_MEMBER_ID = getZSystem().listQRMember(null /*ZSID*/,4096,0); } catch(Exception e) { printerrmsg(e);}
  try{ rl_DOC_ID = getZSystem().listQRCard(null /*ZSID*/,4096,0); } catch(Exception e) { printerrmsg(e);}
  for(i=0;i<fs.countFS();i++)
  {
   ZFieldSet fsMM = fs.getFS(i);
   if(!MMember.TAB_CODE.equals(fsMM.getCode())) continue;
   was.beginRecord(); //MM
   was.printSTDFieldsInputDORecord(fsMM);
   was.beginTable();
    //was.printTabRecordDeletionStatus(fsMM);
    //was.printTabFieldInput(fsMM.get(MM.F_NUM));
    //was.printTabFieldInputDic(fsMM.get(MM.F_TYPE),getDic(MM.FC_TYPE));
    //was.printTabFieldInput(fsMM.get(MM.F_DESCR));


// edit form from ZFieldSet
// edit record QR.MMember
was.printTabFieldInput(fsMM.get(MM.F_CODE));
//was.printTabFieldInput(fsMM.get(MM.F_ORG_ID));
was.printTabFieldInputList(fsMM.get(MM.F_ORG_ID),rl_MEMBER_ID);
//was.printTabFieldInput(fsMM.get(MM.F_SCOPE_ID));
was.printTabFieldInputList(fsMM.get(MM.F_SCOPE_ID),rl_scopes);
was.printTabFieldInputDic(fsMM.get(MM.F_STATUS),getDic("QR_MEMBER_STATUS"));
was.printTabFieldInputDic(fsMM.get(MM.F_LEI_TYPE),getDic("LEI_TYPE"));
was.printTabFieldInput(fsMM.get(MM.F_LEI));
was.printTabFieldInputDic(fsMM.get(MM.F_LET),getDic("LET_TYPE"));
was.printTabFieldInput(fsMM.get(MM.F_NAME));
was.printTabFieldInput(fsMM.get(MM.F_ADDR));
was.printTabFieldInput(fsMM.get(MM.F_SITE));
was.printTabFieldInput(fsMM.get(MM.F_PHONE));
was.printTabFieldInput(fsMM.get(MM.F_EMAIL));
was.printTabFieldInputTextarea(fsMM.get(MM.F_DESCR));
was.printTabFieldInputOWiki(fsMM.get(MM.F_OWIKI));


   was.closeTable();
   int i2;
   was.beginIndent();
   for(i2=0;i2<fsMM.countFS();i2++)
   {
    ZFieldSet fsMR = fsMM.getFS(i2);
    if(!MRange.TAB_CODE.equals(fsMR.getCode())) continue;
    was.beginRecord(); //MR
    was.printSTDFieldsInputDORecord(fsMR);
    was.beginTable();
     was.printTabRecordDeletionStatus(fsMR);
     //was.printTabFieldInput(fsMR.get(MR.F_OBJ_ID));
// view form from ZFieldSet
// edit record QR.MRange
was.printTabFieldInput(fsMR.get(MR.F_RSTART));
was.printTabFieldInput(fsMR.get(MR.F_RBITL));
was.printTabFieldInputDic(fsMR.get(MR.F_RTYPE),getDic("RANGE_TYPE"));
was.printTabFieldInputDic(fsMR.get(MR.F_STATUS),getDic("QR_RANGE_STATUS"));
was.printTabFieldInputDic(fsMR.get(MR.F_ACTION),getDic("QR_ACTION"));
was.printTabFieldInput(fsMR.get(MR.F_REDIRECT));
was.printTabFieldInput(fsMR.get(MR.F_ALLOC));
//was.printTabFieldInput(fsMR.get(MR.F_MEMBER_ID));
//was.printTabFieldInput(fsMR.get(MR.F_DOC_ID));
was.printTabFieldInputList(fsMR.get(MR.F_MEMBER_ID),rl_MEMBER_ID);
was.printTabFieldInputList(fsMR.get(MR.F_DOC_ID),rl_DOC_ID);
was.printTabFieldInputTextarea(fsMR.get(MR.F_DESCR));
was.printTabFieldInputOWiki(fsMR.get(MR.F_OWIKI));

    was.closeTable();
    was.closeRecord(); //MR
   }
   was.printButtonAddRecord(MRange.TAB_CODE);was.println();
   was.closeIndent();
   was.closeRecord(); //MM
  }
  if(i==0)was.println();
  //was.printButtonAddRecord(MMember.TAB_CODE);was.println();
  //was.print(wam.getMsgText(wam.ADD_RECORD));
  //was.printButtonAddRecord(MMember.TAB_CODE,MMember.TAB_CODE);was.println();
  was.printButtonMenuBarEdit();
  was.closeRecord(); //DObject
  was.closeForm();
 } //printThisDocEditForm

 // constructors
 public WARHQRMember(
	javax.servlet.http.HttpServletRequest request,
	javax.servlet.http.HttpServletResponse response,
	java.io.PrintWriter out)
 { super(request,response,out); }
 public WARHQRMember(WARequestHandler warh) { super(warh); }

} //WARHQRMember
