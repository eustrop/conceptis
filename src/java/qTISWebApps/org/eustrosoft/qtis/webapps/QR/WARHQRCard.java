// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

//package ru.mave.ConcepTIS.webapps;
package org.eustrosoft.qtis.webapps.QR;

import java.util.*;
import java.sql.*;
import ru.mave.ConcepTIS.dao.*;
import ru.mave.ConcepTIS.dao.TIS.*;
import ru.mave.ConcepTIS.dao.QR.*;
import ru.mave.ConcepTIS.dao.SAM.*;
import org.eustrosoft.qtis.webapps.*;

public class WARHQRCard extends WARHTypicalObject
{
 public static final String SUBSYS_THIS = WAMessages.SUBSYS_QR;
 public static final String TARGET_THIS = WAMessages.TARGET_QR_CARD;

 public String getThisDocSubsys(){return(SUBSYS_THIS); }
 public String getThisDocTarget(){return(TARGET_THIS); }
 public int getThisDocTypeCaptionID() { return(wam.OBJ_QR_CARD); }
 public DObject newThisDocDObject() { DObject o = Card.newDObject(); o.addRecord(new CCard()); return(o); }

 public void printThisDocGeneralHMenu()
 {
  was.printHMenuItem(wam.MNU_QR_CARD_LIST);
  was.printHMenuItem(wam.MNU_QR_CARD_CREATE);
  was.printHMenuItem(wam.MNU_QR_CARD_FIND);
 }

 public RList loadThisDocList(Long ZSID, int rows_per_page, long row_offset)
   throws java.sql.SQLException,ZException
 { return( getZSystem().listQRCard(ZSID,rows_per_page,row_offset) ); }

 public DObject loadThisDocDObject(Long ZOID)
   throws java.sql.SQLException,ZException
 { return(getZSystem().loadQRCard(ZOID)); }

 public DObject extractThisDocDObject(ZFieldSet fs)
   throws java.sql.SQLException,ZException
 {DObject o=Card.newDObject();o.getFromFS(fs);return(o);}

 public void printThisDocView(DObject o)
 {
  RList rl = o.getTable(CCard.TAB_CODE);
  RList rl_scopes=null; // list of SAMScopes
  try{ rl_scopes = getZSystem().listSAMScopes(); } catch(Exception e) { printerrmsg(e);}
  ZDictionary dicSLEVEL = getZSystem().getDictionary(wam.DIC_SLEVEL);
  load_DocReferences(o);
   if(rl.size() < 1) { // for empty objects
    was.printMsg(wam.INFO_EMPTY_DOBJECT_VIEWED);
    was.beginTable();
    was.printTabField(ZName.F_ZNAME,o.getZNAME());
    was.printTabField(ZObject.F_ZOID,wam.obj2string(o.getZOID()));
    was.printTabFieldDic(new ZField(ZObject.F_ZTYPE, o.getZTYPE()),
	getDic(wam.DIC_TIS_OBJECT_TYPE));
    was.closeTable();
   }
   // header
   for(int i=0;i<rl.size();i++)
   {
   ZFieldSet fs=((DORecord)(rl.get(i))).toFieldSet();
   was.beginTable();

// view record QR.CCard
was.printTabField(fs,CCard.F_CARDATE);
was.printTabField(fs,CCard.F_CNUM);
was.printTabField(fs,CCard.F_CDATE);
//was.printTabField(fs,CCard.F_CNUM_ID);
was.printTabFieldZOID2Descr(fs,CCard.F_CNUM_ID,getZNames4Field(CCard.FC_CNUM_ID),wam.SUBSYS_QR);
was.printTabFieldZOID2Descr(fs,CCard.F_SUPPLIER_ID,getZNames4Field(CCard.FC_SUPPLIER_ID),wam.SUBSYS_QR);
was.printTabField(fs,CCard.F_SUPPLIER);
was.printTabFieldHeader("Client","Клиент по договору");
was.printTabFieldZOID2Descr(fs,CCard.F_CLIENT_ID,getZNames4Field(CCard.FC_CLIENT_ID),wam.SUBSYS_QR);
was.printTabField(fs,CCard.F_CLIENT);
was.printTabField(fs,CCard.F_CLADDR);
was.printTabFieldHeader("Money","Условия договора:");
was.printTabField(fs,CCard.F_CMONEY);
was.printTabFieldDic(fs,CCard.F_CMONEY_CUR,getDic("CURRENCY_CODE"));
was.printTabFieldDic(fs,CCard.F_CMONEY_VAT,getDic("VAT_CODES"));
was.printTabField(fs,CCard.F_CMONEY_DESC);
was.printTabFieldHeader("Product","Характеристики изделия:");
was.printTabField(fs,CCard.F_PRODTYPE);
was.printTabField(fs,CCard.F_PRODMODEL);
was.printTabField(fs,CCard.F_PMREVISION);
was.printTabField(fs,CCard.F_SN);
was.printTabField(fs,CCard.F_PRODATE);
was.printTabField(fs,CCard.F_GTD);
was.printTabFieldHeader("Tracking","События по договору:");
was.printTabField(fs,CCard.F_SALEDATE);
was.printTabField(fs,CCard.F_SENDATE);
was.printTabField(fs,CCard.F_WSTART);
was.printTabField(fs,CCard.F_WEND);
was.printTabFieldHeader("QR-Card","связанная публичная QR-карточка");
was.printTabFieldQR(fs,CCard.F_QR);
was.printTabFieldDic(fs,CCard.F_AUTOEXPORT,getDic("YESNO"));
//was.printTabField(fs,CCard.F_PUBQR_ID);
was.printTabFieldZOID2Descr(fs,CCard.F_PUBQR_ID,getZNames4Field(CCard.FC_PUBQR_ID),wam.SUBSYS_QR);
was.printTabFieldHeader("GIS","Географические координаты объекта:");
was.printTabField(fs,CCard.F_GIS_LONG);
was.printTabField(fs,CCard.F_GIS_LAT);
was.printTabField(fs,CCard.F_GIS_ALT);
was.printTabFieldTextarea(fs,CCard.F_COMMENT);
was.printTabFieldTextarea(fs,CCard.F_CSVCARD);
   was.closeTable();
   } //header
 } // printDocumentView

 public void printThisDocEditForm(ZFieldSet fs)
 {
  int i;
  CCard CC = null;
  RList rl_scopes = getSAMScopes(); // exceptions are ignored!
  RList rl_SUPPLIER_ID = null; 
  RList rl_CLIENT_ID = null; 
  RList rl_PUBQR_ID = null; 
  RList rl_CNUM_ID = null; 
  try{ rl_SUPPLIER_ID = getZSystem().listQRMember(null /*ZSID*/,4096,0); } catch(Exception e) { printerrmsg(e);}
  try{ rl_CNUM_ID = getZSystem().listQRCard(null /*ZSID*/,4096,0); } catch(Exception e) { printerrmsg(e);}
  try{ rl_PUBQR_ID = getZSystem().listQRQR(null /*ZSID*/,4096,0); } catch(Exception e) { printerrmsg(e);}
  rl_CLIENT_ID = rl_SUPPLIER_ID;
  for(i=0;i<fs.countFS();i++)
  {
   ZFieldSet fsCC = fs.getFS(i);
   if(!CCard.TAB_CODE.equals(fsCC.getCode())) continue;
   was.beginRecord(); //CC
   was.printSTDFieldsInputDORecord(fsCC);
   //was.printFieldInputHidden(fsCC.get(r.F_ID));
   //was.printRecordDeletionStatus(fsCC);
   was.beginTable();
    //was.printTabRecordDeletionStatus(fsCC);

// edit record QR.CCard
was.printTabFieldInput(fsCC.get(CC.F_CARDATE));
was.printTabFieldInput(fsCC.get(CC.F_CNUM));
was.printTabFieldInput(fsCC.get(CC.F_CDATE));
//was.printTabFieldInput(fsCC.get(CC.F_CNUM_ID));
was.printTabFieldInputList(fsCC.get(CC.F_CNUM_ID),rl_CNUM_ID);
was.printTabFieldInputList(fsCC.get(CC.F_SUPPLIER_ID),rl_SUPPLIER_ID);
was.printTabFieldInput(fsCC.get(CC.F_SUPPLIER));
was.printTabFieldHeader("Client","Клиент по договору");
was.printTabFieldInputList(fsCC.get(CC.F_CLIENT_ID),rl_CLIENT_ID);
was.printTabFieldInput(fsCC.get(CC.F_CLIENT));
was.printTabFieldInput(fsCC.get(CC.F_CLADDR));
was.printTabFieldHeader("Money","Условия договора:");
was.printTabFieldInput(fsCC.get(CC.F_CMONEY));
was.printTabFieldInputDic(fsCC.get(CC.F_CMONEY_CUR),getDic("CURRENCY_CODE"));
was.printTabFieldInputDic(fsCC.get(CC.F_CMONEY_VAT),getDic("VAT_CODES"));
was.printTabFieldInput(fsCC.get(CC.F_CMONEY_DESC));
was.printTabFieldHeader("Product","Характеристики изделия:");
was.printTabFieldInput(fsCC.get(CC.F_PRODTYPE));
was.printTabFieldInput(fsCC.get(CC.F_PRODMODEL));
was.printTabFieldInput(fsCC.get(CC.F_PMREVISION));
was.printTabFieldInput(fsCC.get(CC.F_SN));
was.printTabFieldInput(fsCC.get(CC.F_PRODATE));
was.printTabFieldInput(fsCC.get(CC.F_GTD));
was.printTabFieldHeader("Tracking","События по договору:");
was.printTabFieldInput(fsCC.get(CC.F_SALEDATE));
was.printTabFieldInput(fsCC.get(CC.F_SENDATE));
was.printTabFieldInput(fsCC.get(CC.F_WSTART));
was.printTabFieldInput(fsCC.get(CC.F_WEND));
was.printTabFieldHeader("QR-Card","связанная публичная QR-карточка");
was.printTabFieldInput(fsCC.get(CC.F_QR));
//was.printTabFieldInput(fsCC.get(CC.F_PUBQR_ID));
was.printTabFieldInputList(fsCC.get(CC.F_PUBQR_ID),rl_PUBQR_ID);
was.printTabFieldInputDic(fsCC.get(CC.F_AUTOEXPORT),getDic("YESNO"));
was.printTabFieldHeader("GIS","Географические координаты объекта:");
was.printTabFieldInput(fsCC.get(CC.F_GIS_LONG));
was.printTabFieldInput(fsCC.get(CC.F_GIS_LAT));
was.printTabFieldInput(fsCC.get(CC.F_GIS_ALT));
was.printTabFieldInputTextarea(fsCC.get(CC.F_COMMENT));
was.printTabFieldInputTextarea(fsCC.get(CC.F_CSVCARD));
   was.closeTable();
   was.closeRecord(); //CC
  }
  if(i==0)was.println();
  //was.printButtonAddRecord(CCard.TAB_CODE);was.println();
  //was.print(wam.getMsgText(wam.ADD_RECORD));
  //was.printButtonAddRecord(CCard.TAB_CODE,CCard.TAB_CODE);was.println();
  was.printButtonMenuBarEdit();
  was.closeRecord(); //DObject
  was.closeForm();
 } //printThisDocEditForm

 // constructors
 public WARHQRCard(
	javax.servlet.http.HttpServletRequest request,
	javax.servlet.http.HttpServletResponse response,
	java.io.PrintWriter out)
 { super(request,response,out); }
 public WARHQRCard(WARequestHandler warh) { super(warh); }

} //WARHQRCard
