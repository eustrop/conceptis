// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

//package ru.mave.ConcepTIS.webapps;
package org.eustrosoft.qtis.webapps.QR;

import java.util.*;
import java.sql.*;
import ru.mave.ConcepTIS.dao.*;
import ru.mave.ConcepTIS.dao.TIS.*;
import ru.mave.ConcepTIS.dao.QR.*;
import ru.mave.ConcepTIS.dao.SAM.*;
import org.eustrosoft.qtis.webapps.*;

public class WARHQRQR extends WARHTypicalObject
{
 public static final String SUBSYS_THIS = WAMessages.SUBSYS_QR;
 public static final String TARGET_THIS = WAMessages.TARGET_QR_QR;

 public String getThisDocSubsys(){return(SUBSYS_THIS); }
 public String getThisDocTarget(){return(TARGET_THIS); }
 public int getThisDocTypeCaptionID() { return(wam.OBJ_QR_QR); }
 public DObject newThisDocDObject() { DObject o = QR.newDObject(); o.addRecord(new QRecord()); return(o); }

 public void printThisDocGeneralHMenu()
 {
  was.printHMenuItem(wam.MNU_QR_QR_LIST);
  was.printHMenuItem(wam.MNU_QR_QR_CREATE);
  was.printHMenuItem(wam.MNU_QR_QR_FIND);
 }

 public RList loadThisDocList(Long ZSID, int rows_per_page, long row_offset)
   throws java.sql.SQLException,ZException
 { return( getZSystem().listQRQR(ZSID,rows_per_page,row_offset) ); }

 public DObject loadThisDocDObject(Long ZOID)
   throws java.sql.SQLException,ZException
 { return(getZSystem().loadQRQR(ZOID)); }

 public DObject extractThisDocDObject(ZFieldSet fs)
   throws java.sql.SQLException,ZException
 {DObject o=QR.newDObject();o.getFromFS(fs);return(o);}

 public void printThisDocView(DObject o)
 {
  RList rl = o.getTable(QRecord.TAB_CODE);
  RList rl_scopes=null; // list of SAMScopes
  try{ rl_scopes = getZSystem().listSAMScopes(); } catch(Exception e) { printerrmsg(e);}
  ZDictionary dicSLEVEL = getZSystem().getDictionary(wam.DIC_SLEVEL);
  load_DocReferences(o);
   if(rl.size() < 1) { // for empty objects
    was.printMsg(wam.INFO_EMPTY_DOBJECT_VIEWED);
    was.beginTable();
    was.printTabField(ZName.F_ZNAME,o.getZNAME());
    was.printTabField(ZObject.F_ZOID,wam.obj2string(o.getZOID()));
    was.printTabFieldDic(new ZField(ZObject.F_ZTYPE, o.getZTYPE()),
	getDic(wam.DIC_TIS_OBJECT_TYPE));
    was.closeTable();
   }
   // header
   for(int i=0;i<rl.size();i++)
   {
   ZFieldSet fs=((DORecord)(rl.get(i))).toFieldSet();
   was.beginTable();
// view record QR.QRecord
was.printTabFieldQR(fs,QRecord.F_QR);
was.printTabFieldDic(fs,QRecord.F_ACTION,getDic("QR_ACTION"));
was.printTabField(fs,QRecord.F_REDIRECT);
was.printTabFieldZOID2Descr(fs,QRecord.F_OBJ_ID,getZNames4Field(QRecord.FC_OBJ_ID),wam.SUBSYS_QR);
was.printTabField(fs,QRecord.F_CNUM);
was.printTabField(fs,QRecord.F_CDATE);
was.printTabField(fs,QRecord.F_PRICE_GPL);
was.printTabFieldDic(fs,QRecord.F_PRICE_VAT,getDic("VAT_CODES"));
was.printTabFieldZOID2Descr(fs,QRecord.F_SUPPORT_ID,getZNames4Field(QRecord.FC_SUPPORT_ID),wam.SUBSYS_QR);
was.printTabField(fs,QRecord.F_PRODTYPE);
was.printTabField(fs,QRecord.F_PRODMODEL);
was.printTabField(fs,QRecord.F_PMREVISION);
was.printTabField(fs,QRecord.F_SN);
was.printTabField(fs,QRecord.F_PRODATE);
was.printTabField(fs,QRecord.F_GTD);
was.printTabField(fs,QRecord.F_SALEDATE);
was.printTabField(fs,QRecord.F_SENDATE);
was.printTabField(fs,QRecord.F_WSTART);
was.printTabField(fs,QRecord.F_WEND);
was.printTabField(fs,QRecord.F_GIS_LONG);
was.printTabField(fs,QRecord.F_GIS_LAT);
was.printTabField(fs,QRecord.F_GIS_ALT);
was.printTabField(fs,QRecord.F_COMMENT);
was.printTabFieldTextarea(fs,QRecord.F_CSVCARD);
was.printTabFieldOWiki(fs,QRecord.F_OWIKI);

   was.closeTable();
   } //header
 } // printDocumentView

 public void printThisDocEditForm(ZFieldSet fs)
 {
  int i;
  QRecord QR = null;
  RList rl_scopes = getSAMScopes(); // exceptions are ignored!
  RList rl_SUPPORT_ID = null; 
  RList rl_OBJ_ID = null; 
  try{ rl_OBJ_ID = getZSystem().listQRQR(null /*ZSID*/,4096,0); } catch(Exception e) { printerrmsg(e);}
  try{ rl_SUPPORT_ID = getZSystem().listQRMember(null /*ZSID*/,4096,0); } catch(Exception e) { printerrmsg(e);}
  for(i=0;i<fs.countFS();i++)
  {
   ZFieldSet fsQR = fs.getFS(i);
   if(!QRecord.TAB_CODE.equals(fsQR.getCode())) continue;
   was.beginRecord(); //QR
   was.printSTDFieldsInputDORecord(fsQR);
   //was.printFieldInputHidden(fsQR.get(r.F_ID));
   //was.printRecordDeletionStatus(fsQR);
   was.beginTable();
    //was.printTabRecordDeletionStatus(fsQR);
// edit record QR.QRecord
was.printTabFieldInput(fsQR.get(QR.F_QR));
was.printTabFieldInputDic(fsQR.get(QR.F_ACTION),getDic("QR_ACTION"));
was.printTabFieldInput(fsQR.get(QR.F_REDIRECT));
was.printTabFieldInputList(fsQR.get(QR.F_OBJ_ID),rl_OBJ_ID);
was.printTabFieldInput(fsQR.get(QR.F_CNUM));
was.printTabFieldInput(fsQR.get(QR.F_CDATE));
was.printTabFieldInput(fsQR.get(QR.F_PRICE_GPL));
was.printTabFieldInputDic(fsQR.get(QR.F_PRICE_VAT),getDic("VAT_CODES"));
was.printTabFieldInputList(fsQR.get(QR.F_SUPPORT_ID),rl_SUPPORT_ID);
was.printTabFieldInput(fsQR.get(QR.F_PRODTYPE));
was.printTabFieldInput(fsQR.get(QR.F_PRODMODEL));
was.printTabFieldInput(fsQR.get(QR.F_PMREVISION));
was.printTabFieldInput(fsQR.get(QR.F_SN));
was.printTabFieldInput(fsQR.get(QR.F_PRODATE));
was.printTabFieldInput(fsQR.get(QR.F_GTD));
was.printTabFieldInput(fsQR.get(QR.F_SALEDATE));
was.printTabFieldInput(fsQR.get(QR.F_SENDATE));
was.printTabFieldInput(fsQR.get(QR.F_WSTART));
was.printTabFieldInput(fsQR.get(QR.F_WEND));
was.printTabFieldInput(fsQR.get(QR.F_GIS_LONG));
was.printTabFieldInput(fsQR.get(QR.F_GIS_LAT));
was.printTabFieldInput(fsQR.get(QR.F_GIS_ALT));
was.printTabFieldInput(fsQR.get(QR.F_COMMENT));
was.printTabFieldInputTextarea(fsQR.get(QR.F_CSVCARD));
was.printTabFieldInputOWiki(fsQR.get(QR.F_OWIKI));

   was.closeTable();
   was.closeRecord(); //QR
  }
  if(i==0)was.println();
  //was.printButtonAddRecord(QRecord.TAB_CODE);was.println();
  //was.print(wam.getMsgText(wam.ADD_RECORD));
  //was.printButtonAddRecord(QRecord.TAB_CODE,QRecord.TAB_CODE);was.println();
  was.printButtonMenuBarEdit();
  was.closeRecord(); //DObject
  was.closeForm();
 } //printThisDocEditForm

 // constructors
 public WARHQRQR(
	javax.servlet.http.HttpServletRequest request,
	javax.servlet.http.HttpServletResponse response,
	java.io.PrintWriter out)
 { super(request,response,out); }
 public WARHQRQR(WARequestHandler warh) { super(warh); }

} //WARHQRQR
