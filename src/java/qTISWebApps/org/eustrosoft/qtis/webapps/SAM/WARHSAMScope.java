// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

//package ru.mave.ConcepTIS.webapps;
package org.eustrosoft.qtis.webapps.SAM;

import java.util.*;
import java.sql.*;
import ru.mave.ConcepTIS.dao.*;
import ru.mave.ConcepTIS.dao.SAM.*;
import org.eustrosoft.qtis.webapps.*;

public class WARHSAMScope
	extends WARequestHandler
{
 public static final String SUBSYS_THIS = WAMessages.SUBSYS_SAM;
 public static final String TARGET_THIS = WAMessages.TARGET_SAMSCOPE;

 public void processREST() { was.printRESTNotImplemented(); }

 // interfaces/abstracts implementation
 public void process()
 {
  String req = request.getParameter(wam.PARAM_REQUEST);
  was.beginHMenu();
  was.printHMenuSeparator();
  //was.printHMenuCaption(wam.getMnuDesc(wam.MNU_SAM_MAIN)+":");
  was.printHMenuItem(wam.MNU_SAM_SCOPE_LIST);
  was.printHMenuItem(wam.MNU_SAM_SCOPE_CREATE);
  was.printHMenuItem(wam.MNU_SAM_SCOPE_FIND);
  was.closeHMenu();
  was.printSeparator();
  try{ db_logon(); } catch(Exception e){printerrmsg(e); return; }
  try{
   if(wam.REQUEST_VIEW.equals(req))  do_view(); 
   else if(wam.REQUEST_CREATE.equals(req)) do_create();
   else if(wam.REQUEST_EDIT.equals(req)) do_edit();
   else if(wam.REQUEST_DELETE.equals(req)) do_delete();
   else if(wam.REQUEST_SAVE.equals(req)) do_save();
   else if(wam.REQUEST_FIND.equals(req)) do_find();
   else if(wam.REQUEST_MNGPOLICY.equals(req)) do_mngpolicy();
   else if(wam.REQUEST_SETCHPTS.equals(req)) do_set_chpts();
   else if(wam.REQUEST_BTNMENU.equals(req)) process_btnmenu();
   else if(req == null || wam.REQUEST_LIST.equals(req)) do_list();
   else throw(new WAException(wam,wam.ERROR_NOT_IMPLEMENTED));
  }catch(Exception e){printerrmsg(e); return; }
  finally{ db_logoff(); } 
 } // process

 /** process request passed per button clicking. */
 private void process_btnmenu()
  throws java.sql.SQLException, ZException, WAException
 {
  if(request.getParameter(wam.PARAM_BTN_CANCEL) != null) {do_list();}
  else if(request.getParameter(wam.PARAM_BTN_EDIT) != null) {do_edit(); }
  else if(request.getParameter(wam.PARAM_BTN_DELETE) != null) {do_delete(); }
  //else if(request.getParameter(wam.PARAM_BTN_VIEW) != null) {do_view(); }
  else throw(new WAException(wam,wam.ERROR_NOT_IMPLEMENTED));
 } // process_btnmenu()

 /** delete SAMScopes. */
 private void do_delete()
  throws java.sql.SQLException, ZException, WAException
 {
  Long id = new Long(request.getParameter(wam.PARAM_ZID));
  if(request.getParameter(wam.PARAM_BTN_NO) != null) {do_view();return;}
  else if(request.getParameter(wam.PARAM_BTN_YES) != null) {
   getZSystem().deleteSAMScope(id);
   was.printMsg(wam.INFO_SUCCESS_DELETED);
   return;
  }
  // else
  Scope r=null; try { r = getZSystem().loadSAMScope(id);}
  catch(Exception e) { printerrmsg(e);}
  was.printQuestDelete(wam.OBJ_SAMSCOPE,r,SUBSYS_THIS,TARGET_THIS,id);
 } //do_delete()

 private void do_set_chpts()
  throws java.sql.SQLException, ZException, WAException
 {
  //throw(new WAException(wam,wam.ERROR_NOT_IMPLEMENTED));
  Long sid = new Long(request.getParameter(wam.PARAM_ZID));
  try{
  DateTime chpts = new DateTime(request.getParameter(Scope.F_CHPTS));
  if(request.getParameter(wam.PARAM_BTN_SET) != null)
   //was.println(wam.obj2text(chpts));
   getZSystem().setSAMScopeCHPTS(sid,chpts);
  } catch(Exception e) { printerrmsg(e);}
  do_view();
 } // do_set_chpts()

 private void do_mngpolicy()
  throws java.sql.SQLException, ZException, WAException
 {
  ScopePolicy r; // passed object or missed data donor
  boolean is_delete=false;
  // get object data from request's parameters
  ZFieldSet fs = was.extractFieldSet(request);
  is_delete=(request.getParameter(wam.PARAM_BTN_DELETE)!=null);
  try {
   r = new ScopePolicy(fs);
   if(is_delete){ getZSystem().deleteSAMScopePolicy(r.sid,r.obj_type); }
   else{ getZSystem().setSAMScopePolicy(r); }
  } catch(Exception e){printerrmsg(e);}
  do_view();
 } // do_mngpolicy()

 /** show list of all SAMScopes. */
 private void do_list()
  throws java.sql.SQLException, ZException
 {
   RList rl = getZSystem().listSAMScopes();
   ZFieldSet fs;
   // WARHPSQL warh = new WARHPSQL(this); warh.print_sql_rs(rs);
   int did=was.beginDocument(); try{
   was.beginTable();
    was.beginTableRow();
     fs= (new Scope()).toFieldSet();
     was.printHCellValue(wam.getMsgText(wam.CAPTION_SCOPE));
     //was.printHCellCaption(fs.get(Scope.F_NAME));
     was.printHCellCaption(fs.get(Scope.F_DESCR));
    was.closeTableRow();
   for(int i=0;i<rl.size();i++)
   {
    Scope r = (Scope)rl.get(i);
    fs = r.toFieldSet();
    was.beginTableRow();
    was.printCellHRefView(r.name,SUBSYS_THIS,TARGET_THIS,r.id);
    was.printCellValue(wam.obj2string(r.descr));
    was.closeTableRow();
   }
   was.closeTable();
   }finally{was.closeDocument(did);}
 } // do_list()

 /** show SAMScope and all it's properties. */
 private void do_view()
  throws java.sql.SQLException, ZException, WAException
 {
  String key = request.getParameter(wam.PARAM_ZID);
  Long id = Long.valueOf(key);
  Scope r = getZSystem().loadSAMScope(id);
  ScopePolicy XSP;
  ZFieldSet fs;
  if( r == null ) throw(new WAException(wam,wam.ERROR_NOT_FOUND));
  RList rl;
  RList rl_scopes=null; // list of SAMScopes
  try{ rl_scopes = getZSystem().listSAMScopes(); } catch(Exception e) { printerrmsg(e);}
  int did=was.beginDocument();
  try{
   // header
   was.printButtonMenuBarView(SUBSYS_THIS, wam.REQUEST_BTNMENU, TARGET_THIS, r.id);
   was.beginTable();
    fs = r.toFieldSet();
    was.printTabField(fs,r.F_NAME);
    was.printTabField(fs,r.F_ID);
    was.printTabFieldRefView(fs,r.F_SID,rl_scopes,SUBSYS_THIS,wam.TARGET_SAMSCOPE);
    was.printTabFieldDic(fs,r.F_SLEVEL_MIN,getDic(wam.DIC_SLEVEL));
    was.printTabFieldDic(fs,r.F_SLEVEL_MAX,getDic(wam.DIC_SLEVEL));
    was.printTabFieldDic(fs,r.F_IS_LOCAL,getDic(wam.DIC_YESNO));
    was.printTabFieldDic(fs,r.F_AUDITLVL,getDic(wam.DIC_EVENT_CLASS));
    was.printTabField(fs,r.F_CHPTS);
    was.printTabField(fs,r.F_DESCR);
   was.closeTable();
   fs = (new Scope()).toFieldSet();
   was.beginFormPost(SUBSYS_THIS,wam.REQUEST_SETCHPTS,TARGET_THIS,id);
    was.beginTable();
     ZField f_chpts = fs.get(Scope.F_CHPTS);
     if( f_chpts.getValue() == null)
	f_chpts.setValue(new DateTime(java.util.Calendar.getInstance().getTime() )); //current
     String sz_chpts = request.getParameter(Scope.F_CHPTS);
     if(sz_chpts != null) f_chpts.setValue(sz_chpts);
     was.printTabFieldInput( f_chpts);
    was.closeTable();
    was.printButtonSet();
   was.closeForm();
   // ScopePolicy
   rl = getZSystem().listSAMScopePolicy(id);
   fs = (new ScopePolicy()).toFieldSet();
   was.beginTable();
    was.beginTableRow();
     was.printHCellCaption(fs.get(ScopePolicy.F_OBJ_TYPE));
     was.printHCellCaption(fs.get(ScopePolicy.F_MAXCOUNT));
     was.printHCellCaption(fs.get(ScopePolicy.F_OCOUNT));
     was.printHCellCaption(fs.get(ScopePolicy.F_DOAUDIT));
     was.printHCellCaption(fs.get(ScopePolicy.F_DODEBUG));
    was.closeTableRow();
    for(int i=0;i<rl.size();i++)
    {
     ScopePolicy r2 = (ScopePolicy)rl.get(i);
     fs = r2.toFieldSet();
     was.beginTableRow();
     was.printCellFieldDic(fs.get(ScopePolicy.F_OBJ_TYPE),
	getDic(wam.DIC_TIS_OBJECT_TYPE));
     was.printCellField(fs.get(ScopePolicy.F_MAXCOUNT));
     was.printCellField(fs.get(ScopePolicy.F_OCOUNT));
     was.printCellField(fs.get(ScopePolicy.F_DOAUDIT));
     was.printCellField(fs.get(ScopePolicy.F_DODEBUG));
     was.closeTableRow();
    }
   was.closeTable();
   XSP = new ScopePolicy(); XSP.sid = r.id; fs = XSP.toFieldSet();
   was.beginFormPost(SUBSYS_THIS,wam.REQUEST_MNGPOLICY,TARGET_THIS,id);
    was.beginRecord();
    was.printFieldInputHidden( fs.get(ScopePolicy.F_SID));
    was.beginTable();
     was.printTabFieldInputDic( fs.get(ScopePolicy.F_OBJ_TYPE), getDic(wam.DIC_TIS_OBJECT_TYPE));
     was.printTabFieldInput( fs.get(ScopePolicy.F_MAXCOUNT));
     was.printTabFieldInputDic( fs.get(ScopePolicy.F_DOAUDIT), getDic(wam.DIC_YESNO));
     was.printTabFieldInputDic( fs.get(ScopePolicy.F_DODEBUG), getDic(wam.DIC_YESNO));
    was.closeTable();
    was.closeRecord();
    was.printButtonSet();
    was.printButtonDelete();
   was.closeForm();
  // infor about ranges
  if(r != null && id != null) try { // !SIC move it to separate request SHOW_USER_SESSIONS and put link here
   WARHPSQL wasql = new WARHPSQL(this);
   wasql.exec_sql(getDBC(),"select qtype,start_hex,rend_hex,lastid_hex,lastid,prange_hex || '/' || pbitl::text as prange_hex_b,sorder,ts from SAM.V_RangeSeqInfo where sid = " + id + " order by qtype");
  } catch(Exception e) { printerrmsg(e);}
  }finally{was.closeDocument(did);}
 } // do_view()

 private void do_save()
	throws java.sql.SQLException,ZException,WAException
 {
  Scope r; // passed object or missed data donor
  boolean is_create=false;
  // get object data from request's parameters
  ZFieldSet fs = was.extractFieldSet(request);
  // cancel request
  if(request.getParameter(wam.PARAM_BTN_CANCEL) != null)
   {if(request.getParameter(wam.PARAM_ZID) != null) do_view();
    else do_list(); return; }
  // save object
  is_create=(request.getParameter(wam.PARAM_DO_CREATE)!=null);
  if(request.getParameter(wam.PARAM_BTN_SAVE) != null)
  try {
   r = new Scope(fs);
   if(is_create) getZSystem().createSAMScope(r);
   else getZSystem().updateSAMScope(r);
   was.printMsg(wam.INFO_SUCCESS_SAVED);
   was.print(" (");
   was.printHRefView(r.getCaption(),SUBSYS_THIS,TARGET_THIS,r.id);
   was.print(")");
   return;
  } catch(Exception e){printerrmsg(e);}
  // correct data and display it again
  r = new Scope(); // missed data donor
  fs.importMissedData(r.toFieldSet());
  int did=was.beginDocument();
  try{ print_form_edit(fs,is_create); }finally{was.closeDocument(did);}
 } //do_save()

 private void do_edit()
	throws java.sql.SQLException,ZException,WAException
 {
  String key = request.getParameter(wam.PARAM_ZID);
  Long id = Long.valueOf(key);
  Scope r = getZSystem().loadSAMScope(id);
  if( r == null ) throw(new WAException(wam,wam.ERROR_NOT_FOUND));
  int did=was.beginDocument();
  try{ print_form_edit(r.toFieldSet(),false); }finally{was.closeDocument(did);}
 } // do_create()

 private void do_create()
 {
  int did=was.beginDocument();
  Scope r = new Scope();
  try{ print_form_edit(r.toFieldSet(),true); }
  finally{was.closeDocument(did);}
 } // do_create()

 private void print_form_edit(ZFieldSet fs, boolean do_create)
 {
  Scope r = new Scope(); // short alias for Scope class
  RList rl_scopes=null; // list of SAMScopes
  try{ rl_scopes = getZSystem().listSAMScopes(); }
    catch(Exception e) { printerrmsg(e); rl_scopes = null; }
  int did=was.beginDocument();
  try{
   // header
   was.beginFormPost(SUBSYS_THIS,wam.REQUEST_SAVE,TARGET_THIS);
   // put ZID into request form
   ZField f_id = fs.get(r.F_ID);
   if(f_id != null){
     f_id = new ZField(f_id); f_id.setName(wam.PARAM_ZID);
     if(f_id.getValue() != null) was.printFieldInputHidden(f_id);
   }
   // do_create if so
   if(do_create) was.printFieldInputHidden(wam.PARAM_DO_CREATE,"yes");
   was.printButtonMenuBarEdit();
   was.beginRecord();
   if(!do_create)  was.printFieldInputHidden(fs.get(r.F_ID));
   was.beginTable();
    was.printTabFieldInput(fs.get(r.F_NAME));
    if(!do_create) was.printTabField(fs.get(r.F_ID));
     else was.printTabFieldInput(fs.get(r.F_ID));
    was.printTabFieldInputList(fs.get(r.F_SID),rl_scopes);
    was.printTabFieldInputDic(fs.get(r.F_SLEVEL_MIN),getDic(wam.DIC_SLEVEL));
    was.printTabFieldInputDic(fs.get(r.F_SLEVEL_MAX),getDic(wam.DIC_SLEVEL));
    was.printTabFieldInputDic(fs.get(r.F_IS_LOCAL),getDic(wam.DIC_YESNO));
    was.printTabFieldInputDic(fs.get(r.F_AUDITLVL),getDic(wam.DIC_EVENT_CLASS));
    was.printTabFieldInput(fs.get(r.F_DESCR));
   was.closeTable();
   was.printButtonMenuBarEdit();
    was.closeRecord();
   was.closeForm();
  }finally{was.closeDocument(did);}
 } // print_form_edit()

 /** search for target objects. */
 private void do_find()
  throws WAException
 {
 throw(new WAException(wam,wam.ERROR_NOT_IMPLEMENTED));
 } // do_find()

 private void print_form_find()
 {
 } // print_form_find()

 // constructors
 public WARHSAMScope(
	javax.servlet.http.HttpServletRequest request,
	javax.servlet.http.HttpServletResponse response,
	java.io.PrintWriter out)
 { super(request,response,out); }
 public WARHSAMScope(WARequestHandler warh) { super(warh); }

} //WARHSAMScope
