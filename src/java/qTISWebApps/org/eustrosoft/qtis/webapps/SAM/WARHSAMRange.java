// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

//package ru.mave.ConcepTIS.webapps;
package org.eustrosoft.qtis.webapps.SAM;

import java.util.*;
import java.sql.*;
import ru.mave.ConcepTIS.dao.*;
import ru.mave.ConcepTIS.dao.SAM.*;
import org.eustrosoft.qtis.webapps.*;

public class WARHSAMRange
	extends WARequestHandler
{
 public static final String SUBSYS_THIS = WAMessages.SUBSYS_SAM;
 public static final String TARGET_THIS = WAMessages.TARGET_SAMRANGE;

 public void processREST() { was.printRESTNotImplemented(); }

 // interfaces/abstracts implementation
 public void process()
 {
  String req = request.getParameter(wam.PARAM_REQUEST);
  was.beginHMenu();
  was.printHMenuSeparator();
  //was.printHMenuCaption(wam.getMnuDesc(wam.MNU_SAM_MAIN)+":");
  was.printHMenuItem(wam.MNU_SAM_RANGE_LIST);
  was.printHMenuItem(wam.MNU_SAM_RANGE_CREATE);
  was.printHMenuItem(wam.MNU_SAM_RANGE_FIND);
  was.closeHMenu();
  was.printSeparator();
  try{ db_logon(); } catch(Exception e){printerrmsg(e); return; }
  try{
   if(wam.REQUEST_VIEW.equals(req))  do_view(); 
//   else if(wam.REQUEST_CREATE.equals(req)) do_create();
//   else if(wam.REQUEST_EDIT.equals(req)) do_edit();
//   else if(wam.REQUEST_DELETE.equals(req)) do_delete();
//   else if(wam.REQUEST_SAVE.equals(req)) do_save();
//   else if(wam.REQUEST_FIND.equals(req)) do_find();
//   else if(wam.REQUEST_BTNMENU.equals(req)) process_btnmenu();
   else if(req == null || wam.REQUEST_LIST.equals(req)) do_list();
   else throw(new WAException(wam,wam.ERROR_NOT_IMPLEMENTED));
  }catch(Exception e){printerrmsg(e); return; }
  finally{ db_logoff(); } 
 } // process

 /** show list of all SAMRanges. */
 private void do_list()
  throws java.sql.SQLException, ZException
 {
   // WARHPSQL warh = new WARHPSQL(this); warh.print_sql_rs(rs);
   int did=was.beginDocument(); try{
  // info about ranges
  try { // !SIC rewrite!
   WARHPSQL wasql = new WARHPSQL(this);
   wasql.exec_sql(getDBC(),"select startid,startid_hex || '/' || bitl::text as startid_hex_b, endid_hex, prange_hex || '/' || pbitl::text as prange_hex_b, ownerid,descr from SAM.V_RangesInfo order by startid,bitl");
  } catch(Exception e) { printerrmsg(e);}
   }finally{was.closeDocument(did);}
 } // do_list()

 /** show SAMRange and all it's properties. */
 private void do_view()
  throws java.sql.SQLException, ZException, WAException
 {
  String key = request.getParameter(wam.PARAM_ZID);
  Long id = Long.valueOf(key);
   int did=was.beginDocument(); try{
  // info about ranges
  if(id != null) try { // !SIC rewrite
   WARHPSQL wasql = new WARHPSQL(this);
   wasql.exec_sql(getDBC(),"select startid_hex || '/' || bitl::text as startid_hex_b, endid_hex, prange_hex || '/' || pbitl::text as prange_hex_b, ownerid,descr from SAM.V_RangesInfo where startid = " + id + " order by startid");
   wasql.exec_sql(getDBC(),"select sid,qtype,start_hex,rend_hex,lastid_hex,lastid,prange_hex || '/' || pbitl::text as prange_hex_b,sorder,ts from SAM.V_RangeSeqInfo where prange = " + id + " order by sid,qtype");
  } catch(Exception e) { printerrmsg(e);}
  }finally{was.closeDocument(did);}
 } // do_view()

 private void print_form_find()
 {
 } // print_form_find()

 // constructors
 public WARHSAMRange(
	javax.servlet.http.HttpServletRequest request,
	javax.servlet.http.HttpServletResponse response,
	java.io.PrintWriter out)
 { super(request,response,out); }
 public WARHSAMRange(WARequestHandler warh) { super(warh); }

} //WARHSAMRange
