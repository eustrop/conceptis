// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

//package ru.mave.ConcepTIS.webapps;
package org.eustrosoft.qtis.webapps;

import java.util.*;
import java.sql.*;
import ru.mave.ConcepTIS.dao.*;
import ru.mave.ConcepTIS.dao.TIS.*;
import ru.mave.ConcepTIS.dao.TISC.*;
import ru.mave.ConcepTIS.dao.SAM.*;

/** Abstract template for typical dobject based document list/view/edit.
 * This class contains implementation of most methods which are common for
 * subclasses, so just a little bit should be implemented by them.
 **/
public abstract class WARHTypicalObject extends WARequestHandler
{

 /** show form of requested action if invalid or no dobject found (for testing and debugging). */
 public static boolean SHOW_TESTING_ACTION = true;

 public abstract String getThisDocSubsys();
 public abstract String getThisDocTarget();
 public abstract int getThisDocTypeCaptionID();

 public abstract void printThisDocGeneralHMenu();
 public abstract RList loadThisDocList(Long ZSID, int rows_per_page, long row_offset)
  throws java.sql.SQLException, ZException;
 public abstract DObject loadThisDocDObject(Long ZOID)
  throws java.sql.SQLException, ZException;
 public abstract DObject newThisDocDObject();
 public abstract DObject extractThisDocDObject(ZFieldSet fs)
  throws java.sql.SQLException, ZException;

 public abstract void printThisDocView(DObject o);
 public abstract void printThisDocEditForm(ZFieldSet fs);

  private RList rl_scopes; // list of SAMScopes
  public RList getSAMScopes()
  {
   if(rl_scopes!=null) return(rl_scopes);
   try{ rl_scopes = getZSystem().listSAMScopes(); }
   catch(Exception e) { }
   if(rl_scopes == null) rl_scopes = new RList();
   return(rl_scopes);
  }

 //
 // semi-abstract records/fields display configuration
 //

 public static int REC_VIEW_HEADER = 1;
 public static int REC_VIEW_ROW = 2;
 public static int REC_VIEW_IGNORE = 3;
 public static int REC_VIEW_PROPERTY = 4;
 public static int REC_VIEW_DEFAULT = REC_VIEW_HEADER;

 public static int F_VIEW_HIDDEN = 1;
 public static int F_VIEW_ZOID = 2;
 public static int F_VIEW_ZOID2ZNAME = 3;
 public static int F_VIEW_DIC = 4;
 public static int F_VIEW_SAMSCOPE = 5;
 public static int F_VIEW_SAMUSER = 6;
 public static int F_VIEW_MILTILINE = 7;
 public static int F_VIEW_TEXT = 8;
 public static int F_VIEW_IGNORE = 9;
 public static int F_VIEW_DEFAULT = F_VIEW_TEXT;

 public static int VIEW_MODE_DEFAULT = 0;
 public static int VIEW_MODE_NORMAL = 1;
 public static int VIEW_MODE_BASIC = 2;
 public static int VIEW_MODE_DEBUG = 3;
 public static int VIEW_MODE_JSON = 4;

 /**(semi-abstratct) info about record (REC_VIEW_DEFAULT and so on). */
 public int getRecordViewType(ZFieldSet fsr)
 {
  return(REC_VIEW_DEFAULT);
 } //getRecordViewType

 /**(semi-abstratct) info about field (F_VIEW_DEFAULT and so on). */
 public int getFieldViewMode(ZField f)
 {
  String fname = f.getName();
  if(ZMessages.isSTD_HEADER_FIELD(fname)) return(F_VIEW_IGNORE);
  if(fname.equals("scope_id")) return(F_VIEW_SAMSCOPE);
  if(fname.equals("sid")) return(F_VIEW_SAMSCOPE);
  if(fname.equals("uid")) return(F_VIEW_SAMUSER);
  if(fname.endsWith("code")) return(F_VIEW_DIC);
  if(fname.endsWith("type")) return(F_VIEW_DIC);
  if(fname.endsWith("_id")) return(F_VIEW_ZOID);
  return(F_VIEW_DEFAULT);
 } // getFieldViewMode

 /**(semi-abstratct) get dictionary for field. */
 public ZDictionary getFieldDic(ZField f)
 {
  return(getZSystem().getDictionary(f.getCode()));
 } // getFieldDic

 /** collection (indexed by ZField.getCode()) of RList objects
  * with set of ZNAME records, one per each F_VIEW_ZOID2ZNAME field.
  */
 private Hashtable ht_ZNLists = null;
 /** result of DObject.createMembersFactory() for target object. */
 private DORecord[] members = null;
 /** result members indexed by DORecord.getTabCode(). */
 private Hashtable ht_members = null;

 /** (semi-abstract) load lists of records/objects related to target
  * DObject o and required for its viewing an editing.
  */
 public void load_DocReferences(DObject o)
 {
  int i,k;
  members = o.createMembersFactory();
  ht_members = new Hashtable();
  for(i=0;i<members.length;i++)
   {ht_members.put(members[i].getTabCode(),members[i]);}
  ht_ZNLists = new Hashtable();
  for(i=0;i<members.length;i++)
  {
   ZFieldSet fs = members[i].toFieldSet();
   for(k=0;k<fs.size();k++){
    ZField f = fs.get(k);
    int view_type = getFieldViewMode(f);
    if(view_type == F_VIEW_ZOID || view_type == F_VIEW_ZOID2ZNAME)
    {
     try{
     RList rlZN = getZSystem().listZNAMEs4IDFIELD(
      members[i].getSubsysCode(), members[i].getTabName(),
      f.getName(),o.getZOID());
     ht_ZNLists.put(f.getCode(),rlZN);
     }
     catch(Exception e){printerrmsg(e);;}
    }
   } //for k
  } //for i
 } //load_DocReferences

 public DORecord getMemberRecordByCode(String code)
 {
 if(ht_members==null)return(null);
 return((DORecord)ht_members.get(code));
 }

 public RList getZNames4Field(ZField f)
 {
 if(ht_ZNLists==null)return(null);
 return((RList)ht_ZNLists.get(f.getCode()));
 }

 public RList getZNames4Field(String field_code)
 {
 if(ht_ZNLists==null)return(null);
 return((RList)ht_ZNLists.get(field_code));
 }

 //
 // view/edit document assistance methods 
 //

 public void print_SomeDocView(DObject o)
 {
  int i,k;
  ZFieldSet fs = o.toFieldSet();
  load_DocReferences(o);
  int rowcount = print_SomeRecordChildren(fs);
  if( rowcount < 1) { // for empty objects
    was.printMsg(wam.INFO_EMPTY_DOBJECT_VIEWED);
    was.beginTable();
    was.printTabField(ZName.F_ZNAME,o.getZNAME());
    was.printTabFieldZOID(ZObject.F_ZOID,o.getZOID());
    was.printTabFieldDic(new ZField(ZObject.F_ZTYPE, o.getZTYPE()),
        getDic(wam.DIC_TIS_OBJECT_TYPE));
    was.closeTable();
  }
 } //print_SomeDocView()

 /** returns number of records displayed. */
 public int print_SomeRecordChildren(ZFieldSet fs)
 {
  boolean is_row = false;
  String last_tab_code = null;
  int rowcount=0;
  for(int i=0;i<fs.countFS();i++)
  {
   ZFieldSet fsr = fs.getFS(i);
   int rec_view = getRecordViewType(fsr);
   if(rec_view == REC_VIEW_ROW) { //case
   // ROW
    if(!fsr.getCode().equals(last_tab_code)) {
     if(is_row) print_SomeRecordTabFooter();
     print_SomeRecordTabHeader(fsr);
    }
    if(!is_row) print_SomeRecordTabHeader(fsr);
    print_SomeRecordTabRow(fsr);
    is_row=true;
   } else if(rec_view == REC_VIEW_IGNORE) { continue;
   } else { // if(rec_view == REC_VIEW_HEADER)
   // HEADER
     if(is_row) print_SomeRecordTabFooter(); is_row=false;
     print_SomeRecord(fsr);
   } //esac
   rowcount++;
   last_tab_code = fsr.getCode();
  } //for countFS()
  return(rowcount);
 } //print_SomeRecordChildren()

 public void print_SomeRecord(ZFieldSet fs)
 {
  int i,k;
  was.beginTable();
  for(k=0;k<fs.size();k++)
  {
   ZField f=fs.get(k); String fname=f.getName();
   int f_view = getFieldViewMode(f);
   if(f_view == F_VIEW_HIDDEN) continue;
   else if(f_view == F_VIEW_IGNORE) continue;
   else if(f_view == F_VIEW_ZOID) {
    //was.printTabFieldZOID(fs,fname);
    was.printTabFieldZOID2ZNAME(fs,fname,getZNames4Field(f), wam.SUBSYS_TISC);
   }
   // else if(f_view == F_VIEW_ZOID2ZNAME
   else if(f_view == F_VIEW_DIC)
    was.printTabFieldDic(fs,fname,getFieldDic(f));
   else if(f_view == F_VIEW_SAMSCOPE)
    was.printTabFieldList(fs,fname,getSAMScopes(),
	wam.SUBSYS_SAM, wam.TARGET_SAMSCOPE);
   // else if(f_view == F_VIEW_SAMUSER)
   // else if(f_view == F_VIEW_MILTILINE)
   else
    was.printTabField(fs,fname);
  } //fs.size()
  was.closeTable(); //header
  if(fs.countFS()>0) //print nested records
  {
  was.beginIndent(); // ... with Indentation
   print_SomeRecordChildren(fs);
  was.closeIndent();
  }
 } //print_SomeRecord

 public void print_SomeRecordTabHeader(ZFieldSet rfs){throw(new RuntimeException("not implemented"));}
 public void print_SomeRecordTabRow(ZFieldSet rfs){throw(new RuntimeException("not implemented"));}
 public void print_SomeRecordTabFooter(){was.closeTable();}

 public void print_SomeDocEditForm(ZFieldSet fs)
 {
  //load_DocReferences(o);
  int i;
  int rowcount = print_SomeRecordEditChildren(fs);
  if( rowcount < 1) { // for empty objects
   was.printDebug("empty object");
  }
  // Add children records buttons
  DORecord[] members = newThisDocDObject().createMembersFactory();
  for(i=0;i<members.length; i++)
  {
   DORecord dr = members[i];
   if(dr.getParentTabCode() == null)
    was.printButtonAddRecord(dr.getTabCode());was.println();
  }
   //was.printDebug(fs.toJSONString());
 } // print_SomeDocEditForm(ZFieldSet fs)

 /** returns number of records displayed. */
 public int print_SomeRecordEditChildren(ZFieldSet fs)
 {
  boolean is_row = false;
  String last_tab_code = null;
  int rowcount=0;
  for(int i=0;i<fs.countFS();i++)
  {
   ZFieldSet fsr = fs.getFS(i);
   print_SomeRecordEdit(fsr);
   rowcount++;
  } //for countFS()
  return(rowcount);
 } //print_SomeRecordChildren()

 public void print_SomeRecordEdit(ZFieldSet fs)
 {
  int i,k;
  DORecord dor = getMemberRecordByCode(fs.getCode());
  was.beginRecord(); 
  was.printSTDFieldsInputDORecord(fs);
  was.beginTable();
  was.printTabRecordDeletionStatus(fs);
  for(k=0;k<fs.size();k++)
  {
   ZField f=fs.get(k); String fname=f.getName();
   int f_view = getFieldViewMode(f);
   if(f_view == F_VIEW_HIDDEN) continue;
   else if(f_view == F_VIEW_IGNORE) continue;
   else if(f_view == F_VIEW_ZOID) {
    was.printTabFieldInput(f);
    //was.printTabFieldZOID2ZNAME(fs,fname,getZNames4Field(f), wam.SUBSYS_TISC);
   }
   // else if(f_view == F_VIEW_ZOID2ZNAME
   else if(f_view == F_VIEW_DIC)
    was.printTabFieldInputDic(f,getFieldDic(f));
   else if(f_view == F_VIEW_SAMSCOPE)
    //was.printTabFieldList(fs,fname,getSAMScopes(),
//	wam.SUBSYS_SAM, wam.TARGET_SAMSCOPE);
      was.printTabFieldInput(f);
   // else if(f_view == F_VIEW_SAMUSER)
   // else if(f_view == F_VIEW_MILTILINE)
   else
    was.printTabFieldInput(f);
  } //fs.size()
  was.closeTable(); //header
  // Add children records buttons
  if(dor != null)
  {
   String child_codes[] = dor.getChildTabCodes();
   for(i=0;i<child_codes.length; i++)
   {
    was.printButtonAddRecord(child_codes[i]);was.println();
   }
  }
  // Children records
  if(fs.countFS()>0) //print nested records
  {
  was.beginIndent(); // ... with Indentation
   print_SomeRecordEditChildren(fs);
  was.closeIndent();
  }
  was.closeRecord();
 } // print_SomeRecordEdit(ZFieldSet fs)
 public void print_SomeRecordEditAdd(ZFieldSet fs){throw(new RuntimeException("not implemented"));}
 public void print_SomeRecordEditTabRow(ZFieldSet fs){throw(new RuntimeException("not implemented"));}
 public void print_SomeRecordEditTabRowAdd(ZFieldSet fs){throw(new RuntimeException("not implemented"));}

 // view/edit document assistance methods (private members)

 private void print_form_edit(ZFieldSet fs, int view_mode)
 {
  ZObject ZO = new ZObject(); // short alias for record
  int i;
  String request_name = wam.REQUEST_SAVE;
  if(view_mode == VIEW_MODE_BASIC) request_name = wam.REQUEST_SAVE_BASIC;
  fs.importMissedData(newThisDocDObject()); // correct form's data
  was.printWarnMsgEdit(fs);
  try{ rl_scopes = getZSystem().listSAMScopes(); }
    catch(Exception e) { printerrmsg(e); rl_scopes = null; }
  int did=was.beginDocument();
  try{
   // header
   was.beginFormPost(getThisDocSubsys(),request_name,getThisDocTarget());
   // put ZID into request form
   Long ZID = wam.string2Long(fs.getValue(ZObject.F_ZOID));
   if(ZID != null){ was.printFieldInputHidden(wam.PARAM_ZID,ZID); }
   //
   if(view_mode == VIEW_MODE_BASIC) was.printButtonMenuBarEditBasic();
   else was.printButtonMenuBarEdit();
   was.beginRecord(); //DObject
   was.printSTDFieldsInputDObject(fs,rl_scopes, getZSystem().getDictionary(wam.DIC_SLEVEL));
   if(view_mode == VIEW_MODE_BASIC) print_SomeDocEditForm(fs);
   else printThisDocEditForm(fs);
  }finally{was.closeDocument(did);}
 } // print_form_edit()


 private void print_TabRowList(DObject r)
 {
    was.beginTableRow();
     was.printCellZOID(getThisDocSubsys(),getThisDocTarget(),r.getZOID());
     was.printCellZSID(r.getZSID());
     was.printCellValue(wam.obj2string(r.getZLVL()));
     was.printCellValue(r.getZNAME());
    was.closeTableRow();
 } // print_TabRow_ZObject

 private void print_TabHeaderList()
 {
    was.beginTableRow();
     ZFieldSet ZO=(new ZObject()).toFieldSet(), ZN=(new ZName()).toFieldSet();
     was.printHCellCaption(ZO.get(ZObject.F_ZOID));
     was.printHCellCaption(ZO.get(ZObject.F_ZSID));
     was.printHCellCaption(ZO.get(ZObject.F_ZLVL));
     was.printHCellCaption(ZN.get(ZName.F_ZNAME));
    was.closeTableRow();
 } //print_TabHeader_ZObject

 private void print_form_list(ZFieldSet fs)
 {
  ZObject r = new ZObject(); // short alias for ZObject class
  try{ rl_scopes = getZSystem().listSAMScopes(); }
    catch(Exception e) { printerrmsg(e); rl_scopes = null; }
  int did=was.beginDocument();
  try{
   // header
   was.beginFormGet(getThisDocSubsys(),wam.REQUEST_LIST,getThisDocTarget());
   was.beginRecord();
   was.beginTable();
    was.printTabFieldInputList(fs.get(r.F_ZSID),rl_scopes,wam.ITEM_ANY_SCOPE);
    was.printTabFieldRowsPager(fs);
   was.closeTable();
   was.printButtonMenuBarList();
   was.closeRecord();
   was.closeForm();
  }finally{was.closeDocument(did);}
 } // print_form_list()

 //
 // interfaces/abstracts implementation
 //

 public void processREST() {
  String req = request.getParameter(wam.PARAM_REQUEST);
  try{ db_logon(); } catch(Exception e){printerrmsg(e); return; }
  try{
   if(wam.REQUEST_VIEW.equals(req))  do_view_rest(); 
   else if(wam.REQUEST_VIEW_BASIC.equals(req))  do_view_basic(); 
   else if(wam.REQUEST_CREATE.equals(req)) do_create();
   else if(wam.REQUEST_ROLLBACK.equals(req)) do_rollback();
   else if(wam.REQUEST_DELETE.equals(req)) do_delete();
   else if(wam.REQUEST_FIND.equals(req)) do_find();
   else if(wam.REQUEST_SAVE.equals(req)) do_save();
   else if(wam.REQUEST_SAVE_BASIC.equals(req)) do_save_basic();
   else if(wam.REQUEST_EDIT.equals(req)) do_edit_rest();
   else if(wam.REQUEST_EDIT_BASIC.equals(req)) do_edit_basic();
   else if(wam.REQUEST_LIST.equals(req)) do_list();
   else throw(new WAException(wam,wam.ERROR_NOT_IMPLEMENTED));
  }catch(Exception e){was.printRESTException(e); return; }
  finally{ db_logoff(); } 
 }

 public void process()
 {
  String req = request.getParameter(wam.PARAM_REQUEST);
  was.beginHMenu();
  was.printHMenuSeparator();
  printThisDocGeneralHMenu();
  was.closeHMenu();
  was.printSeparator();
  try{ db_logon(); } catch(Exception e){printerrmsg(e); return; }
  try{
   if(wam.REQUEST_VIEW.equals(req))  do_view(); 
   else if(wam.REQUEST_VIEW_BASIC.equals(req))  do_view_basic(); 
   else if(wam.REQUEST_CREATE.equals(req)) do_create();
   else if(wam.REQUEST_ROLLBACK.equals(req)) do_rollback();
   else if(wam.REQUEST_DELETE.equals(req)) do_delete();
   else if(wam.REQUEST_FIND.equals(req)) do_find();
   else if(wam.REQUEST_SAVE.equals(req)) do_save();
   else if(wam.REQUEST_SAVE_BASIC.equals(req)) do_save_basic();
   else if(wam.REQUEST_EDIT.equals(req)) do_edit();
   else if(wam.REQUEST_EDIT_BASIC.equals(req)) do_edit_basic();
   else if(wam.REQUEST_BTNMENU.equals(req)) process_btnmenu();
   else if(req == null || wam.REQUEST_LIST.equals(req)) do_list();
   else throw(new WAException(wam,wam.ERROR_NOT_IMPLEMENTED));
  }catch(Exception e){printerrmsg(e); return; }
  finally{ db_logoff(); } 
 } // process

 /** process request passed per button clicking. */
 private void process_btnmenu()
  throws java.sql.SQLException, ZException, WAException
 {
  String key = request.getParameter(wam.PARAM_ZID);
  Long ZID = null; try{ZID = Long.valueOf(key);} catch(Exception e){}
  String ZTYPE = request.getParameter(wam.PARAM_TARGET);
  if(request.getParameter(wam.PARAM_BTN_CANCEL) != null) {do_list();}
  else if(request.getParameter(wam.PARAM_BTN_DELETE) != null) {do_delete(); }
  else if(request.getParameter(wam.PARAM_BTN_VIEW) != null) {do_view(); }
  else if(request.getParameter(wam.PARAM_BTN_VIEW_BASIC) != null) {do_view_basic(); }
  else if(request.getParameter(wam.PARAM_BTN_METADATA) != null)
   {was.sendRedirect(response,wam.SUBSYS_TIS,wam.REQUEST_VIEW,ZTYPE,ZID); }
  else if(request.getParameter(wam.PARAM_BTN_EDIT) != null) {do_edit();}
  else if(request.getParameter(wam.PARAM_BTN_EDIT_BASIC) != null) {do_edit_basic();}
  else throw(new WAException(wam,wam.ERROR_NOT_IMPLEMENTED));
 } // process_btnmenu()

 /** list target objects. */
 private void do_list()
  throws java.sql.SQLException, ZException
 {
  ZFieldSet fs = was.extractFieldSet(request), etalon_fs;
  int rows_per_page=was.getRowsPerPage(fs);
  long row_offset=was.getRowOffset(fs);
  // correct user-supplied-filter data and display it again
  ZObject ZO = new ZObject();
   etalon_fs = ZO.toFieldSet();
   fs.importMissedData(etalon_fs);
   try { ZO = new ZObject(fs); } catch(Exception e){ printerrmsg(e);}
  if(was.isButtonPressed(request,wam.PARAM_BTN_SHOW)) row_offset = 0;
  if(was.isButtonPressed(request,wam.PARAM_BTN_NEXT)) row_offset+=rows_per_page;
  fs.setValue(wam.PARAM_ROW_OFFSET, new Long(row_offset));
  if(was.isButtonPressed(request,wam.PARAM_BTN_CANCEL)) fs = etalon_fs;
  print_form_list(fs); etalon_fs = fs;
  // cancel request if no one of SHOW/NEXT/REFRESH button was pressed
  if( !( was.isButtonPressed(request,wam.PARAM_BTN_SHOW) ||
	 was.isButtonPressed(request,wam.PARAM_BTN_NEXT) ||
	 was.isButtonPressed(request,wam.PARAM_BTN_REFRESH) )) return;
  // load list of objects/records
  RList rl = loadThisDocList(ZO.ZSID,rows_per_page,row_offset);
  // display list of objects
   int did=was.beginDocument(); try{
   was.beginTable();
   print_TabHeaderList();
   for(int i=0;i<rl.size();i++)
    { print_TabRowList( (DObject)rl.get(i) ); }
   was.closeTable();
   //was.println(wam.getMsgText(wam.TOTAL_ROWS,Integer.toString(rl.size())));
   was.printTotalRowsPager(request,etalon_fs,rl.size());
   }finally{was.closeDocument(did);}
 } // do_list()

 /** list target objects. */
 private void do_list_rest() throws java.sql.SQLException, ZException
 {
/*/
  int rows_per_page= 50; //was.getRowsPerPage(fs);
  long row_offset= 0; //was.getRowOffset(fs);
  ZObject ZO = new ZObject();
  // load list of objects/records
  RList rl = loadThisDocList(ZO.ZSID,rows_per_page,row_offset);
*/
 } //do_list_rest()

 private void do_cancel()
 {
  String key = request.getParameter(wam.PARAM_ZID);
  Long ZOID = wam.string2Long(key);
  if(ZOID != null)
     was.sendRedirect(response,getThisDocSubsys(),wam.REQUEST_VIEW,getThisDocTarget(),ZOID);
  else
     was.sendRedirect(response,getThisDocSubsys(),wam.REQUEST_LIST,getThisDocTarget());
 } //do_cancel

 private void do_view_basic()
  throws java.sql.SQLException, ZException, WAException
 { do_view(VIEW_MODE_BASIC);}

 private void do_view()
  throws java.sql.SQLException, ZException, WAException
 {do_view(VIEW_MODE_DEFAULT);} 

 private void do_view(int view_mode)
  throws java.sql.SQLException, ZException, WAException
 {
  String key = request.getParameter(wam.PARAM_ZID);
  Long ZOID = wam.string2Long(key);
  ZFieldSet fs;
  DObject o = loadThisDocDObject(ZOID);
  //if(!o.isExists()) throw(new WAException(wam,wam.ERROR_NOT_FOUND));
  if(!checkFoundDObject(o,getThisDocTarget(),ZOID) && !SHOW_TESTING_ACTION) return;
  int did=was.beginDocument(); try{
   if(view_mode == VIEW_MODE_BASIC){ // select view mode switch
    was.printMenuBarViewDObjectTISCBasic(o.getZTYPE(), o.getZOID());
    print_SomeDocView(o);
    was.printSeparator();
    //was.printDebug(o.toFieldSet().toJSONString()); // DEBUGGING
   } else if(view_mode == VIEW_MODE_NORMAL){
    was.printMenuBarViewDObjectTISC(o.getZTYPE(), o.getZOID());
    printThisDocView(o);
   } else { // VIEW_MODE_DEFAULT
    was.printMenuBarViewDObjectTISC(o.getZTYPE(), o.getZOID());
    printThisDocView(o);
   } // end select view mode switch
  }finally{was.closeDocument(did);}
 } // do_view()

 private void do_view_rest()
  throws java.sql.SQLException, ZException, WAException
 {
  String key = request.getParameter(wam.PARAM_ZID);
  Long ZOID = wam.string2Long(key);
  ZFieldSet fs;
  DObject o = loadThisDocDObject(ZOID);
  if(o != null) was.printRESTRawString(o.toFieldSet().toJSONString());
  else was.printRESTNullObject();
  
 } // do_view_rest()

 private void do_edit_rest()
  throws java.sql.SQLException, ZException, WAException
 {
  String key = request.getParameter(wam.PARAM_ZID);
  Long ZOID = wam.string2Long(key);
  ZFieldSet fs;
  DObject o = loadThisDocDObject(ZOID);
  if(o != null) was.printRESTRawString(o.toFieldSet().toZJson().toJSONString());
  else was.printRESTNullObject();
  
 } // do_view_rest()

 private void do_create() throws ZException, WAException
 {
 //ZFieldSet fs = (new DObject(getThisDocTarget())).toFieldSet();
 ZFieldSet fs = newThisDocDObject().toFieldSet();
 print_form_edit(fs, VIEW_MODE_DEFAULT);
 }

 private void do_rollback() throws ZException, WAException
 {throw(new WAException(wam,wam.ERROR_NOT_IMPLEMENTED));}

 private void do_edit_basic()
	throws java.sql.SQLException,ZException,WAException
 {
   do_edit(VIEW_MODE_BASIC);
 } //do_edit_basic()

 private void do_edit()
	throws java.sql.SQLException,ZException,WAException
 { do_edit(VIEW_MODE_DEFAULT); }

 private void do_edit(int view_mode)
	throws java.sql.SQLException,ZException,WAException
 {
  String key = request.getParameter(wam.PARAM_ZID);
  Long ZOID = wam.string2Long(key);
  if(ZOID==null) throw(new WAException(wam,wam.ERROR_NOT_FOUND));
  Long ZVER = wam.string2Long(request.getParameter(wam.PARAM_ZVER));
  String ZSTA = request.getParameter(wam.PARAM_ZSTA);
  DObject o = loadThisDocDObject(ZOID);
  //if(!o.isExists()) throw(new WAException(wam,wam.ERROR_NOT_FOUND));
  if(!checkFoundDObject(o,getThisDocTarget(),ZOID) && !SHOW_TESTING_ACTION) return;
  if(wam.SZ_ZSTA_I.equals(ZSTA)){ //edit intermediate version
   getZSystem().applyDObjectIntermediate(o,ZVER);
   was.printWarnMsgEditIntermediate(o);
  }
  else { was.printWarnMsgEdit(o); }
  int did=was.beginDocument();
  //was.print(o.toFieldSet().toDebugString());
  try{
    print_form_edit(o.toFieldSet(), view_mode); 
  }
  finally{was.closeDocument(did);}
 } // do_edit()

 private void do_save_basic()
	throws java.sql.SQLException,ZException,WAException
 {
  do_save(VIEW_MODE_BASIC);
 } //do_save_basic()

 private void do_save()
	throws java.sql.SQLException,ZException,WAException
 { do_save(VIEW_MODE_DEFAULT); }

 private void do_save(int view_mode)
	throws java.sql.SQLException,ZException,WAException
 {
  DObject o=null; // passed object or missed data donor
  boolean was_opened = false; // DObject.isOpened();
  // get object's data from requests's parameters
  ZFieldSet fs = was.extractFieldSet(request);
  //was.printErrMsg("SAVING DISABLED");
  //was.print(fs.toDebugString());
  // cancel request
  if(request.getParameter(wam.PARAM_BTN_CANCEL) != null)
   { do_cancel(); return; }
  // try to extract DObject from FieldSet & load references
  try{
   o = extractThisDocDObject(fs);
  } catch(Exception e){printerrmsg(e);}
  if( o == null) o = newThisDocDObject();
  load_DocReferences(o);
  // save object
  if(request.getParameter(wam.PARAM_BTN_SAVE) != null)
  try {
   //was.print(o.toFieldSet().toDebugString());
   was_opened = o.isOpened();
   getZSystem().saveDObject(o,!was_opened);
   was.printMsg(wam.INFO_SUCCESS_SAVED);
   was.print(" (");
   was.printHRefView(o.getCaption(),getThisDocSubsys(),getThisDocTarget(),o.getZOID());
   was.print(")");
   if(was_opened) {
    was.println(wam.getMsg(wam.WARN_OBJECT_WAS_PREOPENED_BEFORE_EDITION));
    was.println(wam.getMsg(wam.WARN_FOLLOW_ONE_OF_TO_FINISH));
    was.beginHMenu(); //was.printHMenuSeparator();
    was.printHMenuAction(wam.MNU_COMMIT,wam.SUBSYS_TIS,o.getZTYPE(),o.getZOID(),o.getZVER());
    was.printHMenuAction(wam.MNU_ROLLBACK,wam.SUBSYS_TIS,o.getZTYPE(),o.getZOID(),o.getZVER());
    was.closeHMenu();
   }
   return;
  } catch(Exception e){printerrmsg(e);}
  if(request.getParameter(wam.PARAM_BTN_EDIT_BASIC) != null) view_mode=VIEW_MODE_BASIC;
  if(request.getParameter(wam.PARAM_BTN_EDIT) != null) view_mode=VIEW_MODE_NORMAL;
  // correct data and display it again
  if(o != null) if(!was_opened) if(o.isOpened()) try {
   getZSystem().rollbackDObject(o); o=null;
  }catch(Exception e){printerrmsg(e);}
  if(o != null) fs=o.toFieldSet();
  int did=was.beginDocument();
  try{ print_form_edit(fs, view_mode); } finally{was.closeDocument(did);}
 } //do_save()

 private void do_delete()
  throws java.sql.SQLException, ZException, WAException
 {
  Long ZOID = wam.string2Long(request.getParameter(wam.PARAM_ZID));
  if(request.getParameter(wam.PARAM_BTN_NO) != null) {do_cancel();return;}
  else if(request.getParameter(wam.PARAM_BTN_YES) != null) {
   ZExecStatus es = getZSystem().delete_object(getThisDocTarget(),ZOID,null);
   getZSystem().checkExecStatus(es); // throws ZException
   was.printMsg(wam.INFO_SUCCESS_DELETED);
   was.printExecStatus(es);
   return;
  }
  // else
  DObject o=new DObject(ZOID);
  try { getZSystem().loadDObjectMetadata(o); }
  catch(Exception e) { printerrmsg(e); return; }
  if(!checkFoundDObject(o,getThisDocTarget(),ZOID) && !SHOW_TESTING_ACTION) return;
  was.printQuestDeleteDObject(getThisDocTypeCaptionID(),o,
	getThisDocSubsys(),getThisDocTarget(),ZOID);
 } // do_delete()}

 /** check and warn user if passed DObject not exists or not confirm to target or to ZOID,
  * (false if not exists or not conform to).
  * getThisDocTarget() and value of wam.PARAM_ZID used instead of nulled target or ZOID.
  */ 
 private boolean checkFoundDObject(DObject o, String target, Long ZOID)
  throws ZException, WAException
 {
  boolean check_fail = false;
  if(target == null) target = getThisDocTarget();
  if(ZOID == null) ZOID = wam.string2Long(request.getParameter(wam.PARAM_ZID));
  if(!o.isExists()) { check_fail = true;
   was.printWarnMsgObjectNotExists(target,ZOID,getThisDocTypeCaptionID());
  }
  else if(ZOID != null) if( !ZOID.equals(o.getZOID()) ) { check_fail = true;
   was.printWarnMsgNoTargetZOID(o,target,ZOID,getThisDocTypeCaptionID());
  }
  else if(!target.equals(o.getZTYPE())) { check_fail = true;
   was.printWarnMsgNoTargetZTYPE(o,target,ZOID,getThisDocTypeCaptionID());
  }
  if(check_fail) {
   if(SHOW_TESTING_ACTION){
    was.printMsg(wam.WARN_NEXT_ACTION_IS_FOR_TESTING);
    was.printSeparator();
    }
   //throw(new WAException(wam,wam.ERROR_NOT_FOUND));
  }
  return(check_fail);
 } // checkFoundDObject()

 private void do_find()
  throws java.sql.SQLException, ZException
 {do_list();}

 // constructors
 public WARHTypicalObject(
	javax.servlet.http.HttpServletRequest request,
	javax.servlet.http.HttpServletResponse response,
	java.io.PrintWriter out)
 { super(request,response,out); }
 public WARHTypicalObject(WARequestHandler warh) { super(warh); }

} //WARHTypicalObject
