// ConcepTIS project/QTIS project
// (c) Alex V Eustrop 2009-2010
// (c) Alex V Eustrop & EustroSoft.org 2023
// see LICENSE.ConcepTIS at the project's root directory
//
// $Id$
//

//package ru.mave.ConcepTIS.webapps;
package org.eustrosoft.qtis.webapps;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import org.eustrosoft.qtis.webapps.DIC.*;
import org.eustrosoft.qtis.webapps.SAM.*;
import org.eustrosoft.qtis.webapps.TIS.*;
import org.eustrosoft.qtis.webapps.TISC.*;
import org.eustrosoft.qtis.webapps.FS.*;
import org.eustrosoft.qtis.webapps.QR.*;
import org.eustrosoft.qdbp.*;
import org.eustrosoft.qtis.SessionCookie.*;


public class WARHMain extends WARequestHandler{
    private boolean showTopMenu = false; // show topMenu() at each page
    public boolean setTopMenu(boolean v) { boolean ov = v; showTopMenu = v; return(ov); }

    // constructors
    public WARHMain(HttpServletRequest request,HttpServletResponse response,java.io.PrintWriter out){
        super(request,response,out);
    } // WARHMain(HttpServletRequest,JspWriter)

    public WARHMain(WARequestHandler warh) {
        super(warh);
    } // WARHMain(WARequestHandler warh)


    /** find, create and return specific request handler using request's parameters (subsys, request, target)
     * 
     */
    public WARequestHandler findRequestHandler()
    {
     String pSubsys = request.getParameter(wam.PARAM_SUBSYS);
     String pRequest = request.getParameter(wam.PARAM_REQUEST);
     String pTarget = request.getParameter(wam.PARAM_TARGET);
     String pZID = request.getParameter(wam.PARAM_ZID);
     Long ZID = wam.string2Long(pZID);
     WARequestHandler warh = null;
     if(wam.SUBSYS_PSQL.equals(pSubsys)) warh = new WARHPSQL(this);
     else if(wam.SUBSYS_DIC.equals(pSubsys)) warh = new WARHDictionary(this);
     else if(wam.SUBSYS_TIS.equals(pSubsys)) { warh = new WARHTISObject(this); }
     else if(wam.SUBSYS_SAM.equals(pSubsys)){
            if(wam.TARGET_SAMUSER.equals(pTarget)) warh = new WARHSAMUser(this);
            else if(wam.TARGET_SAMGROUP.equals(pTarget)) warh = new WARHSAMGroup(this);
            else if(wam.TARGET_SAMSCOPE.equals(pTarget)) warh = new WARHSAMScope(this);
            else if(wam.TARGET_SAMRANGE.equals(pTarget)) warh = new WARHSAMRange(this);
     }
     else if(wam.SUBSYS_TISC.equals(pSubsys)){
            if(wam.TARGET_AREA.equals(pTarget)) warh = new WARHTISCArea(this);
            else if(wam.TARGET_CONTAINER.equals(pTarget)) warh=new WARHTISCContainer(this);
            else if(wam.TARGET_DOCUMENT.equals(pTarget)) warh=new WARHTISCDocument(this);
        }
      return(warh);
    } // findRequestHandler()


    public void process(){

        String pSubsys = request.getParameter(wam.PARAM_SUBSYS);
        String pRequest = request.getParameter(wam.PARAM_REQUEST);
        String pTarget = request.getParameter(wam.PARAM_TARGET);
        String pZID = request.getParameter(wam.PARAM_ZID);
        String flagJS = request.getParameter("flagJS");
        Long ZID = wam.string2Long(pZID);
        WARequestHandler warh = null;
        // if (flagJS !=null && !flagJS.equals("flag")) { // I don't understand this, possible example of bad code for discuss (Eustrop 2019/08/08)
	if(showTopMenu) topMenu();
        if(wam.SUBSYS_PSQL.equals(pSubsys)) warh = new WARHPSQL(this);
        else if(wam.SUBSYS_DIC.equals(pSubsys)) warh = new WARHDictionary(this);
        else if(wam.SUBSYS_TIS.equals(pSubsys)) { warh = new WARHTISObject(this); }
        else if(wam.SUBSYS_FS.equals(pSubsys)) { warh = new WARHFSFile(this); }
        else if(wam.SUBSYS_QR.equals(pSubsys)) { warh = new WARHQRSubsys(this); }
        else if(wam.SUBSYS_SAM.equals(pSubsys)){

            if(wam.TARGET_SAMUSER.equals(pTarget)) warh = new WARHSAMUser(this);
            else if(wam.TARGET_SAMGROUP.equals(pTarget)) warh = new WARHSAMGroup(this);
            else if(wam.TARGET_SAMSCOPE.equals(pTarget)) warh = new WARHSAMScope(this);
            else if(wam.TARGET_SAMRANGE.equals(pTarget)) warh = new WARHSAMRange(this);
            else {
                menuSAM();
                return;
            }

        }else if(wam.SUBSYS_TISC.equals(pSubsys)){
            if(pTarget == null && ZID != null) { // resolve dobject's type code 

                try{
                    db_logon();
                    pTarget = getZSystem().resolveZTYPE(ZID);
                    if(pTarget == null) pSubsys = wam.SUBSYS_TIS;

                    was.sendRedirect(response,pSubsys,pRequest,pTarget,ZID);

                }catch(Exception e){ printerrmsg(e); }
                finally{ db_logoff(); }
                return;

            } if(wam.TARGET_AREA.equals(pTarget)) warh = new WARHTISCArea(this);
            else if(wam.TARGET_CONTAINER.equals(pTarget)) warh=new WARHTISCContainer(this);
            else if(wam.TARGET_DOCUMENT.equals(pTarget)) warh=new WARHTISCDocument(this);
            else {
                menuTISC();
                return;
            }
        }

        if(warh != null) warh.process();
        else{
            if(wam.SUBSYS_MAIN.equals(pSubsys) && wam.REQUEST_LOGIN.equals(pRequest) ) {warh = new WARHLogin(this); warh.process();} 
            else if(wam.SUBSYS_MAIN.equals(pSubsys) || !request.getParameterNames().hasMoreElements() ) mainMenu();
            else was.printErrMsg(wam.getMsgText(wam.ERROR_CALL_NOT_IMPLEMENTED));
        }
    } // process()

    public void processREST()
    {
        String pSubsys = request.getParameter(wam.PARAM_SUBSYS);
        String pRequest = request.getParameter(wam.PARAM_REQUEST);
        String pTarget = request.getParameter(wam.PARAM_TARGET);
        String pZID = request.getParameter(wam.PARAM_ZID);
        String flagJS = request.getParameter("flagJS");
        Long ZID = wam.string2Long(pZID);
        WARequestHandler warh = null;
	warh = findRequestHandler();
	if(warh == null) was.printRESTInvalidRequest();
	else warh.processREST();
    } // processREST()

    public void topMenu(){
	 was.beginHMenu();
	 was.printHMenuItem(wam.MNU_MAIN);
	 was.printHMenuItem(wam.MNU_SAM_MAIN);
	 was.printHMenuItem(wam.MNU_TISC_MAIN);
	 was.printHMenuItem(wam.MNU_QR_MAIN);
	 was.printHMenuItem(wam.MNU_FS_MAIN);
	 was.printHMenuItem(wam.MNU_DIC_MAIN);
	 was.printHMenuItem(wam.MNU_PSQL_MAIN);
			//was.printHMenuItem("<TISC>",wam.SUBSYS_TISC,null);
			//was.printHMenuItem("<SQL ad hoc>",wam.SUBSYS_PSQL,null);
	 was.closeHMenu();
   } //topMenu()

    public void login() 
   {
    
 // 1. get or create DB pool
 QDBPool dbp = QDBPool.get(getQDBPoolName());
 QDBPSession dbps = null;
    try{
 if(dbp == null){dbp = new QDBPool(getQDBPoolName(),getQDBPoolURL(),getQDBPoolClass()); QDBPool.add(dbp);}
 // 2. get session
 // 2.1. get session cookie and its value if so
 QTISSessionCookie session_cookie = new QTISSessionCookie(request, response);
 // 2.2. get session by cookie
 dbps = dbp.logon(session_cookie.value());
 // 2.3. renew session if ready (reserved for future use)
 if(dbps != null && dbps.isSessionRenewReady()) {dbps.renewSession(); session_cookie.set(dbps.getSessionSecretCookie(),dbps.getSessionCookieMaxAge()); }
 if(dbps == null) dbps = dbp.createSession(); // temporary session stub, not stored in the pool
 // END get session

    //was.printErrMsg(wam.getMsgText(wam.ERROR_CALL_NOT_IMPLEMENTED));
    String btn_login=request.getParameter("btn_login");
    String btn_logout=request.getParameter("btn_logout");
    if(btn_logout != null)
    {
     dbps.logout();
     session_cookie.delete();
     out.println("Logout!<br>");
    }
    if(btn_login != null)
    {
     String login=request.getParameter("login");
     String password=request.getParameter("password");
     try{
      if(dbps != null) dbps.logout();
      dbps = dbp.logon(login,password);
      session_cookie.set(dbps.getSessionSecretCookie(),dbps.getSessionCookieMaxAge());
      out.println("login Ok!<br>");
      }
      catch(Exception e){was.printErrMsg(e);}
    }
    } catch(Exception e){was.printErrMsg(e);}
    out.println("session cookie should expare at (s):" + dbps.getSessionCookieExpire());
    if(!request.isSecure()){
        out.println("Logon over insecure connection depricated");
    }
    else {
     out.println("<form method=\"POST\" action=\""  + getCGI() + "?subsys=main&request=login\">");
     out.println("Login:<input type=\"text\" name=\"login\" value=\"\"><br>");
     out.println("Password:<input type=\"password\" name=\"password\" value=\"\"></br>");
     out.println("<input type=\"submit\" name=\"btn_login\" value=\"login\">");
     out.println("<input type=\"submit\" name=\"btn_logout\" value=\"logout\">");
     out.println("</form>");
    }

   } // login()

    public void mainMenu(){

        was.beginTMenu();
        was.printTMenuCurrent(wam.MNU_MAIN);
        was.beginTMenu();
        //was.printTMenuItem(wam.MNU_MAIN);
        was.printTMenuItem(wam.MNU_LOGIN_MAIN);
        was.printTMenuSeparator();
        was.printTMenuItem(wam.MNU_TISC_MAIN);
        was.printTMenuItem(wam.MNU_SAM_MAIN);
        was.printTMenuItem(wam.MNU_QR_MAIN);
        was.printTMenuItem(wam.MNU_FS_MAIN);
        was.printTMenuSeparator();
        was.printTMenuItem(wam.MNU_TIS_MAIN);
        was.printTMenuItem(wam.MNU_DIC_MAIN);
        was.printTMenuSeparator();
        was.printTMenuItem(wam.MNU_PSQL_MAIN);
        was.closeTMenu();
        was.printTMenuItem(wam.MNU_HELP_MAIN);
        was.closeTMenu();
    } // mainMenu()

    public void menuSAM(){

        was.beginTMenu();
        was.printTMenuCurrent(wam.MNU_SAM_MAIN);
        was.beginTMenu();
        was.printTMenuItem(wam.MNU_SAM_USER_LIST);
        was.printTMenuItem(wam.MNU_SAM_USER_CREATE);
        was.printTMenuItem(wam.MNU_SAM_USER_FIND);
        was.printTMenuSeparator();
        was.printTMenuItem(wam.MNU_SAM_GROUP_LIST);
        was.printTMenuItem(wam.MNU_SAM_GROUP_CREATE);
        was.printTMenuItem(wam.MNU_SAM_GROUP_FIND);
        was.printTMenuSeparator();
        was.printTMenuItem(wam.MNU_SAM_SCOPE_LIST);
        was.printTMenuItem(wam.MNU_SAM_SCOPE_CREATE);
        was.printTMenuItem(wam.MNU_SAM_SCOPE_FIND);
        was.printTMenuSeparator();
        was.printTMenuItem(wam.MNU_SAM_RANGE_LIST);
        was.printTMenuItemCustomPSQL(wam.getMsgText(wam.CAPTION_CREATE_DBOWNER_USER_ONCE),wam.getSQL(wam.SQL_CREATE_DBOWNER_USER_ONCE));
        was.closeTMenu();
        was.closeTMenu();
    } //menuSAM

    public void menuTISC(){

        was.beginTMenu();
        was.printTMenuCurrent(wam.MNU_TISC_MAIN);
        was.beginTMenu();
        was.printTMenuItem(wam.MNU_TISC_AREA_LIST);
        was.printTMenuItem(wam.MNU_TISC_AREA_CREATE);
        was.printTMenuItem(wam.MNU_TISC_AREA_FIND);
        was.printTMenuSeparator();
        was.printTMenuItem(wam.MNU_TISC_CONTAINER_LIST);
        was.printTMenuItem(wam.MNU_TISC_CONTAINER_CREATE);
        was.printTMenuItem(wam.MNU_TISC_CONTAINER_FIND);
        was.printTMenuSeparator();
        was.printTMenuItem(wam.MNU_TISC_DOCUMENT_LIST);
        was.printTMenuItem(wam.MNU_TISC_DOCUMENT_CREATE);
        was.printTMenuItem(wam.MNU_TISC_DOCUMENT_FIND);
        was.closeTMenu();
        was.closeTMenu();
    } //menuTISC


} //WARHMain
