#!/usr/bin/awk -f
# (c) Alex V Eustrop & EustroSoft.org 2023
#     while developing ConcepTIS/QTIS projects
# LICENSE: BALES, MIT, BSD or MIT on your choice:
#
# You can do with this Source Code anything that you want
# while keeping the list of its authors
# until this code will be rewritten by you.
# Also you can re-license this Source Code under any other licenses
# for instance well known BSD, MIT or ISC licenses on your choice
#
# script name: to_awk.awk
# usage: to_awk.awk < somefile
# purpose : convert any text to awk script printing this text
#
# test : cat to_awk.awk | ./to_awk.awk | awk -f - > to_awk.awk.tmp; diff to_awk.awk to_awk.awk.tmp
#
# History:
#  2023-04-42 not found - written from the scratch (41 lines)
#

BEGIN{
 print "#!/usr/bin/awk -f";
 printf("BEGIN {%s","\n");
}

{ print "print \"" escape($0) "\";"; }

function escape(s,	c,i,size)
{
size=split(s,c,"");
s="";
for(i=1;i<=size;i++)
{
 if(c[i]=="\\"){c[i]="\\\\";}
 if(c[i]=="\""){c[i]="\\\"";}
 s=s c[i];
}
return s;
}
END{ print "}"; }
